<?php
class yaziyla {
 
    var $sayi=0;
    var $kurus=0;
    var $eksi="";
    var $birim="TL";
    var $kurus_birim = "KR";
    var $bolukler;
    var $birler;
    var $onlar;
 
    function yaziyla($birim="TL", $kurus_birim="KR") {
 
        $this->birim          = $birim;
        $this->kurus_birim    = $kurus_birim;
        $this->bolukler       = array("","BİN","Milyon","Milyar","Trilyon","Katrilyon","Trilyar","Kentrilyon","Kentrilyar","Zontrilyar");
        $this->birler         = array("SIFIR","BİR","İKİ","ÜÇ","DÖRT","BEŞ","ALTI","YEDİ","SEKİZ","DOKUZ");
        $this->onlar          = array("","ON","YİRMİ","OTUZ","KIRK","ELLİ","ALTMIŞ","YETMİŞ","SEKSEN","DOKSAN","YÜZ");
 
    }

    function yaz($sayi) {
 
        $tam="";
        $kurus="";
        if($this->sayi_cozumle($sayi)) {
 
        //return "Hatalı Sayı Formatı!";
        return "";
        }
 
        if(($this->sayi+$this->kurus) == 0) return $this->birler[0].' '.$this->birim;
 
        if($this->sayi>0) $tam = $this->oku($this->sayi);
        if($this->kurus>0) $kurus = $this->oku($this->kurus);
 
        if( $this->sayi == 0 ) return $this->eksi.' '.$kurus.' '.$this->kurus_birim;
        if( $this->kurus == 0 ) return $this->eksi.' '.$tam.' '.$this->birim;
        return $this->eksi.' '.$tam.' '.$this->birim.' '.$kurus.' '.$this->kurus_birim;
    }
    function oku($sayi) {
 
    if($sayi == 0) return $this->birler[0];
        $ubb = sizeof($this->bolukler);
        $kac_sifir = 3 - (strlen($sayi) % 3);
        if($kac_sifir!=3) for($i=0;$i<$kac_sifir;++$i) { $sayi = "0$sayi"; }
 
        $k = 0; $sonuc = "";
        for($i = strlen($sayi); $i>0; $i-=3,++$k) {
 
           $boluk = $this->boluk_oku(substr($sayi, $i-3, 3));
           if($boluk) {
           if(($k == 1) && ($boluk == $this->birler[1])) $boluk = "";
           if(  $k > $ubb) $sonuc = $boluk ."Tanımsız(".($k*3).".Basamak) $sonuc";
           else $sonuc = $boluk . $this->bolukler[$k]." $sonuc";
           }
        }
        return $sonuc;
    }
    function boluk_oku($sayi) {
 
         $sayi = ((int)($sayi)) % 1000; $sonuc = "";
         $bir = $sayi % 10;
         $on_ = (int)($sayi / 10) % 10;
         $yuz = (int)($sayi / 100) % 10;
 
         if($yuz) { if($yuz == 1) $sonuc = $this->onlar[10];
         else $sonuc = $this->birler[$yuz].$this->onlar[10]; }
 
         if($on_) $sonuc = $sonuc.$this->onlar[$on_];
         if($bir) $sonuc = $sonuc.$this->birler[$bir];
         return $sonuc;
    }
    function sayi_cozumle($sayi) {
 
        $sayi = trim($sayi);
        if($sayi[0] == "-") { $this->eksi="Eksi"; $sayi = substr($sayi, 1); }
        if(preg_match("/^(0*\.0+|0*|\.0+)$/", $sayi)) { $this->sayi = $this->kurus = 0; return 0; }
        if(preg_match("/^(\d+)\.(\d+)$/", $sayi, $m))
        {
        $sayi = $m[1]; $this->sayi = (int)preg_replace("/^0+/","",$sayi);
        if(!preg_match("/^0+$/",$m[2])) $this->kurus = (int)$m[2];
        }
        else if(preg_match("/^0*(\d+)$/", $sayi, $m) || preg_match("/^0*(\d+)\.0+$/", $sayi, $m)) { $this->sayi = (int)$m[1]; }
        else if(preg_match("/^0*\.(\d+)$/", $sayi, $m)) { $this->sayi = 0; $this->kurus = (int)$m[1]; }
        else return 1;
        if($this->kurus>0) {
 
        $this->kurus= number_format('0.'.$this->kurus, 2);
        if( (int)$this->kurus == 1 ) { ++$this->sayi; $this->kurus = 0; }
        else $this->kurus = (int)str_replace("0.", "", $this->kurus);
        }
        return 0;
    }
}

$yaziyla = new yaziyla("TL", "KR.");
 
?>
<style>
.container {
    width: 100%!important;
}
.fatura{
	position: relative;
    width: 32%;
    float: left;
    margin: 0 0 0 1%;
}
.fatura .ust{
	height:100px;
}
.fatura .tarih{
	font-size: 11px;
	position: absolute;
	top:120px;
	right: 0;
}
.fatura .adres {
    width: 80%;
}
.fatura .urunler{
	height:400px;
}
.fatura .toplamlar{
	height: 80px;
}
.fatura .yaziile {
    position: absolute;
    bottom: 50px;
}
.fatura .table > tbody > tr > td{
	border-top:0;
	padding: 3px;
	font-size: 11px;
}
</style>
<!DOCTYPE html>

<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<head>
<meta charset="UTF-8" />
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<link href="view/template/sale/invoice/bootstrap/css/bootstrap.css" rel="stylesheet" media="all" />
<script type="text/javascript" src="view/template/sale/invoice/jquery/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="view/template/sale/invoice/bootstrap/js/bootstrap.min.js"></script>
<link href="view/template/sale/invoice/font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet" />
<link type="text/css" href="view/template/sale/invoice/stylesheet.css" rel="stylesheet" media="all" />
</head>
<body>
<div class="container">
  <?php foreach ($orders as $order) { ?>
  
    <div class="fatura bir-nusha">
    <div class="ust"></div>
	<div class="adres">
    <table class="table">
      <tbody>
        <tr>
          <td><address>
            <?php echo $order['payment_address']; ?>

			<?php if($order['customer_fields']){ echo '<br/>'; ?>
				<?php foreach ($order['customer_fields'] as $field){?>
        			<?php echo $field?'<br/>'.$field:''; ?>
       			<?php } ?>
       		<?php } ?>
			
            </address></td>
                        <td><phone>
            Tel: <?php echo $order['telephone']; ?>
            </phone></td>
        </tr>
      </tbody>
    </table>
	</div>
	<div class="tarih"><?php echo $order['date_added']; ?></div>
	<div class="urunler">
    <table class="table">
      <tbody>
        <?php foreach ($order['product'] as $product) { ?>
        <tr>
		  <td width="10%"><?php echo $product['model']; ?></td>
          <td width="40%"><?php echo $product['name']; ?>
            <?php foreach ($product['option'] as $option) { ?>
            &nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
            <?php } ?></td>
          <td width="10%" class="text-right"><?php echo $product['quantity']; ?></td>
          <td width="20%" class="text-right"><?php echo $product['price']; ?></td>
          <td width="20%" class="text-right"><?php echo $product['total']; ?></td>
        </tr>
        <?php } ?>
        <?php foreach ($order['voucher'] as $voucher) { ?>
        <tr>
          <td><?php echo $voucher['description']; ?></td>
          <td></td>
          <td class="text-right">1</td>
          <td class="text-right"><?php echo $voucher['amount']; ?></td>
          <td class="text-right"><?php echo $voucher['amount']; ?></td>
        </tr>
        <?php } ?>
      </tbody>
    </table>
	</div>
	<div class="toplamlar">
		<table class="table">
			<tbody>
			   <?php foreach ($order['total'] as $total) { ?>
				<tr>
				  <td class="text-right" colspan="4"><b><?php echo $total['title']; ?></b></td>
				  <td class="text-right"><?php echo $total['text']; ?></td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
	<div class="yaziile hidden"><?php // echo $yaziyla->yaz($total['yaziyla']); ?>#</div>
  </div>
  
  <div class="fatura iki-nusha">
    <div class="ust"></div>
	<div class="adres">
    <table class="table">
      <tbody>
        <tr>
          <td><address>
            <?php echo $order['payment_address']; ?>

			<?php if($order['customer_fields']){ echo '<br/>'; ?>
				<?php foreach ($order['customer_fields'] as $field){?>
        			<?php echo $field?'<br/>'.$field:''; ?>
       			<?php } ?>
       		<?php } ?>
			
            </address></td>
                        <td><phone>
            Tel: <?php echo $order['telephone']; ?>
            </phone></td>
        </tr>
      </tbody>
    </table>
	</div>
	<div class="tarih"><?php echo $order['date_added']; ?></div>
	<div class="urunler">
    <table class="table">
      <tbody>
        <?php foreach ($order['product'] as $product) { ?>
        <tr>
		  <td width="10%"><?php echo $product['model']; ?></td>
          <td width="40%"><?php echo $product['name']; ?>
            <?php foreach ($product['option'] as $option) { ?>
            &nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
            <?php } ?></td>
          <td width="10%" class="text-right"><?php echo $product['quantity']; ?></td>
          <td width="20%" class="text-right"><?php echo $product['price']; ?></td>
          <td width="20%" class="text-right"><?php echo $product['total']; ?></td>
        </tr>
        <?php } ?>
        <?php foreach ($order['voucher'] as $voucher) { ?>
        <tr>
          <td><?php echo $voucher['description']; ?></td>
          <td></td>
          <td class="text-right">1</td>
          <td class="text-right"><?php echo $voucher['amount']; ?></td>
          <td class="text-right"><?php echo $voucher['amount']; ?></td>
        </tr>
        <?php } ?>
      </tbody>
    </table>
	</div>
	<div class="toplamlar">
		<table class="table">
			<tbody>
			   <?php foreach ($order['total'] as $total) { ?>
				<tr>
				  <td class="text-right" colspan="4"><b><?php echo $total['title']; ?></b></td>
				  <td class="text-right"><?php echo $total['text']; ?></td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
	<div class="yaziile hidden">#<?php //echo $yaziyla->yaz($total['yaziyla']); ?>#</div>
  </div>
      
  <div class="fatura uc-nusha">
    <div class="ust"></div>
	<div class="adres">
    <table class="table">
      <tbody>
        <tr>
          <td><address>
            <?php echo $order['payment_address']; ?>

			<?php if($order['customer_fields']){ echo '<br/>'; ?>
				<?php foreach ($order['customer_fields'] as $field){?>
        			<?php echo $field?'<br/>'.$field:''; ?>
       			<?php } ?>
       		<?php } ?>
			
            </address></td>
                        <td><phone>
            Tel: <?php echo $order['telephone']; ?>
            </phone></td>
        </tr>
      </tbody>
    </table>
	</div>
	<div class="tarih"><?php echo $order['date_added']; ?></div>
	<div class="urunler">
    <table class="table">
      <tbody>
        <?php foreach ($order['product'] as $product) { ?>
        <tr>
		  <td width="10%"><?php echo $product['model']; ?></td>
          <td width="40%"><?php echo $product['name']; ?>
            <?php foreach ($product['option'] as $option) { ?>
            &nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
            <?php } ?></td>
          <td width="10%" class="text-right"><?php echo $product['quantity']; ?></td>
          <td width="20%" class="text-right"><?php echo $product['price']; ?></td>
          <td width="20%" class="text-right"><?php echo $product['total']; ?></td>
        </tr>
        <?php } ?>
        <?php foreach ($order['voucher'] as $voucher) { ?>
        <tr>
          <td><?php echo $voucher['description']; ?></td>
          <td></td>
          <td class="text-right">1</td>
          <td class="text-right"><?php echo $voucher['amount']; ?></td>
          <td class="text-right"><?php echo $voucher['amount']; ?></td>
        </tr>
        <?php } ?>
      </tbody>
    </table>
	</div>
	<div class="toplamlar">
		<table class="table">
			<tbody>
			   <?php foreach ($order['total'] as $total) { ?>
				<tr>
				  <td class="text-right" colspan="4"><b><?php echo $total['title']; ?></b></td>
				  <td class="text-right"><?php echo $total['text']; ?></td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
	<div class="yaziile hidden">#<?php //echo $yaziyla->yaz($total['yaziyla']); ?>#</div>
  </div>

  <?php } ?>
</div>
</body>
</html>