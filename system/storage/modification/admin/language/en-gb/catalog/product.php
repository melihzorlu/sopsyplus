<?php

  //SF PRODUCTS IMAGE MANAGER ->
    //pim drop zone lang
    $_['sf_pim']['u_up_max']    = 'Upload a maximum of';
    $_['sf_pim']['images']        = 'images';
    $_['sf_pim']['dad_an_image']  = 'Drag & Drop an image';
    $_['sf_pim']['image_damaged'] = 'This image is damaged!';
    $_['sf_pim']['select_below']  = 'or select an option below';
    $_['sf_pim']['zoom']          = 'Büyüt';
    $_['sf_pim']['upload']        = 'Upload';
    $_['sf_pim']['link']          = 'Link';
    $_['sf_pim']['server']        = 'Server';
    $_['sf_pim']['loading']       = 'Loading ';
    $_['sf_pim']['exist']         = 'Image already exists!';
    $_['sf_pim']['keep_old']      = 'Take old';
    $_['sf_pim']['keep_both']     = 'Keep both';
    $_['sf_pim']['replace']       = 'Replace';
    $_['sf_pim']['delete']        = 'Sil';
    $_['sf_pim']['delete_all']    = 'Remove next all';
    $_['sf_pim']['mAdd']          = 'Multiple Add';

    //pim modal lang
    $_['sf_pim']['pim']             = 'Products image manager';
    $_['sf_pim']['settings']        = 'Settings';
    $_['sf_pim']['max_images']      = 'Max images';
    $_['sf_pim']['max_image_size']  = 'Reduce size';
    $_['sf_pim']['resolution']      = 'Resolution';
    $_['sf_pim']['resolution']      = 'Compression';
    $_['sf_pim']['image_quality']   = 'Reduce quality';
    $_['sf_pim']['remove_images']   = 'Remove images';
    $_['sf_pim']['when_should']     = 'When should remove images from the server';
    $_['sf_pim']['do_not']          = 'Do not';
    $_['sf_pim']['when_pro_is_del'] = 'When product is deleted';
    $_['sf_pim']['when_rem_4_pro']  = 'When removed from product';
    $_['sf_pim']['anyway']          = 'Anyway';
    $_['sf_pim']['image_name']      = 'Image name';
    $_['sf_pim']['default']         = 'Default';
    $_['sf_pim']['random']          = 'Random';
    $_['sf_pim']['translit']        = 'Translit';
    $_['sf_pim']['example']         = 'Example';
    $_['sf_pim']['upload_folder']   = 'Upload folder';
    $_['sf_pim']['ask_me']          = 'Ask me';
    $_['sf_pim']['top_left']        = 'Top left';
    $_['sf_pim']['bottom_left']     = 'Bottom left';
    $_['sf_pim']['top_right']       = 'Top right';
    $_['sf_pim']['bottom_right']    = 'Bottom right';
    $_['sf_pim']['center']          = 'Center';
    $_['sf_pim']['multiple']        = 'Multiple';
    $_['sf_pim']['position']        = 'Position';
    $_['sf_pim']['text_or_url']     = 'Insert an image url or a text';
    $_['sf_pim']['size']            = 'Size';
    $_['sf_pim']['opacity']         = 'Opacity';
    $_['sf_pim']['dimensions']      = 'Dimensions';
    $_['sf_pim']['transmittance']   = 'Transmittance';
    $_['sf_pim']['watermark']       = 'Watermark';
    $_['sf_pim']['watermark_not_found']= 'Watermark image not found: Please try another image link or use any text!';

    //text  success
    $_['sf_pim']['save_success']    = 'You have modified settings!';
    $_['sf_pim']['received_success']= 'Content received!';

    //text error
    $_['sf_pim']['nodir_error']     = 'Undefined directory';
    $_['sf_pim']['save_error']      = 'You did not modify the SF PIM settings!';
    $_['sf_pim']['file_error']      = 'Undefined file';
    $_['sf_pim']['permission_error']= 'You do not have permission to modify the SF PIM settings!';
  //<-SF PRODUCTS IMAGE MANAGER
  
// Heading
$_['heading_title']          = 'Products';

// Text
$_['text_success']           = 'Success: You have modified products!';
$_['text_list']              = 'Product List';
$_['text_add']               = 'Add Product';
$_['text_edit']              = 'Edit Product';
$_['text_plus']              = '+';
$_['text_minus']             = '-';
$_['text_default']           = 'Default';
$_['text_option']            = 'Option';
$_['text_option_value']      = 'Option Value';
$_['text_percent']           = 'Percentage';
$_['text_amount']            = 'Fixed Amount';

// Column
$_['column_name']            = 'Product Name';
$_['column_model']           = 'Model';
$_['column_image']           = 'Image';
$_['column_price']           = 'Price';
$_['column_quantity']        = 'Quantity';
$_['column_status']          = 'Status';
$_['column_action']          = 'Action';

// Entry
$_['entry_name']             = 'Product Name';
$_['entry_description']      = 'Description';
$_['entry_meta_title'] 	     = 'Meta Tag Title';
$_['entry_meta_keyword'] 	 = 'Meta Tag Keywords';
$_['entry_meta_description'] = 'Meta Tag Description';
$_['entry_keyword']          = 'SEO URL';
$_['entry_model']            = 'Model';
$_['entry_sku']              = 'SKU';
$_['entry_upc']              = 'UPC';
$_['entry_ean']              = 'EAN';
$_['entry_jan']              = 'JAN';
$_['entry_isbn']             = 'ISBN';
$_['entry_mpn']              = 'MPN';
$_['entry_location']         = 'Location';
$_['entry_shipping']         = 'Requires Shipping';
$_['entry_manufacturer']     = 'Manufacturer';
$_['entry_store']            = 'Stores';
$_['entry_date_available']   = 'Date Available';
$_['entry_quantity']         = 'Quantity';
$_['entry_minimum']          = 'Minimum Quantity';
$_['entry_stock_status']     = 'Out Of Stock Status';
$_['entry_price']            = 'Price';
$_['entry_tax_class']        = 'Tax Class';
$_['entry_points']           = 'Points';
$_['entry_option_points']    = 'Points';
$_['entry_subtract']         = 'Subtract Stock';
$_['entry_weight_class']     = 'Weight Class';
$_['entry_weight']           = 'Weight';
$_['entry_dimension']        = 'Dimensions (L x W x H)';
$_['entry_length_class']     = 'Length Class';
$_['entry_length']           = 'Length';
$_['entry_width']            = 'Width';
$_['entry_height']           = 'Height';
$_['entry_image']            = 'Image';
$_['entry_additional_image'] = 'Additional Images';
$_['entry_customer_group']   = 'Customer Group';
$_['entry_date_start']       = 'Date Start';
$_['entry_date_end']         = 'Date End';
$_['entry_priority']         = 'Priority';
$_['entry_attribute']        = 'Attribute';
$_['entry_attribute_group']  = 'Attribute Group';
$_['entry_text']             = 'Text';
$_['entry_option']           = 'Option';
$_['entry_option_value']     = 'Option Value';
$_['entry_required']         = 'Required';
$_['entry_status']           = 'Status';
$_['entry_sort_order']       = 'Sort Order';
$_['entry_category']         = 'Categories';
$_['entry_filter']           = 'Filters';
$_['entry_download']         = 'Downloads';
$_['entry_related']          = 'Related Products';
$_['entry_tag']          	 = 'Product Tags';
$_['entry_reward']           = 'Reward Points';
$_['entry_layout']           = 'Layout Override';
$_['entry_recurring']        = 'Recurring Profile';

// Help
$_['help_keyword']           = 'Do not use spaces, instead replace spaces with - and make sure the SEO URL is globally unique.';
$_['help_sku']               = 'Stock Keeping Unit';
$_['help_upc']               = 'Universal Product Code';
$_['help_ean']               = 'European Article Number';
$_['help_jan']               = 'Japanese Article Number';
$_['help_isbn']              = 'International Standard Book Number';
$_['help_mpn']               = 'Manufacturer Part Number';
$_['help_manufacturer']      = '(Autocomplete)';
$_['help_minimum']           = 'Force a minimum ordered amount';
$_['help_stock_status']      = 'Status shown when a product is out of stock';
$_['help_points']            = 'Number of points needed to buy this item. If you don\'t want this product to be purchased with points leave as 0.';
$_['help_category']          = '(Autocomplete)';
$_['help_filter']            = '(Autocomplete)';
$_['help_download']          = '(Autocomplete)';
$_['help_related']           = '(Autocomplete)';
$_['help_tag']               = 'Comma separated';

// Error
$_['error_warning']          = 'Warning: Please check the form carefully for errors!';
$_['error_permission']       = 'Warning: You do not have permission to modify products!';
$_['error_name']             = 'Product Name must be greater than 3 and less than 255 characters!';
$_['error_meta_title']       = 'Meta Title must be greater than 3 and less than 255 characters!';
$_['error_model']            = 'Product Model must be greater than 1 and less than 64 characters!';
$_['error_keyword']          = 'SEO URL already in use!';
