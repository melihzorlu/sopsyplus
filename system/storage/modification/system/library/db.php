<?php
class DB {

    // Journal Theme Modification
    public static $LOG = [];
    // End Journal Theme Modification
      
	private $adaptor;

	public function __construct($adaptor, $hostname, $username, $password, $database, $port = NULL) {
		$class = 'DB\\' . $adaptor;

		if (class_exists($class)) {
			$this->adaptor = new $class($hostname, $username, $password, $database, $port);
		} else {
			throw new \Exception('Error: Could not load database adaptor ' . $adaptor . '!');
		}
	}

	public function query($sql, $params = array()) {

    // Journal Theme Modification
    $time = microtime(true);

    if (version_compare(VERSION, '3', '>=')) {
        $result = $this->adaptor->query($sql);
    } else {
        $result = $this->adaptor->query($sql, $params);
    }

    $time = (microtime(true) - $time) * 1000;

    $data = [
      'file' => debug_backtrace()[array_search(__FUNCTION__, array_column(debug_backtrace(), 'function'))]['file'],
    ];

    if (function_exists('clock')) {
      clock()->addDatabaseQuery($sql, [], $time, $data);
    } else {
      static::$LOG[] = ['sql' => $sql, 'time' => $time, 'data' => $data];
    }

    return $result;
    // End Journal Theme Modification
      
		return $this->adaptor->query($sql, $params);
	}

	public function escape($value) {
		return $this->adaptor->escape($value);
	}

	public function countAffected() {
		return $this->adaptor->countAffected();
	}

	public function getLastId() {
		return $this->adaptor->getLastId();
	}
	
	public function connected() {
		return $this->adaptor->connected();
	}
}