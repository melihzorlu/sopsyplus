<?php

$_['notifywhenavailable_name']                           = 'NotifyWhenAvailable';
$_['notifywhenavailable_name_small']                     = 'notifywhenavailable';

$_['notifywhenavailable_version']                        = '2.8';

$_['notifywhenavailable_path']                           = 'module/notifywhenavailable';
$_['notifywhenavailable_model_call']                     = 'model_module_notifywhenavailable';

$_['notifywhenavailable_extensions_link']                = 'extension/module';
$_['notifywhenavailable_extensions_link_params']         = '';

$_['notifywhenavailable_token_string']                   = 'token';