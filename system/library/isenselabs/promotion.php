<?php

if(version_compare(VERSION, '2.2.0.0', '<=')) {

	$_['promotion_moduleName'] 			= 'promotion';
	$_['promotion_moduleVersion'] 		= '2.4.1';
	$_['promotion_modulePath'] 			= 'module/promotion';

	$_['promotion_modulePath_stats'] 	= 'module/promotion_stats';

	$_['promotion_callModel'] 			= 'model_module_promotion';

	$_['promotion_extensionLink'] 		= 'extension/module';
	$_['promotion_extensionLink_type'] 	= '';

	$_['promotion_token']				= 'token';

	$_['promotion_setting_model_path']	= 'extension/module';
	$_['promotion_setting_model']		= 'model_extension_module';


	$_['promotion_totalName']  			= 'promotion_total';

	$_['promotion_totalPath']  			= 'total/promotion_total';
	$_['promotion_totalModel']  		= 'model_total_promotion_total';

	$_['promotion_totalsLink']  		= 'extension/total';
	$_['promotion_totalsLink_type']  	= '';

} else if(version_compare(VERSION, '2.2.0.0', '>') && version_compare(VERSION, '3.0.0.0', '<')) {

	$_['promotion_moduleName'] 			= 'promotion';
	$_['promotion_moduleVersion'] 		= '2.4.1';
	$_['promotion_modulePath'] 			= 'extension/module/promotion';

	$_['promotion_modulePath_stats'] 	= 'extension/module/promotion_stats';

	$_['promotion_callModel'] 			= 'model_extension_module_promotion';

	$_['promotion_extensionLink'] 		= 'extension/extension';
	$_['promotion_extensionLink_type'] 	= '&type=module';

	$_['promotion_token']				= 'token';

	$_['promotion_setting_model_path']	= 'extension/module';
	$_['promotion_setting_model']		= 'model_extension_module';


	$_['promotion_totalName']  			= 'promotion_total';

	$_['promotion_totalPath']  			= 'extension/total/promotion_total';
	$_['promotion_totalModel']  		= 'model_extension_total_promotion_total';

	$_['promotion_totalsLink']  		= 'extension/extension';
	$_['promotion_totalsLink_type']  	= '&type=total';

} elseif(version_compare(VERSION, '3.0.0.0', '==')) {

	$_['promotion_moduleName'] 			= 'promotion';
	$_['promotion_moduleVersion'] 		= '2.4.1';
	$_['promotion_modulePath'] 			= 'extension/module/promotion';

	$_['promotion_modulePath_stats'] 	= 'extension/module/promotion_stats';

	$_['promotion_callModel'] 			= 'model_extension_module_promotion';

	$_['promotion_extensionLink'] 		= 'marketplace/extension';
	$_['promotion_extensionLink_type'] 	= '&type=module';

	$_['promotion_token']				= 'user_token';

	$_['promotion_setting_model_path']	= 'setting/module';
	$_['promotion_setting_model']		= 'model_setting_module';


	$_['promotion_totalName']  			= 'total_promotion_total';

	$_['promotion_totalPath']  			= 'extension/total/promotion_total';
	$_['promotion_totalModel']  		= 'model_extension_total_promotion_total';

	$_['promotion_totalsLink']  		= 'marketplace/extension';
	$_['promotion_totalsLink_type']  	= '&type=total';
}






