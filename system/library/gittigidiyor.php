<?php
ini_set("soap.wsdl_cache", "0");
ini_set("soap.wsdl_cache_enabled", "0");
class GittiGidiyor{
	
	public $apiKey;
	public $secretKey;
	public $lang;													
	public $sign;													
	public $time;													
	public $auth_user;
	public $auth_pass;

	//Start Category Service
	
	/**
	 * Kategori kodu ve kategori detay bilgilerine ulasmak icin bu metod kullanilmalidir. 
	 * 
	 * Detayli Bilgi: http://dev.gittigidiyor.com/metotlar/getCategories-CategoryService-soap-anonymous
	 * 
	 * Category Service
	 * Get Categories Method
	 * 
	 * @param integer	$startOffset
	 * @param integer	$rowCount
	 * @param string	$withSpecs
	 * 
	 * @return object Category list
	 */
	public function getCategories($startOffset = 0, $rowCount = 2, $withSpecs){
		//$withSpecs = $this->booleanConvert($withSpecs);
		return $this->clientConnect('anonymous','Category','getCategories',get_defined_vars());
	}
	
	
	/**
	 * Kategori bilgilerinde zaman icerisinde degisiklik olabilmektedir.
	 * Sadece degisen kategori bilgilerine ihtiyac duyuldugunda belirtilen bir tarihten 
	 * sonra olan degisiklikler bu metod araciligi ile elde edilebilmektedir 
	 * Burada dikkat edilmesi gereken nokta changeTime parametresinin su an ki zamandan 
	 * buyuk bir degere sahip olmamasi ve gecmisteki bir zamana isaret etmesidir. 
	 * 
	 * Detayli Bilgi: http://dev.gittigidiyor.com/metotlar/getModifiedCategories-CategoryService-soap-anonymous
	 * 
	 * Category Service
	 * Get Modified Categories Method
	 * 
	 * @param integer	$startOffset
	 * @param integer	$rowCount
	 * @param time		$changeTime		format => 01/01/2008+00:00:00
	 * 
	 * @return object Modified Category list
	 */
	public function getModifiedCategories($startOffset = 0, $rowCount = 2, $changeTime = '01/01/2008+00:00:00'){
		return $this->clientConnect('anonymous','Category','getModifiedCategories',get_defined_vars());
	}
	
	
	/**
	 * Herhangi bir kategorinin detay bilgisine ihtiyac duyuldugunda cagirilmasi gereken metoddur.
	 * 
	 * Detayli Bilgi: http://dev.gittigidiyor.com/metotlar/getCategory-CategoryService-soap-anonymous
	 * 
	 * Category Service
	 * Get Category Info Method
	 * 
	 * @param string	$categoryCode
	 * @param string	$withSpecs
	 * 
	 * @return object Selected category info
	 */
	public function getCategory($categoryCode = null, $withSpecs = true){
		//$withSpecs = $this->booleanConvert($withSpecs);
		return $this->clientConnect('anonymous','Category','getCategory',get_defined_vars());
	}

	// ana kategorileri alalım
	public function getParentCategories($withSpecs = false, $withDeepest = false, $withCatalog = false){
		//$withSpecs = $this->booleanConvert($withSpecs);
		return $this->clientConnect('anonymous','Category','getParentCategories',get_defined_vars());
	}

	public function getSubCategories($categoryCode, $withSpecs = false, $withDeepest = false, $withCatalog = false){
		//$withSpecs = $this->booleanConvert($withSpecs);
		return $this->clientConnect('anonymous','Category','getSubCategories',get_defined_vars());
	}
	
	
	
	/**
	 * Sadece kategori ozelliklerinin alinmasi gerektigi durumda cagirilmasi gereken metoddur. 
	 * 
	 * Detayli Bilgi: http://dev.gittigidiyor.com/metotlar/getCategorySpecs-CategoryService-soap-anonymous
	 * 
	 * Category Service
	 * Get Category Specs Info Method
	 * 
	 * @param string	$categoryCode
	 * 
	 * @return object Selected categorys specs
	 */
	public function getCategorySpecs($categoryCode = null){
		return $this->clientConnect('anonymous','Category','getCategorySpecs',get_defined_vars());
	}
	
	
	
	/**
	 * Istenilen kategoriye ait variant seceneklerinin cagirilmasi icin gereken metoddur.
	 * 
	 * Category Service
	 * Get Category Variant Specs
	 * 
	 * @param string $categoryCode
	 * 
	 * @return object Selected category variant specs
	 */
	
	public function getCategoryVariantSpecs($categoryCode = null){
		return $this->clientConnect('anonymous','Category','getCategoryVariantSpecs',get_defined_vars());
	}
	
	
	
	/**
	 * Hangi kategorilerde variant oldugunun ogrenilmesi icin gereken metoddur.
	 * 
	 * Cateogry Service
	 * Get Categories Having Variant Specs
	 * 
	 * @return object Categories
	 */
	public function getCategoriesHavingVariantSpecs(){
		return $this->clientConnect('anonymous','Category','getCategoriesHavingVariantSpecs',get_defined_vars());
	}
	
	
	//End Category Service
	
	
	
	
	
	//Start City Service
		
	/**
	 * Gitti Gidiyor sistemindeki sehir ad ve kodlarini sunan servistir
	 * Şehir kodlari il plaka kodlarindan farkli olabilecegi icin degerlerinin bu servisten kontrol edilmesi gerekmektedir.
	 * 
	 * Detayli Bilgi: http://dev.gittigidiyor.com/metotlar/getCities-CityService-soap-anonymous
	 * 
	 * City Service
	 * Get Cities Method
	 * 
	 * @param integer	$startOffset
	 * @param integer	$rowCount
	 * 
	 * @return object Cities list
	 */
	public function getCities($startOffset = 0, $rowCount = 5){
		return $this->clientConnect('anonymous','City','getCities',get_defined_vars());
	}

	
	/**
	 * Şehir bilgilerinde zaman icerisinde degisiklik olabilmektedir,
	 * Sadece degisen sehir bilgilerine ihtiyac duyuldugunda belirtilen bir tarihten 
	 * sonra olan degisiklikler bu metod araciligi ile elde edilebilmektedir.
	 * 
	 * Detayli Bilgi : http://dev.gittigidiyor.com/metotlar/getModifiedCities-CityService-soap-anonymous
	 * 
	 * City Service
	 * 
	 * Get Modified Cities Method
	 * 
	 * @param time		$changeTime		format=>01/01/2008+00:00:00
	 * @param integer	$startOffset
	 * @param integer	$rowCount
	 * 
	 * @return object Modified Cities List
	 */
	public function getModifiedCities($changeTime = '01/01/2008+00:00:00', $startOffset = 0, $rowCount = 5){
		return $this->clientConnect('anonymous','City','getModifiedCities',get_defined_vars());
	}
	
	
	/**
	 * Sistemde kullanilan sehir verilerinden herhangi birisinin detayina 
	 * erisilmek istendigi durumda cagirilmasi gereken metoddur.
	 * 
	 * Detayli Bilgi : http://dev.gittigidiyor.com/metotlar/getCity-CityService-soap-anonymous
	 * 
	 * City Service
	 * Get City Info Method
	 * 
	 * @param string $code
	 * 
	 * @return object Selected City info
	 */
	public function getCity($code = null){
		return $this->clientConnect('anonymous','City','getCity',get_defined_vars());
	}
	
	//End City Service
	
	
	
	
	
	//Start Product Service
	
	/**
	 * Bir urunun detay bilgisini almak icin bu metod kullanilmalidir.
	 * 
	 * Detayli Bilgi : http://dev.gittigidiyor.com/metotlar/getProduct-ProductService-soap-individual
	 * 
	 * Product Service
	 * Get Product Info Method
	 * 
	 * @param string	$productId
	 * @param string	$itemId
	 * 
	 * @return object Selected Product info
	 */
	public function getProduct($productId = '',$itemId = ''){
		return $this->clientConnect('individual','Product','getProduct',get_defined_vars());
	}
	
	
	/**
	 * Bu metod farkli durumlara (productStatus) sahip urunlerin listelesini almak icin kullanilmaktadir
	 * “status” parametresi listesi alinmak istenen urunlerin durumunu ifade etmektedir.
	 * 
	 * Detayli Bilgi : http://dev.gittigidiyor.com/metotlar/getProducts-ProductService-soap-individual
	 * 
	 * Product Service
	 * Get Products Info Method
	 * 
	 * @param integer	$startOffset
	 * @param integer	$rowCount
	 * @param string	$status
	 * @param string	$withData
	 * 
	 * @return object Products List
	 */
	public function getProducts($startOffset = 0, $rowCount = 3, $status = 'A', $withData = false){
		//$withData = $this->booleanConvert($withData);
		return $this->clientConnect('individual','Product','getProducts',get_defined_vars());
	}
	
	
	/**
	 * Urune ait variant bilgilerini dondurur
	 * 
	 * Product Service
	 * Get Product Variants
	 * 
	 * @param string $productId
	 * @param string $itemId
	 * @param string $variantId
	 * @param string $variantStockCode
	 * 
	 * @return object variand data
	 */
	public function getProductVariants($productId = '',$itemId = '',$variantId = '',$variantStockCode = ''){
		return $this->clientConnect('individual','Product','getProductVariants',get_defined_vars());
	}
	
	
	/**
	 * Listeleme servisine urun kaydetmek icin kullanilmasi gereken metoddur.
	 * Detayli Bilgi : http://dev.gittigidiyor.com/servisler/insertProduct-ProductService-soap-individual
	 * 
	 * Product Service
	 * Insert Product Method
	 * 
	 * @param string	$options Icerigi xml olan bu degisken belli bir standarda sahipdir. Daha fazla bilgi icin api document.
	 * @param string	$itemId Kategori bazli spec girisi 
	 * @param string	$nextDateOption Ileri tarihli urun girme islemi
	 * 
	 * @return Web Servis mesajini object olarak dondurur
	 */
	public function insertProducts($itemId = null,$product,$forceToSpecEntry = false,$nextDateOption = false){
		return $this->clientConnect('individual','Product','insertProduct',get_defined_vars(),array('product'));
	}

	public function insertProductWithNewCargoDetail($itemId = null,$product,$forceToSpecEntry = false,$nextDateOption = false){
		return $this->clientConnect('individual','Product','insertProductWithNewCargoDetail',get_defined_vars(),array('product'));
	}
	
	
	
	/**
	 * Listeleme servisine variant urun kaydetmek icin kullanilmasi gereken metoddur.
	 * 
	 * Product Service
	 * Insert Retail Product Method
	 *
	 * @param string	$options Icerigi xml olan bu degisken belli bir standarda sahipdir. Daha fazla bilgi icin api document.
	 * @param string	$itemId Kategori bazli spec girisi
	 * @param string	$nextDateOption Ileri tarihli urun girme islemi
	 *
	 * @return Web Servis mesajini object olarak dondurur
	 */
	public function insertRetailProduct($itemId = null,$product,$forceToSpecEntry = false,$nextDateOption = false){
		return $this->clientConnect('individual','Product','insertRetailProduct',get_defined_vars(),array('product'));
	}

	public function insertRetailProductWithNewCargoDetail($itemId = null,$product,$forceToSpecEntry = false,$nextDateOption = false){
		return $this->clientConnect('individual','Product','insertRetailProductWithNewCargoDetail',get_defined_vars(),array('product'));
	}

	public function updateRetailProductWithNewCargoDetail($itemId = null,$product,$forceToSpecEntry = false,$nextDateOption = false){
		return $this->clientConnect('individual','Product','updateRetailProductWithNewCargoDetail',get_defined_vars(),array('product'));
	}
	
	
	
	/**
	 * Belli bir urunun kopyasini almak icin kullanilmasi gereken metoddur.
	 * Detayli Bilgi : http://dev.gittigidiyor.com/metotlar/cloneProduct-ProductService-soap-individual
	 * 
	 * Product Service
	 * Clone Product Method
	 * 
	 * @param string $productId
	 * @param string $itemId
	 * 
	 * @return  Web Servis mesajini object olarak dondurur
	 */
	public function cloneProduct($productId = '',$itemId = ''){
		return $this->clientConnect('individual','Product','cloneProduct',get_defined_vars());
	}
	
	
	/**
	 * Ürun/urunleri silmek icin kullanilir. 
	 * Detayli Bilgi : http://dev.gittigidiyor.com/metotlar/deleteProduct-ProductService-soap-individual
	 * 
	 * Product Service
	 * Delete Product Method
	 * 
	 * @param array $products
	 * @param array $items
	 * 
	 * @return Web Servis mesajini object olarak dondurur
	 */
	
	public function deleteProduct($products = array(),$items = array()){
		$xml = '<productIdList>';
		if (count($products) > 0){
			foreach ($products as $product){
				$xml .= "<item>{$product}</item>";
			}
		}
		$xml .= '</productIdList>';
		$options['productIdList'] = $xml;
		
		$xml = '<itemIdList>';
		if (count($items) > 0){
			foreach ($items as $item){
				$xml .= "<item>{$item}</item>";
			}
		}
		$xml .= '</itemIdList>';
		$options['itemIdList'] = $xml;
		
		return $this->clientConnect('individual','Product','deleteProduct',$options,array('productIdList','itemIdList'));
	}
	
	
	
	/**
	 * Ürun/urunleri silmek icin kullanilir. 
	 * Detayli Bilgi : 
	 * 
	 * Product Service
	 * Delete Products Method
	 * 
	 * @param array $products
	 * @param array $items
	 * 
	 * @return Web Servis mesajini object olarak dondurur
	*/
	public function deleteProducts($products = array(),$items = array()){
		$xml = '<productIdList>';
		if (count($products) > 0){
			foreach ($products as $product){
				$xml .= "<item>{$product}</item>";
			}
		}
		$xml .= '</productIdList>';
		$options['productIdList'] = $xml;
		
		$xml = '<itemIdList>';
		if (count($items) > 0){
			foreach ($items as $item){
				$xml .= "<item>{$item}</item>";
			}
		}
		$xml .= '</itemIdList>';
		$options['itemIdList'] = $xml;
		
		return $this->clientConnect('individual','Product','deleteProducts',$options,array('productIdList','itemIdList'));
	}
	
	
	
	
	/**
	 * Satılmayan urunleri yeniden listelemek icin kullanilir.
	 * Detayli Bilgi : ?
	 * 
	 * Product Service
	 * ReList Products Method
	 * 
	 * @param array $products
	 * @param array $items
	 * 
	 * @return Web Servis mesajini object olarak dondurur
	 */
	public function relistProducts($products = array(),$items = array()){
		$xml = '<productIdList>';
		if (count($products) > 0){
			foreach ($products as $product){
				$xml .= "<item>{$product}</item>";
			}
		}
		$xml .= '</productIdList>';
		$options['productIdList'] = $xml;
		
		$xml = '<itemIdList>';
		if (count($items) > 0){
			foreach ($items as $item){
				$xml .= "<item>{$item}</item>";
			}
		}
		$xml .= '</itemIdList>';
		$options['itemIdList'] = $xml;
		
		return $this->clientConnect('individual','Product','relistProducts',$options,array('productIdList','itemIdList'));
	}
	
	
	
	/**
	 * GittiGidiyor'da "Yeni Listelenenler" bolumundeki urunlere ulasmak icin kullanilir. 
	 * Detayli Bilgi : http://dev.gittigidiyor.com/metotlar/getNewlyListedProductIdList-ProductService-soap-individual
	 * 
	 * Product Service
	 * getNewlyListedProductIdList Method
	 * 
	 * @param integer	$startOffset
	 * @param integer	$rowCount
	 * @param boolean	$viaApi
	 * 
	 * @return object Yeni Listelenen Ürunler
	 */
	public function getNewlyListedProductIdList($startOffset = 0, $rowCount = 3,$viaApi = true){
		return $this->clientConnect('individual','Product','getNewlyListedProductIdList',get_defined_vars());
	}
	
	
	/**
	 * Satisa cikarilacak urun/urunler icin odenmesi gereken listeleme servisi ucretini ya da satilmayan 
	 * fakat yeniden listelenmesi istenen urun/urunlerin listeleme ucretini yeniden hesaplamak icin kullanilir. 
	 * Detayli Bilgi : http://dev.gittigidiyor.com/metotlar/calculatePriceForShoppingCart-ProductService-soap-individual
	 * 
	 * Product Service
	 * calculatePriceForShoppingCart Method
	 * 
	 * @param array $products
	 * @param array $items
	 * 
	 * @return object urun fiyati
	 */
	public function calculatePriceForShoppingCart($products = array(),$items = array()){
		$xml = '<productIdList>';
		if (count($products) > 0){
			foreach ($products as $product){
				$xml .= "<item>{$product}</item>";
			}
		}
		$xml .= '</productIdList>';
		$options['productIdList'] = $xml;
		
		$xml = '<itemIdList>';
		if (count($items) > 0){
			foreach ($items as $item){
				$xml .= "<item>{$item}</item>";
			}
		}
		$xml .= '</itemIdList>';
		$options['itemIdList'] = $xml;
		return $this->clientConnect('individual','Product','calculatePriceForShoppingCart',$options,array('productIdList','itemIdList'));
	}
	
	
	/**
	 * Satisa cikarilmis bir urunde yapilacak revizyonun ucretini hesaplamak icin kullanilir. 
	 * Detayli Bilgi : http://dev.gittigidiyor.com/metotlar/calculatePriceForRevision-ProductService-soap-individual
	 * 
	 * Product Service
	 * calculatePriceForRevision Method
	 * 
	 * @param string $productId
	 * @param string $itemId
	 * 
	 * @return object revizyon fiyati
	 */
	public function calculatePriceForRevision($productId = null, $itemId = null){
		return $this->clientConnect('individual','Product','calculatePriceForRevision',get_defined_vars());
	}
	
	
	/**
	 * Listeleme servisi ve revizyon ucretlerini odemek icin kullanilir. 
	 * Kullanici, odeme ceki ve kredi karti bilgilerini kullanarak odeme islemini gerceklestirir.
	 * Detayli Bilgi : http://dev.gittigidiyor.com/metotlar/payPrice-ProductService-soap-individual
	 * 
	 * Product Service
	 * Pay Price Method
	 * 
	 * @param string $voucher
	 * @param string $ccOwnerName
	 * @param string $ccOwnerSurname
	 * @param integer $ccNumber
	 * @param integer $cvv
	 * @param integer $expireMonth
	 * @param integer $expireYear
	 * 
	 * @return Web Servis mesajini object olarak dondurur
	 */
	public function payPrice($voucher,$ccOwnerName,$ccOwnerSurname,$ccNumber,$cvv,$expireMonth,$expireYear){
		return $this->clientConnect('individual','Product','payPrice',get_defined_vars());
	}
	
	
	/**
	 * Satistaki urunu erken sonlandirmak icin kullanilir. 
	 * Detayli Bilgi : http://dev.gittigidiyor.com/metotlar/finishEarly-ProductService-soap-individual
	 * 
	 * Product Service
	 * finishEarly Method
	 * 
	 * @param array $products
	 * 
	 * @return Web Servis mesajini object olarak dondurur
	 */
	public function finishEarly($products = array(),$items = array()){
		$xml = '<productIdList>';
		if (count($products) > 0){
			foreach ($products as $product){
				$xml .= "<item>{$product}</item>";
			}
		}
		$xml .= '</productIdList>';
		$options['productIdList'] = $xml;
		
		$xml = '<itemIdList>';
		if (count($items) > 0){
			foreach ($items as $item){
				$xml .= "<item>{$item}</item>";
			}
		}
		$xml .= '</itemIdList>';
		$options['itemIdList'] = $xml;
		return $this->clientConnect('individual','Product','finishEarly',$options,array('productIdList','itemIdList'));
	}
	
	
	
	/**
	 * Satistaki urunu erken sonlandirmak icin kullanilir. 
	 * Detayli Bilgi : http://dev.gittigidiyor.com/metotlar/finishEarly-ProductService-soap-individual
	 * 
	 * Product Service
	 * finishEarly Method
	 * 
	 * @param array $products
	 * 
	 * @return Web Servis mesajini object olarak dondurur
	 */
	public function finishEarlyProducts($products = array(),$items = array()){
		$xml = '<productIdList>';
		if (count($products) > 0){
			foreach ($products as $product){
				$xml .= "<item>{$product}</item>";
			}
		}
		$xml .= '</productIdList>';
		$options['productIdList'] = $xml;
		
		$xml = '<itemIdList>';
		if (count($items) > 0){
			foreach ($items as $item){
				$xml .= "<item>{$item}</item>";
			}
		}
		$xml .= '</itemIdList>';
		$options['itemIdList'] = $xml;
		return $this->clientConnect('individual','Product','finishEarlyProducts',$options,array('productIdList','itemIdList'));
	}
	
	
	
	/**
	 * Fiyat bilgilerini guncellemek icin kullanilir.  
	 * Detayli Bilgi : http://dev.gittigidiyor.com/metotlar/updatePrice-ProductService-soap-individual
	 * 
	 * Product Service
	 * Update Price Method
	 * 
	 * @param string $productId
	 * @param string $itemId
	 * @param string $price
	 * @param string $cancelBid
	 * 
	 * @return Web Servis mesajini object olarak dondurur
	 */
	public function updatePrice($productId,$itemId,$price,$cancelBid){
		return $this->clientConnect('individual','Product','updatePrice',get_defined_vars());
	}
	
	
	/**
	 * Stok bilgilerini guncellemek icin kullanilir.   
	 * Detayli Bilgi : http://dev.gittigidiyor.com/metotlar/updateStock-ProductService-soap-individual
	 * 
	 * Product Service
	 * Update Stock Method
	 * 
	 * @param string $productId
	 * @param string $itemId
	 * @param string $stock
	 * @param string $cancelBid
	 * 
	 * @return Web Servis mesajini object olarak dondurur
	 */
	public function updateStock($productId,$itemId,$stock,$cancelBid){
		return $this->clientConnect('individual','Product','updateStock',get_defined_vars());
	}
	
	
	
	/**
	 * Urun guncellemek icin kullanilmasi gereken metoddur.
	 * Detayli Bilgi : http://dev.gittigidiyor.com/servisler/updateProduct-ProductService-soap-individual
	 * 
	 * Product Service
	 * Update Product Method
	 *
	 * @return Web Servis mesajini object olarak dondurur
	 */
	public function updateProduct($itemId = null, $productId = null, $product, $onSale = true, $forceToSpecEntry = false, $nextDateOption = false){
		return $this->clientConnect('individual','Product','updateProduct',get_defined_vars(),array('product'));
	}

	public function updateProductWithNewCargoDetail($itemId = null, $productId = null, $product, $onSale = true, $forceToSpecEntry = false, $nextDateOption = false){
		return $this->clientConnect('individual','Product','updateProductWithNewCargoDetail',get_defined_vars(),array('product'));
	}
	
	
	/**
	 * Urun variant bilgileirni guncellemek icin kullanılmasi gereken metoddur.
	 * 
	 * Product Service
	 * Update Product Variants
	 * 
	 * @param unknown_type $itemId
	 * @param unknown_type $productId
	 * @param unknown_type $productVariant
	 * 
	 * @return Web Servis mesajini object olarak dondurur
	 */
	public function updateProductVariants($itemId = null, $productId = null, $productVariant){
		return $this->clientConnect('individual','Product','updateProductVariants',get_defined_vars(),array('productVariant'));
	}
	
	
	
	
	/**
	 * Stok ve Fiyat bilgilerini almak icin kullanilir.  
	 * Detayli Bilgi : http://dev.gittigidiyor.com/metotlar/getStockAndPrice-ProductService-soap-individual
	 * 
	 * Product Service
	 * getStockAndPrice Method
	 * 
	 * @param array $products
	 * @param array $items
	 * 
	 * @return Web Servis mesajini object olarak dondurur
	 */
	public function getStockAndPrice($products = array(),$items = array()){
		$xml = '<productIdList>';
		if (count($products) > 0){
			foreach ($products as $product){
				$xml .= "<item>{$product}</item>";
			}
		}
		$xml .= '</productIdList>';
		$options['productIdList'] = $xml;
		
		$xml = '<itemIdList>';
		if (count($items) > 0){
			foreach ($items as $item){
				$xml .= "<item>{$item}</item>";
			}
		}
		$xml .= '</itemIdList>';
		$options['itemIdList'] = $xml;
		return $this->clientConnect('individual','Product','getStockAndPrice',$options,array('productIdList','itemIdList'));
	}
	
	
	
	
	/**
	 * Product Service
	 * getProductStatuses Method
	 * 
	 * @param array $products
	 * @param array $items
	 * 
	 * @return Web Servis mesajini object olarak dondurur
	 */
	public function getProductStatuses($products = array(),$items = array()){
		$xml = '<productIdList>';
		if (count($products) > 0){
			foreach ($products as $product){
				$xml .= "<item>{$product}</item>";
			}
		}
		$xml .= '</productIdList>';
		$options['productIdList'] = $xml;
		
		$xml = '<itemIdList>';
		if (count($items) > 0){
			foreach ($items as $item){
				$xml .= "<item>{$item}</item>";
			}
		}
		$xml .= '</itemIdList>';
		$options['itemIdList'] = $xml;
		return $this->clientConnect('individual','Product','getProductStatuses',$options,array('productIdList','itemIdList'));
	}
	
	
	
	
	/**
	 * Urun ozellikleri bilgilerini almak icin erisilir.  
	 * Detayli Bilgi : http://dev.gittigidiyor.com/metotlar/getProductSpecs-ProductService-soap-individual
	 * 
	 * Product Service
	 * getProductSpecs Method
	 * 
	 * @param string $productId
	 * @param string $itemId
	 * 
	 * @return Web Servis mesajini object olarak dondurur
	 */
	public function getProductSpecs($productId = null, $itemId = null){
		return $this->clientConnect('individual','Product','getProductSpecs',get_defined_vars());
	}
	
	
	
	/**
	 * Product Service
	 * getProductsByIds Method
	 * 
	 * @param array $products
	 * @param array $items
	 * 
	 * @return Web Servis mesajini object olarak dondurur
	 */
	public function getProductsByIds($products = array(),$items = array()){
		$xml = '<productIdList>';
		if (count($products) > 0){
			foreach ($products as $product){
				$xml .= "<item>{$product}</item>";
			}
		}
		$xml .= '</productIdList>';
		$options['productIdList'] = $xml;
		
		$xml = '<itemIdList>';
		if (count($items) > 0){
			foreach ($items as $item){
				$xml .= "<item>{$item}</item>";
			}
		}
		$xml .= '</itemIdList>';
		$options['itemIdList'] = $xml;
		return $this->clientConnect('individual','Product','getProductsByIds',$options,array('productIdList','itemIdList'));
	}
	
	
	
	/**
	 * Urun aciklamasini almak icin erisilir.  
	 * Detayli Bilgi : http://dev.gittigidiyor.com/metotlar/getProductDescription-ProductService-soap-individual
	 * 
	 * Product Service
	 * getProductDescription Method
	 * 
	 * @param string $productId
	 * @param string $itemId
	 * 
	 * @return Web Servis mesajini object olarak dondurur
	 */
	public function getProductDescription($productId = null, $itemId = null){
		return $this->clientConnect('individual','Product','getProductDescription',get_defined_vars());
	}
	
	
	//End Product Service
	
	
	
	
	
	//Start Sale Service
	
	/**
	 * Satici satis kodunu girmek suretiyle mevcut satisin bilgilerini elde edebilir.
	 * 
	 * Detayli Bilgi : http://dev.gittigidiyor.com/metotlar/getSale-SaleService-soap-individual
	 * 
	 * Sale Service
	 * 
	 * Get Sale
	 * 
	 * @param integer	$saleCode
	 * 
	 * @return object Sale code info
	 */
	public function getSale($saleCode = null){
		return $this->clientConnect('individual','Sale','getSale',get_defined_vars());
	}	
	
	
	/**
	 * Satici konumundaki kullanici bu metod araciligi ile 
	 * GittiGidiyor Bana Özel alaninin Sattiklarim bolumunde sunulan bilgilerin tamamini elde edebilir, 
	 * bilgiler filtrelenebilir ve de siralanabilir. 
	 * 
	 * Detayli Bilgi : http://dev.gittigidiyor.com/metotlar/getSales-SaleService-soap-individual
	 * 
	 * Get Sales
	 * 
	 * @param integer	$saleCode
	 * @param integer	$rowCount
	 * @param string	$withData
	 * @param string	$byStatus
	 * @param string	$byUser
	 * @param string	$orderBy
	 * @param string	$orderType
	 * 
	 * @return object Sales info
	 */
	public function getSales($withData = false,
							$byStatus = 'R',
							$byUser = 'A',
							$orderBy = 'C',
							$orderType = 'A',
							$pageNumber = 1,
							$pageSize = 50){
		//$withData = $this->booleanConvert($withData);
		return $this->clientConnect('individual', 'Sale','getPagedSales', get_defined_vars());
	}
	
	
	//Start Messages Service
	
	/**
	 * Gelen kutusundaki mesaj bilgilerine erismek icin kullanilmasi gereken metoddur.
	 * 
	 * Detayli Bilgi : http://dev.gittigidiyor.com/metotlar/getInboxMessages-UserMessageService-soap-individual 
	 * 
	 * UserMessage Service
	 * Get Inbox Message Method
	 * 
	 * @param integer	$startOffset
	 * @param integer	$rowCount
	 * @param string	$unread
	 * 
	 * @return object Inbox Message List
	 */
	public function getInboxMessages($getConversationsRequest){
		//$unread = $this->booleanConvert($unread);
		return $this->clientConnect('individual', 'UserConversation', 'getConversations', get_defined_vars());
	}

	public function getMessages($getMessagesRequest){
		//$unread = $this->booleanConvert($unread);
		return $this->clientConnect('individual', 'UserConversation', 'getMessages', get_defined_vars());
	}

	public function putMessage($putMessageRequest){
		//$unread = $this->booleanConvert($unread);
		return $this->clientConnect('individual', 'UserConversation', 'putMessage', get_defined_vars());
	}

	
	/**
	 * Gonderilen kutusundaki mesaj bilgilerine erismek icin kullanilmasi gereken metoddur.
	 * 
	 * Detayli Bilgi : http://dev.gittigidiyor.com/metotlar/getSendedMessages-UserMessageService-soap-individual
	 * 
	 * UserMessage Service
	 * Get Outbox Message Method
	 * 
	 * @param integer	$startOffset
	 * @param integer	$rowCount
	 * 
	 * @return object Outbox Message List
	 */
	public function getSendedMessages($startOffset = 0 ,$rowCount = 5){
		return $this->clientConnect('individual','UserMessage','getSendedMessages', get_defined_vars());
	}
	
	
	/**
	 * Mesaj gonderme islemi icin kullanilmasi gereken metoddur.
	 * 
	 * Detayli Bilgi : http://dev.gittigidiyor.com/metotlar/sendNewMessage-UserMessageService-soap-individual
	 * 
	 * UserMessage Service
	 * Send New Message Method
	 * 
	 * @param string $to
	 * @param string $title
	 * @param string $messageContent
	 * @param string $sendCopy
	 * 
	 * @return Web Servis mesajini object olarak dondurur
	 */
	public function sendNewMessage($to = null,$title = null,$messageContent = null, $sendCopy = null){
		return $this->clientConnect('individual','UserMessage','sendNewMessage',get_defined_vars());
	}
	
	
	
	/**
	 * UserMessage Service
	 * readMessage Method
	 * 
	 * @param integer	$startOffset
	 * @param integer	$rowCount
	 * 
	 * @return object Outbox Message List
	 */
	public function readMessage($messageId){
		return $this->clientConnect('individual','UserMessage','readMessage',get_defined_vars());
	}
	
	
	
	/**
	 * UserMessage Service
	 * deleteIncomingMessages Method
	 * 
	 * 
	 * @return Web Servis mesajini object olarak dondurur
	 */
	public function deleteIncomingMessages($messageIds = array()){
		$xml = '<messageId>';
		if (count($messageIds) > 0){
			foreach ($messageIds as $messageId){
				$xml .= "<item>{$messageId}</item>";
			}
		}
		$xml .= '</messageId>';
		$options['messageId'] = $xml;
		
		return $this->clientConnect('individual','UserMessage','deleteIncomingMessages',$options,array('messageId'));
	}
	
	
	
	/**
	 * UserMessage Service
	 * deleteOutgoingMessages Method
	 * 
	 * 
	 * @return Web Servis mesajini object olarak dondurur
	 */
	public function deleteOutgoingMessages($messageIds = array()){
		$xml = '<messageId>';
		if (count($messageIds) > 0){
			foreach ($messageIds as $messageId){
				$xml .= "<item>{$messageId}</item>";
			}
		}
		$xml .= '</messageId>';
		$options['messageId'] = $xml;
		
		return $this->clientConnect('individual','UserMessage','deleteOutgoingMessages',$options,array('messageId'));
	}
	
	
	//End Message Service
	
	
	
	//Cargo Service
	
	/**
	 * Bu metot, satış kodunu girdikten sonra mevcut satışın kargo bilgisini girmek için kullanılır. 
	 * 
	 * Detayli Bilgi : http://dev.gittigidiyor.com/metotlar/getCargoInformation-SaleService-soap-individual
	 * 
	 * getCargoInformation Service
	 * Get Cargo Information
	 * 
	 * @param integer	$saleCode
	 * 
	 * @return object Cargo Information
	 */
	public function getCargoInformation($saleCode){
		return $this->clientConnect('individual','Cargo','getCargoInformation',get_defined_vars());
	}
	
	
	/**
	 * İlgili satış kodu girildikten sonra, satış bilgisine kargo bilgisi eklemek için kullanılır. Bu metot satıcılar tarafından kullanılır. 
	 * 
	 * Detayli Bilgi : http://dev.gittigidiyor.com/metotlar/sendCargoInformation-SaleService-soap-individual
	 * 
	 * sendCargoInformation Service
	 * Send Cargo Information
	 * 
	 * @param integer	$saleCode
	 * 
	 * @return object Cargo Information
	 */
	public function sendCargoInformation($saleCode,$cargoPostCode,$cargoCompany,$cargoBranch,$followUpUrl,$userType){
		return $this->clientConnect('individual','Cargo','sendCargoInformation',get_defined_vars());
	}
	
	
	//End Cargo Service
	
	
	
	//Store Service
	
	/**
	 * Kullanicinin dukkan ve dukkan kategori bilgilerine ulasmak icin kullanilir. 
	 * 
	 * Detayli Bilgi : http://dev.gittigidiyor.com/metotlar/getStore-StoreService-soap-individual
	 * 
	 * Store Service
	 * getStore  Information
	 * 
	 * @param integer	$saleCode
	 * 
	 * @return object Web Servis mesajini object olarak dondurur
	 */
	public function getStore(){
		return $this->clientConnect('individual','Store','getStore',get_defined_vars());
	}
	
	//End Store Service
	
	
	
	
	
	
	/**
	 * Client Boolean Value Converter
	 * 
	 * @param $data
	 * 
	 * @return string $data
	 */
	protected function booleanConvert($data){
		if ($data){
			$data = 'true';
		}else{
			$data = 'false';
		}
		return $data;
	}
	
	
	
	/**
	 * GG Client Curl Connect Service ON SOAP
	 * 
	 * @param string $serviceAccessType
	 * @param string $serviceType
	 * @param string $method
	 * @param array  $parameters
	 * 
	 */
	protected  function clientConnect($serviceAccessType, $serviceType, $method, $parameters, $xml = array()){
		
		$soapParams = array();
		if ($serviceType =='Product' || $serviceType =='Developer' || $serviceType == 'UserConversation'){
			$url = 'https://dev.gittigidiyor.com:8443/';
		}else{
			$url = 'http://dev.gittigidiyor.com:8080/';
		}
	   	$url .= 'listingapi/ws/';
	   	switch ($serviceAccessType) {
	   		case 'anonymous': 
	   			$url .= $serviceType.'Service';
	   			break;
	   		case 'individual':
	   			$url .= 'Individual'.$serviceType.'Service';
	   			$soapParams = array('apiKey' => $this->apiKey,
									'sign' => $this->sign,
									'time' => (float)$this->time);
	   			break;		
	   		case 'internal':
	   			$url .= 'Internal'.$serviceType.'Service';break;
	   			
	   		case 'community':
	   			$url .= 'Community'.$serviceType.'Service';break;
	   	}
	   	$url .= '?wsdl';
	   	
		foreach ($parameters as $key => $param){
			$soapParams[$key]=$param;

	   	}
	   	if (count($xml) > 0){
	   		foreach ($xml as $xmlRow){
	   			$value = $parameters[$xmlRow];
				$soapParams[$xmlRow] = new SoapVar($value, XSD_ANYXML);		
	   		}
	   	}

	   	if($serviceType != 'UserConversation'){
	   		
	   		$soapParams['lang'] = $this->lang;
	   	}
	   	
	   	/*
	   	echo '<pre>';
		print_r($soapParams);
		echo '</pre>';
		*/
		$soapClient = new SoapClient($url, array('login' => $this->auth_user, 'password' => $this->auth_pass, 'authentication' => SOAP_AUTHENTICATION_BASIC));
		$result = $soapClient->__soapCall($method, $soapParams);
		
		return $result;
		
	}	
	
}