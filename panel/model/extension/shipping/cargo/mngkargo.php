<?php
if (!interface_exists('CargoTemplate')) {

    require "cargoInterface.php";
}
class ModelExtensionShippingCargoYurticikargo extends Model implements CargoTemplate{

    protected static $_wsUserName, $_wsPassword, $_userLanguage, $_parameters, $_sclient;
    public $_debug = false;

    public $projectId;
    public $cargoPrefix;
    public $invCustId;
    public $ortam;
    public $wsURL       = 'http://webservices.yurticikargo.com:8080/KOPSWebServices/NgiShipmentInterfaceServices?wsdl';
    public $wsReportURL = 'http://webservices.yurticikargo.com:8080/KOPSWebServices/WsReportWithReferenceServices?wsdl';

    public function __construct($registry) {
      parent::__construct($registry);

      $this->load->model('extension/shipping/softomikargo');

      $config = $this->model_extension_shipping_softomikargo->getCargoDetail('yurticikargo');
      $data =   $this->model_extension_shipping_softomikargo->getCargoData('yurticikargo');

//print_r($data);exit;

        self::$_wsUserName  = $config['username'];
        self::$_wsPassword  = $config['password'];
        $this->projectId    = $data['projectId'];
        $this->invCustId    = $data['musteriKodu'];
        $this->cargoPrefix  = $data['prefix'];
        $this->ortam        = $data['ortam'];

        if($this->ortam == 'test'){
          $this->wsURL       = 'http://testwebservices.yurticikargo.com:9090/KOPSWebServices/NgiShipmentInterfaceServices?wsdl';
          $this->wsReportURL = 'http://testwebservices.yurticikargo.com:9090/KOPSWebServices/WsReportWithReferenceServices?wsdl';
        }

        self::$_userLanguage = "TR";

        self::$_parameters = [
      		'wsUserName'   => self::$_wsUserName,
      		'wsPassword'   => self::$_wsPassword,
      		'wsUserLanguage' => self::$_userLanguage,
      		];

    }

    public function getprojectid(){
        return $this->projectId;
    }
    public function getcargoPrefix(){
      return $this->cargoPrefix;
    }

    public function getinvCustId(){
      return $this->invCustId;
    }

    public function setUrl($url) {
        self::$_sclient = new \SoapClient($url);
    }

    public function cityId($zone){
      $c = $this->db->query("SELECT city_id FROM `" . DB_PREFIX . "city` WHERE city_name LIKE '%".$zone."%'")->row;
      if($c['city_id'] > 0)
        return $c['city_id'];

      return $zone;
    }

    public function createShipment($cargo_data) {

    //$this->load->model('customerpartner/yurtici');
    $referans = rand(111111111111,999999999999);
    $referans = $this->cargoPrefix.$referans;
    $sender   = $cargo_data['sender'];
    $receiver = $cargo_data['receiver'];
    $total = $cargo_data['totals'];
      if(is_array($sender) && is_array($receiver)) {
        $barcode = $referans;

        require_once(DIR_SYSTEM."library/Barcode.class.php");
        $barcode = new Barcode\Barcode();

        if(!isset($sender['address'])){
          exit(json_encode(array('status' => 'error', 'error'=> 'SATICI ADRESİ GEREKLİ! Lütfen "Profilim/Adreslerim" menüsünden bir adres tanımlaması yapınız.')));
        }

        $order_data = $cargo_data;

        $shipmentData =  array(
                'ngiDocumentKey'    => $referans,
                'cargoType'         => 2,
                'totalCargoCount'   => 1,
                'totalDesi'         => $total['desi'],
                'totalWeight'       => $total['weight'],
                'personGiver'       => $sender["firstname"] .' '.$sender["lastname"],
                'description'       => $sender['company'],
                'productCode'       =>  'STA',
                'docCargoDataArray'     => array(
                          'ngiCargoKey'   =>  $referans,
                          'cargoType'     =>  2,
                          'cargoDesi'     =>  $total['desi'],
                          'cargoWeight'   =>  $total['weight'],
                          'cargoCount'    =>  1,
                          'length'        =>  $total['length'],
                          'width'         =>  $total['width'],
                          'height'        =>  $total['height'],
                          'docCargoSpecialFieldDataArray' => array()
                ),
                'specialFieldDataArray' => array(
                          'specialFieldName' =>53,
                          'specialFieldValue'=>$referans

                ),
                'codData' => array(
                           'ttInvoiceAmount'=>'',
                           'dcSelectedCredit'=>''
                )
        );

        $complementaryProductDataArray= array();

        $XSenderCustAddress  = array(

                'senderCustName'  => $sender['company'] ? $sender['company']: $sender["firstname"] .' '.$sender["lastname"],
                'senderAddress'   => $sender['address']." ".$sender['city']." ".$sender['zone'].' '. $sender['country'],
                'cityId'          => $this->cityId($sender['zone']),
                'townName'        => $sender['city'],
                'senderPhone'     => $sender["telephone"]
        );

        $XConsigneeCustAddress= array(

                'consigneeCustName'        => $receiver['company'] ? $receiver['company']: $receiver["firstname"] .' '.$receiver["lastname"],
                'consigneeAddress'         => $receiver['address']." ".$receiver['city']." ".$receiver['zone'].' '. $receiver['country'],
                'cityId'                   => $this->cityId($receiver['zone']),
                'townName'                 => $receiver['city'],
                'consigneeMobilePhone'     => $receiver["telephone"]

        );

        $payerCustData = array(
                'invCustId'           =>  $this->getinvCustId()
        );

// KARGO GÖNDERİSİ OLUŞTURMA

		$data = array_merge(
              array("wsUserName" => self::$_parameters['wsUserName'],
                    "wsPassword" => self::$_parameters['wsPassword'],
                    "wsUserLanguage" => self::$_parameters['wsUserLanguage'],
              ),
              array("shipmentData" => $shipmentData),
              array("complementaryProductDataArray" => $complementaryProductDataArray),
              array("XSenderCustAddress" => $XSenderCustAddress),
              array("XConsigneeCustAddress" => $XConsigneeCustAddress),
              array("payerCustData" => $payerCustData)
            );


        $this->setUrl($this->wsURL);

        $json = json_decode(json_encode(self::$_sclient->createNgiShipmentWithAddress($data)), true);

        if(isset($json['XShipmentDataResponse']['projectId']) && !empty($json['XShipmentDataResponse']['projectId']) && ($json['XShipmentDataResponse']['outFlag'] == 0) ){


          return $this->returnFormat($cargo_data, $order_data, $referans);


        } else {

          return json_encode(
                    array(
                      'status'      => 'error',
                      'Message'     => 'Kargo oluşturulamadı.'.$json['XShipmentDataResponse']['outResult']
                    )
                  );

        }

    }
  }

/*
  Referansı oluşturulmuş gönderinin dokümanını geri çevirir.

*/

  public function getShipment($order_id, $data){

    $cargo_data = json_decode($data['bilgi'], true);

    return $this->returnFormat($cargo_data, $orderdata = array(), $data);

  }


  private function returnFormat($cargo_data, $orderdata, $referans){

        if(is_array($referans)){

          $tarih    = $referans['date_added'];
          $referans = $referans['referans'];

        }

        $this->load->model('setting/setting');
        $config = $this->model_setting_setting->getSetting('config');

        require_once(DIR_SYSTEM."library/Barcode.class.php");
        $barcode = new Barcode\Barcode();

        $base64       = $barcode->generate128($referans)->base64(70);
        $projectid64  = $barcode->generate128($this->projectId)->base64(70);

              $html = '';
              $html .= '<div><img src="image/cargo/yurticikargo.jpg" style="max-height:100px; max-width:200px;">&#09;<img src="image/'.$config['config_logo'].'" style="max-height:100px; max-width:200px;"></div>';
              $html .= '<div><b>'.$_SERVER['SERVER_NAME'].'</b> | Müşteri Numarası : <b>'.$this->invCustId.'</b></div>';
              $html .= '<div align="center"><b>YENİ ÖDER - Merkezi Faturalandırma - Platform Ödemeli</b></div>';
              $html .= '<hr>';
              $html .= '<div><img style="height:30px;width: 226px;" src="data:image/png;base64,' . $base64 . '"></div>';
              $html .= '<div>Referans: <b>' . $referans . '</b></div>';
              $html .= '<hr>';
              $html .= '<div><img style="height:30px;width: 226px;" src="data:image/png;base64,' . $projectid64 . '"></div>';
              $html .= '<div>Project ID: <b>'.$this->projectId.'</b></div>';
              $html .= '<hr>';
              $html .= '<pre align="left"><h4>ALICI BİLGİLERİ</h4>';
              $html .= '<br>Adı Soyadı:&#09;<strong>'.$cargo_data['receiver']["firstname"].' '.$cargo_data['receiver']["lastname"].'</strong>';
              $html .= '<br>Telefon:&#09;<strong>'.$cargo_data['receiver']["telephone"].'</strong>';
              $html .= '<br>Adres:&#09;&#09;<strong>'.$cargo_data['receiver']["address"].'</strong>';
              $html .= '<br>&#09;&#09;<strong>'.$cargo_data['receiver']["postcode"].' '.$cargo_data['receiver']["city"].' /  '. $cargo_data['receiver']["zone"] .'</strong>';
              $html .= '<br>&#09;&#09;<strong>'.$cargo_data['receiver']["country"].'</strong>';
              $html .= '</pre>';
              $html .= '<hr>';
              $html .= '<pre align="left">';
              $html .= '<h4>GÖNDERİCİ BİLGİLERİ</h4>';
              $html .= '<strong>'.$cargo_data['sender']["company"].'</strong><br>';
              $html .= '<br>Adı Soyadı:&#09;<strong>'.$cargo_data['sender']["firstname"].' '.$cargo_data['sender']["lastname"].'</strong>';
              $html .= '<br>Telefon:&#09;<strong>'.$cargo_data['sender']["telephone"].'</strong>';
              $html .= '<br>Adres:&#09;&#09;<strong>'.$cargo_data['sender']["address"].'</strong>';
              $html .= '<br>&#09;&#09;<strong>'.$cargo_data['sender']["postcode"].' '.$cargo_data['sender']["city"].' /  '. $cargo_data['sender']["zone"] .'</strong>';
              $html .= '<br>&#09;&#09;<strong>'.$cargo_data['sender']["country"].'</strong>';
              $html .= '</pre>';

              return json_encode(
                      array(
                      'status'         => 'success',
                      'referans'       => $referans,
                      'barcode'        => '<img style="height:30px;width: 226px;" src="data:image/png;base64,' . $base64 . '">',
                      'projectid'      => $this->projectId,
                      'projectcode'    => '<img style="height:30px;width: 226px;" src="data:image/png;base64,' . $projectid64 . '">',
                      'order_details'  => $order_data,
                      'document'       => $html
                    ) , JSON_UNESCAPED_UNICODE
                  );

  }

//KARGO İPTALİ

    public function cancelShipment($referans) {

		$data = array_merge(
          array("wsUserName" => self::$_parameters['wsUserName'],
                "wsPassword" => self::$_parameters['wsPassword'],
                "userLanguage" => self::$_parameters['wsUserLanguage'],
                "ngiCargoKey" => $referans,
                "ngiDocumentKey" =>$referans,
                "cancellationDescription" => "SİLME İŞLEMİ MÜŞTERİ İSTEĞİ İLE"
          )

        );


        $this->setUrl($this->wsURL);
        return self::$_sclient->cancelNgiShipment($data);
    }

    public function queryShipment($cargokey) {

		$data = array_merge(
          array("wsUserName"        => self::$_parameters['wsUserName'],
                "wsPassword"        => self::$_parameters['wsPassword'],
                "wsUserLanguage"        => self::$_parameters['wsUserLanguage'],
                "shipmentParamsData" => array(
                          'ngiCargoKey' =>$cargokey
                  )
          )

        );


        $this->setUrl($this->wsURL);
        return self::$_sclient->selectShipment($data);
    }

// KARGO HAREKETLERİ VE DETAYLAR
    public function queryShipmentDetail($keys) {

      $data = array(
        'userName'              =>self::$_parameters['wsUserName'],
        'password'              =>self::$_parameters['wsPassword'],
        'language'              =>self::$_parameters['wsUserLanguage'],
        //'docIdArray'            => $cargo_id,
        'custParamsVO'          => array('invCustIdArray' => $this->invCustId),
        'fieldName'             => 53,
        'fieldValueArray'       => $keys, //'ELR'.$cargo_id,
        'withCargoLifecycle'    => 0
      );

        $this->setUrl($this->wsReportURL);
        return self::$_sclient->listInvDocumentInterfaceByReference($data);
    }


    public function shipmentControl($referans){

      $data = array(
        'userName'              =>self::$_parameters['wsUserName'],
        'password'              =>self::$_parameters['wsPassword'],
        'language'              =>self::$_parameters['wsUserLanguage'],
        //'docIdArray'            => $cargo_id,
        'custParamsVO'          => array('invCustIdArray' => $this->invCustId),
        'fieldName'             => 53,
        'fieldValueArray'       => $referans,
        'withCargoLifecycle'    => 0
      );


      $this->setUrl($this->wsReportURL);
      $request = self::$_sclient->listInvDocumentInterfaceByReference($data);

      $cevap = json_decode(json_encode($request),true);

      if(isset($cevap["ShippingDataResponseVO"]["shippingDataDetailVOArray"]["docId"]) && $cevap["ShippingDataResponseVO"]["shippingDataDetailVOArray"]["docId"]){

        $return['barcode']          = $cevap["ShippingDataResponseVO"]["shippingDataDetailVOArray"]["docId"];
        $return['takip_url']        = $cevap["ShippingDataResponseVO"]["shippingDataDetailVOArray"]["trackingUrl"];
        $return['gonderi_ucreti']   = $cevap["ShippingDataResponseVO"]["shippingDataDetailVOArray"]["totalAmount"];
        $return['gonderi_agirlik']  = $cevap["ShippingDataResponseVO"]["shippingDataDetailVOArray"]["totalDesi"].' D/'.$cevap["ShippingDataResponseVO"]["shippingDataDetailVOArray"]["totalKg"].' KG';
        $return['teslim_alan']      = $cevap["ShippingDataResponseVO"]["shippingDataDetailVOArray"]["receiverInfo"];
        $return['son_islem']        = $cevap["ShippingDataResponseVO"]["shippingDataDetailVOArray"]["cargoEventExplanation"];
        $return['islem_tarihi']     = date_format(date_create($cevap["ShippingDataResponseVO"]["shippingDataDetailVOArray"]["cargoEventDate"]), 'd/m/Y')." ".date_format(date_create($cevap["ShippingDataResponseVO"]["shippingDataDetailVOArray"]["cargoEventTime"]), 'H:i:s');

        $return['date_added']       = $return['islem_tarihi'];

        if(stristr($return['son_islem'], 'Teslim edildi')){
          $return['process'] = 'COMPLATED';
        } else {
          $return['process'] = 'PROCESS';
        }



        return $return;

      } else {

         return false;

      }

    }


    public function __destruct() {
        if ($this->_debug) {
            print_r(self::$_parameters);
        }
    }

}
?>
