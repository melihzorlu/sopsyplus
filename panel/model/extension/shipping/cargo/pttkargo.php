<?php
if (!interface_exists('CargoTemplate')) {

    require "cargoInterface.php";
}
class ModelExtensionShippingCargoPttkargo extends Model implements CargoTemplate{

  private $ortam;
  private $musteriId;
  private $musteriSifre;
  private $iadeMusteriId;
  private $iadeMusteriSifre;
  private $musteriPrefix;
  private $wsURL;
  private $wsReportURL;
  private $pttCekNo;


  public function __construct($registry) {

    parent::__construct($registry);

    $this->load->model('extension/shipping/softomikargo');

    $config = $this->model_extension_shipping_softomikargo->getCargoDetail('pttkargo');
    $data =   $this->model_extension_shipping_softomikargo->getCargoData('pttkargo');

    $this->ortam            = ($data['ortam'] == 'test' ? 'Test' : '');
    $this->musteriId        = $config['username'];
    $this->musteriSifre     = $config['password'];
    $this->iadeMusteriId    = $data['iadeMusteriId'];
    $this->iadeMusteriSifre = $data['iadeMusteriSifre'];
    $this->musteriPrefix    = $data['prefix'];
    $this->pttCekNo         = $data['ptt_cek_no'];


    $this->wsURL            = 'https://pttws.ptt.gov.tr/PttVeriYukleme'.$this->ortam.'/services/Sorgu';
    $this->wsReportURL      = 'https://pttws.ptt.gov.tr/GonderiTakipV2'.$this->ortam.'/services/Sorgu';

  }


  public function createShipment($cargo_data){

    $referans = rand(111111111111,999999999999);
    $referans = $this->cargoPrefix.$referans;
    $sender   = $cargo_data['sender'];
    $receiver = $cargo_data['receiver'];
    $total = $cargo_data['totals'];
    //print_r($cargo_data);exit;
    // @TODO: BURAYA BAK
    if ($type == 'Gönderici Ödemeli') {
      $paymentMethod = 'N';
    } elseif ($type == 'Alıcı Ödemeli') {
      $paymentMethod = 'UA';
    } else {
      $paymentMethod = 'N';
    }
    if ($type == 'Kapıda Ödeme' || @$sql['payment_code'] == 'cod') {
      $odeme_sart_ucreti = number_format((float)$total['price'], 2, '.', '');
      $ekhizmet = 'OS';
      $cekNo = $this->pttCekNo;
    } else {
      $odeme_sart_ucreti = 0;
      $ekhizmet = '';
      $cekNo = '';
    }

    $soap = array(
      'aAdres'            => $receiver['address']." ".$receiver['city']." ".$receiver['zone'].' '. $receiver['country'],
      'agirlik'           => $total['weight'],
      'aliciAdi'          => $receiver['company'] ? $receiver['company']: $receiver["firstname"] .' '.$receiver["lastname"],
      'aliciIlAdi'        => $receiver['zone'],
      'aliciIlceAdi'      => $receiver['city'],
      'aliciSms'          => $receiver["telephone"],
      'aliciTel'          => $receiver["telephone"],
      'boy'               => $total['height'],
      'deger_ucreti'      => $total['price'],
      'desi'              => $total['desi'],
      'ekhizmet'          => $ekhizmet,
      'en'                => $total['width'],
      'musteriReferansNo' => $referans,
      'odemesekli'        => $paymentMethod,
      'odeme_sart_ucreti' => $odeme_sart_ucreti,
      'rezerve1'          => $cekNo,
      'posta_ceki_no'     => $cekNo,
      'yukseklik'         => $total['length'],
      'gondericibilgi'    => array(
        'gonderici_adi'       => $sender['company'] ? $sender['company'] : $sender["firstname"] .' '.$sender["lastname"],
        'gonderici_adresi'    => $sender['address']." ".$sender['city']." ".$sender['zone'].' '. $sender['country'],
        'gonderici_il_ad'     => $sender['zone'],
        'gonderici_ilce_ad'   => $sender['city'],
        'gonderici_telefonu'  => $sender["telephone"],
      ),
      'iadeAAdres'        => $sender['address']." ".$sender['city']." ".$sender['zone'].' '. $sender['country'],
      'iadeAliciAdi'      => $sender['company'] ? $sender['company']: $sender["firstname"] .' '.$sender["lastname"],
      'iadeAliciIlAdi'    => $sender['zone'],
      'iadeAliciIlceAdi'  => $sender['city'],

    );


    $response = $this->callAPI($this->wsURL);
    $response->kabulEkle2([
      'input'       => [
        'dongu'     => $soap,
        'dosyaAdi'  => $referans,
        'index.php?route=extension/shipping/shipping&cargokey=pttkargo&order_id' => '?',
        'musteriId' => $this->musteriId,
        'sifre'     => $this->musteriSifre
      ]
    ]);

    $response = $response->__last_response;
    $response = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $response);
    $xml = new SimpleXMLElement($response);
    $response = $xml->soapenvBody->nskabulEkle2Response->nsreturn;

    //print_r($response);exit;

    if (json_decode($response->ax21hataKodu,true) == 1) {

      return $this->returnFormat($cargo_data, $order_data = array(), $referans);

    } else {

      return json_encode(
        array(
          'status'      => 'error',
          'Message'     => 'Kargo oluşturulamadı. HATA: '.json_decode($response->ax21hataKodu,true)
        )
      );

    }

  }

  public function cancelShipment($referans){

    $response = $this->callAPI($this->wsURL);
    $response->referansVeriSil([
      'inpRefDelete'       => [
        'dosyaAdi'  => $referans,
        'musteriId' => $this->musteriId,
        'referansNo' => $referans,
        'sifre'     => $this->musteriSifre
      ]
    ]);

    $response = $response->__last_response;
    $response = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $response);
    $xml = new SimpleXMLElement($response);
    $response = $xml->soapenvBody->nsreferansVeriSilResponse->nsreturn;

    if (json_decode($response->ax21hataKodu,true) == 1) {
      return true;
    }
    return false;

  }

  public function shipmentControl($referans){

    $response = $this->callAPI($this->wsReportURL);
    $request = $response->gonderiSorgu_referansNo([
      'input'       => [
        'kullanici' => $this->musteriId,
        'referansNo' => $referans,
        'sifre'     => $this->musteriSifre
      ]
    ]);

    $cevap = json_decode(json_encode($request),true);

    if(isset($cevap["return"]["BARNO"]) && $cevap["return"]["BARNO"]){

      $return['barcode']          = $cevap["return"]["BARNO"];
      $return['takip_url']        = $cevap["return"]["sonucAciklama"];
      $return['gonderi_ucreti']   = $cevap["return"]["GONUCR"];
      $return['gonderi_agirlik']  = $cevap["return"]["GR"];
      $return['teslim_alan']      = $cevap["return"]["TESALAN"];
      $toplamislem                = count($cevap["return"]["dongu"])-1;
      $return['son_islem']        = $cevap["return"]["dongu"][$toplamislem]["ISLEM"];
      $return['islem_tarihi']     = $cevap["return"]["dongu"][$toplamislem]["ITARIH"]." ".$cevap["return"]["dongu"][$toplamislem]["ISAAT"];
      $ilkislem                   = $cevap["return"]["dongu"][0]["ISLEM"];

      $date                       = date_create($return['islem_tarihi']);
      $return['date_added']       = date_format($date, 'Y-m-d H:i:s');

      if(stristr($return['son_islem'], 'Teslim edildi')){
        $return['process'] = 'COMPLATED';
      } else {
        $return['process'] = 'PROCESS';
      }


      return $return;

    } else {

       return false;

    }

  }


  public function getShipment($order_id, $data){

    $cargo_data = json_decode($data['bilgi'], true);

    return $this->returnFormat($cargo_data, $orderdata = array(), $data);

  }

  private function returnRmaFormat($cargo_data, $orderdata, $referans){

    if(is_array($referans)){

      $tarih    = $referans['date_added'];
      $referans = $referans['referans'];

    }

    $this->load->model('setting/setting');
    $config = $this->model_setting_setting->getSetting('config');

    /*require_once(DIR_SYSTEM."library/Barcode.class.php");
    $barcode = new Barcode\Barcode();

    $base64       = $barcode->generate128($referans)->base64(70);
    //$projectid64  = $barcode->generate128($this->projectId)->base64(70);
*/
  $html = $_SERVER['SERVER_NAME'].'<br>Kargo Şirketi: <b>PTT Kargo</b><br>Müşteri Numarası: <b>'.$this->musteriId.'</b><br>Referans Numarası: <b>'.$referans.'</b>';

    return json_encode(
      array(
        'status'         => 'success',
        'referans'       => $referans,
        //'barcode'        => '<img style="height:30px;width: 226px;" src="data:image/png;base64,' . $base64 . '">',
        'projectid'      => $this->projectId,
        'order_details'  => $order_data,
        'document'       => $html
      ) , JSON_UNESCAPED_UNICODE
    );

  }

  private function returnFormat($cargo_data, $orderdata, $referans){

    if($cargo_data['rma_id']){

            return $this->returnRmaFormat($cargo_data, $order_data, $referans);

    }

    if(is_array($referans)){

      $tarih    = $referans['date_added'];
      $referans = $referans['referans'];

    }

    $this->load->model('setting/setting');
    $config = $this->model_setting_setting->getSetting('config');

    require_once(DIR_SYSTEM."library/Barcode.class.php");
    $barcode = new Barcode\Barcode();

    $base64       = $barcode->generate128($referans)->base64(70);
    //$projectid64  = $barcode->generate128($this->projectId)->base64(70);

    $html = '';
    //$html .= '<div><img src="image/cargo/pttkargo.jpg" style="max-height:100px; max-width:200px;">&#09;<img src="image/'.$config['config_logo'].'" style="max-height:100px; max-width:200px;"></div>';
    $html .= '<div><img src="image/cargo/pttkargo.jpg" style="max-height:100px; max-width:200px;"></div>';
    $html .= '<div><b>Pazarmo.com</b> | Müşteri Numarası : <b>'.$this->musteriId.'</b></div>';
    $html .= '<div align="center"><b>AYLIK MAHSUP - Platform Ödemeli</b></div>';
    $html .= '<hr>';
    $html .= '<div><img style="height:30px;width: 226px;" src="data:image/png;base64,' . $base64 . '"></div>';
    $html .= '<div>Referans: <b>' . $referans . '</b></div>';
    $html .= '<hr>';
    $html .= '<pre align="left"><h4>ALICI BİLGİLERİ</h4>';
    $html .= '<br>Adı Soyadı:&#09;<strong>'.$cargo_data['receiver']["firstname"].' '.$cargo_data['receiver']["lastname"].'</strong>';
    $html .= '<br>Telefon:&#09;<strong>'.$cargo_data['receiver']["telephone"].'</strong>';
    $html .= '<br>Adres:&#09;&#09;<strong>'.$cargo_data['receiver']["address"].'</strong>';
    $html .= '<br>&#09;&#09;<strong>'.$cargo_data['receiver']["postcode"].' '.$cargo_data['receiver']["city"].' /  '. $cargo_data['receiver']["zone"] .'</strong>';
    $html .= '<br>&#09;&#09;<strong>'.$cargo_data['receiver']["country"].'</strong>';
    $html .= '</pre>';
    $html .= '<hr>';
    $html .= '<pre align="left">';
    $html .= '<h4>GÖNDERİCİ BİLGİLERİ</h4>';
    $html .= '<strong>'.$cargo_data['sender']["company"].'</strong><br>';
    $html .= '<br>Adı Soyadı:&#09;<strong>'.$cargo_data['sender']["firstname"].' '.$cargo_data['sender']["lastname"].'</strong>';
    $html .= '<br>Telefon:&#09;<strong>'.$cargo_data['sender']["telephone"].'</strong>';
    $html .= '<br>Adres:&#09;&#09;<strong>'.$cargo_data['sender']["address"].'</strong>';
    $html .= '<br>&#09;&#09;<strong>'.$cargo_data['sender']["postcode"].' '.$cargo_data['sender']["city"].' /  '. $cargo_data['sender']["zone"] .'</strong>';
    $html .= '<br>&#09;&#09;<strong>'.$cargo_data['sender']["country"].'</strong>';
    $html .= '</pre>';

    return json_encode(
      array(
        'status'         => 'success',
        'referans'       => $referans,
        'barcode'        => '<img style="height:30px;width: 226px;" src="data:image/png;base64,' . $base64 . '">',
        'projectid'      => $this->projectId,
        'order_details'  => $order_data,
        'document'       => $html
      ) , JSON_UNESCAPED_UNICODE
    );

  }



  public function callAPI($url)
  {
    $options = array(
      'uri'=>'http://schemas.xmlsoap.org/wsdl/soap/',
      'style'=>SOAP_RPC,
      'use'=>SOAP_ENCODED,
      'soap_version'=>SOAP_1_1,
      'cache_wsdl'=>WSDL_CACHE_NONE,
      'connection_timeout'=>15,
      'trace'=>true,
      'encoding'=>'UTF-8',
      'exceptions'=>true,

    );
    $soap = new SoapClient($url.'?wsdl', $options);
    return $soap;

  }

}
