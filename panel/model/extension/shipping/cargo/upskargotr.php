<?php
if (!interface_exists('CargoTemplate')) {

    require "cargoInterface.php";
}
class ModelExtensionShippingCargoUpsKargoTr extends Model implements CargoTemplate{

	private $error = array();
  private $customerCode;
  private $customerName;
  private $customerPass;
	private $wsURL;
  //private $customerEmail= "info@tasarimcafest.com";

	public function __construct($registry) {

    parent::__construct($registry);

    $this->load->model('extension/shipping/softomikargo');

    $config = $this->model_extension_shipping_softomikargo->getCargoDetail('upskargotr');
    $data =   $this->model_extension_shipping_softomikargo->getCargoData('upskargotr');

    $this->ortam            = ($data['ortam'] == 'test' ? 'Test' : '');
    $this->customerCode     = $data['musteriKodu'];
    $this->customerName     = $config['username'];
    $this->customerPass    	= $config['password'];

    $this->wsURL            = 'http://ws.ups.com.tr/wsCreateShipment/wsCreateShipment.asmx?WSDL';


  }
	public function getcity($name){

		$ret = $this->db->query("SELECT * FROM ups_ilceler WHERE name LIKE '%$name%'")->row;

		return $ret;
	}

	public function createShipment($cargo_data){
			$sender   = $cargo_data['sender'];
	    $receiver = $cargo_data['receiver'];
	    $total = $cargo_data['totals'];
			$order_id = $cargo_data['order']['id'];

			$scity = $this->getcity($sender['city']);
			$sender['ilkodu'] = $scity['parentID'];
			$sender['ilcekodu'] = $scity['code'];

			$rcity = $this->getcity($receiver['city']);
			$receiver['ilkodu'] = $rcity['parentID'];
			$receiver['ilcekodu'] = $rcity['code'];

			$client = new SoapClient($this->wsURL);
			$params = array();
			$paramsGonderi = array();
			$params["CustomerNumber"] = $this->customerCode;
			$params["UserName"]       = $this->customerName;
			$params["Password"]       = $this->customerPass;

			$session = $client->Login_Type1($params)->Login_Type1Result->SessionID;

      if(!$session){
        return json_encode(
                  array(
                    'status'      => 'error',
                    'Message'     => 'Kargo Servisi Hatası: Giriş Yapılamadı.'
                  )
                );
      }
				$array["SessionID"] = $session;
				$array["ShipmentInfo"]["ShipperAccountNumber"] = $this->customerCode;
				$array["ShipmentInfo"]["ShipperName"] = $sender['company'] ? $sender['company']: $sender["firstname"] .' '.$sender["lastname"];
				$array["ShipmentInfo"]["ShipperContactName"] = $sender["firstname"] .' '.$sender["lastname"];
				$array["ShipmentInfo"]["ShipperAddress"] = $sender['address']." ".$sender['city']." ".$sender['zone'].' '. $sender['country'];
				$array["ShipmentInfo"]["ShipperCityCode"] = $sender['ilkodu'];
				$array["ShipmentInfo"]["ShipperAreaCode"] = $sender['ilcekodu'];
				$array["ShipmentInfo"]["ShipperPhoneNumber"] = $sender["telephone"];
				$array["ShipmentInfo"]["ShipperEMail"] = $sender["email"];
				$array["ShipmentInfo"]["ShipperExpenseCode"] = "";

				$array["ShipmentInfo"]["ConsigneeAccountNumber"] = "";
				$array["ShipmentInfo"]["ConsigneeName"] =   $receiver['company'] ? $receiver['company']: $receiver["firstname"] .' '.$receiver["lastname"];
				$array["ShipmentInfo"]["ConsigneeContactName"] = $receiver["firstname"] .' '.$receiver["lastname"];
				$array["ShipmentInfo"]["ConsigneeAddress"] = $receiver['address']." ".$receiver['city']." ".$receiver['zone'].' '. $receiver['country'];
				$array["ShipmentInfo"]["ConsigneeCityCode"] = $receiver['ilkodu'];
				$array["ShipmentInfo"]["ConsigneeAreaCode"] = $receiver['ilcekodu'];
				$array["ShipmentInfo"]["ConsigneePhoneNumber"] = $receiver['telephone'];
				$array["ShipmentInfo"]["ConsigneeEMail"] = $receiver['email'];
				$array["ShipmentInfo"]["ConsigneeExpenseCode"] = "";
				$array["ShipmentInfo"]["ServiceLevel"] = 3;
				$array["ShipmentInfo"]["PaymentType"] = 2;
				$array["ShipmentInfo"]["PackageType"] = "K";
				$array["ShipmentInfo"]["NumberOfPackages"] = 1;
				$array["ShipmentInfo"]["CustomerReferance"] = "";
				$array["ShipmentInfo"]["CustomerInvoiceNumber"] = $order_id.$this->customer->getID();
				$array["ShipmentInfo"]["DescriptionOfGoods"] = "ETICARET";// What's KİTAP?
				$array["ShipmentInfo"]["DeliveryNotificationEmail"] = $this->customerEmail;

				$array["ReturnLabelLink"] = "1";//"barkod".$order_id.$this->customer->getID();
				$array["ReturnLabelImage"] = "1";//"barkod".$order_id.$this->customer->getID();

				$gonderi = $client->CreateShipment_Type1($array);

			if($gonderi->CreateShipment_Type1Result->ShipmentNo){
						$order_data['barcode'] = $gonderi->CreateShipment_Type1Result->ShipmentNo;
						$order_data['labelLink'] = $gonderi->CreateShipment_Type1Result->LinkForLabelPrinting;
						$order_data['barcodePng'] = $gonderi->CreateShipment_Type1Result->BarkodArrayPng->string;

						return $this->returnFormat($cargo_data, $order_data, $order_data['barcode']);
			}
			else
			{

				          return json_encode(
				                    array(
				                      'status'      => 'error',
				                      'Message'     => 'Kargo oluşturulamadı.'
				                    )
				                  );

			}

	}


	  private function returnFormat($cargo_data, $order_data, $referans){


			        if(is_array($referans)){

			          $tarih    = $referans['date_added'];
			          $referans = $referans['referans'];

			        }

								if(isset($cargo_data['document_detail'])){
									$barcodePng = $cargo_data['document_detail']['content'];
									$labelLink 	= $cargo_data['document_detail']['link'];
								} else {
									$barcodePng = $order_data['barcodePng'];
									$labelLink 	= $order_data['labelLink'];
								}

	              $html = '';
								$html .= '<img src="data:image/png;base64,'.$barcodePng.'">';


	              return json_encode(
	                      array(
	                      'status'         => 'success',
	                      'referans'       => $referans,
	                      'document_detail' => array(
													'type' 	=> 'PNG',
													'link'	=> $labelLink,
													'content' => $barcodePng
												),
	                      'order_details'  => $order_data,
	                      'document'       => $html
	                    ) , JSON_UNESCAPED_UNICODE
	                  );

	  }






	public function cancelShipment($referans){

		$client = new SoapClient($this->wsURL);
		$params = array();
		$paramsGonderi = array();
		$params["CustomerNumber"] = $this->customerCode;
		$params["UserName"]       = $this->customerName;
		$params["Password"]       = $this->customerPass;

		$session = $client->Login_Type1($params)->Login_Type1Result->SessionID;

			$array["sessionId"] = $session;
			$array["customerCode"] = $this->customerCode;
			$array["waybillNumber"] = $referans;

			$gonderi = $client->Cancel_Shipment_V1($array);

			if($gonderi->Cancel_Shipment_V1Result->ErrorCode == 0){
				return 'OK';
			} elseif($gonderi->Cancel_Shipment_V1Result->ErrorCode == 4){
				return 'Gönderi Daha Önce İptal Edilmiş';
			} else {
				return $gonderi->Cancel_Shipment_V1Result->ErrorDefinition;
			}

	}

	public function getShipment($order_id, $data){

    $cargo_data = json_decode($data['bilgi'], true);

    return $this->returnFormat($cargo_data, $orderdata = array(), $data);

  }

/*
	public function GetUpsEntegrasyon($order_id, $product_id, $customer_id) {
		$this->load->model('account/customerpartner');

		$order_info = $this->model_account_customerpartner->getOrder($order_id);
		$customer_info = $this->getCustomerAddress($customer_id);
		$cust_info = $this->getCustomer($customer_info['customer_id']);

		if ($order_info) {

			if ($order_info['invoice_no']) {
		  		$invoice_no = $order_info['invoice_prefix'] . $order_info['invoice_no'];
	  		} else {
		  		$invoice_no = '';
			}

	  		$email = $order_info['email'];
	  		$telephone = $order_info['telephone'];

	  		if(isset($order_info['shipping_firstname']) && $order_info['shipping_firstname']){
			   $firstname = $order_info['shipping_firstname'];
	  		}else{
	      		$firstname = $order_info['firstname'];
	  		}

	  		if(isset($order_info['shipping_firstname']) && $order_info['shipping_firstname']){
			    $lastname  = $order_info['shipping_lastname'];
	  		}else{
	      		$lastname  = $order_info['lastname'];
	  		}

	  		$company   = $order_info['shipping_company'];
		  	$address_1 = $order_info['shipping_address_1'];
		  	$address_2 = $order_info['shipping_address_2'];
		  	$city      = trim($order_info['shipping_city']);
		  	$postcode  = $order_info['shipping_postcode'];
		  	$zone      = $order_info['shipping_zone'];
		  	$zone_code = $order_info['shipping_zone_code'];
		  	$country   = $order_info['shipping_country'];

		  	if(isset($company) && $company){
	     		$ConsigneeName = $company;
	  		}else{
	     		$ConsigneeName = $firstname." ".$lastname;
	  		}

	  		$areacode = $this->db->query("SELECT * FROM ilceler where name LIKE '%$city%'")->row;

	  		if(isset($areacode["code"]) && $areacode["code"]) {
	  			$ilcekodu = $areacode["code"];
	    		$ilkodu = $areacode["parentID"];

	    		if(!empty($customer_info['company']))
		        {
		           $ShipperName = $customer_info['company'];
		        }
		        else
		        {
		           $ShipperName = $customer_info['firstname']." ".$customer_info['lastname'];
		        }

		        $ShipperAddress = $customer_info['address_1'];
		        $customer_city  = trim($customer_info['city']);
		        $konum = strpos($customer_city, '/');
		        if(isset($konum) && $konum) {
		          	$customer_city  = explode('/', $customer_info['city']);
		          	$customer_city  = $customer_city[0];
		        }
		        else
		        {
		          	$customer_city  = trim($customer_info['city']);
		        }

		        $cus_areacode   = $this->db->query("SELECT * FROM ilceler where name LIKE '%$customer_city%'")->row;

		        if(isset($cus_areacode["code"]) && $cus_areacode["code"]){
		            $c_ilcekodu = $cus_areacode["code"];
		            $c_ilkodu   = $cus_areacode["parentID"];
		        }

		        $customer_telephone = $cust_info['telephone'];

		        $sql = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product where order_id='".$order_id."' AND product_id='".$product_id."'")->row;

		        if($sql['tracking'] == '') {
		        	$client = new SoapClient("HTTP://ws.ups.com.tr/wsCreateShipment/wsCreateShipment.asmx?WSDL");
	      			$params = array();
	      			$paramsGonderi = array();
	      			$params["CustomerNumber"] = $this->customerCode;
	      			$params["UserName"]       = $this->customerName;
	      			$params["Password"]       = $this->customerPass;

	      			$session = $client->Login_Type1($params)->Login_Type1Result->SessionID;

	      			$array["SessionID"] = $session;
		          	$array["ShipmentInfo"]["ShipperAccountNumber"] = $this->customerCode;
		          	$array["ShipmentInfo"]["ShipperName"] = $ShipperName;
		          	$array["ShipmentInfo"]["ShipperContactName"] = "";
		          	$array["ShipmentInfo"]["ShipperAddress"] = $ShipperAddress;
		          	$array["ShipmentInfo"]["ShipperCityCode"] = $c_ilkodu;
		          	$array["ShipmentInfo"]["ShipperAreaCode"] = $c_ilcekodu;
		          	$array["ShipmentInfo"]["ShipperPhoneNumber"] = $customer_telephone;
		          	$array["ShipmentInfo"]["ShipperEMail"] = $this->customerEmail;
		          	$array["ShipmentInfo"]["ShipperExpenseCode"] = "";

		          	$array["ShipmentInfo"]["ConsigneeAccountNumber"] = "";
		          	$array["ShipmentInfo"]["ConsigneeName"] =  $ConsigneeName;
		          	$array["ShipmentInfo"]["ConsigneeContactName"] = $firstname." ".$lastname;
		          	$array["ShipmentInfo"]["ConsigneeAddress"] = $address_1;
		          	$array["ShipmentInfo"]["ConsigneeCityCode"] = $ilkodu;
		          	$array["ShipmentInfo"]["ConsigneeAreaCode"] = $ilcekodu;
		          	$array["ShipmentInfo"]["ConsigneePhoneNumber"] = $telephone;
		          	$array["ShipmentInfo"]["ConsigneeEMail"] = $email;
		          	$array["ShipmentInfo"]["ConsigneeExpenseCode"] = "";
		          	$array["ShipmentInfo"]["ServiceLevel"] = 3;
		          	$array["ShipmentInfo"]["PaymentType"] = 2;
		          	$array["ShipmentInfo"]["PackageType"] = "K";
		          	$array["ShipmentInfo"]["NumberOfPackages"] = 1;
		          	$array["ShipmentInfo"]["CustomerReferance"] = "";
		          	$array["ShipmentInfo"]["CustomerInvoiceNumber"] = $order_id;
		          	$array["ShipmentInfo"]["DescriptionOfGoods"] = "KİTAP";// What's KİTAP?
		          	$array["ShipmentInfo"]["DeliveryNotificationEmail"] = $this->customerEmail;

		          	$array["ReturnLabelLink"] = "barkod$order_id";
		          	$array["ReturnLabelImage"] = "barkod$order_id";

		          	$gonderi = $client->CreateShipment_Type1($array);

			        if($gonderi->CreateShipment_Type1Result->ShipmentNo){
			        	$gonderikodu = $gonderi->CreateShipment_Type1Result->ShipmentNo;
	              		$gonderibarkod = $gonderi->CreateShipment_Type1Result->LinkForLabelPrinting;
	              		$gonderibarkodx = $gonderi->CreateShipment_Type1Result->BarkodArrayPng->string;

	              		$comment = "Ürünler Kargoya Verildi. Takip No : $gonderikodu";
	              		$tarih = date("Y-m-d H:i:s");

	              		$this->db->query("INSERT INTO " . DB_PREFIX . "order_history SET order_id='$order_id', order_status_id='3', comment='$comment', date_added=NOW(), carrier='ups'");
	              		$this->db->query("INSERT INTO " . DB_PREFIX . "customerpartner_to_order_status SET order_id='$order_id', product_id='".$product_id."', order_status_id='3', comment='$comment', date_added=NOW()");
	              		$this->db->query("UPDATE ".DB_PREFIX."order SET order_status_id='3', date_modified=NOW() WHERE order_id='$order_id'");
	              		$this->db->query("UPDATE ".DB_PREFIX."order_product SET tracking='$gonderikodu', barcode='$gonderibarkodx' WHERE order_id='$order_id' AND product_id='".$product_id."'");
	              		return $gonderikodu;
			        }
			        else
			        {
			            return $gonderi;
			        }
		        }
		        else
		        {
		        	$mevcut_barcode = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product where order_id='".$order_id."' AND product_id='".$product_id."'")->row;
	      			$gonderikodu = $mevcut_barcode['tracking'];
	            	$gonderibarkodx = $mevcut_barcode['barcode'];

	            	$comment = "Ürünler Kargoya Verildi. Takip No : $gonderikodu";

	            	$this->db->query("INSERT INTO " . DB_PREFIX . "order_history SET order_id='$order_id', order_status_id='3', comment='$comment', date_added='$tarih', carrier='ups'");
	            	$this->db->query("INSERT INTO " . DB_PREFIX . "customerpartner_to_order_status SET order_id='$order_id', product_id='".$product_id."', order_status_id='3', comment='$comment', date_added=NOW()");
	            	$this->db->query("UPDATE ".DB_PREFIX."order SET order_status_id='3', date_modified=NOW() WHERE order_id='$order_id'");
	            	$this->db->query("UPDATE ".DB_PREFIX."order_product SET tracking='$gonderikodu', barcode='$gonderibarkodx' WHERE order_id='$order_id' AND product_id='".$product_id."'");
	          		return $gonderikodu;
		        }
	  		}
	  		else
	  		{
	  			return "Adres Bilgileri UPS Sistemi İle Uyuşmamaktadır...";
	  		}
	  	}
	}
*/

	public function shipmentControl($referans){

	}

}
?>
