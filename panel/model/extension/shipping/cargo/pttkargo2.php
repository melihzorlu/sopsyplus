<?php

class ControllerExtensionShippingCargoPttkargo extends Controller
{
  private $ortam            = 'Test'; // TEST ORTAMI İÇİN küçük harfle 'Test' yazın, GERÇEK ORTAM İÇİN BOŞ BIRAKIN
  private $musteriId        = '804084718';
  private $musteriSifre     = '9AaZp5lhJkHErHEZv6mvw';
  private $iadeMusteriId    = '804084718';
  private $iadeMusteriSifre = '9AaZp5lhJkHErHEZv6mvw';
  //private $musteriTelefon   = '';
  private $musteriPrefix    = 'TEST';

  public function cleatTel($tel)
  {
    return substr(ltrim(str_replace(' ', '', str_replace('-', '', str_replace(')', '', str_replace('(', '', $tel)))), '0'), 0, 10);
  }


  public function eslestir(){

    $sql=$this->db->query("SELECT * FROM ".DB_PREFIX."order WHERE takipkod='BASARILI'");
    foreach ($sql->rows as $order){
      $p=$this->db->query("SELECT * FROM ".DB_PREFIX."pttkargo WHERE referansno='".$order["barcode"]."'");
      if(isset($p->row)){
        echo $order["order_id"]." --- ".$order["barcode"]." --- ".$p->row["barkodno"]."<br>";

        $this->db->query("UPDATE ".DB_PREFIX."pttkargo SET order_id='".$order["order_id"]."' WHERE referansno='".$order["barcode"]."'");
      }
    }
  }

  public function esletirme2(){

    $sql=$this->db->query("SELECT * FROM ".DB_PREFIX."pttkargo");
    foreach ($sql->rows as $order){

      $dates=explode("/",$order["islemtarihi"]);
      if($dates[1]){
        $dates2=explode(" ",$dates[2]);
        $newdate=$dates2[0]."-".$dates[1]."-".$dates[0]." ".$dates2[1];
        $this->db->query("UPDATE ".DB_PREFIX."pttkargo SET date_added='$newdate' WHERE order_id='".$order["order_id"]."'");
        echo $newdate."<br>";
      }
    }
  }

  public function gonderikontrolislemli(){
    $wsdl = 'https://pttws.ptt.gov.tr/GonderiTakipV2'.$this->ortam.'/services/Sorgu?wsdl';
    $options = array(
      'uri' => 'http://schemas.xmlsoap.org/wsdl/soap/',
      'style' => SOAP_RPC,
      'use' => SOAP_ENCODED,
      'soap_version' => SOAP_1_1,
      'cache_wsdl' => WSDL_CACHE_NONE,
      'connection_timeout' => 15,
      'trace' => true,
      'encoding' => 'UTF-8',
      'exceptions' => true,
    );
    $soap = new SoapClient($wsdl, $options);
    date_default_timezone_set('Europe/Istanbul');


    $sql = $this->db->query("SELECT * FROM ".DB_PREFIX."pttkargo WHERE sonislem NOT IN ('Teslim Edildi','Göndericisine Teslim Edildi') ORDER BY `date_added` ASC");
    foreach ($sql->rows as $kargo){

      $i=$i+1;
      echo $i."<br>";

      $referansno = $kargo["referansno"];
      $order_id   = $kargo["order_id"];


      $xml_array["input"]= array("kullanici"=> $this->musteriId, "referansNo"=>$referansno,"sifre"=>$this->musteriSifre);
      $request = $soap->gonderiSorgu_referansNo($xml_array);

      $cevap = json_decode(json_encode($request),true);

      if(isset($cevap["return"]["BARNO"]) && $cevap["return"]["BARNO"]){
        $barkodno         = $cevap["return"]["BARNO"];
        $takip            = $cevap["return"]["sonucAciklama"];
        $gonderiucreti    = $cevap["return"]["GONUCR"];
        $gonderiagirlik   = $cevap["return"]["GR"];
        $teslimalan       = $cevap["return"]["TESALAN"];
        $toplamislem      = count($cevap["return"]["dongu"])-1;
        $sonislem         = $cevap["return"]["dongu"][$toplamislem]["ISLEM"];
        $islemtarihi      = $cevap["return"]["dongu"][$toplamislem]["ITARIH"]." ".$cevap["return"]["dongu"][$toplamislem]["ISAAT"];
        $ilkislem         = $cevap["return"]["dongu"][0]["ISLEM"];

        $dates=explode("/",$islemtarihi);

        $dates2=explode(" ",$dates[2]);
        $newdate          = $dates2[0]."-".$dates[1]."-".$dates[0]." ".$dates2[1];


        $this->db->query("UPDATE ".DB_PREFIX."pttkargo SET sonislem='".$sonislem."', islemtarihi='".$islemtarihi."', teslimalan='".$teslimalan."', date_added='".$newdate."' WHERE order_id='".$order_id."'");


        echo "<br>".$newdate."---".$order_id." --- ".$barkodno." --- ".$sonislem." --- ".$islemtarihi."---"."<br>";

        //print_r(array("islem"=>"basarili","barkod"=>$barkodno,"gonderiucreti"=>$gonderiucreti,"gonderiagirlik"=>$gonderiagirlik,"sonislem"=>$sonislem,"islemtarihi"=>$islemtarihi,"teslimalan"=>$teslimalan,"takipurl"=>$takip));
      }else{
        print_r(array('islem'=>"Kargo Henüz İşlem Görmemiş"));
      }
      sleep(1);
    }
  }

  public function gonderikontrolislemsiz(){
    $wsdl       = 'https://pttws.ptt.gov.tr/GonderiTakipV2'.$this->ortam.'/services/Sorgu?wsdl';
    $options    = array(
      'uri'           => 'http://schemas.xmlsoap.org/wsdl/soap/',
      'style'         => SOAP_RPC,
      'use'           => SOAP_ENCODED,
      'soap_version'  => SOAP_1_1,
      'cache_wsdl'    => WSDL_CACHE_NONE,
      'connection_timeout' => 15,
      'trace'         => true,
      'encoding'      => 'UTF-8',
      'exceptions'    => true,
    );
    $soap = new SoapClient($wsdl, $options);
    date_default_timezone_set('Europe/Istanbul');


    $sql = $this->db->query("SELECT * FROM ".DB_PREFIX."pttkargo_islem WHERE barcode NOT IN (SELECT referansno FROM ".DB_PREFIX."pttkargo) and takipkod='BASARILI' ORDER by date_added DESC");

    foreach ($sql->rows as $kargo){

      $i=$i+1;

      echo $i."<br>";

      $referansno   = $kargo["barcode"];
      $order_id     = $kargo["order_id"];

      $xml_array["input"]=array("kullanici"=>$this->musteriId,"referansNo"=>$referansno,"sifre"=>$this->musteriSifre);
      $request = $soap->gonderiSorgu_referansNo($xml_array);

      $cevap = json_decode(json_encode($request),true);


      echo $kargo["order_id"]." -- ".$kargo["barcode"]." --- ".$cevap."<br>";

      if(isset($cevap["return"]["BARNO"]) && $cevap["return"]["BARNO"]){
        $barkodno       = $cevap["return"]["BARNO"];
        $takip          = $cevap["return"]["sonucAciklama"];
        $gonderiucreti  = $cevap["return"]["GONUCR"];
        $gonderiagirlik = $cevap["return"]["GR"];
        $teslimalan     = $cevap["return"]["TESALAN"];
        $toplamislem    = count($cevap["return"]["dongu"])-1;
        $sonislem       = $cevap["return"]["dongu"][$toplamislem]["ISLEM"];
        $islemtarihi    = $cevap["return"]["dongu"][$toplamislem]["ITARIH"]." ".$cevap["return"]["dongu"][$toplamislem]["ISAAT"];
        $ilkislem       = $cevap["return"]["dongu"][0]["ISLEM"];

        $dates          = explode("/",$islemtarihi);

        $dates2         = explode(" ",$dates[2]);
        $newdate=$dates2[0]."-".$dates[1]."-".$dates[0]." ".$dates2[1];

        $ref=$this->db->query("SELECT * FROM ".DB_PREFIX."pttkargo WHERE order_id='".$order_id."' and referansno='".$referansno."'");
        if($ref->num_rows>0){
          $this->db->query("UPDATE ".DB_PREFIX."pttkargo SET sonislem='Kabul Edildi', islemtarihi='".$islemtarihi."', teslimalan='".$teslimalan."', date_added='".$newdate."' WHERE order_id='".$order_id."' and referansno='".$referansno."'");
        }else{
          $this->db->query("INSERT INTO ".DB_PREFIX."pttkargo SET order_id='".$order_id."', referansno='".$referansno."', barkodno='".$barkodno."', gonderiucreti='".$gonderiucreti."', gonderiagirlik='".$gonderiagirlik."', sonislem='Kabul Edildi', islemtarihi='".$islemtarihi."', date_added='".$newdate."', teslimalan='".$teslimalan."', takip='".$takip."'");

          $this->db->query("DELETE FROM ".DB_PREFIX."pttkargo_islem WHERE order_id='".$order_id."' and barcode='".$referansno."'");
        }


        $date = date("Y-m-d H:i:s");
        $this->db->query("UPDATE ".DB_PREFIX."customerpartner_to_order SET order_product_status='3' WHERE  order_id='".$order_id."'");

        $this->db->query("UPDATE ".DB_PREFIX."customerpartner_to_order_status SET order_product_status='3' WHERE  order_id='".$order_id."'");

        $this->db->query("UPDATE ".DB_PREFIX."order SET order_status_id='3', date_modified = NOW() WHERE  order_id='".$order_id."'");

        $this->db->query("INSERT INTO ".DB_PREFIX."order_history SET order_id='".$order_id."', order_status_id='3', comment='Ürünleriniz Kargoya Verilmiştir',  	date_added = NOW()");

        //$this->db->query("UPDATE ".DB_PREFIX."pttkargo SET sonislem='".$sonislem."', islemtarihi='".$islemtarihi."', teslimalan='".$teslimalan."', date_added='".$newdate."' WHERE order_id='".$order_id."'");

        echo "<br>".$newdate."---".$order_id." --- ".$barkodno." --- ".$sonislem." --- ".$islemtarihi."---"."<br>";

        //print_r(array("islem"=>"basarili","barkod"=>$barkodno,"gonderiucreti"=>$gonderiucreti,"gonderiagirlik"=>$gonderiagirlik,"sonislem"=>$sonislem,"islemtarihi"=>$islemtarihi,"teslimalan"=>$teslimalan,"takipurl"=>$takip));
      }else{
        print_r(array('islem'=>"Kargo Henüz İşlem Görmemiş"));
      }
      sleep(1);


    }
  }

  public function gonderikontrol(){
    $wsdl         = 'https://pttws.ptt.gov.tr/GonderiTakipV2'.$this->ortam.'/services/Sorgu?wsdl';
    $options      = array(
      'uri'           => 'http://schemas.xmlsoap.org/wsdl/soap/',
      'style'         => SOAP_RPC,
      'use'           => SOAP_ENCODED,
      'soap_version'  => SOAP_1_1,
      'cache_wsdl'    => WSDL_CACHE_NONE,
      'connection_timeout' => 15,
      'trace'         => true,
      'encoding'      => 'UTF-8',
      'exceptions'    => true,
    );

    date_default_timezone_set('Europe/Istanbul');

    $bugun  = date("Y-m-d H:i:s");
    $cevir  = strtotime('-3 hour',strtotime($bugun));
    $cevir2 = strtotime('-2 day',strtotime($bugun));
    $dun    = date("Y-m-d H:i:s",$cevir);
    $dun2   = date("Y-m-d H:i:s",$cevir2);



    $toplam = $this->db->query("SELECT * FROM ".DB_PREFIX."order WHERE `date_modified` >= '$dun2' and `date_modified` <= '$bugun' and takipkod='BASARILI' ORDER by date_added DESC")->num_rows;

    $limit=1;

    if (isset($this->request->get['page'])) {
      $page = $this->request->get['page'];
    } else {
      $page = 1;
    }

    $start=($page - 1) * $limit;

    $sinir= " LIMIT " . (int)$start . "," . (int)$limit;

    //$sql=$this->db->query("SELECT * FROM ".DB_PREFIX."order WHERE `date_modified` >= '$dun2' and `date_modified` <= '$bugun' and takipkod='BASARILI' ORDER by date_added DESC");

    $barkodpttkargoislem=$this->db->query("SELECT * FROM ".DB_PREFIX."pttkargo_islem WHERE takipkod='BASARILI' ORDER by date_added DESC")->rows;
     $i=0;


    foreach ($barkodpttkargoislem as $ref){
      $i=$i+1;
      $referansno=$ref["barcode"];
      $soap = new SoapClient($wsdl, $options);
      $xml_array["input"] = array(
        "kullanici"   =>$this->musteriId,
        "referansNo"  =>$referansno,
        "sifre"       =>$this->musteriSifre
      );

      $request = $soap->gonderiSorgu_referansNo($xml_array);

      $cevap = json_decode(json_encode($request),true);


      if(isset($cevap["return"]["BARNO"]) && $cevap["return"]["BARNO"]){
        $barkodno=$cevap["return"]["BARNO"];
        $takip=$cevap["return"]["sonucAciklama"];
        $gonderiucreti=$cevap["return"]["GONUCR"];
        $gonderiagirlik=$cevap["return"]["GR"];
        $teslimalan=$cevap["return"]["TESALAN"];
        $toplamislem=count($cevap["return"]["dongu"])-1;
        $sonislem=$cevap["return"]["dongu"][$toplamislem]["ISLEM"];
        $islemtarihi=$cevap["return"]["dongu"][$toplamislem]["ITARIH"]." ".$cevap["return"]["dongu"][$toplamislem]["ISAAT"];

        $ilkislem=$cevap["return"]["dongu"]["ISLEM"];



        $refx=$this->db->query("SELECT * FROM ".DB_PREFIX."pttkargo WHERE referansno='".$referansno."'");
        if($refx->num_rows>0){
          $this->db->query("UPDATE ".DB_PREFIX."pttkargo SET sonislem='".$sonislem."', islemtarihi='".$islemtarihi."', teslimalan='".$teslimalan."' WHERE referansno='".$referansno."'");
        }else{
          $this->db->query("INSERT INTO ".DB_PREFIX."pttkargo SET order_id='".$ref["order_id"]."', referansno='".$referansno."', barkodno='".$barkodno."', gonderiucreti='".$gonderiucreti."', gonderiagirlik='".$gonderiagirlik."', sonislem='".$sonislem."', islemtarihi='".$islemtarihi."', teslimalan='".$teslimalan."', takip='".$takip."'");
        }

        if(isset($sonislem) && $sonislem=="Teslim Edildi"){
         // $checkstatus = $this->db->query("SELECT * FROM ".DB_PREFIX."order WHERE barcode='".$referansno."'");
          $orderstatuses=$this->db->query("SELECT * FROM ".DB_PREFIX."pttkargo_islem islem INNER JOIN ".DB_PREFIX."pttkargo pt ON(islem.barcode=pt.referansno) INNER JOIN ".DB_PREFIX."customerpartner_to_order c2o ON(islem.barcode=c2o.barcode) WHERE islem.barcode='".$referansno."' AND c2o.customer_id='".$ref["seller_id"]."'")->rows;

          if(isset($orderstatuses)){
            foreach ($orderstatuses as $orderstatus) {
              $date = date("Y-m-d H:i:s");
              $this->db->query("UPDATE ".DB_PREFIX."customerpartner_to_order SET order_product_status='17' WHERE  order_id='".$orderstatus["order_id"]."' AND product_id='".$orderstatus['product_id']."'");

              $this->db->query("UPDATE ".DB_PREFIX."customerpartner_to_order_status SET order_product_status='17' WHERE  order_id='".$orderstatus["order_id"]."' AND product_id='".$orderstatus['product_id']."'");


            }
          }
          //order tablosu durum guncelleme

         /* $countTeslimedildi=$this->db->query("SELECT COUNT(order_id) FROM ".DB_PREFIX."pttkargo pt WHERE pt.order_id='".$ref["order_id"]."' AND pt.sonislem='Teslim Edildi'")->row;
          $countpttislem=$this->db->query("SELECT COUNT(order_id) FROM ".DB_PREFIX."pttkargo_islem ptislem WHERE ptislem.order_id='".$ref["order_id"]."'")->row;
          $countorder=$this->db->query("SELECT COUNT(order_id) FROM ".DB_PREFIX."customerpartner_to_order WHERE order_id='".$ref["order_id"]."' AND barcode=0")->row;*/


          $this->db->query("UPDATE ".DB_PREFIX."order SET order_status_id='17', date_modified = NOW() WHERE  order_id='".$ref["order_id"]."'");

          $this->db->query("INSERT INTO ".DB_PREFIX."order_history SET order_id='".$checkstatus->row["order_id"]."', order_status_id='17', comment='Ürünleriniz Teslim Edilmiştir',   date_added = NOW()");

          //order tablosu durum guncelleme bitti
        }elseif(isset($ilkislem) && $ilkislem=="Kabul Edildi"){
             $orderstatuses=$this->db->query("SELECT * FROM ".DB_PREFIX."pttkargo_islem islem INNER JOIN ".DB_PREFIX."pttkargo pt ON(islem.barcode=pt.referansno) INNER JOIN ".DB_PREFIX."customerpartner_to_order c2o ON(islem.barcode=c2o.barcode) WHERE islem.barcode='".$referansno."' AND c2o.customer_id='".$ref["seller_id"]."'")->rows;


            if(isset($orderstatuses)){
              foreach ($orderstatuses as $orderstatus) {
                $date = date("Y-m-d H:i:s");
                $this->db->query("UPDATE ".DB_PREFIX."customerpartner_to_order SET order_product_status='3' WHERE  order_id='".$orderstatus["order_id"]."' AND product_id='".$orderstatus['product_id']."'");

                $this->db->query("UPDATE ".DB_PREFIX."customerpartner_to_order_status SET order_product_status='3' WHERE  order_id='".$orderstatus["order_id"]."' AND product_id='".$orderstatus['product_id']."'");

              }
              $this->db->query("UPDATE ".DB_PREFIX."order SET order_status_id='3', date_modified = NOW() WHERE  order_id='".$orderstatus["order_id"]."'");

              $this->db->query("INSERT INTO ".DB_PREFIX."order_history SET order_id='".$orderstatus["order_id"]."', order_status_id='3', comment='Kargo Barkod no: ".$barkodno." olan ürünleriniz Kargoya Verilmiştir', date_added = NOW()");

            }
          }

        echo "<br>".$ref["date_added"]."---".$checkstatus->row["order_id"]." --- ".$barkodno." --- ".$sonislem." --- ".$islemtarihi."---"."<br>";

        print_r(array("islem"=>"basarili","barkod"=>$barkodno,"gonderiucreti"=>$gonderiucreti,"gonderiagirlik"=>$gonderiagirlik,"sonislem"=>$sonislem,"islemtarihi"=>$islemtarihi,"teslimalan"=>$teslimalan,"takipurl"=>$takip));
        }else{
          print_r(array('islem'=>"Kargo Henüz İşlem Görmemiş"));
        }
        sleep(1);
      }

    ini_set("soap.wsdl_cache_enabled", 0);
    ini_set('soap.wsdl_cache_ttl', 0);

    /*$page=$page+1;
    if($toplam==$page){

  }else{
  sleep(5);
  header( "Refresh:1; url=https://www.kastamo.com/index.php?route=api/pttkargo/gonderikontrol&page=".$page."");
}*/
}

public function getBarcode($barkodIncrementId)
{
  $carpanSplit = [1, 3, 1, 3, 1, 3, 1, 3, 1, 3, 1, 3];
  $barkodSplit = str_split($barkodIncrementId);
  if (count($barkodSplit) != 8) {
    return false;
  }
  $sum = 0;
  for ($i = 0; $i < 8; $i++) {
    $sum += $carpanSplit[$i] * $barkodSplit[$i];
  }
  $nearest = (int)ceil($sum / 9) * 9;
  $checkDigit = $nearest - $sum;
  array_push($barkodSplit, $checkDigit);
  return implode('', $barkodSplit);
}

private function getClient($uri, $options = null)
{
  $config = [];
  if (is_array($options)) {
    $config = array_merge($config, $options);
  }
  return new \SoapClient('https://pttws.ptt.gov.tr/' . $uri . '/services/Sorgu?wsdl', $config);
}

public function converter($data){
  $search=array('&#x218;','&#x219;','&#x215;','&#x214;','&#x20D;','&#x20C;','&#x20B;','&#x20A;','&#x1F5;','&#x1F4;','&#x1E7;','&#x1E6;','Ș','ș','ȕ','Ȕ','ȍ','Ȍ','ȋ','Ȋ','ǵ','Ǵ','ǧ','Ǧ','ū');
  $replace=array('Ş','ş','ü','Ü','ö','Ö','i','İ','ğ','Ğ','ğ','Ğ','Ş','ş','ü','Ü','ö','Ö','i','İ','ğ','Ğ','ğ','Ğ','ü');
  return str_replace($search,$replace,$data);
}

public function sendData()
{

  if ($this->request->get['order_id']) {
    $order_id = $this->request->get['order_id'];
    $sqltmp = "SELECT *,c.firstname as saticiadi,c.lastname as saticisoyadi,c.telephone as saticitel,ac.address_id as aid, o.telephone as alicitel, c.customer_id as satici_id,c.address_id as said, o.customer_id as aliciid,(cto.admin+cto.customer) as total, o.shipping_custom_field as custom_fields FROM oc_customerpartner_to_order cto LEFT JOIN oc_order o ON (cto.order_id = o.order_id) LEFT JOIN oc_customerpartner_to_customer ctc ON (ctc.customer_id = cto.customer_id) LEFT JOIN oc_customer c ON (ctc.customer_id=c.customer_id) LEFT JOIN oc_customer ac ON o.customer_id = ac.customer_id WHERE cto.order_id = '" . $order_id . "' and cto.customer_id='".$this->customer->getID()."'";
    $sql = $this->db->query($sqltmp);
    $sql = $sql->row;


    $check = $this->db->query("SELECT * FROM ".DB_PREFIX."pttkargo_islem WHERE order_id='".$this->request->get['order_id']."' and seller_id='".$this->customer->getID()."'");

    if (isset($check->row['barcode']) && $check->row['barcode']) {
      $this->printBarcode2($this->request->get['order_id'],$this->customer->getID());
    }

    $sql2tmp            = "SELECT identityNumber,ptt_cek_no FROM oc_customerpartner_to_customer WHERE customer_id = " . $sql['satici_id'];
    $sql2               = $this->db->query($sql2tmp)->row;

    $addresstmp         = "SELECT a.*,oz.name as zone_n,oc.name as country FROM oc_address a INNER JOIN oc_country oc ON a.country_id = oc.country_id INNER JOIN oc_zone oz ON a.zone_id = oz.zone_id WHERE a.address_id = {$sql["said"]}";
    $address            = $this->db->query($addresstmp)->row;

    $sellerName         = $this->converter($sql["saticiadi"]);
    $sellerLastName     = $this->converter($sql["saticisoyadi"]);
    $sellerAddress      = $address['address_1'] . ' ' . $address['address_2'] . ' ' . $address['zone_n'] . ' ' . $address['country'];
    $sellerPhoneNumber  = $sql["saticitel"];
    //$sellerPhoneNumber = '05382531164';
    $receiverAddress    = $sql["shipping_address_1"] . ' ' . $sql["shipping_zone"] . ' ' . $sql["shipping_country"];
    $type               = $sql['shipping_method'];
    //$barcode = $this->getBarcode(rand(1000000000, 99999999));
    $barcode            = $order_id.$this->customer->getID();
    if ($type == 'Gönderici Ödemeli') {
      $paymentMethod = 'N';
    } elseif ($type == 'Alıcı Ödemeli') {
      $paymentMethod = 'UA';
    } else {
      $paymentMethod = 'N';
    }
    if ($type == 'Kapıda Ödeme') {
      $odeme_sart_ucreti = number_format((float)$sql['total'], 2, '.', '');;
      $ekhizmet = 'OS';
      $cekNo = $sql2['ptt_cek_no'];
    }elseif($sql['payment_code'] == 'cod'){
      $odeme_sart_ucreti = number_format((float)$sql['total'], 2, '.', '');;
      $ekhizmet = 'OS';
      $cekNo = $sql2['ptt_cek_no'];
    } else {
      $odeme_sart_ucreti = 0;
      $ekhizmet = '';
      $cekNo = '';
    }


    $alicib=$this->db->query("SELECT * FROM ".DB_PREFIX."order WHERE order_id='".$order_id."'")->row;


    $customfield = json_decode($alicib["shipping_custom_field"], true);

    if(isset($customfield[22]) && $customfield[22]){
      $alicib["telephone"]=$customfield[22];
    }else{
      $alicib["telephone"]=$alicib["telephone"];
    }


    $soap = [
      'aAdres'            => $receiverAddress,
      'agirlik'           => 0,
      'aliciAdi'          => $this->converter($alicib["shipping_firstname"]) . ' ' . $this->converter($alicib["shipping_lastname"]),
      'aliciIlAdi'        => $alicib["shipping_zone"],
      'aliciIlceAdi'      => $alicib["shipping_city"],
      'aliciSms'          => $this->cleatTel($alicib["telephone"]),
      'aliciTel'          => $this->cleatTel($alicib["telephone"]),
      'boy'               => 0,
      'deger_ucreti'      => 0,
      'desi'              => 0,
      'ekhizmet'          => $ekhizmet,
      'en'                => 0,
      'musteriReferansNo' => $barcode,
      'odemesekli'        => $paymentMethod,
      'odeme_sart_ucreti' => $odeme_sart_ucreti,
      'rezerve1'          => $cekNo,
      'posta_ceki_no'     => $cekNo,
      'yukseklik'         => 0,
      'gondericibilgi'    => [
        'gonderici_adi'       => $sellerName . ' ' . $sellerLastName,
        'gonderici_adresi'    => $sellerAddress,
        'gonderici_il_ad'     => $address['zone_n'],
        'gonderici_ilce_ad'   => $address['city'],
        'gonderici_telefonu'  => $this->cleatTel($sellerPhoneNumber),
      ],
      'iadeAAdres'        => $sellerAddress,
      'iadeAliciAdi'      => $sellerName . ' ' . $sellerLastName,
      'iadeAliciIlAdi'    => $address['zone_n'],
      'iadeAliciIlceAdi'  => $address['city'],

    ];


    //https://pttws.ptt.gov.tr/PttVeriYukleme/services/Sorgu?wsdl

    //$response = $this->callAPI('https://pttws.ptt.gov.tr/PttVeriYukleme/services/Sorgu');
    //$response = $this->callAPI('https://pttws.ptt.gov.tr/PttVeriYukleme/services/Sorgu');
    $response = $this->callAPI('https://pttws.ptt.gov.tr/PttVeriYukleme'.$this->ortam.'/services/Sorgu');
    $response->kabulEkle2([
      'input'       => [
        'dongu'     => $soap,
        'dosyaAdi'  => $order_id.$this->customer->getID(),
        'index.php?route=api/pttkargo/sendData&order_id' => '?',
        'musteriId' => $this->musteriId,
        'sifre'     => $this->musteriSifre
      ]
    ]);




    $response = $response->__last_response;
    $response = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $response);
    $xml = new SimpleXMLElement($response);
    $response = $xml->soapenvBody->nskabulEkle2Response->nsreturn;


    if(isset($check->row) && $check->row){

    }else{
      $sqlins = "INSERT INTO ".DB_PREFIX."pttkargo_islem SET order_id='".$this->request->get['order_id']."', seller_id='".$this->customer->getID()."', barcode='".$barcode."', takipkod='" . $response->ax21dongu->ax21donguAciklama . "', date_added=NOW()";
      $this->db->query($sqlins);
    }

    $this->printBarcode2($this->request->get['order_id'],$this->customer->getID());


  }
}


public function iadesendData()
{

  if ($this->request->get['order_id'] && $this->request->get['iade_id']) {

    $order_id     = $this->request->get['order_id'];
    $iade_id      = $this->request->get['iade_id'];
    $sqltmp       = "SELECT *,c.firstname as saticiadi,c.lastname as saticisoyadi,c.telephone as saticitel,ac.address_id as aid, o.telephone as alicitel, c.customer_id as satici_id,c.address_id as said, o.customer_id as aliciid,(cto.admin+cto.customer) as total FROM oc_customerpartner_to_order cto LEFT JOIN oc_order o ON (cto.order_id = o.order_id) LEFT JOIN oc_customerpartner_to_customer ctc ON (ctc.customer_id = cto.customer_id) LEFT JOIN oc_customer c ON (ctc.customer_id=c.customer_id) LEFT JOIN oc_customer ac ON o.customer_id = ac.customer_id WHERE cto.order_id = '" . $order_id . "'";
    $sql = $this->db->query($sqltmp);
    $sql = $sql->row;

    $checklabel=$this->db->query("SELECT * FROM ".DB_PREFIX."wk_rma_order WHERE order_id='".$order_id."'")->row["shipping_label"];

    if ($checklabel != '') {

      $this->printBarcode3($order_id);

    }

    $sql2tmp = "SELECT identityNumber,ptt_cek_no FROM oc_customerpartner_to_customer WHERE customer_id = " . $sql['satici_id'];
    $sql2 = $this->db->query($sql2tmp)->row;

    $addresstmp = "SELECT a.*,oz.name as zone_n,oc.name as country FROM oc_address a INNER JOIN oc_country oc ON a.country_id = oc.country_id INNER JOIN oc_zone oz ON a.zone_id = oz.zone_id WHERE a.address_id = {$sql["said"]}";
    $address = $this->db->query($addresstmp)->row;

    $sellerName         = $sql["saticiadi"];
    $sellerLastName     = $sql["saticisoyadi"];
    $sellerAddress      = $address['address_1'] . ' ' . $address['address_2'] . ' ' . $address['city'] . ' ' . $address['zone_n'];
    $sellerPhoneNumber  = $sql["saticitel"];
    $receiverAddress    = $sql["shipping_address_1"].' '. $sql["shipping_city"]. ' ' . $sql["shipping_zone"];
    $type               = $sql['shipping_method'];
    //$barcode = $this->getBarcode(rand(1000000000, 9999999999));
    $barcode            = $this->musteriPrefix.'-IADE-'.$order_id. $this->customer->getID();
    $paymentMethod      = 'UA';
    $odeme_sart_ucreti  = 0;
    $ekhizmet           = '';
    $cekNo              = '';


    $soap = [
      'aAdres'            => $sellerAddress,
      'agirlik'           => 0,
      'aliciAdi'          => $sellerName . ' ' . $sellerLastName,
      'aliciIlAdi'        => $address['zone_n'],
      'aliciIlceAdi'      => $address['city'],
      'aliciSms'          => $this->cleatTel($sellerPhoneNumber),
      'aliciTel'          => $this->cleatTel($sellerPhoneNumber),
      'boy'               => 0,
      'deger_ucreti'      => 0,
      'desi'              => 0,
      'ekhizmet'          => $ekhizmet,
      'en'                => 0,
      'musteriReferansNo' => $barcode,
      'odemesekli'        => $paymentMethod,
      'odeme_sart_ucreti' => $odeme_sart_ucreti,
      'rezerve1'          => $cekNo,
      'posta_ceki_no'     => $cekNo,
      'yukseklik'         => 0,
      'gondericibilgi'    => [
        'gonderici_adi'       => $sql["shipping_firstname"] . ' ' . $sql["shipping_lastname"],
        'gonderici_adresi'    => $receiverAddress,
        'gonderici_il_ad'     => $sql["shipping_zone"],
        'gonderici_ilce_ad'   => $sql["shipping_city"],
        'gonderici_telefonu'  => $this->cleatTel($sql["alicitel"]),
      ],
      'iadeAAdres'        => $receiverAddress,
      'iadeAliciAdi'      => $sql["shipping_firstname"] . ' ' . $sql["shipping_lastname"],
      'iadeAliciIlAdi'    => $sql["shipping_zone"],
      'iadeAliciIlceAdi'  => $sql["shipping_city"],

    ];

    //print_r($soap); exit;
    //https://pttws.ptt.gov.tr/PttVeriYukleme/services/Sorgu?wsdl

    //$response = $this->callAPI('https://pttws.ptt.gov.tr/PttVeriYukleme/services/Sorgu');
    //$response = $this->callAPI('https://pttws.ptt.gov.tr/PttVeriYukleme/services/Sorgu');
    $response = $this->callAPI('https://pttws.ptt.gov.tr/PttVeriYukleme'.$this->ortam.'/services/Sorgu');//Test

    $response->kabulEkle2([
      'input'   => [
        'dongu'     => $soap,
        'dosyaAdi'  => $this->musteriPrefix."-IADE-".$order_id.$this->customer->getID().$iade_id,
        'kullanici' => '?',
        'musteriId' => $this->iadeMusteriId,
        'sifre'     => $this->iadeMusteriSifre
      ]
    ]);


    $response = $response->__last_response;
    $response = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $response);
    $xml      = new SimpleXMLElement($response);
    $response = $xml->soapenvBody->nskabulEkle2Response->nsreturn;

    if (json_decode($response->ax21hataKodu,true) == 1) {
      $sqlrma = "UPDATE " . DB_PREFIX . "wk_rma_order SET admin_status = 2, shipping_label='".$this->musteriPrefix."-IADE-".$order_id.$this->customer->getID().$iade_id."', takipkod = '" . $response->ax21dongu->ax21donguAciklama . "' WHERE order_id = '".$order_id."'";
      $this->db->query($sqlrma);

      //@TODO: BURADA BİR HATA OLABİLİR

      die(json_encode(array('status' => 'success','orderid' => $order_id, 'barcode' => $this->musteriPrefix."-IADE-".$order_id.$this->customer->getID().$iade_id, 'order_details' => '')));
      //$this->printBarcode3($this->request->get['order_id']);
    } else {
      die( json_encode(array('status' => 'error','message' => 'İlerleyen zamanda tekrar deneyiniz.')));
    }
  }
}

public function printBarcode($order_id){
  if (!$this->customer->isLogged()) {
    $this->session->data['redirect'] = $this->url->link('account/customerpartner/profile', '', true);
    $this->response->redirect($this->url->link('account/login', '', true));
  }

  $this->load->model('account/customerpartner');

  $data['chkIsPartner'] = $this->model_account_customerpartner->chkIsPartner();

  $this->load->model('checkout/order');
  $order_data = $this->model_checkout_order->getOrder($order_id);
  $order_data['seller_name']      = $this->customer->getFirstName();
  $order_data['seller_lastname']  = $this->customer->getLastName();
  $order_data['seller_telephone'] = $this->customer->getTelephone();
  $address_id                     = $this->customer->getAddressId();
  $this->load->model('account/address');
  $address                        = $this->model_account_address->getAddress($address_id);
  $order_data['seller_address']   = $address['address_1']. ' '.@$address['address_2'];
  $order_data['seller_city'] = $address['city'];
  $order_data['seller_zone'] = $address['zone'];

  $generator      = new Picqer\Barcode\BarcodeGeneratorPNG();
  $barcode        =  base64_encode($generator->getBarcode($order_data['barcode'], $generator::TYPE_CODE_128));
  //die( json_encode(array('status' => 'success','orderid' => $order_data['barcode'],'barcode' => '<img src="data:image/png;base64,' . $barcode . '">','order_details' => $order_data)));
  die( json_encode(array('status' => 'success','orderid' => $order_data['barcode'],'barcode' => '<img src="data:image/png;base64,' . $barcode . '">','order_details' => $order_data)));

}


public function printBarcode2($order_id,$seller_id){

  if (!$this->customer->isLogged()) {
    $this->session->data['redirect'] = $this->url->link('account/customerpartner/profile', '', true);
    $this->response->redirect($this->url->link('account/login', '', true));
  }

  $this->load->model('account/customerpartner');

  $data['chkIsPartner'] = $this->model_account_customerpartner->chkIsPartner();

  $this->load->model('checkout/order');

  $order_data           = $this->model_checkout_order->getOrder($order_id);
  $alicib=$this->db->query("SELECT * FROM ".DB_PREFIX."order WHERE order_id='".$order_id."'")->row;


  $order_data['barcode'] = $order_id.$seller_id;

  $customfield           = json_decode($alicib["shipping_custom_field"], true);

  if(isset($customfield[22]) && $customfield[22]){
    $order_data["telephone"]=$customfield[22];
  }else{
    $order_data["telephone"]=$order_data["telephone"];
  }
  $order_data['seller_name']        = $this->customer->getFirstName();
  $order_data['seller_lastname']    = $this->customer->getLastName();
  $order_data['seller_telephone']   =  $this->customer->getTelephone();
  $address_id = $this->customer->getAddressId();
  $this->load->model('account/address');
  $address = $this->model_account_address->getAddress($address_id);
  $order_data['seller_address'] = $address['address_1']. ' '.@$address['address_2'];
  $order_data['seller_city'] = $address['city'];
  $order_data['seller_zone'] = $address['zone'];

  $generator = new Picqer\Barcode\BarcodeGeneratorPNG();
  $barcode =  base64_encode($generator->getBarcode($order_data['barcode'], $generator::TYPE_CODE_128));
  //die( json_encode(array('status' => 'success','orderid' => $order_data['barcode'],'barcode' => '<img src="data:image/png;base64,' . $barcode . '">','order_details' => $order_data)));
  die( json_encode(array('status' => 'success','orderid' => $order_data['barcode'],'barcode' => '<img src="data:image/png;base64,' . $barcode . '">','order_details' => $order_data)));
}


public function printBarcode3($order_id){
  if (!$this->customer->isLogged()) {
    $this->session->data['redirect'] = $this->url->link('account/customerpartner/profile', '', true);
    $this->response->redirect($this->url->link('account/login', '', true));
  }

  $this->load->model('account/customerpartner');

  $data['chkIsPartner'] = $this->model_account_customerpartner->chkIsPartner();

  $this->load->model('checkout/order');
  $order_data = $this->model_checkout_order->getOrder($order_id);
  $order_data['seller_name'] = $this->customer->getFirstName();
  $order_data['seller_lastname'] = $this->customer->getLastName();
  $order_data['seller_telephone'] = $this->customer->getTelephone();;
  $address_id = $this->customer->getAddressId();
  $this->load->model('account/address');
  $address = $this->model_account_address->getAddress($address_id);
  $order_data['seller_address'] = $address['address_1']. ' '.@$address['address_2'];
  $order_data['seller_city'] = $address['city'];
  $order_data['seller_zone'] = $address['zone'];

  $barkod=$this->db->query("SELECT * FROM ".DB_PREFIX."wk_rma_order WHERE order_id='".$order_id."'")->row['shipping_label'];

  $generator = new Picqer\Barcode\BarcodeGeneratorPNG();
  $barcode =  base64_encode($generator->getBarcode($barkod, $generator::TYPE_CODE_128));
  //die( json_encode(array('status' => 'success','orderid' => $order_data['barcode'],'barcode' => '<img src="data:image/png;base64,' . $barcode . '">','order_details' => $order_data)));
  json_encode(array('status' => 'success','orderid' => $barkod,'barcode' => '<img src="data:image/png;base64,' . $barcode . '">','order_details' => $order_data));
}
public function callAPI($url)
{
  $options = array(
    'uri'=>'http://schemas.xmlsoap.org/wsdl/soap/',
    'style'=>SOAP_RPC,
    'use'=>SOAP_ENCODED,
    'soap_version'=>SOAP_1_1,
    'cache_wsdl'=>WSDL_CACHE_NONE,
    'connection_timeout'=>15,
    'trace'=>true,
    'encoding'=>'UTF-8',
    'exceptions'=>true,

  );
  $soap = new SoapClient($url.'?wsdl', $options);
  return $soap;

}

}
