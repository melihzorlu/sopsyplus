<?php
if (!interface_exists('CargoTemplate')) {

    require "cargoInterface.php";
}
class ModelExtensionShippingCargoOzlkrg extends Model implements CargoTemplate{

  public function __construct($registry) {

    parent::__construct($registry);

    $this->load->model('extension/shipping/softomikargo');

  }


  public function createShipment($cargo_data){

    $referans = $cargo_data['sender']['customer_id'].'-'.$cargo_data['order']['id'];
    $referans = 'OZLKRG'.$referans;
    $sender   = $cargo_data['sender'];
    $receiver = $cargo_data['receiver'];

    //BURASI
    $total = $cargo_data['totals'];

      return $this->returnFormat($cargo_data, $order_data = array(), $referans);

  }

  public function cancelShipment($referans){

      return true;


  }

  public function shipmentControl($referans){
      $s = "SELECT ss.id as id, ss.referans as referans, ss.bilgi as bilgi, ss.cargokey as cargokey, ssp.*, cto.order_product_status as order_product_status FROM `".DB_PREFIX."softomikargo_shipment_product` ssp
LEFT JOIN ".DB_PREFIX."softomikargo_shipment ss ON ssp.shipment_id = ss.id
LEFT JOIN ".DB_PREFIX."customerpartner_to_order cto ON cto.order_product_id = ssp.order_product_id
WHERE ss.referans = '".$referans."'";

      $shipment_sql = $this->db->query($s)->row;

      if(count($shipment_sql)>0){
        $cargokey = $shipment_sql['cargokey'];

        $bilgi = json_decode($shipment_sql['bilgi'], true);
        $this->load->model('extension/shipping/cargo/'.$cargokey);

      }


      if($sql['order_product_status'] == 17 || $sql['order_product_status'] == 18 || $sql['order_product_status'] == 5){
        $return['process'] = 'COMPLATED';
        $return['son_islem']  = 'Teslim Edildi';

      } else{
        $return['process']    = 'PROCESS';
        $return['son_islem'] = 'Kargoya Verildi';

      }

      $s = "UPDATE ". DB_PREFIX ."softomikargo_shipment_process
      SET
      `islem_tarihi` = '". date('Y/m/d H:i:s') ."',
      `son_islem` = '". $return['son_islem'] ."',
      WHERE shipment_id = ". $shipment_sql['id'];

      $sql = $this->db->query($s);
      return true;

  }


  public function getShipment($order_id, $data){

    $cargo_data = json_decode($data['bilgi'], true);

    return $this->returnFormat($cargo_data, $orderdata = array(), $data);

  }

  private function returnRmaFormat($cargo_data, $orderdata, $referans){

    if(is_array($referans)){

      $tarih    = $referans['date_added'];
      $referans = $referans['referans'];

    }

    $this->load->model('setting/setting');
    $config = $this->model_setting_setting->getSetting('config');

  $html = $_SERVER['SERVER_NAME'].'<br>Kargo Şirketi: <b>ÖZEL</b><br>Referans Numarası: <b>'.$referans.'</b>';

    return json_encode(
      array(
        'status'         => 'success',
        'referans'       => $referans,
        'order_details'  => $order_data,
        'document'       => $html
      ) , JSON_UNESCAPED_UNICODE
    );

  }

  private function returnFormat($cargo_data, $orderdata, $referans){

    if($cargo_data['rma_id']){

      return $this->returnRmaFormat($cargo_data, $order_data, $referans);

    }

    if(is_array($referans)){

      $tarih    = $referans['date_added'];
      $referans = $referans['referans'];

    }

    $this->load->model('setting/setting');
    $config = $this->model_setting_setting->getSetting('config');

    $html = '';
    $html .= '<div><img src="image/'.$config['config_logo'].'" style="max-height:100px; max-width:200px;"></div>';
    $html .= '<div><b>'.$_SERVER['SERVER_NAME'].'</b> | Sipariş Numarası : <b>'.$cargo_data['order']["id"].'</b></div>';
    $html .= '<hr>';
    $html .= '<div>Referans: <b>' . $referans . '</b></div>';
    $html .= '<hr>';
    $html .= '<pre align="left"><h4>ALICI BİLGİLERİ</h4>';
    $html .= '<br>Adı Soyadı:&#09;<strong>'.$cargo_data['receiver']["firstname"].' '.$cargo_data['receiver']["lastname"].'</strong>';
    $html .= '<br>Telefon:&#09;<strong>'.$cargo_data['receiver']["telephone"].'</strong>';
    $html .= '<br>Adres:&#09;&#09;<strong>'.$cargo_data['receiver']["address"].'</strong>';
    $html .= '<br>&#09;&#09;<strong>'.$cargo_data['receiver']["postcode"].' '.$cargo_data['receiver']["city"].' /  '. $cargo_data['receiver']["zone"] .'</strong>';
    $html .= '<br>&#09;&#09;<strong>'.$cargo_data['receiver']["country"].'</strong>';
    $html .= '</pre>';
    $html .= '<hr>';
    $html .= '<pre align="left">';
    $html .= '<h4>GÖNDERİCİ BİLGİLERİ</h4>';
    $html .= '<strong>'.$cargo_data['sender']["company"].'</strong><br>';
    $html .= '<br>Adı Soyadı:&#09;<strong>'.$cargo_data['sender']["firstname"].' '.$cargo_data['sender']["lastname"].'</strong>';
    $html .= '<br>Telefon:&#09;<strong>'.$cargo_data['sender']["telephone"].'</strong>';
    $html .= '<br>Adres:&#09;&#09;<strong>'.$cargo_data['sender']["address"].'</strong>';
    $html .= '<br>&#09;&#09;<strong>'.$cargo_data['sender']["postcode"].' '.$cargo_data['sender']["city"].' /  '. $cargo_data['sender']["zone"] .'</strong>';
    $html .= '<br>&#09;&#09;<strong>'.$cargo_data['sender']["country"].'</strong>';
    $html .= '</pre>';

    return json_encode(
      array(
        'status'         => 'success',
        'referans'       => $referans,
        'barcode'        => $referans,
        'order_details'  => $order_data,
        'document'       => $html
      ) , JSON_UNESCAPED_UNICODE
    );

  }



  public function callAPI($url)
  {

    return true;

  }

}
