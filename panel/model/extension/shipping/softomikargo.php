<?php
class ModelExtensionShippingSoftomikargo extends Model {

	function getQuote($address) {

	}

	// Tüm Aktif Kargoların key ve title değerlerini Döndürür
	public function getAllCargo(){
		$sql = $this->db->query("SELECT cargokey, title FROM " .DB_PREFIX ."softomikargo WHERE status = 1");
		return $sql->rows;
	}

	// Kargo Bilgilerini döndürür
	public function getCargoDetail($cargokey){

		$sql = $this->db->query("SELECT * FROM " .DB_PREFIX ."softomikargo WHERE cargokey = '".$cargokey."' AND status = 1")->row;
		return $sql;

	}

	public function getCargo($cargokey){

		$sql = $this->db->query("SELECT id, cargokey, description, status FROM " .DB_PREFIX ."softomikargo WHERE cargokey = '".$cargokey."' AND status = 1")->row;
		return $sql;

	}

	// Kurulmuş kargoların ek data verisini döndürür
	public function getCargoData($cargokey){
		$sql = $this->db->query("SELECT data FROM " .DB_PREFIX ."softomikargo WHERE cargokey = '".$cargokey."'")->row;
		return $this->decode($sql['data']);
	}

	public function getProductShipment($product_id, $order_id, $customer_id , $rma_id = 0){
		if($rma_id == ''){
			$rma_id = 0;
		}
		$sql = "SELECT ss.*, ssp.product_id, ssp.order_product_id, sspr.barcode, sspr.gonderi_ucreti, sspr.gonderi_agirlik,sspr.teslim_alan, sspr.takip_url,sspr.islem_tarihi,sspr.son_islem,sspr.bilgi
						FROM `".DB_PREFIX."softomikargo_shipment_product` AS ssp
						LEFT JOIN `".DB_PREFIX."softomikargo_shipment` AS ss ON	ss.id = ssp.shipment_id
						LEFT JOIN `".DB_PREFIX."softomikargo_shipment_process` AS sspr ON	ss.id = sspr.shipment_id
						WHERE ssp.product_id = ".$product_id." AND ssp.order_id = ".$order_id." AND (ss.alici_id = ".$customer_id." OR ss.gonderici_id = ".$customer_id.") AND ss.rma_id = ".$rma_id."";

		$query = $this->db->query($sql)->row;
		//print_r($product_id.'-'.$order_id.'-'.$customer_id.'-'.$rma_id);
		return $query;

	}

	//Referans barkodu ve göndericiye göre gönderi verisini döndürür
	public function queryShipmentReference($referans, $customer_id){

		$sql = $this->db->query("SELECT * FROM " .DB_PREFIX ."softomikargo_shipment WHERE referans = '".$referans."' AND gonderici_id = ".$customer_id."")->row;
		return $sql;

	}

	//Sipariş numarası ve gönderiye göre gönderi verisini döndürür
	public function queryShipmentOrder($order_id, $customer_id,$rma_id = 0){
		/*if($rma_id != 0){
			$s ="SELECT * FROM " .DB_PREFIX ."softomikargo_shipment WHERE order_id = ".$order_id." AND alici_id = ".$customer_id." AND rma_id = ".$rma_id."";
		}else{
			$s ="SELECT * FROM " .DB_PREFIX ."softomikargo_shipment WHERE order_id = ".$order_id." AND gonderici_id = ".$customer_id." AND rma_id = ".$rma_id."";
		}*/
		$s ="SELECT * FROM " .DB_PREFIX ."softomikargo_shipment WHERE order_id = ".$order_id." AND (alici_id = ".$customer_id." OR gonderici_id = ".$customer_id.") AND rma_id = ".$rma_id."";


		$sql = $this->db->query($s)->row;

		return $sql;
	}

	//Gönderiyi siler
	public function deleteShipment($order_id){

		$query = $this->queryShipmentOrder($order_id, $this->customer->getId());

		$shipment_id = $query['id'];

		$this->db->query("DELETE FROM " .DB_PREFIX ."softomikargo_shipment WHERE order_id = ".$order_id." AND gonderici_id = ".$this->customer->getId()."");

		$this->db->query("DELETE FROM " .DB_PREFIX ."softomikargo_shipment_product WHERE shipment_id = ".$shipment_id." AND order_id = ".$order_id."");

		return true;
	}

	//Shipment numarasına göre kargolanmış ürünleri döndürür
	public function queryCustomerShipmentProducts($shipment_id){

		$sql = "SELECT ss.cargokey, ssp.* FROM `".DB_PREFIX."softomikargo_shipment_product` as ssp
		LEFT JOIN `".DB_PREFIX."softomikargo_shipment` as ss ON ss.order_id=ssp.order_id
		WHERE ssp.shipment_id = ".$shipment_id;

		$return = $this->db->query($sql)->rows;

		return $return;

	}

	//Sipariş numarasına göre kargolanmış ürünleri döndürür
	public function queryShipmentOrderProducts($order_id){

		$sql = "SELECT ss.cargokey, ssp.* FROM `".DB_PREFIX."softomikargo_shipment_product` as ssp
		LEFT JOIN `".DB_PREFIX."softomikargo_shipment` as ss ON ss.order_id=ssp.order_id
		WHERE ssp.order_id = ".$order_id;

		$return = $this->db->query($sql)->rows;

		return $return;

	}

	/* Kargoya eklenmiş product verilerini döndürür.
	@return:
	select	 = Gönderilmiş tüm ürün idleri
	shipment = Kargo kaydı yapılmış ürünler
	diff		 = Kargo kaydı olmayan ürün id'leri
	*/
	public function queryShipmentProduct($order_id, $product_ids){
		//print_r($product_ids);exit;

//print_r("SELECT product_id FROM " .DB_PREFIX ."softomikargo_shipment_product WHERE order_id = ".$order_id." AND product_id IN (".$product_ids.")");exit;
		$sql = $this->db->query("SELECT product_id FROM " .DB_PREFIX ."softomikargo_shipment_product WHERE order_id = ".$order_id." AND product_id IN (".$product_ids.")")->rows;
		$shipmentProduct = array();
		foreach ($sql as $key => $value) {
			$shipmentProduct[$value['product_id']] = $value['product_id'];
		}
//print_r($product_ids);exit;
		$product_ids = explode(',', $product_ids);

		foreach ($product_ids as $key => $value) {
			$selectProduct[$value] = $value;
		}
		// Seçili ürünlerden kargo kaydı yapılmamış olanlar
		$return['diff'] 		= array_diff($selectProduct, $shipmentProduct);
		$return['select'] 	= $selectProduct;
		$return['shipment']	=	$shipmentProduct;
		//print_r("SELECT product_id FROM " .DB_PREFIX ."softomikargo_shipment_product WHERE order_id = ".$order_id." AND product_id IN (".$product_ids.")");exit;
		return $return;

	}

	// Satıcı Bilgilerini döndürür
	public function getSellerDetails($order_id) {
/*
		$query_detail['seller_detail'] 	= $this->db->query("SELECT cto.customer_id as seller_id, c.telephone, c.firstname,c.lastname, c.email, ctc.is_partner, ctc.companyname, ctc.screenname, ctc.companylogo, c.address_id as address
			FROM " . DB_PREFIX . "customer c
			LEFT JOIN ". DB_PREFIX ."customerpartner_to_order cto	ON(cto.customer_id = c.customer_id)
			LEFT JOIN " . DB_PREFIX . "customerpartner_to_customer ctc ON(c.customer_id = ctc.customer_id)
			WHERE cto.order_id = '" . (int)$order_id . "' AND c.customer_id = '" . (int)$this->customer->getId() . "'")->row;

			$query_detail['seller_address'] = $this->db->query("SELECT a.*,a.company as company, oz.name as zone_n,oc.name as country FROM oc_address a INNER JOIN oc_country oc ON a.country_id = oc.country_id INNER JOIN oc_zone oz ON a.zone_id = oz.zone_id WHERE a.address_id = {$query_detail['seller_detail']['address']}")->row;
*/
		$query_detail['seller_detail'] 	= $this->db->query("SELECT
		cto.customer_id as seller_id,
		c.telephone,
		c.firstname,
		c.lastname,
		c.email,
		ctc.official companyname,
		ctc.store_name screenname,
		ctc.store_logo companylogo,
		c.address_id as address
			FROM " . DB_PREFIX . "customer c
			LEFT JOIN ". DB_PREFIX ."order cto	ON(cto.customer_id = c.customer_id)
			LEFT JOIN " . DB_PREFIX . "customer_store ctc ON(c.customer_id = ctc.customer_id)
			WHERE cto.order_id = '" . (int)$order_id . "' AND c.customer_id = '" . (int)$this->session->data['user_id'] . "'")->row;

			/*$query_detail['seller_address'] = $this->db->query("SELECT a.*,
				a.company as company,
				oz.name as zone_n,
				oc.name as country
				FROM oc_address a
				INNER JOIN oc_country oc ON a.country_id = oc.country_id
				INNER JOIN oc_zone oz ON a.zone_id = oz.zone_id
				WHERE a.address_id = {$query_detail['seller_detail']['address']}")->row;*/

				$query_detail['seller_address'] = $this->db->query("SELECT a.*,
					a.official as company,
					oz.name as zone_n,
					oc.name as country
					FROM oc_customer_store a
					INNER JOIN oc_country oc ON a.store_country = oc.country_id
					INNER JOIN oc_zone oz ON a.store_city = oz.zone_id
					WHERE a.address_id = {$query_detail['seller_detail']['address']}")->row;

			return $query_detail;
		}

		public function saveShipment($data){
			$sql = $this->db->query("INSERT INTO ". DB_PREFIX ."softomikargo_shipment (cargokey, order_id, gonderici_id, alici_id, rma_id, referans, islem, bilgi, date_added)
			VALUES ('". $data['cargokey'] ."', '". $data['order_id'] ."', '". $data['gonderici_id'] ."', '". $data['alici_id'] ."', '". $data['rma_id'] ."', '". $data['referans'] ."', '". $data['islem'] ."', '". $data['bilgi'] ."', NOW() )");

			$shipment_id = $this->db->getLastId();

			if($shipment_id){
				return $shipment_id;
			}

			return false;
		}
		//BURA
		public function insertProcess($data){

			$sql = $this->db->query("INSERT INTO ". DB_PREFIX ."softomikargo_shipment_process
			(`shipment_id`, `barcode`, `gonderi_ucreti`, `gonderi_agirlik`, `teslim_alan`, `takip_url`, `islem_tarihi`, `son_islem`, `bilgi`)
			VALUES (". $data['shipment_id'] .", '". $data['barcode'] ."', '". $data['gonderi_ucreti'] ."', '". $data['gonderi_agirlik'] ."', '". $data['teslim_alan'] ."', '". $data['takip_url'] ."', '". $data['islem_tarihi'] ."', '". $data['son_islem'] ."', '". $data['bilgi'] ."' )");

			$this->db->query("UPDATE ". DB_PREFIX ."softomikargo_shipment SET islem = 'PROCESS' WHERE id = ".$data['shipment_id']."");


			$id = $this->db->getLastId();

			if($id){
				return $id;
			}

			return false;
		}

		public function updateProcess($data){
//print_r($data);exit;
			$s = "UPDATE ". DB_PREFIX ."softomikargo_shipment_process
			SET
			`gonderi_ucreti` = '". $data['gonderi_ucreti'] ."',
			`gonderi_agirlik` = '". $data['gonderi_agirlik'] ."',
			`teslim_alan` = '". $data['teslim_alan'] ."',
			`takip_url` = '". $data['takip_url'] ."',
			`islem_tarihi` = '". $data['islem_tarihi'] ."',
			`son_islem` = '". $data['son_islem'] ."',
			`bilgi`	= '". $data['bilgi'] ."'
			WHERE shipment_id = ". $data['shipment_id'];

			$sql = $this->db->query($s);

			if($sql){
				return true;
			}

			return false;
		}

		public function complatedShipment($shipment_id){

			$sql = $this->db->query("UPDATE ".DB_PREFIX."softomikargo_shipment SET islem = 'COMPLATED' WHERE id = ".$shipment_id."");

			if($sql)
			return true;

			return false;

		}


		public function getShipmentProducts($shipment_id){

			$sql = $this->db->query("SELECT * FROM ".DB_PREFIX."softomikargo_shipment_product WHERE shipment_id = ".$shipment_id."")->rows;

			return $sql;

		}

		public function sellerCargoPrice($seller_id, $weight){
			$s = "SELECT * FROM ".DB_PREFIX."wk_advance_custom_shipping_rangetable
			WHERE seller_id = ".$seller_id." AND (`from`<".$weight." AND `to` >=".$weight.") AND `type` = 'weight'";
			$sql= $this->db->query($s)->row;

			return $sql;

		}



		public function productChangeStatus($product_id, $order_id, $status_id){

			//////////// order_product tablosunda satıcı idsi var
			$this->db->query("UPDATE ".DB_PREFIX."customerpartner_to_order SET order_product_status='".$status_id."' WHERE order_product_status !='".$status_id."' AND order_id=".$order_id." AND product_id=".$product_id."");

			/////////// order_product tablosunda yeni sütun eklenecek
			$this->db->query("UPDATE ".DB_PREFIX."customerpartner_to_order_status SET order_product_status='".$status_id."', comment='Durum Değiştirildi' WHERE order_product_status !='".$status_id."'' AND order_id=".$order_id." AND product_id=".$product_id."");


			//Tüm ordera ait ürünler aynı statüte ise order durumunu değiştir.
			$this->db->query("UPDATE ".DB_PREFIX."order SET order_status_id='".$status_id."', date_modified = NOW() WHERE order_product_status !='".$status_id."' AND order_id='".$order_id."'");

		}

		public function saveProducts($shipment_id, $product_ids, $order_id){

			$query = "INSERT INTO " .DB_PREFIX. "softomikargo_shipment_product (`shipment_id`, `order_id`, `product_id`, `order_product_id`) VALUES ";
			if(!is_array($product_ids)){
				$product_ids = explode(',', $product_ids);
			}
			foreach($product_ids as $key=>$value){
				$query .= "(".$shipment_id.", ".$order_id.", ".$value.", (SELECT order_product_id FROM ".DB_PREFIX."order_product WHERE order_id = ".$order_id." AND product_id=".$value." LIMIT 1 ) )";

			}

			$query = str_replace(")(", "),(", $query);
//print_r($query);exit;
			if(isset($shipment_id, $product_ids, $order_id) && $shipment_id && is_array($product_ids) && $order_id){
				$sql = $this->db->query($query);
				$save = $this->db->getLastId();

			}
			return $save;

		}

		public function getWaitShipment(){

			$sql = $this->db->query("SELECT * FROM ".DB_PREFIX."softomikargo_shipment WHERE islem='WAIT'")->rows;

			if(count($sql) > 0){
				return $sql;
			}

			return false;

		}

		public function getProcessShipment(){

			$sql = $this->db->query("SELECT * FROM ".DB_PREFIX."softomikargo_shipment WHERE islem='PROCESS'")->rows;

			if(count($sql) > 0){
				return $sql;
			}

			return false;

		}

		//PROCESS TABLOSUNDAKİ VERİLERİ GETİRİR
		public function getShipmentProcess($shipment_id){
			$sql = $this->db->query("SELECT * FROM ".DB_PREFIX."softomikargo_shipment_process WHERE shipment_id=".$shipment_id."")->row;

			return $sql;
		}

		public function getSuccessShipment(){

			$sql = $this->db->query("SELECT * FROM ".DB_PREFIX."softomikargo_shipment WHERE islem='SUCCESS'")->rows;

			if(count($sql) > 0){
				return $sql;
			}

			return false;

		}


		private function decode($data){
			return json_decode($data,true);
		}

		private function encode($data){
			return json_encode($data);
		}



	}
	?>
