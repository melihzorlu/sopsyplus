<?php
// HTTP
define('HTTP_SERVER', 'https://panel.pazarmo.com/');
define('HTTP_CATALOG', 'https://pazarmo.com/');

// HTTPS
define('HTTPS_SERVER', 'https://panel.pazarmo.com/');
define('HTTPS_CATALOG', 'https://pazarmo.com/');

// DIR
define('DIR_APPLICATION', '/home/pazarmo/public_html/panel/');
define('DIR_SYSTEM', '/home/pazarmo/public_html/panel/system/');
define('DIR_IMAGE', '/home/pazarmo/public_html/image/');
define('DIR_LANGUAGE', '/home/pazarmo/public_html/panel/language/');
define('DIR_TEMPLATE', '/home/pazarmo/public_html/panel/view/template/');
define('DIR_CONFIG', '/home/pazarmo/public_html/panel/system/config/');
define('DIR_CACHE', '/home/pazarmo/public_html/panel/system/storage/cache/');
define('DIR_DOWNLOAD', '/home/pazarmo/public_html/panel/system/storage/download/');
define('DIR_LOGS', '/home/pazarmo/public_html/panel/system/storage/logs/');
define('DIR_MODIFICATION', '/home/pazarmo/public_html/panel/system/storage/modification/');
define('DIR_UPLOAD', '/home/pazarmo/public_html/panel/system/storage/upload/');
define('DIR_CATALOG', '/home/pazarmo/public_html/catalog/');

// DB
define('DB_DRIVER', 'mpdo');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'pazarmo_db');
define('DB_PASSWORD', 'CtwE3V@!GI^B');
define('DB_DATABASE', 'pazarmo_db');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
