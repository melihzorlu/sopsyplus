<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right"><a href="<?php echo $invoice; ?>" target="_blank" data-toggle="tooltip" title="<?php echo $button_invoice_print; ?>" class="btn btn-info"><i class="fa fa-print"></i></a> <a href="<?php echo $shipping; ?>" target="_blank" data-toggle="tooltip" title="<?php echo $button_shipping_print; ?>" class="btn btn-info"><i class="fa fa-truck"></i></a> <a href="<?php echo $edit; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a> <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>

  <div class="container-fluid">
    <div class="row">
      <div class="col-md-4">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-shopping-cart"></i> <?php echo $text_order_detail; ?></h3>
          </div>
          <table class="table">
            <tbody>
              <tr>
                <td style="width: 1%;"><button data-toggle="tooltip" title="<?php echo $text_store; ?>" class="btn btn-info btn-xs"><i class="fa fa-shopping-cart fa-fw"></i></button></td>
                <td><a href="<?php echo $store_url; ?>" target="_blank"><?php echo $store_name; ?></a></td>
              </tr>
              <tr>
                <td><button data-toggle="tooltip" title="<?php echo $text_date_added; ?>" class="btn btn-info btn-xs"><i class="fa fa-calendar fa-fw"></i></button></td>
                <td><?php echo $date_added; ?></td>
              </tr>
              <tr>
                <td><button data-toggle="tooltip" title="<?php echo $text_payment_method; ?>" class="btn btn-info btn-xs"><i class="fa fa-credit-card fa-fw"></i></button></td>
                <td><?php echo $payment_method; ?></td>
              </tr>
              <?php if ($shipping_method) { ?>
              <tr>
                <td><button data-toggle="tooltip" title="<?php echo $text_shipping_method; ?>" class="btn btn-info btn-xs"><i class="fa fa-truck fa-fw"></i></button></td>
                <td><?php echo $shipping_method; ?></td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
      <div class="col-md-4">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-user"></i> <?php echo $text_customer_detail; ?></h3>
          </div>
          <table class="table">
            <tr>
              <td style="width: 1%;"><button data-toggle="tooltip" title="<?php echo $text_customer; ?>" class="btn btn-info btn-xs"><i class="fa fa-user fa-fw"></i></button></td>
              <td><?php if ($customer) { ?>
                <a href="<?php echo $customer; ?>" target="_blank"><?php echo $firstname; ?> <?php echo $lastname; ?></a>
                <?php } else { ?>
                <?php echo $firstname; ?> <?php echo $lastname; ?>
                <?php } ?></td>
            </tr>
            <tr>
              <td><button data-toggle="tooltip" title="<?php echo $text_customer_group; ?>" class="btn btn-info btn-xs"><i class="fa fa-group fa-fw"></i></button></td>
              <td><?php echo $customer_group; ?></td>
            </tr>
            <tr>
              <td><button data-toggle="tooltip" title="<?php echo $text_email; ?>" class="btn btn-info btn-xs"><i class="fa fa-envelope-o fa-fw"></i></button></td>
              <td><a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></td>
            </tr>
            <tr>
              <td><button data-toggle="tooltip" title="<?php echo $text_telephone; ?>" class="btn btn-info btn-xs"><i class="fa fa-phone fa-fw"></i></button></td>
              <td><?php echo $telephone; ?></td>
            </tr>
          </table>
        </div>
      </div>
      <div class="col-md-4">
        <div class="panel panel-default">
          <button type="button" class = "btn btn-default" formaction="index.php?route=extension/shipping/shipping&order_id=<?php echo $order_id; ?>" id ="generate-shipping-label">&nbsp;&nbsp;Kargo Etiketi Oluştur</button>

          <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-cog"></i> <?php echo $text_option; ?></h3>
          </div>
          <table class="table">
            <tbody>
              <tr>
                <td><?php echo $text_invoice; ?></td>
                <td id="invoice" class="text-right"><?php echo $invoice_no; ?></td>
                <td style="width: 1%;" class="text-center"><?php if (!$invoice_no) { ?>
                  <button id="button-invoice" data-loading-text="<?php echo $text_loading; ?>" data-toggle="tooltip" title="<?php echo $button_generate; ?>" class="btn btn-success btn-xs"><i class="fa fa-cog"></i></button>
                  <?php } else { ?>
                  <button disabled="disabled" class="btn btn-success btn-xs"><i class="fa fa-refresh"></i></button>
                  <?php } ?></td>
              </tr>
              <tr>
                <td><?php echo $text_reward; ?></td>
                <td class="text-right"><?php echo $reward; ?></td>
                <td class="text-center"><?php if ($customer && $reward) { ?>
                  <?php if (!$reward_total) { ?>
                  <button id="button-reward-add" data-loading-text="<?php echo $text_loading; ?>" data-toggle="tooltip" title="<?php echo $button_reward_add; ?>" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i></button>
                  <?php } else { ?>
                  <button id="button-reward-remove" data-loading-text="<?php echo $text_loading; ?>" data-toggle="tooltip" title="<?php echo $button_reward_remove; ?>" class="btn btn-danger btn-xs"><i class="fa fa-minus-circle"></i></button>
                  <?php } ?>
                  <?php } else { ?>
                  <button disabled="disabled" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i></button>
                  <?php } ?></td>
              </tr>
              <tr>
                <td><?php echo $text_affiliate; ?>
                  <?php if ($affiliate) { ?>
                  (<a href="<?php echo $affiliate; ?>"><?php echo $affiliate_firstname; ?> <?php echo $affiliate_lastname; ?></a>)
                  <?php } ?></td>
                <td class="text-right"><?php echo $commission; ?></td>
                <td class="text-center"><?php if ($affiliate) { ?>
                  <?php if (!$commission_total) { ?>
                  <button id="button-commission-add" data-loading-text="<?php echo $text_loading; ?>" data-toggle="tooltip" title="<?php echo $button_commission_add; ?>" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i></button>
                  <?php } else { ?>
                  <button id="button-commission-remove" data-loading-text="<?php echo $text_loading; ?>" data-toggle="tooltip" title="<?php echo $button_commission_remove; ?>" class="btn btn-danger btn-xs"><i class="fa fa-minus-circle"></i></button>
                  <?php } ?>
                  <?php } else { ?>
                  <button disabled="disabled" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i></button>
                  <?php } ?></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-info-circle"></i> <?php echo $text_order; ?></h3>
      </div>
      <div class="panel-body">
        <table class="table table-bordered">
          <thead>
            <tr>
              <td style="width: 50%;" class="text-left"><?php echo $text_payment_address; ?></td>
              <?php if ($shipping_method) { ?>
              <td style="width: 50%;" class="text-left"><?php echo $text_shipping_address; ?></td>
              <?php } ?>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td class="text-left"><?php echo $payment_address; ?></td>
              <?php if ($shipping_method) { ?>
              <td class="text-left"><?php echo $shipping_address; ?></td>
              <?php } ?>
            </tr>
          </tbody>
        </table>
        <table class="table table-bordered">
          <thead>
            <tr>
              <td class="text-left"><?php echo $column_product; ?></td>
              <td class="text-left"><?php echo $column_model; ?></td>
              <td class="text-right"><?php echo $column_quantity; ?></td>
              <td class="text-right"><?php echo $column_price; ?></td>
              <td class="text-right"><?php echo $column_total; ?></td>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($products as $product) { ?>
            <tr>
              <td class="text-left">
                <input class="selection" type="checkbox" name="selected" id="productcheck" value="<?php echo $product['product_id']; ?>"/>
              </td>
              <td class="text-left"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                <?php foreach ($product['option'] as $option) { ?>
                <br />
                <?php if ($option['type'] != 'file') { ?>
                &nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
                <?php } else { ?>
                &nbsp;<small> - <?php echo $option['name']; ?>: <a href="<?php echo $option['href']; ?>"><?php echo $option['value']; ?></a></small>
                <?php } ?>
                <?php } ?></td>
              <td class="text-left"><?php echo $product['model']; ?></td>
              <td class="text-right"><?php echo $product['quantity']; ?></td>
              <td class="text-right"><?php echo $product['price']; ?></td>
              <td class="text-right"><?php echo $product['total']; ?></td>
            </tr>
            <?php } ?>
            <?php foreach ($vouchers as $voucher) { ?>
            <tr>
              <td class="text-left"><a href="<?php echo $voucher['href']; ?>"><?php echo $voucher['description']; ?></a></td>
              <td class="text-left"></td>
              <td class="text-right">1</td>
              <td class="text-right"><?php echo $voucher['amount']; ?></td>
              <td class="text-right"><?php echo $voucher['amount']; ?></td>
            </tr>
            <?php } ?>
            <?php foreach ($totals as $total) { ?>
            <tr>
              <td colspan="4" class="text-right"><?php echo $total['title']; ?></td>
              <td class="text-right"><?php echo $total['text']; ?></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
        <?php if ($comment) { ?>
        <table class="table table-bordered">
          <thead>
            <tr>
              <td><?php echo $text_comment; ?></td>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><?php echo $comment; ?></td>
            </tr>
          </tbody>
        </table>
        <?php } ?>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-comment-o"></i> <?php echo $text_history; ?></h3>
      </div>
      <div class="panel-body">
        <ul class="nav nav-tabs">
          <li class="active"><a href="#tab-history" data-toggle="tab"><?php echo $tab_history; ?></a></li>
          <li><a href="#tab-additional" data-toggle="tab"><?php echo $tab_additional; ?></a></li>
          <?php foreach ($tabs as $tab) { ?>
          <li><a href="#tab-<?php echo $tab['code']; ?>" data-toggle="tab"><?php echo $tab['title']; ?></a></li>
          <?php } ?>
        </ul>
        <div class="tab-content">
          <div class="tab-pane active" id="tab-history">
            <div id="history"></div>
            <br />
            <fieldset>
              <legend><?php echo $text_history_add; ?></legend>
              <form class="form-horizontal">
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="input-order-status"><?php echo $entry_order_status; ?></label>
                  <div class="col-sm-10">
                    <select name="order_status_id" id="input-order-status" class="form-control">
                      <?php foreach ($order_statuses as $order_statuses) { ?>
                      <?php if ($order_statuses['order_status_id'] == $order_status_id) { ?>
                      <option value="<?php echo $order_statuses['order_status_id']; ?>" selected="selected"><?php echo $order_statuses['name']; ?></option>
                      <?php } else { ?>
                      <option value="<?php echo $order_statuses['order_status_id']; ?>"><?php echo $order_statuses['name']; ?></option>
                      <?php } ?>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="input-override"><span data-toggle="tooltip" title="<?php echo $help_override; ?>"><?php echo $entry_override; ?></span></label>
                  <div class="col-sm-10">
                    <input type="checkbox" name="override" value="1" id="input-override" />
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="input-notify"><?php echo $entry_notify; ?></label>
                  <div class="col-sm-10">
                    <input type="checkbox" name="notify" value="1" id="input-notify" />
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="input-comment"><?php echo $entry_comment; ?></label>
                  <div class="col-sm-10">
                    <textarea name="comment" rows="8" id="input-comment" class="form-control"></textarea>
                  </div>
                </div>
              </form>
            </fieldset>
            <div class="text-right">
              <button id="button-history" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i> <?php echo $button_history_add; ?></button>
            </div>
          </div>
          <div class="tab-pane" id="tab-additional">
            <?php if ($account_custom_fields) { ?>
            <div class="table-responsive">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <td colspan="2"><?php echo $text_account_custom_field; ?></td>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($account_custom_fields as $custom_field) { ?>
                  <tr>
                    <td><?php echo $custom_field['name']; ?></td>
                    <td><?php echo $custom_field['value']; ?></td>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
            <?php } ?>
            <?php if ($payment_custom_fields) { ?>
            <div class="table-responsive">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <td colspan="2"><?php echo $text_payment_custom_field; ?></td>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($payment_custom_fields as $custom_field) { ?>
                  <tr>
                    <td><?php echo $custom_field['name']; ?></td>
                    <td><?php echo $custom_field['value']; ?></td>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
            <?php } ?>
            <?php if ($shipping_method && $shipping_custom_fields) { ?>
            <div class="table-responsive">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <td colspan="2"><?php echo $text_shipping_custom_field; ?></td>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($shipping_custom_fields as $custom_field) { ?>
                  <tr>
                    <td><?php echo $custom_field['name']; ?></td>
                    <td><?php echo $custom_field['value']; ?></td>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
            <?php } ?>
            <div class="table-responsive">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <td colspan="2"><?php echo $text_browser; ?></td>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td><?php echo $text_ip; ?></td>
                    <td><?php echo $ip; ?></td>
                  </tr>
                  <?php if ($forwarded_ip) { ?>
                  <tr>
                    <td><?php echo $text_forwarded_ip; ?></td>
                    <td><?php echo $forwarded_ip; ?></td>
                  </tr>
                  <?php } ?>
                  <tr>
                    <td><?php echo $text_user_agent; ?></td>
                    <td><?php echo $user_agent; ?></td>
                  </tr>
                  <tr>
                    <td><?php echo $text_accept_language; ?></td>
                    <td><?php echo $accept_language; ?></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <?php foreach ($tabs as $tab) { ?>
          <div class="tab-pane" id="tab-<?php echo $tab['code']; ?>"><?php echo $tab['content']; ?></div>
          <?php } ?>
        </div>
      </div>
    </div>
  </div>


  <div class="modal fade" id="modalComplete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
        </div>
        <div class="modal-body" id="complete-modal" style="text-align: center;">

        </div>
        <div class="modal-footer" style="text-align:left;">
          <button type="button" class="btn btn-md btn-primary" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">Kapat</span>
          </button>
            <button data-dismiss="modal" aria-label="Close" type="button" class="yazdir btn btn-md btn-primary pull-right" onclick="printDiv('complete-modal')">Yazdır</button>
        </div>
      </div>
    </div>
  </div>          


  <script type="text/javascript"><!--
$(document).delegate('#button-ip-add', 'click', function() {
	$.ajax({
		url: 'index.php?route=user/api/addip&token=<?php echo $token; ?>&api_id=<?php echo $api_id; ?>',
		type: 'post',
		data: 'ip=<?php echo $api_ip; ?>',
		dataType: 'json',
		beforeSend: function() {
			$('#button-ip-add').button('loading');
		},
		complete: function() {
			$('#button-ip-add').button('reset');
		},
		success: function(json) {
			$('.alert').remove();

			if (json['error']) {
				$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
			}

			if (json['success']) {
				$('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$(document).delegate('#button-invoice', 'click', function() {
	$.ajax({
		url: 'index.php?route=sale/order/createinvoiceno&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>',
		dataType: 'json',
		beforeSend: function() {
			$('#button-invoice').button('loading');
		},
		complete: function() {
			$('#button-invoice').button('reset');
		},
		success: function(json) {
			$('.alert').remove();

			if (json['error']) {
				$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}

			if (json['invoice_no']) {
				$('#invoice').html(json['invoice_no']);

				$('#button-invoice').replaceWith('<button disabled="disabled" class="btn btn-success btn-xs"><i class="fa fa-cog"></i></button>');
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$(document).delegate('#button-reward-add', 'click', function() {
	$.ajax({
		url: 'index.php?route=sale/order/addreward&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>',
		type: 'post',
		dataType: 'json',
		beforeSend: function() {
			$('#button-reward-add').button('loading');
		},
		complete: function() {
			$('#button-reward-add').button('reset');
		},
		success: function(json) {
			$('.alert').remove();

			if (json['error']) {
				$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}

			if (json['success']) {
                $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

				$('#button-reward-add').replaceWith('<button id="button-reward-remove" data-toggle="tooltip" title="<?php echo $button_reward_remove; ?>" class="btn btn-danger btn-xs"><i class="fa fa-minus-circle"></i></button>');
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$(document).delegate('#button-reward-remove', 'click', function() {
	$.ajax({
		url: 'index.php?route=sale/order/removereward&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>',
		type: 'post',
		dataType: 'json',
		beforeSend: function() {
			$('#button-reward-remove').button('loading');
		},
		complete: function() {
			$('#button-reward-remove').button('reset');
		},
		success: function(json) {
			$('.alert').remove();

			if (json['error']) {
				$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}

			if (json['success']) {
                $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

				$('#button-reward-remove').replaceWith('<button id="button-reward-add" data-toggle="tooltip" title="<?php echo $button_reward_add; ?>" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i></button>');
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$(document).delegate('#button-commission-add', 'click', function() {
	$.ajax({
		url: 'index.php?route=sale/order/addcommission&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>',
		type: 'post',
		dataType: 'json',
		beforeSend: function() {
			$('#button-commission-add').button('loading');
		},
		complete: function() {
			$('#button-commission-add').button('reset');
		},
		success: function(json) {
			$('.alert').remove();

			if (json['error']) {
				$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}

			if (json['success']) {
                $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

				$('#button-commission-add').replaceWith('<button id="button-commission-remove" data-toggle="tooltip" title="<?php echo $button_commission_remove; ?>" class="btn btn-danger btn-xs"><i class="fa fa-minus-circle"></i></button>');
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$(document).delegate('#button-commission-remove', 'click', function() {
	$.ajax({
		url: 'index.php?route=sale/order/removecommission&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>',
		type: 'post',
		dataType: 'json',
		beforeSend: function() {
			$('#button-commission-remove').button('loading');
		},
		complete: function() {
			$('#button-commission-remove').button('reset');
		},
		success: function(json) {
			$('.alert').remove();

			if (json['error']) {
				$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}

			if (json['success']) {
                $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

				$('#button-commission-remove').replaceWith('<button id="button-commission-add" data-toggle="tooltip" title="<?php echo $button_commission_add; ?>" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i></button>');
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

var token = '';

// Login to the API
$.ajax({
	url: '<?php echo $catalog; ?>index.php?route=api/login',
	type: 'post',
	dataType: 'json',
	data: 'key=<?php echo $api_key; ?>',
	crossDomain: true,
	success: function(json) {
		$('.alert').remove();

        if (json['error']) {
    		if (json['error']['key']) {
    			$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['key'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
    		}

            if (json['error']['ip']) {
    			$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['ip'] + ' <button type="button" id="button-ip-add" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-danger btn-xs pull-right"><i class="fa fa-plus"></i> <?php echo $button_ip_add; ?></button></div>');
    		}
        }

        if (json['token']) {
			token = json['token'];
		}
	},
	error: function(xhr, ajaxOptions, thrownError) {
		alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
	}
});

$('#history').delegate('.pagination a', 'click', function(e) {
	e.preventDefault();

	$('#history').load(this.href);
});

$('#history').load('index.php?route=sale/order/history&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>');

$('#button-history').on('click', function() {
	/*
	if (typeof verifyStatusChange == 'function'){
		if (verifyStatusChange() == false){
			return false;
		} else{
			addOrderInfo();
		}
	} else{
		addOrderInfo();
	}*/

	$.ajax({
		url: '<?php echo $catalog; ?>index.php?route=api/order/history&token=' + token + '&store_id=<?php echo $store_id; ?>&order_id=<?php echo $order_id; ?>',
		type: 'post',
		dataType: 'json',
		data: 'order_status_id=' + encodeURIComponent($('select[name=\'order_status_id\']').val()) + '&notify=' + ($('input[name=\'notify\']').prop('checked') ? 1 : 0) + '&override=' + ($('input[name=\'override\']').prop('checked') ? 1 : 0) + '&append=' + ($('input[name=\'append\']').prop('checked') ? 1 : 0) + '&comment=' + encodeURIComponent($('textarea[name=\'comment\']').val()),
		beforeSend: function() {
			$('#button-history').button('loading');
		},
		complete: function() {
			$('#button-history').button('reset');
		},
		success: function(json) {
			$('.alert').remove();

			if (json['error']) {
				$('#history').before('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
			}

			if (json['success']) {
				$('#history').load('index.php?route=sale/order/history&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>');

				$('#history').before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');

				$('textarea[name=\'comment\']').val('');
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

function changeStatus(){
	var status_id = $('select[name="order_status_id"]').val();

	$('#openbay-info').remove();

	$.ajax({
		url: 'index.php?route=extension/openbay/getorderinfo&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>&status_id=' + status_id,
		dataType: 'html',
		success: function(html) {
			$('#history').after(html);
		}
	});
}

function addOrderInfo(){
	var status_id = $('select[name="order_status_id"]').val();

	$.ajax({
		url: 'index.php?route=extension/openbay/addorderinfo&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>&status_id=' + status_id,
		type: 'post',
		dataType: 'html',
		data: $(".openbay-data").serialize()
	});
}

$(document).ready(function() {
	changeStatus();
});

$('select[name="order_status_id"]').change(function(){
	changeStatus();
});
//--></script>
</div>








        <script>

          $('#edit'+button.data('whatever')+'Modal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) // Button that triggered the modal
            var recipient = button.data('whatever') // Extract info from data-* attributes
            var modal = $(this)
            modal.find('.modal-title').text(recipient + ' Düzenle')
            modal.find('.modal-body input').val(recipient)
          })

        </script>



        <script>
        var order_status_sequence = [];
          $(':checkbox').change(function(){
            var status_ids = [];
            var index_array = [];

            $(".selection:checked").each(function(){
              status_ids.push($(this).attr('data-status-id'));
            });

            if (status_ids.length > 0) {
              $("#order_status_id").children('option').remove();
              $.each(order_status_sequence,function(index,value){
                $.each(status_ids,function(i,v){
                   if (value == v) {
                      index_array.push(index);
                   }
                });
              });

              max_change_status_id = Math.max.apply(null, index_array);
              changeOrderStatusSequence(max_change_status_id);
            }
          });

        function changeOrderStatusSequence (max_change_status_id){
            var final_order_sequence = [];
            $.each(order_status_sequence,function(index,value){
              if (index >= max_change_status_id) {
                  final_order_sequence.push(value);
              }
            });

        }
        </script>

        <script>

              </script>
                <script type= "text/javascript">
                  function showPrintModal(){
                  $('.print-shipping-label').on('click', function(e) {
                    var p_ids = [];
                    var success = false;
                    var formaction = $(this).data('formaction');
                    $.ajax({
                      url : formaction,
                      type: 'post',
                      dataType: 'json',
                      data: '',
                      beforeSend : function() {
                        //$('#print-shipping-label').button('loading');
                        //$('#content > .container-fluid').prepend('<center><div class="loader text-center" id="loader"></div></center>');
                        //$('button').addClass('disabled');
                      },
                      success : function(json) {

                      p_ids = [];
                      $('input[type*=\'checkbox\']').prop('checked',false);
                      $('.alert').remove();

                        if (json['warning']) {
                          html = '';
                          html += '<div class ="alert alert-warning"><i class="fa fa-exclamation-circle"></i>' + json['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>'
                          $('#alert').append(html);
                        }

                        if (json['error']) {

                          html = '';
                          html += '<div class ="alert alert-danger"><i class="fa fa-exclamation-circle"></i>' + json['error'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>'
                          $('#alert').append(html);
                        } else {
                          success = true;
                        }
                      },
                      complete : function(json) {

                        $('#print-shipping-label').button('reset');
                        $('input[type*=\'checkbox\']').prop('checked',false);
                        $('.loader').addClass('hide');
                          $('button').removeClass('disabled');

                          if(json.responseJSON.document){
                            $('#complete-modal').html(json.responseJSON.document);


                            $('#modalComplete').modal('show');

                            $('#print-label').addClass('hide');
                            $('#download-label').addClass('hide');

                        } else {
                          $('#print-label').addClass('hide');
                          $('#download-label').addClass('hide');
                        }
                      }
                    });
                  });
                  }

showPrintModal();


                                  $('#generate-shipping-label').on('click', function(e) {
                                    var p_ids = [];
                                    var success = false;

                                  $(".selection:checked").each(function() {
                                    p_ids.push($(this).val());
                                  });
                                    $.ajax({
                                      url : 'index.php?route=extension/shipping/shipping&order_id=<?php echo $order_id; ?>&token=<?php echo $token; ?>',
                                      type: 'post',
                                      dataType: 'json',
                                      data: 'product_ids='+p_ids,
                                      beforeSend : function() {
                                        //$('#generate-shipping-label').button('loading');
                                        //$('#content > .container-fluid').prepend('<center><div class="loader text-center" id="loader"></div></center>');
                                        //$('button').addClass('disabled');
                                      },
                                      success : function(json) {

                                      p_ids = [];
                                      $('input[type*=\'checkbox\']').prop('checked',false);
                                      $('.alert').remove();

                                        if (json['warning']) {
                                          html = '';
                                          html += '<div class ="alert alert-warning"><i class="fa fa-exclamation-circle"></i>' + json['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>'
                                          $('#alert').append(html);
                                        }

                                        if (json['error']) {

                                          html = '';
                                          html += '<div class ="alert alert-danger"><i class="fa fa-exclamation-circle"></i>' + json['error'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>'
                                          $('#alert').append(html);
                                        } else {
                                          success = true;
                                        }
                                      },
                                      complete : function(json) {

                                        $('#generate-shipping-label').button('reset');
                                        $('input[type*=\'checkbox\']').prop('checked',false);
                                        $('.loader').addClass('hide');
                                          $('button').removeClass('disabled');

                                          if(json.responseJSON.document){
                                            $('#complete-modal').html(json.responseJSON.document);

                                            $('#modalComplete').modal('show');

                                            $('#print-label').addClass('hide');
                                            $('#download-label').addClass('hide');
                                          }
                                        else if (success) {

                                            html=`
                                            <div>
                                              ${json.responseJSON.cargos.map(function(elem){
                                                console.log(elem)
                                                return `
                                                <div class="cargo-col" style="">
                                                  <div onclick="cargoManager.getCargoInfo(${JSON.stringify(json).split('"').join("&quot;")}, '${typeof elem.cargokey == "undefined" ? "ozlkrg" : elem.cargokey }');" >
                                                    <img src="image/cargo/${elem.image}" style="max-width: 150px; max-height:150px; padding: 5px;">
                                                  </div>
                                                <br>
                                                </div>`;
                                              }).join('')}
                                            </div><div style="clear:both;"></div>
                                            `;

                                          $('#complete-modal').html(html);
                                          //$('#complete-modal').html(JSON.stringify(json));


                                          $('#modalComplete').modal('show');
                                          //window.location.reload();
                                        } else {
                                          $('#print-label').addClass('hide');
                                          $('#download-label').addClass('hide');
                                        }
                                      }
                                    });
                                  });

                                  function cancelShipment(elem,referans){
                                  var wrapperId= $(elem).parents('td').attr('id').split('_')[1];

                                    $.ajax({
                                      url : 'index.php?route=extension/shipping/shipping/cancelShipment&referans='+referans,
                                      type: 'post',
                                      dataType: 'json',

                                      beforeSend : function() {

                                      },
                                      success : function(json) {

                                        if (json['warning']) {
                                          html = '';
                                          html += '<div class ="alert alert-warning"><i class="fa fa-exclamation-circle"></i>' + json['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>'
                                          $('#alert').append(html);
                                        }
                                        if (json['success']) {

                                        $(elem).parents('.order-desc__wrapper').attr('rowspan',1);
                                        $('.group_'+wrapperId).find('.order-desc__wrapper').show();
                                        $(elem).parents('.order-desc').html('');
                                        console.log($(elem).parents())
                                         $("html, body").animate({ scrollTop: 0 }, 600);
                                        }

                                      },
                                      complete : function(json) {

                                      }
                                    });
                                  }

                                  var cargoManager = {
                                    getCargoInfo:function(cargo, cargokey){
                                      var parsedData = cargo.responseJSON;
                                      var otherProductKeys = Object.keys(parsedData.otherProduct);

                                      $.ajax({
                                        url : `index.php?route=extension/shipping/shipping&cargokey=${cargokey}&order_id=${parsedData.order_id}`,
                                        type: 'post',
                                        dataType: 'json',
                                        data: {
                                          product_ids:parsedData.otherProduct
                                        },
                                        beforeSend : function() {
                                        $('#cargoLoading').css('display','flex');
                                        },
                                        complete:function(){
                                        $('#cargoLoading').css('display','none');
                                        },
                                        success : function(json) {
                                        $('#cargoLoading').css('display','none');

                                        if(json.success == false){
                                        console.log(json.error)
                                        $('.alert').remove();
                                            html = '';
                                            html += '<div class ="alert alert-danger"><i class="fa fa-exclamation-circle"></i>' + json.error + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>'
                                            $('#alert').append(html);
                                            $('#modalComplete').modal('hide')
                                        }else{



                                        $('#odwrapper_'+otherProductKeys[0]).attr('rowspan',otherProductKeys.length);

                                          var cargoButtonsHtml = '<div class="order-desc">';
                                          cargoButtonsHtml += '<span style="color:green;margin-bottom:10px;"><i class="fa fa-truck" aria-hidden="true" style="border:3px solid green; color:green;padding:3px; border-radius:50px;"></i>&nbsp;'+json.cargo_title+'</span>';
                                          cargoButtonsHtml += '<button type="button" data-toggle="tooltip" class="btn btn-info print-shipping-label" data-formaction="index.php?route=extension/shipping/shipping&order_id=<?php echo $order_id; ?>&shipment_id='+json.referans+'">&nbsp;&nbsp;Kargo Etiketini Görüntüle</button>';
                                          cargoButtonsHtml += '<button class="cancel-shipping btn btn-md btn-danger" data-dismiss="modal" aria-label="Cancel" type="button" onclick="cancelShipment(this,\''+json.referans+'\')"><span aria-hidden="true">Gönderiyi İptal Et</span></button>';
                                          cargoButtonsHtml += '</div>';
                                          otherProductKeys.forEach(function(item,index){
                                          $('#odwrapper_'+item).parents('tr').addClass('group_'+otherProductKeys[0]);
                                            if(index > 0){
                                                $('#odwrapper_'+item).hide();
                                            }
                                          })
                                          $('#odwrapper_'+otherProductKeys[0]).html(cargoButtonsHtml);

                                          showPrintModal();


                                        $('#complete-modal').html(json.document);

                                        $('#modalComplete').modal('show');
                                        }

                                          console.log(json);

                                        },
                                        complete : function(json) {

                                        }
                                      });
                                    }

                                  }

                              </script>
                              <style>
                                .loader {
                                    position: fixed;
                                    z-index: 999;
                                    overflow: visible;
                                    margin: auto;
                                    top: 0;
                                    left: 0;
                                    bottom: 0;
                                    right: 0;
                                    border: 10px solid #f3f3f3; /* Light grey */
                                    border-top: 10px solid #3498db; /* Blue */
                                    border-radius: 50%;
                                    width: 80px;
                                    height: 80px;
                                    animation: spin 2s linear infinite;
                                }

                                @keyframes spin {
                                    0% { transform: rotate(0deg); }
                                    100% { transform: rotate(360deg); }
                                }
                                .disabled {
                                  pointer-events: none;
                                  cursor: default;
                                }
                            </style>

<style type="text/css">
  .order-info-buttons{
    background-color: blue;
    padding: 10px;
  }
</style>


        <script type="text/javascript">
          $(document).ready(function () {
            $('.print-barcode').on('click',function(){
              var order_id = $(this).attr('data-order-id');
              $.get('index.php?route=account/customerpartner/orderinfo/gonderikodu&order_id='+order_id, function (response) {
                $('#send').text('Gönderi Oluştur');
                $('#send').removeAttr('disabled');
                response = JSON.parse(response);
                if(response.status == 'success'){
                  function addZero(i) {
                    if (i < 10) {
                      i = "0" + i;
                    } return i;
                  }

                  var completeModalHTML = '';

                  completeModalHTML += '<h5>Kargo Kodu</h5>';
                  completeModalHTML += '<strong>' + response.orderid + '</strong>';
                  completeModalHTML += '<p>' + response.barcode + '</p>';
                  completeModalHTML += '<br>';

                  var date = new Date();
                  var h = addZero(date.getHours());
                  var m = addZero(date.getMinutes());
                  var s = addZero(date.getSeconds());

                  var date =  date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear() + " " + h + ":" + m + ":" + s;

                  completeModalHTML += '<h5>Etiket Tarihi</h5>';
                  completeModalHTML += '<strong>' + date + '</strong>';
                  completeModalHTML += '<br><br>';

                  var sellerName    = response.order_details.seller_name + ' ' + response.order_details.seller_lastname;
                  var sellerPhone   = response.order_details.seller_telephone;
                  var sellerAddress = response.order_details.seller_address;

                  completeModalHTML += '<h5>Satıcı Bilgileri</h5>';
                  completeModalHTML += '<span>' + sellerName    + '</span>';
                  completeModalHTML += '<br>';
                  completeModalHTML += '<span>' + sellerPhone   + '</span>';
                  completeModalHTML += '<br>';
                  completeModalHTML += '<span style="font-style: italic;">' + sellerAddress + '</span>';
                  completeModalHTML += '<br><br>';

                  var customerName = response.order_details.shipping_firstname + ' ' + response.order_details.shipping_lastname;
                  var customerCity = response.order_details.shipping_zone;
                  var customerZone = response.order_details.shipping_country;
                  var customerTelephone = response.order_details.telephone;

                  completeModalHTML += '<h5>Alıcı Bilgileri</h5>';
                  completeModalHTML += '<strong>' + customerName + '</strong>';
                  //completeModalHTML += '<p>' + customerTelephone + '</p>';
                  completeModalHTML += '<p>' + response.order_details.shipping_address_1 + '</p>';
                  completeModalHTML += '<p>' + customerCity + ' / ' + customerZone + '</p>';
                  completeModalHTML += '<br>';

                  // Ürünler


                  $('#complete-modal').html(completeModalHTML);

                  $('#modalComplete').modal('show');
                }else{
                  alert(response.message);
                }
              });
            })
          })
          function printDiv(divName)
          {
            var divToPrint=document.getElementById(divName);

            var newWin=window.open('','Print-Window');

            newWin.document.open();

            newWin.document.write('<html><style>@media print {\n'+
        '    .print div div {\n'+
        '        background-color: #000000 !important;\n'+
        '        -webkit-print-color-adjust: exact; \n'+
        '    }\n'+
        '}\n' +
                    '\n' +
                    '@media print {\n'+
        '    .print div div {\n'+
        '        color: white !important;\n'+
        '    }\n'+
        '}table th, table td { border:1px solid #000;padding;0.5em;}</style> <body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

            newWin.document.close();

            setTimeout(function(){newWin.close();},10);

          }
        </script>




<?php echo $footer; ?>
