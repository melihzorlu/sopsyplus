<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
<div class="page-header">
  <div class="container-fluid">
    <div class="pull-right">
      <?php if ($apply_btn) { ?>
      <a onclick="$('#apply').val('1'); $('#form-yomenu').submit();" class="btn btn-success" data-toggle="tooltip" title="<?php echo $button_apply; ?>" role="button"><i class="fa fa-check"></i> <span class="hidden-sm"> <?php echo $button_apply; ?></span></a>
      <?php } ?>
      <button type="submit" form="form-yomenu" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i> <span class="hidden-sm"> <?php echo $button_save; ?></span></button>
      <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
    <h1><?php echo $heading_title; ?></h1>
    <ul class="breadcrumb">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
      <?php } ?>
    </ul>
  </div>
</div>
<div class="container-fluid">
  <?php if ($error_warning) { ?>
  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title"><i class="fa fa-pencil-square-o"></i>
      <?php echo $text_edit; ?>
      <?php if (!empty($name)) { ?>
      <?php echo '"'. $name .'"'; ?>
      <?php } ?>
      </h3>
      <?php if ($success) { ?>
      <div class="yo-apply text-success pull-right"><i class="fa fa-check"></i> <?php echo $success; ?></div>
      <?php } ?>
      <?php if ($error_name) { ?>
      <div class="text-danger pull-right"><strong><?php echo $error_name; ?></strong></div>
      <?php } ?>
    </div>
    <div class="panel-body">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-yomenu" class="form-horizontal">
        <ul id="settingTab" class="nav nav-tabs" role="tablist">
          <li class="active"><a href="#menuSetting" role="tab" data-toggle="tab"> <?php echo $tab_menu_setting; ?></a></li>
          <li><a href="#moduleSetting" role="tab" data-toggle="tab"> <?php echo $tab_module_setting; ?></a></li>
        </ul>
        <div class="tab-content">
          <div id="menuSetting" class="tab-pane active">
            <div class="row">
              <div class="col-lg-6">
                <div class="form-group">
                  <label class="col-sm-4 control-label"><?php echo $entry_title; ?></label>
                  <div class="col-sm-8">
                    <?php foreach ($languages as $language) { ?>
                    <div class="input-group"> <span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                      <input type="text" name="yomenu[<?php echo $language['language_id']; ?>][title]" value="<?php echo isset($yomenu[$language['language_id']]['title']) ? $yomenu[$language['language_id']]['title'] : ''; ?>" class="form-control" />
                    </div>
                    <?php } ?>
                  </div>
                </div>
                <hr class="hidden-lg">
              </div>
              <div class="col-lg-6">
                <div class="form-group">
                  <label class="col-sm-4 control-label"><?php echo $entry_minimized; ?></label>
                  <div class="col-sm-8">
                    <div class="btn-group btn-group-justified" data-toggle="buttons">
                      <label class="btn btn-success">
                        <input type="radio" name="yomenu[minimized]" value="1" <?php echo isset($yomenu['minimized']) ? 'checked="checked"' : ''; ?> />
                        <?php echo $text_minimized; ?> </label>
                      <label class="btn btn-success">
                        <input type="radio" name="yomenu[minimized]" value="0" <?php echo empty($yomenu['minimized']) ? 'checked="checked"' : ''; ?> />
                        <?php echo $text_expanded; ?> </label>
                    </div>
                    <div class="checkbox">
                      <label>
                        <input type="checkbox" name="yomenu[save_view]" value="1" <?php echo isset($yomenu['save_view']) ? 'checked="checked"' : ''; ?> />
                        <?php echo $text_save_view; ?> </label>
                    </div>
                  </div>
                </div>
                <hr class="hidden-lg">
              </div>
            </div>
            <div class="row">
              <div class="col-lg-6">
                <hr class="visible-lg">
                <div class="form-group">
                  <label class="col-sm-4 control-label"><?php echo $entry_menu_items; ?></label>
                  <div class="col-sm-4">
                    <div class="radio">
                      <label>
                        <input type="radio" name="yomenu[menu_type]" value="tree" <?php echo empty($yomenu['menu_type']) || $yomenu['menu_type'] == 'tree' ? 'checked="checked"' : ''; ?> onchange="if($(this).prop('checked')) {$('#tree-config').show().siblings('.menu-type').hide();}" />
                        <?php echo $text_tree; ?> </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input type="radio" name="yomenu[menu_type]" value="brands" <?php echo isset($yomenu['menu_type']) && $yomenu['menu_type'] == 'brands' ? 'checked="checked"' : ''; ?> onchange="if($(this).prop('checked')) {$('#brands-config').show().siblings('.menu-type').hide();}" />
                        <?php echo $text_brands; ?> </label>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="radio">
                      <label>
                        <input type="radio" name="yomenu[menu_type]" value="current" <?php echo isset($yomenu['menu_type']) && $yomenu['menu_type'] == 'current' ? 'checked="checked"' : ''; ?> onchange="if($(this).prop('checked')) {$('#current-config').show().siblings('.menu-type').hide();}" />
                        <?php echo $text_current; ?> </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input type="radio" name="yomenu[menu_type]" value="parent" <?php echo isset($yomenu['menu_type']) && $yomenu['menu_type'] == 'parent' ? 'checked="checked"' : ''; ?> onchange="if($(this).prop('checked')) {$('#current-config').show().siblings('.menu-type').hide();}" />
                        <?php echo $text_parent; ?> </label>
                    </div>
                  </div>
                </div>
                <hr>
                <div id="tree-config" <?php echo empty($yomenu['menu_type']) || $yomenu['menu_type'] == 'tree' ? '' : 'style="display:none;"' ; ?> class="menu-type">
                  <div class="form-group">
                    <label class="col-sm-4 control-label"><?php echo $entry_categories; ?></label>
                    <div class="col-sm-8">
                      <div class="radio">
                        <label>
                          <input type="radio" name="yomenu[all_categories]" value="1" <?php echo isset($yomenu['all_categories']) ? 'checked="checked"' : ''; ?> onchange="if($(this).prop('checked')) {$('#featured-categories').hide();}" />
                          <?php echo $text_all_categories; ?> </label>
                      </div>
                      <div class="radio">
                        <label>
                          <input type="radio" name="yomenu[all_categories]" value="0" <?php echo empty($yomenu['all_categories']) ? 'checked="checked"' : ''; ?> onchange="if($(this).prop('checked')) {$('#featured-categories').show();}" />
                          <?php echo $text_current_categories; ?> </label>
                      </div>
                    </div>
                  </div>
                  <div id="featured-categories" <?php echo !empty($yomenu['all_categories']) ? 'style="display:none;"' : ''; ?>>
                    <div class="form-group">
                      <div class="col-sm-8 col-sm-offset-4">
                        <input type="text" name="yomenu[featured_category]" value="" placeholder="<?php echo $text_current_categories; ?>" class="form-control" />
                        <div id="yomenu-featured-category" class="well well-sm" style="height: 150px; overflow: auto; margin-bottom: 0;">
                          <?php foreach ($categories as $category) { ?>
                          <div id="yomenu-featured-category<?php echo $category['category_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $category['name']; ?>
                            <input type="hidden" name="yomenu[featured_category][]" value="<?php echo $category['category_id']; ?>" />
                          </div>
                          <?php } ?>
                        </div>
                      </div>
                    </div>
                  </div>
                  <hr>
                  <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-4">
                      <div class="checkbox">
                        <label>
                          <input type="checkbox" name="yomenu[count]" value="1" <?php echo isset($yomenu['count']) ? 'checked="checked"' : ''; ?> />
                          <?php echo $text_count; ?> </label>
                      </div>
                    </div>
                  </div>
                  <hr>
                  <div class="form-group">
                    <label class="col-sm-4 control-label"><?php echo $entry_levels; ?></label>
                    <div class="col-sm-4">
                      <div class="checkbox">
                        <label>
                          <input type="checkbox" name="yomenu[level_1]" value="1" checked="checked" disabled />
                          <?php echo $text_level_1; ?> </label>
                      </div>
                      <div class="checkbox">
                        <label>
                          <?php if (!empty($yomenu['level_2'])) { ?>
                          <input type="checkbox" name="yomenu[level_2]" value="1" checked="checked" />
                          <?php echo $text_level_2; ?>
                          <?php } else { ?>
                          <input type="checkbox" name="yomenu[level_2]" value="1" />
                          <?php echo $text_level_2; ?>
                          <?php } ?>
                        </label>
                      </div>
                      <div class="checkbox">
                        <label>
                          <?php if (!empty($yomenu['level_3'])) { ?>
                          <input type="checkbox" name="yomenu[level_3]" value="1" checked="checked" />
                          <?php echo $text_level_3; ?>
                          <?php } else { ?>
                          <input type="checkbox" name="yomenu[level_3]" value="1" />
                          <?php echo $text_level_3; ?>
                          <?php } ?>
                        </label>
                      </div>
                    </div>
                    <div class="col-sm-4">
                      <div class="checkbox">
                        <label>
                          <?php if (!empty($yomenu['level_4'])) { ?>
                          <input type="checkbox" name="yomenu[level_4]" value="1" checked="checked" />
                          <?php echo $text_level_4; ?>
                          <?php } else { ?>
                          <input type="checkbox" name="yomenu[level_4]" value="1" />
                          <?php echo $text_level_4; ?>
                          <?php } ?>
                        </label>
                      </div>
                      <div class="checkbox">
                        <label>
                          <?php if (!empty($yomenu['level_5'])) { ?>
                          <input type="checkbox" name="yomenu[level_5]" value="1" checked="checked" />
                          <?php echo $text_level_5; ?>
                          <?php } else { ?>
                          <input type="checkbox" name="yomenu[level_5]" value="1" />
                          <?php echo $text_level_5; ?>
                          <?php } ?>
                        </label>
                      </div>
                    </div>
                  </div>
                  <hr>
                  <div class="form-group">
                    <label class="col-sm-4 control-label"><?php echo $entry_sub_limit; ?></label>
                    <div class="col-sm-4">
                      <input type="text" name="yomenu[subcat_limit]" value="<?php echo !empty($yomenu['subcat_limit']) ? $yomenu['subcat_limit'] : ''; ?>" class="form-control" />
                    </div>
                  </div>
                </div>
                <div id="current-config" <?php echo isset($yomenu['menu_type']) && $yomenu['menu_type'] == 'current' || isset($yomenu['menu_type']) && $yomenu['menu_type'] == 'parent' ? '' : 'style="display:none;"' ; ?> class="menu-type">
                  <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-4">
                      <div class="checkbox">
                        <label>
                          <input type="checkbox" name="yomenu[current_count]" value="1" <?php echo isset($yomenu['current_count']) ? 'checked="checked"' : ''; ?> />
                          <?php echo $text_count; ?> </label>
                      </div>
                    </div>
                  </div>
                  <hr>
                  <div class="form-group">
                    <label class="col-sm-4 control-label"><?php echo $entry_prod_by_cat; ?></label>
                    <div class="col-sm-8">
                      <div class="btn-group btn-group-justified" data-toggle="buttons">
                        <label class="btn btn-success">
                          <input type="radio" name="yomenu[products_by_category]" value="1" <?php echo isset($yomenu['products_by_category']) ? 'checked="checked"' : ''; ?> onchange="if($(this).prop('checked')) {$('#current-products-config').show(); $('#current-levels-config').hide();}" />
                          <?php echo $text_enabled; ?> </label>
                        <label class="btn btn-danger">
                          <input type="radio" name="yomenu[products_by_category]" value="0" <?php echo empty($yomenu['products_by_category']) ? 'checked="checked"' : ''; ?> onchange="if($(this).prop('checked')) {$('#current-products-config').hide(); $('#current-levels-config').show();}" />
                          <?php echo $text_disabled; ?> </label>
                      </div>
                    </div>
                  </div>
                  <hr>
                  <div id="current-products-config" <?php echo !empty($yomenu['products_by_category']) ? '' : 'style="display:none;"' ; ?>>
                    <div class="form-group">
                      <label class="col-sm-4 control-label"><?php echo $entry_sort_order; ?></label>
                      <div class="col-sm-8">
                        <select name="yomenu[products_sort]" class="form-control">
                          <?php foreach ($sorts as $sort) { ?>
                          <?php if (isset($yomenu['products_sort']) && $sort['value'] == $yomenu['products_sort']) { ?>
                          <option value="<?php echo $sort['value']; ?>" selected="selected"><?php echo $sort['name']; ?></option>
                          <?php } else { ?>
                          <option value="<?php echo $sort['value']; ?>"><?php echo $sort['name']; ?></option>
                          <?php } ?>
                          <?php } ?>
                        </select>
                      </div>
                    </div>
                    <hr>
                    <div class="form-group">
                      <label class="col-sm-4 control-label"><?php echo $entry_products_limit; ?></label>
                      <div class="col-sm-4">
                        <input type="text" name="yomenu[products_limit]" value="<?php echo isset($yomenu['products_limit']) ? $yomenu['products_limit'] : ''; ?>" class="form-control" />
                      </div>
                    </div>
                  </div>
                  <div id="current-levels-config" <?php echo !empty($yomenu['products_by_category']) ? 'style="display:none;"' : '' ; ?>>
                    <div class="form-group">
                      <label class="col-sm-4 control-label"><?php echo $entry_levels; ?></label>
                      <div class="col-sm-4">
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" name="yomenu[current_level_1]" value="1" checked="checked" disabled />
                            <?php echo $text_level_1; ?> </label>
                        </div>
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" name="yomenu[current_level_2]" value="1" checked="checked" disabled />
                            <?php echo $text_level_2; ?> </label>
                        </div>
                        <div class="checkbox">
                          <label>
                            <?php if (!empty($yomenu['current_level_3'])) { ?>
                            <input type="checkbox" name="yomenu[current_level_3]" value="1" checked="checked" />
                            <?php echo $text_level_3; ?>
                            <?php } else { ?>
                            <input type="checkbox" name="yomenu[current_level_3]" value="1" />
                            <?php echo $text_level_3; ?>
                            <?php } ?>
                          </label>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="checkbox">
                          <label>
                            <?php if (!empty($yomenu['current_level_4'])) { ?>
                            <input type="checkbox" name="yomenu[current_level_4]" value="1" checked="checked" />
                            <?php echo $text_level_4; ?>
                            <?php } else { ?>
                            <input type="checkbox" name="yomenu[current_level_4]" value="1" />
                            <?php echo $text_level_4; ?>
                            <?php } ?>
                          </label>
                        </div>
                        <div class="checkbox">
                          <label>
                            <?php if (!empty($yomenu['current_level_5'])) { ?>
                            <input type="checkbox" name="yomenu[current_level_5]" value="1" checked="checked" />
                            <?php echo $text_level_5; ?>
                            <?php } else { ?>
                            <input type="checkbox" name="yomenu[current_level_5]" value="1" />
                            <?php echo $text_level_5; ?>
                            <?php } ?>
                          </label>
                        </div>
                      </div>
                    </div>
                    <hr>
                    <div class="form-group">
                      <label class="col-sm-4 control-label"><?php echo $entry_sub_limit; ?></label>
                      <div class="col-sm-4">
                        <input type="text" name="yomenu[current_subcat_limit]" value="<?php echo !empty($yomenu['current_subcat_limit']) ? $yomenu['current_subcat_limit'] : ''; ?>" class="form-control" />
                      </div>
                    </div>
                  </div>
                </div>
                <div id="brands-config" <?php echo isset($yomenu['menu_type']) && $yomenu['menu_type'] == 'brands' ? '' : 'style="display:none;"' ; ?> class="menu-type">
                  <div class="form-group">
                    <label class="col-sm-4 control-label"><?php echo $entry_brands; ?></label>
                    <div class="col-sm-8">
                      <div class="radio">
                        <label>
                          <input type="radio" name="yomenu[all_brands]" value="1" <?php echo isset($yomenu['all_brands']) ? 'checked="checked"' : ''; ?> onchange="if($(this).prop('checked')) {$('#featured-brands').hide();}" />
                          <?php echo $text_all_brands; ?> </label>
                      </div>
                      <div class="radio">
                        <label>
                          <input type="radio" name="yomenu[all_brands]" value="0" <?php echo empty($yomenu['all_brands']) ? 'checked="checked"' : ''; ?> onchange="if($(this).prop('checked')) {$('#featured-brands').show();}" />
                          <?php echo $text_current_brands; ?> </label>
                      </div>
                    </div>
                  </div>
                  <div id="featured-brands" <?php echo !empty($yomenu['all_brands']) ? 'style="display:none;"' : ''; ?>>
                    <div class="form-group">
                      <div class="col-sm-8 col-sm-offset-4">
                        <input type="text" name="yomenu[featured_brands]" value="" placeholder="<?php echo $text_current_brands; ?>" class="form-control" />
                        <div id="yomenu-featured-brands" class="well well-sm" style="height: 150px; overflow: auto; margin-bottom: 0;">
                          <?php foreach ($manufacturers as $manufacturer) { ?>
                          <div id="yomenu-featured-brands<?php echo $manufacturer['manufacturer_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $manufacturer['name']; ?>
                            <input type="hidden" name="yomenu[featured_brands][]" value="<?php echo $manufacturer['manufacturer_id']; ?>" />
                          </div>
                          <?php } ?>
                        </div>
                      </div>
                    </div>
                  </div>
                  <hr>
                  <div class="form-group">
                    <label class="col-sm-4 control-label"><?php echo $entry_prod_by_brand; ?></label>
                    <div class="col-sm-8">
                      <div class="btn-group btn-group-justified" data-toggle="buttons">
                        <label class="btn btn-success">
                          <input type="radio" name="yomenu[products_by_brand]" value="1" <?php echo isset($yomenu['products_by_brand']) ? 'checked="checked"' : ''; ?> onchange="if($(this).prop('checked')) {$('#brand-products-config').show();}" />
                          <?php echo $text_enabled; ?> </label>
                        <label class="btn btn-danger">
                          <input type="radio" name="yomenu[products_by_brand]" value="0" <?php echo empty($yomenu['products_by_brand']) ? 'checked="checked"' : ''; ?> onchange="if($(this).prop('checked')) {$('#brand-products-config').hide();}" />
                          <?php echo $text_disabled; ?> </label>
                      </div>
                    </div>
                  </div>
                  <div id="brand-products-config" <?php echo !empty($yomenu['products_by_brand']) ? '' : 'style="display:none;"'; ?>>
                    <hr>
                    <div class="form-group">
                      <label class="col-sm-4 control-label"><?php echo $entry_sort_order; ?></label>
                      <div class="col-sm-8">
                        <select name="yomenu[products_by_brand_sort]" class="form-control">
                          <?php foreach ($sorts as $sort) { ?>
                          <?php if (isset($yomenu['products_by_brand_sort']) && $sort['value'] == $yomenu['products_by_brand_sort']) { ?>
                          <option value="<?php echo $sort['value']; ?>" selected="selected"><?php echo $sort['name']; ?></option>
                          <?php } else { ?>
                          <option value="<?php echo $sort['value']; ?>"><?php echo $sort['name']; ?></option>
                          <?php } ?>
                          <?php } ?>
                        </select>
                      </div>
                    </div>
                    <hr>
                    <div class="form-group">
                      <label class="col-sm-4 control-label"><?php echo $entry_products_limit; ?></label>
                      <div class="col-sm-4">
                        <input type="text" name="yomenu[products_by_brand_limit]" value="<?php echo isset($yomenu['products_by_brand_limit']) ? $yomenu['products_by_brand_limit'] : ''; ?>" class="form-control" />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-6">
                <hr>
                <div class="form-group">
                  <label class="col-sm-4 control-label"><?php echo $entry_menu_design; ?></label>
                  <div class="col-sm-4">
                    <div class="radio">
                      <label>
                        <input type="radio" name="yomenu[menu_design]" value="am" <?php echo empty($yomenu['menu_design']) || $yomenu['menu_design'] == 'am' ? 'checked="checked"' : ''; ?> onchange="if($(this).prop('checked')) {$('#accordion-config').show().siblings('.menu-design').hide();}" />
                        <?php echo $text_am; ?> </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input type="radio" name="yomenu[menu_design]" value="fm" <?php echo isset($yomenu['menu_design']) && $yomenu['menu_design'] == 'fm' ? 'checked="checked"' : ''; ?> onchange="if($(this).prop('checked')) {$('#flyout-config').show().siblings('.menu-design').hide();}" />
                        <?php echo $text_fm; ?> </label>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="radio">
                      <label>
                        <input type="radio" name="yomenu[menu_design]" value="pm" <?php echo isset($yomenu['menu_design']) && $yomenu['menu_design'] == 'pm' ? 'checked="checked"' : ''; ?> onchange="if($(this).prop('checked')) {$('#push-config').show().siblings('.menu-design').hide();}" />
                        <?php echo $text_pm; ?> </label>
                    </div>
                  </div>
                </div>
                <hr>
                <div id="accordion-config" class="menu-design" <?php echo empty($yomenu['menu_design']) || $yomenu['menu_design'] == 'am' ? '' : 'style="display:none;"'; ?>>
                  <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-4">
                      <div class="checkbox">
                        <label>
                          <input type="checkbox" name="yomenu[toggle]" value="1" <?php echo isset($yomenu['toggle']) ? 'checked="checked"' : ''; ?> />
                          <?php echo $text_toggle; ?> </label>
                      </div>
                      <div class="checkbox">
                        <label>
                          <input type="checkbox" name="yomenu[am_icon]" value="1" <?php echo isset($yomenu['am_icon']) ? 'checked="checked"' : ''; ?> />
                          <?php echo $text_icons_status; ?> </label>
                      </div>
                    </div>
                  </div>
                  <hr>
                  <div class="form-group">
                    <label class="col-sm-4 control-label"><?php echo $entry_effect; ?></label>
                    <div class="col-sm-4">
                      <select name="yomenu[easing]" class="form-control">
                        <?php foreach ($easings as $value) { ?>
                        <?php $value = trim($value); ?>
                        <?php if (isset($yomenu['easing']) && $value == $yomenu['easing']) { ?>
                        <option value="<?php echo $value; ?>" selected="selected"><?php echo $value; ?></option>
                        <?php } else { ?>
                        <option value="<?php echo $value; ?>"><?php echo $value; ?></option>
                        <?php } ?>
                        <?php } ?>
                      </select>
                    </div>
                    <div class="col-sm-4">
                      <input type="text" name="yomenu[easing_d]" value="<?php echo !empty($yomenu['easing_d']) ? $yomenu['easing_d'] : '200'; ?>" class="form-control text-center" />
                    </div>
                  </div>
                  <hr>
                </div>
                <div id="flyout-config" class="menu-design" <?php echo isset($yomenu['menu_design']) && $yomenu['menu_design'] == 'fm' ? '' : 'style="display:none;"'; ?>>
                  <div class="form-group">
                    <label class="col-sm-4 control-label"><?php echo $entry_flyout_column; ?></label>
                    <div class="col-sm-8">
                      <div class="radio">
                        <label>
                          <input type="radio" name="yomenu[flyout_design]" value="fm-one" <?php echo empty($yomenu['flyout_design']) || $yomenu['flyout_design'] == 'fm-one' ? 'checked="checked"' : ''; ?> onchange="if($(this).prop('checked')) {$('#one-config').show(); $('#multi-config').hide();}" />
                          <?php echo $text_one_column; ?> </label>
                      </div>
                      <div class="radio">
                        <label>
                          <input type="radio" name="yomenu[flyout_design]" value="fm-multi" <?php echo isset($yomenu['flyout_design']) && $yomenu['flyout_design'] == 'fm-multi' ? 'checked="checked"' : ''; ?> onchange="if($(this).prop('checked')) {$('#one-config').hide(); $('#multi-config').show();}" />
                          <?php echo $text_multi_column; ?> </label>
                      </div>
                    </div>
                  </div>
                  <hr>
                  <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-4">
                      <div class="checkbox">
                        <label>
                          <input type="checkbox" name="yomenu[fm_icon]" value="1" <?php echo isset($yomenu['fm_icon']) ? 'checked="checked"' : ''; ?> />
                          <?php echo $text_icons_status; ?> </label>
                      </div>
                    </div>
                  </div>
                  <hr>
                  <div id="multi-config" <?php echo isset($yomenu['flyout_design']) && $yomenu['flyout_design'] == 'fm-multi' ? '' : 'style="display:none;"'; ?>>
                    <div class="form-group">
                      <label class="col-sm-4 control-label"><?php echo $entry_item_column; ?></label>
                      <div class="col-sm-4">
                        <input type="text" name="yomenu[column]" value="<?php echo !empty($yomenu['column']) ? $yomenu['column'] : '3'; ?>" class="form-control text-center" />
                      </div>
                    </div>
                    <hr>
                    <div class="form-group">
                      <label class="col-sm-4 control-label"><?php echo $entry_banner; ?></label>
                      <div class="col-sm-8">
                        <div class="btn-group btn-group-justified" data-toggle="buttons">
                          <label class="btn btn-success">
                            <input type="radio" name="yomenu[item_banner]" value="1" <?php echo isset($yomenu['item_banner']) ? 'checked="checked"' : ''; ?> />
                            <?php echo $text_enabled; ?> </label>
                          <label class="btn btn-danger">
                            <input type="radio" name="yomenu[item_banner]" value="0" <?php echo empty($yomenu['item_banner']) ? 'checked="checked"' : ''; ?> />
                            <?php echo $text_disabled; ?> </label>
                        </div>
                      </div>
                    </div>
                    <hr>
                    <div class="form-group">
                      <label class="col-sm-4 control-label"><?php echo $entry_size; ?></label>
                      <div class="col-sm-4">
                        <input type="text" name="yomenu[banner_width]" value="<?php echo !empty($yomenu['banner_width']) ? $yomenu['banner_width'] : '200'; ?>" class="form-control text-center" />
                      </div>
                      <div class="col-sm-4">
                        <input type="text" name="yomenu[banner_height]" value="<?php echo !empty($yomenu['banner_height']) ? $yomenu['banner_height'] : '200'; ?>" class="form-control text-center" />
                      </div>
                    </div>
                    <hr>
                    <div class="form-group">
                      <label class="col-sm-4 control-label"><?php echo $entry_item_info; ?></label>
                      <div class="col-sm-8">
                        <div class="btn-group btn-group-justified" data-toggle="buttons">
                          <label class="btn btn-success">
                            <input type="radio" name="yomenu[item_description]" value="1" <?php echo isset($yomenu['item_description']) ? 'checked="checked"' : ''; ?> />
                            <?php echo $text_enabled; ?> </label>
                          <label class="btn btn-danger">
                            <input type="radio" name="yomenu[item_description]" value="0" <?php echo empty($yomenu['item_description']) ? 'checked="checked"' : ''; ?> />
                            <?php echo $text_disabled; ?> </label>
                        </div>
                      </div>
                    </div>
                    <hr>
                    <div class="form-group">
                      <label class="col-sm-4 control-label"><?php echo $entry_info_limit; ?></label>
                      <div class="col-sm-4">
                        <input type="text" name="yomenu[description_limit]" value="<?php echo !empty($yomenu['description_limit']) ? $yomenu['description_limit'] : ''; ?>" class="form-control text-center" />
                      </div>
                    </div>
                    <hr>
                    <div class="form-group">
                      <label class="col-sm-4 control-label"><?php echo $entry_image_position; ?></label>
                      <div class="col-sm-8">
                        <div class="btn-group btn-group-justified" data-toggle="buttons">
                          <label class="btn btn-success">
                            <input type="radio" name="yomenu[image_position]" value="center" <?php echo empty($yomenu['image_position']) || $yomenu['image_position'] == 'center' ? 'checked="checked"' : ''; ?> />
                            <?php echo $text_center; ?> </label>
                          <label class="btn btn-success">
                            <input type="radio" name="yomenu[image_position]" value="left" <?php echo isset($yomenu['image_position']) && $yomenu['image_position'] == 'left' ? 'checked="checked"' : ''; ?> />
                            <?php echo $text_left; ?> </label>
                        </div>
                      </div>
                    </div>
                    <hr>
                  </div>
                </div>
                <div id="push-config" class="menu-design" <?php echo isset($yomenu['menu_design']) && $yomenu['menu_design'] == 'pm' ? '' : 'style="display:none;"'; ?>>
                  <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-4">
                      <div class="checkbox">
                        <label>
                          <input type="checkbox" name="yomenu[pm_icon]" value="1" <?php echo isset($yomenu['pm_icon']) ? 'checked="checked"' : ''; ?> />
                          <?php echo $text_icons_status; ?> </label>
                      </div>
                    </div>
                  </div>
                  <hr>
                </div>
                <div id="image-config">
                  <div class="form-group">
                    <label class="col-sm-4 control-label"><?php echo $entry_image_status; ?></label>
                    <div class="col-sm-8">
                      <div class="btn-group btn-group-justified" data-toggle="buttons">
                        <label class="btn btn-success">
                          <input type="radio" name="yomenu[item_image]" value="1" <?php echo isset($yomenu['item_image']) ? 'checked="checked"' : ''; ?> />
                          <?php echo $text_enabled; ?> </label>
                        <label class="btn btn-danger">
                          <input type="radio" name="yomenu[item_image]" value="0" <?php echo empty($yomenu['item_image']) ? 'checked="checked"' : ''; ?> />
                          <?php echo $text_disabled; ?> </label>
                      </div>
                    </div>
                  </div>
                  <hr>
                  <div class="form-group">
                    <label class="col-sm-4 control-label"><?php echo $entry_size; ?></label>
                    <div class="col-sm-4">
                      <input type="text" name="yomenu[image_width]" value="<?php echo !empty($yomenu['image_width']) ? $yomenu['image_width'] : '200'; ?>" class="form-control text-center" />
                    </div>
                    <div class="col-sm-4">
                      <input type="text" name="yomenu[image_height]" value="<?php echo !empty($yomenu['image_height']) ? $yomenu['image_height'] : '200'; ?>" class="form-control text-center" />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div id="moduleSetting" class="tab-pane">
            <div class="row">
              <div class="col-lg-6">
                <div class="form-group required">
                  <label class="col-sm-4 control-label" for="input-name"><?php echo $entry_name; ?></label>
                  <div class="col-sm-8">
                    <input type="text" name="name" value="<?php echo $name; ?>" id="input-name" class="form-control" />
                    <?php if ($error_name) { ?>
                    <div class="text-danger"><?php echo $error_name; ?></div>
                    <?php } ?>
                  </div>
                </div>
                <hr>
                <div class="form-group">
                  <label class="col-sm-4 control-label"><?php echo $entry_status; ?></label>
                  <div class="col-sm-8">
                    <div class="btn-group btn-group-justified" data-toggle="buttons">
                      <label class="btn btn-success">
                        <input type="radio" name="status" value="1" <?php echo isset($status) ? 'checked="checked"' : ''; ?> />
                        <?php echo $text_enabled; ?> </label>
                      <label class="btn btn-danger">
                        <input type="radio" name="status" value="0" <?php echo empty($status) ? 'checked="checked"' : ''; ?> />
                        <?php echo $text_disabled; ?> </label>
                    </div>
                  </div>
                </div>
                <hr>
                <div class="form-group">
                  <label class="col-sm-4 control-label"><?php echo $entry_class; ?></label>
                  <div class="col-sm-8">
                    <input type="text" name="yomenu[class]" value="<?php echo !empty($yomenu['class']) ? $yomenu['class'] : 'yo-menu'; ?>" class="form-control" />
                  </div>
                </div>
                <hr>
                <div class="form-group">
                  <label class="col-sm-4 control-label"><?php echo $entry_store; ?></label>
                  <div class="col-sm-8">
                    <div class="well well-sm" style="height: 150px; overflow: auto; margin-bottom: 0;">
                      <div class="checkbox">
                        <label>
                          <input type="checkbox" name="yomenu[store_id][]" value="0" <?php echo isset($yomenu['store_id']) && in_array(0, $yomenu['store_id']) ? 'checked="checked" ' : ''; ?> />
                          <?php echo $default_store; ?> </label>
                      </div>
                      <?php foreach ($stores as $store) { ?>
                      <div class="checkbox">
                        <label>
                          <input type="checkbox" name="yomenu[store_id][]" value="<?php echo $store['store_id']; ?>" <?php echo isset($yomenu['store_id']) && in_array($store['store_id'], $yomenu['store_id']) ? 'checked="checked" ' : ''; ?> />
                          <?php echo $store['name']; ?> </label>
                      </div>
                      <?php } ?>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-6">
                <div class="form-group">
                  <label class="col-sm-4 control-label"><?php echo $entry_location; ?></label>
                  <div class="col-sm-8">
                    <input type="text" name="yomenu[fcid]" value="" placeholder="<?php echo $text_cat_autocomplete; ?>" class="form-control" />
                    <div id="yomenu-location" class="well well-sm" style="height: 131px; overflow: auto; margin-bottom: 0;">
                      <div class="checkbox" style="border-bottom: 1px dashed #CCC; padding-bottom: 9px;">
                        <label>
                          <input type="checkbox" name="yomenu[location]" value="1" <?php echo isset($yomenu['location']) ? 'checked="checked"' : ''; ?> />
                          <?php echo $text_all_categories; ?> </label>
                      </div>
                      <?php foreach ($locations as $location) { ?>
                      <div id="yomenu-location<?php echo $location['category_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $location['name']; ?>
                        <input type="hidden" name="yomenu[fcid][]" value="<?php echo $location['category_id']; ?>" />
                      </div>
                      <?php } ?>
                    </div>
                  </div>
                </div>
                <hr>
                <div class="form-group">
                  <label class="col-sm-4 control-label"><?php echo $entry_customers; ?></label>
                  <div class="col-sm-8">
                    <div class="well well-sm" style="height: 150px; overflow: auto; margin-bottom: 0;">
                      <div class="checkbox" style="border-bottom: 1px dashed #CCC; padding-bottom: 9px;">
                        <label>
                          <input type="checkbox" name="yomenu[all_customers]" value="1" <?php echo isset($yomenu['all_customers']) ? 'checked="checked"' : ''; ?> />
                          <?php echo $text_all_customers; ?> </label>
                      </div>
                      <?php foreach ($customer_groups as $customer_group) { ?>
                      <div class="checkbox">
                        <label>
                          <input type="checkbox" name="yomenu[customer_group_id][]" value="<?php echo $customer_group['customer_group_id']; ?>" <?php echo isset($yomenu['customer_group_id']) && in_array($customer_group['customer_group_id'], $yomenu['customer_group_id']) ? 'checked="checked" ' : ''; ?> />
                          <?php echo $customer_group['name']; ?> </label>
                      </div>
                      <?php } ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <input type="hidden" name="apply" id="apply" value="0" />
      </form>
    </div>
  </div>
  <div class="footer">
    <ul class="list-inline pull-right">
      <li><a href="mailto:info@idiy.club"><?php echo $text_support; ?></a></li>
      <li><a href="<?php echo $text_author_link; ?>" onclick="return !window.open(this.href)"><?php echo $text_more; ?></a></li>
    </ul>
    <p>2016 © <?php echo $text_author; ?> <a href="<?php echo $text_author_link; ?>" onclick="return !window.open(this.href)">iDiY</a>. <?php echo $heading_title; ?> <?php echo $version; ?></p>
  </div>
</div>
<script type="text/javascript"><!--
$('.yo-apply').delay(5000).fadeOut(300);
$('input[name=\'yomenu[featured_category]\']').autocomplete({
  source: function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/category/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
      dataType: 'json',
      success: function(json) {
        response($.map(json, function(item) {
          return {
            label: item['name'],
            value: item['category_id']
          }
        }));
      }
    });
  },
  select: function(item) {
    $('input[name=\'yomenu[featured_category]\']').val('');
    
    $('#yomenu-featured-category' + item['value']).remove();
    
    $('#yomenu-featured-category').append('<div id="yomenu-featured-category' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="yomenu[featured_category][]" value="' + item['value'] + '" /></div>');
  }
});

$('#yomenu-featured-category').delegate('.fa-minus-circle', 'click', function() {
  $(this).parent().remove();
});

$('input[name=\'yomenu[featured_brands]\']').autocomplete({
  source: function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/manufacturer/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
      dataType: 'json',
      success: function(json) {
        response($.map(json, function(item) {
          return {
            label: item['name'],
            value: item['manufacturer_id']
          }
        }));
      }
    });
  },
  select: function(item) {
    $('input[name=\'yomenu[featured_brands]\']').val('');
    
    $('#yomenu-featured-brands' + item['value']).remove();
    
    $('#yomenu-featured-brands').append('<div id="yomenu-featured-brands' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="yomenu[featured_brands][]" value="' + item['value'] + '" /></div>');
  }
});

$('#yomenu-featured-brands').delegate('.fa-minus-circle', 'click', function() {
  $(this).parent().remove();
});

$('input[name=\'yomenu[fcid]\']').autocomplete({
  source: function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/category/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
      dataType: 'json',
      success: function(json) {
        response($.map(json, function(item) {
          return {
            label: item['name'],
            value: item['category_id']
          }
        }));
      }
    });
  },
  select: function(item) {
    $('input[name=\'yomenu[fcid]\']').val('');
    
    $('#yomenu-location' + item['value']).remove();
    
    $('#yomenu-location').append('<div id="yomenu-location' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="yomenu[fcid][]" value="' + item['value'] + '" /></div>');
  }
});

$('#yomenu-location').delegate('.fa-minus-circle', 'click', function() {
  $(this).parent().remove();
});

$('label.btn input').each(function() {
  if ($(this).prop('checked')) {
    $(this).parent('.btn').addClass('active');
  };
});
//--></script>
<?php echo $footer; ?>