<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <!--<?php if(isset($success) && $success){ ?>
  <div class="alert alert-success" role="alert">
  <?php echo $success;  ?>
  </div>
<?php } ?> -->
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-customer-group" data-toggle="tooltip" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1>Ödeme Linki Oluştur</h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if (isset($error_warning)) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i><?=$heading_title;?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-customer-group" class="form-horizontal">

          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-sort-order"><?=$entry_firstname;?></label>
            <div class="col-sm-10">
              <input type="text" name="pay_firstname"  placeholder="<?=$entry_firstname;?>" id="input-pay_firstname" class="form-control" />
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-sort-order"><?=$entry_lastname;?></label>
            <div class="col-sm-10">
              <input type="text" name="pay_lastname"  placeholder="<?=$entry_lastname;?>" id="input-pay_lastname" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-sort-order"><?=$entry_email;?></label>
            <div class="col-sm-10">
              <input type="text" name="pay_email"  placeholder="<?=$entry_email;?>" id="input-pay_email" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-sort-order"><?=$entry_telephone;?></label>
            <div class="col-sm-10">
              <input type="text" name="pay_telephone" value placeholder="<?=$entry_telephone;?>" id="input-pay_telephone" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-sort-order"><?=$entry_identitynumber;?></label>
            <div class="col-sm-10">
              <input type="text" name="pay_identitynumber"  placeholder="<?=$entry_identitynumber;?>" id="input-pay_identitynumber" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-sort-order"><?=$entry_price;?></label>
            <div class="col-sm-10">
              <input type="text" id="pay_price" name="pay_price"  placeholder="<?=$entry_price;?>" id="input-pay_price" class="form-control" />
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script src="view/javascript/jquery/jquery.priceformat.min.js"></script>
<script>
$('#pay_price').priceFormat({
    prefix: '',
    suffix: '₺'
});
 </script>
<script type="text/javascript" src="view/javascript/summernote/summernote.js"></script>
<link href="view/javascript/summernote/summernote.css" rel="stylesheet" />
<script type="text/javascript" src="view/javascript/summernote/opencart.js"></script>
<?php echo $footer; ?>
