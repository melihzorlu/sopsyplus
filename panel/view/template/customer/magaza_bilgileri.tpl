<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-customer-group" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1>Mağaza Bilgileri</h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if (isset($error_warning)) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> Mağaza Bilgileri</h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-customer-group" class="form-horizontal">

          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-sort-order">Mağaza Adı</label>
            <div class="col-sm-10">
              <input type="text" name="store_name" value="<?php echo $store_name; ?>" placeholder="Mağaza Adı" id="input-store_name" class="form-control" />
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-sort-order">Mağaza Adresi</label>
            <div class="col-sm-10">
              <input type="text" name="store_address" value="<?php echo $store_address; ?>" placeholder="Mağaza Adresi" id="input-store_name" class="form-control" />
            </div>
          </div>



          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-sort-order">Mağaza Hakkında</label>
            <div class="col-sm-10">
              <textarea name="official" placeholder="Mağaza Hakkında" id="input-official" class="form-control summernote"><?php echo isset($official) ? $official : ''; ?></textarea>
            </div>
          </div>


          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-sort-order">İl</label>
            <div class="col-sm-10">
              <input type="text" name="store_city" value="<?php echo $store_city; ?>" placeholder="İl" id="input-store_name" class="form-control" />
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-sort-order">İlçe</label>
            <div class="col-sm-10">
              <input type="text" name="store_district" value="<?php echo $store_district; ?>" placeholder="İlçe" id="input-store_name" class="form-control" />
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-sort-order">Vergi Dairesi</label>
            <div class="col-sm-10">
              <input type="text" name="taxoffice" value="<?php echo $taxoffice; ?>" placeholder="Vergi Dairesi" id="input-store_name" class="form-control" />
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-sort-order">Vergi Numarası</label>
            <div class="col-sm-10">
              <input type="text" name="taxoffice" value="<?php echo $taxnumber; ?>" placeholder="Vergi Numarası" id="input-store_name" class="form-control" />
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-sort-order">Banka Adı</label>
            <div class="col-sm-10">
              <input type="text" name="store_bank_name" value="<?php echo $store_bank_name; ?>" placeholder="Banka Adı" id="input-store_name" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-sort-order">Banka Hesap Sahibi</label>
            <div class="col-sm-10">
              <input type="text" name="store_bank_username" value="<?php echo $store_bank_username; ?>" placeholder="Banka Hesap Sahibi" id="input-store_name" class="form-control" />
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-sort-order">IBAN</label>
            <div class="col-sm-10">
              <input type="text" name="store_iban" value="<?php echo $store_iban; ?>" placeholder="IBAN" id="input-store_name" class="form-control" />
            </div>
          </div>

        </form>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript" src="view/javascript/summernote/summernote.js"></script>
<link href="view/javascript/summernote/summernote.css" rel="stylesheet" />
<script type="text/javascript" src="view/javascript/summernote/opencart.js"></script>
<?php echo $footer; ?>
