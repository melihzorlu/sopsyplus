<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<head>
<meta charset="UTF-8" />
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content="<?php echo $keywords; ?>" />
<?php } ?>
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />
<script type="text/javascript" src="view/javascript/jquery/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="view/javascript/bootstrap/js/bootstrap.min.js"></script>
<link href="view/stylesheet/bootstrap.css" type="text/css" rel="stylesheet" />
<link href="view/javascript/font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet" />
<script src="view/javascript/jquery/datetimepicker/moment.js" type="text/javascript"></script>
<script src="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<link href="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen" />
<link type="text/css" href="view/stylesheet/stylesheet.css" rel="stylesheet" media="screen" />
<?php foreach ($styles as $style) { ?>
<link type="text/css" href="<?php echo $style['href']; ?>" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<script src="view/javascript/common.js" type="text/javascript"></script>
<?php foreach ($scripts as $script) { ?>
<script type="text/javascript" src="<?php echo $script; ?>"></script>
<?php } ?>
</head>
<body>
<div id="container">
<header id="header" class="navbar navbar-static-top">
<div class="container">
  <div class="navbar-header">

    <a href="<?php echo $home; ?>" class="navbar-brand"><img src="https://www.pazarmo.com/image/cache/catalog/pazarmo_logo-738x194.png" alt="<?php echo $heading_title; ?>" title="<?php echo $heading_title; ?>" /></a></div>
  <?php if ($logged) { ?>
  <ul class="nav pull-right">

    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-home fa-lg"></i></a>
      <ul class="dropdown-menu dropdown-menu-right">
        <li class="dropdown-header"><?php echo $text_store; ?></li>
        <?php foreach ($stores as $store) { ?>
        <li><a href="<?php echo $store['href']; ?>" target="_blank"><?php echo $store['name']; ?></a></li>
        <?php } ?>
      </ul>
    </li>

    <li><a href="<?php echo $logout; ?>"><span class="hidden-xs hidden-sm hidden-md"><?php echo $text_logout; ?></span> <i class="fa fa-sign-out fa-lg"></i></a></li>
  </ul>
  <?php } ?>
</div>
</header>

<?php if ($logged) { ?>
<nav class="navbar navbar-default main_menu">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li><a href="<?php echo $home; ?>">Görev Yöneticisi</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Ürün Yönetimi <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="index.php?route=catalog/product/add&token=<?php echo $token; ?>">Ürün Ekle</a></li>
            <li><a href="index.php?route=catalog/product&token=<?php echo $token; ?>">Ürünleriniz</a></li>
            <li><a href="index.php?route=catalog/allproduct&token=<?php echo $token; ?>">Satılabilir Ürünler</a></li>
            <li><a href="index.php?route=catalog/category&token=<?php echo $token; ?>">Kullanılabilir Kategoriler</a></li>
            <li><a href="index.php?route=customer/xml&token=<?php echo $token; ?>">XML Ürün Aktarımı</a></li>
            <li><a href="index.php?route=customer/excel&token=<?php echo $token; ?>">Excel Ürün Aktarımı</a></li>
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Sipariş Yönetimi <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="index.php?route=customer/paylink&token=<?php echo $token; ?>">Ödeme Linki Oluştur</a></li>
            <li><a href="index.php?route=sale/order&token=<?php echo $token; ?>">Siparişlerim</a></li>
            <li><a href="index.php?route=customer/return&token=<?php echo $token; ?>">İade İşlemleri</a></li>
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Kargo Yönetimi <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="index.php?route=customer/addshipping&token=<?php echo $token; ?>">Kargo Kodu Oluştur</a></li>
            <li><a href="index.php?route=customer/myshipping&token=<?php echo $token; ?>">Kargolarım</a></li>
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Genel İşlemler <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="index.php?route=customer/mypay&token=<?php echo $token; ?>">Ödeme İşlemlerim</a></li>
            <li><a href="index.php?route=customer/mygetmoney&token=<?php echo $token; ?>">Alacak İşlemlerim</a></li>
            <li><a href="index.php?route=customer/statictics&token=<?php echo $token; ?>">İstatistikler</a></li>
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Mağaza İşlemleri <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="index.php?route=customer/magaza_bilgileri&token=<?php echo $token; ?>">Mağaza Bilgilerim</a></li>
            <li><a href="index.php?route=customer/document&token=<?php echo $token; ?>">Evrak Yönetimi</a></li>
          </ul>
        </li>
      </ul>

    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
<?php } ?>

<div class="container">
