<?php
/* Turkceye Ceviren eka7a - http://www.e-piksel.com */

// Heading
$_['heading_title']    = 'Modüller';

// Text
$_['text_success']     = 'Başarılı: Modül başarılı bir şekilde değiştirildi!';
$_['text_list']        = 'Modül Listesi';
$_['text_add']         = 'Modül Ekle';
$_['text_edit']        = 'Modül Düzenle';
$_['text_default']     = 'Varsayılan';

// Column
$_['column_name']      = 'Modül Adı';
$_['column_status']    = 'Durumu';
$_['column_action']    = 'Eylem';

// Entry
$_['entry_name']       = 'Modül Adı';
$_['entry_title']      = 'Başlık';
$_['entry_link']       = 'Bağlantı';
$_['entry_image']      = 'Resim';
$_['entry_status']     = 'Durumu';
$_['entry_sort_order'] = 'Sıralama';

// Error
$_['error_permission'] = 'Uyarı: Modülleri düzenleme iznine sahip değilsiniz!';
$_['error_name']       = 'Modül Adı 3 ile 64 karakter arasında olmalı!';
$_['error_title']      = 'Modül Başlığı 2 ile 64 karakter arasında olmalı!';
