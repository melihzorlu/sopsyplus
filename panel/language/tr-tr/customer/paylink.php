<?php
/* Turkceye Ceviren eka7a - http://www.e-piksel.com */

// Heading
$_['heading_title']               = 'Ödeme Linki';
$_['text_success']               = 'Ödeme Linki Kaydedildi.';

// Entry
$_['entry_firstname']             = 'Adı';
$_['entry_lastname']              = 'Soyadı';
$_['entry_email']                 = 'E-Posta';
$_['entry_telephone']             = 'Telefon';
$_['entry_identitynumber']        = 'Tc Kimlik';
$_['entry_price']        = 'Tutar';
