<?php
// Heading
$_['heading_title']           = '<b>Sol Mega Kategori Menü</b>';
$_['module_title']            = 'Sol Mega Kategori Menü';

// Text
$_['text_extension']          = 'Module';
$_['text_success']            = 'Modül Başarıyla Düzenleendi';
$_['text_apply']              = 'Güncelleme Başarılı!';
$_['text_edit']               = 'Modül DÜzenle';

$_['text_author']             = 'Yazar';
$_['text_author_link']        = 'http://www.opencart-themes.org';
$_['text_support']            = 'Destek';
$_['text_more']               = 'Modüller';

$_['text_expanded']           = 'Genişletilmiş';
$_['text_minimized']          = 'Minimize edilmiş';

$_['text_tree']               = 'Kategoriler';
$_['text_brands']             = 'Markalar';
$_['text_current']            = 'Alt Kategorilerde Geçerli';
$_['text_parent']             = 'Ana Kategorilerde Geçerli';

$_['text_am']                 = 'Accordion Menu';
$_['text_fm']                 = 'Açılır Menu';
$_['text_pm']                 = 'Drill Down Menu';

$_['text_all_categories']     = 'Tüm Kateogriler';
$_['text_current_categories'] = 'Seçme Kateogriler';
$_['text_all_brands']         = 'Tüm Markalar';
$_['text_all_customers']      = 'Tüm Müşteri Grubları ve Misifir müşteriler';
$_['text_current_brands']     = 'Seçme Markalar';
$_['text_count']              = 'Ürün Sayısı';
$_['text_one_column']         = 'Default';
$_['text_multi_column']       = 'Genişletilmiş';
$_['text_save_view']          = 'Kullanıcnın Seçmini Kaydet';
$_['text_cat_autocomplete']   = 'Kategoriler...';

$_['text_level_1']            = 'Level 1';
$_['text_level_2']            = 'Level 2';
$_['text_level_3']            = 'Level 3';
$_['text_level_4']            = 'Level 4';
$_['text_level_5']            = 'Level 5';

$_['text_toggle']             = 'Geçiş Düğmesi';
$_['text_icons_status']       = 'Öğe İkonu';

$_['text_center']             = 'Ortada';
$_['text_left']               = 'Solda';

$_['text_default_asc']        = 'Sayısal Sıra';
$_['text_viewed_asc']         = 'Popüler';
$_['text_rating_desc']        = 'Rating';
$_['text_date_desc']          = 'Yeni';
$_['text_name_asc']           = 'İsim (A - Z)';
$_['text_name_desc']          = 'İsim (Z - A)';
$_['text_price_asc']          = 'Fiyat (Düşük - Yüksek)';
$_['text_price_desc']         = 'Fiyat (Yüksek - Düşük)';

// Entry
$_['entry_categories']        = 'Kategoriler:';
$_['entry_brands']            = 'Mrkalar:';
$_['entry_title']             = 'Menu Başlığı:';
$_['entry_menu_items']        = 'Menu Öğeleri:';
$_['entry_minimized']         = 'Görünüm:';
$_['entry_menu_design']       = 'Stil:';
$_['entry_location']          = 'Sadece bu kategoriler göster:';
$_['entry_prod_by_cat']       = 'Kategorinin Ürünleri:';
$_['entry_prod_by_brand']     = 'Markanın Ürünleri:';
$_['entry_levels']            = 'Kategori Düzeyleri:';
$_['entry_sub_limit']         = 'Limit (Alta Kategoriler):';
$_['entry_products_limit']    = 'Ürün Limit:';
$_['entry_flyout_column']     = 'Açılır Pencere:';
$_['entry_item_column']       = 'Kolon:';
$_['entry_banner']            = 'Banner:';
$_['entry_item_info']         = 'Öğe Açıklaması:';
$_['entry_info_limit']        = 'Açıklama Limiti, (symbol):';
$_['entry_image_status']      = 'Resimler:';
$_['entry_size']              = 'Resim Size (W x H), px:';
$_['entry_image_position']    = 'Resim Pozisyonu:';
$_['entry_effect']            = 'Effect (easing | duration):';

$_['entry_name']              = 'Modül Adı:';
$_['entry_status']            = 'Durum:';
$_['entry_store']             = 'Mağazalar:';
$_['entry_customers']         = 'Müşteri Grubu:';
$_['entry_sort_order']        = 'Sırlama:';

$_['entry_class']             = 'Container Class:';

// Tab
$_['tab_menu_setting']        = 'Menü Yapılandırması';
$_['tab_module_setting']      = 'Modül Ayarları';

// Button
$_['button_apply']            = 'Onayla';

// Error
$_['error_permission']        = 'Modül Düzenleme yetkiniz yok!';
$_['error_name']              = 'Modül Adı 3 ile 64 Karekter arasında olmalı!';
?>