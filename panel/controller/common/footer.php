<?php
class ControllerCommonFooter extends Controller {
	public function index() {
		$this->load->language('common/footer');

		$data['text_footer'] = $this->language->get('text_footer');

		if ($this->user->isLogged() && isset($this->request->get['seller_token']) && ($this->request->get['seller_token'] == $this->session->data['seller_token'])) {
			$data['text_version'] = sprintf($this->language->get('text_version'), '1.6.2');
		} else {
			$data['text_version'] = '';
		}

		return $this->load->view('common/footer', $data);
	}
}
