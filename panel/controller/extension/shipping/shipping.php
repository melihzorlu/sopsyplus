<?php
error_reporting(0);
//error_reporting(0);
// Mağazaların yapacakları işlemler için tasarlanan Controller

class ControllerExtensionShippingShipping extends Controller {

  public function index() {

    //$this->load->model('account/customerpartner');
    $this->load->model('extension/shipping/softomikargo');

    $cargokey = isset($this->request->get['cargokey']) ? $this->request->get['cargokey'] : NULL;
    $order_id = isset($this->request->get['order_id']) ? $this->request->get['order_id'] : NULL;
    $shipment_id = isset($this->request->get['shipment_id']) ? $this->request->get['shipment_id'] : NULL;
    $product_ids = (isset($this->request->post['product_ids']) && !empty($this->request->post['product_ids'])) ? $this->request->post['product_ids'] : NULL;
    $data['order_id'] = $order_id;

    if(!is_null($cargokey)){
      $data['cargokey'] = $cargokey;
    }

    if(is_null($order_id)) {
      $json['error'] = "Hata Oluştu: Sipariş Numarası eksik";
    }

    /*
    Bu geliştirmeye göre bir siparişe sadece bir kere referans kodu oluşturulabilir. Kullanıcı daha önce seçmediği bir
    ürün için yeniden referans kodu oluşturmaya çalışırsa hata kodu basılır.

    Daha önce referans kodu oluşturulmamış ürünler için satıcı kargo bedelini ödemeyi kabul etmeli ve bireysel olarak kargoyu gönderip
    farklı kargo gönderi formunu doldurmalıdır.
    */
    if(!is_null($order_id)) {

      if(!is_array($product_ids) && $product_ids != ''){
        $product_ids = explode(',', $product_ids);

        foreach ($product_ids as $key => $value) {

          $product_ids[$value] = $value;
          unset($product_ids[$key]);

        }

      }

      if(!is_null($shipment_id)){

        $shipment_sql = $this->model_extension_shipping_softomikargo->queryShipmentId($shipment_id, $this->customer->getId());

        if(count($shipment_sql) == 0){

          $shipment_sql = $this->model_extension_shipping_softomikargo->queryShipmentReference($shipment_id, $this->customer->getId());

        }

        if(count($shipment_sql)>0){

          $cargokey = $shipment_sql['cargokey'];

          $this->load->model('extension/shipping/cargo/'.$cargokey);

          $return = $this->{model_extension_shipping_cargo_.$cargokey}->getShipment($order_id, $shipment_sql);

          //$json['error'] = "Bu siparişinize daha önce kargo referansı oluşturmuşsunuz. Kargo fiziksel işlem görmemiş ise İPTAL edebilir yeniden kargo referansı oluşturabilirsiniz. Referansı oluşturulmamış bir ürünü kargolamak için ürünleri kargolardıktan sonra takip kodunu bu sayfanın altındaki ilgili forma giriniz.";
          return $this->response->setOutput($return);

        }


      }

    }

    if(is_null($product_ids) || count($product_ids)<=0 || $product_ids =='') {
      $json['error'] = "Hata Oluştu: Kargolanacak ürünleri seçiniz";
    }

    /* Seçilmiş ürünlerin kargo kaydı yapılıp yapılmadığı tespit ediliyor.
    $data['selectedProduct'] = Seçilen ürün listesini tutar
    $data['shipmentProduct'] = Seçilen ürünlerden hangilerinin kargoya gönderildiği verisini tutar
    data['otherProduct'] = Seçilen ürünlerden hangilerinin kargoya gönderilmediği verisini tutar.
    */
    /* TODO: Parçalı kargo sistemi, sipariş toplamında değişiklik oluşturacağı için düzenleme daha sonra planlı şekilde yapılmalıdır.*/
    if(!is_null($product_ids)){

      if(is_array($product_ids)){
        $product_ids = implode(',',$product_ids);
      }

      $shipment_sql = $this->model_extension_shipping_softomikargo->queryShipmentProduct($order_id, $product_ids);
      $data['selectedProduct'] = $shipment_sql['select'];
      $data['shipmentProduct'] = $shipment_sql['shipment'];
      $data['otherProduct'] = $shipment_sql['diff'];

      if(count($data['shipmentProduct'])>0){
        $json['error'] = "Daha önce kargo referansı oluşturulmuş ürün seçemezsiniz. Kargo fiziksel işlem görmemiş ise İPTAL edebilir yeniden kargo referansı oluşturabilirsiniz.";
      }
    }

    if(isset($json['error']) && $json['error']){
      $this->response->setOutput(json_encode($json));

    } else {
      $json['success']  = true;

      $data['shipping_status']=null;

      $seller_info = $this->model_extension_shipping_softomikargo->getSellerDetails($order_id);

      $shipping_seller_id = $seller_info['seller_detail']['seller_id'];

      if (isset($shipping_seller_id) && $shipping_seller_id) {

          $sql = "SELECT shipping_status FROM " . DB_PREFIX ."customerpartner_to_order c2o WHERE order_id = '".(int)$order_id."' AND customer_id=".$shipping_seller_id;

          $getShippingStatus = $this->db->query($sql)->row;

          if(isset($getShippingStatus['shipping_status'])){
              $data['shipping_status'] = $getShippingStatus['shipping_status'];
          }

      }
/*
      if($data['shipping_status'] != 'OZLKRG'){
        $data['cargos'][0] = $this->model_extension_shipping_softomikargo->getCargoDetail($data['shipping_status']);
      } else {
        $data['cargos'] = $this->model_extension_shipping_softomikargo->getAllCargo();

      }*/
      $data['cargos'] = $this->model_extension_shipping_softomikargo->getAllCargo();

      //$data['cargos'][0] = $this->model_extension_shipping_softomikargo->getCargo($data['shipping_status']);

      foreach($data['cargos'] as $item=>$value){

        if(file_exists(DIR_IMAGE.'cargo/'.$value['cargokey'].'.jpg')) {
          $data['cargos'][$item]['image'] = $value['cargokey'].'.jpg';
        } else {
          $data['cargos'][$item]['image'] = 'softomi.jpg';
        }

      }

      if(!is_null($data['cargos'])){

        if(count($data['cargos']) == 1){

          $cargokey = $data['cargos'][0]['cargokey'];

        }

      }

      if(is_null($cargokey)){

        $data['heading_title'] = "Kargo Seçiniz";

        $this->response->setOutput(json_encode($data));

      } else {
        // Seçili Kargo modelini çağır
        $this->load->model('account/order');
        $this->load->model('extension/shipping/cargo/'.$cargokey);
        $this->load->model('localisation/country');
        $this->load->model('localisation/zone');

        $this->load->model('sale/order');
        $this->load->model('catalog/product');
        $this->load->model('catalog/category');
        //$this->load->model('account/customerpartner');
        //Kargo etiketi burada oluşacak
        //$order_info = $this->model_account_customerpartner->getOrder($order_id);
        $order_info = $this->model_account_order->getOrder($order_id);

        $seller_info = $this->model_extension_shipping_softomikargo->getSellerDetails($order_id);
        //$country_info = $this->model_localisation_country->getCountry($address['country_id']);
        //$zone_info  = $this->model_localisation_zone->getZone($address['zone_id']);

        $total_weight = 0 ;
        //$products = $this->model_account_customerpartner->getSellerOrderProducts($order_id);
        $products = $this->model_sale_order->getOrderProducts($order_id);

        if(!is_array($product_ids)){
          $product_ids = explode(',',$product_ids);
        }
        foreach ($product_ids as $key => $value) {
          $product_ids[$value] = $value;
          unset($product_ids[$key]);
          // code...
        }

        foreach ($products as $key=>$value) {

          if(in_array($value['product_id'], $product_ids)){

            //unset($product_ids[$value['product_id']]);

            //$product      = $this->model_account_customerpartner->getProduct($value['product_id']);
            //$category_ids = $this->model_account_customerpartner->getProductCategories($value['product_id']);
            $product      = $this->model_catalog_product->getProduct($value['product_id']);
            $category_ids = $this->model_catalog_product->getProductCategories($value['product_id']);

            foreach ($category_ids as $cat) {
              //$catname = $this->model_account_customerpartner->getCategory($cat);
              $catname = $this->model_catalog_category->getCategory($cat);

              if(strlen(@$malcinsi.",".$catname['name'])<=40){
                @$malcinsi += $catname['name'].",";
              }
            }

            if(!strlen(@$malcinsi)){
              $malcinsi = "Elbise";
            }

            $product['weight'] = ($product['weight'] < 0.1 ? 0.1 : $product['weight']);
            $product['height'] = ($product['height'] < 0.1 ? 0.1 : $product['height']);
            $product['length'] = ($product['length'] < 0.1 ? 0.1 : $product['length']);

            @$total_weight +=  ($product['weight'] * $value['quantity']);

            @$total_height +=  ($product['height'] * $value['quantity']);
            @$total_width  +=  ($product['width'] * $value['quantity']);
            @$total_length +=  ($product['length'] * $value['quantity']);


            //hesaplama kontrol
            $toplamfiyat += ($value['price'] + ($this->config->get('config_tax') ? $value['tax']  : 0))* $value['quantity'];

            //bitti
            $pt = $value['total'] + ($this->config->get('config_tax') ? ($value['tax'] * $value['quantity']) : 0);
            $kdv += $pt-$value['c2oprice'];


          }


        }


        $b = array('ı','i','ğ','ü','ş','ö','ç');
        $d = array('I','İ','Ğ','Ü','Ş','Ö','Ç');

        $cargo_data = array(
          'order' => array(
            'id' => $order_id
          ),
          'totals' => array(
            'weight'  => number_format($total_weight, 2, '.', ''),
            'width'   => number_format($total_width, 2, '.', ''),
            'length'  => number_format($total_length, 2, '.', ''),
            'height'  => number_format($total_height, 2, '.', ''),
            'desi'    => number_format((($total_width * $total_length * $total_height) / 3000)),
            'price'   => $toplamfiyat
          ),

          'sender' => array(
            'customer_id'   =>$seller_info['seller_detail']['seller_id'],
            'company'       => (strlen($seller_info['seller_detail']['companyname']) < 5 ? mb_strtoupper(str_replace($b,$d, $seller_info['seller_detail']['firstname'].' '.$seller_info['seller_detail']['lastname']),"UTF-8") : mb_strtoupper(str_replace($b,$d, $seller_info['seller_detail']['companyname']),"UTF-8")),
            'firstname'     => mb_strtoupper(str_replace($b,$d, $seller_info['seller_detail']['firstname']),"UTF-8"),
            'lastname'      => mb_strtoupper(str_replace($b,$d, $seller_info['seller_detail']['lastname']),"UTF-8"),
            'telephone'     => $seller_info['seller_detail']['telephone'],
            'email'         => $seller_info['seller_detail']['email'],
            'address'       => mb_strtoupper(str_replace($b,$d, $seller_info['seller_address']['address_1'].' '.$seller_info['seller_address']['address_2']),"UTF-8"),
            'postcode'      => $seller_info['seller_address']['postcode'],
            'city'          => mb_strtoupper(str_replace($b,$d, $seller_info['seller_address']['city']),"UTF-8"),
            'zone_code'     => $this->model_localisation_zone->getZone($seller_info['seller_address']['zone_id'])['code'],
            'zone'          => mb_strtoupper(str_replace($b,$d, $this->model_localisation_zone->getZone($seller_info['seller_address']['zone_id'])['name']),"UTF-8"),
            'country_code'  => $this->model_localisation_country->getCountry($seller_info['seller_address']['country_id'])['iso_code_2'],
            'country'       => mb_strtoupper(str_replace($b,$d, $seller_info['seller_address']['country']),"UTF-8")

          ),
          'receiver' => array(
            //'customer_id'   => $order_info['alici_id'],
            'customer_id'   => $order_info['customer_id'],
            'company'       => (strlen($order_info['shipping_company']) < 5 ? mb_strtoupper(str_replace($b,$d, $order_info['shipping_firstname'].' '.$order_info['shipping_lastname']),"UTF-8") : mb_strtoupper(str_replace($b,$d, $order_info['shipping_company']),"UTF-8")),
            'firstname'     => mb_strtoupper(str_replace($b,$d, $order_info['shipping_firstname']),"UTF-8"),
            'lastname'      => mb_strtoupper(str_replace($b,$d, $order_info['shipping_lastname']),"UTF-8"),
            'telephone'     => $order_info['telephone'],
            'email'         => $order_info['email'],
            'address'       => mb_strtoupper(str_replace($b,$d, $order_info['shipping_address_1'].' '.$order_info['shipping_address_2']),"UTF-8"),
            'postcode'      => $order_info['shipping_postcode'],
            'city'          => mb_strtoupper(str_replace($b,$d, $order_info['shipping_city']),"UTF-8"),
            'zone_code'     => $this->model_localisation_zone->getZone($order_info['shipping_zone_id'])['code'],
            'zone'          => mb_strtoupper(str_replace($b,$d, $this->model_localisation_zone->getZone($order_info['shipping_zone_id'])['name']),"UTF-8"),
            'country_code'  => $this->model_localisation_country->getCountry($order_info['shipping_country_id'])['iso_code_2'],
            'country'       => mb_strtoupper(str_replace($b,$d, $order_info['shipping_country']),"UTF-8")
          ),
        );
        //print_r($order_info);exit;
        $return = $this->{model_extension_shipping_cargo_.$cargokey}->createShipment($cargo_data);

        //REFERANS KODU servise başarılı şekilde ulaştırıldıysa shipment tablosuna kaydetme işlemi
        $return = json_decode($return,true);

//print_r($return);exit;
        if($return['status'] == 'error') {
          //print_r($return['Message']);exit;
          $json['success'] = false;
          $json['error'] = $return['Message'];

          return $this->response->setOutput(json_encode($json));
        }

        if($return['status'] == 'success'){

          if(isset($return['document_detail']) && is_array($return['document_detail'])){
            $cargo_data['document_detail'] = $return['document_detail'];
          }

$cargo_detail = $this->model_extension_shipping_softomikargo->getCargo($cargokey);

          $data = array(
            'cargokey'    => $cargokey,
            'cargo_title' => $cargo_detail['title'],
            'order_id'    => $order_id,
            'gonderici_id'=> $cargo_data['sender']['customer_id'],
            'alici_id'    => $cargo_data['receiver']['customer_id'],
            'rma_id'      => (isset($this->request->get['rma_id']) ? $this->request->get['rma_id']: NULL),
            'referans'    => $return['referans'],
            'islem'       => 'WAIT',
            'bilgi'       => json_encode($cargo_data, JSON_UNESCAPED_UNICODE),
            'date_added'  => date('Y-m-d H:i:s'),
            'document'    => $return['document']
          );

          $shipmentSave = $this->model_extension_shipping_softomikargo->saveShipment($data);

          if($shipmentSave) {
            // Seçili Ürünleri Kaydeder
            $productSave = $this->model_extension_shipping_softomikargo->saveProducts($shipmentSave, $product_ids, $data['order_id']);

            if($productSave){

              foreach($product_ids as $key=>$value){

                // PRODUCTLARI HAZIRLANIYOR KONUMUNA GETİR
                $this->model_extension_shipping_softomikargo->productChangeStatus($value, $order_id, 2);

              }

            }



          }

        }

        // Seçilen ürünleri referansa kaydet


        // Referans kodu oluşturulmuş ürün tekrar seçilememesi için order_info' da checkbox kontrolü yapılacak.

      }
      $json = $data;
      $this->response->setOutput(json_encode($json));

    }

  }

    public function rma(){
      $this->load->model('account/customerpartner');
      $this->load->model('extension/shipping/softomikargo');

      $method = isset($this->request->post['method']) ? $this->request->post['method'] : NULL;
      $cargokey = isset($this->request->get['cargokey']) ? $this->request->get['cargokey'] : NULL;
      $order_id = isset($this->request->get['order_id']) ? $this->request->get['order_id'] : NULL;
      $rma_id   = isset($this->request->get['rma_id']) ? $this->request->get['rma_id'] : NULL;
      //$product_ids = (isset($this->request->post['product_ids']) && !empty($this->request->post['product_ids'])) ? $this->request->post['product_ids'] : NULL;

      $product_ids = $this->db->query("SELECT product_id FROM ".DB_PREFIX."wk_rma_product WHERE rma_id=".$rma_id."")->rows;

      if(!is_array($product_ids)){

        $product_ids = explode(',', $product_ids);

        foreach ($product_ids as $key => $value) {

          $product_ids[$value['product_id']] = $value['product_id'];
          unset($product_ids[$key]);

        }
      //  print_r($product_ids);exit;

      }

      $sql = $this->db->query("SELECT * FROM ".DB_PREFIX."wk_rma_product WHERE rma_id = ".(int)$rma_id)->row;
      $order_product_id = $sql['order_product_id'];

      $cop = $this->db->query("SELECT * FROM ".DB_PREFIX."customerpartner_to_order WHERE order_product_id = ".$order_product_id."")->row;
      $cargokey = $cop['shipping_status'];
//print_R($cargokey);exit;
      if(is_null($cargokey)){
        $data['error']= "Bu Sipariş için Kargo Metodu Belirlememişsiniz. Görüşme sekmesi üzerinden alıcı ile iletişime geçebilirsiniz.";
      }elseif($cargokey == 'OZLKRG'){
        $data['error']= "Bu Sipariş için ÖZEL KARGO Metodunu Belirlemişsiniz. Görüşme sekmesi üzerinden Alıcı ile iletişime geçip iadenin gerçekleşmesini sağlamalısınız.";
      }else{
        $data['cargokey'] = $cargokey;
      }

      if(is_null($order_id)) {
        $json['error'] = "Hata Oluştu: Sipariş Numarası eksik";
      }

      if(is_null($rma_id)) {
        $json['error'] = "Hata Oluştu: İade Numarası eksik";
      }

      if(!is_null($order_id)) {

        $shipment_sql = $this->model_extension_shipping_softomikargo->queryRmaShipmentOrder($order_id, $this->customer->getId(), $rma_id);

        if(count($shipment_sql)>0){
          $cargokey = $shipment_sql['cargokey'];

          $this->load->model('extension/shipping/cargo/'.$cargokey);
          $shipment_sql['rma_id'] = $rma_id;
          $return = $this->{model_extension_shipping_cargo_.$cargokey}->getShipment($order_id, $shipment_sql);

          //$json['error'] = "Bu siparişinize daha önce kargo referansı oluşturmuşsunuz. Kargo fiziksel işlem görmemiş ise İPTAL edebilir yeniden kargo referansı oluşturabilirsiniz. Referansı oluşturulmamış bir ürünü kargolamak için ürünleri kargolardıktan sonra takip kodunu bu sayfanın altındaki ilgili forma giriniz.";
          return $this->response->setOutput($return);
        } else {
          if($method == 'receiver'){

            // Seçili Kargo modelini çağır
            $this->load->model('localisation/country');
            $this->load->model('localisation/zone');

            $order_info = $this->model_account_customerpartner->getOrder($order_id);
            $seller_info = $this->model_extension_shipping_softomikargo->getSellerDetails($order_id);

            //$country_info = $this->model_localisation_country->getCountry($address['country_id']);
            //$zone_info  = $this->model_localisation_zone->getZone($address['zone_id']);

                    $b = array('ı','i','ğ','ü','ş','ö','ç');
                    $d = array('I','İ','Ğ','Ü','Ş','Ö','Ç');

                    $cargo_data = array(
                      'order' => array(
                        'id' => $order_id,
                        'rma_id' => $rma_id
                      ),

                      'sender' => array(
                        'customer_id'   =>$seller_info['seller_detail']['seller_id'],
                        'company'       => mb_strtoupper(str_replace($b,$d, $seller_info['seller_detail']['companyname']),"UTF-8"),
                        'firstname'     => mb_strtoupper(str_replace($b,$d, $seller_info['seller_detail']['firstname']),"UTF-8"),
                        'lastname'      => mb_strtoupper(str_replace($b,$d, $seller_info['seller_detail']['lastname']),"UTF-8"),
                        'telephone'     => $seller_info['seller_detail']['telephone'],
                        'email'         => $seller_info['seller_detail']['email'],
                        'address'       => mb_strtoupper(str_replace($b,$d, $seller_info['seller_address']['address_1'].' '.$seller_info['seller_address']['address_2']),"UTF-8"),
                        'postcode'      => $seller_info['seller_address']['postcode'],
                        'city'          => mb_strtoupper(str_replace($b,$d, $seller_info['seller_address']['city']),"UTF-8"),
                        'zone_code'     => $this->model_localisation_zone->getZone($seller_info['seller_address']['zone_id'])['code'],
                        'zone'          => mb_strtoupper(str_replace($b,$d, $this->model_localisation_zone->getZone($seller_info['seller_address']['zone_id'])['name']),"UTF-8"),
                        'country_code'  => $this->model_localisation_country->getCountry($seller_info['seller_address']['country_id'])['iso_code_2'],
                        'country'       => mb_strtoupper(str_replace($b,$d, $seller_info['seller_address']['country']),"UTF-8")
                      )
                    );

                    $html .= 'Lütfen "Görüşme" sekmesine tıklayıp, aşağıdaki bilgileri mesaj olarak alıcıya iletiniz.';
                    $html .= '<hr>';
                    $html .= 'Aşağıda bulunan adres bilgilerimize iade etmek istediğiniz ürünleri "Gönderici Ödemeli" olarak gönderiniz.';
                    $html .= 'ALICI BİLGİLERİ<br>';
                    $html .= '<strong>'.$cargo_data['sender']["company"].'</strong><br>';
                    $html .= '<br>Adı Soyadı:&#09;<strong>'.$cargo_data['sender']["firstname"].' '.$cargo_data['sender']["lastname"].'</strong>';
                    $html .= '<br>Telefon:&#09;<strong>'.$cargo_data['sender']["telephone"].'</strong>';
                    $html .= '<br>Adres:&#09;&#09;<strong>'.$cargo_data['sender']["address"].'</strong>';
                    $html .= '<br>&#09;&#09;<strong>'.$cargo_data['sender']["postcode"].' '.$cargo_data['sender']["city"].' /  '. $cargo_data['sender']["zone"] .'</strong>';
                    $html .= '<br>&#09;&#09;<strong>'.$cargo_data['sender']["country"].'</strong>';

              $return = json_encode(
                array(
                  'status'         => 'success',
                  /*'referans'       => $referans,
                  'projectid'      => $this->projectId,
                  'order_details'  => $order_data,*/
                  'document'       => $html
                ) , JSON_UNESCAPED_UNICODE
              );

            return $this->response->setOutput($return);
          }


        }
      }


      if(!$json['error']){
        $json['success']  = true;
        //$this->response->setOutput(json_encode($json));


        if(!is_null($cargokey)) {

          // Seçili Kargo modelini çağır
          $this->load->model('extension/shipping/cargo/'.$cargokey);
          $this->load->model('localisation/country');
          $this->load->model('localisation/zone');
          //Kargo etiketi burada oluşacak
          $order_info = $this->model_account_customerpartner->getOrder($order_id);
          $seller_info = $this->model_extension_shipping_softomikargo->getSellerDetails($order_id);

          //$country_info = $this->model_localisation_country->getCountry($address['country_id']);
        //  $zone_info  = $this->model_localisation_zone->getZone($address['zone_id']);

          $total_weight = 0 ;
          $products = $this->model_account_customerpartner->getSellerOrderProducts($order_id);

          foreach ($products as $key=>$value) {

            if(in_array($value['product_id'], $product_ids)){

              //unset($product_ids[$value['product_id']]);

              $product      = $this->model_account_customerpartner->getProduct($value['product_id']);
              $category_ids = $this->model_account_customerpartner->getProductCategories($value['product_id']);

              foreach ($category_ids as $cat) {
                $catname = $this->model_account_customerpartner->getCategory($cat);

                if(strlen($malcinsi.",".$catname['name'])<=40){
                  $malcinsi += $catname['name'].",";
                }
              }

              if(!strlen($malcinsi)){
                $malcinsi = "Elbise";
              }

              $product['weight'] = ($product['weight'] < 0.1 ? 0.1 : $product['weight']);
              $product['height'] = ($product['height'] < 0.1 ? 0.1 : $product['height']);
              $product['width'] = ($product['width'] < 0.1 ? 0.1 : $product['width']);
              $product['length'] = ($product['length'] < 0.1 ? 0.1 : $product['length']);

              /*$total_weight += $total_weight + ($product['weight'] * $value['quantity']);

              $total_height += $total_height + ($product['height'] * $value['quantity']);
              $total_width  += $total_width + ($product['width'] * $value['quantity']);
              $total_length += $total_lenght + ($product['length'] * $value['quantity']);*/
              $total_weight += ($product['weight'] * $value['quantity']);
              $total_height += ($product['height'] * $value['quantity']);
              $total_width  += ($product['width'] * $value['quantity']);
              $total_length += ($product['length'] * $value['quantity']);
              //hesaplama kontrol
              $toplamfiyat += ($value['price'] + ($this->config->get('config_tax') ? $value['tax']  : 0))* $value['quantity'];

              //bitti
              $pt = $value['total'] + ($this->config->get('config_tax') ? ($value['tax'] * $value['quantity']) : 0);
              $kdv += $pt-$value['c2oprice'];


            }


          }


          $b = array('ı','i','ğ','ü','ş','ö','ç');
          $d = array('I','İ','Ğ','Ü','Ş','Ö','Ç');

          $cargo_data = array(
            'order' => array(
              'id' => $order_id,
              'rma_id' => $rma_id
            ),
            'totals' => array(
              'weight'  => number_format($total_weight, 2, '.', ''),
              'width'   => number_format($total_width, 2, '.', ''),
              'length'  => number_format($total_length, 2, '.', ''),
              'height'  => number_format($total_height, 2, '.', ''),
              'desi'    => number_format((($total_width * $total_length * $total_height) / 3000)),
              'price'   => $toplamfiyat
            ),

            'sender' => array(
              'customer_id'   =>$seller_info['seller_detail']['seller_id'],
              'company'       => mb_strtoupper(str_replace($b,$d, $seller_info['seller_detail']['companyname']),"UTF-8"),
              'firstname'     => mb_strtoupper(str_replace($b,$d, $seller_info['seller_detail']['firstname']),"UTF-8"),
              'lastname'      => mb_strtoupper(str_replace($b,$d, $seller_info['seller_detail']['lastname']),"UTF-8"),
              'telephone'     => $seller_info['seller_detail']['telephone'],
              'email'         => $seller_info['seller_detail']['email'],
              'address'       => mb_strtoupper(str_replace($b,$d, $seller_info['seller_address']['address_1'].' '.$seller_info['seller_address']['address_2']),"UTF-8"),
              'postcode'      => $seller_info['seller_address']['postcode'],
              'city'          => mb_strtoupper(str_replace($b,$d, $seller_info['seller_address']['city']),"UTF-8"),
              'zone_code'     => $this->model_localisation_zone->getZone($seller_info['seller_address']['zone_id'])['code'],
              'zone'          => mb_strtoupper(str_replace($b,$d, $this->model_localisation_zone->getZone($seller_info['seller_address']['zone_id'])['name']),"UTF-8"),
              'country_code'  => $this->model_localisation_country->getCountry($seller_info['seller_address']['country_id'])['iso_code_2'],
              'country'       => mb_strtoupper(str_replace($b,$d, $seller_info['seller_address']['country']),"UTF-8")

            ),
            'receiver' => array(
              'customer_id'   => $order_info['alici_id'],
              'company'       => mb_strtoupper(str_replace($b,$d, $order_info['shipping_company']),"UTF-8"),
              'firstname'     => mb_strtoupper(str_replace($b,$d, $order_info['shipping_firstname']),"UTF-8"),
              'lastname'      => mb_strtoupper(str_replace($b,$d, $order_info['shipping_lastname']),"UTF-8"),
              'telephone'     => $order_info['telephone'],
              'email'         => $order_info['email'],
              'address'       => mb_strtoupper(str_replace($b,$d, $order_info['shipping_address_1'].' '.$order_info['shipping_address_2']),"UTF-8"),
              'postcode'      => $order_info['shipping_postcode'],
              'city'          => mb_strtoupper(str_replace($b,$d, $order_info['shipping_city']),"UTF-8"),
              'zone_code'     => $this->model_localisation_zone->getZone($order_info['shipping_zone_id'])['code'],
              'zone'          => mb_strtoupper(str_replace($b,$d, $this->model_localisation_zone->getZone($order_info['shipping_zone_id'])['name']),"UTF-8"),
              'country_code'  => $this->model_localisation_country->getCountry($order_info['shipping_country_id'])['iso_code_2'],
              'country'       => mb_strtoupper(str_replace($b,$d, $order_info['shipping_country']),"UTF-8")
            ),
          );

          $s = $cargo_data['sender'];
          $r = $cargo_data['receiver'];

          $cargo_data['sender'] = $r;
          $cargo_data['receiver'] = $s;
          $cargo_data['rma_id'] = $rma_id;
          $return = $this->{model_extension_shipping_cargo_.$cargokey}->createShipment($cargo_data);

          //REFERANS KODU servise başarılı şekilde ulaştırıldıysa shipment tablosuna kaydetme işlemi
          $return = json_decode($return,true);
          //print_r($cargo_data);exit;

          if($return['status'] == 'success'){

            if(isset($return['document_detail']) && is_array($return['document_detail'])){
              $cargo_data['document_detail'] = $return['document_detail'];
            }

            $cargo_detail = $this->model_extension_shipping_softomikargo->getCargo($cargokey);

            $data = array(
              'cargokey'    => $cargokey,
              'cargo_title' => $cargo_detail['title'],
              'order_id'    => $order_id,
              'gonderici_id'=> $cargo_data['sender']['customer_id'],
              'alici_id'    => $cargo_data['receiver']['customer_id'],
              'rma_id'      => (isset($this->request->get['rma_id']) ? $this->request->get['rma_id']: NULL),
              'referans'    => $return['referans'],
              'islem'       => 'WAIT',
              'bilgi'       => json_encode($cargo_data, JSON_UNESCAPED_UNICODE),
              'date_added'  => date('Y-m-d H:i:s'),
              'document'    => $return['document']
            );

            $shipmentSave = $this->model_extension_shipping_softomikargo->saveShipment($data);

            /*if($shipmentSave) {


                foreach($product_ids as $key=>$value){

                  // PRODUCTLARI İADE SÜRECİ BAŞLATILDI KONUMUNA GETİR
                  $this->model_extension_shipping_softomikargo->productChangeStatus($value, $order_id, 19);

                }

            }*/

            if($shipmentSave) {
              //print_r($product_ids);exit;
              // Seçili Ürünleri Kaydeder
              $productSave = $this->model_extension_shipping_softomikargo->saveProducts($shipmentSave, $product_ids, $order_id);

              if($productSave){

                foreach($product_ids as $key=>$value){

                  // PRODUCTLARI HAZIRLANIYOR KONUMUNA GETİR
                  $this->model_extension_shipping_softomikargo->productChangeStatus($value, $order_id, 19);

                }

              }

            }

          }

        }
        $json = $data;
        $this->response->setOutput(json_encode($json));
    }
  }


  public function createShipment(){

    $cargokey = $this->request->get['cargo'];

    $order_id = $this->request->get['order'];

    $partner_id = $this->request->get['partner'];

    $customer_id = $this->request->get['customer'];

    $this->load->model('extension/shipping/cargo/'.$cargokey);

    $model = "model_extension_shipping_cargo_".$cargokey;

    echo $this->$model->createShipment();

  }

  public function cancelWaitingShipment(){

    //$shipment_id = $this->request->get['referans'];//Shipment ID Alınır
    $this->load->model('extension/shipping/softomikargo');

    $waitShipment = $this->model_extension_shipping_softomikargo->getCancelWaitingShipment();

    foreach ($waitShipment as $key => $shipment) {

      if(!is_null($shipment['id'])) {

        $shipment_sql = $this->model_extension_shipping_softomikargo->queryShipmentId($shipment['id']);

        if(count($shipment_sql) == 0){

          $shipment_sql = $this->model_extension_shipping_softomikargo->queryShipmentReference($shipment_id, $this->customer->getId());

        }

        $referans = $shipment_sql['referans'];

        if(count($shipment_sql)>0){

          $process_sql = $this->model_extension_shipping_softomikargo->getShipmentProcess($shipment_sql['id']);

          if(count($process_sql)>0){

            echo $shipment_sql['id']." Gönderi Kargo Tarafında Fiziksel İşlem Görmüş. İptal Gerçekleştirilemez.";

          } else {

            $cargokey = $shipment_sql['cargokey'];

            $this->load->model('extension/shipping/cargo/'.$cargokey);

            $return = $this->{model_extension_shipping_cargo_.$cargokey}->cancelShipment($referans);

            $this->load->model('extension/shipping/softomikargo');

            $product_ids = $this->model_extension_shipping_softomikargo->queryCustomerShipmentProducts($shipment['id']);

            foreach ($product_ids as $key => $value) {
              $this->model_extension_shipping_softomikargo->productChangeStatus($value['product_id'], $shipment_sql['order_id'], 20);
            }

            $delete_sql = $this->model_extension_shipping_softomikargo->deleteShipmentId($shipment['id']);

            if(!$delete_sql){
              echo $shipment['id']." gönderi iptali gerçekleştirilemedi.<br>";
            } else {
              echo $shipment['id']." gönderisi zaman aşımı nedeniyle iptal edildi.<br>";
            }
          }
        }
      }
    }
    //return;

  }

  public function cancelShipment(){

    $shipment_id = $this->request->get['referans'];//Shipment ID Alınır

    if(!is_null($shipment_id)) {


      $this->load->model('extension/shipping/softomikargo');
      $shipment_sql = $this->model_extension_shipping_softomikargo->queryShipmentId($shipment_id, $this->customer->getId());

      if(count($shipment_sql) == 0){

        $shipment_sql = $this->model_extension_shipping_softomikargo->queryShipmentReference($shipment_id, $this->customer->getId());

        if(count($shipment_sql)>0){
          $shipment_id = $shipment_sql['id'];
        }

      }


      $referans = $shipment_sql['referans'];

      if(count($shipment_sql)>0){

        $process_sql = $this->model_extension_shipping_softomikargo->getShipmentProcess($shipment_sql['id']);

        if(count($process_sql)>0){

          $json['success'] = false;
          $json['warning'] = "Gönderi Kargo Tarafında Fiziksel İşlem Görmüş. İptal Gerçekleştirilemez.";

        } else {

          $cargokey = $shipment_sql['cargokey'];

          $this->load->model('extension/shipping/cargo/'.$cargokey);

          $return = $this->{model_extension_shipping_cargo_.$cargokey}->cancelShipment($referans);

          $this->load->model('extension/shipping/softomikargo');

          /*$query = $this->model_extension_shipping_softomikargo->queryShipmentOrder($shipment_sql['order_id'], $this->customer->getId());

      		$shipment_id = $query['id'];*/

          $product_ids = $this->model_extension_shipping_softomikargo->queryCustomerShipmentProducts($shipment_id);

          foreach ($product_ids as $key => $value) {
            $this->model_extension_shipping_softomikargo->productChangeStatus($value['product_id'], $shipment_sql['order_id'], 20);
          }

          $delete_sql = $this->model_extension_shipping_softomikargo->deleteShipment($shipment_id);

          if($delete_sql){
            $json['success'] = true;
            $json['warning'] = "Kargo Gönderisi İptal Edildi.";
          } else {
            $json['success'] = false;
            $json['error'] = "Gönderi iptali gerçekleştirilemedi.";
          }
        }
        //$json['error'] = "Bu siparişinize daha önce kargo referansı oluşturmuşsunuz. Kargo fiziksel işlem görmemiş ise İPTAL edebilir yeniden kargo referansı oluşturabilirsiniz. Referansı oluşturulmamış bir ürünü kargolamak için ürünleri kargolardıktan sonra takip kodunu bu sayfanın altındaki ilgili forma giriniz.";
        return $this->response->setOutput(json_encode($json));

      }
    }

  }


  public function tracking(){

    $this->load->model('extension/shipping/softomikargo');

    $this->cancelWaitingShipment();

    $cargos = $this->model_extension_shipping_softomikargo->getAllCargo();

    foreach($cargos as $i=>$v){

      $this->load->model('extension/shipping/cargo/'.$v['cargokey']);

    }
    // WAIT durumundaki kargo işlemlerini kontrol et
    $wait = $this->model_extension_shipping_softomikargo->getWaitShipment();

    if(is_array($wait)){
//print_r($wait);exit;
      foreach($wait as $key => $value){
        echo $value['id']." Kontrol Ediliyor.<br>";

        // Kargonun işlem görüp görmediğini kontrol et
        $return = $this->{model_extension_shipping_cargo_.$value['cargokey']}->shipmentControl($value['referans']);
        // İşlem Gördüyse işlemi kaydet ve statüsünü değiştir. Mail ve SMS gönderimi mümkün
        if($return['process']){
          $return['shipment_id'] = $value['id'];
          $shipment = $this->model_extension_shipping_softomikargo->insertProcess($return);

          if($shipment){

            $shipmentProducts = $this->model_extension_shipping_softomikargo->getShipmentProducts($value['id']);

            foreach ($shipmentProducts as $key => $val) {
              echo $value['id']." Kargoya Verildi.<br>";
              // Ürünleri Kargoya Verildi
              $this->model_extension_shipping_softomikargo->productChangeStatus($val['product_id'], $val['order_id'], 3);

            }

          }

        }

      }

    }
    //print_r('DUR'); exit;

    // Süreçte olan Kargoları Kontrol et
    $process = $this->model_extension_shipping_softomikargo->getProcessShipment();
//print_r($process); exit;
    if(is_array($process)){

      foreach($process as $key => $value){

        //echo "BURDA";
        // Kargonun işlem görüp görmediğini kontrol et
        $return = $this->{model_extension_shipping_cargo_.$value['cargokey']}->shipmentControl($value['referans']);
        $return['shipment_id'] = $value['id'];

        // İşlem Gördüyse işlemi kaydet ve statüsünü değiştir. Mail ve SMS gönderimi mümkün
        $this->model_extension_shipping_softomikargo->updateProcess($return);

        if($return['process'] == 'COMPLATED'){
          echo "TAMAM";
          $shipment = $this->model_extension_shipping_softomikargo->complatedShipment($value['id']);

          if($shipment){

            $shipmentProducts = $this->model_extension_shipping_softomikargo->getShipmentProducts($value['id']);

            foreach ($shipmentProducts as $key => $val) {

              // Ürünler Müşteri Onayında
              $this->model_extension_shipping_softomikargo->productChangeStatus($val['product_id'], $val['order_id'], 17);

            }

          }

        }

      }

    }
    // İşlem görmüş kargoları process tablosuna aktar ve WAIT durumunu PROCESS olarak değiştir.

    // Kargo İşlem gördüyse referans çıktısı yerine gönderi takip sayfasını göster

    // Kargo işlem gördüyse Kargoya Verildi olarak işaretle

    // Kargo Teslim edildiyse Müşteri Onayında durumuna çevir.

    // Müşteri onayına girdiğinde tarih kaydetmelisin ki 14 günlük süre başlasın




  }


  public function services(){

    $cargokey = isset($this->request->get['cargokey']) ? $this->request->get['cargokey'] : NULL;
    $referans = isset($this->request->get['referans']) ? $this->request->get['referans'] : NULL;
    $islev = isset($this->request->get['islev']) ? $this->request->get['islev'] : NULL;

    if(!is_null($referans)){

        // Seçili Kargo modelini çağır
        $this->load->model('account/customerpartner');
        $this->load->model('extension/shipping/softomikargo');

        //$c = explode('-',$order_id)

        $shipment_sql = $this->model_extension_shipping_softomikargo->queryReference($referans);

        if(count($shipment_sql) > 0){
          //return $this->response->setOutput(json_encode(json_decode($shipment_sql['bilgi']),JSON_UNESCAPED_UNICODE));

          if(is_null($islev)){

            return $this->response->setOutput($shipment_sql['bilgi']);

          } elseif($islev == 'new'){

            if($shipment_sql['islem'] != 'WAIT'){

              return $this->response->setOutput(json_encode(array('error' => '2', 'message' => 'saleCode ve shipmentCode daha önce kayıt edilmiştir.')));

            } else {

              $data['shipment_id']    = $shipment_sql['id'];
              $data['barcode']        = $this->request->post['shipmentCode'];
              $data['gonderi_ucreti']  = '';
              $data['gonderi_agirlik'] = '';
              $data['teslim_alan']     = '';
              $data['takip_url']       = $this->request->post['cargotrachUrl'];
              $data['islem_tarihi']    = date('Y-m-d H:i:s');
              $data['son_islem']       = 'ÇIKIŞ ŞUBESİNDE - '.$this->request->post['cargoCompanyBranch'];
              $data['bilgi']           = '';

              $s = $this->model_extension_shipping_softomikargo->insertProcess($data);

              if($s){
                //$data = array('Approved' => 'Tamam', 'ErrorCode' => '', 'ErrorMessage'=>'');
                return $this->response->setOutput(json_encode(array('Approved' => 'Tamam', 'ErrorCode' => '', 'ErrorMessage'=>'')));
              }
            }

          }

        }else{
          return $this->response->setOutput(json_encode(array('error' => '1', 'message' => 'Kargo Bulunamadı')));
        }

      }

  }


}

?>
