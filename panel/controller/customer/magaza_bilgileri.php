<?php
class ControllerCustomerMagazaBilgileri extends Controller {
  public function index() {

    $data['store_name'] = '';
    $data['store_logo'] = '';
    $data['store_banner'] = '';
    $data['store_address'] = '';
    $data['store_district'] = '';
    $data['store_city'] = '';
    $data['store_country'] = '';
    $data['store_url'] = '';
    $data['store_bank_name'] = '';
    $data['store_iban'] = '';
    $data['store_bank_username'] = '';
    $data['official'] = '';
    $data['taxoffice'] = '';
    $data['taxnumber'] = '';

    $data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('customer/magaza_bilgileri', $data));
  }
}
