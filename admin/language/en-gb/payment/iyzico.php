<?php
// Heading
$_['heading_title'] = 'iyzico Checkout Form';

// Text
$_['text_iyzico'] = '<a href="https://www.iyzico.com/" target="_blank"><img src="view/image/payment/iyzico.svg" alt="iyzico" title="iyzico"/></a>';
$_['text_enabled'] = 'Enabled';
$_['text_disabled'] = 'Disabled';

// Entry
$_['general_select']  = 'Select';
$_['entry_api_channel'] = 'Api Type';
$_['entry_api_live'] = 'Live';
$_['entry_api_sandbox'] = 'Sandbox / Test';
$_['entry_api_id_live'] = 'Api Key';
$_['entry_secret_key_live'] = 'Secret Key';
$_['entry_order_status'] = 'Order Status';
$_['extension_status'] = 'Extension Status';
$_['entry_sort_order'] = 'Sort Order';
$_['entry_class'] = 'Form Class';
$_['entry_class_popup'] = 'Popup';
$_['entry_class_responsive'] = 'Responsive';
$_['entry_cancel_order_status'] = 'Cancel Order Status';

$_['entry_buyer_protection'] = 'Buyer Protection Logo';
$_['entry_overlay_bottom_left'] = 'Bottom Left';
$_['entry_overlay_bottom_right'] = 'Bottom Right';
$_['entry_overlay_closed'] = 'Hidden';


$_['order_status_after_payment_tooltip'] = 'Order status after successful payment';
$_['order_status_after_cancel_tooltip'] = 'Order status after successful cancel';
$_['api_channel_tooltip'] = 'API Type Live or Sandbox';
$_['buyer_protection_tooltip'] = 'Secure Shopping Information Bar';


$_['api_connection_text'] = 'Api Connection Status';
$_['api_connection_success'] = 'Success';
$_['api_connection_failed'] = 'Failed';

$_['iyzico_settings']                   = 'Settings';
$_['iyzico_webhook']                    = 'Webhook Integration';
$_['iyzico_webhook_url_description']    = "Don't forget to do webhook Integration.";
$_['iyzico_webhook_url_key_error']      = "Webhook URL Error! Please, try these steps respectively.  1- Refresh this page 1-2 times. 2- If webhook url doesn't create , Please, after delete the iyzico plugin, re-install the iyzico plugin and save module settings. 3- Send an email to entegrasyon@iyzico.com";

$_['pwi_status_error']                  = "Pay with iyzico module is not enable!";
$_['pwi_status_error_detail']           = "You can not access Settings of iyzico Checkout Form Module without installing the pay with iyzico module.";
$_['dev_iyzipay_opencart_link']         = "https://dev.iyzipay.com/tr/acik-kaynak/opencart";
$_['dev_iyzipay_detail']                = "Complete the installation of the Opencart 2.x - Pay with iyzico module via dev.iyzipay:";