<?php
// Heading
$_['heading_title']    = 'Gelişmiş Kargo Modülü';
$_['tab_general']    = 'Genel Ayarlar';
$_['tab_rate']    = 'Yöntem Ayarı';
$_['module_status']    = 'Modül Durumu';
$_['text_debug']    = 'Hata ayıklama:';
$_['text_description']    = 'Sipariş E-postasında Nakliye Açıklamasını Göster:';
$_['text_desc_estimate_popup']    = 'Nakliye ve Vergileri Tahmin Et';
$_['text_desc_delivery_method']    = 'Teslimat Metodu Adımı';
$_['text_desc_confirmation']    = 'Onay Adımı';
$_['text_desc_site_order_detail']    = 'Müşteri Siparişi Ayrıntısı';
$_['text_desc_admin_order_detail']    = 'Yönetici Siparişi Ayrıntısı';
$_['text_desc_order_email']    = 'E-posta';
$_['text_desc_order_invoice']    = 'Fatura';
$_['text_edit']        = 'Edit Gelişmiş Kargo Modülü';

// Text 
$_['text_extension']  = 'Uzantıları';
$_['text_shipping']    = 'Nakliye';
$_['text_success']     = 'Başarı: Gelişmiş Kargo Modülünü değiştirdiniz!';

$_['text_select_all']    = 'Hepsini seç';
$_['text_unselect_all']    = 'Tümünü Seç seçimini kaldır';
$_['text_any']    = 'Herhangi';
$_['text_heading']    = 'Başlık';
$_['no_unit_row']    = 'Bu yöntem için birim aralığı yok!';

//group
$_['text_group_shipping_mode']    = 'Gruplar grubu';
$_['entry_group']    = 'Grubu Seç';
$_['entry_group_tip']    = 'Grubu Seç';
$_['text_group_none']    = 'Yok';
$_['text_no_grouping']    = 'Gruplama Yok';
$_['text_lowest']    = 'En düşük';
$_['text_highest']    = 'En yüksek';
$_['text_average']    = 'Ortalama';
$_['text_sum']    = 'Toplam';
$_['text_and']    = 'VE';
$_['text_group_type']    = 'Grup Türü';
$_['text_group_limit']    = 'Gösterilecek yöntem sayısı';
$_['text_group_name']    = 'Grup ismi';
$_['text_method_group']    = 'Yöntemler grubu';
$_['tip_method_group']    = 'Nakliye yöntemlerinizi 10 gruba bölebilirsiniz ve her bir gruba farklı işlem uygulanabilir. En yüksek / en düşük işlem durumunda, göstermek istediğiniz yöntem sayısını sınırlandırabilirsiniz. İşte grup modu var <br /><b>En düşük</b>: En düşük maliyetle yalnızca nakliye yöntemi gösterilecektir.<br /><b>En yüksek</b>: En yüksek maliyetle Yalnızca Nakliye yöntemi gösterilir.<br /><b>Ortalama</b>: Tüm gönderim bedelleri ortalaması alınır.<br /><b>Toplam</b>: Tüm nakliye masrafları toplanacak.<br /><b>VE</b>: Grubun Tüm Nakliye Yöntemleri AND işlemi olarak kabul edilecektir. Grubun yalnızca ilk gönderim yöntemi, ancak grubun tüm gönderim yöntemleri tüm kuralları doğrularsa gösterilir.';
$_['tab_general_global']    = 'Grup Seçeneği';
$_['tab_general_general']    = 'Genel';


$_['text_add_new_method']    = 'Yeni Yöntem Ekle';
$_['text_remove']    = 'Kaldır';
$_['text_general']    = 'Genel';
$_['text_criteria_setting']    = 'Kriterler Ayarı';
$_['text_category_product']    = 'Kategori / Ürün';
$_['text_price_setting']    = 'Fiyat Ayarı';
$_['text_others']    = 'Diğerleri';
$_['text_zip_postal']    = 'Zip / Postal';
$_['text_enter_zip']    = 'Zip / Posta Kodunu Giriniz';
$_['text_zip_rule']    = 'Posta / Posta kuralı';
$_['text_zip_rule_inclusive']    = 'Yalnızca girilen zip / posta kodları için';
$_['text_zip_rule_exclusive']    = 'Girilen posta kodu / posta kodu hariç hepsi için';
$_['text_group_limit']    = 'Yöntem sayısı gösterileri:';
$_['text_method_remove']    = 'Yöntemi kaldır';
$_['text_method_copy']    = 'Bu yöntemi kopyala';

$_['text_coupon']    = 'Kupon';
$_['text_enter_coupon']    = 'Kupon kodunu girin';
$_['text_coupon_rule']    = 'Kupon kuralı';
$_['text_coupon_rule_inclusive']    = 'Yalnızca girilen kupon kodları için';
$_['text_coupon_rule_exclusive']    = 'Girilen kupon kodları dışındaki herkes için';

$_['text_rate_type']    = 'Oran Türü';
$_['text_rate_flat']    = 'Düz';
$_['text_rate_quantity']    = 'miktar';
$_['text_rate_weight']    = 'Ağırlık';
$_['text_rate_volume']    = 'hacim';
$_['text_rate_total']    = 'Toplam';
$_['text_grand_total']    = 'Genel Toplam';
$_['text_rate_total_coupon']    = 'Kupon olmadan Toplam';

$_['text_rate_total_method']    = 'Toplam - Metoda Özgü';
$_['text_rate_sub_total_method']    = 'Alt Toplam - Metoda Özgü';
$_['text_rate_quantity_method']    = 'Miktar - Metoda Özgü';
$_['text_rate_weight_method']    = 'Ağırlık - Metoda Özgü';
$_['text_rate_volume_method']    = 'Cilt - Metoda Özgü';

$_['text_rate_sub_total']    = 'Alt Toplam';
$_['text_unit_range']    = 'Birim Aralığı';
$_['text_delete_all']    = 'Hepsini sil';
$_['text_csv_import']    = 'CSV İçe Aktarma';
$_['text_start']    = 'Başlama';
$_['text_end']    = 'Son';
$_['text_cost']    = 'Maliyet';
$_['text_qnty_block']    = 'Birim Blok Başına';
$_['text_add_new']    = 'Yeni ekle';
$_['text_final_cost']    = 'Son Maliyet';
$_['text_final_single']    = 'Tek';
$_['text_final_cumulative']    = 'birikimli';
$_['text_percentage_related']    = 'İle ilgili yüzdesi';
$_['text_percent_sub_total']    = 'Alt Toplam';
$_['text_percent_total']    = 'Toplam';
$_['text_percent_shipping']    = 'Nakliye maliyeti';
$_['text_percent_sub_total_shipping']    = 'Nakliye ile alt toplam';
$_['text_percent_total_shipping']    = 'Nakliye ile birlikte toplam';
$_['text_price_adjustment']    = 'Fiyat ayarlaması';
$_['text_price_min']    = 'Min';
$_['text_price_max']    = 'Max';
$_['text_price_add']    = 'niteleyici';
$_['text_days_week']    = 'Haftanın günleri';
$_['text_time_period']    = 'Zaman dilimi';
$_['text_sunday']    = 'Pazar';
$_['text_monday']    = 'Pazartesi';
$_['text_tuesday']    = 'Salı';
$_['text_wednesday']    = 'Çarşamba';
$_['text_thursday']    = 'Perşembe';
$_['text_friday']    = 'Cuma';
$_['text_saturday']    = 'Cumartesi';
$_['text_logo']    = 'Logo URLsi';

$_['entry_all']    = 'Tümü';
$_['entry_any']    = 'herhangi';
$_['text_mask_price']    = 'Ödeme sırasında maliyet yerine bu metni göster';

// Entry 
$_['entry_weight_include']       = 'Ağırlığı Adına Ekle:';
$_['entry_cost']       = 'Nakliye maliyeti:';
$_['entry_name']       = 'Yöntem Adı:';
$_['entry_desc']       = 'Açıklama:';
$_['entry_order_total']       = 'Sipariş Toplam Aralığı:';
$_['entry_order_weight']       = 'Ağırlık Aralığı:';
$_['entry_quantity']       = 'Miktar Aralığı:';
$_['entry_to']       = 'için';
$_['entry_order_hints']       = 'Lütfen geçerli değilse 0 (sıfır) girin';
$_['entry_tax']        = 'Vergi Sınıfı:';
$_['entry_geo_zone']   = 'Coğrafi bölge:';
$_['entry_status']     = 'Durum:';
$_['entry_sort_order'] = 'Sıralama düzeni:';
$_['entry_customer_group'] = 'müşteri grubu:';
$_['entry_store'] = 'mağaza:';
$_['entry_manufacturer'] = 'Üretici firma:';
$_['store_default'] = 'Varsayılan';
$_['text_all'] = 'herhangi';
$_['text_category'] = 'Kategori Kural';
$_['text_multi_category'] = 'Çok Kategorili Kural';
$_['text_category_any'] = 'Herhangi bir kategori için';
$_['text_category_all'] = 'Seçili kategorilere sahip olmalı';
$_['text_category_least'] = 'Seçilen kategorilerin herhangi biri';
$_['text_category_least_with_other'] = 'Seçilen kategorilerin herhangi biri diğerleriyle';
$_['text_category_except'] = 'Seçilen kategoriler dışında';
$_['text_category_exact'] = 'Yalnızca seçilen kategoriler için';
$_['text_category_except_other'] = 'Seçilen kategoriler hariç olmak üzere';

$_['entry_category']     = 'Kategoriler';
$_['text_product'] = 'Ürün Kuralları';
$_['text_product_any'] = 'Herhangi bir ürün için';
$_['text_product_all'] = 'Seçilen ürünler olmalı';
$_['text_product_least'] = 'Seçilen ürünlerden herhangi biri';
$_['text_product_least_with_other'] = 'Seçilen ürünlerden herhangi biri diğerleriyle';
$_['text_product_exact'] = 'Yalnızca seçilen ürünler için';
$_['text_product_except'] = 'Seçilen ürünler hariç';
$_['text_product_except_other'] = 'Seçilen ürünler hariç diğer ürünler';
$_['entry_product']     = 'Ürünler';


$_['text_manufacturer_rule'] = 'Üretici Kuralları';
$_['text_manufacturer_any'] = 'Herhangi bir üretici için';
$_['text_manufacturer_all'] = 'Seçilen üreticilere sahip olmalı';
$_['text_manufacturer_least'] = 'Seçilen üreticilerin herhangi biri';
$_['text_manufacturer_least_with_other'] = 'Seçilen üreticilerin herhangi biriyle';
$_['text_manufacturer_exact'] = 'Sadece seçilen üreticiler için';
$_['text_manufacturer_except'] = 'Seçilen üreticiler hariç';
$_['text_manufacturer_except_other'] = 'Seçilen üreticiler hariç olmak üzere';

$_['text_dimensional_weight'] = 'Boyut Ağırlığı';
$_['text_dimensional_weight_method'] = 'Boyutsal Ağırlık - Sadece Yöntem';
$_['text_dimensional_factor'] = 'Boyut / hacim Ağırlık Faktörü';
$_['text_dimensional_overrule'] = 'Boyut / Hacimsel Ağırlıktan daha yüksekse Fiili Ağırlığı düşünün';

$_['button_save_continue'] = 'Kaydet ve devam Et';
$_['ignore_modifier'] = 'Fiyat Ayarlamasını Yoksay <i>Birim blok başına</i> Aktif hale gelme';

$_['entry_group_name']       = 'Grup Yöntemi Adı (İsteğe Bağlı)';
$_['text_admin_name'] = 'Ekran adı';
$_['text_admin_name_tip'] = 'Yalnızca yönetici için. Sipariş ayrıntılarıyla gösterilecek.';

/* tooltip */
$_['tip_group_name']       = 'Yöntem adı verilen grup adı ile değiştirilir. Özgün yöntem adını ve fiyatını grup adına eklemek mümkündür. Ad koyarken, kural # nın önündeki yöntem numarasıdır. Örneğin, # 1 ilk yöntem adı anlamına gelir. Benzer şekilde fiyat için @ kullanın. Örneğin, @ 1 ilk yöntem fiyatını ifade eder.';
$_['tip_group_limit']       = 'Gösterilecek en yüksek / en düşük yöntem sayısını sınırlayabilirsiniz. Varsayılan olarak yalnızca bir yöntem gösterecektir';
$_['tip_weight_include']       = 'Gönderim yöntemi adının yanında sepet ağırlığını gösterir';
$_['tip_sorting_own']       = 'Gelişmiş Kargo yöntemlerine göre sıralama sırası';
$_['tip_status_own']       = 'Yalnızca bu belirli yöntemi etkinleştir / devre dışı bırak';
$_['tip_store']       = 'Lütfen bu gönderim yönteminin çalışacağı Mağazaları seçin';
$_['tip_geo']       = 'Lütfen bu nakliye yönteminin çalışacağı coğrafi bölgeleri seçin';
$_['tip_manufacturer']       = 'Lütfen bu gönderim yönteminin çalışacağı üreticiyi seçin';
$_['tip_customer_group']       = 'Lütfen bu gönderim yönteminin çalışacağı müşteri gruplarını seçin';
$_['tip_zip']       = 'Lütfen, bu gönderim yönteminin çalışacağı zip / posta adresini girin';
$_['tip_coupon']       = 'Lütfen, bu gönderim yönteminin çalışacağı kupon kodunu girin';
$_['tip_category']       = '<b>Herhangi bir kategori için</b>: Herhangi bir kategori için geçerlidir.<br /><b>Must have selected categories</b>: Seçilen kategorilerin alışveriş sepetinde diğer kategorilere sahip olması gerekir.<br /><b>Seçilen kategorilerin herhangi biri diğerleriyle</b>: Seçilen kategoriden en az biri alışveriş sepetinde diğer kategorilere sahip olmalıdır.<br /><b>Seçilen kategorilerin herhangi biri</b>: Seçilen kategoriden en az biri alışveriş sepetinde olmalıdır. Diğer kategorilere izin verilmez. <br /> <b> Yalnızca seçilen kategoriler için </ b>: Tüm alışveriş kategorisi seçili kategorilerde olmalıdır. Diğer kategorilere izin verilmez. <br /> <b> Seçili kategorileri hariç </ b>: Alışveriş sepetinin seçilen kategorilerin hiçbirine sahip olmaması gerekir. Yalnızca seçili olmayan kategorilere izin verilir. <br /> <b> Seçili kategorileri başkaları ile hariç tutma </ b>: Alışveriş sepeti, yalnızca en az bir başka kategoriye sahip olan kategorileri seçmiş olabilir';
$_['tip_multi_category']       = 'Bu seçenek yalnızca ürününüz birden fazla kategoriye ait olduğunda geçerlidir. <br /> <b> Tam </ b>: Ürün kategorilerinden bağımsız olarak değerlendirme aşamasında aşağıda listelenen seçili kategorileri dikkate alacaktır. <br /> <B> Herhangi </ b>: Bir ürünü seçtiğinizi ya da seçtiğinizi değerlendirirken tüm kategoriler dikkate alınacaktır. Emin değilseniz, varsayılan değeri tutun ';
$_['tip_product']       = '<B> Herhangi Bir Ürün İçin </ b>: Herhangi bir Ürün için geçerlidir. <br /> <b> Seçili ürünler olmalı </ b>: Seçilen ürünlerin alışveriş sepetinde diğer ürünlerle olması gerekir. <br /> <b > Seçilen ürünlerden herhangi biri diğerleriyle </ b>: Seçili ürünün en az biri diğer ürünlerle alışveriş sepetinde olmalıdır. <br /> <b> Seçili ürünlerden herhangi biri </ b>: En az bir tane Seçilen ürünün alışveriş sepetinde olması gerekir. Diğer ürünlere izin verilmez. <br /> <b> Sadece seçilen ürünler için </ b>: Tüm sepet ürünleri seçilen ürünler olmalıdır. Diğer ürünlere izin verilmez. <br /> <b> Seçilen ürünü hariç </ b>: Alışveriş sepetinde seçilen ürün bulunmamalıdır. Yalnızca seçilmemiş ürünlere izin verilir. <br /> <b> Başkaları ile seçilen ürünleri hariç </ b>: Alışveriş sepeti, yalnızca en az bir başka ürüne sahipse ürün seçmiş olabilir';
$_['tip_manufacturer_rule']       = '<b>For any Manufacturers</b>: Valid for any Manufacturers.<br /><b>Must have selected manufacturer</b>: Selected manufacturer must have in the shopping cart with  other manufacturer.<br /><b>Any of the selected manufacturer with others</b>: At least one of the selected pmanufacturer must have in the shopping cart with  other manufacturer.<br /><b>Any of the selected manufacturer</b>: At least one of the selected pmanufacturer must have in the shopping cart.  Other manufacturer are not allowed.<br /><b>Only for the selected manufacturer</b>: All cart manufacturer should be in the selected manufacturer. Other manufacturer are not allowed.<br /><b>Except the selected manufacturer</b>: Shopping cart should not have any of the selected manufacturer. Only non-selected manufacturer are allowed . <br /><b>Except the selected manufacturer with others</b>: Shopping cart may have selected manufacturers only if cart have at least any other manufacturer';
$_['tip_rate_type']       = '<b>Flat</b>: Only a single flat value.<br /><b>Quantity</b>: Total quantities of the shopping cart.<br /><b>Weight</b>: Total weight of the shopping cart.<br /><b>Volume</b>: Total volume of the shopping cart.<br /><b>Total</b>: Cart Sub-total with tax.<br /><b>Sub-Total</b>: Cart Sub-total without tax. <br /><b>Grand Total</b>: Grand Total except shipping cost. <br /> <b>* Method Only</b>: It will consider weight/quantity/total/sub-total of the products for this method only. Only applicable when either category or product or manufacturer rule is active.';
$_['tip_cost']       = 'Percentage is allowed. e.g. 5%, 99.99 etc.';
$_['tip_unit_start']       = 'Counting will start from this value';
$_['tip_unit_end']       = 'Counting will end in this value';
$_['tip_unit_price']       = 'If \'<i>Birim Blok Başına</i>\' 0 ise, bu Satırın seçilen aralığı için Fiyat olacaktır. <br /> \ <i> Birim Blok Başına </ i> \ 0dan büyükse, fiyat \ Birim blok başına \ olacaktır. Yüzeye örneğin; 5,% 7 vb.';
$_['tip_unit_ppu']       = '<b>Birim blok başına </ b>: Nakliye ücretini birim blok bazında hesaplamak isterseniz bu özelliği kullanabilirsiniz. Örneğin, her 3 öğe için her 3 öğe için 5 $ şarj etmek isterseniz toplam puan 10a eşittir. Dolayısıyla, Start = 0, End = 10, Cost = 5 ve Unit block = 3 olarak ayarlamanız gerekecektir. Basitçe <i> Birim Blok Başına </ i>, her n. Birim için fiyat anlamına gelir.';
$_['tip_single_commulative']       = '<b>Tek </ b>: Yalnızca ilgili satır aralık arasında düşünülür. <br /> <b> Kümülatif </ b>: Birden fazla satır dikkate alınır. Dolayısıyla maliyet, başlangıç satırından ilgili satıra kadar birikimli olacaktır';
$_['tip_percentage']       = 'Lütfen hangi yüzdeli maliyetin hesaplanacağını seçin. <I> Nakliye Maliyeti, Nakliye ile birlikte alt toplam ve fiyat ayarlama bölümünün değiştiricisine uygulanabilen nakliye ile toplam. </ I>';
$_['tip_price_adjust']       = 'Nihai fiyatı ayarlayabilirsiniz. <br /> <b> Min </ b>: Son fiyat Minden küçükse, Min kabul edilir. <br /> <b> Max </ b>: Son fiyat Maxden büyükse, Max kabul edilir. <br /> <b> Değiştirici </ b>: Son fiyata eklenecek, çarpılacak / çıkarılacak / bölünecek bir operatörle (+, -, *, /) herhangi bir değer operatöre bağlıdır. Örneğin: +5.2, nihai fiyata 5.2 eklenecek demektir.';
$_['tip_day']       = 'Lütfen bu gönderim yönteminin geçerli olacağı günleri seçin';
$_['tip_time']       = 'Lütfen bu gönderim yönteminin zaman dilimini ayarlayın.';
$_['tip_heading']       = 'Sitedeki nakliye bölümünün başlığı';
$_['tip_status_global']       = 'Modülün Genel Durumu';
$_['tip_sorting_global']       = 'Global Sıralama Siparişi diğer nakliye modüllerine göre';
$_['tip_grouping']       = 'Kurallar yöntem gruplarıyla aynıdır, ancak bu gruplama grup gruplarıyla birlikte çalışacaktır. Aynı <i> Sırala Sırası </ i> değerine sahip gönderim yöntemleri grup olarak kabul edilecektir.';
$_['tip_debug']       = 'Ödeme sırasında belirli bir gönderim yönteminin görünmemesine neden olan nedeni gösterecektir';
$_['tip_desc']       = 'İsteğe bağlı alan. Sitede nakliye yöntemi adı altında destekleyici küçük bir açıklama gösterecektir.';
$_['tip_import']       = '<b>CSV içeri aktarımı için: </ b> CSV, benzer <br /> <br /> 1,10,4.99,0,0 <br /> 11,20,5,99,2,0,21,30 olmalıdır. , 6.99,0,0';
$_['tip_text_logo'] = 'İsteğe bağlı alan. Yalnızca resim URL sine izin verilir. Bu logo, gönderim başlığından hemen önce görüntülenecektir ';
$_['tip_postal_code']='Virgülle ayrılmış. Joker karakterler destekler (*,?) Ve Aralık Desteği. <br /> <b> Örnek: </ b> <br /> 12345,443300-443399,9843 *, 875 * 22,45433?, S3432? 2 <br /> <b> Açıklama </ b>: < Br /> 12345: Tek Posta Kodu <br /> 443300-443399: Posta Kodu 443300dan 443399a başlar <br /> 9843 *: 9843 ile başlayan herhangi bir kod <br /> 875 * 22: ile başlayan herhangi bir kod 875 ve biter 22? <br /> 45433 ?: 45433 ile başlayan ve herhangi bir tek alfa-sayısal karakterle biten herhangi bir kod. <br /> SE-1-10: Posta kodu önek SE ile birlikte 1den 10a, yani SE9dan başlayacaktır. <br /> PA-1-10-NK: Posta kodu, ön ek PK ve son ek NK ile birlikte 1den 10a kadar başlar PA9NK ' ;
$_['text_partial']    = 'Kısmen İzin Ver';
$_['tip_partial'] ='Kısmi blok fiyatlandırmayı dikkate alacaktır. Örneğin, Kişi Başına Birim Bloğu 5dir ve kullanıcı yalnızca 3ü seçerse, 3 birim için ücretlendirilir';
$_['text_yes']    = 'Evet';
$_['text_no']    = 'Hayır';
$_['text_additional']    = 'Ek Birim Fiyat';
$_['tip_additional'] ='Bu seçenek, yukarıdaki birim aralıklardan herhangi bir eşleşme bulunmaması durumunda nakliye maliyetini en son bulunan fiyat temelinde otomatik olarak hesaplar; yani fiyat = (Son bulunan fiyat + (ek fiyat * ek birimler)) ';
$_['text_sort_type']    = 'Tarafından sipariş';
$_['text_sort_manual']    = 'Manuel';
$_['text_sort_price_asc']    = 'Fiyata Göre Artan';
$_['text_sort_price_desc']    = 'Fiyata Göre Azalan';
$_['tip_text_sort_type'] ='Orders of the Shipping methods in the site. Manual order will be based on the <b>Sort Order</b> input value.';

$_['tip_weight']       = 'Additional weight range option. This weight ranges will also validate with other rules if you provide.';
$_['tip_total']       = 'Additional sub total range option.This total ranges will also validate with other rules if you provide.';
$_['tip_quantity']       = 'Additional quantity range option. This quantity ranges will also validate with other rules if you provide.';

$_['text_export']    = 'Dışa Aktar';
$_['tip_export'] = 'Tüm gönderim verilerini dışa aktarma';
$_['text_import']    = 'İçe Aktar';
$_['tip_import'] = 'Uyarı! Dosyadan içe aktarma mevcut yöntemlerin üzerine yazacaktır.';
$_['tab_import_export']    = 'İçe Aktar / Dışa Aktar';

$_['text_country']    = 'ülke';

$_['text_equation']    = 'Nihai Denklem';
$_['tip_equation']    = 'Herhangi bir aritmetik denklem. Verildiğinde, son nakliye bedeli bu denklemi temel alarak hesaplanacaktır.';
$_['text_equation_help'] = 'Kullanılabilir parametreler {cartTotal}, {cartQnty}, {cartWeight} {shipping}, {modifier}. Ex. {cartTotal}-5*{cartQnty}';

$_['text_option'] = 'Ürün Seçenek Kural';
$_['text_option_any'] = 'Herhangi bir Ürün Seçeneği için';
$_['text_option_all'] = 'Seçilen seçeneklere sahip olmalı';
$_['text_option_least'] = 'Seçilen seçeneklerden herhangi biri';
$_['text_option_least_with_other'] = 'Seçilen seçeneklerden herhangi biri ile diğerleri';
$_['text_option_exact'] = 'Yalnızca seçilen seçenekler için';
$_['text_option_except'] = 'Seçilen seçenekler haricinde';
$_['text_option_except_other'] = 'Diğerleriyle seçilen seçenekler haricinde';
$_['entry_option']     = 'Ürün Seçenekleri';
$_['tip_option']       = '<b>Herhangi bir Ürün Seçeneği için </ b>: Herhangi bir seçenek için geçerlidir. <br /> <b> Seçilen seçeneklere sahip olmalı </ b>: Seçili seçeneklerin, alışveriş sepetinde başka seçeneklerle olması gerekir. <br /> <b> Herhangi </ B>: Seçili ürünün en az biri diğer seçeneklerle birlikte alışveriş sepetinde bulunmalıdır. <br /> <b> Seçilen seçeneklerden herhangi biri </ b>: En az bir Seçilen ürünün alışveriş sepetinde olması gerekir. Diğer seçeneklere izin verilmiyor. <br /> <b> Yalnızca seçilen seçenekler için </ b>: Tüm alışveriş sepeti seçenekleri seçili seçenekler arasında olmalıdır. Diğer seçeneklere izin verilmemektedir. <br /> <b> Seçilen ürünü hariç </ b>: Alışveriş sepetinde seçilen seçeneklerden hiçbirinin olması gerekmez. Yalnızca seçili olmayan seçeneklere izin verilir. <br /> <b> Diğerleriyle seçilen seçenekleri hariç </ b>: Alışveriş sepeti, yalnızca en azından herhangi bir seçeneğe sahipse, seçenekleri seçmiş olabilir ';
// Error
$_['error_permission'] = 'Uyarı: Gelişmiş Kargo Modülüyu değiştirme izniniz yok!';
$_['error_filetype'] = 'Geçersiz dosya türü. Yalnızca CSVye izin verilir';
$_['error_upload'] = 'Hata! Yükleyemiyor';
$_['error_no_data'] = 'Alınacak hiçbir veri bulunamadı';
$_['error_partial'] = 'Kısmi yükleme hatası. Lütfen yükleme boyutu limitinizi kontrol edin.';
$_['error_import'] = 'Lütfen içe aktarmak için geçerli bir dosya seçin';

?>