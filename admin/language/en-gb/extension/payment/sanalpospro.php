<?php
// Heading
$_['heading_title']				= 'EticSoft SanalPOS PRO! Payment';

// Text
$_['text_payment']       = 'Ödeme Metodları';
$_['text_success']       = 'Başarılı: Banka havalesi detayları güncellendi!';
$_['text_edit']          = 'Sanalpospro Entegrasyon Bilgileriniz';
$_['text_sanalpospro']                      = '<a href="http://sanalpospro.com/" target="_blank"><img src="../catalog/view/theme/default/image/sanalpospro/logo.png" alt="EticSoft SanalPOS PRO!" title="SanalPOS PRO!" style="border: 1px solid #EEEEEE;" /></a>';
