<?php
// Heading
$_['heading_title'] = 'iyzico Ödeme Formu';

// Text
$_['text_iyzico'] = '<a href="https://www.iyzico.com/" target="_blank"><img src="view/image/payment/iyzico.svg" alt="iyzico" title="iyzico"/></a>';
$_['text_enabled'] = 'Açık';
$_['text_disabled'] = 'Kapalı';

// Entry
$_['general_select']  = 'Seçiniz';
$_['entry_api_channel'] = 'Api Türü';
$_['entry_api_live'] = 'Live';
$_['entry_api_sandbox'] = 'Sandbox / Test';
$_['entry_api_id_live'] = 'Api Anahtarı';
$_['entry_secret_key_live'] = 'Güvenlik Anahtarı';
$_['entry_order_status'] = 'Sipariş Durumu';
$_['extension_status'] = 'Eklenti Durumu';
$_['entry_sort_order'] = 'Sıralama';
$_['entry_class'] = 'Form Görünümü';
$_['entry_class_popup'] = 'Popup';
$_['entry_class_responsive'] = 'Responsive';
$_['entry_cancel_order_status'] = 'İptal Durumu';

$_['entry_buyer_protection'] = 'Korumalı Alışveriş Logosu';
$_['entry_overlay_bottom_left'] = 'Alt Sol';
$_['entry_overlay_bottom_right'] = 'Alt Sağ';
$_['entry_overlay_closed'] = 'Gizli';


$_['order_status_after_payment_tooltip'] = 'Sipariş alındığında müşteriye gösterilecek durum bilgisi';
$_['order_status_after_cancel_tooltip'] = 'Sipariş iptal edildiğinde müşteriye gösterilecek durum bilgisi';
$_['api_channel_tooltip'] = 'Api türünüz Live veya Sandbox';
$_['buyer_protection_tooltip'] = 'iyzico ile Öde logosu ile, müşterilerinize 7/24 Canlı Destek, %100 Sorunsuz Alışveriş, Tek Tıkla Ödeme Kolaylığı vurgularını yapabilir, sitenizin ödeme yöntemi olarak iyzico kullandığını belirtip, müşterinizin güvenle alışveriş yapmasını sağlayabilirsiniz.';


$_['api_connection_text'] = 'Api Bağlantı Durumu';
$_['api_connection_success'] = 'Başarılı';
$_['api_connection_failed'] = 'Başarısız';

$_['iyzico_settings']                   = 'Ayarlar';
$_['iyzico_webhook']                    = 'Webhook Entegrasyonu';
$_['iyzico_webhook_url_description']    = "Webhook entegrasyonunu yapmayı unutmayınız.";
$_['iyzico_webhook_url_key_error']      = "Webhook URL üretilemedi. Lütfen sırasıyla bu adımları deneyin. 1- Bu sayfayı 1-2 defa yenileyin. 2- Webhook URL üretilmediyse lütfen plugini kaldırıp, yeniden kurun ve ayarları kaydedin. 3- Sorun devam ederse entegrasyon@iyzico.com'a mail atınız.";

$_['pwi_status_error']              = "iyzico İle Öde modülü aktif değil!";
$_['pwi_status_error_detail']       = "iyzico ile Öde modülünü kurmadan iyzico Ödeme Formu ayarlarına erişemezsiniz.";
$_['dev_iyzipay_opencart_link']     = "https://dev.iyzipay.com/tr/acik-kaynak/opencart";
$_['dev_iyzipay_detail']            = "Opencart 2.x - iyzico ile Öde modülünün kurulumunu tamamlayınız: ";