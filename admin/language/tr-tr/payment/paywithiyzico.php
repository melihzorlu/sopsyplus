<?php
// Heading
$_['heading_title']    = 'iyzico ile Öde';

// Text
$_['text_module']      = 'Ödeme';
$_['text_paywithiyzico'] = '<a href="https://www.iyzico.com/isim-icin/iyzico-ile-ode" target="_blank"><img width="30%" src="view/image/payment/pay-with-iyzico-tr.svg" alt="iyzico" title="iyzico"/></a>';
$_['extension_status'] = 'Eklenti Durumu';
$_['text_enabled'] = 'Açık';
$_['text_disabled'] = 'Kapalı';

// Order
$_['entry_order_status'] = 'Sipariş Durumu';
$_['entry_status'] = 'Status';
$_['entry_cancel_order_status'] = "İptal Durumu";
$_['order_status_after_payment_tooltip'] = 'Başarılı ödeme sonrasında müşteriye gösterilecek sipariş durum bilgisi';
$_['order_status_after_cancel_tooltip'] = 'Sipariş iptal edildiğinde müşteriye gösterilecek sipariş durum bilgisi';
$_['entry_sort_order'] = 'Sıralama';

// Error
$_['error_permission'] = 'Yetkiniz yok!';
$_['error_status'] = 'Geçersiz Eklenti Durumu';
$_['error_order_status_id'] = 'Geçersiz Sipariş Durumu!';
$_['error_cancel_order_status_id'] = 'Geçersiz İptal Durumu!';