<?php
// Heading
$_['heading_title']    				= 'Üye Hesabım Sayfası';

// Text
$_['text_extension']   				= 'Extensions';

$_['text_success'] = 'Başarı: Üye Hesabım Sayfasını Değiştirdiniz!';
$_['text_edit'] = 'Üye Hesabım Sayfası Düzenle';
$_['text_first'] = 'Hesap İstatistikleri';

$_['text_template_1'] = 'Tasarım 1';
$_['text_template_2'] = 'Tasarım 2';
$_['text_template_3'] = 'Tasarım 3';

$_['text_type_setting'] = 'Çeşitli';
$_['text_type1'] = 'Resim';
$_['text_type2'] = 'Simge';
$_['text_third'] = 'Renk';
$_['text_registered'] = 'Kayıtlı';
$_['text_unregistered'] = 'BM Kayıtlı';

// Giriş
$_['entry_status'] = 'Durum';
$_['entry_latest_order'] = 'Son Siparişler';
$_['entry_adresi'] = 'Adres';
$_['entry_total_wishlist'] = 'Toplam İstek Listesi';
$_['entry_downloads'] = 'Toplam İndirme Sayısı';
$_['entry_reward_points'] = 'Toplam Ödül Puanı';
$_['entry_total_orders'] = 'Toplam Sipariş';
$_['entry_total_transactions'] = 'Toplam İşlem';
$_['entry_file_ext_allowed'] = 'İzin Verilen Dosya Uzantıları';
$_['entry_file_mime_allowed'] = 'İzin Verilen Dosya Mim Türleri';
$_['entry_template']      		   	= 'Tasarım Seç';
$_['entry_required'] = 'Gerekli';
$_['entry_affiliate_type'] = 'Hesap Türü';
$_['entry_field_value'] = 'Değer';
$_['entry_field_error'] = 'Hata Mesajı';
$_['entry_field_placeholder'] = 'Yer Tutucu';
$_['entry_type'] = 'Tip';
$_['entry_option_value'] = 'Değer Ad';
$_['entry_sort_order'] = 'Sıralama Düzeni';
$_['entry_icontype'] = 'Simge Türü';
$_['entry_image'] = 'Resim';
$_['entry_iconclass'] = 'Simge';
$_['entry_theme_color'] = 'Tema Rengi';
$_['entry_text_color'] = 'Metin Rengi';
$_['entry_border_color'] = 'Kenarlık Rengi';

$_['entry_title'] = 'Başlık';
$_['entry_link'] = 'Bağlantı';
$_['entry_customcss'] = 'Özel Css';

$_['text_upload'] = 'Profil Resmi';

$_['button_add_link'] = 'Bağlantı Ekle';
$_['button_save'] = 'Değişiklikleri Kaydet';

// Yardım et
$_['help_file_max_size'] = 'Resim Galerisinde yükleyebileceğiniz maksimum resim dosyası boyutu. Bayt olarak girin. ';
$_['help_file_ext_allowed'] = 'Hangi dosya uzantılarının yüklenmesine izin verildiğini ekleyin. Her değer için yeni bir satır kullanın. ';
$_['help_file_mime_allowed'] = 'Hangi dosya tipi türlerinin yüklenmesine izin verildiğini ekleyin. Her değer için yeni bir satır kullanın. ';

$_['help_title'] = 'Başlığı Ayarla.';
$_['help_sort_order'] = 'Siparişi Sırala';


$_['help_customcss'] = 'Üye Hesabım Sayfası Kişiselleştirmeye Özel CSS Ekle';
$_['help_theme_color'] = 'Hesap Panosu Tema Rengini Ayarla. Bu Renk Sınırlar Üzerine Uygulanacak Üye Hesabım Sayfası Üzerine Arkaplan vb.';
$_['help_text_color'] = 'Hesap Panosu Metin Rengini Ayarla. Bu Renk Üye Hesabım Sayfası Üzerine Uygulanacak Metin! ';



// Hata
$_['error_warning'] = 'Uyarı: Lütfen hataları dikkatlice kontrol edin!';
$_['error_permission'] = 'Uyarı: Üye Hesabım Sayfasını değiştirme izniniz yok!';
$_['error_title'] = 'Başlık 1 ile 255 karakter arasında olmalı!';
$_['error_link'] = 'Bağlantı Gerekli!';
$_['error_image'] = 'Lütfen Resim Seçin!';
$_['error_icon_class'] = 'Lütfen Icon sınıfını seçiniz!';

// Sekme
$_['tab_profile'] = 'Profil Resmi';
$_['tab_template'] = 'Şablonlar';
$_['tab_customer_link'] = 'Hesap Kontrol Paneli Bağlantıları';
$_['tab_affiliate_link'] = 'Ortak Bağlantılar';
$_['tab_support'] = 'Destek';
$_['tab_link'] = 'Bağlantılar';
