<?php
// Text
$_['text_saving']                   = 'Kaydediliyor ...';
$_['text_copying']                  = 'Kopyalanıyor ...';
$_['text_deleting']                 = 'Siliniyor ...';
$_['text_loading']                  = 'Yükleniyor ...';
$_['text_click_edit']               = 'Tıkla Düzenle ...';
$_['text_double_click_edit']        = 'Çift Tıkla Düzenle ...';
$_['text_image_manager']            = 'Resim Yönetimi';
$_['text_batch_edit']               = 'Toplu Düzenleme';
$_['text_autocomplete']             = 'Otomatik Tamamla';
$_['text_notify_customer']          = 'Müşteriye Bildir';
$_['text_confirm_delete']           = 'Silmeyi Onaylayın!';
$_['text_are_you_sure']             = 'Silme işlemi geri alınamaz! Devam etmek istediğinizden emin misiniz?';
$_['text_toggle_navigation']        = 'Gezinmeyi aç / kapat';
$_['text_toggle_dropdown']          = 'Açılır menüyü aç / kapat';
$_['text_filter']                   = 'Filtre';
$_['text_clear_filter']             = 'Filtreyi Temizle';
$_['text_first_name']               = 'Ad';
$_['text_last_name']                = 'Soyad';

// Buttons
$_['button_add']                    = 'Yeni Ekle';
$_['button_ok']                     = 'Onayla';

// Actions
$_['action_edit']                   = 'Düzenle';

// Columns
$_['column_action']                 = 'Eylem';
$_['column_address_1']              = 'Adres 1';
$_['column_address_2']              = 'Adres 2';
$_['column_approved']               = 'Onaylı';
$_['column_balance']                = 'Sütunları Ayarlar';
$_['column_categories']             = 'Kategoriler';
$_['column_city']                   = 'Şehir';
$_['column_clicks']                 = 'Tıklanmalar';
$_['column_code']                   = 'Kod';
$_['column_commission']             = 'Komisyon';
$_['column_company']                = 'Şirket';
$_['column_country']                = 'İl';
$_['column_date_added']             = 'Ekleme Tarihi';
$_['column_date_end']               = 'Bitiş Tarihi';
$_['column_date_start']             = 'Başlama Tarihi';
$_['column_description']            = 'Açıklama';
$_['column_email']                  = 'E-Posta';
$_['column_fax']                    = 'Fax';
$_['column_id']                     = 'ID';
$_['column_ip']                     = 'IP Adres';
$_['column_logged']                 = 'Müşteri Girişi';
$_['column_name']                   = 'Ad';
$_['column_orders']                 = 'Siparişler';
$_['column_postcode']               = 'Posta Kodu';
$_['column_products']               = 'Ürünler';
$_['column_region']                 = 'Bölge / Şehir';
$_['column_selector']               = '<ayırıcı>';
$_['column_shipping']               = 'Ücretsiz Kargo';
$_['column_status']                 = 'Durumu';
$_['column_tax']                    = 'Vergi ID';
$_['column_telephone']              = 'Telefon';
$_['column_total']                  = 'Tutar Toplamı';
$_['column_tracking_code']          = 'İzleme Kodu';
$_['column_type']                   = 'Tipi';
$_['column_uses_customer']          = 'Müşteri Başına Kullanım';
$_['column_uses_total']             = 'Kupon Başına Kullanım';

// Errors
$_['error_warning']                 = '<strong>Uyarı!</strong> Lütfen formu kontrol edin!';
$_['error_update']                  = '<strong>Uyarı!</strong> Değerler Güncellenemedi!';
$_['error_load_zone']               = '<strong>Uyarı!</strong> Bölge Verileri Yüklenemedi!';
$_['error_load_popup']              = '<strong>Uyarı!</strong> Popup yüklenirken hata oluştu!';
$_['error_ajax_request']            = 'Bir AJAX hatası oluştu!';
$_['error_batch_edit_email']        = 'Toplu e-posta düzenleme mümkün değildir!';
$_['error_batch_edit_code']         = 'Toplu kod düzenleme mümkün değildir!';
$_['error_date_format']             = '<strong>Uyarı!</strong> Tarih Formatı Yanlış! Olması Gereken: \'YYYY-AA-DD SS:DD\'';
$_['error_invalid_date']            = '<strong>Uyarı!</strong> Tarih Yanlış!';
