<?php

// Text
$_['text_success_delete']           = 'Başarılı: %s kampanya(s) silindi!';

// Columns
$_['column_name']                   = 'Kampanya Adı';

// Errors
$_['error_name']                    = 'Uyarı: Kampanya adı 1 ile 32 karakter arasında olmalıdır!';
