<?php

// Text
$_['text_success_copy']             = 'Başarılı: %s özellik grupları kopyalandı!';
$_['text_success_delete']           = 'Başarılı: %s özellik grupları silindi!';

// Actions
$_['action_name']                   = 'Özellik Grup Adı';

// Errors
$_['error_name']                    = 'Özellik Grup Adı 3 ile 64 karakter arasında olmalıdır!';
$_['error_delete']                  = '<strong>Uyarı!</strong> Özellik Grubu silinemedi. Bu özellik grubu kullanılıyor.';