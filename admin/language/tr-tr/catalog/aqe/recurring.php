<?php

// Text
$_['text_success_copy']             = 'Başarılı: %s tekrarlayan profil(ler) kopyalandı!';
$_['text_success_delete']           = 'Başarılı: %s tekrarlayan profil(ler) silindi!';

// Actions
$_['action_name']                   = 'Tekrarlayan Profil Adı';

// Errors
$_['error_group']                   = 'Tekrarlayan Profil Adı 3 ile 255 karakter aralığında olmalıdır!';
$_['error_delete']                  = '<strong>Dikkat!</strong> Tekrarlayan Profil \'%s\' bir üründe kullanılıyor!';
