<?php

// Text
$_['text_success_copy']             = 'Başarılı: %s özellik(ler) kopyalandı!';
$_['text_success_delete']           = 'Başarılı: %s özellik(ler) silindi!';

// Actions
$_['action_name']                   = 'Özellik Adı';

// Errors
$_['error_name']                    = 'Özellik adı 3 ile 64 karakter aralığında olmak zorundadır!';
$_['error_delete']                  = '<strong>Uyarı!</strong> Bu özellik \'%s\' bir üründe kullanılıyor';
