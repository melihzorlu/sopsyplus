<?php

// Text
$_['text_success_delete']           = 'Başarılı: %s bilgi(ler) silindi!';

// Actions
$_['action_title']                   = 'Bilgi Başlığı';

// Errors
$_['error_title']                    = 'Bilgi başlığı 3 ile 64 karakter aralığında olmalıdır!';
$_['error_account']                  = '<strong>Uyarı!</strong> Bilgi sayfası \'%s\' silinemiyor. Bu sayfa üyelik şartlarında bulunuyor!';
$_['error_checkout']                 = '<strong>Uyarı!</strong> Bilgi sayfası \'%s\' silinemiyor. Bu sayfa satış şartlarında bulunuyor!';
$_['error_affiliate']                = '<strong>Uyarı!</strong> Bilgi sayfası \'%s\' silinemiyor. Bu sayfa ortalık şartlarında bulunuyor!';
$_['error_return']                   = '<strong>Uyarı!</strong> Bilgi sayfası \'%s\' silinemiyor. Bu sayfa iade şartlarında bulunuyor!';
$_['error_store']                    = '<strong>Uyarı!</strong> Bilgi sayfası \'%s\' silinemiyor. Bu sayfa mağaza şartlarında bulunuyor!';
