<?php

// Text
$_['text_success_copy']             = 'Başarılı: %s kategori(ler) kopyalandı!';
$_['text_success_delete']           = 'Başarılı: %s kategori(ler) silindi!';
$_['text_rebuilding']               = 'Tekrar düzenle ...';

// Actions
$_['action_name']                   = 'Kategori Adı';

// Errors
$_['error_name']                    = 'Kategori Adı 1 ile 255 karakter aralığında olmak zorundadır!';
