<?php

// Text
$_['text_success_copy']             = 'Başarılı: %s indirilen(ler) kopyalandı!';
$_['text_success_delete']           = 'Başarılı: %s indirilen(ler) silindi!';

// Actions
$_['action_name']                   = 'İndirilen Adı';

// Errors
$_['error_name']                    = 'İndirilen Adı 3 ile 64 karakter aralığında olmak zorundadır!';
$_['error_delete']                  = '<strong>Dikkat!</strong> Bu indirme \'%s\' bir üründe kullanılıyor';
