<?php

// Text
$_['text_success_copy']             = 'Başarılı: %s Seçenek(s) kopyalandı!';
$_['text_success_delete']           = 'Başarılı: %s Seçenek(s) silindi!';

// Action
$_['action_group_name']             = 'Seçenek Adı';
$_['action_option_full']            = 'Seçenek Değerleri';

// Errors
$_['error_name']                    = 'Seçenek Adı 1 ile 128 karakter aralığında olmalıdır!';
$_['error_option_value']            = 'Seçenek Değer Adı 1 ile 128 karakter aralığında olmalıdır!';
$_['error_delete']                  = '<strong>Uyarı!</strong> Seçenek \'%s\' başka bir üründe kullanılıyor';
