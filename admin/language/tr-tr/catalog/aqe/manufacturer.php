<?php

// Text
$_['text_success_copy']             = 'Başarılı: %s üretici(ler) kopyalandı!';
$_['text_success_delete']           = 'Başarılı: %s üretici(ler) silindi!';

// Errors
$_['error_name']                    = 'Üretici Adı 2 ile 64 karakter aralığında olmalıdır!';
$_['error_delete']                  = '<strong>Uyarı!</strong> Üretici \'%s\' bir üründe kullanılıyor!';
