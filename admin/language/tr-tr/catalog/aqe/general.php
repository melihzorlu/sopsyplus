<?php
// Text
$_['text_saving']                   = 'Kaydedildi ...';
$_['text_copying']                  = 'Kopyalandı ...';
$_['text_deleting']                 = 'Silindi ...';
$_['text_loading']                  = 'Yükleniyor ...';
$_['text_click_edit']               = 'Tıkla Düzenle ...';
$_['text_double_click_edit']        = 'Çift Tıkla Düzenle ...';
$_['text_image_manager']            = 'Resim Yönetimi';
$_['text_batch_edit']               = 'Toplu Düzenleme';
$_['text_autocomplete']             = 'Otomatik Tamamla';
$_['text_notify_customer']          = 'Müşteriye Bildir';
$_['text_store']                    = 'Mağazalar';
$_['text_filters']                  = 'Fitreler';
$_['text_filter']                   = 'Filtreler';
$_['text_name']                     = 'Ad';
$_['text_title']                    = 'Başlık';
$_['text_seo']                      = 'SEO Kelimeler';
$_['text_group_name']               = 'Grup Adı';
$_['text_confirm_delete']           = 'Silmeyi onayla!';
$_['text_are_you_sure']             = 'Silme işlemi geri alınamaz! Devam etmek istediğinden emin misin?';
$_['text_toggle_navigation']        = 'Gezinmeyi aç / kapat';
$_['text_toggle_dropdown']          = 'Açılır menüyü aç / kapat';
$_['text_filter']                   = 'Filte';
$_['text_clear_filter']             = 'Filtreyi Temizle';

// Buttons
$_['button_add']                    = 'Yeni Ekle';
$_['button_ok']                     = 'Onayla';

// Actions
$_['action_name']                   = 'Ürün Adı';
$_['action_download']               = 'İndirilenler';
$_['action_category']               = 'Kategoriler';
$_['action_store']                  = 'Mağazalar';
$_['action_attributes']             = 'Özellikler';
$_['action_discounts']              = 'İndirimler';
$_['action_images']                 = 'Diğer Resşmler';
$_['action_filter']                 = 'Filtreler';
$_['action_filters']                = 'Filtreler';
$_['action_options']                = 'Seçenekler';
$_['action_recurrings']             = 'Yinelenenler';
$_['action_related']                = 'Benzer Ürünler';
$_['action_downloads']              = 'Benzer İndirmeler';
$_['action_specials']               = 'Kampanyalar';
$_['action_descriptions']           = 'Açıklamalar';
$_['action_seo']                    = 'SEO Kelimeler';
$_['action_option_values']          = 'Seçenek Değerleri';
$_['action_option_value']           = 'Seçenek Değerleri';
$_['action_view']                   = 'Görüntüle';
$_['action_edit']                   = 'Düzenle';
$_['action_attr']                   = 'A';
$_['action_dscnt']                  = 'D';
$_['action_img']                    = 'I';
$_['action_fltr']                   = 'F';
$_['action_opts']                   = 'O';
$_['action_rec']                    = 'C';
$_['action_rel']                    = 'RP';
$_['action_dls']                    = 'RD';
$_['action_spcl']                   = 'S';
$_['action_desc']                   = 'T';
$_['action_vw']                     = 'V';
$_['action_ed']                     = 'E';
$_['action_opt_val']                = 'OV';

// Columns
$_['column_action']                 = 'İşlemler';
$_['column_address_1']              = 'Adres 1';
$_['column_address_2']              = 'Adres 2';
$_['column_amount']                 = 'Miktar';
$_['column_approved']               = 'Onaylı';
$_['column_attribute_group']        = 'Özellik Grubu';
$_['column_author']                 = 'Yazar';
$_['column_balance']                = 'Balance';
$_['column_bottom']                 = 'Bottom';
$_['column_categories']             = 'Kategoriler';
$_['column_category']               = 'Kategori';
$_['column_city']                   = 'Şehir';
$_['column_clicks']                 = 'Tıklamalar';
$_['column_code']                   = 'Kod';
$_['column_column']                 = 'Sütunlar';
$_['column_comment']                = 'Yorum';
$_['column_commission']             = 'Komisyon';
$_['column_company']                = 'Şirket';
$_['column_country']                = 'Şehir';
$_['column_customer']               = 'Müşteri';
$_['column_customer_group']         = 'Müşteri Grubu';
$_['column_customer_id']            = 'Müşteri ID';
$_['column_cycle']                  = 'Cycle';
$_['column_date_added']             = 'Eklenme Tarihi';
$_['column_date_available']         = 'Date Available';
$_['column_date_end']               = 'Bitiş Tarihi';
$_['column_date_modified']          = 'Değiştirilme Tarihi';
$_['column_date_ordered']           = 'Sipariş Tarihi';
$_['column_date_start']             = 'Başlangı Tarihi';
$_['column_description']            = 'Açıklama';
$_['column_discount']               = 'İndirim';
$_['column_download']               = 'Downloads';
$_['column_duration']               = 'Duration';
$_['column_ean']                    = 'EAN';
$_['column_email']                  = 'E-Mail';
$_['column_fax']                    = 'Fax';
$_['column_filename']               = 'Dosya Adı';
$_['column_filter']                 = 'Filtreler';
$_['column_frequency']              = 'Sıklık';
$_['column_from_email']             = 'Gönderici Mail';
$_['column_from_name']              = 'Gönderici Adı';
$_['column_group_name']             = 'Grup Adı';
$_['column_height']                 = 'Yükseklik';
$_['column_image']                  = 'Resim';
$_['column_id']                     = 'ID';
$_['column_ip']                     = 'IP Adres';
$_['column_isbn']                   = 'ISBN';
$_['column_jan']                    = 'JAN';
$_['column_length']                 = 'Uzunluk';
$_['column_length_class']           = 'Uzunluk Sınıfı';
$_['column_location']               = 'Location';
$_['column_logged']                 = 'Müşteri Girişi';
$_['column_manufacturer']           = 'Üretici Firma';
$_['column_mask']                   = 'Mask';
$_['column_message']                = 'Message';
$_['column_minimum']                = 'Minimum Adet';
$_['column_model']                  = 'Model';
$_['column_mpn']                    = 'MPN';
$_['column_name']                   = 'Ad';
$_['column_newsletter']             = 'Bülten';
$_['column_opened']                 = 'Açıldı';
$_['column_option_value']           = 'Seçenek Değerleri';
$_['column_order_id']               = 'Sipariş ID';
$_['column_orders']                 = 'Siparişler';
$_['column_parent']                 = 'Ana';
$_['column_points']                 = 'Puan';
$_['column_postcode']               = 'Posta Kodu';
$_['column_price']                  = 'Fiyat';
$_['column_product']                = 'Ürün';
$_['column_product_id']             = 'Ürün ID';
$_['column_products']               = 'Ürünler';
$_['column_quantity']               = 'Miktar';
$_['column_rating']                 = 'Değerlendirme';
$_['column_region']                 = 'Bölge / Eyalet';
$_['column_requires_shipping']      = 'Kargo Gerekli';
$_['column_return_id']              = 'Dönüş ID';
$_['column_return_action']          = 'İade İşlemi';
$_['column_return_reason']          = 'İade Nedeni';
$_['column_return_status']          = 'İade Durumu';
$_['column_safe']                   = 'Güvenli';
$_['column_selector']               = '<selector>';
$_['column_seo']                    = 'SEO Kelimeler';
$_['column_shipping']               = 'Ücretsiz Kargo';
$_['column_sku']                    = 'SKU / Barkod';
$_['column_sort_order']             = 'Sıralama';
$_['column_status']                 = 'Durum';
$_['column_stock_status']           = 'Stokta Yok Durumu';
$_['column_store']                  = 'Mağazalar';
$_['column_subtract']               = 'Stoktan Düş';
$_['column_tag']                    = 'Product Tags';
$_['column_tax']                    = 'Vergi ID';
$_['column_tax_class']              = 'Vergi Sınıfı';
$_['column_telephone']              = 'Telefon';
$_['column_text']                   = 'Metin';
$_['column_theme']                  = 'Tema';
$_['column_title']                  = 'Başlık';
$_['column_to_email']               = 'E-mail';
$_['column_to_name']                = 'Name';
$_['column_top']                    = 'Üst';
$_['column_total']                  = 'Toplam Tutar';
$_['column_tracking_code']          = 'Takip kodu';
$_['column_trial_cycle']            = 'Trial cycle';
$_['column_trial_duration']         = 'Trial duration';
$_['column_trial_frequency']        = 'Trial frequency';
$_['column_trial_price']            = 'Trial price';
$_['column_trial_status']           = 'Trial status';
$_['column_type']                   = 'Type';
$_['column_upc']                    = 'UPC';
$_['column_uses_customer']          = 'Müşteri Başına Kullanım';
$_['column_uses_total']             = 'Kupon Başına Kullanım';
$_['column_view_in_store']          = 'Mağazada Görüntüle';
$_['column_viewed']                 = 'Görüntülenen';
$_['column_weight']                 = 'Ağırlık';
$_['column_weight_class']           = 'Ağırlık Sınıfı';
$_['column_width']                  = 'Genişlik';

// Errors
$_['error_warning']                 = '<strong>Hata!</strong> Hatalar için lütfen formu dikkatlice kontrol edin!';
$_['error_update']                  = '<strong>Hata!</strong> Değer güncellenemedi!';
$_['error_load_name']               = '<strong>Hata!</strong> Ad/İsim verileri yüklenemedi!';
$_['error_load_zone']               = '<strong>Hata!</strong> Bölge verileri yüklenemedi!';
$_['error_load_popup']              = '<strong>Hata!</strong> Açılır pencere verileri yüklenemedi!';
$_['error_return_action']           = 'İade işlemi gerekli!';
$_['error_ajax_request']            = 'Bir AJAX hatası oluştu!';
$_['error_batch_edit_seo']          = 'SEO Kelimeler toplu düzenleme yapılamaz';
$_['error_batch_edit_email']        = 'E-Mail toplu düzenleme yapılamaz!';
$_['error_batch_edit_filters']      = 'Filterler toplu düzenleme yapılamaz!';
$_['error_date_format']             = '<strong>Hata!</strong> Tarih formatı hatalı!  \'YYYY-MM-DD HH:MM\' Şeklinde olmalı';
$_['error_invalid_date']            = '<strong>Hata!</strong> Tarih geçerli değil!';
$_['error_duplicate_seo_keyword']   = '<strong>Warning!</strong> SEO Kelime kullanımda!';
