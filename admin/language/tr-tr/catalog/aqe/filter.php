<?php

// Text
$_['text_success_copy']             = 'Başarılı: %s filtre(ler) kopyalandı!';
$_['text_success_delete']           = 'Başarılı: %s filtre(ler) silindi!';

// Action
$_['action_group_name']             = 'Filtre Grup Adı';
$_['action_filter_full']            = 'Filtreler';

// Errors
$_['error_group']                   = 'Filtre Grup Adı 1 ile 64 karakter arasında olmalıdır!';
$_['error_name']                    = 'Filtre Adı 1 ile 64 karakter arasında olmalıdır!';
