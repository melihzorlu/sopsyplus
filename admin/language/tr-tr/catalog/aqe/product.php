<?php

// Text
$_['text_success_copy']             = 'Başarılı: %s ürün(ler) kopyalandı!';
$_['text_success_delete']           = 'Başarılı: %s ürün(ler) silindi!';
$_['text_special_off']              = 'Normal Fiyatı';
$_['text_special_active']           = 'İndirimli Fiyat aktif';
$_['text_special_expired']          = 'İndirimli Fiyat süresi bitmiş';
$_['text_special_future']           = 'Gelecekteki indirimli fiyat';
$_['text_special_not_available']    = 'İndirimli Fiyat bulunmuyor';
$_['text_name']                     = 'Ürün Adı';
$_['text_tag']                      = 'Ürün Etiketi';

// Text
$_['entry_downloads']               = 'Benzer indirilenler';

// Errors
$_['error_name']                    = 'Ürün Adı 1 ile 255 karakter aralığında olmalıdır!';
