<?php

// Text
$_['text_success_delete']           = 'Başarılı: %s müşteri(ler) silindi!';

// Actions
$_['action_name']                   = 'Müşteri Adı';
$_['action_approve']                = 'Onayla';
$_['action_unlock']                 = 'Kilidi Aç';
$_['action_login']                  = 'Mağazada oturum aç';
