<?php
// Text
$_['text_saving']                   = 'Kaydediliyor ...';
$_['text_copying']                  = 'Kopyalanıyor ...';
$_['text_deleting']                 = 'Siliniyor ...';
$_['text_loading']                  = 'Yükleniyor ...';
$_['text_sending']                  = 'Gönderiliyor ...';
$_['text_click_edit']               = 'Tek Tıkla Düzenle ...';
$_['text_double_click_edit']        = 'Çift Tıkla Düzenle ...';
$_['text_image_manager']            = 'Resim Yönetimi';
$_['text_batch_edit']               = 'Toplu Düzenle';
$_['text_autocomplete']             = 'Otomatik Tamamla';
$_['text_notify_customer']          = 'Müşteriye Bildir';
$_['text_confirm_delete']           = 'Silmeyi Onaylayın!';
$_['text_are_you_sure']             = 'Silme işlemi geri alınamaz! Devam etmek istediğinizden emin misiniz?';
$_['text_toggle_navigation']        = 'Gezinmeyi aç / kapat';
$_['text_toggle_dropdown']          = 'Açılır menüyü aç / kapat';
$_['text_filter']                   = 'Filtre';
$_['text_clear_filter']             = 'Filtreyi Temizle';
$_['text_first_name']               = 'Adı';
$_['text_last_name']                = 'Soyadı';

// Buttons
$_['button_add']                    = 'Yeni Ekle';
$_['button_ok']                     = 'Onayla';

// Actions
$_['action_edit']                   = 'Düzenle';

// Columns
$_['column_action']                 = 'Action';
$_['column_approved']               = 'Approved';
$_['column_customer_group']         = 'Müşteri Grubu';
$_['column_date_added']             = 'Eklenme Tarihi';
$_['column_email']                  = 'E-Posta';
$_['column_fax']                    = 'Fax';
$_['column_image']                  = 'Resim';
$_['column_id']                     = 'ID';
$_['column_ip']                     = 'IP Adresi';
$_['column_name']                   = 'Ad';
$_['column_newsletter']             = 'Bülten';
$_['column_safe']                   = 'Safe';
$_['column_selector']               = '<ayırıcı>';
$_['column_status']                 = 'Durumu';
$_['column_telephone']              = 'Telefon';

// Errors
$_['error_warning']                 = '<strong>Uyarı!</strong> Lütfen formu kontrol edin!';
$_['error_update']                  = '<strong>Uyarı!</strong> Değerler Güncellenemedi!';
$_['error_load_popup']              = '<strong>Uyarı!</strong> Popup yüklenirken hata oluştu!';
$_['error_ajax_request']            = 'Bir AJAX hatası oluştu!';
$_['error_batch_edit_email']        = 'Toplu e-posta düzenleme mümkün değildir!';
$_['error_batch_edit_code']         = 'Toplu kod düzenleme mümkün değildir!';
$_['error_date_format']             = '<strong>Uyarı!</strong> Tarih Formatı Yanlış! Olması Gereken: \'YYYY-AA-GG SS:DD\'';
$_['error_invalid_date']            = '<strong>Uyarı!</strong> Tarih Yanlış!';
