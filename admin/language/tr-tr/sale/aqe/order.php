<?php

// Text
$_['text_success_update']           = '<strong>Başarılı!</strong> %s sipariş(s) güncellendi!';

// Error
$_['error_login']                   = '<strong>Hata!</strong> Yanlış API Anahtarı';
$_['error_failed_update']           = '<strong>Gata!</strong> %s sipariş(s) güncellenemedi!';
