<?php
// Text
$_['text_saving']                   = 'Kaydediliyor...';
$_['text_copying']                  = 'Kopyalanıyor ...';
$_['text_deleting']                 = 'Siliniyor ...';
$_['text_loading']                  = 'Yükleniyor ...';
$_['text_sending']                  = 'Gönderiliyor ...';
$_['text_click_edit']               = 'Tıkla Düzenle...';
$_['text_double_click_edit']        = 'Çift Tıkla Düzenle ...';
$_['text_image_manager']            = 'Resim Yönetimi';
$_['text_batch_edit']               = 'Toplu Düzenleme';
$_['text_autocomplete']             = 'Otomatik Tamamla';
$_['text_notify_customer']          = 'Müşteriye Bildir';
$_['text_confirm_delete']           = 'Silmeyi Onaylayın!';
$_['text_are_you_sure']             = 'Silme işlemi geri alınamaz! Devam etmek istediğinizden emin misiniz?';
$_['text_toggle_navigation']        = 'Gezinmeyi aç / kapat';
$_['text_toggle_dropdown']          = 'Açılır menüyü aç / kapat';
$_['text_filter']                   = 'Filtre';
$_['text_clear_filter']             = 'Filtreyi Temizle';
$_['text_first_name']               = 'Ad';
$_['text_last_name']                = 'Soyad';

// Buttons
$_['button_add']                    = 'Yeni Ekle';
$_['button_ok']                     = 'Onayla';

// Actions
$_['action_edit']                   = 'Düzenle';

// Columns
$_['column_action']                 = 'Eylem';
$_['column_amount']                 = 'Miktar';
$_['column_approved']               = 'Onaylanmış';
$_['column_code']                   = 'Kod';
$_['column_comment']                = 'Yorum';
$_['column_customer']               = 'Müşteri';
$_['column_customer_group']         = 'Müşteri Grubu';
$_['column_customer_id']            = 'Müşteri ID';
$_['column_date_added']             = 'Eklenme Tarihi';
$_['column_date_modified']          = 'Değiştirilme Tarihi';
$_['column_date_ordered']           = 'Sipariş Tarihi';
$_['column_email']                  = 'E-Posta';
$_['column_fax']                    = 'Fax';
$_['column_from_email']             = 'E-Posta';
$_['column_from_name']              = 'Ad';
$_['column_image']                  = 'Resim';
$_['column_id']                     = 'ID';
$_['column_ip']                     = 'IP Adresi';
$_['column_message']                = 'Mesaj';
$_['column_model']                  = 'Model';
$_['column_name']                   = 'Ad';
$_['column_newsletter']             = 'Bülten';
$_['column_opened']                 = 'Açıldı';
$_['column_order_id']               = 'Sipariş ID';
$_['column_product']                = 'Ürün';
$_['column_product_id']             = 'Ürün ID';
$_['column_quantity']               = 'Miktar';
$_['column_return_id']              = 'İade ID';
$_['column_return_action']          = 'İade Eylemi';
$_['column_return_reason']          = 'İade Nedeni';
$_['column_return_status']          = 'İade Durumu';
$_['column_safe']                   = 'Güvenli';
$_['column_selector']               = '<ayırıcı>';
$_['column_status']                 = 'Durumu';
$_['column_telephone']              = 'Telefon';
$_['column_theme']                  = 'Tema';
$_['column_to_email']               = 'E-Posta ile';
$_['column_to_name']                = 'Ad ile';

// Errors
$_['error_warning']                 = '<strong>Uyarı!</strong> Lütfen formu kontrol edin!';
$_['error_update']                  = '<strong>Uyarı!</strong> Değerler Güncellenemedi!';
$_['error_load_popup']              = '<strong>Uyarı!</strong> Popup yüklenirken hata oluştu!';
$_['error_ajax_request']            = 'Bir AJAX hatası oluştu!';
$_['error_batch_edit_email']        = 'Toplu e-posta düzenleme mümkün değildir!';
$_['error_batch_edit_code']         = 'Toplu kod düzenleme mümkün değildir!';
$_['error_date_format']             = '<strong>Uyarı!</strong> Tarih Formatı Yanlış! Olması Gereken: \'YYYY-MM-DD HH:MM\'';
$_['error_invalid_date']            = '<strong>Uyarı!</strong> Tarih Yanlış!';
