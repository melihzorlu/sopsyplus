<?php

// Text
$_['text_success_delete']           = 'Başarılı: %s kupon(s) silindi!';

// Actions
$_['action_name']                   = 'Kupon adı';

// Errors
$_['error_name']                    = 'Kupon kodu 3 ile 32 karakter aralığında olmadılıdır!';
$_['error_delete']                  = '<strong>Uyarı!</strong> Kupon kodu \'%s\' şu anda kullanıldığından silinemiyor %s!';
