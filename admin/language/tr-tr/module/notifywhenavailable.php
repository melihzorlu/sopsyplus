<?php
// Heading
$_['heading_title']       				= 'Gelince Haber Ver Modülü';


// Text
$_['text_module']        				= 'Modüller';
$_['text_nwa_success']       			= 'Başarılı. Güncelleme yapıldı.';
$_['text_success_activation']  			= 'Aktif Edildi';
$_['text_enabled']  					= 'Aktif';
$_['text_disabled']   					= 'Pasif';
$_['text_add']   						= 'Ekle';

$_['text_customer_email']       		= 'Müşteri E-posta';
$_['text_customer_name']       			= 'Müşteri Adı';
$_['text_product']   					= 'Ürün';
$_['text_date']  						= 'Tarih';
$_['text_language']   					= 'Dil';
$_['text_actions']   					= 'Eylem';
$_['text_remove']   					= 'Sil';
$_['text_remove_all']  					= 'Tümünü Sil';
$_['text_status']       				= 'Durumu:';
$_['text_status_help']       			= 'Eklenti durumunu aktif/pasif yapabilirsiniz.';
$_['text_scheduled']   					= 'Zamanlanmış Görevler:';
$_['text_scheduled_help']  				= 'Aktif edildiğinde stokta olan ürünleri bekleyen müşterilere otomatik olarak e-posta gönderecektir.';
$_['text_scheduled_help_sec']  			= "Belirli bir ürünün miktarını <strong> Ürünler -> Düzenle </strong> bölümünden değiştirdiğinizde, <strong>Gelince Haber Ver Eklentisi</strong> otomatik olarak, bekleyen müşterileri olup olmadığını kontrol eder. Ancak, hızlı düzenleme modülleri veya excel gibi ürün miktarlarını düzenlemek için başka bir yazılım kullanırsanız, modül görevi tetiklenmez. Bu gibi durumlarda, <strong> Gelince Haber Ver Eklentisi </strong> 'deki zamanlama görevlerini kullanabilirsiniz; bunlar, bekledikleri ürün stoğa geri döndüğünde müşterileri uyarmanıza yardımcı olur.";
$_['text_receive_notifications']		= 'Admin Bildirimi:';
$_['text_receive_notifications_help']   = 'Müşteriye haber verildiğinde admine de bildirim gönder.';
$_['text_type']  						= 'Zamanlama:';
$_['text_type_help']       				= 'Bildirim gönderme zamanı';
$_['text_settings_help']       			= 'Bildirim gönderme zamanı';
$_['text_schedule']   					= 'Zaman:';
$_['text_schedule_help']  				= 'Bildirimler için istenen programı girin. Yukarıdaki seçeneklere bağlı olarak belirli tarihler veya periyodik işlevler için seçenekleri göreceksiniz.';
$_['text_fixed']       					= 'Belirli Tarih';
$_['text_periodic']   					= 'Periyodik';
$_['text_test_cron']   					= 'Test Cron (Zamanlanmış Görev)';
$_['text_test_cron_help']   			= 'Sunucunuzun Cron (Zamanlanmış Görev) komutlarını destekleyip desteklemediğini kontrol etmek için buraya tıklayın.';
$_['text_test_cron_help_sec']  			= 'Zamanlama özelliklerini kullanmak istiyorsanız, sunucunuzun işlevlerini desteklemesi gerekir.<br/><br/>Cron, belirli tarih ve saatlerde komutları yürüten uzun bir çalışma sürecidir. <Strong> Test Cron </strong> butonuna tıklayarak sunucunuzun komutlarını destekleyip desteklemediğini kontrol edebilirsiniz. Sunucunuz Cron işlerini destekliyorsa, ancak bu betik, özelliğin devre dışı olduğunu gösterir, bu da Cron komutlarının otomatik oluşturulmasının devre dışı bırakıldığı anlamına gelir. Bu durumda, sunucu yapılandırma panelinizde bu URL dizesini - <strong> {site_dizini}vendors/notifywhenavailable/sendMails.php 0 http</ strong> - kullanabilirsiniz.';
$_['text_test_cron_help_third']   		= '* SendMails.php den sonra mağaza kimliğinize HTTPS yoluyla erişilip girilmediğine bağlı olarak mağaza kimliğinizi http veya https ile yazmanız gerekir. Varsayılan mağazanız ID <strong>0</strong> .';
$_['text_cron_disabled']   				= 'Sunucum cron desteklemiyor';
$_['text_close']  						= 'Kapat';
$_['text_schedule_cron']   				= 'Bildirim & Cron';
$_['text_alternative_cron']  			= 'Zamanlanmış Görevler İçin Alternatif';
$_['text_alternative_cron_help'] 		= 'Sunucunuz cron komutlarını desteklemiyorsa, <strong> easycron.com </strong>, <strong> setcronjob.com </strong> gibi hizmetleri veya bu özelliği sağlayabilecek diğer kişileri kullanmayı deneyebilirsiniz.<br/><br/>Bunu yapmak için, seçilen hizmete kaydolmanız ve bu URL yi çalıştırmanız gerekir.:';
$_['text_alternative_cron_help_two']    = '* Store_id den sonra mağaza kimliğinizi yazmanız gerekir. Varsayılan mağazanız ID si: <strong>0</strong>.</span><br/><br/>
Ayrıca, Stok Gelince Haber Ver Modülü ayarlarında <strong> Zamanlanmış görevler </strong> özelliğini de etkinleştirmelisiniz..';
 
 
$_['text_stock_status']       			= 'Aktif Olacak Stok Dışı Durumları:';
$_['text_stock_status_help']       		= 'Ürün stoğu 0 olduğunda ve seçili durumda olanlar için gelince haber ver butonu görünür.';
$_['text_admin_notifications']   		= 'Admin Bildirimi:';
$_['text_admin_notifications_help']  	= 'Bir ziyaretçi gelince haber ver bildirimi bıraktığında admine e-posta gönderilir';
$_['text_popup_width']   				= 'Pencere Genişliği:';
$_['text_popup_width_help']   			= 'Pixel';
$_['text_design']   					= 'Pencere Dizaynı:';
$_['text_design_help']  				= 'Kullanılabilir Parametreler:<br /><br />
  {name_field} - İsim<br />

  {email_field} - E-posta <br />
 
  {submit_button} - Gönder';
$_['text_button_label']       			= 'Buton Yazısı:';
$_['text_button_label_help']       		= "Stokta olmayan ürünler için 'Sepete Ekle' butonu yerine bu yazı yazılacaktır.";
$_['text_popup_title']   				= 'Pencere Başlığı:';
$_['text_popup_title_help']  			= 'Stok dışı ürünlerden haberdar olmak istediğinde bir e-posta alacaksınız..';
$_['text_notification_customer']   		= 'Müşteriye Bildirim Gönder:';
$_['text_notification_customer_help']  	= 'Ürün stoğa girdikten sonra müşterilere e-posta gönder.';
$_['text_notification_mail']   			= 'E-posta İçeriği:';
$_['text_notification_mail_help']  		= 'Kullanılabilir Parametreler:<br /><br />
    {name} - Müşteri Adı<br />
    {product_name} - Ürün Adı<br />
    {product_model} - Ürün Kodu<br />';
$_['text_notification_subject']       	= 'E-posta Konusu';
$_['text_email_text']  					= 'E-posta İçeriği:';
$_['text_email_text_help']  			= 'Ürün stokta girdikten sonra gönderilecek e-posta:';
$_['text_email_text_help_sec']  		= 'Kullanılabilir Parametreler:<br />

    {c_name} - Müşteri Adı<br />

    {p_name} - Ürün Adı<br />

    {p_model} - Ürün Kodu<br />

    {p_image} - Ürün Resmi<br />

    {p_link} - Ürün Linki';

$_['text_email_subject']       			= 'E-posta Konusu:';
$_['text_custom_css']       			= 'CSS:';
$_['text_custom_css_help']   			= 'Bu alanı kullanarak, pencere için özel stiller ayarlayabilirsiniz.';
$_['text_export_csv']   				= 'CSV Aktar';
 

$_['preorder_enabled']					= '<strong>Ön Siparişi</strong> aktif edebilirsiniz. Ön Sipariş modülü sadece Ön Sipariş kontrol paneli sekmesi stok durumlarında seçilen ürünler için etkinleştirilir. Gelince haber ver modülü tüm stok dışı durumları için aktif olur.';

$_['text_most_wanted_ofs']  = "Genel Kullanılan (Tükendi)";
$_['text_most_wanted_all_time']  = "Genel Kullanılan (Her Zaman)";

$_['text_count']              = "Say";
