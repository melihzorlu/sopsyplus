<?php
// Heading
$_['heading_title']       = '<b>Anasayfa Ürün Tab Modülü<b>';

// Text
$_['text_module']      = 'Modul';
$_['text_success']     = 'Degiştir!';
$_['text_edit']        = 'Modul Düzenle';

// Entry
$_['entry_name']       = 'Adı';
$_['entry_product']    = 'Özel Ürünler';
$_['entry_limit']      = 'Limit';
$_['entry_width']      = 'Genişlik';
$_['entry_height']     = 'Yükseklik';
$_['entry_status']     = 'Durum';
$_['entry_features']   = 'Sizin İÇin Seçtiklerimiz';
$_['entry_latest'] 	   = 'Yeni Ürünler';
$_['entry_bestseller'] = 'Çok Satanlar';
$_['entry_special']    = 'Kampanyalı';

// Error
$_['error_permission'] = 'Moüdlü Düzenleme Yetkiniz Yok';
$_['error_name']       = 'Uzunluk 3 ile 64 karekter Arasında Olmalı';
$_['error_width']      = 'Genişlik Giriniz!';
$_['error_height']     = 'Yükseklik Giriniz!';
?>