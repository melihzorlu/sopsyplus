<?php
// Heading
$_['heading_title']				= '<b>Html E-Mail Tasarımları Modülü</b> ';


// Text
$_['text_module'] = 'Modüller';
$_['text_image_manager'] = 'Resim Yöneticisi';
$_['text_browse'] = 'Gözat';
$_['text_clear'] = 'Temizle';
$_['text_add_feed'] = 'Yeni feed';
$_['text_success'] = 'Basarili: Html E-Mail Tasarımları Modülü Güncellendi!';
$_['text_info'] = 'Bu site haritası çiftleri içermez ve seo paketi seçeneklerinde etkinleştirilmişse hreflang etiketini entegre eder.';
$_['text_minute'] = 'Dakika';
$_['text_hour'] = 'Saat';
$_['text_day'] = 'Gün';
$_['text_preview'] = 'Önizleme';
$_['text_repeat'] = 'Tekrar';
$_['text_no-repeat'] = 'Tekrar yok';
$_['text_no-repeat_center'] = 'Tekrar yok - Ortali';
$_['text_repeat-x'] = 'Tekrar X';
$_['text_repeat-y'] = 'Tekrar Y';
$_['text_store_select'] = 'Magaza:';
$_['text_warning_layout_zones'] = 'Kullandiginiz temaya bagli olarak bazi bölümlerde uygulanamayabilir.';

// Tabs
$_['text_tab_0'] = 'Ana Düzen';
$_['entry_layout'] = 'Sablon:<span class="help">Varsayilan sablonu seçin</span>';
$_['entry_color_scheme'] = 'Renk Seçimi:<span class="help">Detayli düzenlemek için Tasarim sekmesinden ayarlama yapin.</span>';
$_['entry_logo'] = 'Logo';

// Tasarım
$_['text_tab_1'] = 'Tasarim';
$_['text_save_scheme'] = 'Renk Ayarlarini Kaydet';
$_['text_color_scheme_saved'] = 'Renk Ayarlari Kaydedildi.';
$_['text_page'] = 'Sayfa';
$_['text_top'] = 'Header';
$_['text_header'] = 'Üst Kısım';
$_['text_body'] = 'Sayfa İçeriği';
$_['text_foot'] = 'Alt Kısım';
$_['text_bottom'] = 'Footer';
$_['text_button'] = 'Buton';
$_['text_link'] = 'link';
$_['entry_layout_width'] = 'Genişlik:';
$_['entry_layout_width_i'] = 'Ana düzenin piksel cinsinden genişliği. Yalnızca masaüstü bilgisayarlarda, küçük cihazlar otomatik olarak% 100e ulaşır';
$_['entry_logo_width'] = 'Logo genişliği:';
$_['entry_logo_width_i'] = 'Logo genişliği';
$_['entry_color_text'] = 'Yaz rengi';
$_['entry_color_link'] = 'Link rengi';
$_['entry_color_btn'] = 'Buton rengi';
$_['entry_color_btn_text'] = 'Buton yazi rengi';
$_['text_global'] = 'Genel';
$_['entry_color'] = 'Arkaplan rengi:';
$_['entry_background_image'] = 'Arkaplan resmi:';
$_['entry_repeat'] = 'Arkaplan tekrari:';

// Content editor
$_['text_tab_2'] = 'Mail Içerik Editörü';
$_['text_tab_content_0'] = 'Müsteri bildirimleri';
$_['text_tab_content_1'] = 'Siparis durumu güncelleme';
$_['text_tab_content_2'] = 'Admin bildirimleri';
$_['text_tab_content_3'] = 'Orta içerik';
$_['text_tab_content_4'] = 'Özel email';
$_['entry_from'] = 'Gönderen (From):';
$_['entry_subject'] = 'Konu:<span class="help">Varsayilan degeri kullanmak için bos birakin</span>';
$_['entry_content'] = 'Içerik:<span class="help">Varsayilan degeri kullanmak için bos birakin</span>';
$_['entry_attachment'] = 'Dosya Eki:';
$_['button_upload'] = 'Yükle';
$_['text_loading'] = 'Bekleyin...';
$_['placeholder_file'] = 'Maile dosya ekleyin.';
$_['text_tab_3'] = 'Özel Blok';
// modules
$_['text_tab_3'] = 'Modüller';

  // Fatura
  $_['tab_module_invoice'] = 'Fatura';
  $_['entry_custom_fields']	= 'ÖZel alanları görüntüle';
  $_['entry_custom_fields_i']	= 'Faturadaki müşteri bilgilerinde hangi özel alanların görüntüleneceğini seçin';
  $_['entry_custom_fields_empty']	= 'Özel alanlar tanımlanmadı, bunları Satış> Müşteriler> Özel Alanlar da yapılandırın (örneğin, müşteri KDV Kimliğini eklemek istiyorsanız)';
  $_['entry_total_tax']	= 'KDV Dahil ';
  $_['entry_total_tax_i']	= 'Fiyat ve toplam sütunlarda vergiyi dahil et veya hariç tut';
  $_['entry_customer_comment']	= 'Müşteri yorumlarını görüntüle';
  $_['entry_customer_comment_i']	= 'Müşteri ödeme sırasında bir mesaj bırakırsa, bu seçenekle faturada görüntüleyebilirsiniz.';

  // ürün tanıtımı
  $_['tab_module_prodad'] = 'Ürün tanıtımı';
  $_['tab_prod_ad_latest'] = 'Son';
  $_['tab_prod_ad_featured'] = 'Öne çıkan';
  $_['entry_product'] = 'Ürünler';
  $_['help_product'] = 'Otomatik tamamlama';
  $_['entry_img_size'] = 'Resim boyutu';
  $_['entry_item_number'] = 'Limit';
  $_['entry_item_number_i'] = 'Görüntülenecek maksimum öğe sayısı?';
  $_['entry_per_row'] = 'Satır başına öğe';
  $_['entry_per_row_i'] = 'Tek bir satırda kaç öğe olsun?';
  $_['entry_width'] = 'Genişlik';
  $_['entry_height'] = 'Yükseklik';
  $_['info_product_ad_featured'] = 'E-postalarınıza 3 ürün eklemek için bu modülü kullanın, ürünler tanımladığınız listeden rastgele seçilecektir.<br/>Etiketi bazı e-postalara (veya ortak içeriğe> altbilgi olarak ayarlayarak tüm e-postalara) ekleyin.
<br/><br/>Etiket: <b>{product_featured}</b>';
  $_['info_product_ad_latest'] = 'Mağazanızın en son 3 ürününü eklemek için bu modülü kullanın.<br/>
Etiketi bazı e-postalara (veya ortak içeriğe> altbilgi olarak ayarlayarak tüm e-postalara) ekleyin.<br/><br/>Etiket: <b>{product_latest}</b>';

// config
$_['text_tab_4'] = 'Yapilandirma';
$_['tab_config_1'] = 'Varsayilan degerler';
$_['tab_config_2'] = 'Ayarlar';
$_['tab_config_10'] = 'Gelismis seçenekler';
$_['entry_reset_content'] = 'Email içerigini geri yükle';
$_['btn_reset_content'] = 'E-posta içerigi için varsayilan degerleri geri yükle';
$_['info_msg_reset_content'] = 'Tüm email içerigini (konular ve içerikler) varsayilan degerlerini geri yüklemek için bu islevi kullanin.<br/><br/>Ayrica yeni yüklediginiz dil dosyasinda hata alirsaniz bu seçenegi kullanarak düzeltmeyi deneyebilirsiniz.';
$_['text_confirm_restore_content'] = 'Emin misiniz? Tüm email içerigi sifirlayacak.';
$_['entry_admin_layout'] = 'Admin email ';
$_['entry_admin_layout_i'] = 'Yönetici e-postaları için uygulamak istediğiniz e-posta düzeni nedir?';
$_['entry_bcc_forward'] = 'Müşteri e-postalarını yönlendir';
$_['entry_bcc_forward_i'] = '
Müşteriye gönderilen tüm e-postaların bir bcc kopyasını almak istiyorsanız bu alanı doldurun.';
$_['text_layout_default'] = 'Varsayılan';
$_['text_layout_opencart'] = 'Varsayılan';


$_['text_btn_custom_binder'] = 'Bağlantı bilgisi';

$_['text_tab_about'] = 'Hakkında';

// Content editor
$_['text_type_affiliate'] = 'Ortaklik';
$_['text_type_customer'] = 'Müsteri';
$_['text_type_order'] = 'Siparis';
$_['text_type_admin'] = 'Admin bildirimleri';
$_['text_type_orderstatus'] = 'Siparis durumu';
$_['text_type_affiliate.approve'] = 'Ortaklik onaylandi';
$_['text_type_affiliate.deny'] = 'Ortaklık Reddedildi';
$_['text_type_affiliate.forgotten'] = 'Ortaklik sifremi unuttum';
$_['text_type_affiliate.register'] = 'Ortaklik kayit';
$_['text_type_affiliate.transaction'] = 'Ortaklik islemleri';
$_['text_type_customer.approve'] = 'Müsteri onaylandi';
$_['text_type_customer.deny'] = 'Müşteri reddedildi';
$_['text_type_customer.credit'] = 'Müsteri kredi';
$_['text_type_customer.forgotten'] = 'Müsteri sifremi unuttum';
$_['text_type_customer.register'] = 'Müsteri kayit';
$_['text_type_customer.reward'] = 'Müsteri ödül puani';
$_['text_type_customer.voucher'] = 'Müsteri hediye çekleri';
$_['text_type_order.confirm'] = 'Siparis onaylandi';
$_['text_type_order.update'] = 'Siparis güncellendi';
$_['text_type_order.return'] = 'Siparis iade';
$_['text_type_sale.contact'] = 'Satis mail';
$_['text_type_admin.forgotten'] = 'Yönetici şifremi unuttum';
$_['text_type_admin.order.confirm'] = 'Siparis onaylandi';
$_['text_type_admin.information.contact'] = 'Iletisim';
$_['text_type_admin.customer.register'] = 'Müsteri kayit';
$_['text_type_admin.affiliate.register'] = 'Ortaklik kayit';
$_['text_type_common.top'] = 'Üst Baslik (Top header)';
$_['text_type_common.header'] = 'Baslik (Header)';
$_['text_type_common.footer'] = 'Alt Bilgi';
$_['text_type_common.bottom'] = 'Alt Bilgi';

// Entry
$_['entry_status'] = 'Durum:';

// Info
$_['info_title_default']		= 'Yardim';
$_['info_msg_default']	= 'Bu bölüme özel kullanilabilir etiket yok.';
$_['info_title_custom_mail']	= 'Herhangi bir mail için e-posta şablonu';
$_['info_msg_custom_mail']	= '<p>Modül, e-posta şablonunuzu özel modüllerden gelebilecek herhangi bir postaya eklemenin kolay bir yolunu sunar.
</p>
<ul>
  <li>Dosyayı kopyala _extra_features/pro_email_template_custom_mail.xml into /vqmod/xml/</li>
  <li>Dosyayı düzenleyin ve aşağıdaki endikasyonları takip edin</li>
</ul>
<p>Dosyada değiştirmek için çeşitli değerlere sahip olacaksınız:
</p>
<ol>
  <li style="padding-bottom:10px">
    <code>&lt;Dosya Adı=&quot;<b>catalog/model/module/custom_module.php</b>&quot;&gt;</code><br/>
   Öncelikle, hangi Adresinizden posta gönderiyor olduğunu özel modülünüzde bulmanız gerekir. Bunu yapmak için dizinin içinde bulunduğu modül dosyalarında arama yapabilirsiniz.
 <code>$mail->send();</code><br/>
   Bir kez bulunduğunda, bu dosyaya giden yolu
<file> Etiket
  </li>
  <li style="padding-bottom:10px">
    <code>&lt;search position=&quot;replace&quot; <b>index=&quot;1&quot;</b>&gt;&lt;![CDATA[<b>$mail-&gt;send();</b>]]&gt;&lt;/search&gt;</code><br/>
    Sonra bu dosyada birden fazla $ mail-> send () varsa, hangisini şablona bağlamak istediğinizi ve bu değeri <code>index=""</code><br/>
    Mesela ikincisiyse
$mail->send() sadece dosyada bulundu
 <code>index="2"</code><br/>
    Ayrıca dikkat et
 <code>$mail->send();</code> çünkü $ mail başka bir şey olabilir
 ($email->send(), $message->send(), etc), bu yüzden adapte olmanız gerekecek
 <code>$mail->send();</code> ve <code>\'mail\' => $mail,</code>buna göre değerler.
 </li>
  <li style="padding-bottom:10px">
    <code>\'name\' =&gt; \'<b>Özel modül için e-posta</b>\'</code><br/>

Bu cümleyi bulun ve istediğiniz adla "Özel modül için e-posta" seçeneğini değiştirin.<br/>
  </li>
  <li style="padding-bottom:10px">
    <code>\'<b>tag</b>\' => \'<b>value</b>\',</code><br/>

Ayrıca, etiket olarak kullanılabilen dinamik değerleri ekleyebilir, bu cümleyi bulabilir ve \ value \' kodunu kodunuzda bulunan bir değişkene göre değiştirebilirsiniz.<br/>
    Örneğin <code>\'customer_name\' => $customer_name,</code><br/>

Ardından, dinamik olarak eklemek için şablon içeriğinde {customer_name} öğesini kullanabilirsiniz. <br/>

  </li>
</ol>
<p>Tüm bunlar bittiğinde, bu sayfaya geri dönün ve mesajın içeriğini değiştirebileceksiniz.</p>';
$_['info_title_tags']	= 'Kullanilabilir etiketler';
$_['info_msg_tags']	= '
<div class="infotags">
<h5>Ortak Etiketler</h5>
<p>
<span><b class="tag">{store_name}</b> Magaza adi</span>
<span><b class="tag">{store_url}</b> Magaza URL</span>
<span><b class="tag">{store_email}</b> Magaza Email</span>
<span><b class="tag">{store_phone}</b> Magaza Telefon</span><br/>
<span><b class="tag">[link=URL][/link]</b> Link (Link veya Tag seklinde kullanilabilir)</span>
<span><b class="tag">[button=URL][/button]</b> Buton (Link veya Etiket seklinde kullanilabilir)</span>
</p>
</div>';
$_['info_title_custom']	= 'Özel etiketler';
$_['info_msg_custom']	= '<div class="infotags">
<h5>Özel etiketler</h5>
<p>Özel mailler için kendi tanimladiginiz etiketleri kullanin</p>
</div>';
$_['info_msg_tags_qosu']	= '
<div class="infotags">
<h5>Takip (Ortaklik) (Hizli siparis durumu güncelleme)</h5>
<p>
<span><b class="tag">{tracking_no}</b> Takip Numarasi</span>
<span><b class="tag">{tracking_url}</b> Takip Linki</span>
<span><b class="tag">{tracking_link}</b> Takip Linki (tiklanabilir link)</span>
<span><b class="tag">[if_tracking][/if_tracking]</b> Eger takip varsa</span>
</p>
</div>';
$_['info_msg_tags_conditions']	= '
<div class="infotags">
<h5>Kosullu Blok</h5>
<p>
Koşullu bir bloğu kullanmak için içeriğinizi bir açılış etiketine dahil ettiniz
<b class="tag">[if_condition]</b> ve aynı ada sahip bir kapanış etiketi <b class="tag">[/if_condition]</b><br/>
Tüm koşulların olumsuz versiyonları da vardır:
 <b class="tag">[if_not_condition]</b>...<b class="tag">[/if_not_condition]</b>
</p>
</div>';
$_['info_msg_tags_status']	= '
<div class="infotags">
<h5>Siparis güncelle</h5>
<p>
<span><b class="tag">{order_status}</b> Durum adi</span>
<span><b class="tag">{message}</b> Bildirim mesaji</span>
</p>
</div>';
$_['info_msg_tags_order_cond']	= '
<div class="infotags">
<h5>Siparis kosullari</h5>
<p>
<span><b class="tag">[if_message]</b> Mesaj varsa</span>
<span><b class="tag">[if_customer]</b> Kayitli müsteri ise</span>
</p>
</div>';
$_['info_msg_customer.approve']	= '
<div class="infotags">
<h5>Müsteri</h5>
<p>
<span><b class="tag">{firstname}</b> Adi</span>
<span><b class="tag">{account_url}</b> Hesabim URL</span>
</p>
</div>';
$_['info_msg_customer.forgotten']	= '
<div class="infotags">
<h5>Müsteri</h5>
<p>
<span><b class="tag">{password}</b> Sifre</span>
<span><b class="tag">{account_url}</b> Hesabim URL</span>
</p>
</div>';
$_['info_msg_customer.register']	= '
<div class="infotags">
<h5>Müsteri</h5>
<p>
<span><b class="tag">{email}</b> Email</span>
<span><b class="tag">{password}</b> Sifre</span>
<span><b class="tag">{account_url}</b> Hesabim URL</span>
<span><b class="tag">[if_approval]</b> Onay gerekli</span>
<span><b class="tag">[if_not_approval]</b> Onay gerekli degil</span>
</p>
</div>';
$_['info_msg_customer.credit']	= '
<div class="infotags">
<h5>Müsteri</h5>
<p>
<span><b class="tag">{firstname}</b> Adi</span>
<span><b class="tag">{amount}</b> Kredi tutari</span>
<span><b class="tag">{total}</b> Toplam kredi</span>
<span><b class="tag">{account_url}</b> Hesabim URL</span>
</p>
</div>';
$_['info_msg_customer.reward']	= '
<div class="infotags">
<h5>Müsteri</h5>
<p>
<span><b class="tag">{firstname}</b> Adi</span>
<span><b class="tag">{amount}</b> Ödül puanlari</span>
<span><b class="tag">{total}</b> Toplam ödül puanlari</span>
<span><b class="tag">{account_url}</b> Hesabim URL</span>
</p>
</div>';
$_['info_msg_customer.voucher']	= '
<div class="infotags">
<h5>Müsteri</h5>
<p>
<span><b class="tag">{amount}</b> Hediye çeki tutari</span>
<span><b class="tag">{from}</b> Gönderenin adi</span>
<span><b class="tag">{code}</b> Hediye çeki kodu</span>
<span><b class="tag">{image}</b> Hedi çeki önceden tanimlanmis görüntü</span>
<span><b class="tag">{message}</b> Gönderen mesaji</span><br/>
<span><b class="tag">[if_image]</b> Önceden tanimlanmis görüntü varsa</span>
<span><b class="tag">[if_message]</b> Gönderen mesaj birakmissa</span>
</p>
</div>';
$_['info_msg_affiliate.approve']	= '
<div class="infotags">
<h5>Ortaklik</h5>
<p>
<span><b class="tag">{firstname}</b> Adi</span>
<span><b class="tag">{affiliate_url}</b> Ortaklik hesabi URL</span>
</p>
</div>';
$_['info_msg_affiliate.forgotten']	= '
<div class="infotags">
<h5>Ortaklik</h5>
<p>
<span><b class="tag">{password}</b> Sifre</span>
<span><b class="tag">{affiliate_url}</b> Ortaklik hesabi URL</span>
</p>
</div>';
$_['info_msg_affiliate.register']	= '
<div class="infotags">
<h5>Ortaklik</h5>
<p>
<span><b class="tag">{email}</b> Email</span>
<span><b class="tag">{password}</b> Sifre</span>
<span><b class="tag">{affiliate_url}</b> Ortaklik hesabi URL</span>
<span><b class="tag">[if_approval]</b> Onay gerekli</span>
<span><b class="tag">[if_not_approval]</b> Onay gerekli degil</span>
</p>
</div>';
$_['info_msg_affiliate.transaction']	= '
<div class="infotags">
<h5>Ortaklik
</h5>
<p>
<span><b class="tag">{firstname}</b> Adi</span>
<span><b class="tag">{amount}</b> Ortaklik komisyonu</span>
<span><b class="tag">{total}</b> Toplam komisyon tutari</span>
<span><b class="tag">{affiliate_url}</b> Ortaklik hesabi URL</span>
</p>
</div>';
$_['info_msg_order.return']	= '
<div class="infotags">
<h5>Siparis iade</h5>
<p>
<span><b class="tag">{return_id}</b> Iade ID</span>
<span><b class="tag">{order_status}</b> Siparis durumu</span>
<span><b class="tag">{message}</b> Mesaj</span>
<span><b class="tag">[if_message]</b> Mesaj varsa</span>
</p>
</div>';
$_['info_msg_sale.contact']	= '
<div class="infotags">
<h5>Pazarlama mail</h5>
<p>Bu Mail, müşterilere pazarlama> Mailinden gönderilen Maildir.
</p>
<p>
<span><b class="tag">{message}</b> Mesaj</span>
</p>
</div>';
$_['info_msg_tags_order']	= '
<div class="infotags">
<h5>Fatura</h5>
<p>
<span><b class="tag">{invoice}</b> Tam fatura</span>
</p>
<h5>Müsteri</h5>
<p>
<span><b class="tag">{customer_id}</b> Müsteri ID</span>
<span><b class="tag">{customer}</b> Adi soyadi</span>
<span><b class="tag">{firstname}</b> Adi</span>
<span><b class="tag">{lastname}</b> Soyadi</span>
<span><b class="tag">{telephone}</b> Telefon</span>
<span><b class="tag">{email}</b> Email adresi</span>
</p>
<h5>Siparis</h5>
<p>
<span><b class="tag">{order_id}</b> Siparis ID</span>
<span><b class="tag">{invoice_no}</b> Fatura numarasi</span>
<span><b class="tag">{invoice_prefix}</b> Fatura öneki</span>
<span><b class="tag">{order_url}</b> Siparis URL (kullanici hesabinda)</span>
<span><b class="tag">[if_comment]</b> Kosullu blok</span>
<span><b class="tag">{comment}</b> Yorum</span>
<span><b class="tag">{total}</b> Toplam tutar</span>
<span><b class="tag">{reward}</b> Ödül puani</span>
<span><b class="tag">{commission}</b> Komisyon</span>
<span><b class="tag">{language_code}</b> Dil kodu</span>
<span><b class="tag">{currency_code}</b> Para birimi kodu</span>
<span><b class="tag">{currency_value}</b> Para birimi degeri</span>
<span><b class="tag">{amazon_order_id}</b> Amazon siparis ID</span>
</p>
<h5>Ödeme</h5>
<p>
<span><b class="tag">{payment_firstname}</b> Adi</span>
<span><b class="tag">{payment_lastname}</b> Soyadi</span>
<span><b class="tag">{payment_company}</b> Sirket</span>
<span><b class="tag">{payment_address_1}</b> Adres 1</span>
<span><b class="tag">{payment_address_2}</b> Adres 2</span>
<span><b class="tag">{payment_postcode}</b> Posta kodu</span>
<span><b class="tag">{payment_city}</b> Sehir</span>
<span><b class="tag">{payment_zone}</b> Ilçe</span>
<span><b class="tag">{payment_country}</b> Ülke</span>
<span><b class="tag">{payment_method}</b> Ödeme metodu</span>
<br/><br/>
<span style="width:100%"><b class="tag">[if_payment:pcode]..[/if_payment]</b>
Belirli ödeme yöntemini görüntüleyin, ödeme_kodunu gerçek ödeme kodunuzla değiştirin (bank_transfer, pp_express, ...). Ödeme kodunu, düzenlerken yaptığınız ödemenin URL&#39;
sinde bulabilirsiniz: route=payment/bank_transfer</span>
</p>
<h5>Kargolama</h5>
<p>
<span><b class="tag">{shipping_firstname}</b> Adi</span>
<span><b class="tag">{shipping_lastname}</b> Soyadi</span>
<span><b class="tag">{shipping_company}</b> Sirket</span>
<span><b class="tag">{shipping_address_1}</b> Adres 1</span>
<span><b class="tag">{shipping_address_2}</b> Adres 2</span>
<span><b class="tag">{shipping_postcode}</b> Posta kodu</span>
<span><b class="tag">{shipping_city}</b> Sehir</span>
<span><b class="tag">{shipping_zone}</b> Ilçe</span>
<span><b class="tag">{shipping_country}</b> Ülke</span>
<span><b class="tag">{shipping_method}</b> Kargo Metodu</span>
</p>
<h5>Çesitli</h5>
<p>
<span><b class="tag">{ip}</b> Kullanici IP</span>
<span><b class="tag">{user_agent}</b> Kullanici ajani</span>
</p>
</div>';

// Error
$_['error_permission'] = 'Uyari: Modülü düzenleme yetkiniz yok!';
$_['error_permission_demo'] = 'Demo modundasiniz, kaydedemezsiniz.';
?>
