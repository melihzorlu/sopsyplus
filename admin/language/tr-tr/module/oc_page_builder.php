<?php
// Heading
$_['heading_title'] = '<b><i>Ana Sayfa Bölüm Oluşturucu</i></b>';
$_['page_title'] = "Bölüm Oluşturucu";

// Text
$_['text_success'] = "Modül Başarıyla Düzenlendi!";
$_['text_extension']         = 'Eklentiler';
$_['text_edit'] = "Bölüm Oluşturucu";
$_['text_columns'] = "Kolonlar";
$_['text_custom_columns'] = "Kolon Özelleştirme";
$_['text_custom_classname'] = "Özel Sınıf İsmi";
$_['text_columns_error_format'] = "Sütun biçimi nedeniyle sütun oluşturulamıyor";
$_['text_add_row'] = "Yeni Sutun Ekle";
$_['text_add_sub_row'] = "Bu Kolona Sütun Ekle";
$_['text_add_module'] = "Modül Ekle";
$_['text_insert_module'] = "Modülleri buraya ekleyin veya buraya sürükleyin";
$_['text_number_max_over'] = "Sütun sayısı 12'den az olmalı";
$_['text_number_min_over'] = "Sütun sayısı 0'dan fazla olmalı";

// Entry
$_['entry_name'] = "İçerik Adı";
$_['entry_status'] = "Durum";

// Error
$_['error_permission'] = 'Modülü Düzenleme Yetkiiz yok!';
$_['error_name']       = 'İçerik Adı 3 ile 64 karakter arasında olmalıdır!';
