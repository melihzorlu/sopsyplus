﻿<?php
// Heading
$_['heading_title']     = 'Sepet Hatırlatma Modülü';

// Text
$_['text_success']			= 'Başarılı. E-posta şablonları düzenlendi.';
$_['text_s_setting']		= 'Başarılı. Modül ayarları düzenlendi.';
$_['text_customer']			= 'Üye';
$_['text_guest']				= 'Misafir';
$_['text_products']			= 'Ürünler';
$_['text_view']					= 'Detaylar';
$_['text_customer']			= 'Üye';
$_['text_guest']				= 'Misafir';
$_['text_success_send'] = 'Başarılı. Modül ayarları düzenlendi.';
$_['text_success_delete'] = 'Başarılı. Modül ayarları düzenlendi.';
$_['text_image'] 				= 'Resim';
$_['text_product'] 			= 'Ürün';
$_['text_model'] 				= 'Ürün Kodu';
$_['text_quantity'] 		= 'Adet';
$_['text_date_added'] 	= 'Ekleme Tarihi';
$_['text_no_cart'] 			= 'Sonuç yok!';
$_['text_abandoned_carts']     			= 'Terkedilmiş Sepetler';
$_['total_abandoned_cart_products']	= 'Terkedilmiş Sepet Ürünleri';
$_['text_notified_abandoned_carts']	= 'Bildirim Gönder';
$_['text_use_coupons']			= 'Kupon Kullan';
$_['text_missing_orders']		= 'Kaçan Siparişler';
$_['text_cart_details']			= 'Sepet Detayı';
$_['text_registered']			= 'Kayıtlı';
$_['text_ip']							= 'IP Adres';
$_['text_action']							= 'Eylem';
$_['text_email_reminder']			= 'E-Mail Hatırlatma';
$_['text_email']					= 'E-Mail';
$_['text_telephone']					= 'Telefon';
$_['text_store']					= 'Mağaza';
$_['text_image']					= 'Resim';
$_['text_reminder']					= 'Hatırlatıcı';
$_['text_date_added']					= 'Ekleme Tarihi';
$_['text_name']						= 'İsim';

//Tab
$_['tab_dashboard']     = 'Panel';
$_['tab_control_panel']  = 'Kontrol Paneli';
$_['tab_abandoned_carts']= 'Terkedilmiş Sepetler';
$_['tab_abandoned_carts_history']	= 'Tamamlanan Siparişler';
$_['tab_mail_template']  = 'E-Mail Şablonları';
$_['tab_coupons']     	 = 'Kuponlar';
$_['tab_support']     	 = 'Destek';
$_['tab_setting']     	 = 'Ayarlar';
$_['tab_crown_job'] 		 = 'Crown Job (Zamanlanmış Görevler)';

// Help 
$_['help_mail_template'] = 'E-Mail Şablonları';

// Column
$_['column_image']     	 		= 'Resim';
$_['column_title']     	 	= 'Başlık';
$_['column_status']     	= 'Durum';
$_['column_name']         = 'Kupon Adı';
$_['column_code']         = 'Kod';
$_['column_discount']     = 'İndirim';
$_['column_date_start']   = 'Başlangıç';
$_['column_date_end']     = 'Bitiş';
$_['column_order_id']     = 'Sipariş No';
$_['column_customer']     = 'Üye';
$_['column_amount']       = 'Tutar';
$_['column_date_added']   = 'Ekleme Tarihi';
$_['column_action']       = 'Eylem';

// Entry
$_['entry_title']     	 					= 'Başlık';
$_['entry_subject']     	 				= 'Konu';
$_['entry_message']     	 				= 'Mesaj';
$_['entry_status']     	 					= 'Durum';
$_['entry_email']     	 					= 'Email';
$_['entry_name']     	 						= 'İsim';
$_['entry_email_notify']     		  = 'E-Mail Hatırlatıcı';
$_['entry_email_notify_orders']   = 'E-Mail Sipariş Hatırlatıcı';
$_['entry_account']   						= 'Kayıtlı';
$_['entry_store']   							= 'Mağaza';
$_['entry_date_added']   					= 'Ekleme Tarihi';
$_['entry_deleteproduct_status']	= "Sepetteki Ürünleri Sil";
$_['entry_deletecart_status']			= "Sepetleri Sil";
$_['entry_crownjob_status']				= "Durum";
$_['entry_template']							= "E-Mail Şablonları";
$_['entry_email_send_status']			= "E-Mail Şablonları Sistemi";
$_['entry_url']										= "Crown Job URL";

// Error
$_['error_warning']          = 'Hata! Alanları kontrol edin';
$_['error_permission']       = 'Hata! Düzenleme yetkiniz yok.';
$_['error_title']            = 'Hata! Başlık 3-64 karakter olmalı.';
$_['error_subject']    			 = 'Hata! Konu 3-64 karakter olmalı.';
$_['error_message']     			 = 'Hata! Mesaj en az 10 karakter olmalı.';
$_['error_not_found_product']	 = 'Hata! Sepet Seçiniz.';
$_['error_abandoned_template'] = 'Hata! E-posta şablonu Seçiniz.';