<?php
// Heading
$_['heading_title']    = 'Kampanya';

// Text
$_['text_total']       = 'Sipariş Toplamı';
$_['text_success']     = 'Başarılı!';
$_['text_edit']        = 'Kuponu Düzenle';

// Entry
$_['entry_status']     = 'Durum';
$_['entry_sort_order'] = 'Sıralama';

// Error
$_['error_permission'] = 'Hata= Düzenleme yetkiniz bulunmamakta!';