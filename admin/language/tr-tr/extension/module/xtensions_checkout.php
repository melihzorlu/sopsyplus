<?php
// Heading
$_['heading_title']    ='<i class="fa fa-fw fa-credit-card"></i><b>Hızlı Ödeme Sayfası Modülü</b>';

// Text
$_['text_module']         						= 'Modüller';
$_['text_success']        						= 'Başarılı: Hepsiburada Ödeme Sayfası Modülünü Değiştirdiniz!';
$_['no_file_attached']        					= 'Uyarı: İçe aktarılacak bir json dosyası yok!';
$_['button_export_settings']					= 'Ayarları dışa aktar';
$_['button_import_settings']					= 'Ayarları içe aktar';
// Error
$_['error_permission']    						= 'Uyarı: Modül ayarlarını değiştirme izniniz yok!';
$_['field_title']['firstname']    				= 'Ad';
$_['field_title']['lastname']    				= 'Soyad';
$_['field_title']['email']    					= 'E-posta';
$_['field_title']['confirm_email']    			= 'E-postayı Onayla';
$_['field_title']['telephone']    				= 'Telefon';
$_['field_title']['fax']    					= 'Fax';
$_['field_title']['company']    				= 'Şirket';
$_['field_title']['customer_group_id']    		= 'Üye Grubu';
$_['field_title']['address_1']    				= 'Addres 1';
$_['field_title']['address_2']    				= 'Addres 2';
$_['field_title']['city']    					= 'Şehir';
$_['field_title']['postcode']    				= 'Posta Kodu';
$_['field_title']['country_id']    				= 'Ülke';
$_['field_title']['zone_id']    				= 'Bölge';
$_['field_title']['password']    				= 'Şifre';
$_['field_title']['confirm']    				= 'Şifreyi Onayla';
$_['field_title']['newsletter']   				= 'Bülten';
$_['field_title']['agree']    					= 'Anlaşma Onay Kutusu';
$_['module_heading_title']			    		= 'Durum :';
$_['yes_option']			    				= 'Aktif';
$_['no_option']			    					= 'Pasif';
$_['default_country_heading']			   		= 'Varsayılan Ülke';
$_['default_default_state_region']				= 'Varsayılan İl';
$_['register_page_field_heading']				= 'Üye Kayıt Alanları';
$_['register_area_heading']						= 'Üye Kayıt';
$_['edit_area_heading']							= 'Düzenle';
$_['guest_area_heading']						= 'Ödeme / Kargo / Ziyaretçi';
$_['sorting_heading']							= 'Sınıflandırma';
$_['validation_heading']						= 'Onaylama';
$_['language_heading']							= 'Dil';
$_['checkout_misc_heading']						= 'Ödeme';
$_['social_login_heading']						= 'Sosyal Hesaplara Giriş';
$_['design_heading']							= 'Tasarım ve Özel CSS-JS';
$_['help_heading']								= 'Destek, Dokümanlar ve Özellikler';
$_['single_section_heading']					= 'Tek Seçenek';
$_['show_title']								= 'Göster';
$_['required_title']							= 'Zorunlu';
$_['always_displayed_in_store']					= 'Birden fazla üye grubu varsa herzaman görünür.';
$_['always_displayed']							= 'Her Zaman görünür';
$_['always_mandatory_heading']					= 'Her zaman zorunlu';
$_['cannot_mandatory_heading']					= 'Zorunlu değil';
$_['personal_details_account_heading']			= 'Üye Hesabı Düzenle / Kişisel Bilgiler';
$_['personal_details_address_heading']			= 'Üye Hesabı Düzenle / Adres Bilgiler';
$_['account_guest_heading']						= 'Üye Ol ve Misafir Adımı Kişisel Bilgiler';
$_['guest_area_subheading']						= '1. adımda ziyaretçiye görünmez';
$_['address_panel_heading']						= 'Adres Paneli - Ödeme Sayfasında Ödeme ve Kargo Adresi';
$_['minimum_characters_heading']				= 'Minimum Karakter Sayısı';
$_['maximum_characters_heading']				= 'Maksimum Karakter Sayısı';
$_['numeric_heading']							= 'Sayısal';
$_['masking_heading']							= 'Maskeleme';
$_['regex_heading']								= 'Regex';
$_['error_message_heading']						= 'Hata Mesajı';
$_['validation_title_first']					= 'Min, max ve numerik değer hata mesajı ve maskeleme alanlarını doldurun.';
$_['validation_title_second']					= 'Maskeleme veya normal ifadelerden yalnızca birini ayarlayın';
$_['validation_title_third']					= 'Kendi hata mesajını ayarla';
$_['confirm_password_description']				= 'Min ve max her zaman aynıdır.';
$_['field_heading']								= 'Alan';
$_['title_heading']								= 'Başlık';
$_['tooltip_heading']					 		= 'İpucu';
$_['option_heading']					 		= 'Seçenek';
$_['field_name_heading']						= 'Dosya Adı';
$_['sort_name_heading']							= 'Numara Sırası';
$_['custom_field_heading']						= 'Özel Alan';
$_['account_tab_heading']						= 'Hesap Sekmeleri';
$_['sort_number_heading']						= 'Numara Sırası';
$_['payment_shipping_method_heading']			= 'Ödeme / Kargo';
$_['show_coupons']								= 'Kuponları Göster';
$_['show_vouchers']								= 'Kuponları Göster';
$_['show_reward_points']						= 'Ödül Puanlarını Göster';
$_['required_user_subheading']					= 'Still required user to be eligible';
$_['same_address_heading']						= 'Fatura ve teslimat adresi aynı kullan';
$_['login_page_heading']						= 'Giriş';
$_['change_login_step_heading']					= 'Giriş Tasarımını Değiştirin';
$_['clean_view']								= 'Basit Görünüm';
$_['tabs_view']									= 'Sekmeler Görünümü';
$_['accordion_view']							= 'Akordeon Görünümü';
$_['address_list_heading']						= 'Adres Listesi';
$_['address_list_subheading']					= 'Adres Sayfası Tasarımını Değiştirin';
$_['block_view_heading']						= 'Blok Görünümü';
$_['list_view_heading']							= 'Liste Görünümü';
$_['address_form_heading']						= 'Adres Formu';
$_['address_form_subheading']					= 'Adres Formu Tasarımını Değiştirin';
$_['modal_view_heading']						= 'Modal Görünüm';
$_['inline_view_heading']						= 'Satır İçi Görünüm';
$_['page_view_heading']							= 'Satır İçi Görünüm';
$_['other_option_heading']						= 'Kupon / Çek / Ödül Puan Formu Görünümü';
$_['default_step_heading']						= 'Varsayılan';
$_['default_step_session_heading']				= 'Session kullan';
$_['default_step_session_heading_help']			= 'Bir kişi varsayılandan farklı bir sunum yaptıysa, oturumdan son adım varsayılan olarak kullanılır ve pencereyi yeniler?';
$_['step_active_heading']						= 'Aktif';
$_['login_heading']								= 'Giriş';
$_['register_heading']							= 'Kullanıcı';
$_['guest_heading']								= 'Misafir';
$_['payment_type_heading']						= 'Ödeme Türü';
$_['payment_type_subheading']					= 'Ödeme Türü Sayfası Tasarımını Değiştirin';
$_['override_heading']							= 'Adres biçimini geçersiz kıl';
$_['override_subheading']						= 'Tüm ülkeler için geçersiz kıl.';
$_['address_format_heading']					= 'Adres Biçim';
$_['firstname_heading']							= 'Ad = {firstname}';
$_['lastname_heading']							= 'Soyad = {lastname}';
$_['company_heading']							= 'Şirket İsmi = {company}';
$_['address_1_heading']							= 'Adres 1 = {address_1}';
$_['address_2_heading']							= 'Adres 2 = {address_2}';
$_['city_heading']								= 'Şehir = {city}';
$_['postcode_heading']							= 'Posta Kodu = {postcode}';
$_['zone_heading']								= 'Bölge = {zone}';
$_['zone_code_heading']							= 'Bölge Kodu = {zone_code}';
$_['country_heading']							= 'Ülke = {country}';
$_['identifier_heading']						= '*custom_field = {identifier}';
$_['Applicable_custom_field_heading']			= '* Sadece adres özel alanı için geçerlidir';
$_['address_format_note_text']					= 'Bu, tüm ülkeler için adres biçimini güncelleyecektir. Alternatif olarak, her ülkede adres biçimi koyabilirsiniz <a href="index.php?route=localisation/country&token=<?php echo $token; ?>" target="_newtab">buradan.</a>';
$_['custom_fields_for_address_heading']			= 'Adres için mevcut özel alanlar';
$_['information_page_heading']					= 'Bilgi Sayfası';
$_['display_footer_link_heading']				= 'Altbilgideki Kontrol Edilen Sayfa Bağlantılarını Görüntüle';
$_['display_contact_link']						= 'İletişim Linkini Görüntüle';
$_['display_footer_modal']						= 'Alt kısım bilgi sayfalarını göster<br/><small class="text-muted">Evet -  Modal <br/>Hayır - yeni sekme</small>';
$_['contact_details_heading']					= 'İletişim Bilgileri';
$_['contact_email_heading']						= 'E-posta';
$_['contact_email_subheading']					= 'Görüntülemek istemiyorsanız boş bırakın';
$_['contact_telephone_heading']					= 'Telefon';
$_['contact_telephone_subheading']				= 'Görüntülemek istemiyorsanız boş bırakın';
$_['footer_image_heading']						= 'Altbilgi Resimleri';
$_['payment_method_logo_heading']				= 'Ödeme Yöntemi Logoları';
$_['text_payment_default']						= 'Varsayılan Ödeme Şekli';
$_['facebook_heading']							= 'Facebook';
$_['google_heading']							= 'Google';
$_['linkedin_heading']							= 'Linked';
$_['status_heading']							= 'Durum';
$_['app_id_heading']							= 'App Id';
$_['client_id_heading']							= 'Client Id';
$_['api_key']									= 'Api Key';
$_['extra_fields_heading']						= 'Ekstra Alanlar';
$_['additional_fields_heading']					= 'İlave Alanlar';
$_['additional_fields_subheading']				= 'Kullanıcıyı ek hesap alanları eklemek için zorla<br/>Evet: Müşteri yeni bir form görür.<br />';
$_['additional_fields_subheading_second'] 		= ': Kullanıcı hangi alanlarda verilmiş olursa olsun eklenecektir.';
$_['additional_fields_yes_heading']				= 'Ek Alanlar (Yukarıda ise evet)';
$_['colors_heading']							= 'Renkler';
$_['custom_css_heading']						= 'Custom CSS<b> - style tag eklemeyin</b> - Sadece ödeme sayfasına uygulanır';
$_['custom_js_heading']							= 'Custom Javascript<b> - script tag eklemeyin</b> - Sadece ödeme sayfasına uygulanır';
$_['edit_heading']								= 'Düzenle';
$_['default_heading']							= 'Varsayılan';
$_['select_option_heading']						= '--Seçim--';
$_['masking_and_numeric_heading']				= 'Hem maskeleme hem de sayısal seçmeyin';
$_['language_subheading']						= 'Opencart doğrulamasını geçersiz kılmak ve minimum, maksimum, sayısal tip, kendi hata mesajını kullanmak ve alanı maskelemek için geçersiz kılmayı kontrol edin.';
$_['checkout_heading']							= 'Çıkış';
$_['key_points_heading']						= 'Anahtar Kelimeler';
$_['upcoming_features_heading']					= 'Özellikler';
$_['Support_heading']							= 'Destek';
$_['gift_wrap_image']							= 'Hediye Paketi Resmi';
$_['text_design_option']						= 'Renk Ayarları';
$_['text_design_custom']						= 'Özel Tema';
$_['text_payment_method_content']				= 'Ödeme Yöntemlerinden Önce Özel İçerik';
$_['text_fields_heading']						= 'Alanlar (Görüntüle / Zorunlu)';
$_['text_sort_validation']						= 'Sıralama ve Doğrulama';
$_['custom_colors_heading']						= 'Tema Renkleri';
$_['custom_css_tab_heading']					= 'Özel css';
$_['custom_js_tab_heading']						= 'Özel Javascript';
$_['text_version']								= ' - Versiyon: ';
$_['tab_module_settings']						= 'Modul Ayarları';
$_['tab_export_import']							= 'Export Import';
$_['error_store']								= 'Mağaza seçilmedi';
$_['text_license']								= 'Lisans';
$_['text_license_valid']						= 'Lisansınız geçerli!';
$_['text_license_code']							= 'Lisans kodu';
$_['text_license_user']							= 'Lisans kullanıcısı';
$_['text_license_validate']						= 'Lisansı Doğrula';
$_['tab_license_validate']						= 'Destek ve Lisans Aktivasyonu';
$_['text_license_alert']						= 'Lisans aktivasyonunuzu tamamlamanız gerekiyor';
$_['text_license_alert_2']						= 'Destek ve güncellemeleri almak için lisans aktivasyonunuzu tamamlamanız gerekmektedir!';
$_['text_license_activate']						= 'Şimdi aktifleştir';
$_['text_license_support_date_expiry']			= 'Desteklenene kadar geçerli';
$_['text_how_to_validate']						= 'Nasıl Doğrulanır?';
$_['text_cancel_or_back']						= 'Modüllere Geri Dön';
$_['text_save_and_stay']						= 'Kaydet ve Kal';

$_['text_xtensions_color_section']['buttons']			= 'Butonlar';
$_['text_xtensions_color_section']['tabs']				= 'Sekmeler';
$_['text_xtensions_color_section']['steps']				= 'Üstbilgi';
$_['text_xtensions_color_section']['links']				= 'Href ve Dil / Para Birimi Bağlantıları';
$_['text_xtensions_color_section']['address_type_1']	= 'Adres Bloğu Görünümü Ve Adres Metnini Silme';
$_['text_xtensions_color_section']['address_type_2']	= 'Adres Listesi Görünüm Arka Planları';
$_['text_xtensions_color_section']['inputs']			= 'Giriş Alanları';
$_['text_xtensions_color_section']['alerts']			= 'Uyarılar';
$_['text_xtensions_color_section']['body']				= 'Body';

$_['tab_help']['content']['help-reg'][0] 		= 'Üye ol sayfasını sadeleştirin.';
$_['tab_help']['content']['help-reg'][1] 		= 'Varsayılan ülkeyi ayalaryın';
$_['tab_help']['content']['help-reg'][2] 		= 'Varsayılan ili ayalaryın';
$_['tab_help']['content']['help-reg'][3] 		= 'Kayıt sayfası değişiklikleri kayıt sayfasında geçerlidir';

$_['tab_help']['content']['help-edit'][0] 		= 'Üye hesap düzenle sayfasını sadeleştirin.';
$_['tab_help']['content']['help-edit'][1] 		= 'Varsayılan ülkeyi ayalaryın';
$_['tab_help']['content']['help-edit'][2] 		= 'Varsayılan ili ayalaryın';
$_['tab_help']['content']['help-edit'][3] 		= 'Üye hesap düzenle sayfası değişiklikleri Üye hesap düzenle sayfasında geçerlidir';

$_['tab_help']['content']['help-checkout'][0] 	= 'Ödeme sayfasını sadeleştirin.';
$_['tab_help']['content']['help-checkout'][1] 	= 'Varsayılan ülkeyi ayalaryın';
$_['tab_help']['content']['help-checkout'][2] 	= 'Varsayılan ili ayalaryın';
$_['tab_help']['content']['help-checkout'][3] 	= 'Ödeme sayfasında Ödeme / Kargo / Misafir sekmesindeki değişiklikler uygulanacaktır';
$_['tab_help']['content']['help-checkout'][4] 	= 'Ödeme sekmesindeki Seçenekler bölümü, yalnızca Sekme sekmesinde herhangi bir onay kutusu işaretlendiğinde görüntülenecektir';
$_['tab_help']['content']['help-checkout'][5] 	= 'Bu sekmeler kuponlar, kuponlar, Yorumlar ve kullanıcı uygunsa ödüller gösterilir';
$_['tab_help']['content']['help-checkout'][6] 	= 'Gönderilen kargo ve ödeme adresi aynıysa sadece bir adres bölümü gösterecek ve otomatik olarak hem ödeme hem de kargo adresi olarak ayarlanacaktır.';
$_['tab_help']['content']['help-checkout'][7] 	= 'Alt kısım kart ikonları için maksimum yükseklik 30 pikseldir.';
$_['tab_help']['content']['help-checkout'][12] 	= '<b>Dil: </b> Checkouttaki metinlerin çoğu dil klasörünüzün checkout.php dosyasından alınmıştır.';
$_['tab_help']['content']['help-checkout'][13] 	= 'Geri kalanlar ise  modül / xtensions.php dizinindedir, bu yüzden kendinizi ingilizce olmayan dillere çevirmelisiniz.';
$_['tab_help']['content']['help-checkout'][14] 	= 'checkout.php metinlerini de size uygun şekilde değiştirebilirsiniz. Kısa metinler daha fazla değer katacağından, örneğin Adım metinleri 1. Hesap 2. Adres 3.Ödeme';

$_['tab_help']['content']['help-keyp'][0] 		= 'Alanlarını sayısal olarak ayarlayın, harf veya başka bir girdi kabul edilmez';
$_['tab_help']['content']['help-keyp'][1] 		= 'Uygun alanları Türkiye veya başka ülkeler için maskelenmiş olarak ayarlayın';
$_['tab_help']['content']['help-keyp'][2]		= 'Tek bir alanda hem maskeleme hem de sayısal ayarlamayın';
$_['tab_help']['content']['help-keyp'][3] 		= 'Efekt üzerinden çok dilli vurgulamak için alanınızın araç ipuçlarını ayarlayın';
$_['tab_help']['content']['help-keyp'][5] 		= 'İpuçlarını devre dışı bırakmak için araç ipucu alanlarını boş bırakın';
$_['tab_help']['content']['help-keyp'][6] 		= 'Alanları istediğiniz gibi sıralayın, Sıralama, varsayılan ve özel alanlarla birlikte geçerli olur';
$_['tab_help']['content']['help-keyp'][7] 		= 'Örnek: E-posta, Telefon, Cinsiyet (Özel Alan) seçeneğini görüntülemeniz gerekiyorsa, İsim vermeden önce onları 1,2,3,4 olarak verin.';
$_['tab_help']['content']['help-keyp'][8] 		= 'Örnek: Eğer FirstNamei görüntülemeniz gerekiyorsa, bir satırdaki Soyadı sıralama 1.1.1.2';
$_['tab_help']['content']['help-keyp'][9] 		= 'En fazla 4 alan bir sırada görüntülenebilir. Hepsi eşit genişlikte olacaktır';

$_['tab_help']['content']['help-ufeatures'][0] 	= '<b></b>';
$_['tab_help']['content']['help-ufeatures'][1] 	= '<span class="text-success"><i class="fa fa-check-circle-o"></i>&nbsp;<b> Ödeme için renk şemaları, renk ayarlarını değiştirebilir, önceden tanımlanmış temaları kullanabilir veya özel css ekleyebilirsiniz.</b></span>';
$_['tab_help']['content']['help-ufeatures'][2] 	= '<span class="text-success"><i class="fa fa-check-circle-o"></i>&nbsp;<b> Önceden tanımlanmış renk şemaları ile daha az css yazın</b></span>';
$_['tab_help']['content']['help-ufeatures'][3] 	= '<span class="text-success"><i class="fa fa-check-circle-o"></i>&nbsp;<b> Üye ol ve ödeme sayfası </b></span>';
$_['tab_help']['content']['help-ufeatures'][4] 	= 'Hediye paketi';
$_['tab_help']['content']['help-ufeatures'][5] 	= 'Export';
$_['tab_help']['content']['help-ufeatures'][6] 	= '<span class="text-success"><i class="fa fa-check-circle-o"></i>&nbsp;<b>Varsayılan ili ayalaryın </b></span>';
$_['tab_help']['content']['help-ufeatures'][7] 	= '<span class="text-success"><i class="fa fa-check-circle-o"></i>&nbsp;<b>Varsayılan ili ayalaryın Özel alanlar faturada, e-postada, adres defterlerinde ve sipariş bilgilerini gösterir</b></span>';
$_['tab_help']['content']['help-ufeatures'][8] 	= '<span class="text-success"><i class="fa fa-check-circle-o"></i>&nbsp;<b>Varsayılan ili ayalaryın Özel alanlar içerecek adres kalıbı geçersiz kıl.</b></span>';
$_['tab_help']['content']['help-ufeatures'][9] 	= '<span class="text-success"><i class="fa fa-check-circle-o"></i>&nbsp;<b>Varsayılan ili ayalaryın Özel alan için özel doğrulama kuralları</b></span>s';
$_['tab_help']['content']['help-ufeatures'][10] = '<span class="text-success"><i class="fa fa-check-circle-o"></i>&nbsp;<b>Varsayılan ili ayalaryın Facebook, Google ve Linkedin Giriş</b></span>';
$_['tab_help']['content']['help-ufeatures'][11] = 'Abondoned Carts';
$_['tab_help']['content']['help-ufeatures'][12] = 'En İyi Etkinlik raporlaması';
$_['tab_help']['content']['help-ufeatures'][14] = 'SMS OTP Validation and Mobile Login';
$_['tab_help']['content']['help-ufeatures'][15] = '<span class="text-success"><i class="fa fa-check-circle-o"></i>&nbsp;<b>Varsayılan ili ayalaryınXtensions Best Potscode Module <a target="_newtab" href="https://www.opencart.com/index.php?route=marketplace/extension/info&extension_id=30358&filter_member=xtensions">Link</a></b><span>';
$_['tab_help']['content']['help-ufeatures'][17] = 'Some upgrades will come as a free upgrade, and some maybe with add on modules for example Gift Wraps.';
$_['text_version']								= ' - Version: ';



$_['tab_help']['content']['help-support'][0] 	= '<b>Support:</b> Facing an Issue OR Pre Sales Questions!!';
$_['tab_help']['content']['help-support'][1] 	= 'We provide customized solution for checkouts at reasonable price, so you may send in your requirements at our support center or email us.';
$_['tab_help']['content']['help-support'][2] 	= 'Send Queries : <a href="https://www.opencartmarketi.com" class="btn btn-success" target="_tab">opencartmarketi.com</a>';
$_['tab_help']['content']['help-support'][3] 	= 'Email Us : <a href="mailto:opencarteticaretmarketi@gmail.com" target="_tab">opencarteticaretmarketi@gmail.com</a>';
$_['xtensions_colors'] = array(
			'active_button_bg_color' => 'Aktif Düğme BG Rengi',
			'active_button_text_color' => 'Aktif Düğme Metin rengi',
			'disabled_button_bg_color' => 'Pasif Düğme BG Rengi',
			'disabled_button_text_color' => 'Pasif Düğme Metin rengi',
			'active_tab_text_color' => 'Etkin Sekme Metin Rengi',
			'active_tab_border_color' => 'Aktif Sekme Kenarlık rengi',
			'inactive_tab_text_color' => 'Etkin olmayan Sekme Metin Rengi ',
			'complete_current_step_bg_color' => 'Komple Geçerli adım bg rengi',
			'complete_current_step_number_color' => 'Komple Geçerli adım numarası rengi',
			'complete_current_step_border_color' => 'Komple Geçerli adım Kenarlık rengi',
			'pending_step_bg_color' => 'Pending Step BG Color',
			'pending_step_number_color' => 'Pending Step number color',
			'pending_step_border_color' => 'Pending Step Border Color',
			'link_color' => 'Link Rengi',
			'link_hover_color' => 'Bağlantı Vurgusu Rengi',
			'address_panel_selected_border_color' => 'Adres Paneli Seçili Kenarlık Rengi',
			'address_panel_selected_text_color' => 'Adres Paneli Seçilen Metin Rengi',
			'address_panel_selected_bg_color' => 'Adres Panosu Seçilen BG Rengi',
			'address_panel_border_color' => 'Adres Paneli Kenarlığı Rengi',
			'address_panel_text_color' => 'Adres Paneli Metin Rengi',
			'address_panel_bg_color' => 'Adres Paneli BG Rengi',
			'delete_address_color' => 'Adres Rengini Sil',
			'checkbox_icon_color' => 'Onay Kutusu Simgesi Rengi',
			'radio_icon_color' => 'Buton İkonu Rengi',
			'body_text_color' => 'Gövde Metni',
			'body_bg_color' => 'Body Bg Color',
			'currency_dropdown_color' => 'Para Birimi DropDown Rengi',
			'input_field_color' => 'Giriş Alanı Rengi',
			'input_label_color' => 'Giriş Etiketi Rengi',
			'input_filled_label_color' => 'Input Filled Label Color',	
			'input_field_border_color' => 'Input Field Border Color',
			'input_field_focus_color' => 'Input Field Focus Border Color',
			'currency_link_color' => 'Para Birimi Dil Bağlantısı Rengi',
			'currency_link_hover_color' => 'Para Birimi Dili Hover Rengi',			
			'currency_drop_down_link_color' => 'Para Birimi Dili Açılır Bağlantı Rengi',
			'currency_drop_down_link_hover_color' => 'Para Birimi Dili Açılırsa Bağlantı Hover Rengi',
			'currency_drop_down_link_bg_color' => 'Currency Language Dropdown Link Background Color',
			'success_text_color' => 'Başarılı: Metin Rengi',
			'success_bg_color' => 'Başarılı: Arkaplan Rengi',
			'success_border_color' => 'Başarılı: kenarlık rengi',
			'warning_text_color' => 'Uyarı: Metin Rengi',
			'warning_bg_color' => 'Uyarı: Arkaplan Rengi',
			'warning_border_color' => 'Uyarı: Kenarlık Rengi',
			'info_text_color' => 'Metni Rengi',
			'info_bg_color' => 'Arkaplan Rengi',
			'info_border_color' => 'Kenarlık Rengi',
			'address_heading_color' => 'Adres Bölümü Blokları Başlık Rengi',
			'address_heading_bg_color' => 'Adres Blokları Başlık Arkaplan Rengi',
			'address_selected_bg_color' => 'Seçili Adres Arkaplan Rengi',
			'address_selected_text_color' => 'Seçilen Adres Metin Rengi',
		);
?>
