<?php
// Heading
$_['heading_title']    ='<i class="fa fa-fw fa-id-badge"></i><b>Üye Ek Alanlar Modülü</b>';

// Text
$_['text_success']          = 'Başarı: Özel alanları değiştirdiniz!';
$_['text_list']             = 'Özel Alan Listesi';
$_['text_add']              = 'Özel Alanı Ekle';
$_['text_edit']             = 'Özel Alanını Düzenle';
$_['text_choose']           = 'Seçenek';
$_['text_select']           = 'Seç';
$_['text_radio']            = 'Radyo Buton';
$_['text_checkbox']         = 'Onay Kutusu';
$_['text_input']            = 'Giriş';
$_['text_text']             = 'Metin';
$_['text_textarea']         = 'MEtin Alanı';
$_['text_file']             = 'Dosya';
$_['text_date']             = 'Tarih';
$_['text_datetime']         = 'Tarih &amp; Zaman';
$_['text_time']             = 'Saat';
$_['text_account']          = 'Hesap';
$_['text_address']          = 'Adres';
$_['text_regex']			= 'Onaylama';
$_['button_custom_field_value_add'] = 'Seçenek Ekle';
$_['button_add'] = 'Ekle';
$_['text_confirm'] = "Silmek istediğine emin misin? Bu işlem geri alınamaz. Silmek için Tamam'a basın, kalmak için iptal et.";

// Column
$_['column_name']           = 'Özel alan adı';
$_['column_location']       = 'Konum';
$_['column_type']           = 'Tür';
$_['column_sort_order']     = 'Sıralama';
$_['column_action']         = 'Eylem';

// Entry
$_['entry_name']            = 'Özel Alan Adı';
$_['entry_location']        = 'Konum';
$_['entry_type']            = 'Tür';
$_['entry_value']           = 'Varsayılan Değer'; 
$_['entry_custom_value']    = 'Özel Alan Değeri Adı';
$_['entry_customer_group']  = 'Üye Grubu';
$_['entry_required']        = 'Onayla';
$_['entry_status']          = 'Durum';
$_['entry_sort_order']      = 'Sıralama';
$_['entry_sort_order']      = 'Sıralama';
$_['entry_attributes']      = 'Daha Fazla Özellik';
$_['entry_visibility']      = "Özel Alanın Görünürlüğü";
$_['entry_validation']      = 'Regex Doğrulaması';

$_['entry_isnumeric']      = 'Sayısal Alan';
$_['entry_identifier']      = 'Alan Tanımlayıcısı';
$_['entry_identifier_help'] = "Türkçe karakter, rakam ve boşluk kullanmayın.";
$_['entry_mask']    		= 'Maskeleme';
$_['entry_mask_help']	= 'Cpf, cnpj vb. Alanlar için maskeleme gerekebilir. Belirtilen maske desenini kullanabilirsiniz. (99.999-99) (Only for text type fields)';
$_['entry_minimum']  			= 'Min Uzunluk';
$_['entry_maximum']        		= 'Max Uzunluk';
$_['char_help1']  			= 'Maskelemeyi kullanırsanız alanları boş bırakın';
$_['char_help2']       		= 'Sabit uzunluk için minimum ve maksimuma aynı sayıyı yazın';
$_['entry_history']         = 'Sipariş Geçmişi (Müşteri)';
$_['entry_list']      		= 'Adres Listeleri';
$_['entry_email']      		= 'Sipariş E-postaları';
$_['entry_invoice']      	= 'Sipariş Faturaları';
$_['entry_tips']      		= 'Araç ipuçları';
$_['entry_error']      		= 'Hata Mesajı';
$_['entry_error_help']     	= 'En az bir müşteri grubu için alan gerekiyor';
$_['entry_regional_validation']      	= 'Ülkeye Dayalı Alan Doğrulaması';
$_['entry_regional_validation_help']    = 'Alanın geçerli olup olmadığını anlamak için doğrulamayı seçin.';
// Help
$_['help_sort_order']       = 'on alandan geriye doğru saymak için eksi kullanın.';
$_['important_note']       = 'Önemli Notlar';
$_['help_regex']            = 'Regex kullan. E.g: /[a-zA-Z0-9_-]/';
$_['address_message']       = 'Farklı bir adres formatı oluşturmak için Ödeme Sayfası (Ek alanlar) menüsünden değişkenleri kullanarak özel bir format belirleyin';
$_['fields_retreival1']      = "Özel alanları ödeme sayfasındaki adres alanlarına çekmek için şu değişkeni çağırın \$this->session->data['customer']['custom_field']['defined_identifier']";
$_['fields_retreival2']      = "2) Özel alanları ödeme sayfasındaki adres alanlarına çekmek için şu değişkeni çağırın \$this->session->data['payment']['custom_field']['defined_identifier']";
$_['fields_retreival3']      = "3) Özel alanları ödeme sayfasındaki adres alanlarına çekmek için şu değişkeni çağırın \$this->session->data['shipping']['custom_field']['defined_identifier']";

// Error
$_['xerror_permission']      = 'Uyarı: Özel alanları değiştirmek için izniniz yok!';
$_['xerror_name']            = 'Özel Alan Adı 1 ile 128 karakter arasında olmalıdır!';
$_['xerror_type']            = 'Uyarı: Özel Alan Değerleri gerekli!';
$_['xerror_custom_value']    = 'Özel Değer Adı 1 ile 128 karakter arasında olmalıdır!';
$_['xerror_error']           = 'Zorunlu bir alan için bir hata mesajı tanımlamalısınız!';
$_['xerror_identifier']      = 'Anlaşılır olmalı ve boşluk olmadan 1 ile 30 karakter arasında olmalıdır!';
