<?php
$_['heading_title']               ='<b> Kampanyalar Modülü</b> ';
$_['heading_title_all_promos']    ='<b> Kampanyalar Modülü</b> ';

$_['error_permission']            = 'Hata= Düzenleme Yetkiniz Bulunmamakta!';
$_['text_success']                = 'Başarılı= Değişikliker Kaydedildi!';
$_['text_enabled']                = 'Aktif';
$_['text_disabled']               = 'Pasif';
$_['button_cancel']			   = 'İptal';
$_['save_changes']				= 'Değişiklikleri Kaydet';
$_['text_default']				= 'Default';
$_['text_edit']					= 'Düzenle';
$_['text_module']				 = 'Modül';
// Control Panel
$_['entry_code']                  = 'Kampanya Modülü Durumu: ';
$_['entry_code_help'] 			 = 'Aktif yada Pasif';
$_['text_content_top']			= 'Üst kısım';
$_['text_content_bottom']		 = 'Alt kısım';
$_['text_column_left']			= 'Sol kısım';
$_['text_column_right']		   = 'Sağ kısım';
$_['entry_layout']        		= 'Sayfa düzeni:';
$_['entry_position']      		  = 'Konum:';
$_['entry_status']        		= 'Durum:';
$_['entry_sort_order']    		= 'Sıralama:';
$_['entry_action_options']		= 'Eylem:';
$_['entry_layout_options']        = 'Sayfa düzeni seçenekleri:';
$_['entry_position_options']      = 'Konum seçenekleri:';
$_['button_add_module'] 		   = 'Ekle';
$_['button_remove']			   = 'Kaldır';

// Licensing
$_['text_your_license']         = 'Your License';
$_['text_please_enter_the_code']= 'Please enter your product purchase license code:';
$_['text_activate_license']     = 'Activate License';
$_['text_not_having_a_license'] = 'Not having a code? Get it from here.';
$_['text_license_holder']       = 'License Holder';
$_['text_registered_domains']   = 'Registered domains';
$_['text_expires_on']           = 'License Expires on';
$_['text_valid_license']        = 'VALID LICENSE';
$_['text_manage']               = 'manage';
$_['text_get_support']          = 'Get Support';
$_['text_community']            = 'Community';
$_['text_ask_our_community']    = 'We have a big community. You are free to ask it about your issue on the forum.';
$_['text_browse_forums']        = 'Browse forums';
$_['text_tickets']              = 'Tickets';
$_['text_open_a_ticket']        = 'Want to comminicate one-to-one with our tech people? Then open a support ticket.';
$_['text_open_ticket_for_real'] = 'Open a ticket';
$_['text_pre_sale']             = 'Pre-sale';
$_['text_pre_sale_text']        = 'Have a brilliant idea for your webstore? Our team of developers can make it real.';
$_['text_bump_the_sales']       = 'Bump the sales';

//GENERAL helpers
$_['text_status'] = 'Durum:';

$_['text_sort_order'] = 'Sıralama:';
$_['helper_sort_order'] = 'İki kampanyada aktif olması durumunda sıraya göre işlem yapılır';

$_['text_stackable'] = 'Birleştirilebilir: ';
$_['helper_stackable'] = 'Birden fazla kampanya aynı anda çalıştırılabilir. ';


$_['text_promo_name'] = 'Kampanya İsmi:';
$_['helper_promo_name'] = 'Kampanya İsmi';

$_['text_module_name'] = 'Modül İsmi:';
$_['helper_module_name'] = 'Modül İsmi';

$_['text_start_date'] = 'Başlama Tarihi:';
$_['helper_start_date'] = 'Başlama Tarihi';

$_['text_end_date'] = 'Bitiş Tarihi:';
$_['helper_end_date'] = 'Bitiş Tarihi';

$_['text_total_uses'] = 'Toplam Kullanım Sayısı:';
$_['helper_total_uses'] = 'Toplam Kullanım Sayısı';

$_['text_user_total_uses'] = 'Bir müşteri en fazla kaç defa yararlanabilir:';
$_['helper_user_total_uses'] = 'Bir müşteri en fazla kaç defa yararlanabilir:';

$_['text_promo_type'] = 'Promosyon Tipi:';
$_['helper_promo_type'] = 'Promosyon Tipi';

$_['text_coupon_code'] = 'Kupon Kodu:';
$_['helper_coupon_code'] = 'Kupon Kodu';

$_['text_need_login'] = 'Giriş Yapmak Gerekli:';
$_['helper_need_login'] = 'Giriş Yapmak Gerekli';

$_['text_customer_groups'] = 'Müşteri Grubu:';
$_['helper_customer_groups'] = 'Müşteri Grubu';

$_['text_stores'] = 'Mağaza:';


?>