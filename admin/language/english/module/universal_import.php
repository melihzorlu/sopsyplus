<?php

// Heading

$_['heading_title'] = 'Excel/XML ile Ürün Yükle';



// Text

$_['text_module'] = 'Modül';

$_['text_browse'] = 'Gözat';

$_['text_continue'] = 'Sonraki Adım';

$_['text_add_column'] = 'Yeni Kolon';

$_['text_remove_column'] = 'Kolonu Sil';

$_['not_found'] = 'Bulunamadı';

$_['new'] = 'Yeni';



$_['text_success'] = 'Başarılı: Ayarlar başarılı bir şekilde kayıt edildi!';

$_['text_store_select'] = 'Mağaza:';

$_['text_import'] = 'İç Aktar';

$_['text_source_upload'] = 'Dosya Yükle';

$_['text_source_ftp'] = 'FTP';

$_['text_source_url'] = 'URL';

$_['text_source_path'] = 'Local Dosya';

$_['text_type_product'] = 'Ürün';

$_['text_type_product_update'] = 'Hızlı Ürün Güncelle';

$_['text_type_order'] = 'Sipariş';

$_['text_type_order_status_update'] = 'Sipariş Durumun Güncelle';

$_['text_type_category'] = 'Kategori';

$_['text_type_information'] = 'Bilgi Sayfaları';

$_['text_type_manufacturer'] = 'Üretici firma';

$_['text_type_customer'] = 'Satıcı';

$_['text_product'] = 'Ürünler';

$_['text_category'] = 'Kategoriler';

$_['text_information'] = 'Bilgi Sayfaları';

$_['text_manufacturer'] = 'Markalar';

$_['text_customer'] = 'Müşteriler';

$_['text_order_status'] = 'Sipariş Durumları';

$_['text_ignore'] = '';

$_['text_column'] = 'Sütun';

$_['text_next_step'] = '&ensp;Onayla ve Devam Et';

$_['text_start_process'] = 'İçe Aktarma İşlemini Başlat';

$_['text_pausing_process'] = 'Duraklatılıyor, lütfen son toplu iş bitene kadar bekleyin ';

$_['text_resume_process'] = 'İşlemi Sürdür';

$_['text_start_simu_process'] = '&ensp;Simulasyonu Başlat';

$_['text_pause_process'] = 'İçe aktarma devam ediyor - Duraklatmak için tıklayın';

$_['text_pause_simu_process'] = 'Simülasyon devam ediyor - Duraklatmak için tıklayın';

$_['text_previous_step'] = '&ensp;Önceki adım';

$_['text_data_preview'] = 'Veri önizleme';

$_['text_row'] = 'Satır/İşlem';

$_['text_status'] = 'Durum';

$_['text_message'] = 'Mesaj';



$_['text_action_update'] = 'Güncelle - Değiştir';

$_['text_action_soft_update'] = 'Güncelle - Koru';

$_['text_action_insert'] = 'Ekle';

$_['text_action_skip'] = 'Atla';

$_['text_action_delete'] = 'Sil';

$_['text_action_overwrite'] = 'Üzerine Yaz';

$_['text_action_rename'] = 'Adını Değiştir';

$_['text_img_action_keep'] = 'Asıl Resmi Koru';

$_['text_img_action_rename'] = 'Yeni resmi yeniden adlandır';

$_['text_img_action_overwrite'] = 'Yeni resimi üzerine yaz';



// Tabs

$_['text_tab_0'] = 'İçe Aktar';



$_['text_import_step1'] = '<b>Adım 1</b> - Dosya Seçimi';

$_['text_import_step2'] = '<b>Adım 2</b> - Aktarım Ayarları';

$_['text_import_step3'] = '<b>Adım 3</b> - Eşleştirme İşlemleri';

$_['text_import_step4'] = '<b>Adım 4</b> - Kategori Eşleştirme';

$_['text_import_step5'] = '<b>Adım 5</b> - Aktarıma Başla';



// Profile manager

$_['text_profile_dir_not_writable'] = 'Profil klasörü yazılabilir değil, klasörde (ve alt klasörlerde) yazma haklarına sahip olduğunuzdan emin olun:';

$_['text_profile_name'] = 'Profil Adı';

$_['text_new_profile'] = '- Yeni profil olarak kaydet -';

$_['text_save_profile'] = '&ensp;Profili kaydet';

$_['text_delete_profile'] = 'Profili sil';

$_['text_really_delete'] = 'Bu profili silmek istediğinizden emin misiniz?';

$_['text_save_profile_i'] = 'Mevcut ayarlarınızı daha fazla içe aktarmada kullanmak için kaydedin. <br/> Üzerine yazmak veya mevcut bir profil oluşturmak için mevcut profili seçebilirsiniz.';

$_['text_profile_saved'] = 'Profiliniz doğru şekilde kaydedildi';



// Step 1

$_['entry_name'] = 'Adı';

$_['entry_demo_file'] = 'Demo dosyasını yükle';

$_['entry_demo_file_i'] = 'Modülün davranışını test etmek için önceden yüklenmiş dosyaları kullanın. Kendi alanınızı yüklemek istiyorsanız bu alanı boş bırakın ve önceki alana bir dosya yükleyin.';

$_['entry_type'] = 'İçe aktarma türü';

$_['entry_type_i'] = 'Ne tür verileri içe aktarmak üzeresiniz ?';

$_['entry_profile'] = 'Profil Ayarları';

$_['text_select_profile'] = 'Profil Ayarlarını Seçiniz';

$_['entry_profile_i'] = 'Profil, 2. ve 3. adımlarda ayarlayabileceğiniz tüm ayarları içerir. 4. adımda yeni bir profil kaydedebilirsiniz';

$_['entry_file'] = 'Dosya kaynağını seçin';

$_['entry_file_i'] = 'Dosyanızı buradan seçin. <br/> Sınırsız boyut desteklenir. <br/> Uyumlu biçimler: CSV, XML, XLS, XLSX, ODS';

$_['source_ftp'] = 'FTP bağlantısı';

$_['source_ftp_i'] = 'Dosyayı almak için bir FTPye bağlanın ftp://';

$_['source_ftp_path'] = 'FTP file path';

$_['source_url'] = 'Dosya bağlantısı';

$_['source_url_i'] = 'Harici bir URL\'den dosya alma';

$_['source_path'] = 'Yerel dosya yolu';

$_['source_path_i'] = 'Bu, sunucunuzdaki bir dosyaya atıfta bulunuyor';

$_['text_extension_auto'] = 'Uzantıyı otomatik algıla';

$_['text_dropzone'] = 'Dosyayı buraya bırakın veya yüklemek için tıklayın';

$_['text_file_loaded'] = 'Dosya yüklendi:';

$_['text_file_error'] = 'Hata:';



// Step 2

$_['text_common_settings'] = 'Genel Ayarlar';

$_['entry_xml_node'] = 'XML öğe bloğu';

$_['entry_xml_node_i'] = 'Dosyanızdaki her öğe için xml düğümünü ayarlayın, örneğin <ürün> veya <öğe> (köşeli parantezsiz değer). Sistem otomatik olarak algılamaya çalışacaktır, sadece yanlış değer varsa değiştirin';

$_['entry_csv_separator'] = 'CSV alan ayırıcı';

$_['entry_multiple_separator'] = 'Çoklu değer ayırıcı';

$_['entry_multiple_separator_i'] = 'Alanda birden fazla değer varsa, bunları ayırmak için kullanılan karakteri tanımlayın';

$_['entry_csv_header'] = 'İlk satır başlıktır';

$_['entry_item_identifier'] = 'Ürün Eşleştirme Etiketi';

$_['entry_item_identifier_i'] = 'Eklemek üzere olduğunuz öğenin bu alanla eşleşip eşleşmediğini tespit edin';

$_['entry_item_exists'] = 'Sitede yüklü olan ürünü';

$_['entry_item_exists_i'] = 'Değiştirme modu: tüm değerler güncellenecek, boş sütun eşleme, gerçek bir değerin kaldırılacağı anlamına gelir (bu, varsayılan opencart yöntemini kullanır ve buna uygulanan değişiklikleri içerir) - Koruma modu: yalnızca sütun eşlemede ayarlanan alanları güncelleyecektir, boş alan, gerçek değerde değişiklik olmadığı anlamına gelir (opencart yönteminde değil, özel değerler burada uygulanamayabilir)';

$_['entry_item_not_exists'] = 'Yeni öğe işlemi';

$_['text_image_settings'] = 'Resim Ayarları';

$_['entry_image_download'] = 'Resim indir';

$_['entry_image_download_i'] = 'Görüntü harici url ise, otomatik olarak sunucunuza indirin, mevcut resimleri bağlamak için / image / catalog /';

$_['entry_image_exists'] = 'Resim varsa';

$_['text_delete_settings'] = 'Öğeleri kaldırma veya devre dışı bırakma';

$_['entry_delete'] = 'Mod';

$_['entry_delete_action'] = 'İşlem';

$_['entry_delete_batch'] = 'Etiketi içe aktar';

$_['text_delete_nothing'] = 'Hiçbir şeyi çıkarmayın';

$_['text_delete_all'] = 'İçe aktarmadan önce tüm öğeler için';

$_['text_delete_batch'] = 'Belirli bir içe aktarma etiketindeki tüm öğeler için';

$_['text_delete_missing'] = 'Yalnızca güncellenmeyen öğeler için';

$_['text_delete_missing_brand'] = 'Güncellenmemiş öğeler için, yalnızca güncellenmiş markalarda';

$_['text_delete_zero'] = 'Miktarı sıfıra ayarla';

$_['batch_delete_everything'] = 'Herhangi bir içe aktarma etiketi';

$_['batch_delete_empty'] = 'İçe aktarma etiketi yok';

$_['batch_delete_defined'] = 'Mevcut içe aktarma etiketi';

$_['batch_group_general'] = 'Öğeleri sil:';

$_['batch_group_specific'] = 'Veya yalnızca belirli içe aktarma etiketleriyle:';

$_['warning_delete'] = 'Uyarı: Öğeler veritabanından kaldırılacak, bunun istediğiniz şey olduğundan emin olun';

$_['text_deleted'] = 'Silinmiş öğe: ';

$_['text_nothing_deleted'] = 'silinecek bir şey yok';

$_['text_disabled_all'] = 'Tüm öğeleri devre dışı olarak ayarla';

$_['text_delete_delete'] = 'Sil (veritabanından kaldır)';

$_['text_delete_disable'] = 'Devre Dışı Bırak (durumu devre dışı olarak ayarla)';

$_['entry_image_exists_i'] = 'Aynı isme sahip resim varsa ne yapmalı?';

$_['entry_image_location'] = 'Resim konumu';

$_['entry_image_location_i'] = 'Resmin indirilmesi durumunda, bunları belirli bir dizine koyun (ör: products /). Bu parametre mevcut resimlerde kullanılmaz';

$_['entry_image_keep_path'] = 'URL yolunu çoğalt';

$_['entry_image_keep_path_i'] = 'Geçerli konumdakiyle aynı yapıyı koruyun. Örneğin, url myshop.com/dir/subdir/image.jpg ise resim dir / subdir / image.jpg dosyasına kaydedilir.';

$_['text_category_settings'] = 'Kategori Ayarları';

$_['entry_category_create'] = 'Varsa oluştur';

$_['entry_include_subcat'] = 'Kategori Tanımla';

$_['entry_include_subcat_i'] = 'Bu Masaüstü> PC> Klavyeler gibi tanımlanmış bir kategoriniz varsa, varsayılan olarak ürün Klavyelere girer, ebeveyn modunu ayarlarsanız Klavyelere ve PCye gider ve eğer tümüne ayarlarsanız bu 3 kategoride de tanımlanır';

$_['entry_filter_to_category'] = 'Filtreleri otomatik oluştur';

$_['entry_filter_to_category_i'] = 'İçe aktarılan ürünlerin filtrelerini karşılık gelen kategorilerine otomatik olarak ekleyin';

$_['text_include_subcat_none'] = 'Ürünü yalnızca tanımlanmış kategoriye ekleyin';

$_['text_include_subcat_parent'] = 'Ürünü üst kategoriye de ekleyin';

$_['text_include_subcat_all'] = 'Ürünü tüm ana kategorilere ekleyin';

$_['text_manufacturer_settings'] = 'Üretici Firma Ayarları';

$_['entry_manufacturer_create'] = 'Varsa oluştur';

$_['text_product_settings'] = 'Ürün Ayarları';

$_['entry_preserve_attribute'] = 'Öznitelikleri koru';

$_['entry_preserve_attribute_i'] = 'Etkinleştirilen öznitelikler içe aktarırken sıfırlanmayacaksa, bu yalnızca güncelleme koruma modunda çalışıyor!';

$_['entry_filter_generate'] = 'Filtre oluştur:';

$_['entry_filter_generate_i'] = 'Aşağıdaki alanlara göre ürün filtrelerini otomatik olarak oluşturmak için bu seçenekleri işaretleyin';

$_['entry_limit'] = 'Başlangıç ​​satırını içe aktar';

$_['entry_limit_i'] = 'İçe aktarılacak öğeleri bu satır numarasından içe aktarmaya başla';

$_['entry_limit_start'] = 'Başlangıç Satırı';

$_['entry_limit_max'] = 'Maks. Öğe';

$_['text_extra_fields'] = 'Extra alanlar';

$_['entry_extra_fields'] = 'Extra alanlar';

$_['entry_extra_fields_i'] = 'Virgülle ayrılmış olarak, ekstra işlevlerle daha fazla kullanım için ihtiyacınız olan fazladan alanı ekleyin, örneğin feedinizin mevcut bir alanında işlem yapabilir ve ardından sonucu fazladan bir alana kaydedebilirsiniz';



// Step 3

$_['tab_extra'] = 'Custom Fields';

$_['entry_extra'] = 'Custom Field';

$_['entry_extra_ml'] = 'Description Custom Field';

$_['entry_disable_config'] = 'Disable config';

$_['placeholder_disable_config'] = 'config_module_option, config_extra_option';

$_['placeholder_extra_col'] = 'Field name';

$_['tab_disable_cfg'] = 'Disable config options';

$_['info_disable_cfg'] = 'Just like the previous option this could be useful in case some third party module wants to insert data on item creation/edition.<br/>If you don\'t have corresponding data in your import file and the import fails because of the missing extra data you can either use the extra field option with empty value, or in some case it is even easier to just disable the corresponding config option.<br/><br/>For example if you have in your model something like that:<br/><code>if ($this->config->get(\'superfilter_status\')) {<br/>&nbsp;&nbsp;&nbsp;// insert module data...<br/>&nbsp;}</code><br/><br/>Then if this code part is making the import to fail you can just put in the field below "superfilter_status", it will temporary set this parameter to false so you can avoid related errors during import.<br/>Use comma as separator if you need to put various values.';

$_['info_extra_field'] = 'Here you can define your own custom fields, this is useful in case you have some module that is adding data into your tables.<br/>If the custom field is multilingual, then it is stored in description part so you have to use Description custom field, can be useful for seo module for example which are adding SEO H1 and other data, find out which is the name used by this module and put "meta_h1" or "seo_h1" into Field name, and in right selector just select to which column it will take the data.<br/><br/>If during the import you have some undefined index errors it is many time because you have custom fields added by modules, so you have to use this section to add them for the import process, the error will tell you what is the exact name required.<br/><br/>If you set "update preserve" mode in step 2 then put in custom field name the name as it appears into your database, if you use "update replace" mode put the name of the input field as it appears on your product edit page (you can find it by using right click &gt; inspect element on your browser).';

$_['text_remove_extra_col'] = 'Remove custom field';

$_['text_add_extra_field'] = 'Add new custom field';

$_['text_add_extra_field_ml'] = 'Add new description custom field';

$_['tab_functions'] = 'Extra Functions';

$_['text_add_function'] = 'Add function';

$_['text_function_type'] = 'Tipi';

$_['text_function_action'] = 'Action';

$_['text_function_target'] = 'Save result in';

$_['text_function_same_field'] = 'Current field';

$_['text_function_extra_field'] = 'Extra field';

$_['text_remove_function'] = 'Remove function';

$_['tab_quick_update'] = 'Quick update';

$_['text_quick_update_identifier'] = 'Here you can update your items quickly with most common updated values.<br/>The field below is mandatory as it is the one used to identify the item to update.';

$_['entry_product_id'] = 'Product ID';

$_['entry_order_id'] = 'Order ID';

$_['entry_order_id_user'] = 'Random Order ID';

$_['entry_notify'] = 'Notify';

$_['entry_tracking_no'] = 'Tracking number';

$_['entry_tracking_url'] = 'Tracking URL';

$_['entry_order_status_comment'] = 'Comment';

$_['entry_order_status'] = 'Order status';

$_['help_field_order_status'] = 'All order will be updated with given order status, can be order status ID or name, you can also let empty and force the order status with the Default field';

$_['entry_seo_h1'] = 'SEO H1';

$_['entry_seo_h2'] = 'SEO H2';

$_['entry_seo_h3'] = 'SEO H3';

$_['entry_img_title'] = 'Resim başlığı';

$_['entry_img_alt'] = 'Resim alt';

$_['entry_separator'] = 'Ayırıcı';

$_['entry_subcat_separator'] = 'Alt kategori ayırıcı';

$_['entry_subcat_separator_i'] = 'Örnek cat1 > subcat1 ; cat2 > subcat2';

$_['entry_dimension_l'] = 'Uzunluk';

$_['entry_dimension_w'] = 'Genişlik';

$_['entry_dimension_h'] = 'Yükseklik';

$_['entry_category_id'] = 'Kategori ID';

$_['entry_information_id'] = 'Bilgi ID';

$_['entry_manufacturer_id'] = 'Üretici firma ID';

$_['entry_customer_id'] = 'Mağaza ID';

$_['entry_value_modifier'] = 'Mod';

$_['entry_value_modifier_i'] = 'Değiştir: gerçek değeri yenisiyle değiştir - Ekle: gerçek değere yeni değer ekle - Çıkar: yeni değeri gerçek değere çıkarır';

$_['entry_email'] = 'Email';

$_['import_default_value'] = 'Varsayılan';

$_['button_save'] = 'Kaydet';

$_['entry_default'] = 'Varsayılan';

$_['entry_default_i'] = 'Bu alana bu alan için varsayılan bir değer girin, alan boşsa veya herhangi bir değerle eşlenmemişse varsayılan değer kullanılır';

$_['help_field_category'] = 'Bu alanda birden çok kategori tanımlanmışsa, birden çok değer ayırıcı kullanın, örneğin: Cat1> subcat1; Cat2> alt kedi2';

$_['help_field_related_id'] = 'Birden çok değer eklemek için birden çok ayırıcı kullanın; değer, 2. adımda ayarlanan ürün kimliği veya öğe tanımlayıcısı olabilir';

$_['help_field_image'] = 'Birden çok değer olması durumunda, ilk değer seçilir';

$_['help_field_product_image'] = 'Çeşitli görüntüleri içe aktarmak için birden çok ayırıcılı bir alan kullanabilir veya görüntüleri çeşitli alanlardan almak için sütun ekleyi tıklayabilirsiniz.';

$_['help_field_product_option_xml__'] = 'Use this format: <option> <optionType>type</optionType> <optionNameEn>Name english</optionNameEn> <optionValueEn>Value english</optionValueEn> <optionPrice>Price</optionPrice> <optionRequired>Required</optionRequired> </option> - The options and options values are created automatically if they doesn\'t exist.';

$_['help_field_product_option__'] = 'Can be formatted in various ways - 1: <name>:<value> (type is automatically set to select if the option not exists) - 2: <type>:<name>:<value> 3: <type>:<name>:<value>:<price> - 4: <type>:<name>:<value>:<price>:<qty> - 5: <type>:<name>:<value>:<price>:<qty>:<subtract> - 6: <type>:<name>:<value>:<price>:<qty>:<subtract>:<weight> - 7: <type>:<name>:<value>:<price>:<qty>:<subtract>:<weight>:<required> - The options and options values are created automatically if they doesn\'t exist.';

$_['help_field_product_attribute__'] = 'Can be formatted in various ways - 1: <attribute_group_name>:<attribute_name>:<value> - 2: <attribute_name>:<value> 3: <value> (header is used as attribute name) - 4: <ul><li>Group name: value</li></ul>The group and attribute are created automatically if they doesn\'t exists. In cases 2 and 3, if attribute not exist it is assigned to group `Default`';

$_['help_field_product_special'] = 'Must be formatted like this: <price> - or <customer_group_id>:<price> - or <customer_group_id>:<priority>:<price> - or <customer_group_id>:<priority>:<price>:<date_end> - or <customer_group_id>:<priority>:<price>:<date_start>:<date_end> (if a date is not inserted it will be set as no time limit)';

$_['help_field_product_discount'] = 'Must be formatted like this: <price> - or <price>:<date_end> - or <price>:<date_start>:<date_end> - or <qty>:<price>:<date_start>:<date_end> - or <customer_group_id>:<qty>:<price>:<date_start>:<date_end> - or <customer_group_id>:<qty>:<priority>:<price>:<date_start>:<date_end>';

$_['help_field_product_id'] = 'Use this if you want to force product ID';

$_['help_field_manufacturer_id'] = 'Use either manufacturer ID or manufacturer name';

$_['help_field_product_category'] = 'Use either category ID or category name';

$_['help_field_product_filter'] = 'Format your filters like this: filter_group:filter_name';

$_['help_field_status'] = $_['help_field_notify'] = 'It works with these values: 1/0, on/off, true/false, enabled/disabled, yes/no, active/inactive';

$_['help_field_order_status_comment'] = 'You can use these tags: {tracking_no} {tracking_url}';

$_['help_field_parent_id'] = 'If more than 2 levels this field should be formatted with subcategory separator: Cat1 > Subcat1 > Subcat2';

$_['entry_salt'] = 'salt';

$_['help_field_password'] = '';

$_['help_field_salt'] = 'Salt is additional protection for the password, this field must be set if the passwords you import are already encrypted, if not set your users won\'t be able to login';

$_['entry_pwd_hash'] = 'Encryption';

$_['entry_pwd_hash_i'] = 'If your password is encrypted check the corresponding value here and fill the salt field which is necessary for the customer to login';

$_['text_pwd_clear'] = 'Clear password';

$_['text_pwd_hash'] = 'Encrypted password';



// extra functions

$_['xfn_group_string'] = 'Text operations';

$_['xfn_group_regex'] = 'Regex text operations';

$_['xfn_group_number'] = 'Number operations';

$_['xfn_group_html'] = 'HTML';

$_['xfn_group_web'] = 'Web';

$_['xfn_group_other'] = 'Special functions';

$_['xfn_manual_value'] = 'Manual value -&gt;';

$_['xfn_append'] = '<i class="fa fa-fw">&#xf038;</i> Append';

$_['xfn_append_i'] = 'Append something after a field';

$_['xfn_prepend'] = '<i class="fa fa-fw">&#xf036;</i> Prepend';

$_['xfn_prepend_i'] = 'Prepend something before a field';

$_['xfn_uppercase'] = '<i class="fa fa-fw">&#xf031;</i> Uppercase';

$_['xfn_uppercase_i'] = 'Example: LUKE, I AM YOUR FATHER';

$_['xfn_lowercase'] = '<i class="fa fa-fw">&#xf031;</i> Lowercase';

$_['xfn_lowercase_i'] = 'Example: luke, i am your father';

$_['xfn_ucfirst'] = '<i class="fa fa-fw">&#xf031;</i> Uppercase on first letter';

$_['xfn_ucfirst_i'] = 'Example: Luke, I am your father';

$_['xfn_ucwords'] = '<i class="fa fa-fw">&#xf031;</i> Uppercase on first word letters';

$_['xfn_ucwords_i'] = 'Example: Luke, I Am Your Father';

$_['xfn_substr'] = '<i class="fa fa-fw">&#xf0c4;</i> Truncate string';

$_['xfn_substr_i'] = 'Get x first chars of a field';

$_['xfn_urlify'] = '<i class="fa fa-fw">&#xf0c1;</i> Format for url';

$_['xfn_urlify_i'] = 'Format a string for use in seo keyword, apply lowercase and replace whitespaces by dashes, if translit is applied all accentuated chars will be transformed to their ascii equivalent';

$_['xfn_substr_set'] = 'Get';

$_['xfn_substr_of'] = 'first chars from the field';

$_['xfn_nl2br'] = '<i class="fa fa-fw">&#xf121;</i> Convert line breaks to HTML';

$_['xfn_nl2br_i'] = 'Line breaks will be converted to html format with a br tag';

$_['xfn_strip_tags'] = '<i class="fa fa-fw">&#xf121;</i> Strip HTML tags';

$_['xfn_strip_tags_i'] = 'Remove any HTML tag to get a clean text';

$_['xfn_replace'] = '<i class="fa fa-fw">&#xf0d0;</i> Replace text';

$_['xfn_replace_set'] = 'Replace';

$_['xfn_regex_replace'] = '<i class="fa fa-fw">&#xf0d0;</i> Regex replace';

$_['xfn_regex_replace_i'] = 'Replace a part of a string using regex, for example if you want to change a field like brand:Sony|type:Laptop to put MyBrand instead of any other brand, following regex search: brand:(.+)| replace: brand:MyBrand|';

$_['xfn_regex'] = '<i class="fa fa-fw">&#xf0c4;</i> Regex extract';

$_['xfn_regex_i'] = 'Extract a part of a string using regex, use the () to define the part you want to extract, for example if you have a field like brand:Sony|type:Laptop, and you want to extract Sony for inserting in brands apply the following regex: brand:(.+)|';

$_['xfn_regex_remove'] = '<i class="fa fa-fw">&#xf12d;</i> Regex remove';

$_['xfn_regex_remove_i'] = 'Remove text with regex, for example remove any special char from a string: [^0-9a-zA-Z]';

$_['xfn_remove'] = '<i class="fa fa-fw">&#xf12d;</i> Remove text';

$_['xfn_remove_set'] = 'Remove';

$_['xfn_remote_content_set'] = 'Get content from url of field';

$_['xfn_remote_content'] = '<i class="fa fa-fw">&#xf019;</i> Remote content';

$_['xfn_remote_content_i'] = 'Get the content of the page contained in an url, can be useful if only a link is provided in feed to get description or other data.';

$_['xfn_add'] = '<i class="fa fa-fw">&#xf1ec;</i> Add';

$_['xfn_subtract'] = '<i class="fa fa-fw">&#xf1ec;</i> Subtract';

$_['xfn_multiply'] = '<i class="fa fa-fw">&#xf1ec;</i> Multiply';

$_['xfn_divide'] = '<i class="fa fa-fw">&#xf1ec;</i> Divide';

$_['xfn_round'] = '<i class="fa fa-fw">&#xf1ec;</i> Round';

$_['xfn_skip_set'] = 'Skip current item if';

$_['xfn_skip'] = '<i class="fa fa-fw">&#xf054;</i> Skip item';

$_['xfn_delete_item'] = '<i class="fa fa-fw">&#xf014;</i> Delete item';

$_['xfn_multiple_separator'] = '<i class="fa fa-fw">&#xf03a;</i> Multiple values separator';

$_['xfn_multiple_separator_set'] = 'Set multiple values separator as';

$_['xfn_for_column'] = 'for column';

$_['xfn_of'] = 'of';

$_['xfn_to'] = 'to';

$_['xfn_by'] = 'by';

$_['xfn_in'] = 'in';

$_['xfn_for'] = 'for';

$_['xfn_from'] = 'from';

$_['xfn_after'] = 'after';

$_['xfn_before'] = 'before';

$_['xfn_is_equal'] = 'is equal to';

$_['xfn_is_not_equal'] = 'is not equal to';

$_['xfn_is_greater'] = 'is greater than';

$_['xfn_is_lower'] = 'is lower than';

$_['xfn_mode'] = 'mode';

$_['xfn_precision'] = 'with a precision of';

$_['xfn_precision_0'] = 'Integer (1)';

$_['xfn_precision_1'] = '1 decimal (1.2)';

$_['xfn_precision_2'] = '2 decimals (1.23)';

$_['xfn_precision_3'] = '3 decimals (1.234)';

$_['xfn_precision_4'] = '4 decimals (1.2345)';

$_['text_delete_if'] = 'Delete if';

$_['text_value_is'] = 'value is equal to';

$_['text_urlify_basic'] = 'Basic';

$_['text_urlify_default'] = 'Basic formatting';

$_['text_urlify_ascii'] = 'Translit to ascii';

$_['text_translit'] = 'Translit';

$_['info_extra_functions'] = 'With extra functions you can make some specific operations on your source data, like for example multiply a number, or put a string in uppercase, etc.<br/><br/>Usage:<ol><li>Select the function you want in the box below</li><li>Click on "Add function"</li><li>Select the source field on which to apply the function</li><li>Define other parameters if any</li><li>Select the target field in which you want to save the result</li></ol>You can apply multiple operation on a same field, they will be applied in the order they are defined.<br/><br/>If you want to save some data in extra fields don\'t forget to add your extra fields in step 2.';



// option bindings

$_['text_optbinding_name'] = 'Tipi';

$_['text_optbinding_bind_to'] = 'Alan';

$_['text_get_optbinding'] = '&ensp;Gelişmiş Seçenekler';

$_['entry_length_class'] = 'Uzunluk Sınıfı';

// category bindings

$_['tab_cat_binding'] = 'Kategori Bağlantılar';

$_['entry_cat_binding_mode'] = 'Bağlantı modu';

$_['text_cat_binding_default'] = 'Karışık - alan değeri olan ilişkili kategoriler ve ilişkili olmayan kategoriler ekle';

$_['text_cat_binding_exclusive'] = 'Özel - yalnızca ilişkili kategoriler ekle, kategori alanı değeri yok sayıldı';

$_['text_cat_binding_skip'] = 'Bağlı olmayanı atla - yalnızca ilişkili kategoriler ekle, bağlı olmayan kategorilere sahip öğeler atlanacak';

$_['text_catbinding_name'] = 'Ana Kategori';

$_['text_catbinding_bind_to'] = 'Bağlanan';

$_['text_get_bindings'] = 'Kategori listesini yenile';

$_['text_no_bindings'] = 'Hiçbir kategori tanımlanmadı, gerçek içe aktarma dosyanızdaki tüm kategorileri görmek için yenile düğmesine basın.';

$_['text_no_binding'] = '- Yok -';

$_['info_cat_binding_title'] = 'Kategori Bağlantılar';

$_['info_cat_binding'] = 'Kaynak verilerinizde kendinize karşılık gelmeyen kategori adlarınız varsa, kaynak dosyanızdaki her kategori için bir bağlayıcı tanımlamak için bu özelliği kullanabilirsiniz. <br/> <br/> Kullanım: <ul> <li> Değilse tamamlandığında, "Bağlantılar" sekmesinde kategori alanınızı seçin </li> <li> Kaynak dosyanızda bulunan tüm kategorilerin listesini almak için "Kategori listesini yenile" yi tıklayın </li> <li> Hangi açık kategori kategorisine gittiğinizi seçin her bir kaynak kategorisini aktarmak istiyorum </li> </ul> Tüm kategorileri ayarlamak zorunlu değildir, sadece bağlayıcı olmayanları silin, bu kategoriler varsayılan davranışı uygulanır (kategorinin mevcut olup olmadığını kontrol edin ve oluşturmaya çalışın) Kategorilerinizin yalnızca bağlı değerlere dayalı olarak eklenmesini istiyorsanız, harici ciltleme modunu seçin.';

$_['info_col_binding_skip'] = 'Hiçbir kategori bağlaması tanımlanmadığı için öğe yok sayıldı';



// Step 4

$_['text_simu_summary'] = 'Özet (10 satır):';

$_['text_full_simu_summary'] = 'Rapor:';

$_['text_item_to_update'] = 'Güncellenecek öğe';

$_['entry_row_status'] = 'Satır eylemi';

$_['text_simu_inserted'] = 'Eklemek için';

$_['text_simu_updated'] = 'Güncellemek için';

$_['text_simu_disabled'] = 'Devre dışı bırakmak için';

$_['text_simu_qtyzero'] = 'Adet sıfıra ayarla';

$_['text_simu_deleted'] = 'Sil';

$_['text_simu_skipped'] = 'Geç';

$_['text_simu_error'] = 'Hata';



// Step 5

$_['text_import_label'] = 'Etiketi içe aktar';

$_['text_import_label_i'] = 'Bu içe aktarma oturumuna bir ad verin, ardından ürün listesinde bu kez içe aktarılan ürünleri filtreleyebilirsiniz.';

$_['text_process_summary'] = 'İşlem özeti';

$_['text_rows_csv'] = 'Dosyadaki toplam satır sayısı';

$_['text_rows_process'] = 'İşlenecek toplam satır';

$_['text_rows_insert'] = 'Eklenecek toplam satır';

$_['text_rows_update'] = 'Güncellenecek toplam satır sayısı';

$_['text_process_done'] = 'İşlem durumu';

$_['text_rows_processed'] = 'İşlenmiş';

$_['text_rows_inserted'] = 'Eklenen';

$_['text_rows_updated'] = 'Güncellenmiş';

$_['text_rows_disabled'] = 'Engelli';

$_['text_simu_qtyzero'] = 'Adet sıfıra ayarla';

$_['text_rows_deleted'] = 'Silindi';

$_['text_rows_skipped'] = 'Atlandı';

$_['text_rows_error'] = 'Hata';

$_['text_empty_line_skip'] = 'Boş satır';





$_['entry_color_scheme'] = 'Renk şeması:<span class="help">Bazı renk şemalarına hızlı erişim, tasarım sekmesinde düzenleyin</span>';

$_['entry_logo'] = 'Logo:';

$_['entry_feed_title'] = 'Adı:';

$_['entry_cache_delay'] = 'Önbellek Gecikmesi:<span class="help">Yeniden oluşturulana kadar oluşturulan dosyayı görüntülemek için kaç kez ?</span>';

$_['entry_language'] = 'Dil:<span class="help"></span>';

$_['entry_feed_url'] = 'Feed URL:<span class="help">Bu urlyi hedef hizmete verin</span>';

$_['entry_additional_image'] = 'Ek resimler';



$_['text_no_change'] = '- Değiştirme -';

$_['text_on'] = 'Açık';

$_['text_off'] = 'Kapalı';



// File format

$_['text_format_csv'] = 'CSV';

$_['text_format_xml'] = 'XML';

$_['text_format_xls'] = 'XLS';

$_['text_format_xlsx'] = 'XLSX';

$_['text_format_ods'] = 'ODS';

$_['text_format_pdf'] = 'PDF';

$_['text_format_html'] = 'HTML';



// Export

$_['text_tab_1'] = 'Dışarı Aktar';

$_['entry_export_type'] = 'Aktarma Türü';

$_['entry_export_type_i'] = 'Ne tür verileri dışa aktarmak istiyorsunuz?';

$_['entry_export_format'] = 'Aktarma Formatı';

$_['entry_export_format_i'] = 'Dosya formatını seçiniz';

$_['text_start_export'] = '&nbsp;&nbsp;Dosyayı Dışarı Aktar';

$_['export_all'] = '- Hepsi -';



// Export filters

$_['export_filters'] = 'Filtreler';

$_['total_export_items'] = 'Dışa aktarılacak toplam ürün:';

$_['filter_language'] = 'Dil';

$_['filter_manufacturer'] = 'Üretici firma';

$_['filter_manufacturer_i'] = 'Birden fazla üretici seçebilir, üreticinizi hızlı bir şekilde bulmak için arayabilirsiniz';

$_['filter_category'] = 'Kategori';

$_['filter_category_i'] = 'Birden fazla kategori seçebilir, kategorinizi hızlı bir şekilde bulmak arayabilirsiniz';

$_['filter_store'] = 'Mağaza';

$_['filter_limit'] = 'Limit';

$_['filter_limit_i'] = 'Dışa aktarılacak öğe sayısını sınırla Başlangıç: bu öğeden dışa aktarmaya başla - Limit: kaç tane dışa aktarılacağını';

$_['filter_limit_start'] = 'Başlangıç';

$_['filter_limit_limit'] = 'Limit';



// Export options

$_['export_options'] = 'Seçenekler';

$_['export_fields'] = 'Dışa aktarılacak alanları seçiniz';

$_['param_image_path'] = 'Resim modu';

$_['param_image_path_i'] = 'Resminizin tam URL sini almak için URL yi kullanın';

$_['image_path_relative'] = 'Local dosya';

$_['image_path_absolute'] = 'URL';



// Configuration

$_['text_tab_2'] = 'Seçenekler';

$_['tab_option_1'] = 'Performans';

$_['tab_option_2'] = 'Cron jobs';

$_['tab_option_3'] = 'Ana seçenekler';

$_['entry_default_import_label'] = 'Varsayılan içe aktarma etiketi';

$_['default_label_i'] = 'Set here the default label for your import session, this will help you to easily filter products imported in a specific session in product list.<br/><br/>You can use these tags : <code>[profile] [day] [month] [year]</code>';

$_['entry_batch_import'] = 'Parti numarasını içe aktar';

$_['entry_batch_export'] = 'Parti numarasını dışa aktar';

$_['entry_sleep'] = 'İşlem gecikmesi';

$_['batch_import_i'] = '<h4>Batch Number</h4><p>Choose the number of items processed on each request, adjust this setting to improve import/export performance.<br/>High number may speed up the process time but too large number may result in failure depending your server ressources.</p><h4 style="margin-top:20px">Process delay</h4><p>Importing and exporting are using very high amount of ressources from server, on shared host or low-profile server it may use almost all ressources and then your website can be unaccessible during the process.<br/>The delay parameter allows you to wait a few time before each item processing, this time will free up some ressource to let the server breath a while and process other requests.<br/>The number is in milliseconds, which means 1000 ms = 1 second, recommended setting is from 50 to 500.<br/>Keep in mind a big value on this setting will increase a lot the import process time.</p>';



// Cron

$_['text_tab_cron_1'] = 'Configuration';

$_['text_tab_cron_2'] = 'Report';

$_['text_cli_log_save'] = 'Save log file';

$_['text_cli_clear_logs'] = 'Clear logs';

$_['entry_cron_key'] = 'Secure key';

$_['entry_cron_key_i'] = 'Define your own secure key, this must be included in the cron command';

$_['entry_cron_command'] = 'Cron commands';

$_['entry_cron_command_i'] = 'This is the command you will have to enter in your cron jobs (adapt [path_to_php] do make it match with your server executable, can be /usr/bin/php, /usr/local/bin/php, /usr/local/cpanel/3rdparty/bin/php, ...). Don\'t forget to adapt give exact profile name';

$_['cron_jobs_i'] = 'You can automatically run your predefined profiles by using cron jobs.<br/><br/>All you have to do is to define a profile by adjusting all the settings in first tab, then save it in step 4. Put the exact name of your profile at the end of the command.<br/>A report will be generated so you can consult here if all gone well.<br/><br/>Please note the cron jobs do work only with files from URL, FTP or Local path, not with uploaded files which are for instant use.<br/><br/><b>Set up your cron jobs:</b><ol><li>Go into import tab and choose the file to work with you must use a file in URL, FTP, or Local path</li><li>Set up your feed with the parameters you need, go until step 4</li><li>In step 4 save your profile</li><li>Go into your cPanel or server administration to set up a cron job</li><li>Copy the command you need below (don\'t forget to change the profile name to yours)</li><li>That\'s done, your cron will run based on your parameters and you can check if all is going well into the report tab.</li></ol>';



$_['text_tab_about'] = 'Hakkında';



// Warnings

$_['warning'] = 'Uyarı';

$_['warning_category_id'] = 'Kategori ID bulunamadı.';

$_['warning_discount_format'] = 'Yanlış indirim biçimi, biçim: <b>&lt;customer_group_id&gt;:&lt;quantity&gt;:&lt;priority&gt;:&lt;price&gt;:&lt;date_start&gt;:&lt;date_end&gt;</b>, bu indirim ürüne eklenmeyecek';

$_['warning_remote_image_not_found'] = 'Uzak resim bulunamadı: ';



// Entry

$_['entry_status'] = 'Durum:';



// Message

$_['text_skip_insert']		= 'Yeni öğe eylemi, yeni öğeleri atlayacak şekilde ayarlandı';

$_['text_skip_update']		= 'Mevcut öğe işlemi mevcut öğeleri atlayacak şekilde ayarlandı';



// Info

$_['info_title_default']		= 'Yardım';

$_['info_msg_default']	= 'Bu konuyla ilgili yardım bölümü bulunamadı';

$_['info_soft_update_mode']	= 'Koruma modunu güncelle: yalnızca görüntülenen alanlar güncellenir.';

$_['current_server_path']	= 'Kök dizin: ';

$_['info_attributes_title']		= 'Özellik içe aktarma';

$_['info_attributes_xml_title']		= 'Değer dizisi için gelişmiş parametreler (XML içe aktarma)';

$_['info_attributes']		= '

<p>Çeşitli şekillerde biçimlendirilebilir:

<ul><li>attribute_group_name:attribute_name:value</li>

<li>attribute_name:value</li>

<li>değeri (başlık özellik adı olarak kullanılır)</li>

<li>&lt;ul&gt;&lt;li&gt;Group name: value&lt;/li&gt;&lt;/ul&gt;</li></ul></p>

<p>Grup ve özellik yoksa otomatik olarak oluşturulur. Özellik mevcut değilse ve grup tanımlanmamışsa, otomatik olarak "Varsayılan" grubuna atanır.</p>';

$_['info_attributes_xml']		= '

<p>Örneğin şöyle bir diziniz var:<br/>

<code style="white-space:pre">'.print_r(array('0'=> array('name' => 'Attribute name', 'value' => array('val' => 'Attribute value'))), true).'</code></p>

<p>Bu durumda, ana diziyi değer dizisini içeren alanla eşleşecek şekilde tanımlayın ve aşağıdaki belirli parametrelerde Özellik adını ve Özellik değerini de değer / val olarak ayarlayın.</p>';

$_['info_options_title']		= 'Seçenek içe aktarma';

$_['info_options_xml_title']		= 'Değer dizisi için gelişmiş parametreler (XML içe aktarma)';

$_['info_options']		= '

<p>Çeşitli şekillerde biçimlendirilebilir:

<ul><li>name : value (tür, seçenek yoksa otomatik olarak seçilir)</li>

<li>type : name : value</li>

<li>type : name : value : price</li>

<li>type : name : value : price : qty</li>

<li>type : name : value : price : qty : subtract</li>

<li>type : name : value : price : qty : subtract : genişlik</li>

<li>type : name : value : price : qty : subtract : genişlik : zorunlu</li></ul></p>

<p>Seçenek ve seçenek değerleri yoksa otomatik olarak oluşturulur.</p>';

$_['info_options_xml']		= '

<p>Örnek:<br/>

<code style="white-space:pre">'.print_r(array('0'=> array('name' => 'Option name', 'value' => array('val' => 'Option value'))), true).'</code></p>

<p>Bu durumda, değer dizisini içeren alanla eşleşecek ana seçeneği tanımlayın ve aşağıdaki belirli parametrelerde Seçenek adını ada ve Seçenek değerini değer / val olarak ayarlayın.<br/>

Birden çok değer alanınız varsa ve bir veya diğer alanda değerler olabiliyorsa, her iki alana da | bunları ayırmak için: field1 | field2</p>';



// Error

$_['warning_incorrect_image_format'] = 'Resim formatı geçersiz:';

$_['error_curl'] = 'Dosya indirilmeye çalışılırken bir hata oluştu. Bunun nedeni dosyanın mevcut olmaması veya sunucuya yasaklanmış erişim olabilir.<br/>Detay: <b>%s</b>';

$_['error_extension'] = 'Dosya türü bulunamadı veya içe aktarma için uyumlu değil. <b>%s</b> İzin verilen içe aktarma türleri: csv, xml, xls, xlsx.';

$_['error_file_not_found'] = 'Uyarı: Dosya açılamıyor. Lütfen yolun doğru olup olmadığını kontrol edin.';

$_['error_xml_no_data'] = 'Uyarı: Verilen xml içerisinde veri bulunamadı, veri kümenizi içeren düğüm adını doğru ayarladığınızdan emin olun.';

$_['error_permission'] = 'Uyarı: Bu modülü değiştirme izniniz yok!';

$_['error_permission_demo'] = 'Demo modu, kaydetmeye izin verilmiyor';



$_['tab_general']            = 'Ürün Bilgileri';

$_['tab_data']           	 = 'Veri';

$_['tab_attribute']          = 'Özellik Ekle';

$_['tab_option']           	 = 'Seçenek Ekle';

$_['tab_discount']        	 = 'İndirim';

$_['tab_special']            = 'Kampanya Fiyatı';

$_['tab_image']          	 = 'Fotoğraf Ekle';

$_['tab_links']        		 = 'Bağlantılar';



// Entry

$_['entry_name']             = 'Ürün adı';

$_['entry_meta_title'] 	 	 = 'Meta Başlığı';

$_['entry_meta_keyword'] 	 = 'Meta Anahtar Kelimeleri';

$_['entry_meta_description'] = 'Meta (Google\'da Görülecek Metin- Önerilen uzunluk boşluklar dahil 160 karakterdir.)';

$_['entry_description']      = 'Açıklama (Sitede görülecek açıklama metnidir.)';

$_['entry_store']            = 'Mağazalar';

$_['entry_keyword']          = 'SEO Anahtar Kelime';

$_['entry_model']            = 'Ürün Kodu';

$_['entry_sku']              = 'Barkod Numarası';

$_['entry_upc']              = 'UPC';

$_['entry_ean']              = 'EAN';

$_['entry_jan']              = 'JAN';

$_['entry_isbn']             = 'ISBN';

$_['entry_mpn']              = 'Kaç Günde Kargoda';

$_['entry_location']         = 'Konum';

$_['entry_shipping']         = 'Kargo Gerekli mi?';

$_['entry_manufacturer']     = 'Marka';

$_['entry_date_available']   = 'Tarih Uygun';

$_['entry_quantity']         = 'Stok Adedi';

$_['entry_minimum']          = 'Minimum Miktar';

$_['entry_stock_status']     = 'Stokta Yok';

$_['entry_price']            = 'Fiyat (Örnek 10.90)';

$_['entry_tax_class']        = 'Vergi Oranı';

$_['entry_points']           = 'Puan';

$_['entry_option_points']    = 'Puan';

$_['entry_subtract']         = 'Stok Düş';

$_['entry_weight_class']     = 'Ağırlık Sınıfı';

$_['entry_weight']           = 'Ağırlığı';

$_['entry_length']           = 'Boy Sınıfı';

$_['entry_dimension']        = 'Boyutları (U x G x Y)';

$_['entry_image']            = 'Resim';

$_['entry_customer_group']   = 'Müşteri Grubu';

$_['entry_date_start']       = 'Başlangıç Tarihi';

$_['entry_date_end']         = 'Son Tarih';

$_['entry_priority']         = 'Öncelik';

$_['entry_attribute']        = 'Nitelik';

$_['entry_attribute_group']  = 'Özellik Grubu';

$_['entry_text']             = 'Metin';

$_['entry_option']           = 'Seçenek';

$_['entry_option_value']     = 'Opsiyon Değeri';

$_['entry_required']         = 'Zorunlu mu ?';

$_['entry_status']           = 'Durum';

$_['entry_sort_order']       = 'Sıralama';

$_['entry_category']         = 'Kategori Seçiniz';

$_['entry_filter']           = 'Filtreler';

$_['entry_download']         = 'İndirme';

$_['entry_related']          = 'Benzer Ürünler';

$_['entry_tag']          	 = 'Etiketler';

$_['entry_layout']           = 'Yerleşim Geçersiz Kılma';

$_['entry_profile']          = 'Profil';

// custom field

$_['text_custom_field']        	 = 'Özel alan';

$_['entry_select_option']        = 'Seçeneği Seç :';

$_['entry_select_date']          = 'Tarih Seçiniz :';

$_['entry_select_datetime']      = 'Tarih-Saat Seç :';

$_['entry_select_time']          = 'Saati Seç :';

$_['entry_enter_text']           = 'Metin Gir :';
