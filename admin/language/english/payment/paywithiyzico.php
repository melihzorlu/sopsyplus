<?php
// Heading
$_['heading_title']    = 'Pay with iyzico';

// Text
$_['text_module']      = 'Payment';
$_['text_paywithiyzico'] = '<a href="https://www.iyzico.com/isim-icin/iyzico-ile-ode" target="_blank"><img width="30%" src="view/image/payment/pay-with-iyzico.svg" alt="iyzico" title="iyzico"/></a>';
$_['extension_status'] = 'Extension Status';
$_['text_enabled'] = 'Enabled';
$_['text_disabled'] = 'Disabled';

// Order Status
$_['entry_order_status'] = 'Order Status';
$_['entry_status'] = 'Status';
$_['entry_cancel_order_status'] = "Cancel Order Status";
$_['order_status_after_payment_tooltip'] = 'Order status after successful payment';
$_['order_status_after_cancel_tooltip'] = 'Order status after successful cancel';
$_['entry_sort_order'] = 'Sort Order';

// Error
$_['error_permission'] = 'You are not authorized!';
$_['error_status'] = 'Invalid Status';
$_['error_order_status_id'] = 'Invalid Order Status!';
$_['error_cancel_order_status_id'] = 'Invalid Cancel Order Status!';