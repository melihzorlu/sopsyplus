<?php
/*
	gittigidiyor.com ürün işlemleri için model dosyasıdır.
	* ürün silme
	* ürün gönderme
	* ürün güncelleme
	* stok ve  fiyat güncelleme
	* ürün eşitleme
	kullanılan tablolar
	".db_prefix."ggproducts - gittigidiyor ürünlerinin bulunduğu tablodur
*/
class ModelGittigidiyorProduct extends Model {
	public function vergihesapla($value, $tax_class_id, $calculate = true){
		$amountm = 0;
		$tax_query = $this->db->query("SELECT tr1.tax_class_id, tr2.tax_rate_id, tr2.name, tr2.rate, tr2.type, tr1.priority FROM " . DB_PREFIX . "tax_rule tr1 LEFT JOIN " . DB_PREFIX . "tax_rate tr2 ON (tr1.tax_rate_id = tr2.tax_rate_id) INNER JOIN " . DB_PREFIX . "tax_rate_to_customer_group tr2cg ON (tr2.tax_rate_id = tr2cg.tax_rate_id) LEFT JOIN " . DB_PREFIX . "zone_to_geo_zone z2gz ON (tr2.geo_zone_id = z2gz.geo_zone_id) LEFT JOIN " . DB_PREFIX . "geo_zone gz ON (tr2.geo_zone_id = gz.geo_zone_id) WHERE tr1.based = 'store' AND tr2cg.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND z2gz.country_id = '" . (int)$this->config->get('config_country_id') . "' AND (z2gz.zone_id = '0' OR z2gz.zone_id = '" . (int)$this->config->get('config_zone_id') . "') ORDER BY tr1.priority ASC");

		foreach ($tax_query->rows as $result) {
			$tax_rates[$result['tax_class_id']][$result['tax_rate_id']] = array(
				'tax_rate_id' => $result['tax_rate_id'],
				'name'        => $result['name'],
				'rate'        => $result['rate'],
				'type'        => $result['type'],
				'priority'    => $result['priority']
			);
		}

		$tax_rate_data = array();
		if (isset($tax_rates[$tax_class_id])) {
			foreach ($tax_rates[$tax_class_id] as $tax_rate) {
				if (isset($tax_rate_data[$tax_rate['tax_rate_id']])) {
					$amount = $tax_rate_data[$tax_rate['tax_rate_id']]['amount'];
				} else {
					$amount = 0;
				}
				if ($tax_rate['type'] == 'F') {
					$amount += $tax_rate['rate'];
				} elseif ($tax_rate['type'] == 'P') {
					$amount += ($value / 100 * $tax_rate['rate']);

				}
				$tax_rate_data[$tax_rate['tax_rate_id']] = array(
					'tax_rate_id' => $tax_rate['tax_rate_id'],
					'name'        => $tax_rate['name'],
					'rate'        => $tax_rate['rate'],
					'type'        => $tax_rate['type'],
					'amount'      => $amount
				);
			}
		}
		foreach ($tax_rate_data as $tax_ratem) {
			
				if ($calculate != 'P' && $calculate != 'F') {
					$amountm += $tax_ratem['amount'];
				} elseif ($tax_ratem['type'] == $calculate) {
					$amountm += $tax_ratem['amount'];
				}
			
			
		}
		if(isset($amountm)){
			return $value + $amountm;
		} else {
			return $value;
		}
		
	}

	public function eszamanla($client, $shop_id, $page){
		if(VERSION < 3){
			$token = $this->session->data['token'];
			$token_link = 'token=' . $this->session->data['token'];
		} else {
			$token = $this->session->data['user_token'];
			$token_link = 'user_token=' . $this->session->data['user_token'];
		}

		$product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) ORDER BY p.product_id DESC LIMIT ".$page.",1");
		if($product_query->num_rows > 0){
			$eszamanlar = $client->getProduct('', $product_query->row['model']);
			if($eszamanlar->ackCode != 'failure'){
				$prdggquery = $this->db->query("SELECT * FROM " . DB_PREFIX . "ggproduct WHERE product_id = '".$product_query->row['product_id']."' AND shop_id = '". $shop_id."' AND gg_id = '".$eszamanlar->productDetail->productId."'");
				if($prdggquery->num_rows < 1){
					$this->db->query("INSERT INTO " . DB_PREFIX . "ggproduct SET product_id = '".$product_query->row['product_id']."', shop_id = '". $shop_id."', gg_id = '".$eszamanlar->productDetail->productId."'");
					return array('status' => 0, 'product_id' => $product_query->row['product_id'], 'msg' => $product_query->row['name'].' Eşleştirildi', 'next' => 'index.php?route=gittigidiyor/product/eszamanla&'.$token_link.'&page='.($page+1));
				} else {
					return array('status' => 0, 'product_id' => $product_query->row['product_id'], 'msg' => $product_query->row['name'].' zaten eşleşmiş ', 'next' => 'index.php?route=gittigidiyor/product/eszamanla&'.$token_link.'&page='.($page+1));
				}
			} else {
				return array('status' => 0, 'product_id' => $product_query->row['product_id'], 'msg' => $product_query->row['name'].' - '.$eszamanlar->error->message, 'next' => 'index.php?route=gittigidiyor/product/eszamanla&'.$token_link.'&page='.($page+1));
			}
		} else {
			return array('status' => 0, 'product_id' => $product_query->row['product_id'], 'msg' => 'Ürün bulunamadı');
		}
	}

	public function fiyatstokGuncelle($client, $shop_id, $product_id = null, $page, $stocksifirla = 0){
		if(VERSION < 3){
			$token = $this->session->data['token'];
			$token_link = 'token=' . $this->session->data['token'];
		} else {
			$token = $this->session->data['user_token'];
			$token_link = 'user_token=' . $this->session->data['user_token'];
		}	


		ini_set('max_execution_time', 0); //0=NOLIMIT
		ini_set('set_time_limit', 0); //0=NOLIMIT
		$dosya_adi = DIR_LOGS.'gglogs.txt';
		/*
			ürün öncelik sırası
			1. ürün eşleşmesi
			2. kategori eşleşmesi
			3. global eşleşme
		*/

		if($product_id == null){
			$product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE p.product_id IN (SELECT product_id FROM " . DB_PREFIX . "ggproduct WHERE product_id = p.product_id AND (gg_id != '' OR gg_id != '0')) ORDER BY p.product_id DESC LIMIT ".$page.",1");
		} else {
			$product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE p.product_id = '".$product_id."' AND p.product_id  IN (SELECT product_id FROM " . DB_PREFIX . "ggproduct WHERE product_id = p.product_id)");
		}
		
		if($product_query->num_rows > 0){
			// ürün varsa
			$shop_query = $this->db->query("SELECT * FROM ".DB_PREFIX."ggshops WHERE id = '".$shop_id."'");
			if(strlen($product_query->row['name']) > 65){
				$title =  mb_substr($product_query->row['name'], 0, 65, 'utf8');
			} else {
				$title = $product_query->row['name'];
			}
			$price = $product_query->row['price'];
			$subtitle = $shop_query->row['default_subtitle'];
			$comission_type = $shop_query->row['difference_type'];
			$commission = $shop_query->row['difference_value'];
			$cargo_company = $shop_query->row['cargo_company'];
			$shipping_condition = $shop_query->row['shipping_condition'];
			$shipping_price = $shop_query->row['shipping_price'];
			$shipping_aprice = $shop_query->row['shipping_aprice'];

			$listingdays = $shop_query->row['ship_listingday'];


			$description = str_replace('{aciklama}', $product_query->row['description'], $shop_query->row['description']);

			// kategori eşleştirmesini kontrol edelim
			$prodtocat = $this->db->query("SELECT * FROM ".DB_PREFIX."product_to_category WHERE product_id = '".$product_query->row['product_id']."' ORDER BY category_id DESC");
			if($prodtocat->num_rows > 0){
				foreach ($prodtocat->rows as $occat) {
					$category_query = $this->db->query("SELECT * FROM ".DB_PREFIX."category WHERE category_id = '".$occat['category_id']."' AND gg_id != '0'");
					if($category_query->row){
						// kategori ürün gönderim durumu
						if(isset($category_query->row['gcatstatus'])){
							if($category_query->row['gcatstatus'] == 1){
								$status = 1;
							} else {
								$status = 0;
							}
						}
						if(isset($category_query->row['gg_id'])){
							if($category_query->row['gg_id'] != "0"){
								$catid = $category_query->row['gg_id'];
							}
						}

						if(isset($category_query->row['ggcomission'])){
							if($category_query->row['ggcomission'] != 0){
								$commission = $category_query->row['ggcomission'];
							}
						}
						if(isset($category_query->row['ggattr'])){
							if($category_query->row['ggattr'] != ''){
								$attribute = json_decode($category_query->row['ggattr']);
							}
						}
					}
				}
			}
			

			// birebir eşleştirme
			$prdggquery = $this->db->query("SELECT * FROM " . DB_PREFIX . "ggproduct WHERE product_id = '".$product_query->row['product_id']."' AND shop_id = '". $shop_id."'");	
			if($prdggquery->num_rows > 0){
				if(isset($prdggquery->row['gg_salestatus']) and $prdggquery->row['gg_salestatus'] != '0'){
					if($stocksifirla == 0){
						if($prdggquery->row['gg_salestatus'] == 4){
							$status = 1;
						} else {
							$status = 0;
						}
					}
				}
				if(isset($prdggquery->row['gg_title']) and $prdggquery->row['gg_title'] != ''){
					$title = $prdggquery->row['gg_title'];
				}
				
				if(isset($prdggquery->row['gg_price']) and $prdggquery->row['gg_price'] != '0'){
					$price = $prdggquery->row['gg_price'];
				}
				if(isset($prdggquery->row['gg_description']) and $prdggquery->row['gg_description'] != ''){
					$description = $prdggquery->row['gg_description'];
				}
				if(isset($prdggquery->row['gg_subtitle']) and $prdggquery->row['gg_subtitle'] != ''){
					$subtitle = $prdggquery->row['gg_subtitle'];
				}
				if(isset($prdggquery->row['gg_catid']) and $prdggquery->row['gg_catid'] != '' and $prdggquery->row['gg_catid'] != '0'){
					$catid = $prdggquery->row['gg_catid'];
				}
				
				if(isset($prdggquery->row['gg_commission']) and $prdggquery->row['gg_commission'] != '0'){
					$commission = $prdggquery->row['gg_commission'];
				}
				if(isset($prdggquery->row['gg_attribute']) and $prdggquery->row['gg_attribute'] != ''){
					$attribute = json_decode($prdggquery->row['gg_attribute']);
				}
			}



			if($comission_type == 'yuzde'){
				// yüzde olarak arttır
				$yuzde = ($price / 100) * $commission;
				$price = $price + $yuzde;
			} else if($comission_type == 'sabit'){
				// sabit arttır
				$price = $price +  $commission;
			} else if($comission_type == 'yuzdeazalt'){
				// yüzde azalat
				$yuzde = ($price / 100) *  $commission;
				$price = $price - $yuzde;
			} else if($comission_type == 'sabitazalt'){
				// sabit olarak azalt
				$price = $price -  $commission;
			}

			if($shop_query->row['currency'] != '1'){
				$connect_web = simplexml_load_file('http://www.tcmb.gov.tr/kurlar/today.xml');
				$usd = $connect_web->Currency[0]->BanknoteSelling;
				$euro = $connect_web->Currency[3]->BanknoteSelling;
				if($shop_query->row['currency'] == '2'){
					$price = (float)$price * (float)$usd;
				} else {
					$price = $price * $euro;
				}
			}


			if($shop_query->row['tax_option'] == 1){
				$price = $this->vergihesapla($price, $product_query->row['tax_class_id'], true);
			}

			
			if(!isset($catid)){
				$dosya = fopen ($dosya_adi , 'a') or die ("Dosya açılamadı!"); 
					$metin = date('d.m.Y H:i:s')." - ".$product_query->row["model"]." ürününün kategorisi eşleşmemiştir\r\n";
					fputs($dosya , $metin); 
				if($product_id == null){
					return array('status' => 0, 'product_id' => $product_query->row['product_id'], 'msg' => $product_query->row['name'].' ürününün kategorisi eşleşmemiştir!', 'next' => 'index.php?route=gittigidiyor/product/sendProduct&'.$token_link.'&page='.($page+1));
				} else {
					return array('status' => 0, 'product_id' => $product_query->row['product_id'], 'msg' =>  $product_query->row['name'].' ürününün kategorisi eşleşmemiştir!');
				}
				exit;
			} else {
				if(empty($catid)){
					$dosya = fopen ($dosya_adi , 'a') or die ("Dosya açılamadı!"); 
					$metin = date('d.m.Y H:i:s')." - ".$product_query->row["name"]." ürününün kategorisi eşleşmemiştir\r\n";
					fputs($dosya , $metin); 
					if($product_id == null){
						return array('status' => 0, 'product_id' => $product_query->row['product_id'], 'msg' => $product_query->row['name'].' ürününün kategorisi eşleşmemiştir!', 'next' => 'index.php?route=gittigidiyor/product/sendProduct&'.$token_link.'&page='.($page+1));
					} else {
						return array('status' => 0, 'product_id' => $product_query->row['product_id'], 'msg' =>  $product_query->row['name'].' ürününün kategorisi eşleşmemiştir!');
					}
					exit;
				}
			}
			// stok sıfırlıyorsa status durumunu daima 1 yapsın
			if($stocksifirla == 1 or $stocksifirla == 2){ $status = 1; }
			// ürün gönderime kapalı ise
			if($status == 0){
				$dosya = fopen ($dosya_adi , 'a') or die ("Dosya açılamadı!"); 
				$metin = date('d.m.Y H:i:s')." - ".$product_query->row["name"]." ürünü gönderime Kapalıdır\r\n";
				fputs($dosya , $metin); 
				if($product_id == null){
					return array('status' => 0, 'product_id' => $product_query->row['product_id'], 'msg' => 'Bu ürün gönderime kapalıdır!', 'next' => 'index.php?route=gittigidiyor/product/sendProduct&'.$token_link.'&page='.($page+1));
				} else {
					return array('status' => 0, 'product_id' => $product_query->row['product_id'], 'msg' => $product_query->row['name'].' ürünü gönderime kapalıdır!');
				}
				exit;
			}
			$atrbute = '';
			if(isset($attribute) and $attribute != ''){
				foreach ($attribute as $key => $value) {
					if(is_array($value)){
						if($value[0] != ''){
							$atrbute .='<spec name="'.$key.'" value="'.$value[0].'" type="Combo" required="true" />';
						}
					} else {
						if($value != ''){
							$atrbute .='<spec name="'.$key.'" value="'.$value.'" type="Combo" required="true" />';
						}
					}
				}
			}
			$photos = '';
			$photos .= '<photo photoId="0">
				            <url>'.str_replace(' ', '%20', 'https://'.$_SERVER['SERVER_NAME'].'/image/'.$product_query->row["image"]).'</url>
				            <base64></base64>
				        </photo>';
			$additional_image = $this->db->query("SELECT * FROM  " . DB_PREFIX . "product_image WHERE product_id = '".$product_query->row['product_id']."'");
			foreach ($additional_image->rows as $key => $ekresim) {
				$photos .= '<photo photoId="'.($key+1).'">
					            <url>'.str_replace(' ', '%20', 'https://'.$_SERVER['SERVER_NAME'].'image/'.$ekresim["image"]).'</url>
					            <base64></base64>
					        </photo>';
				if(($key+1) == 7){ break; }
			}
			// $shipping_condition
			$kargolar = explode(',', $cargo_company);
			$kargo_sirketleri  = '';
			foreach ($kargolar as $kargo){
				$kargo_sirketleri .= '
				<cargoCompanyDetail>
				    <name>'.ltrim(rtrim($kargo)).'</name>
				    <cityPrice>5.90</cityPrice>
               		<countryPrice>5.90</countryPrice>
				</cargoCompanyDetail>';
			}
			$kargo_kod  = 'B'; // varsayılan olarak buyer (alıcı öder)
			if($shipping_condition == 1){
				// x TL ücretsiz
				if($price > $shipping_price){
					$kargo_kod  = 'S';
				} else {
					$kargo_kod  = 'B';
				}
			} else if($shipping_condition == 2){
				// kargo gereksiz seçili ürünlerde
				if($product_query->row['shipping'] == 0){
					$kargo_kod  = 'S';
				} else {
					$kargo_kod  = 'B';
				}
			} else if($shipping_condition == 3){
				// fiyata dahil
				$kargo_kod  = 'S';
				$price = $price + $shipping_aprice;
			}

			

			if($stocksifirla == 1){ $sec_quantity = 0; } else { $sec_quantity = $product_query->row["quantity"]; }
			$insertprice = $client->updatePrice($prdggquery->row['gg_id'], null, number_format($price, 2, '.', ''), true);
			$msg = '';
			if($insertprice->ackCode == 'success'){
				$msg .= '<div class="alert alert-success" style="padding:5px !important; margin-bottom:5px;">'.$product_query->row['name']. 'ürünü için fiyat güncellemesi : '.$insertprice->result."</div>";
			} else {
				$msg .= '<div class="alert alert-danger" style="padding:5px !important; margin-bottom:5px;">'.$product_query->row['name']. 'ürünü için fiyat güncellemesi : '.$insertprice->error->message."</div>";
			}
			$insertstock = $client->updateStock($prdggquery->row['gg_id'], null, $sec_quantity, true);
			if($insertstock->ackCode == 'success'){
				$msg .= '<div class="alert alert-success" style="padding:5px !important; margin-bottom:5px;">'.$product_query->row['name']. ' ürünü için stok güncellemesi : '.$insertstock->result."</div>";
			} else {
				$msg .= '<div class="alert alert-danger" style="padding:5px !important; margin-bottom:5px;">'.$product_query->row['name']. ' ürünü için stok güncellemesi : '.$insertstock->error->message."</div>";
			}
			
			$dosya = fopen ($dosya_adi , 'a') or die ("Dosya açılamadı!"); 
			$metin = date('d.m.Y H:i:s')." - ".$msg;
			fputs($dosya , $metin); 
			if($product_id == null){
				return array('status' => 1, 'product_id' => $product_query->row['product_id'], 'msg' => $msg, 'next' => 'index.php?route=gittigidiyor/product/stokfiyatguncelle&'.$token_link.'&page='.($page+1));
			} else {
				return array('status' => 1, 'product_id' => $product_query->row['product_id'], 'msg' => $msg);
			}	
		} else {
			// ürün yoksa
			return array('status' => 0, 'msg' => '<div class="alert alert-danger">Ürün Aktarımı Tamamlandı</div>');
		}
	}


	/* 
		gttigidiyor ürün gönderme fonksiyonu
	*/
	public function SaveProduct($client, $shop_id, $product_id = null, $page, $stocksifirla = 0){
		if(VERSION < 3){
			$token = $this->session->data['token'];
			$token_link = 'token=' . $this->session->data['token'];
		} else {
			$token = $this->session->data['user_token'];
			$token_link = 'user_token=' . $this->session->data['user_token'];
		}	

		ini_set('max_execution_time', 0); //0=NOLIMIT
		ini_set('set_time_limit', 0); //0=NOLIMIT

		$dosya_adi = DIR_LOGS.'gglogs.txt';
		/*
			ürün öncelik sırası
			1. ürün eşleşmesi
			2. kategori eşleşmesi
			3. global eşleşme
		*/

		 $ch = curl_init();
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_URL, 'https://www.tcmb.gov.tr/kurlar/today.xml'); 
            $kur_data = curl_exec($ch); // execute curl request
            curl_close($ch);
            $kur_xml = simplexml_load_string($kur_data);
            foreach ($kur_xml->Currency as $value) {
                if($value->CurrencyName == 'US DOLLAR'){
                    $kur_usd = (string)$value->BanknoteSelling;
                }
                if($value->CurrencyName == 'EURO'){
                    $kur_eur = (string)$value->BanknoteSelling;
                }
            }



		if($product_id == null){
			$product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE p.product_id NOT IN (SELECT product_id FROM " . DB_PREFIX . "ggproduct WHERE product_id = p.product_id) ORDER BY p.product_id DESC LIMIT ".$page.",1");
		} else {
			$product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE p.product_id = '".$product_id."' AND (p.product_id  NOT IN (SELECT product_id FROM " . DB_PREFIX . "ggproduct WHERE product_id = p.product_id) OR p.product_id IN (SELECT product_id FROM ".DB_PREFIX."ggproduct WHERE product_id = p.product_id AND gg_id = '0'))");
		}
	
		if($product_query->num_rows > 0){
			// ürün varsa
			$shop_query = $this->db->query("SELECT * FROM ".DB_PREFIX."ggshops WHERE id = '".$shop_id."'");
			if(strlen($product_query->row['name']) > 65){
				$title =  mb_substr($product_query->row['name'], 0, 65, 'utf8');
			} else {
				$title = $product_query->row['name'];
			}
			$price = $product_query->row['price'];
			if($product_query->row['currency_id'] == 2){ // usd 
                $price = (float)$price * $kur_usd; 
            } else if($product_query->row['currency_id'] == 3){ // eur 
                $price = (float)$price * $kur_eur; 
            }

			$subtitle = $shop_query->row['default_subtitle'];
			$comission_type = $shop_query->row['difference_type'];
			$commission = $shop_query->row['difference_value'];
			$cargo_company = $shop_query->row['cargo_company'];
			$shipping_condition = $shop_query->row['shipping_condition'];
			$shipping_price = $shop_query->row['shipping_price'];
			$shipping_aprice = $shop_query->row['shipping_aprice'];
			$ship_listingday = $shop_query->row['ship_listingday'];
			$ship_cityprice = $shop_query->row['ship_cityprice'];
			$ship_countryprice = $shop_query->row['ship_countryprice'];

			$description = str_replace('{aciklama}', $product_query->row['description'], $shop_query->row['description']);

			// kategori eşleştirmesini kontrol edelim
			$prodtocat = $this->db->query("SELECT * FROM ".DB_PREFIX."product_to_category WHERE product_id = '".$product_query->row['product_id']."' ORDER BY category_id DESC");
			if($prodtocat->num_rows > 0){
				foreach ($prodtocat->rows as $occat) {
					$category_query = $this->db->query("SELECT * FROM ".DB_PREFIX."category WHERE category_id = '".$occat['category_id']."' AND gg_id != '0'");
					if($category_query->row){
						// kategori ürün gönderim durumu
						if(isset($category_query->row['gcatstatus'])){
							if($category_query->row['gcatstatus'] == 1){
								$status = 1;
							} else {
								$status = 0;
							}
						}
						if(isset($category_query->row['gg_id'])){
							if($category_query->row['gg_id'] != "0"){
								$catid = $category_query->row['gg_id'];
							}
						}

						if(isset($category_query->row['ggcomission'])){
							if($category_query->row['ggcomission'] != 0){
								$commission = $category_query->row['ggcomission'];
							}
						}
						if(isset($category_query->row['ggattr'])){
							if($category_query->row['ggattr'] != ''){
								$attribute = (array)json_decode($category_query->row['ggattr']);
							}
						}
					}
				}
			}
			

			// birebir eşleştirme
			$prdggquery = $this->db->query("SELECT * FROM " . DB_PREFIX . "ggproduct WHERE product_id = '".$product_query->row['product_id']."' AND shop_id = '". $shop_id."'");	
			if($prdggquery->num_rows > 0){
				if(isset($prdggquery->row['gg_salestatus']) and $prdggquery->row['gg_salestatus'] != '0'){
					if($stocksifirla == 0){
						if($prdggquery->row['gg_salestatus'] == 1){
							$status = 1;
						} else {
							$status = 0;
						}
					}
				}
				if(isset($prdggquery->row['gg_title']) and $prdggquery->row['gg_title'] != ''){
					$title = $prdggquery->row['gg_title'];
				}
				
				if(isset($prdggquery->row['gg_price']) and $prdggquery->row['gg_price'] != '0'){
					$price = $prdggquery->row['gg_price'];
				}
				if(isset($prdggquery->row['gg_description']) and $prdggquery->row['gg_description'] != ''){
					$description = $prdggquery->row['gg_description'];
				}
				if(isset($prdggquery->row['gg_subtitle']) and $prdggquery->row['gg_subtitle'] != ''){
					$subtitle = $prdggquery->row['gg_subtitle'];
				}
				if(isset($prdggquery->row['gg_catid']) and $prdggquery->row['gg_catid'] != '' and $prdggquery->row['gg_catid'] != '0'){
					$catid = $prdggquery->row['gg_catid'];
				}
				
				if(isset($prdggquery->row['gg_commission']) and $prdggquery->row['gg_commission'] != '0'){
					$commission = $prdggquery->row['gg_commission'];
				}
				if(isset($prdggquery->row['gg_attribute']) and $prdggquery->row['gg_attribute'] != ''){
					$attribute = (array)json_decode($prdggquery->row['gg_attribute']);
				}
			}

			if($comission_type == 'yuzde'){
				// yüzde olarak arttır
				$yuzde = ((float)$price / 100) * (float)$commission;
				$price = (float)$price + (float)$yuzde;
			} else if($comission_type == 'sabit'){
				// sabit arttır
				$price = $price +  $commission;
			} else if($comission_type == 'yuzdeazalt'){
				// yüzde azalat
				$yuzde = ($price / 100) *  $commission;
				$price = $price - $yuzde;
			} else if($comission_type == 'sabitazalt'){
				// sabit olarak azalt
				$price = $price -  $commission;
			}

		
			
			if(!isset($catid)){
				$dosya = fopen ($dosya_adi , 'a') or die ("Dosya açılamadı!"); 
					$metin = date('d.m.Y H:i:s')." - ".$product_query->row["model"]." ürününün kategorisi eşleşmemiştir\r\n";
					fputs($dosya , $metin); 
				if($product_id == null){
					return array('status' => 0, 'product_id' => $product_query->row['product_id'], 'msg' => $product_query->row['name'].' ürününün kategorisi eşleşmemiştir!', 'next' => 'index.php?route=gittigidiyor/product/sendProduct&'.$token_link.'&page='.($page+1));
				} else {
					return array('status' => 0, 'product_id' => $product_query->row['product_id'], 'msg' =>  $product_query->row['name'].' ürününün kategorisi eşleşmemiştir!');
				}
				exit;
			} else {
				if(empty($catid)){
					$dosya = fopen ($dosya_adi , 'a') or die ("Dosya açılamadı!"); 
					$metin = date('d.m.Y H:i:s')." - ".$product_query->row["name"]." ürününün kategorisi eşleşmemiştir\r\n";
					fputs($dosya , $metin); 
					if($product_id == null){
						return array('status' => 0, 'product_id' => $product_query->row['product_id'], 'msg' => $product_query->row['name'].' ürününün kategorisi eşleşmemiştir!', 'next' => 'index.php?route=gittigidiyor/product/sendProduct&'.$token_link.'&page='.($page+1));
					} else {
						return array('status' => 0, 'product_id' => $product_query->row['product_id'], 'msg' =>  $product_query->row['name'].' ürününün kategorisi eşleşmemiştir!');
					}
					exit;
				}
			}
			// stok sıfırlıyorsa status durumunu daima 1 yapsın
			if($stocksifirla == 1 or $stocksifirla == 2){ $status = 1; }
			// ürün gönderime kapalı ise
			if($status == 0){
				$dosya = fopen ($dosya_adi , 'a') or die ("Dosya açılamadı!"); 
				$metin = date('d.m.Y H:i:s')." - ".$product_query->row["name"]." ürünü gönderime Kapalıdır\r\n";
				fputs($dosya , $metin); 
				if($product_id == null){
					return array('status' => 0, 'product_id' => $product_query->row['product_id'], 'msg' => 'Bu ürün gönderime kapalıdır!', 'next' => 'index.php?route=gittigidiyor/product/sendProduct&'.$token_link.'&page='.($page+1));
				} else {
					return array('status' => 0, 'product_id' => $product_query->row['product_id'], 'msg' => $product_query->row['name'].' ürünü gönderime kapalıdır!');
				}
				exit;
			}
			$atrbute = '';
			
			$photos = '';
			$photos .= '<photo photoId="0">
				            <url>'.str_replace(' ', '%20', 'https://'.$_SERVER['SERVER_NAME'].'/image/'.$product_query->row["image"]).'</url>
				            <base64></base64>
				        </photo>';
		    $specphoto = '<photo photoId="0">
				            <url>'.str_replace(' ', '%20', 'https://'.$_SERVER['SERVER_NAME'].'/image/'.$product_query->row["image"]).'</url>
				            <base64></base64>
				        </photo>';

			$additional_image = $this->db->query("SELECT * FROM  " . DB_PREFIX . "product_image WHERE product_id = '".$product_query->row['product_id']."'");
			foreach ($additional_image->rows as $key => $ekresim) {
				$photos .= '<photo photoId="'.($key+1).'">
					            <url>'.str_replace(' ', '%20', 'https://'.$_SERVER['SERVER_NAME'].'/image/'.$ekresim["image"]).'</url>
					            <base64></base64>
					        </photo>';
				if(($key+1) == 7){ break; }
			}


			


			$kargo_kod  = 'B'; // varsayılan olarak buyer (alıcı öder)
			if($shipping_condition == 1){
				// x TL ücretsiz
				if($price > $shipping_price){
					$kargo_kod  = 'S';
				} else {
					$kargo_kod  = 'B';
				}
			} else if($shipping_condition == 2){
				// kargo gereksiz seçili ürünlerde
				if($product_query->row['shipping'] == 0){
					$kargo_kod  = 'S';
				} else {
					$kargo_kod  = 'B';
				}
			} else if($shipping_condition == 3){
				// fiyata dahil
				$kargo_kod  = 'S';
				$price = $price + $ship_countryprice;
			}

			if($shop_query->row['tax_option'] == 1){
				$price = $this->vergihesapla($price, $product_query->row['tax_class_id'], true);
			}


			if($kargo_kod == 'S'){
				$kargolar = explode(',', $cargo_company);
				$kargo_sirketleri  = '';
				foreach ($kargolar as $kargo){
					$kargo_sirketleri .= '
					<cargoCompanyDetail>
					    <name>'.ltrim(rtrim($kargo)).'</name>
					    <cityPrice></cityPrice>
	               		<countryPrice></countryPrice>
					</cargoCompanyDetail>';
				}
			} else {
				// $shipping_condition
				$kargolar = explode(',', $cargo_company);
				$kargo_sirketleri  = '';
				foreach ($kargolar as $kargo){
					$kargo_sirketleri .= '
					<cargoCompanyDetail>
					    <name>'.ltrim(rtrim($kargo)).'</name>
					    <cityPrice>'.$ship_cityprice.'</cityPrice>
	               		<countryPrice>'.$ship_countryprice.'</countryPrice>
					</cargoCompanyDetail>';
				}
			}

			$xml_variant = '';
			$secenekler = array();
			$secenekler = $this->getProductOptions($product_query->row['product_id']);

			$options = $secenek_ar = array();
			$secenek_es = $this->db->query("SELECT * FROM ".DB_PREFIX."ggcategories WHERE category_code = '".$catid."'")->row;

			//sabit değer
			$client = $this->model_gittigidiyor_general->getAuth();
			$specs = $client->getCategoryVariantSpecs($catid);
			if($secenekler and empty($secenek_es['variants'])){
				$dosya = fopen ($dosya_adi , 'a') or die ("Dosya açılamadı!"); 
				$metin = date('d.m.Y H:i:s')." - ".$product_query->row["name"]." ürününün seçenek eşleştirmesi yapılmamıştır\r\n";
				fputs($dosya , $metin); 
				if($product_id == null){
					return array('status' => 0, 'product_id' => $product_query->row['product_id'], 'msg' => $metin, 'next' => 'index.php?route=gittigidiyor/product/sendProduct&'.$token_link.'&page='.($page+1));
				} else {
					return array('status' => 0, 'product_id' => $product_query->row['product_id'], 'msg' => $metin);
				}
				exit;
			}
			
			if($secenekler and $secenek_es['variants']){
				$scnk_eslestirme = json_decode($secenek_es['variants']);
				foreach ($scnk_eslestirme->{'ocsenecek_deger'} as $skey => $svalue) {
					if(strstr($skey, "sabit")){
						$ggname_id = end(explode('_', $skey));
						$ggvalue_id = $svalue;
						foreach ($specs->specs->spec as $kspec) {
							if($kspec->nameId == $ggname_id){
								$ggname = $kspec->name;
							}
							foreach ($kspec->specValues->specValue as $deger) {
								if($ggvalue_id == $deger->valueId){
									$ggvalue_name = $deger->value;
								}
							}
						}
						$secenek_deger[$ggname][] = $ggname_id.'|'.$ggname.'|'.$ggvalue_id.'|'.$ggvalue_name.'|'.$ggname.'|'.$ggvalue_name.'|0|'.$sec_quantity.'|9999999';
						$secenek_ar[] = $secenek_deger[$ggname];
					}
				}
				
				// opencart seçenekleri
				foreach ($secenekler as $secenek){
					$secenek_deger = array();
					foreach ($secenek['product_option_value'] as $secenek_val){
						$povid = $secenek_val['product_option_value_id'];
						$secenk_degeri = $this->db->query("SELECT * FROM  " . DB_PREFIX . "option_value_description WHERE option_value_id = '".$secenek_val['option_value_id']."'");
						if($secenek_val['price'] == '0'){
							$s_fiyat = '0';
						} else {
							if($shop_query->row['tax_option'] == 1){
								$s_fiyat = $this->vergihesapla($secenek_val['price'], $product_query->row['tax_class_id']);
							} else {
								$s_fiyat = $secenek_val['price'];
							}
							if(isset($option_rebate->{$povid})){
								$op_commission = $option_rebate->{$povid};
								// fiyat komisyon ayarlamaları
								if($comission_type == 'yuzde'){
									// yüzde olarak arttır
									$yuzde = ($s_fiyat / 100) * $op_commission;
									$s_fiyat = $s_fiyat + $yuzde;
								} else if($comission_type == 'sabit'){
									// sabit arttır
									$s_fiyat = $s_fiyat +  $op_commission;
								} else if($comission_type == 'yuzdeazalt'){
									// yüzde azalat
									$yuzde = ($s_fiyat / 100) *  $op_commission;
									$s_fiyat = $s_fiyat - $yuzde;
								} else if($comission_type == 'sabitazalt'){
									// sabit olarak azalt
									$s_fiyat = $s_fiyat - $op_commission;
								}
							} else {
								// fiyat komisyon ayarlamaları
								if($comission_type == 'yuzde'){
									// yüzde olarak arttır
									$yuzde = ($s_fiyat / 100) * $commission;
									$s_fiyat = $s_fiyat + $yuzde;
								} else if($comission_type == 'sabit'){
									// sabit arttır
									$s_fiyat = $s_fiyat +  $commission;
								} else if($comission_type == 'yuzdeazalt'){
									// yüzde azalat
									$yuzde = ($s_fiyat / 100) *  $commission;
									$s_fiyat = $s_fiyat - $yuzde;
								} else if($comission_type == 'sabitazalt'){
									// sabit olarak azalt
									$s_fiyat = $s_fiyat - $commission;
								}
							}
						}
						
						foreach ($scnk_eslestirme->ocsenecek_deger as $key => $esles_tirme) {
							if(!strstr($key, 'sabit')){
								if($secenek_val['option_value_id'] == $esles_tirme){
									$ggop_degerid = $key;
									$ggop_degeradi = $scnk_eslestirme->ggsecenek_deger->{$key};
								}
							}
						}

						foreach ($scnk_eslestirme->ocsecenek as $seckey => $sec_esles_tirme) {
							if($sec_esles_tirme != 'sabit'){
								if($secenek['option_id'] == $sec_esles_tirme){
									$ggop_id = $seckey;
									$ggop_adi = $scnk_eslestirme->ggsecenek->{$seckey};
								}
							}
						}

						if($secenek_val['quantity'] > 0 and (!empty($ggop_id) or !empty($ggop_adi))){
							$secenek_deger[$secenek['name']][] = $ggop_id.'|'.$ggop_adi.'|'.$ggop_degerid.'|'.$ggop_degeradi.'|'.$secenk_degeri->row['name'].'|'.$secenek['name'].'|'.$s_fiyat.'|'.$secenek_val['quantity'].'|'.$secenek_val['option_value_id'];
						}
					}

					if(isset($secenek_deger[$secenek['name']])){
						$secenek_ar[] = $secenek_deger[$secenek['name']];
					}
				}

				$secenek_combine = $this->get_combinations($secenek_ar);
				$xml_variant .= "<variantGroups>";
				$ilk_secenek = explode('|', $secenek_combine[0][0]);
				$xml_variant .= '	<variantGroup nameId="'.$ilk_secenek[0].'" valueId="'.$ilk_secenek[2].'" alias="'.$ilk_secenek[1].':'.$ilk_secenek[3].'">';
				$xml_variant .= '		<variants>';
				foreach ($secenek_combine as $komkey => $komvalue) {
					$xml_variant .= '<variant variantId="'.$product_id.$komkey.'" operation="I">';
					$xml_variant .= ' <variantSpecs>';
					$orderNumber = 1;
					$orderquantity = 0;
					foreach ($komvalue as $kvalue){
						$secenek_dizi = explode('|', $kvalue);
						$orderquantity = $orderquantity + $secenek_dizi[7];
						$xml_variant .= '<variantSpec nameId="'.$secenek_dizi[0].'" name="'.$secenek_dizi[1].'" valueId="'.$secenek_dizi[2].'" specDataOrderNumber="'.$orderNumber.'" value="'.$secenek_dizi[3].'" orderNumber="'.$orderNumber.'" />';
						$orderNumber++;
					}
					if($orderquantity > 999){
						$orderquantity = 999;
					}

					$xml_variant .= '				</variantSpecs>';
					$xml_variant .= '				<quantity>'.$orderquantity.'</quantity>';
					$xml_variant .= '				<stockCode>'.$komkey.'_'.$product_id.'_'.$secenek_dizi[8].'</stockCode>';
					$xml_variant .= '<soldCount>0</soldCount>';
					$xml_variant .= '			</variant>';
				}
				$xml_variant .= '		</variants>';
				$xml_variant .= ' 		<photos>'.$specphoto.'</photos>';
				$xml_variant .= '	</variantGroup>';
				$xml_variant .= '</variantGroups>';
					


				$secenek_to_array  = array();
				foreach ($scnk_eslestirme->ggsecenek as $sta) {
					$secenek_to_array[] = $sta;
				}
				
				if(isset($attribute) and $attribute != ''){
					foreach ($attribute as $key => $value) {
						if($key != 'Renk'){
							if(is_array($value)){
								if($value[0] != '' and !in_array($key, $secenek_to_array)){
									$atrbute .='<spec name="'.$key.'" value="'.$value[0].'" type="Combo" required="true" />';
								}
							} else {
								if($value != '' and !in_array($key, $secenek_to_array)){
									$atrbute .='<spec name="'.$key.'" value="'.$value.'" type="Combo" required="true" />';
								}
							}
						}
					}
				}
			} else {
				if(isset($attribute) and $attribute != ''){
					foreach ($attribute as $key => $value) {
							if($key != 'Renk'){
								if(is_array($value)){
									if($value[0] != ''){
										$atrbute .='<spec name="'.$key.'" value="'.$value[0].'" type="Combo" required="true" />';
									}
								} else {
									if($value != ''){
										$atrbute .='<spec name="'.$key.'" value="'.$value.'" type="Combo" required="true" />';
									}
								}
							}
					}
				}
			}

			


			if($stocksifirla == 1){ $sec_quantity = 0; } else { $sec_quantity = $product_query->row["quantity"]; }
			if($sec_quantity > 999){
				$sec_quantity = 999;
			}
			
			$xml = '
				<product>
				    <categoryCode>'.$catid.'</categoryCode>
				    <storeCategoryId></storeCategoryId>
				    <title>'.htmlspecialchars((string)$title).'</title>
				    <subtitle></subtitle>
				    <specs>'.$atrbute.'</specs>
				    <photos>'.$photos.'</photos>
				    <pageTemplate>1</pageTemplate>
				    <description>'. htmlspecialchars((string)html_entity_decode($description, ENT_QUOTES, 'UTF-8'), ENT_QUOTES).'</description>
				    <startDate></startDate>
				    <catalogId></catalogId>
				    <catalogDetail></catalogDetail>
				    <catalogFilter></catalogFilter>
				    <format>S</format>
				    <startPrice></startPrice>
				    <buyNowPrice>'.(float)number_format($price, 2, '.', '').'</buyNowPrice>
				    <netEarning></netEarning>
				    <listingDays>'.$ship_listingday.'</listingDays>
				    <productCount>'.(int)$sec_quantity.'</productCount>
				    <cargoDetail>
				        <city>34</city>
				        <shippingPayment>'.$kargo_kod.'</shippingPayment>
				        <shippingWhere>country</shippingWhere>
				        <cargoCompanyDetails>
				         	'.$kargo_sirketleri.'
				        </cargoCompanyDetails>
				        <shippingTime>
				            <days>2-3days</days>
				            <beforeTime>02:15</beforeTime>
				        </shippingTime>
				    </cargoDetail>
				      '.$xml_variant.'
				    <affiliateOption>false</affiliateOption>
				    <boldOption>false</boldOption>
				    <catalogOption>false</catalogOption>
				    <vitrineOption>false</vitrineOption>
				</product>';

				if($secenekler){
					if($sec_quantity > 0){
						if($prdggquery->num_rows > 0){
							if($prdggquery->row['gg_id'] == "0" OR $prdggquery->row['gg_id'] == ''){
								$insert = $client->insertRetailProductWithNewCargoDetail($product_query->row['product_id'], $xml);
							} else {
								$insert = $client->updateRetailProductWithNewCargoDetail(null, $prdggquery->row['gg_id'], $xml);
							}
						} else {
							$insert = $client->insertRetailProductWithNewCargoDetail($product_query->row['product_id'], $xml);
						}
					} else {
						if($prdggquery->num_rows > 0){
							if($prdggquery->row['gg_id'] == 0 OR $prdggquery->row['gg_id'] == ''){
									/*
									$bitir = $client->finishEarly(array($prdggquery->row['gg_id']));
									$sil = $client->deleteProduct(array($prdggquery->row['gg_id']));
									*/
							}
						}
					}
				} else {
					if($sec_quantity > 0){
						if($prdggquery->num_rows > 0){
							if($prdggquery->row['gg_id'] == "0" OR $prdggquery->row['gg_id'] == ''){
								$insert = $client->insertProductWithNewCargoDetail($product_query->row['product_id'], $xml);
							} else {
								$insert = $client->updateProductWithNewCargoDetail(null, $prdggquery->row['gg_id'], $xml);
							}
						} else {
							$insert = $client->insertProductWithNewCargoDetail($product_query->row['product_id'], $xml);
						}
					} else {
						if($prdggquery->num_rows > 0){
							if($prdggquery->row['gg_id'] == 0 OR $prdggquery->row['gg_id'] == ''){
									/*
									$bitir = $client->finishEarly(array($prdggquery->row['gg_id']));
									$sil = $client->deleteProduct(array($prdggquery->row['gg_id']));
									*/
							}
						}
					}
				}

				if(isset($insert)){
				if($insert->ackCode == 'success'){
					if($prdggquery->num_rows > 0){
						$this->db->query("UPDATE ".DB_PREFIX."ggproduct SET latest_update = NOW(), gg_salestatus = '4', shop_id = '".$shop_id."', gg_id = '".$insert->productId."' WHERE product_id = '".$product_query->row['product_id']."'");
					} else {
						$this->db->query("INSERT INTO ".DB_PREFIX."ggproduct SET latest_update = NOW(), gg_salestatus = '4', shop_id = '".$shop_id."', product_id = '".$product_query->row['product_id']."', gg_id = '".$insert->productId."'");
					}
					$gonderildi[] = '1';
					$dosya = fopen ($dosya_adi , 'a') or die ("Dosya açılamadı!"); 
					$metin = date('d.m.Y H:i:s')." - ".$product_query->row["model"]." - ".$insert->result."\r\n";
					fputs($dosya , $metin); 
					$hesaplama = $client->calculatePriceForShoppingCart(array($insert->productId));
					if($hesaplama->ackCode == 'success'){
						if($hesaplama->payRequired == '1'){
							if($product_id == null){
								return array('status' => 1, 'product_id' => $product_query->row['product_id'], 'msg' => $insert->result.' - '.$hesaplama->message, 'next' => 'index.php?route=gittigidiyor/product/sendProduct&'.$token_link.'&page='.($page+1));
							} else {
								return array('status' => 1, 'product_id' => $product_query->row['product_id'], 'msg' => $insert->result.' - '.$hesaplama->message);
							}	
						} else {
							if($product_id == null){
								return array('status' => 1, 'product_id' => $product_query->row['product_id'], 'msg' => $insert->result.' - '.$hesaplama->message, 'next' => 'index.php?route=gittigidiyor/product/sendProduct&'.$token_link.'&page='.($page+1));
							} else {
								return array('status' => 1, 'product_id' => $product_query->row['product_id'], 'msg' => $insert->result.' - '.$hesaplama->message);
							}
						}
					} else {
						$gonderilemedi[] = '1';
						$dosya = fopen ($dosya_adi , 'a') or die ("Dosya açılamadı!"); 
						$metin = date('d.m.Y H:i:s')." - ".$product_query->row["model"]." - " . $insert->result.' fakat '.$hesaplama->error->message ."\r\n";
						fputs($dosya , $metin); 
						if($product_id == null){
							return array('status' => 0, 'product_id' => $product_query->row['product_id'], 'msg' => $metin, 'next' => 'index.php?route=gittigidiyor/product/sendProduct&'.$token_link.'&page='.($page+1));
						} else {
							return array('status' => 0, 'product_id' => $product_query->row['product_id'], 'msg' => $metin);
						}
					}
					fclose ($dosya);			
				} else {

					$gonderilemedi[] = '1';
					$dosya = fopen ($dosya_adi , 'a') or die ("Dosya açılamadı!"); 
					$metin = date('d.m.Y H:i:s')." - ".$product_query->row["model"]." - " . $insert->error->message ."\r\n"; 
					fputs($dosya , $metin); 
					fclose ($dosya);
					if($product_id == null){
						return array('status' => 0, 'product_id' => $product_query->row['product_id'], 'msg' => $metin, 'next' => 'index.php?route=gittigidiyor/product/sendProduct&'.$token_link.'&page='.($page+1));
					} else {
						return array('status' => 0, 'product_id' => $product_query->row['product_id'], 'msg' => $metin);
					}
				}
			} else {
				$gonderilemedi[] = '1';
				$dosya = fopen ($dosya_adi , 'a') or die ("Dosya açılamadı!"); 
				$metin = date('d.m.Y H:i:s')." - ".$product_query->row["model"]." - model numaralı ürün için işlem yapılamadı, lütfen yazılımcınıza danışın\r\n"; 
				fputs($dosya , $metin); 
				fclose ($dosya);
				if($product_id == null){
					return array('status' => 0, 'product_id' => $product_query->row['product_id'], 'msg' => $metin, 'next' => 'index.php?route=gittigidiyor/product/sendProduct&'.$token_link.'&page='.($page+1));
				} else {
					return array('status' => 0, 'product_id' => $product_query->row['product_id'], 'msg' => $metin);
				}
			}


		} else {
			// ürün yoksa
			return array('status' => 0, 'msg' => 'Ürün Aktarımı Tamamlandı');
		}
	}


	public function get_combinations($arrays) {

		if(count($arrays) > 1){
			$result = array(array());
			foreach ($arrays as $property => $property_values) {
				$tmp = array();
				foreach ($result as $result_item) {
					foreach ($property_values as $property_value) {
						$tmp[] = array_merge($result_item, array($property => $property_value));
					}
				}
				$result = $tmp;
			}
			return $result;
		} else {
			return $arrays;
		}
	}




	public function urunleriGuncelle($client, $shop_id, $product_id = null, $page, $stocksifirla = 0){
		if(VERSION < 3){
			$token = $this->session->data['token'];
			$token_link = 'token=' . $this->session->data['token'];
		} else {
			$token = $this->session->data['user_token'];
			$token_link = 'user_token=' . $this->session->data['user_token'];
		}	

		ini_set('max_execution_time', 0); //0=NOLIMIT
		ini_set('set_time_limit', 0); //0=NOLIMIT
		$dosya_adi = DIR_LOGS.'gglogs.txt';
		/*
			ürün öncelik sırası
			1. ürün eşleşmesi
			2. kategori eşleşmesi
			3. global eşleşme
		*/

		if($product_id == null){
			$product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE p.product_id IN (SELECT product_id FROM " . DB_PREFIX . "ggproduct WHERE product_id = p.product_id AND (gg_id != '' OR gg_id != '0')) ORDER BY p.product_id DESC LIMIT ".$page.",1");
		} else {
			$product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE p.product_id = '".$product_id."' AND p.product_id  IN (SELECT product_id FROM " . DB_PREFIX . "ggproduct WHERE product_id = p.product_id)");
		}
		
		if($product_query->num_rows > 0){
			// ürün varsa
			$shop_query = $this->db->query("SELECT * FROM ".DB_PREFIX."ggshops WHERE id = '".$shop_id."'");
			if(strlen($product_query->row['name']) > 65){
				$title =  mb_substr($product_query->row['name'], 0, 65, 'utf8');
			} else {
				$title = $product_query->row['name'];
			}
			$price = $product_query->row['price'];
			$subtitle = $shop_query->row['default_subtitle'];
			$comission_type = $shop_query->row['difference_type'];
			$commission = $shop_query->row['difference_value'];
			$cargo_company = $shop_query->row['cargo_company'];
			$shipping_condition = $shop_query->row['shipping_condition'];
			$shipping_price = $shop_query->row['shipping_price'];
			$shipping_aprice = $shop_query->row['shipping_aprice'];
			$description = str_replace('{aciklama}', $product_query->row['description'], $shop_query->row['description']);

			// kategori eşleştirmesini kontrol edelim
			$prodtocat = $this->db->query("SELECT * FROM ".DB_PREFIX."product_to_category WHERE product_id = '".$product_query->row['product_id']."' ORDER BY category_id DESC");
			if($prodtocat->num_rows > 0){
				foreach ($prodtocat->rows as $occat) {
					$category_query = $this->db->query("SELECT * FROM ".DB_PREFIX."category WHERE category_id = '".$occat['category_id']."' AND gg_id != '0'");
					if($category_query->row){
						// kategori ürün gönderim durumu
						if(isset($category_query->row['gcatstatus'])){
							if($category_query->row['gcatstatus'] == 1){
								$status = 1;
							} else {
								$status = 0;
							}
						}
						if(isset($category_query->row['gg_id'])){
							if($category_query->row['gg_id'] != "0"){
								$catid = $category_query->row['gg_id'];
							}
						}

						if(isset($category_query->row['ggcomission'])){
							if($category_query->row['ggcomission'] != 0){
								$commission = $category_query->row['ggcomission'];
							}
						}
						if(isset($category_query->row['ggattr'])){
							if($category_query->row['ggattr'] != ''){
								$attribute = json_decode($category_query->row['ggattr']);
							}
						}
					}
				}
			}
			

			// birebir eşleştirme
			$prdggquery = $this->db->query("SELECT * FROM " . DB_PREFIX . "ggproduct WHERE product_id = '".$product_query->row['product_id']."' AND shop_id = '". $shop_id."'");	
			if($prdggquery->num_rows > 0){
				if(isset($prdggquery->row['gg_salestatus']) and $prdggquery->row['gg_salestatus'] != '0'){
					if($stocksifirla == 0){
						if($prdggquery->row['gg_salestatus'] == 1){
							$status = 1;
						} else {
							$status = 0;
						}
					}
				}
				if(isset($prdggquery->row['gg_title']) and $prdggquery->row['gg_title'] != ''){
					$title = $prdggquery->row['gg_title'];
				}
				
				if(isset($prdggquery->row['gg_price']) and $prdggquery->row['gg_price'] != '0'){
					$price = $prdggquery->row['gg_price'];
				}
				if(isset($prdggquery->row['gg_description']) and $prdggquery->row['gg_description'] != ''){
					$description = $prdggquery->row['gg_description'];
				}
				if(isset($prdggquery->row['gg_subtitle']) and $prdggquery->row['gg_subtitle'] != ''){
					$subtitle = $prdggquery->row['gg_subtitle'];
				}
				if(isset($prdggquery->row['gg_catid']) and $prdggquery->row['gg_catid'] != '' and $prdggquery->row['gg_catid'] != '0'){
					$catid = $prdggquery->row['gg_catid'];
				}
				
				if(isset($prdggquery->row['gg_commission']) and $prdggquery->row['gg_commission'] != '0'){
					$commission = $prdggquery->row['gg_commission'];
				}
				if(isset($prdggquery->row['gg_attribute']) and $prdggquery->row['gg_attribute'] != ''){
					$attribute = json_decode($prdggquery->row['gg_attribute']);
				}
			}

			if($comission_type == 'yuzde'){
				// yüzde olarak arttır
				$yuzde = ($price / 100) * $commission;
				$price = $price + $yuzde;
			} else if($comission_type == 'sabit'){
				// sabit arttır
				$price = $price +  $commission;
			} else if($comission_type == 'yuzdeazalt'){
				// yüzde azalat
				$yuzde = ($price / 100) *  $commission;
				$price = $price - $yuzde;
			} else if($comission_type == 'sabitazalt'){
				// sabit olarak azalt
				$price = $price -  $commission;
			}

			if($shop_query->row['currency'] != 'TRY'){
				$connect_web = simplexml_load_file('http://www.tcmb.gov.tr/kurlar/today.xml');
				$usd = $connect_web->Currency[0]->BanknoteSelling;
				$euro = $connect_web->Currency[3]->BanknoteSelling;
				if($shop_query->row['currency'] == 'USD'){
					$price = $price * $usd;
				} else {
					$price = $price * $euro;
				}
			}
			
			// stok sıfırlıyorsa status durumunu daima 1 yapsın
			if($stocksifirla == 1 or $stocksifirla == 2){ $status = 1; }
			$atrbute = '';
			if(isset($attribute) and $attribute != ''){
				foreach ($attribute as $key => $value) {
					if(is_array($value)){
						if($value[0] != ''){
							$atrbute .='<spec name="'.$key.'" value="'.$value[0].'" type="Combo" required="true" />';
						}
					} else {
						if($value != ''){
							$atrbute .='<spec name="'.$key.'" value="'.$value.'" type="Combo" required="true" />';
						}
					}
				}
			}
			$photos = '';
			$photos .= '<photo photoId="0">
				            <url>'.str_replace(' ', '%20', 'https://'.$_SERVER['SERVER_NAME'].'/image/'.$product_query->row["image"]).'</url>
				            <base64></base64>
				        </photo>';
			$additional_image = $this->db->query("SELECT * FROM  " . DB_PREFIX . "product_image WHERE product_id = '".$product_query->row['product_id']."'");
			foreach ($additional_image->rows as $key => $ekresim) {
				$photos .= '<photo photoId="'.($key+1).'">
					            <url>'.str_replace(' ', '%20', 'https://'.$_SERVER['SERVER_NAME'].'image/'.$ekresim["image"]).'</url>
					            <base64></base64>
					        </photo>';
				if(($key+1) == 7){ break; }
			}
			// $shipping_condition
			$kargolar = explode(',', $cargo_company);
			$kargo_sirketleri  = '';
			foreach ($kargolar as $kargo){
				$kargo_sirketleri .= '
				<cargoCompanyDetail>
				               <name>'.ltrim(rtrim($kargo)).'</name>
				               <cityPrice></cityPrice>
               					<countryPrice></countryPrice>
				            </cargoCompanyDetail>';
			}
			$kargo_kod  = 'B'; // varsayılan olarak buyer (alıcı öder)
			if($shipping_condition == 1){
				// x TL ücretsiz
				if($price > $shipping_price){
					$kargo_kod  = 'S';
				} else {
					$kargo_kod  = 'B';
				}
			} else if($shipping_condition == 2){
				// kargo gereksiz seçili ürünlerde
				if($product_query->row['shipping'] == 0){
					$kargo_kod  = 'S';
				} else {
					$kargo_kod  = 'B';
				}
			} else if($shipping_condition == 3){
				// fiyata dahil
				$kargo_kod  = 'S';
				$price = $price + $shipping_aprice;
			}

			if($shop_query->row['tax_option'] == 1){
				$price = $this->vergihesapla($price, $product_query->row['tax_class_id'], true);
			}

			if($stocksifirla == 1){ $sec_quantity = 0; } else { $sec_quantity = $product_query->row["quantity"]; }
				$xml = '
				   <product>
				      <categoryCode>'.$catid.'</categoryCode>
				      <storeCategoryId></storeCategoryId>
				      <title>'.htmlspecialchars((string)$title).'</title>
				      <subtitle></subtitle>
				      <specs>'.$atrbute.'</specs>
				      <photos>'.$photos.'</photos>
				      <pageTemplate>1</pageTemplate>
				      <description>'. htmlspecialchars((string)html_entity_decode($description, ENT_QUOTES, 'UTF-8'), ENT_QUOTES).'</description>
				      <startDate></startDate>
				      <catalogId></catalogId>
				      <catalogDetail></catalogDetail>
				      <catalogFilter></catalogFilter>
				      <format>S</format>
				      <startPrice></startPrice>
				      <buyNowPrice>'.(string)number_format($price, 2, '.', '').'</buyNowPrice>
				      <netEarning></netEarning>
				      <listingDays>60</listingDays>
				      <productCount>'.(int)$sec_quantity.'</productCount>
				      <cargoDetail>
				         <city>34</city>
				         <shippingPayment>'.$kargo_kod.'</shippingPayment>
				         <shippingWhere>country</shippingWhere>
				         <cargoCompanyDetails>
				         	'.$kargo_sirketleri.'
				             </cargoCompanyDetails>
				             <shippingTime>
				            <days>today</days>
				            <beforeTime>02:15</beforeTime>
				             </shippingTime>
				      </cargoDetail>
				      <affiliateOption>false</affiliateOption>
				      <boldOption>false</boldOption>
				      <catalogOption>false</catalogOption>
				      <vitrineOption>false</vitrineOption>
				   </product>';
				
				$insert = $client->updateProductWithNewCargoDetail(null, $prdggquery->row['gg_id'], $xml);
				if(isset($insert)){
				if($insert->ackCode == 'success'){
					if($prdggquery->num_rows > 0){
						$this->db->query("UPDATE ".DB_PREFIX."ggproduct SET latest_update = NOW(), gg_salestatus = '1', shop_id = '".$shop_id."', gg_id = '".$insert->productId."' WHERE product_id = '".$product_query->row['product_id']."'");
					} else {
						$this->db->query("INSERT INTO ".DB_PREFIX."ggproduct SET latest_update = NOW(), gg_salestatus = '1', shop_id = '".$shop_id."', product_id = '".$product_query->row['product_id']."', gg_id = '".$insert->productId."'");
					}
					$gonderildi[] = '1';
					$dosya = fopen ($dosya_adi , 'a') or die ("Dosya açılamadı!"); 
					$metin = date('d.m.Y H:i:s')." - ".$product_query->row["model"]." - ".$insert->result."\r\n";
					fputs($dosya , $metin); 
					$hesaplama = $client->calculatePriceForShoppingCart(array($insert->productId));
					if($hesaplama->ackCode == 'success'){
						if($hesaplama->payRequired == '1'){
							if($product_id == null){
								return array('status' => 1, 'product_id' => $product_query->row['product_id'], 'msg' => '<div class="alert alert-success" style="padding:5px !important; margin-bottom:5px;">'.$insert->result.' - '.$hesaplama->message.'</div>', 'next' => 'index.php?route=gittigidiyor/product/tumunuguncelle&'.$token_link.'&page='.($page+1));
							} else {
								return array('status' => 1, 'product_id' => $product_query->row['product_id'], 'msg' => '<div class="alert alert-success" style="padding:5px !important; margin-bottom:5px;">'.$insert->result.' - '.$hesaplama->message.'</div>');
							}	
						} else {
							if($product_id == null){
								return array('status' => 1, 'product_id' => $product_query->row['product_id'], 'msg' => '<div class="alert alert-success" style="padding:5px !important; margin-bottom:5px;">'.$insert->result.' - '.$hesaplama->message.'</div>', 'next' => 'index.php?route=gittigidiyor/product/tumunuguncelle&'.$token_link.'&page='.($page+1));
							} else {
								return array('status' => 1, 'product_id' => $product_query->row['product_id'], 'msg' => '<div class="alert alert-success" style="padding:5px !important; margin-bottom:5px;">'.$insert->result.' - '.$hesaplama->message.'</div>');
							}
						}
					} else {
						$gonderilemedi[] = '1';
						$dosya = fopen ($dosya_adi , 'a') or die ("Dosya açılamadı!"); 
						$metin = date('d.m.Y H:i:s')." - ".$product_query->row["model"]." - " . $insert->result.' fakat '.$hesaplama->error->message ."\r\n";
						fputs($dosya , $metin); 
						if($product_id == null){
							return array('status' => 0, 'product_id' => $product_query->row['product_id'], 'msg' => '<div class="alert alert-danger" style="padding:5px !important; margin-bottom:5px;">'.$metin.'</div>', 'next' => 'index.php?route=gittigidiyor/product/tumunuguncelle&'.$token_link.'&page='.($page+1));
						} else {
							return array('status' => 0, 'product_id' => $product_query->row['product_id'], 'msg' => '<div class="alert alert-danger" style="padding:5px !important; margin-bottom:5px;">'.$metin.'</div>');
						}
					}
					fclose ($dosya);			
				} else {

					$gonderilemedi[] = '1';
					$dosya = fopen ($dosya_adi , 'a') or die ("Dosya açılamadı!"); 
					$metin = date('d.m.Y H:i:s')." - ".$product_query->row["model"]." - " . $insert->error->message ."\r\n"; 
					fputs($dosya , $metin); 
					fclose ($dosya);
					if($product_id == null){
						return array('status' => 0, 'product_id' => $product_query->row['product_id'], 'msg' => '<div class="alert alert-dabger" style="padding:5px !important; margin-bottom:5px;">'.$metin.'</div>', 'next' => 'index.php?route=gittigidiyor/product/tumunuguncelle&'.$token_link.'&page='.($page+1));
					} else {
						return array('status' => 0, 'product_id' => $product_query->row['product_id'], 'msg' => '<div class="alert alert-danger" style="padding:5px !important; margin-bottom:5px;">'.$metin.'</div>');
					}
				}
			} else {
				$gonderilemedi[] = '1';
				$dosya = fopen ($dosya_adi , 'a') or die ("Dosya açılamadı!"); 
				$metin = date('d.m.Y H:i:s')." - ".$product_query->row["model"]." - model numaralı ürün için işlem yapılamadı, lütfen yazılımcınıza danışın\r\n"; 
				fputs($dosya , $metin); 
				fclose ($dosya);
				if($product_id == null){
					return array('status' => 0, 'product_id' => $product_query->row['product_id'], 'msg' => '<div class="alert alert-success" style="padding:5px !important; margin-bottom:5px;">'.$metin.'</div>', 'next' => 'index.php?route=gittigidiyor/product/tumunuguncelle&'.$token_link.'&page='.($page+1));
				} else {
					return array('status' => 0, 'product_id' => $product_query->row['product_id'], 'msg' => '<div class="alert alert-success" style="padding:5px !important; margin-bottom:5px;">'.$metin.'</div>');
				}
			}
		} else {
			// ürün yoksa
			return array('status' => 0, 'msg' => '<div class="alert alert-success" style="padding:5px !important; margin-bottom:5px;">Ürün Aktarımı Tamamlandı</div>');
		}
	}


	/* 
		mağaza ürün koduna göre silme fonksiyonu
	*/
	public function DeleteProductBySellerCode($client, $product_id = null, $page = null, $shop_id){
		if(VERSION < 3){
			$token = $this->session->data['token'];
			$token_link = 'token=' . $this->session->data['token'];
		} else {
			$token = $this->session->data['user_token'];
			$token_link = 'user_token=' . $this->session->data['user_token'];
		}	

		/*
			ürün durumları
			1 - gittigidiyora gitsin
			2 - gittigidiyora gitmesin
		*/
		if($product_id == null and $page != null){
			$product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LIMIT ".$page.",1");
		} else {
			$product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE p.product_id = '".$product_id."'");
		}

		if($product_query->num_rows > 0){
			$prdggquery = $this->db->query("SELECT * FROM ".DB_PREFIX."ggproduct WHERE product_id = '".$product_query->row['product_id']."'");
			if(isset($prdggquery->row['gg_id']) and !empty($prdggquery->row['gg_id'])){
				//$bitir = $client->finishEarly(array($prdggquery->row['gg_id']));
				$sonuc = $client->deleteProducts(array($prdggquery->row['gg_id']));
				if($sonuc->ackCode == 'success'){
					$this->db->query("UPDATE ".DB_PREFIX."ggproduct SET gg_id = '' WHERE product_id = '".(int)$product_id."' AND shop_id = '".$shop_id."'");
					if($product_id != null){
						return array('status' => 1, 'msg' => '<div class="alert alert-success" style="padding:5px !important; margin-bottom:5px;">'.$product_query->row['name'].' Başarıyla Silindi</div>');
					} else{
						return array('status' => 1, 'msg' => '<div class="alert alert-success" style="padding:5px !important; margin-bottom:5px;">'.$product_query->row['name'].' Başarıyla Silindi</div>', 'next' => 'index.php?route=gittigidiyor/product/delProduct&'.$token_link.'&page='.($page+1));
					}
				} else {
					if($product_id != null){
						return array('status' => 0, 'msg' => '<div class="alert alert-success" style="padding:5px !important; margin-bottom:5px;">'.$product_query->row['name'].' Silinemedi. Hata :  '.$sonuc->error->message.'</div>');
					} else {
						return array('status' => 0, 'msg' => '<div class="alert alert-success" style="padding:5px !important; margin-bottom:5px;">'.$product_query->row['name'].' Silinemedi. Hata :  '.$sonuc->error->message.'</div>', 'next' => 'index.php?route=gittigidiyor/product/delProduct&'.$token_link.'&page='.($page+1));
					}
				}
			} else {
				if($product_id != null){
					return array('status' => 0, 'msg' => '<div class="alert alert-success" style="padding:5px !important; margin-bottom:5px;">'.$product_query->row['name'].' gittigidiyora gönderilmediği için silinemedi.</div>');
				} else {
					return array('status' => 0, 'msg' => $product_query->row['name'].' gittigidiyora gönderilmediği için silinemedi.</div>', 'next' => 'index.php?route=gittigidiyor/product/delProduct&'.$token_link.'&page='.($page+1));
				}
			}
		} else {
			return array('status' => 0, 'msg' => '<div class="alert alert-danger" style="padding:5px !important; margin-bottom:5px;">Ürün Bulunamadı</div>');
		}
	}


	/* ürün satışını erken bitirmek için kullanıyoruz */
	public function satiSonlandir($client, $product_id = null, $page = null, $shop_id){
		/*
			yeni salestatüs bilgileri
			1 - aktif satışta
			2 - listelenmeye hazır
		*/
		if(VERSION < 3){
			$token = $this->session->data['token'];
			$token_link = 'token=' . $this->session->data['token'];
		} else {
			$token = $this->session->data['user_token'];
			$token_link = 'user_token=' . $this->session->data['user_token'];
		}		
		if($product_id == null and $page != null){
			$product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LIMIT ".$page.",1");
		} else {
			$product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE p.product_id = '".$product_id."'");
		}
		if($product_query->num_rows > 0){
			$prdggquery = $this->db->query("SELECT * FROM ".DB_PREFIX."ggproduct WHERE product_id = '".$product_query->row['product_id']."'");
			if(isset($prdggquery->row['gg_id']) and !empty($prdggquery->row['gg_id'])){
				$sonuc = $client->finishEarly(array($prdggquery->row['gg_id']));
				if($sonuc->ackCode == 'success'){
					if($product_id != null){
						return array('status' => 1, 'msg' => '<div class="alert alert-success" style="padding:5px !important; margin-bottom:5px;">'.$product_query->row['name'].' Başarıyla Silindi</div>');
					} else{
						return array('status' => 1, 'msg' => '<div class="alert alert-danger" style="padding:5px !important; margin-bottom:5px;">'.$product_query->row['name'].' Başarıyla Silindi</div>', 'next' => 'index.php?route=gittigidiyor/product/satiSonlandir&'.$token_link.'&page='.($page+1));
					}
				} else {
					if($product_id != null){
						return array('status' => 0, 'msg' => '<div class="alert alert-danger" style="padding:5px !important; margin-bottom:5px;">'.$product_query->row['name'].' Silinemedi. Hata :  '.$sonuc->error->message.'</div>');
					} else {
						return array('status' => 0, 'msg' => '<div class="alert alert-danger" style="padding:5px !important; margin-bottom:5px;">'.$product_query->row['name'].' Silinemedi. Hata :  '.$sonuc->error->message.'</div>', 'next' => 'index.php?route=gittigidiyor/product/satiSonlandir&'.$token_link.'&page='.($page+1));
					}
				}
			} else {
				if($product_id != null){
					return array('status' => 0, 'msg' => '<div class="alert alert-danger" style="padding:5px !important; margin-bottom:5px;">'.$product_query->row['name'].' gittigidiyora gönderilmediği için silinemedi.</div>');
				} else {
					return array('status' => 0, 'msg' => '<div class="alert alert-danger" style="padding:5px !important; margin-bottom:5px;">'.$product_query->row['name'].' gittigidiyora gönderilmediği için silinemedi.</div>', 'next' => 'index.php?route=gittigidiyor/product/satiSonlandir&'.$token_link.'&page='.($page+1));
				}
			}
		} else {
			return array('status' => 0, 'msg' => '<div class="alert alert-danger" style="padding:5px !important; margin-bottom:5px;">Ürün Bulunamadı</div>');
		}
	}


	
	/*
	 	opencart ürün sayısını getiriyoruz
	*/
	public function getOptotalproduct($data = array()){
		$sql = "SELECT COUNT(DISTINCT p.product_id) AS total FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)";
		$sql .= " WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "'";
		
		if (isset($data['filter_ggstatus']) and $data['filter_ggstatus'] == 'gonderilmemis') {
			$sql .= " AND (p.product_id  NOT IN (SELECT product_id FROM " . DB_PREFIX . "ggproduct WHERE product_id = p.product_id) OR p.product_id IN (SELECT product_id FROM " . DB_PREFIX . "ggproduct WHERE product_id = p.product_id AND gg_id = ''))";
		}
		
		if (!empty($data['filter_category_id'])) {
             $sql .= " AND p.product_id IN (SELECT product_id FROM " . DB_PREFIX . "product_to_category WHERE category_id = " . intVal($data['filter_category_id']) . ")";
         }
        if(!empty($data['filter_eslesme'])){
            if($data['filter_eslesme'] == 1){
                // birebir eşleştirilenler
                $sql .= " AND p.product_id IN (SELECT product_id FROM " . DB_PREFIX . "ggproduct WHERE shop_id = '".$data['shop_id']."' AND gg_catid != '')";
            } else if($data['filter_eslesme'] == 3){
                // Birebir eşleştirilmemişler
                $sql .= " AND p.product_id NOT IN (SELECT product_id FROM " . DB_PREFIX . "ggproduct WHERE shop_id = '".$data['shop_id']."' AND  gg_catid != '')";
            }
        }
        if (!empty($data['filter_manufacturer_id'])) {
            $sql .= " AND p.manufacturer_id = " . intVal($data['filter_manufacturer_id']);
        }
		if (!empty($data['filter_name'])) {
			$sql .= " AND pd.name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
		}
		if (!empty($data['filter_model'])) {
			$sql .= " AND p.model LIKE '" . $this->db->escape($data['filter_model']) . "%'";
		}
		if (isset($data['filter_price']) && !is_null($data['filter_price'])) {
			$sql .= " AND p.price LIKE '" . $this->db->escape($data['filter_price']) . "%'";
		}
		if (isset($data['filter_quantity']) && !is_null($data['filter_quantity'])) {
			$sql .= " AND p.quantity = '" . (int)$data['filter_quantity'] . "'";
		}
		if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
			$sql .= " AND p.status = '" . (int)$data['filter_status'] . "'";
		}
		$query = $this->db->query($sql);
		return $query->row['total'];
	}


	/* 
		opencart ürünleri getiriyorz

	*/
	public function getOpproducts($data=array()){
		$sql = "SELECT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

		if (isset($data['filter_ggstatus']) and $data['filter_ggstatus'] == 'gonderilmemis') {
			$sql .= " AND (p.product_id  NOT IN (SELECT product_id FROM " . DB_PREFIX . "ggproduct WHERE product_id = p.product_id) OR p.product_id IN (SELECT product_id FROM " . DB_PREFIX . "ggproduct WHERE product_id = p.product_id AND gg_id = ''))";
		}
		if (!empty($data['filter_category_id'])) {
             $sql .= " AND p.product_id IN (SELECT product_id FROM " . DB_PREFIX . "product_to_category WHERE category_id = " . intVal($data['filter_category_id']) . ")";
         }
        if(!empty($data['filter_eslesme'])){
            if($data['filter_eslesme'] == 1){
                // birebir eşleştirilenler
                $sql .= " AND p.product_id IN (SELECT product_id FROM " . DB_PREFIX . "ggproduct WHERE shop_id = '".$data['shop_id']."' AND gg_catid != '')";
            } else if($data['filter_eslesme'] == 3){
                // Birebir eşleştirilmemişler
                $sql .= " AND p.product_id NOT IN (SELECT product_id FROM " . DB_PREFIX . "ggproduct WHERE shop_id = '".$data['shop_id']."' AND gg_catid != '')";
            }
        }
        if (!empty($data['filter_manufacturer_id'])) {
            $sql .= " AND p.manufacturer_id = " . intVal($data['filter_manufacturer_id']);
        }
		if (!empty($data['filter_name'])) {
			$sql .= " AND pd.name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
		}
		if (!empty($data['filter_model'])) {
			$sql .= " AND p.model LIKE '" . $this->db->escape($data['filter_model']) . "%'";
		}
		if (!empty($data['filter_sku'])) {
			$sql .= " AND p.sku = '" . $this->db->escape($data['filter_sku']) . "'";
		}
		
		if (isset($data['filter_price']) && !is_null($data['filter_price'])) {
			$sql .= " AND p.price LIKE '" . $this->db->escape($data['filter_price']) . "%'";
		}
		if (isset($data['filter_quantity']) && !is_null($data['filter_quantity'])) {
			$sql .= " AND p.quantity = '" . (int)$data['filter_quantity'] . "'";
		}
		if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
			$sql .= " AND p.status = '" . (int)$data['filter_status'] . "'";
		}
		$sql .= " GROUP BY p.product_id";
		$sort_data = array(
			'pd.name',
			'p.model',
			'p.price',
			'p.quantity',
			'p.status',
			'p.sort_order'
		);
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY p.product_id";
		}
		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		$query = $this->db->query($sql);
		return $query->rows;
	}

	/*
	 	id numarası gönderilen bir ürünün seçenklerini getiriyoruz

	*/
	public function getProductOptions($product_id) {
		$product_option_data = array();
		$product_option_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "product_option` po LEFT JOIN `" . DB_PREFIX . "option` o ON (po.option_id = o.option_id) LEFT JOIN `" . DB_PREFIX . "option_description` od ON (o.option_id = od.option_id) WHERE po.product_id = '" . (int)$product_id . "' AND od.language_id = '" . (int)$this->config->get('config_language_id') . "'");
		foreach ($product_option_query->rows as $product_option) {
			$product_option_value_data = array();
			$product_option_value_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_option_value WHERE product_option_id = '" . (int)$product_option['product_option_id'] . "'");
			foreach ($product_option_value_query->rows as $product_option_value) {
				$product_option_value_data[] = array(
					'product_option_value_id' => $product_option_value['product_option_value_id'],
					'option_value_id'         => $product_option_value['option_value_id'],
					'quantity'                => $product_option_value['quantity'],
					'subtract'                => $product_option_value['subtract'],
					'price'                   => $product_option_value['price'],
					'price_prefix'            => $product_option_value['price_prefix'],
					'points'                  => $product_option_value['points'],
					'points_prefix'           => $product_option_value['points_prefix'],
					'weight'                  => $product_option_value['weight'],
					'weight_prefix'           => $product_option_value['weight_prefix']
				);
			}
			$product_option_data[] = array(
				'product_option_id'    => $product_option['product_option_id'],
				'product_option_value' => $product_option_value_data,
				'option_id'            => $product_option['option_id'],
				'name'                 => $product_option['name'],
				'type'                 => $product_option['type'],
				'value'                => $product_option['value'],
				'required'             => $product_option['required']
			);
		}
		return $product_option_data;
	}
}
?>