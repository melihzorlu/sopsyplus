<?php
/*
	kategori işlemleri için model dosyasıdır.
	* kategori indirme
	* kategori eşleştirme
	* kategori özelliği indirme
	* kategori özelliği eşleştirme gibi işlemler bu alandan yapılır.

	kullanılan tablolar
	".db_prefix."ggcategories - gg kategorilerinin bulunduğu tablodur.
	".db_prefix."ggcategory_attributes - gg kategori özelliklerinin bulunduğu tablodur.
	".db_prefix."ggcategory_attribute_values - gg kategori özellik değerlerinin bulunduğu tablodur.
*/
class ModelGittigidiyorCategory extends Model {
	/*
		Gittigidiyor ana kategorileri indir
	*/
	public function gettoplvl($client){
		$a = $client->getParentCategories(false, true, false);
		return $a->categories->category;
	}

	public function exitsnggcategory($code){
		$check = $this->db->query("SELECT category_code FROM ".DB_PREFIX."ggcategories WHERE category_code = '".$this->db->escape($code)."'");
		if($check->num_rows > 0){
			return true;
		} else {
			return false;
		}
	}

	public function ggcategoryinsert($data, $parent_id = 0){
		$this->db->query("INSERT INTO ".DB_PREFIX."ggcategories SET name = '".$this->db->escape($data['name'])."', category_code = '".$this->db->escape($data['code'])."'");
	}

	public function checkVariants($client){
		$categories = $client->getCategoriesHavingVariantSpecs(array('lang' => 'tr'));
		foreach ($categories->categories->category as $vrcat) {
			$this->db->query("UPDATE ".DB_PREFIX."ggcategories SET variant = 1 WHERE category_code = '".$vrcat->categoryCode."'");
		}
		return array('status' => 1, 'msg' => 'Seçenek İndirme Tamamlandı');
	}

	
	/* 
		gittigidiyor alt kategorilerini indiriyoruz, id null gelirse hepsi
	*/
	public function download_parent($client, $page, $parentid){
		if(VERSION < 3){
			$token = $this->session->data['token'];
			$token_link = 'token=' . $this->session->data['token'];
		} else {
			$token = $this->session->data['user_token'];
			$token_link = 'user_token=' . $this->session->data['user_token'];
		}	
			$a = $client->getSubCategories($parentid, false, true, false);
			$a_info = $client->getCategory($parentid, false);
			$ana = $a_info->categories->category->categoryName;
			$altkategori = array();
			if($a->categoryCount > 1){
				foreach ($a->categories->category as $av){
					if($av->deepest == 1){
						if($this->exitsnggcategory($av->categoryCode) == false){
							$data = array('code' => $av->categoryCode, 'name' =>  $ana.' > '.$av->categoryName);
							$altkategori[] = $this->ggcategoryinsert($data);
						}
					}
					$b = $client->getSubCategories($av->categoryCode, false, true, false);
					if($b->categoryCount > 1){
						foreach ($b->categories->category as $bv) {
							if(isset($bv->deepest) and $bv->deepest == 1){
								if($this->exitsnggcategory($bv->categoryCode) == false){
									$data = array('code' => $bv->categoryCode, 'name' =>  $ana.' > '.$av->categoryName.' > '.$bv->categoryName);
									$altkategori[] = $this->ggcategoryinsert($data);
								}
							}
							$c = $client->getSubCategories($bv->categoryCode, false, true, false);
							if($c->categoryCount > 1){
								foreach ($c->categories->category as $cv) {
									if(isset($cv->deepest) and $cv->deepest == 1){
										if($this->exitsnggcategory($cv->categoryCode) == false){
											$data = array('code' => $cv->categoryCode, 'name' =>  $ana.' > '.$av->categoryName.' > '.$bv->categoryName.' > '.$cv->categoryName);
											$altkategori[] = $this->ggcategoryinsert($data);
										}
									}
									$d = $client->getSubCategories($cv->categoryCode, false, true, false);
									if($d->categoryCount > 1){
										foreach ($d->categories->category as $dv) {
											if(isset($dv->deepest) and $dv->deepest == 1){
												if($this->exitsnggcategory($dv->categoryCode) == false){
													$data = array('code' => $dv->categoryCode, 'name' =>  $ana.' > '.$av->categoryName.' > '.$bv->categoryName.' > '.$cv->categoryName.' > '.$dv->categoryName);
													$altkategori[] = $this->ggcategoryinsert($data);
												}
											}
											$e = $client->getSubCategories($dv->categoryCode, false, true, false);
											if($e->categoryCount > 1){
												foreach ($e->categories->category as $ev) {
													if(isset($ev->deepest) and $ev->deepest == 1){
														if($this->exitsnggcategory($ev->categoryCode) == false){
															$data = array('code' => $ev->categoryCode, 'name' =>  $ana.' > '.$av->categoryName.' > '.$bv->categoryName.' > '.$cv->categoryName.' > '.$dv->categoryName.' > '.$ev->categoryName);
															$altkategori[] = $this->ggcategoryinsert($data);
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		
			return array('status' => 1, 'msg' => $ana.' Ait '.count($altkategori).' Adet Alt Kategori İndirildi', 'next' => 'index.php?route=gittigidiyor/category/download_parent&'.$token_link.'&page='.($page+1));
	}

	/*
		opencart kategorilerini alıyoruz, bize özel değişiklikler olduğu için
		orjinal model dosyasından çekmiyoruz
	*/

	// total kategorimiz
	public function getTotalopcategories($data){
		$sql = "SELECT c1.gg_id, c1.ggcomission, c1.gcatstatus, cp.category_id AS category_id, GROUP_CONCAT(cd1.name ORDER BY cp.level SEPARATOR '&nbsp;&nbsp;&gt;&nbsp;&nbsp;') AS name, c1.parent_id, c1.sort_order FROM " . DB_PREFIX . "category_path cp LEFT JOIN " . DB_PREFIX . "category c1 ON (cp.category_id = c1.category_id) LEFT JOIN " . DB_PREFIX . "category c2 ON (cp.path_id = c2.category_id) LEFT JOIN " . DB_PREFIX . "category_description cd1 ON (cp.path_id = cd1.category_id) LEFT JOIN " . DB_PREFIX . "category_description cd2 ON (cp.category_id = cd2.category_id) WHERE cd1.language_id = '" . (int)$this->config->get('config_language_id') . "' AND cd2.language_id = '" . (int)$this->config->get('config_language_id') . "'";

		if (!empty($data['filter_name'])) {
			$sql .= " AND cd2.name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (isset($data['filter_status']) and $data['filter_status'] != '') {
			$sql .= " AND c2.gcatstatus = '" . $data['filter_status'] . "'";
		}

		if (isset($data['filter_eslesme']) and $data['filter_eslesme'] != '') {
			if($data['filter_eslesme'] == 1){
				$sql .= " AND c1.gg_id != '0'";
			} else {
				$sql .= " AND c1.gg_id = '0'";
			}
		}

		if (isset($data['filter_komisyon']) and $data['filter_komisyon'] != '') {
			if($data['filter_komisyon'] == 1){
				$sql .= " AND c1.ggcomission != 0";
			} else {
				$sql .= " AND c1.ggcomission = 0";
			}
		}

		if (!empty($data['filter_category_id'])) {
			$sql .= " AND cp.category_id LIKE '" . $this->db->escape($data['filter_category_id']) . "%'";
		}

		$sql .= " GROUP BY cp.category_id";

		$sort_data = array(
			'name',
			'sort_order'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY sort_order";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

	

		$query = $this->db->query($sql);

		return $query->num_rows;
	}

	/* n11 kategori adını getir */
	public function getggnamebyid($gg_id){
		if(isset($gg_id) and !empty($gg_id) and $gg_id != '0'){
			$c_query = $this->db->query("SELECT * FROM ".DB_PREFIX."ggcategories WHERE category_code = '".$gg_id."'");
			if($c_query->num_rows > 0){
				return $c_query->row['name'];
			} else {
				return '- yok -';
			}
			
		} else {
			return false;
		}
	}

	/*	
		opencart kategorileri çekiyoruz
	*/

	public function getOpcategories($data = array()){
		$sql = "SELECT c1.gg_id, c1.ggcomission, c1.gcatstatus, cp.category_id AS category_id, GROUP_CONCAT(cd1.name ORDER BY cp.level SEPARATOR '&nbsp;&nbsp;&gt;&nbsp;&nbsp;') AS name, c1.parent_id, c1.sort_order FROM " . DB_PREFIX . "category_path cp LEFT JOIN " . DB_PREFIX . "category c1 ON (cp.category_id = c1.category_id) LEFT JOIN " . DB_PREFIX . "category c2 ON (cp.path_id = c2.category_id) LEFT JOIN " . DB_PREFIX . "category_description cd1 ON (cp.path_id = cd1.category_id) LEFT JOIN " . DB_PREFIX . "category_description cd2 ON (cp.category_id = cd2.category_id) WHERE cd1.language_id = '" . (int)$this->config->get('config_language_id') . "' AND cd2.language_id = '" . (int)$this->config->get('config_language_id') . "'";

		if (!empty($data['filter_name'])) {
			$sql .= " AND cd2.name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (isset($data['filter_status']) and $data['filter_status'] != '') {
			$sql .= " AND c2.gcatstatus = '" . $data['filter_status'] . "'";
		}

		if (isset($data['filter_eslesme']) and $data['filter_eslesme'] != '') {
			if($data['filter_eslesme'] == 1){
				$sql .= " AND c1.gg_id != '0'";
			} else {
				$sql .= " AND c1.gg_id = '0'";
			}
		}

		if (isset($data['filter_komisyon']) and $data['filter_komisyon'] != '') {
			if($data['filter_komisyon'] == 1){
				$sql .= " AND c1.ggcomission != 0";
			} else {
				$sql .= " AND c1.ggcomission = 0";
			}
		}

		if (!empty($data['filter_category_id'])) {
			$sql .= " AND cp.category_id LIKE '" . $this->db->escape($data['filter_category_id']) . "%'";
		}

		$sql .= " GROUP BY cp.category_id";

		$sort_data = array(
			'name',
			'sort_order'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY sort_order";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	/*
		n11 kategorilerini çekiyoruz
	*/
	public function getGgcategories($data){
		$sql = "SELECT *  FROM " . DB_PREFIX . "ggcategories";

		$sort_data = array(
			'name',
			'category_id'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		$query = $this->db->query($sql);
		return $query->rows;
	}

	// total kategorimiz
	public function getTotalGgcategories(){
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "ggcategories");
		return $query->row['total'];
	}

	/*
		bir n11 kategorisine ait özellik sayısı
	*/
	public function attrCount($category_id){
		$attr = $this->db->query("SELECT * FROM ".DB_PREFIX."n11category_to_attr WHERE category_id = '".$category_id."'");
		return $attr->num_rows;
	}
}
?>