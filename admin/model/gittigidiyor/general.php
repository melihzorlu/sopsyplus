<?php
/* 
	Gittigidiyor model dosyasıdır
	Gittigidiyor için lazım gelen genel sorgular bu dosyada barınmaktadır.
	kullanılan tablolar

	".db_prefix."ggshops - mağazaların bulunduğu tablodur.
*/

class ModelGittigidiyorGeneral extends Model {
	public function install(){
		$this->db->query("CREATE TABLE `".DB_PREFIX."ggcategories` (
		  `id` int(11) NOT NULL,
		  `category_code` varchar(255) CHARACTER SET utf8 NOT NULL,
		  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
		  `ggattr` longtext CHARACTER SET utf8 NOT NULL,
		  `status` int(11) NOT NULL DEFAULT '1'
		) ENGINE=MyISAM DEFAULT CHARSET=latin1;");

		$this->db->query("CREATE TABLE `".DB_PREFIX."ggorders` (
		  `id` int(11) NOT NULL,
		  `orderid` varchar(255) CHARACTER SET utf8 NOT NULL,
		  `shop_id` int(11) NOT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");

		$this->db->query("CREATE TABLE `".DB_PREFIX."ggorder_item` (
		  `id` int(11) NOT NULL,
		  `orderid` int(11) NOT NULL,
		  `itemid` int(11) NOT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");

		$this->db->query("CREATE TABLE `".DB_PREFIX."ggproduct` (
		  `id` int(11) NOT NULL,
		  `product_id` int(255) NOT NULL,
		  `shop_id` int(255) NOT NULL,
		  `gg_id` int(11) NOT NULL DEFAULT '0',
		  `gg_title` varchar(255) NOT NULL,
		  `gg_subtitle` varchar(255) NOT NULL,
		  `gg_price` decimal(14,6) NOT NULL,
		  `gg_commission` int(11) NOT NULL DEFAULT '0',
		  `gg_description` longtext NOT NULL,
		  `gg_salestatus` int(11) NOT NULL DEFAULT '0',
		  `gg_catid` varchar(255) NOT NULL,
		  `gg_attribute` longtext NOT NULL,
		  `latest_update` datetime DEFAULT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

		$this->db->query("CREATE TABLE `".DB_PREFIX."ggshops` (
		  `id` int(11) NOT NULL,
		  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
		  `apikey` varchar(255) CHARACTER SET utf8 NOT NULL,
		  `secretkey` varchar(255) CHARACTER SET utf8 NOT NULL,
		  `nick` varchar(255) CHARACTER SET utf8 NOT NULL,
		  `password` varchar(255) CHARACTER SET utf8 NOT NULL,
		  `auth_user` varchar(255) CHARACTER SET utf8 NOT NULL,
		  `auth_pass` varchar(255) CHARACTER SET utf8 NOT NULL,
		  `default_subtitle` varchar(255) CHARACTER SET utf8 NOT NULL,
		  `cargo_company` varchar(255) CHARACTER SET utf8 NOT NULL,
		  `shipping_condition` int(11) NOT NULL,
		  `shipping_price` decimal(14,6) NOT NULL,
		  `shipping_aprice` decimal(14,6) NOT NULL,
		  `difference_type` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '0',
		  `difference_value` int(11) NOT NULL DEFAULT '0',
		  `currency` int(11) NOT NULL DEFAULT '1',
		  `currency_calctype` int(11) NOT NULL DEFAULT '2',
		  `tax_option` int(11) NOT NULL DEFAULT '2',
		  `description` longtext CHARACTER SET utf8 NOT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");

		$this->db->query("ALTER TABLE `".DB_PREFIX."ggcategories` ADD PRIMARY KEY (`id`);");
		$this->db->query("ALTER TABLE `".DB_PREFIX."ggorders` ADD PRIMARY KEY (`id`);");
		$this->db->query("ALTER TABLE `".DB_PREFIX."ggorder_item` ADD PRIMARY KEY (`id`);");
		$this->db->query("ALTER TABLE `".DB_PREFIX."ggproduct` ADD PRIMARY KEY (`id`);");
		$this->db->query("ALTER TABLE `".DB_PREFIX."ggshops` ADD PRIMARY KEY (`id`);");
		$this->db->query("ALTER TABLE `".DB_PREFIX."ggcategories` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5917;");
		$this->db->query("ALTER TABLE `".DB_PREFIX."ggorders`  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;");
		$this->db->query("ALTER TABLE `".DB_PREFIX."ggorder_item` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;");
		$this->db->query("ALTER TABLE `".DB_PREFIX."ggproduct` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;");
		$this->db->query("ALTER TABLE `".DB_PREFIX."ggshops` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;");
		$this->db->query("ALTER TABLE `".DB_PREFIX."category` ADD `gg_id` VARCHAR(255) NOT NULL DEFAULT '0' AFTER `category_id`;");
		$this->db->query("ALTER TABLE `".DB_PREFIX."category` ADD `ggcomission` INT(11) NOT NULL DEFAULT '0' AFTER `gg_id`;");
		$this->db->query("ALTER TABLE `".DB_PREFIX."category` ADD `gcatstatus` INT(11) NOT NULL DEFAULT '1' AFTER `gg_id`;");
		$this->db->query("ALTER TABLE `".DB_PREFIX."category` ADD `ggattr`  longtext CHARACTER SET utf8 AFTER `ggcomission`;");
	}
	/*
	  gerekli linklerimiz
	*/
	 public function getlinks(){
	 	if(VERSION < 3){
			$token = $this->session->data['token'];
			$token_link = 'token=' . $this->session->data['token'];
		} else {
			$token = $this->session->data['user_token'];
			$token_link = 'user_token=' . $this->session->data['user_token'];
		}

	 	if(strstr($_GET['route'],'gittigidiyor/dashboard')){ $css = 'active'; } else { $css = 'atab'; }
	 	$data[] = array(
	 		'link' => $this->url->link('gittigidiyor/dashboard', $token_link, 'SSL'),
	 		'text' => 'DASHBOARD',
	 		'css'  => $css
	 	);

	 	if(strstr($_GET['route'],'gittigidiyor/category')){ 
	 		if(!strstr($_GET['route'],'gittigidiyor/category/ggcategory') and !strstr($_GET['route'], 'gittigidiyor/category/select_root')) {
	 			$css = 'active'; 
	 		} else {
	 			 $css = 'atab';
	 		}
	 	} else { $css = 'atab'; }
	 	$data[] = array(
	 		'link' => $this->url->link('gittigidiyor/category', $token_link, 'SSL'),
	 		'text' => 'KATEGORİLER',
	 		'css'  => $css
	 	);

	 	if(strstr($_GET['route'],'gittigidiyor/category/ggcategory')){ $css = 'active'; } else if(strstr($_GET['route'],'n11/category/select_root')){ $css = 'active'; } else { $css = 'atab'; }
	 	$data[] = array(
	 		'link' => $this->url->link('gittigidiyor/category/ggcategory', $token_link, 'SSL'),
	 		'text' => 'GG KATEGORİLERİ',
	 		'css'  => $css
	 	);

	 	/*
	 	seçenek v1 -- iptal edildi
	 	if(strstr($_GET['route'],'gittigidiyor/secenekler')){ $css = 'active'; } else { $css = 'atab'; }
	 	$data[] = array(
	 		'link' => $this->url->link('gittigidiyor/secenekler', $token_link, 'SSL'),
	 		'text' => 'SEÇENEKLER',
	 		'css'  => $css
	 	);
	 	*/

	 	if(strstr($_GET['route'],'gittigidiyor/product')){ 
	 		if(!strstr($_GET['route'],'gittigidiyor/product/questions')) {
	 			$css = 'active'; 
	 		} else {
	 			 $css = 'atab';
	 		}
	 	} else { $css = 'atab'; }
	 	$data[] = array(
	 		'link' => $this->url->link('gittigidiyor/product', $token_link, 'SSL'),
	 		'text' => 'ÜRÜNLER',
	 		'css'  => $css
	 	);

	 	if(strstr($_GET['route'],'gittigidiyor/product/questions')){ $css = 'active'; } else { $css = 'atab'; }
	 	$data[] = array(
	 		'link' => $this->url->link('gittigidiyor/product/questions', $token_link, 'SSL'),
	 		'text' => 'MESAJ KUTUSU',
	 		'css'  => $css
	 	);

	 	if(strstr($_GET['route'],'gittigidiyor/sales')){ $css = 'active'; } else { $css = 'atab'; }
	 	$data[] = array(
	 		'link' => $this->url->link('gittigidiyor/sales', $token_link, 'SSL'),
	 		'text' => 'SİPARİŞLER',
	 		'css'  => $css
	 	);
 

	 	if(strstr($_GET['route'],'gittigidiyor/logs')){ $css = 'active'; } else { $css = 'atab'; }
	 	$data[] = array(
	 		'link' => $this->url->link('gittigidiyor/logs', $token_link, 'SSL'),
	 		'text' => 'LOGLAR',
	 		'css'  => $css
	 	);

	 	if(strstr($_GET['route'],'gittigidiyor/setting')){ $css = 'active'; } else { $css = 'atab'; }
	 	$data[] = array(
	 		'link' => $this->url->link('gittigidiyor/setting', $token_link, 'SSL'),
	 		'text' => 'DÜKKAN AYARLARI',
	 		'css'  => $css
	 	);


	 	/*
	 	if(strstr($_GET['route'],'gittigidiyor/shops')){ $css = 'active'; } else { $css = 'atab'; }
	 	$data[] = array(
	 		'link' => $this->url->link('gittigidiyor/shops', 'user_token=' . $this->session->data['user_token'], 'SSL'),
	 		'text' => 'DÜKKANLAR',
	 		'css'  => $css
	 	);

	 	
	 	if(strstr($_GET['route'],'gittigidiyor/info')){ $css = 'active'; } else { $css = 'atab'; }
	 	$data[] = array(
	 		'link' => $this->url->link('gittigidiyor/info', 'user_token=' . $this->session->data['user_token'], 'SSL'),
	 		'text' => '<i class="fa fa-info"></i>',
	 		'css'  => $css
	 	);
		*/
		return $data;
	 }


	/*
		getAuth fonkisyonu gittigidiyor güvenlik şeridini geçmek için kullanılır.
	*/
	public function getAuth(){
		if(VERSION < 3){
			$token = $this->session->data['token'];
			$token_link = 'token=' . $this->session->data['token'];
		} else {
			$token = $this->session->data['user_token'];
			$token_link = 'user_token=' . $this->session->data['user_token'];
		}
		
		date_default_timezone_set('Europe/Istanbul');
		if($this->has_shop() == false){
			$this->response->redirect($this->url->link('gittigidiyor/install', $token_link, 'SSL'));
		}

		$shop = $this->getShop();
		$client = new Gittigidiyor();
		$client->apiKey = $shop['apikey'];
		$client->secretKey = $shop['secretkey'];
		//$client->nick = $shop['nick'];
		//$client->password = $shop['password'];
		$client->auth_user = $shop['auth_user'];
		$client->auth_pass = $shop['auth_pass'];
		$client->lang = 'tr';
		list($usec, $sec) = explode(" ", microtime());
		$time = round(((float)$usec + (float)$sec) * 100).'0';
		$client->time = $time;
		$client->sign = md5($shop['apikey'].$shop['secretkey'].$time);
		return $client;
	}



	
	public function has_shop(){
	 	$check = $this->db->query("SELECT id FROM ".DB_PREFIX."ggshops")->num_rows;
	 	if($check > 0){ return true; } else { return false; }
	}

	public function CreatePage($title, $models = array()){
		$this->document->setTitle($title);
		foreach ($models as $model) {
			$this->load->model($model);
		}
		$this->document->addStyle('view/template/gittigidiyor/asset/global_style.css');
		$this->document->addStyle('view/template/gittigidiyor/asset/select2/css/select2.css');
		$this->document->addStyle('view/template/gittigidiyor/asset/bootstrap3-editable/css/bootstrap-editable.css');
		$this->document->addStyle('view/template/gittigidiyor/asset/inputs-ext/typeaheadjs/lib/typeahead.js-bootstrap.css');
		$this->document->addStyle("view/template/gittigidiyor/asset/boottoggle/css/bootstrap-toggle.min.css");
  		$this->document->addScript("view/template/gittigidiyor/asset/boottoggle/js/bootstrap-toggle.min.js");
  		$this->document->addStyle("view/template/gittigidiyor/asset/toast/jquery.toast.min.css");
  		$this->document->addScript("view/template/gittigidiyor/asset/toast/jquery.toast.min.js");
		$this->document->addScript('view/template/gittigidiyor/asset/bootstrap3-editable/js/bootstrap-editable.js');
		$this->document->addScript('view/template/gittigidiyor/asset/inputs-ext/typeaheadjs/lib/typeahead.js');
		$this->document->addScript('view/template/gittigidiyor/asset/inputs-ext/typeaheadjs/typeaheadjs.js');
		$this->document->addScript('view/template/gittigidiyor/asset/select2/js/select2.full.js');
	}


	/* magağazamızı ekleyelim */
	public function addShop($data){
		$this->db->query("INSERT INTO ".DB_PREFIX."ggshops SET name = '".$data['name']."', apikey = '".$data['apikey']."', secretkey = '".$data['secretkey']."', auth_user = '".$data['auth_user']."', auth_pass = '".$data['auth_pass']."'");
		$shop_id = $this->db->getLastId();
		$this->session->data['ggshop_id'] = $shop_id;
		return $shop_id;
	}

	/* mağaza güncelleme v2 */
	public function updateshop($data){
		$shop_id = $data['shop_id'];
		foreach ($data as $key => $value) {
			if($key != 'shop_id' and $key != 'files' and $key != 'iaction'){
				$this->db->query("UPDATE ".DB_PREFIX."ggshops SET ".$key." = '".$value."' WHERE id = '".$shop_id."'");
			}
		}
		return $data['shop_id'];
	}

	

	/*
		mağaza listesini almak için kullandığımız metod
	*/
	public function getShops(){
		$shops_query = $this->db->query("SELECT * FROM ".DB_PREFIX."ggshops ORDER BY id DESC LIMIT 1");
		return $shops_query->rows;
	}

	/*
		tema oluşturmak için yazdığımız fonskiyon
		bir olayı yok kafa karıştırmak
	*/
	public function gettheme($data, $doc){
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		$this->response->setOutput($this->load->view('gittigidiyor/'.$doc, $data));
	}

	/*
		ActShop - Aktif Mağaza Bilgilerini Alır
	*/
	public function actShopid(){
		if($this->has_shop() == true){
			if(isset($this->session->data['ggshop_id'])){
				$shop = $this->db->query("SELECT * FROM ".DB_PREFIX."ggshops WHERE id = '".$this->session->data['ggshop_id']."'");
				if($shop->num_rows > 1){
					return $shop->row['id'];
				} else {
					$shop = $this->db->query("SELECT * FROM ".DB_PREFIX."ggshops ORDER BY id DESC LIMIT 1");
					$this->session->data['ggshop_id'] = $shop->row['id'];
					return $shop->row['id'];
				}
			} else {
				$shop = $this->db->query("SELECT * FROM ".DB_PREFIX."ggshops ORDER BY id DESC LIMIT 1");
				$this->session->data['ggshop_id'] = $shop->row['id'];
				return $shop->row['id'];
			}
		} else {
			return false;
		}
	}

	/* 
	  bir mağazanın blgilerini almak için kulanılır
	*/
	public function getShop($id = null){
		if($id == null){
			$shop = $this->db->query("SELECT * FROM ".DB_PREFIX."ggshops ORDER BY id  DESC LIMIT 1");
		} else {
			$shop = $this->db->query("SELECT * FROM ".DB_PREFIX."ggshops WHERE id = '".$id."'");
		}
		
		return $shop->row;	
	}

	/*
	 bir mağazayı silelim
	*/
	public function deleteshop($id){
		$shop = $this->db->query("DELETE FROM ".DB_PREFIX."ggshops WHERE id = '".$id."'");
		$product = $this->db->query("DELETE FROM ".DB_PREFIX."ggproduct WHERE shop_id = '".$id."'");
		$orders = $this->db->query("SELECT * FROM ".DB_PREFIX."ggorders WHERE shop_id = '".$id."'");
		foreach ($orders->rows as $value) {
			$this->db->query("DELETE FROM ".DB_PREFIX."ggorder_item WHERE order_id = '".$value['orderid']."'");
		}
		$orders = $this->db->query("DELETE FROM ".DB_PREFIX."ggorders WHERE shop_id = '".$id."'");
		return 'ok';
	}

	public function eslesmemisCat(){
		$query = $this->db->query("SELECT * FROM ".DB_PREFIX."category WHERE gg_id = '0'");
		return $query->num_rows;
	}


	public function geturunsoru($client){
		$mesjalar = $client->getInboxMessages(array('startOffSet' => 0, 'rowCount' => 100, 'messageType' => 'ALL', 'lang' => 'tr'));
		if($mesjalar->conversationsCount > 0){
			return $mesjalar->conversationsCount;
		} else {
			$this->session->data['error'] = $mesjalar->error->message;
			return 0;
		}
	}
}
?>