<?php
/*
	sipariş işlemlerini yaptığımız model dosyası
	siparişleri çekme ve eşitleme
	sipariş sayılarını alma
	sipariş onaylama reddetme gibi işlemleri yapıyoruz
*/
class ModelGittigidiyorSale extends Model {
	/*
		getordercount fonkisyonu gittigidiyor sipariş durumuna göre sipariş sayısını aldığımız fonskiyondur
	*/
	public function getOrderCount($client){
		if(VERSION < 3){
			$token = $this->session->data['token'];
			$token_link = 'token=' . $this->session->data['token'];
		} else {
			$token = $this->session->data['user_token'];
			$token_link = 'user_token=' . $this->session->data['user_token'];
		}
		$odeme_beklenen = $client->getSales(false, 'P', null, 'P', 'A', 1, 50);
		if($odeme_beklenen->ackCode != 'failure'){
			$data['odemebeklenen_count'] = $odeme_beklenen->saleCount;
		} else {
			$data['odemebeklenen_count'] = 0;
			$this->session->data['error'] = $odeme_beklenen->error->message;
		}

		$kargo_yapilacaklar = $client->getSales(false, 'S', null, 'P', 'A', 1, 50);
		if($kargo_yapilacaklar->ackCode != 'failure'){
			$data['kargoyapilacaklar_count'] = $kargo_yapilacaklar->saleCount;
		} else {
			$data['kargoyapilacaklar_count'] = 0;
			$this->session->data['error'] = $kargo_yapilacaklar->error->message;
		}

		$onay_bekleyenler = $client->getSales(false, 'C', null, 'P', 'A', 1, 50);
		if($onay_bekleyenler->ackCode != 'failure'){
			$data['onaybekleyenler_count'] = $onay_bekleyenler->saleCount;
		} else {
			$data['onaybekleyenler_count'] = 0;
			$this->session->data['error'] = $onay_bekleyenler->error->message;
		}


		$para_transferi = $client->getSales(false, 'T', null, 'P', 'A', 1, 50);
		if($para_transferi->ackCode != 'failure'){
			$data['paratransferi_count'] = $para_transferi->saleCount;
		} else {
			$data['paratransferi_count'] = 0;
			$this->session->data['error'] = $para_transferi->error->message;
		}
		
		$iade_konumu = $client->getSales(false, 'R', null, 'P', 'A', 1, 50);
		if($iade_konumu->ackCode != 'failure'){
			$data['iadekonumu_count'] = $iade_konumu->saleCount;
		} else {
			$data['iadekonumu_count'] = 0;
			$this->session->data['error'] = $iade_konumu->error->message;
		}

		$tamamlananlar = $client->getSales(false, 'O', null, 'P', 'A', 1, 50);
		if($tamamlananlar->ackCode != 'failure'){
			$data['tamamlananlar_count'] = $tamamlananlar->saleCount;
		} else {
			$data['tamamlananlar_count'] = 0;
			$this->session->data['error'] = $tamamlananlar->error->message;
		}


		$data['odemebeklenen'] = $this->url->link('gittigidiyor/sales', $token_link, true);
		$data['kargoyaplacaklar'] = $this->url->link('gittigidiyor/sales', 'status=S&'.$token_link, true);
		$data['onaybekleyenler'] = $this->url->link('gittigidiyor/sales', 'status=C&'.$token_link, true);
		$data['paratransferi'] = $this->url->link('gittigidiyor/sales','status=T&'.$token_link, true);
		$data['iadekonumu'] = $this->url->link('gittigidiyor/sales', 'status=R&'.$token_link, true);
		$data['tamamlananlar'] = $this->url->link('gittigidiyor/sales', 'status=O&'.$token_link, true);
		return $data;
	}

	public function getSales($client, $filter = array()){

		/* 
			statusler
			Ödeme bekleyenler için (T) | Kargo yapılacaklar için (V) | Onay bekleyenler için (P) | Para transferleri için (A) | İade konumundakiler için (S) | Tamamlananlar için (R) parametresini girmelisiniz
		*/
		$siparisler = $client->getSales(true, $filter['status'], null, 'P', 'A', $filter['start'], $filter['limit']);

		if($siparisler->ackCode == 'failure'){
			$this->session->data['error'] = $siparisler->error->message;
			return array();
		} else {
			return $siparisler;
		}
	}

	public function getSale($client, $saleCode){
		$sale = $client->getSale($saleCode);
		return $sale;
	}

	public function getVariant($client, $urunid, $variantId){
		$variant = $client->getProductVariants($urunid, '', $variantId, '');
		return $variant;
	}

	public function getCargo($client, $saleCode){
		$cargo = $client->getCargoInformation($saleCode);
		return $cargo;
	}

	public function getCity($client, $citycode){
		$city = $client->getCity($citycode);
		return $city;
	}

	public function sendCargo($client, $post){
		$send = $client->sendCargoInformation($post['saleCode'], $post['cargoPostCode'], $post['cargoCompany'], $post['cargoBranchCode'], $post['followUpUrl'], 'S');

		if($send->ackCode == 'success'){
			$this->insertopencart($client, $post['saleCode']);
			return $send;
		} else {
			return $send;
		}
	}

	public function insertopencart($client, $salecode){
		error_reporting(E_ALL);

		$sale = $client->getSale($salecode);
		if(isset($sale->sales->sale)){
			$ggid = $sale->sales->sale->productId;
			$gdata = $this->db->query("SELECT * FROM ".DB_PREFIX."ggproduct WHERE gg_id = '".$ggid."'")->row;
			$shop_query = $this->db->query("SELECT * FROM ".DB_PREFIX."ggshops WHERE id = '".$gdata['shop_id']."'")->row;
			$ocproduct_id = $gdata['product_id'];
			$adet = $sale->sales->sale->amount;
			$totalfiyat = $sale->sales->sale->price;
			/*
			if($shop_query['currency'] != '1'){
				$connect_web = simplexml_load_file('http://www.tcmb.gov.tr/kurlar/today.xml');
				$usd = $connect_web->Currency[0]->BanknoteSelling;
				$euro = $connect_web->Currency[3]->BanknoteSelling;
				if($shop_query['currency'] == '2'){
					$totalfiyat = (float)$totalfiyat / (float)$usd;
				} else {
					$totalfiyat = $totalfiyat / $euro;
				}
			}
			*/
			$alicibilgi = $sale->sales->sale->buyerInfo;
			$urunadi = $sale->sales->sale->productTitle;
		}
		
		$product = $this->db->query("SELECT * FROM ".DB_PREFIX."product WHERE product_id = '".$ocproduct_id."'")->row;
		$order_check = $this->db->query("SELECT * FROM ".DB_PREFIX."order WHERE ggsalecode = '".$salecode."'");
		if($order_check->num_rows > 0){
			// siparişi güncelle
			$tax_rate = $this->db->query("SELECT * FROM ".DB_PREFIX."tax_rule WHERE tax_class_id = '".$product['tax_class_id']."' ORDER BY priority DESC LIMIT 1")->row;
			$tax = $this->db->query("SELECT * FROM ".DB_PREFIX."tax_rate WHERE tax_rate_id = '".$tax_rate['tax_rate_id']."'")->row;
			$kdv = ($totalfiyat / 100) * $tax['rate'];
			if($tax['rate'] == 18){
				$taxxx += $kdv;
			}
			if($tax['rate'] == 8){
				$taxx += $kdv;
			}
			$totaltax = $kdv;
			$total = ($value->dueAmount - $kdv);
			$totalkdv = $value->dueAmount;
			$this->db->query("UPDATE ".DB_PREFIX."order_total SET value = value + '".$total."' WHERE order_id = '".$order_check->row['order_id']."' AND code = 'sub_total'");
			$this->db->query("UPDATE ".DB_PREFIX."order_total SET value = value + '".$totalkdv."' WHERE order_id = '".$order_check->row['order_id']."' AND code = 'total'");
			$this->db->query("UPDATE ".DB_PREFIX."order_total SET value = value + '".$totaltax."' WHERE order_id = '".$order_check->row['order_id']."' AND code = 'tax'");
			$this->db->query("INSERT INTO ".DB_PREFIX."order_product SET order_id = '".$order_check->row['order_id']."', product_id = '".$product['product_id']."', name = '".$urunadi."', model = '".$product['model']."', quantity = '".$adet."', price = '".(($totalfiyat - $kdv) / $adet)."', tax = '".$kdv."'");
			
		} else {
			// yeni ekle
			
			$name = $alicibilgi->name;
			$surname = $alicibilgi->surname;
			$email = $alicibilgi->username."@example.com";
			$phone = str_replace('-','',$alicibilgi->mobilePhone);
			$tck = "";
			
			/* kullanıcı kontrolü */
			$check_user = $this->db->query("SELECT * FROM ".DB_PREFIX."customer WHERE email = '".$alicibilgi->username."@example.com'");
			if($check_user->num_rows > 0){
				// üye var
				$customer_id = $check_user->row['customer_id'];
			} else {
				$this->db->query("INSERT INTO ".DB_PREFIX."customer SET 
						customer_group_id = '".(int) $this->config->get('config_customer_group_id')."', 
						store_id = '" . (int)$this->config->get('config_store_id') . "',
						language_id = '". (int) $this->config->get('config_language_id')."',
						firstname = '".$alicibilgi->name."',
						lastname = '".$alicibilgi->surname."',
						email = '".$alicibilgi->username."@example.com',
						telephone = '".str_replace('-','',$alicibilgi->mobilePhone)."',
						password = '".md5(str_replace('-','',$alicibilgi->mobilePhone))."',
						newsletter = '1',
						status = '1',
						approved = '1',
						safe = '1',
						date_added = NOW()");
				$customer_id = $this->db->getLastId();
			}

			$fname = $alicibilgi->name;
			$fsurname = $alicibilgi->surname;
			$email = $alicibilgi->username."@example.com";
			$phone = str_replace('-','',$alicibilgi->mobilePhone);
			$tck = '';
			$bzone_id = $this->db->query("SELECT * FROM ".DB_PREFIX."zone WHERE name LIKE '".$alicibilgi->city."'")->row;
			if(isset($bzone_id['zone_id'])){
				$bzone_id_result = $bzone_id['zone_id'];
			} else {
				$bzone_id_result = 0;
			}
			// fatura adres girişi
			$this->db->query("INSERT INTO oc_address SET
					customer_id = '".$customer_id."',
					firstname = '".$fname."',
					lastname = '".$fsurname."',
					company = '',
					address_1 = '".$alicibilgi->address."',
					address_2 = '".$alicibilgi->district."',
					city = '".$alicibilgi->city."',
					postcode = '".$alicibilgi->zipCode."',
					country_id = '215',
					zone_id = '".$bzone_id_result."',
					custom_field = ''");

			$sname = $alicibilgi->name;
			$ssurname = $alicibilgi->surname;
			$email = $alicibilgi->username."@example.com";
			$phone = str_replace('-','',$alicibilgi->mobilePhone);
			$tck = '';
			$szone_id = $this->db->query("SELECT * FROM ".DB_PREFIX."zone WHERE name LIKE '".$alicibilgi->city."'")->row;
			if(isset($szone_id['zone_id'])){
				$szone_id_result = $szone_id['zone_id'];
			} else {
				$szone_id_result = 0;
			}
			// fatura adres girişi
			$this->db->query("INSERT INTO oc_address SET
				customer_id = '".$customer_id."',
				firstname = '".$fname."',
				lastname = '".$fsurname."',
				address_1 = '".$alicibilgi->address."',
				address_2 = '".$alicibilgi->district."',
				city = '".$alicibilgi->city."',
				postcode = '".$alicibilgi->zipCode."',
				country_id = '215',
				zone_id = '".$szone_id_result."'");

			$order_products = array();
			$taxxx = 0;
			$taxx = 0;
			$totaltax = 0;
			$total = 0;
			$totalkdv = 0;
			
				$product = $this->db->query("SELECT * FROM ".DB_PREFIX."product WHERE product_id = '".$product['product_id']."'")->row;
				$tax_rate = $this->db->query("SELECT * FROM ".DB_PREFIX."tax_rule WHERE tax_class_id = '".$product['tax_class_id']."' ORDER BY priority DESC LIMIT 1")->row;
				$tax = $this->db->query("SELECT * FROM ".DB_PREFIX."tax_rate WHERE tax_rate_id = '".$tax_rate['tax_rate_id']."'")->row;
				$kdv = ($totalfiyat / 100) * $tax['rate'];
				if($tax['rate'] == 18){
					$taxxx += $kdv;
				}
				if($tax['rate'] == 8){
					$taxx += $kdv;
				}

				$prod_option = array();

				/*
				opsiyon şimdilik yok
				if(isset($order->itemList->item->attributes->attribute)){
					if(isset($order->itemList->item->attributes->attribute->name)){
						// tekli opsiyon
						$oc_opiton = $this->db->query("SELECT * FROM ".DB_PREFIX."option_description WHERE name = '".$order->itemList->item->attributes->attribute->name."'");
						if($oc_opiton->num_rows > 0){
							// opsiyon var
							$oc_option_value = $this->db->query("SELECT * FROM ".DB_PREFIX."option_value_description WHERE option_id = '".$oc_opiton->row['option_id']."' AND name = '".$order->itemList->item->attributes->attribute->value."'");
							$oc_product_option = $this->db->query("SELECT * FROM ".DB_PREFIX."product_option WHERE option_id = '".$oc_opiton->row['option_id']."' AND product_id = '".$product['product_id']."'");
							$oc_product_option_value = $this->db->query("SELECT * FROM ".DB_PREFIX."product_option_value WHERE option_value_id = '".$oc_option_value->row['option_value_id']."' AND product_id = '".$product['product_id']."'");

							$prod_option[] = array(
								'product_option_id' => $oc_product_option->row['product_option_id'],
								'product_option_value_id' => $oc_product_option_value->row['product_option_value_id'],
								'name' => $order->itemList->item->attributes->attribute->name,
								'value' => $order->itemList->item->attributes->attribute->value,
								'type' => 'select'
							);
						}
					} else {
						if(count($order->itemList->item->attributes->attribute) > 1){
							foreach ($order->itemList->item->attributes->attribute as $prod_attr) {
								$oc_opiton = $this->db->query("SELECT * FROM ".DB_PREFIX."option_description WHERE name = '".$prod_attr->name."'");
								if($oc_opiton->num_rows > 0){
									// opsiyon var
									$oc_option_value = $this->db->query("SELECT * FROM ".DB_PREFIX."option_value_description WHERE option_id = '".$oc_opiton->row['option_id']."' AND name = '".$prod_attr->value."'");
									$oc_product_option = $this->db->query("SELECT * FROM ".DB_PREFIX."product_option WHERE option_id = '".$oc_opiton->row['option_id']."' AND product_id = '".$product['product_id']."'");
									$oc_product_option_value = $this->db->query("SELECT * FROM ".DB_PREFIX."product_option_value WHERE option_value_id = '".$oc_option_value->row['option_value_id']."' AND product_id = '".$product['product_id']."'");

									$prod_option[] = array(
										'product_option_id' => $oc_product_option['product_option_id'],
										'product_option_value_id' => $oc_product_option_value->row['product_option_value_id'],
										'name' => $prod_attr->name,
										'value' => $prod_attr->value,
										'type' => 'select'
									);
								}
							}
						}
					}
				}
				*/
				$totaltax += $kdv;
				$total += ($totalfiyat - $kdv);
				$totalkdv += $totalfiyat;
				$order_products[] = array(
					'product_id' => $product['product_id'],
					'name' => $urunadi,
					'model' => $product['model'],
					'quantity' => $adet,
					'price' => (($totalfiyat - $kdv) / $adet),
					'total' => ($totalfiyat - $kdv),
					'option' => $prod_option,
					'tax' => $kdv,
					'reward' => ''
				);
			
		
			$totals = array();
			if($taxx > 0){
				$totals[] = array(
					'code' => 'tax',
					'title' => 'KDV (%8)',
					'sort_order' => '8',
					'value' => $taxx
				);
			}
			if($taxxx > 0){
				$totals[] = array(
					'code' => 'tax',
					'title' => 'KDV (%18)',
					'sort_order' => '7',
					'value' => $taxxx
				);
			}
			$totals[] = array(
				'code' => 'sub_total',
				'title' => 'Ara Toplam',
				'sort_order' => 5,
				'value' => $total
			);
			$totals[] = array(
				'code' => 'total',
				'title' => 'Toplam',
				'sort_order' => '9',
				'value' => $totalkdv
			);

			$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
			$order_data = array(
				'invoice_prefix' => $this->config->get('config_invoice_prefix'),
				'store_id' => $this->config->get('config_store_id'),
				'store_name' => $this->config->get('config_name'),
				'store_url' =>  $this->config->get('config_url'),
				'customer_id' => $customer_id,
				'customer_group_id' => (int)$this->config->get('config_customer_group_id'),
				'firstname' => $name,
				'lastname' => $surname,
				'email' => $email,
				'salecode' => $salecode,
				'telephone' => $phone,
				'fax' => '',
				'custom_field' => '',
				'payment_firstname' => $fname,
				'payment_lastname' => $fsurname,
				'payment_company' => '',
				'payment_address_1' => $alicibilgi->address,
				'payment_address_2' => $alicibilgi->district,
				'payment_city' => $alicibilgi->city,
				'payment_postcode' => $alicibilgi->zipCode,
				'payment_country' => 'Türkiye',
				'payment_country_id' => '215',
				'payment_zone' => $alicibilgi->city,
				'payment_zone_id' => $bzone_id_result,
				'payment_address_format' => $format,
				'payment_custom_field' => '',
				'payment_method' => 'gittigidiyor.com',
				'payment_code' => 'gg',
				'shipping_firstname' => $sname,
				'shipping_lastname' => $ssurname,
				'shipping_company' => '',
				'shipping_address_1' => $alicibilgi->address,
				'shipping_address_2' => $alicibilgi->district,
				'shipping_city' => $alicibilgi->city,
				'shipping_postcode' => $alicibilgi->zipCode,
				'shipping_country' => 'Türkiye',
				'shipping_country_id' => '215',
				'shipping_zone' => $alicibilgi->city,
				'shipping_zone_id' => $szone_id_result,
				'shipping_address_format' => $format,
				'shipping_custom_field' => '',
				'shipping_method' => '',
				'shipping_code' => 'flat.flat',
				'comment' => $alicibilgi->name.' n11.com siparişi',
				'total' => $totalfiyat,
				'affiliate_id' => '',
				'commission' => '',
				'marketing_id' => '',
				'tracking' => '',
				'language_id' => $this->config->get('config_language_id'),
				'currency_id' => $this->currency->getId('TRY'),
				'currency_code' => 'TRY',
				'currency_value' => 1,
				'ip' => '',
				'forwarded_ip' => '',
				'user_agent' => '',
				'accept_language' => '',
				'products' => $order_products,
				'vouchers' => '',
				'totals' => $totals
			);
			$order_id = $this->addOrder($order_data);
			$this->db->query("INSERT INTO " . DB_PREFIX . "order_history SET order_id = '" . (int)$order_id . "', order_status_id = '" . (int)$this->config->get('config_order_status_id') . "', notify = '0', comment = '".$alicibilgi->name." ".$alicibilgi->surname." gittigidiyor.com siparişi', date_added = NOW()");
			$this->db->query("UPDATE ".DB_PREFIX."order SET order_status_id = '".$this->config->get('config_order_status_id')."' WHERE order_id = '".$order_id."'");
		}
	}

	public function addOrder($data) {
		$this->db->query("INSERT INTO `" . DB_PREFIX . "order` SET invoice_prefix = '" . $this->db->escape($data['invoice_prefix']) . "', ggsalecode = '" . $this->db->escape($data['salecode']) . "', store_id = '" . (int)$data['store_id'] . "', store_name = '" . $this->db->escape($data['store_name']) . "', store_url = '" . $this->db->escape($data['store_url']) . "', customer_id = '" . (int)$data['customer_id'] . "', customer_group_id = '" . (int)$data['customer_group_id'] . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', fax = '" . $this->db->escape($data['fax']) . "', custom_field = '" . $this->db->escape(isset($data['custom_field']) ? json_encode($data['custom_field']) : '') . "', payment_firstname = '" . $this->db->escape($data['payment_firstname']) . "', payment_lastname = '" . $this->db->escape($data['payment_lastname']) . "', payment_company = '" . $this->db->escape($data['payment_company']) . "', payment_address_1 = '" . $this->db->escape($data['payment_address_1']) . "', payment_address_2 = '" . $this->db->escape($data['payment_address_2']) . "', payment_city = '" . $this->db->escape($data['payment_city']) . "', payment_postcode = '" . $this->db->escape($data['payment_postcode']) . "', payment_country = '" . $this->db->escape($data['payment_country']) . "', payment_country_id = '" . (int)$data['payment_country_id'] . "', payment_zone = '" . $this->db->escape($data['payment_zone']) . "', payment_zone_id = '" . (int)$data['payment_zone_id'] . "', payment_address_format = '" . $this->db->escape($data['payment_address_format']) . "', payment_custom_field = '" . $this->db->escape(isset($data['payment_custom_field']) ? json_encode($data['payment_custom_field']) : '') . "', payment_method = '" . $this->db->escape($data['payment_method']) . "', payment_code = '" . $this->db->escape($data['payment_code']) . "', shipping_firstname = '" . $this->db->escape($data['shipping_firstname']) . "', shipping_lastname = '" . $this->db->escape($data['shipping_lastname']) . "', shipping_company = '" . $this->db->escape($data['shipping_company']) . "', shipping_address_1 = '" . $this->db->escape($data['shipping_address_1']) . "', shipping_address_2 = '" . $this->db->escape($data['shipping_address_2']) . "', shipping_city = '" . $this->db->escape($data['shipping_city']) . "', shipping_postcode = '" . $this->db->escape($data['shipping_postcode']) . "', shipping_country = '" . $this->db->escape($data['shipping_country']) . "', shipping_country_id = '" . (int)$data['shipping_country_id'] . "', shipping_zone = '" . $this->db->escape($data['shipping_zone']) . "', shipping_zone_id = '" . (int)$data['shipping_zone_id'] . "', shipping_address_format = '" . $this->db->escape($data['shipping_address_format']) . "', shipping_custom_field = '" . $this->db->escape(isset($data['shipping_custom_field']) ? json_encode($data['shipping_custom_field']) : '') . "', shipping_method = '" . $this->db->escape($data['shipping_method']) . "', shipping_code = '" . $this->db->escape($data['shipping_code']) . "', comment = '" . $this->db->escape($data['comment']) . "', total = '" . (float)$data['total'] . "', affiliate_id = '" . (int)$data['affiliate_id'] . "', commission = '" . (float)$data['commission'] . "', marketing_id = '" . (int)$data['marketing_id'] . "', tracking = '" . $this->db->escape($data['tracking']) . "', language_id = '" . (int)$data['language_id'] . "', currency_id = '" . (int)$data['currency_id'] . "', currency_code = '" . $this->db->escape($data['currency_code']) . "', currency_value = '" . (float)$data['currency_value'] . "', ip = '" . $this->db->escape($data['ip']) . "', forwarded_ip = '" .  $this->db->escape($data['forwarded_ip']) . "', user_agent = '" . $this->db->escape($data['user_agent']) . "', accept_language = '" . $this->db->escape($data['accept_language']) . "', date_added = NOW(), date_modified = NOW()");

		$order_id = $this->db->getLastId();

		// Products
		if (isset($data['products'])) {
			foreach ($data['products'] as $product) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "order_product SET order_id = '" . (int)$order_id . "', product_id = '" . (int)$product['product_id'] . "', name = '" . $this->db->escape($product['name']) . "', model = '" . $this->db->escape($product['model']) . "', quantity = '" . (int)$product['quantity'] . "', price = '" . (float)$product['price'] . "', total = '" . (float)$product['total'] . "', tax = '" . (float)$product['tax'] . "', reward = '" . (int)$product['reward'] . "'");

				$order_product_id = $this->db->getLastId();

				foreach ($product['option'] as $option) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "order_option SET order_id = '" . (int)$order_id . "', order_product_id = '" . (int)$order_product_id . "', product_option_id = '" . (int)$option['product_option_id'] . "', product_option_value_id = '" . (int)$option['product_option_value_id'] . "', name = '" . $this->db->escape($option['name']) . "', `value` = '" . $this->db->escape($option['value']) . "', `type` = '" . $this->db->escape($option['type']) . "'");
				}
			}
		}

		// Totals
		if (isset($data['totals'])) {
			foreach ($data['totals'] as $total) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "order_total SET order_id = '" . (int)$order_id . "', code = '" . $this->db->escape($total['code']) . "', title = '" . $this->db->escape($total['title']) . "', `value` = '" . (float)$total['value'] . "', sort_order = '" . (int)$total['sort_order'] . "'");
			}
		}

		return $order_id;
	}

	public function getProduct($client, $product_id){
		$send = $client->getProduct($product_id);
		return $send;
	}
}
?>