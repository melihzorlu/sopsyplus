<?php
include_once(DIR_CATALOG . 'controller/extension/payment/sanalpospro/lib/class/inc.php');

ini_set("display_errors", "on");
error_reporting(E_ALL);
class ControllerExtensionPaymentSanalpospro extends Controller
{

	private $error = array();
	private $version = 2.0;

	public function install()
	{
		$this->load->model('setting/setting');
		Eticconfig::set('payment_sanalpospro_status', true);
		Eticconfig::set('sanalpospro_order_status_id', 2);
		Eticconfig::set('POSPRO_API_DOMAIN', '');
		Eticconfig::set('POSPRO_API_EMAIL', '');
		Eticconfig::set('POSPRO_API_PUBLIC', '');
		Eticconfig::set('POSPRO_API_PRIVATE', '');
		Eticconfig::set('POSPRO_DEBUG_MOD', 'on');
		Eticconfig::set('POSPRO_TERMS', '');
		Eticconfig::set('POSPRO_SHOW_PRO_INST', 'on');
		Eticconfig::set('POSPRO_IOS', 2);
		Eticconfig::set('POSPRO_ORDER_AUTOFORM', 'on');
		Eticconfig::set('POSPRO_PRODUCT_TMP', 'color');
		Eticconfig::set('POSPRO_MAX_INSTALLMENTS', '9');
		Eticconfig::set('POSPRO_MIN_INST_AMOUNT', '1.00');
		Eticconfig::set('POSPRO_PAYMENT_PAGE', 'pro');
		Eticconfig::set('POSPRO_LATEST_VERSION', SanalPosApiClient::$version);
		Eticconfig::set('POSPRO_AUTO_CURRENCY', "on");
		Eticconfig::set('MASTERPASS_STAGE', "TEST");

		$query1 = "
		CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "spr_gateway` (
		`name` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
		`method` enum('cc', 'wire', 'other') NOT NULL,
		`full_name` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
		`lib` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
		`active` tinyint(1) NOT NULL DEFAULT '1',
		`params` text COLLATE utf8_unicode_ci NOT NULL,
		UNIQUE KEY (`name`)
	  ) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
		$query2 = "
		CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "spr_installment` (
		`gateway` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
		`family` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
		`divisor` int(3) NOT NULL,
		`rate` decimal(4,2) NOT NULL,
		`fee` decimal(4,2) NOT NULL DEFAULT '0.00',
		UNIQUE KEY `gateway` (`family`,`divisor`)
	  ) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";

		$query3 = "
		CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "spr_transaction` (
		  `id_transaction` int(11) NOT NULL AUTO_INCREMENT,
		  `notify` tinyint(1) NOT NULL DEFAULT '0',
		  `cc_name` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
		  `cc_number` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
		  `gateway` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
		  `family` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
		  `id_cart` int(11) NOT NULL,
		  `id_currency` int(11) NOT NULL,
		  `id_order` int(11) DEFAULT NULL,
		  `id_customer` int(11) NOT NULL,
		  `total_cart` float(10,2) DEFAULT NULL,
		  `total_pay` float(10,2) DEFAULT NULL,
		  `gateway_fee` float(10,2) DEFAULT NULL,
		  `installment` int(2) NULL,
		  `cip` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
		  `test_mode` int(1) NOT NULL,
		  `tds` int(1) NOT NULL,
		  `boid` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
		  `result_code` varchar(24) COLLATE utf8_unicode_ci DEFAULT NULL,
		  `result_message` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
		  `result` int(1) NOT NULL,
		  `detail` TEXT DEFAULT NULL,
		  `date_create` datetime NOT NULL,
		  `date_update` datetime DEFAULT NULL,
		  PRIMARY KEY (`id_transaction`)
		) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;"
		;

		$query4 = "
		CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "spr_debug` (
		  `id_transaction` int(11) NOT NULL,
		  `debug` text NULL,
		  UNIQUE KEY `id_transaction` (`id_transaction`)
		) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";

		if (!EticSql::execute($query1)
			OR ! EticSql::execute($query2)
			OR ! EticSql::execute($query3)
			OR ! EticSql::execute($query4)
		) {
			$this->_errors [] = 'Error while creating MYSQL table(s)';
			return false;
		}
		Eticconfig::set('POSPRO_INSTALLED_VERSION', SanalPosApiClient::$version);
		return true;
	}

	public function index()
	{
		$this->load->language('payment/sanalpospro');

		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('setting/setting');
		$this->document->addScript(HTTPS_CATALOG . 'catalog/view/javascript/sanalpospro/admin.js');
		$this->document->addStyle(HTTPS_CATALOG . 'catalog/view/theme/default/stylesheet/sanalpospro/admin.css');

		$data['terms'] = EticConfig::get('POSPRO_TERMS');
		$error_message = false;
		$this->load->model('localisation/order_status');
		$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();
		if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
			$data['base'] = $this->config->get('config_ssl');
		} else {
			$data['base'] = $this->config->get('config_url');
		}

		$data['text_edit'] = 'SanalPOS modülü ayarları';
		$data['heading_title'] = 'Paynet SanalPOS modülü ayarları';
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		$data['breadcrumbs'] = array();
		$data['header'] = $this->load->controller('common/header');

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('SanalPOS'),
			'href' => $this->url->link('extension/payment', 'token=' . $this->session->data['token'], true)
		);
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('SanalPOS'),
			'href' => $this->url->link('extension/payment/sanalpospro', 'token=' . $this->session->data['token'], true)
		);
		$data['action'] = $this->url->link('extension/payment/sanalpospro', 'token=' . $this->session->data['token'], true);
		$data['cancel'] = $this->url->link('extension/payment', 'token=' . $this->session->data['token'], true);

		if (Etictools::getValue('spr_terms')) {
			if (!EticTools::isMobile(EticTools::getValue('spr_shop_phone'))) {
				$data['error_message'] = 'Girdiğiniz cep telefonu (' . EticTools::getValue('spr_shop_phone') . ') doğru değil ! Doğru örnek 0505XXXYYZZ.
				<br/> Not:<b> Cep telefonu numaranız kesinlikle spam/rahatsız edici sms amacıyla kullanılmayacaktır </b> ';
				return $this->response->setOutput($this->load->view('extension/payment/sanalpospro', $data));
			}

			$spapi = New SanalPosApiClient(0);
			$data = $spapi->getRegisterVariables();
			$request = Etictools::curlPostExt(
					array('data' => json_encode($data, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES)), $spapi->getLoginFormValues()['url'] . 'register.php', false);
			if (!$result = json_decode(Etictools::removeBOM($request)))
				$error_message = 'Veri alışverişi sırasında hata oldu. Lütfen Tekrar Deneyin veya EticSoft ile görüşünüz'
					. '<small>(' . $request . ')</small>';
			else if ($result->result) {
				EticConfig::set('POSPRO_TERMS', true);
				EticConfig::set('POSPRO_API_PUBLIC', $result->public);
				EticConfig::set('POSPRO_API_PRIVATE', $result->private);
				EticConfig::set('POSPRO_API_DOMAIN', $result->domain);
				EticConfig::set('POSPRO_API_EMAIL', $data['email']);
				$this->response->redirect($this->url->link('extension/payment/sanalpospro', 'token=' . $this->session->data['token'], true));
			} else
				$error_message = $result->result_message;
		}
		if (!EticConfig::get('POSPRO_API_PUBLIC') OR ! EticConfig::get('POSPRO_TERMS')) {
			$data['error_message'] = $error_message;
			return $this->response->setOutput($this->load->view('extension/payment/sanalpospro', $data));
		}

		$definitions = $this->sppGetStoreMethods();

		if ($definitions->result != 1 OR $definitions->result_message != 'success') {
			$data['error_message'] = $definitions->result_message . ' Hata ' . $definitions->result_code;
			return $this->response->setOutput($this->load->view('extension/payment/sanalpospro', $data));
		}
		EticGateway::$gateways = $definitions->data->gateways;
		EticGateway::$api_libs = $definitions->data->api_libs;
		$this->sppSaveSettings();


		$data['spr_url'] = HTTPS_CATALOG . 'catalog/view/theme/default/';
		$data['spr_ver'] = SanalPosApiClient::$version;
		$data['viewlog'] = (Etictools::getValue('id_transaction') ? $this->getDebug(Etictools::getValue('id_transaction')) : false);
		$data['general_tab'] = EticConfig::getAdminGeneralSettingsForm($this);
		$data['banks_tab'] = EticConfig::getAdminGatewaySettingsForm(HTTPS_CATALOG);
		$data['integration_tab'] = EticConfig::getAdminIntegrationForm(HTTPS_CATALOG);
		$data['cards_tab'] = EticConfig::getCardSettingsForm(HTTPS_CATALOG);
		$data['tools_tab'] = EticConfig::getAdminToolsForm(HTTPS_CATALOG);
		$data['help_tab'] = EticConfig::getHelpForm(HTTPS_CATALOG);
		$data['campaign_tab'] = EticConfig::getCampaigns(HTTPS_CATALOG);
		$data['masterpass_tab'] = EticConfig::getMasterPassForm(HTTPS_CATALOG);
		$data['last_records'] = $this->getLastRecordsTable();
		$data['stats_gateways'] = EticStats::getChart('getGwUsagebyTotal');
		$data['stats_monthly'] = EticStats::getChart('getMontlyIncome');
		$data['module_dir'] = HTTPS_CATALOG;
		$data['messages'] = EticConfig::$messages;
		$data['key'] = EticTools::GenerateKey($this->id);



		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		if ($this->config->get('payment_sanalpospro_data_key') == null)
			$data['error_warning'] .= 'payment_sanalpospro Data Key Alanı Boş<br/>';

		if ($this->config->get('payment_sanalpospro_secret_key') == null)
			$data['error_warning'] .= 'payment_sanalpospro Secret Key Alanı Boş<br/>';

		if (isset($this->request->post['payment_sanalpospro_agent_code'])) {
			$data['payment_sanalpospro_agent_code'] = $this->request->post['payment_sanalpospro_agent_code'];
		} else {
			$data['payment_sanalpospro_agent_code'] = $this->config->get('payment_sanalpospro_agent_code');
		}
		if (isset($this->request->post['payment_sanalpospro_data_key'])) {
			$data['payment_sanalpospro_data_key'] = $this->request->post['payment_sanalpospro_data_key'];
		} else {
			$data['payment_sanalpospro_data_key'] = $this->config->get('payment_sanalpospro_data_key');
		}
		if (isset($this->request->post['payment_sanalpospro_secret_key'])) {
			$data['payment_sanalpospro_secret_key'] = $this->request->post['payment_sanalpospro_secret_key'];
		} else {
			$data['payment_sanalpospro_secret_key'] = $this->config->get('payment_sanalpospro_secret_key');
		}

		if (isset($this->request->post['payment_sanalpospro_ins_fee'])) {
			$data['payment_sanalpospro_ins_fee'] = $this->request->post['payment_sanalpospro_ins_fee'];
		} else {
			$data['payment_sanalpospro_ins_fee'] = $this->config->get('payment_sanalpospro_ins_fee');
		}

		if (isset($this->request->post['payment_sanalpospro_ins_fee'])) {
			$data['payment_sanalpospro_ins_fee'] = $this->request->post['payment_sanalpospro_ins_fee'];
		} else {
			$data['payment_sanalpospro_ins_fee'] = $this->config->get('payment_sanalpospro_ins_fee');
		}

		if (isset($this->request->post['payment_sanalpospro_force_tds'])) {
			$data['payment_sanalpospro_force_tds'] = $this->request->post['payment_sanalpospro_force_tds'];
		} else {
			$data['payment_sanalpospro_force_tds'] = $this->config->get('payment_sanalpospro_force_tds');
		}

		if (isset($this->request->post['payment_sanalpospro_test_mode'])) {
			$data['payment_sanalpospro_test_mode'] = $this->request->post['payment_sanalpospro_test_mode'];
		} else {
			$data['payment_sanalpospro_test_mode'] = $this->config->get('payment_sanalpospro_test_mode');
		}

		if (isset($this->request->post['payment_sanalpospro_status'])) {
			$data['payment_sanalpospro_status'] = $this->request->post['payment_sanalpospro_status'];
			if ($this->request->post['payment_sanalpospro_status']) {
				$this->model_setting_setting->editSetting('payment_sanalpospro', array('payment_sanalpospro_status' => 1));
			}
		} else {
			$data['payment_sanalpospro_status'] = $this->config->get('payment_sanalpospro_status');
		}
		if (isset($this->request->post['payment_sanalpospro_order_status_id'])) {
			$data['payment_sanalpospro_order_status_id'] = $this->request->post['payment_sanalpospro_order_status_id'];
		} else {
			$data['payment_sanalpospro_order_status_id'] = $this->config->get('payment_sanalpospro_order_status_id');
		}


		$this->response->setOutput($this->load->view('extension/payment/sanalpospro', $data));
	}

	public function order()
	{
		$tr = EticTransaction::getTransactionByOrderId($this->request->get['order_id']);
		if(!$tr)
			return;
		$ui = new EticUiOpencart();
		return $ui->displayAdminOrder((object)$tr);

		$order_comments = $this->model_sale_order->getOrderHistories($this->request->get['order_id']);
		if (!$order_comments OR empty($order_comments))
			return false;
		$x_id = false;

		foreach ($order_comments as $oc) {
			if (!isset($oc['comment']) OR ! $oc['comment']) {
				continue;
			}
			$decode = json_decode($oc['comment']);
			if (!$decode OR ! isset($decode->Paynet_ID))
				continue;
			$x_id = $decode->Paynet_ID;
			break;
		}
		if ($x_id)
			$transaction = $this->getTransaction($x_id);
		if ((int) $transaction->code != 0)
			return $transaction->code . ' ' . $transaction->message;
		$tr = $transaction->Data[0];
		return '<table class="table table-bordered">
		<tr>
			<td>Ödenen Tutar
			</td><td>' . $tr->amount . ' ' . $tr->currency . '</td>
		</tr>
		<tr>
			<td>Net Tutar</td><td>' . $tr->netAmount . ' ' . $tr->currency . '</td>
		</tr>
		<tr>
			<td>Komisyon Oranı </td><td> &percnt; ' . ($tr->ratio * 100) . ' ' . $tr->payment_string . '</td>
		</tr>
		<tr>
			<td>Tarih </td><td>' . $tr->xact_date . ' ' . $tr->ipaddress . '</td>
		</tr>
		<tr>
			<td>Sonuç</td><td>' . $tr->message . '</td>
		</tr>
		<tr>
			<td>Kredi Kartı</td><td>' . $tr->card_no . '
			<br/>' . $tr->card_holder . '
			<br/>' . $tr->card_type . ' ' . $tr->bank_id . '</td>
		</tr>
		</table>';
		return "Transactin ID not found";
	}

	protected function validate()
	{

		if (!$this->user->hasPermission('modify', 'payment/sanalpospro')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return true;

		$this->load->model('localisation/language');

		$languages = $this->model_localisation_language->getLanguages();

		foreach ($languages as $language) {
			if (empty($this->request->post['payment_sanalpospro_bank' . $language['language_id']])) {
				$this->error['bank' . $language['language_id']] = $this->language->get('error_bank');
			}
		}

		return !$this->error;
	}

	function getLastRecordsTable()
	{
		return false;
	}

	private function sppGetStoreMethods()
	{
		$cli = New SanalPosApiClient(1, 'UNKNOWN', 'getstoregateways2');
		return $cli->validateRequest()
				->run()
				->getResponse();
	}

	// FORM GET AND UPDATE/INSERT DB
	function sppSaveSettings()
	{
		if (EticTools::getValue('add_new_pos')) {
			$gateway = New EticGateway(EticTools::getValue('add_new_pos'));
			$gateway->add();
		}
		if (Etictools::getValue('savetoolsform') OR Etictools::getValue('check-oldtables')) {
			EticConfig::saveToolsForm();
		}
		if (Etictools::getValue('submitcardrates')) {
			EticConfig::saveCardSettingsForm();
		}
		if (Etictools::getValue('submitgwsetting')) {
			EticConfig::saveGatewaySettings();
		}
		if (EticTools::getValue('conf-form') && EticTools::getValue('conf-form') == 1) {
			EticConfig::saveGeneralSettings();
			EticTools::rwm('Genel Ayarlar Güncellendi', true, 'success');
		}
	}
}
