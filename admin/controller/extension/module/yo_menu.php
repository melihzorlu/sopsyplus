<?php
class ControllerExtensionModuleYoMenu extends Controller {
	private $error = array();

	public function index() {
		$lang  = $this->load->language('extension/module/yo_menu');

		foreach ($lang as $key => $value) {
			$data[$key] = $value;
		}

		$this->document->setTitle($this->language->get('module_title'));
		$this->document->addStyle('view/stylesheet/yo_menu/yo_menu.css');

		$this->load->model('setting/module');

		if (!isset($this->request->get['module_id'])) {
			$data['apply_btn'] = false;
		} else {
			$data['apply_btn'] = true;
		}

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {

			if (!isset($this->request->get['module_id'])) {
				$this->model_setting_module->addModule('yo_menu', $this->request->post);
			} else {
				$this->model_setting_module->editModule($this->request->get['module_id'], $this->request->post);
			}

			if (isset($this->request->post['apply']) && $this->request->post['apply'] == '1') {
				$this->session->data['success'] = $this->language->get('text_apply');

					$this->response->redirect($this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=module', true));
			}

			$this->session->data['success'] = $this->language->get('text_success');
			$this->response->redirect($this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=module', true));
		}

		$data['heading_title'] = $this->language->get('module_title');
		$data['default_store'] = $this->config->get('config_name');

		$data['version'] = '1.2';
		$data['text_author'] = $this->language->get('text_author');
		$data['text_author_link'] = $this->language->get('text_author_link');
		$data['text_support'] = $this->language->get('text_support');
		$data['text_more'] = $this->language->get('text_more');

		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');

		$data['text_expanded'] = $this->language->get('text_expanded');
		$data['text_minimized'] = $this->language->get('text_minimized');
		$data['text_save_view'] = $this->language->get('text_save_view');

		$data['text_all_categories'] = $this->language->get('text_all_categories');
		$data['text_all_customers'] = $this->language->get('text_all_customers');
		$data['text_current_categories'] = $this->language->get('text_current_categories');
		$data['text_cat_autocomplete'] = $this->language->get('text_cat_autocomplete');
		$data['text_all_brands'] = $this->language->get('text_all_brands');
		$data['text_current_brands'] = $this->language->get('text_current_brands');
		$data['text_count'] = $this->language->get('text_count');

		$data['text_tree'] = $this->language->get('text_tree');
		$data['text_brands'] = $this->language->get('text_brands');
		$data['text_current'] = $this->language->get('text_current');
		$data['text_parent'] = $this->language->get('text_parent');

		$data['text_level_1'] = $this->language->get('text_level_1');
		$data['text_level_2'] = $this->language->get('text_level_2');
		$data['text_level_3'] = $this->language->get('text_level_3');
		$data['text_level_4'] = $this->language->get('text_level_4');
		$data['text_level_5'] = $this->language->get('text_level_5');

		$data['text_am'] = $this->language->get('text_am');
		$data['text_pm'] = $this->language->get('text_pm');
		$data['text_fm'] = $this->language->get('text_fm');

		$data['text_center'] = $this->language->get('text_center');
		$data['text_left'] = $this->language->get('text_left');

		$data['text_one_column'] = $this->language->get('text_one_column');
		$data['text_multi_column'] = $this->language->get('text_multi_column');

		$data['text_toggle'] = $this->language->get('text_toggle');
		$data['text_icons_status'] = $this->language->get('text_icons_status');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_store'] = $this->language->get('entry_store');
		$data['entry_customers'] = $this->language->get('entry_customers');
		$data['entry_class'] = $this->language->get('entry_class');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');

		$data['entry_location'] = $this->language->get('entry_location');
		$data['entry_menu_type'] = $this->language->get('entry_menu_type');
		$data['entry_menu_items'] = $this->language->get('entry_menu_items');
		$data['entry_prod_by_cat'] = $this->language->get('entry_prod_by_cat');
		$data['entry_prod_by_brand'] = $this->language->get('entry_prod_by_brand');
		$data['entry_products_limit'] = $this->language->get('entry_products_limit');
		$data['entry_levels'] = $this->language->get('entry_levels');
		$data['entry_sub_limit'] = $this->language->get('entry_sub_limit');

		$data['entry_menu_design'] = $this->language->get('entry_menu_design');
		$data['entry_minimized'] = $this->language->get('entry_minimized');
		$data['entry_title'] = $this->language->get('entry_title');
		$data['entry_categories'] = $this->language->get('entry_categories');
		$data['entry_brands'] = $this->language->get('entry_brands');
		$data['entry_item_info'] = $this->language->get('entry_item_info');
		$data['entry_info_limit'] = $this->language->get('entry_info_limit');
		$data['entry_image_position'] = $this->language->get('entry_image_position');
		$data['entry_flyout_column'] = $this->language->get('entry_flyout_column');
		$data['entry_item_column'] = $this->language->get('entry_item_column');
		$data['entry_banner'] = $this->language->get('entry_banner');
		$data['entry_image_status'] = $this->language->get('entry_image_status');
		$data['entry_size'] = $this->language->get('entry_size');
		$data['entry_effect'] = $this->language->get('entry_effect');

		$data['button_apply'] = $this->language->get('button_apply');
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_add_module'] = $this->language->get('button_add_module');

		$data['tab_module'] = $this->language->get('tab_module');
		$data['tab_menu_setting'] = $this->language->get('tab_menu_setting');
		$data['tab_module_setting'] = $this->language->get('tab_module_setting');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=module', true)
		);

		if (!isset($this->request->get['module_id'])) {
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('extension/module/yo_menu', 'token=' . $this->session->data['token'], true)
			);
		} else {
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('extension/module/yo_menu', 'token=' . $this->session->data['token'] . '&module_id=' . $this->request->get['module_id'], true)
			);
		}

		if (!isset($this->request->get['module_id'])) {
			$data['action'] = $this->url->link('extension/module/yo_menu', 'token=' . $this->session->data['token'], true);
		} else {
			$data['action'] = $this->url->link('extension/module/yo_menu', 'token=' . $this->session->data['token'] . '&module_id=' . $this->request->get['module_id'], true);
		}

		$data['cancel'] = $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=module', true);

		if (isset($this->request->get['module_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$module_info = $this->model_setting_module->getModule($this->request->get['module_id']);
		}

		//$data['token'] = $this->session->data['token'];

		if (isset($this->request->post['name'])) {
			$data['name'] = $this->request->post['name'];
		} elseif (!empty($module_info)) {
			$data['name'] = $module_info['name'];
		} else {
			$data['name'] = '';
		}

		if (isset($this->request->post['status'])) {
			$data['status'] = $this->request->post['status'];
		} elseif (!empty($module_info)) {
			$data['status'] = $module_info['status'];
		} else {
			$data['status'] = 1;
		}

		if (isset($this->request->post['yomenu'])) {
			$data['yomenu'] = $this->request->post['yomenu'];
		} elseif (!empty($module_info)) {
			$data['yomenu'] = $module_info['yomenu'];
		} else {
			$data['yomenu'] = array();
			$data['yomenu']['store_id'][] = 0;
			$data['yomenu']['all_customers'] = 1;
			$data['yomenu']['all_categories'] = 1;
			$data['yomenu']['location'] = 1;
			$data['yomenu']['level_2'] = 1;
		}

		$this->load->model('localisation/language');
		$data['languages'] = $this->model_localisation_language->getLanguages();

		$this->load->model('setting/store');
		$data['stores'] = $this->model_setting_store->getStores();

		$this->load->model('catalog/category');
		$data['categories'] = array();

		if (!empty($this->request->post['yomenu']['featured_category'])) {
			$categories = $this->request->post['yomenu']['featured_category'];
		} elseif (!empty($module_info) && !empty($module_info['yomenu']['featured_category'])) {
			$categories = $module_info['yomenu']['featured_category'];
		} else {
			$categories = array();
		}

		foreach ($categories as $category_id) {
			$category_info = $this->model_catalog_category->getCategory($category_id);

			if ($category_info) {
				$data['categories'][] = array(
					'category_id' => $category_info['category_id'],
					'name'        => $category_info['path'] ? $category_info['path'] . ' - &gt; ' . $category_info['name'] : $category_info['name']
					);
			}
		}

		$data['locations'] = array();

		if (!empty($this->request->post['yomenu']['fcid'])) {
			$locations = $this->request->post['yomenu']['fcid'];
		} elseif (!empty($module_info) && !empty($module_info['yomenu']['fcid'])) {
			$locations = $module_info['yomenu']['fcid'];
		} else {
			$locations = array();
		}

		foreach ($locations as $location) {
			$category_info = $this->model_catalog_category->getCategory($location);

			if ($category_info) {
				$data['locations'][] = array(
					'category_id' => $category_info['category_id'],
					'name'        => $category_info['path'] ? $category_info['path'] . ' - &gt; ' . $category_info['name'] : $category_info['name']
					);
			}
		}

		$this->load->model('catalog/manufacturer');
		$data['manufacturers'] = array();

		if (!empty($this->request->post['yomenu']['featured_brands'])) {
			$manufacturers = $this->request->post['yomenu']['featured_brands'];
		} elseif (!empty($module_info) && !empty($module_info['yomenu']['featured_brands'])) {
			$manufacturers = $module_info['yomenu']['featured_brands'];
		} else {
			$manufacturers = array();
		}

		foreach ($manufacturers as $brand_id) {
			$brand_info = $this->model_catalog_manufacturer->getManufacturer($brand_id);

			if ($brand_info) {
				$data['manufacturers'][] = array(
					'manufacturer_id' => $brand_info['manufacturer_id'],
					'name'            => $brand_info['name']
					);
			}
		}

		$this->load->model('yomenu/yo_menu');
		$data['customer_groups'] = $this->model_yomenu_yo_menu->getCustomerGroups();

		$data['sorts'] = array();

		$data['sorts'][] = array(
			'name'  => $this->language->get('text_default_asc'),
			'value' => 'p.sort_order-ASC'
			);

		$data['sorts'][] = array(
			'name'  => $this->language->get('text_viewed_asc'),
			'value' => 'p.viewed-DESC'
			);

		if ($this->config->get('config_review_status')) {
			$data['sorts'][] = array(
				'name'  => $this->language->get('text_rating_desc'),
				'value' => 'rating-DESC'
				);
		}

		$data['sorts'][] = array(
			'name'  => $this->language->get('text_date_desc'),
			'value' => 'p.date_added-DESC'
			);

		$data['sorts'][] = array(
			'name'  => $this->language->get('text_name_asc'),
			'value' => 'pd.name-ASC'
			);

		$data['sorts'][] = array(
			'name'  => $this->language->get('text_name_desc'),
			'value' => 'pd.name-DESC'
			);

		$data['sorts'][] = array(
			'name'  => $this->language->get('text_price_asc'),
			'value' => 'p.price-ASC'
			);

		$data['sorts'][] = array(
			'name'  => $this->language->get('text_price_desc'),
			'value' => 'p.price-DESC'
			);

		$data['easings'] = array('linear', 'swing', 'easeInQuad', 'easeOutQuad', 'easeInOutQuad', 'easeInCubic', 'easeOutCubic', 'easeInOutCubic', 'easeInQuart', 'easeOutQuart', 'easeInOutQuart', 'easeInQuint', 'easeOutQuint', 'easeInOutQuint', 'easeInExpo', 'easeOutExpo', 'easeInOutExpo', 'easeInSine', 'easeOutSine', 'easeInOutSine', 'easeInCirc', 'easeOutCirc', 'easeInOutCirc', 'easeInElastic', 'easeOutElastic', 'easeInOutElastic', 'easeInBack', 'easeOutBack', 'easeInOutBack', 'easeInBounce', 'easeOutBounce', 'easeInOutBounce');

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/module/yo_menu', $data));
	}

	public function install() {
		$this->db->query("ALTER TABLE `" . DB_PREFIX . "category` ADD `yomenu_image` varchar(255) NULL default '' AFTER image, ADD `yomenu_icon` varchar(255) NULL default '' AFTER yomenu_image;");
		$this->db->query("ALTER TABLE `" . DB_PREFIX . "manufacturer` ADD `ym_banner` varchar(255) NULL default '' AFTER image;");
	}

	public function uninstall () {
		$this->db->query("ALTER TABLE `" . DB_PREFIX . "category` DROP `yomenu_image`, DROP `yomenu_icon`;");
		$this->db->query("ALTER TABLE `" . DB_PREFIX . "manufacturer` DROP `ym_banner`;");
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/module/yo_menu')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 64)) {
			$this->error['name'] = $this->language->get('error_name');
		}

		return !$this->error;
	}
}
