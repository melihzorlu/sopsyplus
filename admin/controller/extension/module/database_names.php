<?php
class ControllerExtensionModuleDatabaseNames extends Controller {
    public function index(){

        $this->document->setTitle('Veritabanı Listesi');

        $this->getList();
    }

    public function getList() {
        $data = array();

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');
       $this->response->setOutput($this->load->view('extension/module/database_list', $data));

    }
}