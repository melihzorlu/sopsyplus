<?php
class ControllerExtensionModules extends Controller {
	private $error = array();

	public function index() {

    $data['token'] = $this->session->data['token'];

    $this->document->setTitle('Modüller');

    $data['heading_title'] = 'Modüller';
    $data['odeme_yontemleri'] = 'Ödeme Yöntemleri';
    $data['kargo_islemleri'] = 'Kargo İşlemleri';
    $data['kampanya_yonetimi'] = 'Kampanya Yönetimi';
    $data['analitik_araclari'] = 'Analitik Yönetimi';
    $data['toplu_urun'] = 'Toplu Ürün Yükleme';
    $data['diger_moduller'] = 'Diğer Modüller';
    $data['fatura_modulleri'] = 'Fatura Modülleri';
    $data['muhasebe_moduller'] = 'Muhasebe Modülleri';
    $data['breadcrumbs'][0]['href'] = $this->url->link('extension/modules', 'token=' . $this->session->data['token'], true);
    $data['breadcrumbs'][0]['text'] = 'Modüller';


		$data['odeme_modulleri']=array();

		$data['odeme_modulleri']['paratika']=array(
			'name' => 'Paratika',
			'image' => 'logolar/paratika.jpg',
			'href' => $this->url->link('extension/payment/paytr_checkout', 'token=' . $this->session->data['token'], true)
		);
		$data['odeme_modulleri']['paytr']=array(
			'name' => 'PayTR',
			'image' => 'logolar/paytr.jpg',
			'href' => $this->url->link('extension/payment/paytr_checkout', 'token=' . $this->session->data['token'], true)
		);
		$data['odeme_modulleri']['iyzico']=array(
			'name' => 'İyzico',
			'image' => 'logolar/iyzico.jpg',
			'href' => $this->url->link('extension/payment/iyzico', 'token=' . $this->session->data['token'], true)
		);
		/* $data['odeme_modulleri']['sipay']=array(
			'name' => 'Sipay',
			'image' => 'logolar/sipay.jpg',
			'href' => $this->url->link('extension/payment/paytr_checkout', 'token=' . $this->session->data['token'], true)
		); */

		$data['odeme_modulleri']['kapida_odeme']=array(
			'name' => 'Kapıda Ödeme',
			'image' => 'logolar/kapida_odeme.jpg',
			'href' => $this->url->link('extension/payment/cod', 'token=' . $this->session->data['token'], true)
		);

		$data['odeme_modulleri']['banka_havalesi']=array(
			'name' => 'Banka Havalesi',
			'image' => 'logolar/banka_havalesi.jpg',
			'href' => $this->url->link('extension/payment/bank_transfer', 'token=' . $this->session->data['token'], true)
		);



		$data['kargo_modulleri']['kargo_ucret_hesaplama']=array(
			'name' => 'Kargo Ücret Hesapalama',
			'image' => 'logolar/kargo_ucret_hesaplama.jpg',
			'href' => $this->url->link('extension/shipping/xshippingpro', 'token=' . $this->session->data['token'], true)
		);

		/*$data['kargo_modulleri']['yurtici']=array(
			'name' => 'Yurtiçi Kargo Entegrasyonu',
			'image' => 'logolar/yurtici_kargo.jpg',
			'href' => $this->url->link('extension/payment/bank_transfer', 'token=' . $this->session->data['token'], true)
		);

		$data['kargo_modulleri']['ptt']=array(
			'name' => 'PTT Kargo Entegrasyonu',
			'image' => 'logolar/ptt_kargo.jpg',
			'href' => $this->url->link('extension/payment/bank_transfer', 'token=' . $this->session->data['token'], true)
		);*/


		$data['kampanya_modulleri']['olustur']=array(
			'name' => 'Kampanya Oluştur',
			'image' => 'logolar/kampanya_olusturma.jpg',
			'href' => $this->url->link('extension/module/promotion', 'token=' . $this->session->data['token'], true)
		);


		$data['google_modulleri']['google_analitics']=array(
			'name' => 'Google Analitik',
			'image' => 'logolar/google_analitics.jpg',
			'href' => $this->url->link('extension/analytics/google_analytics', 'token=' . $this->session->data['token'], true)
		);

		$data['google_modulleri']['facebook_business']=array(
			'name' => 'Facebook Business',
			'image' => 'logolar/facebook_business.jpg',
			'href' => $this->url->link('extension/module/facebook_business', 'token=' . $this->session->data['token'], true)
		);


		$data['toplu_urun_modulleri']['n11']=array(
			'name' => 'N11',
			'image' => 'logolar/n11.jpg',
			'href' => $this->url->link('extension/modules', 'token=' . $this->session->data['token'], true)
		);

		$data['toplu_urun_modulleri']['trendyol']=array(
			'name' => 'Trendyol',
			'image' => 'logolar/trendyol.jpg',
			'href' => $this->url->link('trendyol/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['toplu_urun_modulleri']['excell_xml']=array(
			'name' => 'Excell / XML',
			'image' => 'logolar/excell.jpg',
			'href' => $this->url->link('module/universal_import', 'token=' . $this->session->data['token'], true)
		);

		$data['diger']['sepet_hatirlatma'] = array(
			'name' => 'Sepet Hatırlatma',
			'image' => 'logolar/sepet-hatirlatma.jpg',
			'href' =>   $this->url->link('abandoned_carts/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['diger']['gelince_haber_ver'] = array(
			'name' => 'Gelince Haber Ver',
			'image' => 'logolar/product_reminder.jpg',
			'href' =>   $this->url->link('module/notifywhenavailable', 'token=' . $this->session->data['token'], true)
		);

        $data['muhasebe']['nebim']= array(
            'name' => 'Nebim V3 Muhasebe Entegrasyonu',
            'image' => 'logolar/348893.png',
            'href' => ''
        );

        $data['muhasebe']['dia']= array(
            'name' => 'Dia Muhasebe Entegrasyonu',
            'image' => 'logolar/dia.png',
            'href' => ''
        );

        $data['fatura']['parasut'] = array(
            'name' => 'Paraşüt Entegrasyonu',
            'image' => 'logolar/parasut.png',
            'href' => ''
        );

        $data['fatura']['birfatura'] = array(
            'name' => 'Bir Fatura Entegrasyonu',
            'image' => 'logolar/birfatura.png',
            'href' => ''
        );

    $data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/modules', $data));

  }
}
