<?php
class ControllerCustomerPaylink extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('customer/paylink');
		$this->load->model('customer/paylink');


		//$this->document->setTitle($this->language->get('heading_title'));
	//
		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {

			$this->model_customer_paylink->addPaylink($this->request->post);

			$data['success'] = $this->language->get('text_success');

			$url = '';

		$this->response->redirect($this->url->link('customer/paylink', 'token=' . $this->session->data['token'] . $url, true));
		}
		$data['entry_firstname'] = $this->language->get('entry_firstname');
		$data['entry_lastname'] = $this->language->get('entry_lastname');
		$data['entry_email'] = $this->language->get('entry_email');
		$data['entry_telephone'] = $this->language->get('entry_telephone');
		$data['entry_identitynumber'] = $this->language->get('entry_identitynumber');
			$data['entry_price'] = $this->language->get('entry_price');
		$data['heading_title'] = $this->language->get('heading_title');
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		$data['action'] = $this->url->link('customer/paylink', 'token=' . $this->session->data['token'], true);
			$this->response->setOutput($this->load->view('customer/paylink', $data));
	}



	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'customer/paylink')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if ((utf8_strlen($this->request->post['pay_firstname']) < 1) || (utf8_strlen(trim($this->request->post['pay_firstname'])) > 32)) {
			$this->error['firstname'] = $this->language->get('error_firstname');
		}

		if ((utf8_strlen($this->request->post['pay_lastname']) < 1) || (utf8_strlen(trim($this->request->post['pay_lastname'])) > 32)) {
			$this->error['lastname'] = $this->language->get('error_lastname');
		}

		if ((utf8_strlen($this->request->post['pay_email']) > 96) || !filter_var($this->request->post['pay_email'], FILTER_VALIDATE_EMAIL)) {
			$this->error['email'] = $this->language->get('error_email');
		}



		if ((utf8_strlen($this->request->post['pay_telephone']) < 3) || (utf8_strlen($this->request->post['pay_telephone']) > 32)) {
			$this->error['telephone'] = $this->language->get('error_telephone');
		}

		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}

		return !$this->error;
	}




	}
