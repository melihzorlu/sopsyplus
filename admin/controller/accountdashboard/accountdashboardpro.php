<?php
class ControllerAccountdashboardAccountdashboardpro extends Controller {
	private $error = array();

	public function index() {

		$this->load->language('accountdashboard/accountdashboardpro');

		$this->document->setTitle($this->language->get('heading_title'));
		$this->document->addStyle('view/javascript/ciaccountdashboard/colorpicker/css/colorpicker.css');
 		$this->document->addScript('view/javascript/ciaccountdashboard/colorpicker/js/colorpicker.js');

		$this->load->model('setting/setting');

		if(isset($this->request->get['store_id'])) {
			$store_id = $this->request->get['store_id'];
		}else{
			$store_id = 0;
		}

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('accountdashboardpro', $this->request->post, $store_id);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('accountdashboard/accountdashboardpro', 'token=' . $this->session->data['token'] . '&type=module', true));

		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_upload'] = $this->language->get('text_upload');
		$data['text_yes'] = $this->language->get('text_yes');
		$data['text_no'] = $this->language->get('text_no');
		$data['text_first'] = $this->language->get('text_first');
		$data['text_default'] = $this->language->get('text_default');

		$data['text_type_setting'] = $this->language->get('text_type_setting');
		$data['text_type1'] = $this->language->get('text_type1');
		$data['text_type2'] = $this->language->get('text_type2');
		$data['text_third'] = $this->language->get('text_third');

		$data['text_coding_support'] = $this->language->get('text_coding_support');
		$data['text_codinginspect_email'] = $this->language->get('text_codinginspect_email');

		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_theme_color'] = $this->language->get('entry_theme_color');
		$data['entry_border_color'] = $this->language->get('entry_border_color');
		$data['entry_text_color'] = $this->language->get('entry_text_color');
		$data['entry_latest_order'] = $this->language->get('entry_latest_order');
		$data['entry_reward_points'] = $this->language->get('entry_reward_points');
		$data['entry_total_orders'] = $this->language->get('entry_total_orders');
		$data['entry_total_transactions'] = $this->language->get('entry_total_transactions');
		$data['entry_total_wishlist'] = $this->language->get('entry_total_wishlist');
		$data['entry_file_ext_allowed'] = $this->language->get('entry_file_ext_allowed');
		$data['entry_file_mime_allowed'] = $this->language->get('entry_file_mime_allowed');
		$data['entry_template'] = $this->language->get('entry_template');
		$data['entry_title'] = $this->language->get('entry_title');
		$data['entry_icontype'] = $this->language->get('entry_icontype');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_iconclass'] = $this->language->get('entry_iconclass');		
		$data['entry_option_value'] = $this->language->get('entry_option_value');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_link'] = $this->language->get('entry_link');
		$data['entry_action'] = $this->language->get('entry_action');		
		$data['entry_customcss'] = $this->language->get('entry_customcss');		

		$data['help_file_max_size'] = $this->language->get('help_file_max_size');
		$data['help_file_ext_allowed'] = $this->language->get('help_file_ext_allowed');
		$data['help_file_mime_allowed'] = $this->language->get('help_file_mime_allowed');
		$data['help_title'] = $this->language->get('help_title');
		$data['help_sort_order'] = $this->language->get('help_sort_order');
		$data['help_theme_color'] = $this->language->get('help_theme_color');

		$data['help_text_color'] = $this->language->get('help_text_color');
		$data['help_customcss'] = $this->language->get('help_customcss');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_add_link'] = $this->language->get('button_add_link');
		
		$data['tab_general'] = $this->language->get('tab_general');
		$data['tab_profile'] = $this->language->get('tab_profile');
		$data['tab_template'] = $this->language->get('tab_template');
		$data['tab_customer_link'] = $this->language->get('tab_customer_link');
		$data['tab_support'] = $this->language->get('tab_support');
		$data['tab_link'] = $this->language->get('tab_link');
		$data['tab_design'] = $this->language->get('tab_design');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['title'])) {
			$data['error_title'] = $this->error['title'];
		} else {
			$data['error_title'] = array();
		}

		if (isset($this->error['image'])) {
			$data['error_image'] = $this->error['image'];
		} else {
			$data['error_image'] = array();
		}

		if (isset($this->error['link'])) {
			$data['errorlink'] = $this->error['link'];
		} else {
			$data['errorlink'] = array();
		}
		if (isset($this->error['icon_class'])) {
			$data['error_icon_class'] = $this->error['icon_class'];
		} else {
			$data['error_icon_class'] = array();
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('accountdashboard/accountdashboardpro', 'token=' . $this->session->data['token'], true)
		);

		$data['store_id'] = $store_id;
		if(isset($store_id)) {
			$data['action'] = $this->url->link('accountdashboard/accountdashboardpro', 'token=' . $this->session->data['token'] . '&store_id='. $store_id, true);
		} else{
			$data['action'] = $this->url->link('accountdashboard/accountdashboardpro', 'token=' . $this->session->data['token'], true);
		}

		$this->load->model('setting/store');
		$data['stores'] = $this->model_setting_store->getStores();

		if ($this->request->server['REQUEST_METHOD'] != 'POST') {
			$module_info = $this->model_setting_setting->getSetting('accountdashboardpro',  $store_id);
		}

		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();

		if (isset($this->request->post['accountdashboardpro_status'])) {
			$data['accountdashboardpro_status'] = $this->request->post['accountdashboardpro_status'];
		} elseif (!empty($module_info)) {
			$data['accountdashboardpro_status'] = $module_info['accountdashboardpro_status'];
		} else {
			$data['accountdashboardpro_status'] = '';
		}

		if (isset($this->request->post['accountdashboardpro_customer_picture_status'])) {
			$data['accountdashboardpro_customer_picture_status'] = $this->request->post['accountdashboardpro_customer_picture_status'];
		} elseif (!empty($module_info)) {
			$data['accountdashboardpro_customer_picture_status'] = $module_info['accountdashboardpro_customer_picture_status'];
		} else {
			$data['accountdashboardpro_customer_picture_status'] = '';
		}

		if (isset($this->request->post['accountdashboardpro_latest_orders'])) {
			$data['accountdashboardpro_latest_orders'] = $this->request->post['accountdashboardpro_latest_orders'];
		} elseif (!empty($module_info)) {
			$data['accountdashboardpro_latest_orders'] = $module_info['accountdashboardpro_latest_orders'];
		} else {
			$data['accountdashboardpro_latest_orders'] = 1;
		}
		
		if (isset($this->request->post['accountdashboardpro_total_wishlists'])) {
			$data['accountdashboardpro_total_wishlists'] = $this->request->post['accountdashboardpro_total_wishlists'];
		} elseif (!empty($module_info)) {
			$data['accountdashboardpro_total_wishlists'] = $module_info['accountdashboardpro_total_wishlists'];
		} else {
			$data['accountdashboardpro_total_wishlists'] = 1;
		}

		if (isset($this->request->post['accountdashboardpro_total_rewardpoints'])) {
			$data['accountdashboardpro_total_rewardpoints'] = $this->request->post['accountdashboardpro_total_rewardpoints'];
		} elseif (!empty($module_info)) {
			$data['accountdashboardpro_total_rewardpoints'] = $module_info['accountdashboardpro_total_rewardpoints'];
		} else {
			$data['accountdashboardpro_total_rewardpoints'] = 1;
		}

		if (isset($this->request->post['accountdashboardpro_total_orders'])) {
			$data['accountdashboardpro_total_orders'] = $this->request->post['accountdashboardpro_total_orders'];
		} elseif (!empty($module_info)) {
			$data['accountdashboardpro_total_orders'] = $module_info['accountdashboardpro_total_orders'];
		} else {
			$data['accountdashboardpro_total_orders'] = 1;
		}
		if (isset($this->request->post['accountdashboardpro_total_transactions'])) {
			$data['accountdashboardpro_total_transactions'] = $this->request->post['accountdashboardpro_total_transactions'];
		} elseif (!empty($module_info)) {
			$data['accountdashboardpro_total_transactions'] = $module_info['accountdashboardpro_total_transactions'];
		} else {
			$data['accountdashboardpro_total_transactions'] = 1;
		}
		if (isset($this->request->post['accountdashboardpro_theme_color'])) {
			$data['accountdashboardpro_theme_color'] = $this->request->post['accountdashboardpro_theme_color'];
		} elseif (!empty($module_info)) {
			$data['accountdashboardpro_theme_color'] = $module_info['accountdashboardpro_theme_color'];
		} else {
			$data['accountdashboardpro_theme_color'] = '';
		}	

		if (isset($this->request->post['accountdashboardpro_text_color'])) {
			$data['accountdashboardpro_text_color'] = $this->request->post['accountdashboardpro_text_color'];
		} elseif (!empty($module_info)) {
			$data['accountdashboardpro_text_color'] = $module_info['accountdashboardpro_text_color'];
		} else {
			$data['accountdashboardpro_text_color'] = '';
		}

		if (isset($this->request->post['accountdashboardpro_customcss'])) {
			$data['accountdashboardpro_customcss'] = $this->request->post['accountdashboardpro_customcss'];
		} elseif (!empty($module_info)) {
			$data['accountdashboardpro_customcss'] = $module_info['accountdashboardpro_customcss'];
		} else {
			$data['accountdashboardpro_customcss'] = '';
		}		

		if (isset($this->request->post['accountdashboardpro_file_ext_allowed'])) {
			$data['accountdashboardpro_file_ext_allowed'] = $this->request->post['accountdashboardpro_file_ext_allowed'];
		} elseif (!empty($module_info)) {
			$data['accountdashboardpro_file_ext_allowed'] = $module_info['accountdashboardpro_file_ext_allowed'];
		} else {
			$data['accountdashboardpro_file_ext_allowed'] = "png\njpe\njpeg\njpg\ngif\nbmp";
		}

		if (isset($this->request->post['accountdashboardpro_file_mime_allowed'])) {
			$data['accountdashboardpro_file_mime_allowed'] = $this->request->post['accountdashboardpro_file_mime_allowed'];
		} elseif (!empty($module_info)) {
			$data['accountdashboardpro_file_mime_allowed'] = $module_info['accountdashboardpro_file_mime_allowed'];
		} else {
			$data['accountdashboardpro_file_mime_allowed'] = "image/png\nimage/jpeg\nimage/gif\nimage/bmp";
		}

		if (isset($this->request->post['accountdashboardpro_template'])) {
			$data['accountdashboardpro_template'] = $this->request->post['accountdashboardpro_template'];
		} elseif (!empty($module_info)) {
			$data['accountdashboardpro_template'] = $module_info['accountdashboardpro_template'];
		} else {
			$data['accountdashboardpro_template'] = '';
		}


		if (isset($this->request->post['accountdashboardpro_link'])) {
			$links = $this->request->post['accountdashboardpro_link'];
		} elseif (!empty($module_info)) {
			$links = (!empty($module_info['accountdashboardpro_link'])) ? (array)$module_info['accountdashboardpro_link'] : array();
		} else {
			$links = array();
		}

		function linksSort($a, $b) {
		    return $a['sort_order'] - $b['sort_order'];
		}


		uasort($links, 'linksSort');



		$this->load->model('tool/image');

		$data['links'] = array();
		
		foreach($links as $key =>  $link) {
			if (is_file(DIR_IMAGE . $link['image'])) {
				$thumb = $this->model_tool_image->resize($link['image'], 100, 100);
			} else {
				$thumb = $this->model_tool_image->resize('no_image.png', 100, 100);
			}

			$data['links'][$key] = array(
				'description'		=> isset($link['description']) ?  $link['description'] : array(),
				'link'				=> isset($link['link']) ?  $link['link'] : '',
				'status'			=> isset($link['status']) ?  $link['status'] : '',
				'type'				=> isset($link['type']) ?  $link['type'] : '',
				'icon_class'		=> isset($link['icon_class']) ?  $link['icon_class'] : '',
				'sort_order'		=> isset($link['sort_order']) ?  $link['sort_order'] : '',
				'image'				=> isset($link['image']) ?  $link['image'] : '',
				'thumb'				=> $thumb,
			);
		}
		

		$data['placeholder'] = $this->model_tool_image->resize('no_image.png', 100, 100);

		$this->load->model('tool/image');
		$data['templates'] = array();
		$data['templates'][] = array(
			'template_id'		=> 'template_1',
			'name'				=> $this->language->get('text_template_1'),
		);

		$data['templates'][] = array(
			'template_id'		=> 'template_2',
			'name'				=> $this->language->get('text_template_2'),
		);

		$data['templates'][] = array(
			'template_id'		=> 'template_3',
			'name'				=> $this->language->get('text_template_3'),
		);

		$data['config_language_id'] = $this->config->get('config_language_id');

		$data['token'] = $this->session->data['token'];

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('accountdashboard/accountdashboardpro.tpl', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'accountdashboard/accountdashboardpro')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (isset($this->request->post['accountdashboardpro_link'])) {

			foreach ($this->request->post['accountdashboardpro_link'] as $row => $description) {
				
				foreach ($description['description'] as $language_id => $value) {
					if ((utf8_strlen($value['title']) < 1) || (utf8_strlen($value['title']) > 128)) {
						$this->error['title'][$row][$language_id] = $this->language->get('error_title');
					}
				}


				if(!$description['link']) {
					$this->error['link'][$row] = $this->language->get('error_link');
				}

				if($description['type'] == 'image') {
					if (empty($description['image'])) {
						$this->error['image'][$row] = $this->language->get('error_image');
					}
				} else {	
					if (empty($description['icon_class'])) {
						$this->error['icon_class'][$row] = $this->language->get('error_icon_class');
					}
				}	
			}
		}

		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}

		return !$this->error;
	}

	public function template() {
		if ($this->request->server['HTTPS']) {
			$server = HTTPS_CATALOG;
		} else {
			$server = HTTP_CATALOG;
		}
		
		if (isset($this->request->get['template'])) {
			$template = $this->request->get['template'];
		} else {
			$template = '';
		}
		
		if (is_file(DIR_IMAGE . 'accountdashboard/' . $template . '.png')) {
			$this->response->setOutput($server . 'image/accountdashboard/' . $template . '.png');
		} else {
			$this->response->setOutput($server . 'image/no_image.png');
		}
	}	
}