<?php
class ControllerGittigidiyordashboard extends Controller {
	/*
		gittigidiyor genel anasayfasıdır
		modül hakkında bilgi, sipariş sayıları gibi temel bilgiler içerir.
	*/
	public function index(){
		// gittigidiyor general dosyamızı alıyoruz
		ini_set('error_reporting', E_ALL);
		if(VERSION < 3){
			$token = $this->session->data['token'];
			$token_link = 'token=' . $this->session->data['token'];
		} else {
			$token = $this->session->data['user_token'];
			$token_link = 'user_token=' . $this->session->data['user_token'];
		}


		$data = array();

		$this->load->model('gittigidiyor/general');
		$this->load->library('gittigidiyor');
		$this->model_gittigidiyor_general->CreatePage('GG Entegrasyonu', array('gittigidiyor/sale'));

		$table_check = $this->db->query("SHOW TABLES LIKE '".DB_PREFIX."ggshops'");
		if($table_check->num_rows < 1){
			$this->model_gittigidiyor_general->install();
		}

		$data['heading_title'] = 'gittigidiyor Dashboard';
		$data['heading_title2'] = '';


		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', $token_link, 'SSL'),
		);
		$data['breadcrumbs'][] = array(
			'text'      => 'GG Dashboard',
			'href'      => $this->url->link('gittigidiyor/dashboard', $token_link, 'SSL'),
		);

		$data['token'] = $token;
		$data['token_link'] = $token_link;



		$data['links'] = $this->model_gittigidiyor_general->getlinks();
		$data['shops'] = $this->model_gittigidiyor_general->getShops();
		$data['actshop_id'] = $this->model_gittigidiyor_general->actShopid();
		$auth = $this->model_gittigidiyor_general->getAuth();
		$data['salecount'] = $this->model_gittigidiyor_sale->getOrderCount($auth);
		$data['escat'] = $this->model_gittigidiyor_general->eslesmemisCat();
		$data['mesajsayi'] = $this->model_gittigidiyor_general->geturunsoru($auth);

		if(isset($this->session->data['error'])){
			$data['error'] = $this->session->data['error'];
			unset($this->session->data['error']);
		} else {
			$data['error'] = '';
		}
		$this->model_gittigidiyor_general->gettheme($data, 'dashboard');
	}
}
?>
