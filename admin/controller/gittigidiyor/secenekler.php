<?php
class ControllerGittigidiyorSecenekler extends Controller {
	public function index(){
		$data = array();
		ini_set('error_reporting', E_ALL);
		if(VERSION < 3){
			$token = $this->session->data['token'];
			$token_link = 'token=' . $this->session->data['token'];
		} else {
			$token = $this->session->data['user_token'];
			$token_link = 'user_token=' . $this->session->data['user_token'];
		}


		$this->load->model('gittigidiyor/general');
		$this->model_gittigidiyor_general->CreatePage('GG Seçenek İşlemleri', array('gittigidiyor/sale'));
		$data['heading_title'] = 'Gittigidiyor Seçenekleri Eşleştir';
		$data['heading_title2'] = '';


		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', $token_link, 'SSL'),
		);
		$data['breadcrumbs'][] = array(
			'text'      => 'GG Seçenekler',
			'href'      => $this->url->link('gittigidiyor/secenekler', $token_link, 'SSL'),
		);

		$data['links'] = $this->model_gittigidiyor_general->getlinks();
		$data['shops'] = $this->model_gittigidiyor_general->getShops();
		$data['actshop_id'] = $this->model_gittigidiyor_general->actShopid();

		$data['token'] = $token;
		$data['token_link'] = $token_link;

		$this->model_gittigidiyor_general->gettheme($data, 'secenekler_list');
	}


	public function checkvariantmycategories(){
		$this->load->model('gittigidiyor/general');
		$this->load->model('gittigidiyor/category');
		$this->load->model('setting/setting');
		$result = $this->model_gittigidiyor_category->checkVariants($this->model_gittigidiyor_general->getAuth());
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($result));
	}

	public function downloadSecenek(){

	}
}
?>
