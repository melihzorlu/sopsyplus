<?php
class ControllerGittigidiyorSetting extends Controller {
	public function index(){
		// n11 general dosyamızı alıyoruz
		if(VERSION < 3){
			$token = $this->session->data['token'];
			$token_link = 'token=' . $this->session->data['token'];
		} else {
			$token = $this->session->data['user_token'];
			$token_link = 'user_token=' . $this->session->data['user_token'];
		}

		$data = array();
		$this->load->model('gittigidiyor/general');
		$this->document->setTitle('GG Entegrasyon Ayarları');
		$this->document->addStyle('view/template/gittigidiyor/asset/global_style.css');
		$data['heading_title'] = 'GG Entegrasyon Ayarları';
		$data['heading_title2'] = '';

		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', $token_link, 'SSL'),
		);
		$data['breadcrumbs'][] = array(
			'text'      => 'GG Ayarlar',
			'href'      => $this->url->link('gittigidiyor/settings', $token_link, 'SSL'),
		);

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
			unset($this->session->data['action']);
		} else {
			$data['success'] = '';
		}

		$data['links'] = $this->model_gittigidiyor_general->getlinks();
		$data['shops'] = $this->model_gittigidiyor_general->getShops();
		$data['actshop_id'] = $this->model_gittigidiyor_general->actShopid();
		$data['token'] = $token;
		$data['token_link'] = $token_link;

		$data['shop'] = $this->model_gittigidiyor_general->getShop($data['actshop_id']);

		$data['sehirler'] = array();
		$client = $this->model_gittigidiyor_general->getAuth();
		$sehirler = $client->getCity($data['shop']['city']);
		$data['sehir'] = $sehirler->cities->city->cityName;


		$data['degerleri_duzenle'] = $this->url->link('gittigidiyor/install/editing', 'shop_id='.$data['actshop_id'].'&step=2&action=edit&'.$token_link, 'SSL');


		$this->model_gittigidiyor_general->gettheme($data, 'settings');
	}


	public function changeshop(){
		ini_set('error_reporting', E_ALL);
		if(VERSION < 3){
			$token = $this->session->data['token'];
			$token_link = 'token=' . $this->session->data['token'];
		} else {
			$token = $this->session->data['user_token'];
			$token_link = 'user_token=' . $this->session->data['user_token'];
		}

		$this->session->data['ggshop_id'] = $this->request->get['id'];
		$this->response->redirect($this->url->link('gittigidiyor/dashboard', $token_link, 'SSL'));
	}
}
?>
