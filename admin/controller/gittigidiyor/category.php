<?php
class ControllerGittigidiyorCategory extends Controller {
	public function checkvariantmycategories(){
		$this->load->model('gittigidiyor/general');
		$this->load->model('gittigidiyor/category');
		$this->load->model('setting/setting');
		$result = $this->model_gittigidiyor_category->checkVariants($this->model_gittigidiyor_general->getAuth());
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($result));
	}

	public function index(){
		// gittigidiyor general dosyamızı alıyoruz
		ini_set('error_reporting', E_ALL);
		if(VERSION < 3){
			$token = $this->session->data['token'];
			$token_link = 'token=' . $this->session->data['token'];
		} else {
			$token = $this->session->data['user_token'];
			$token_link = 'user_token=' . $this->session->data['user_token'];
		}


		$data = array();

		$this->load->model('gittigidiyor/general');
		$this->model_gittigidiyor_general->CreatePage('GG Entegrasyonu', array('gittigidiyor/category'));
		$data['heading_title'] = 'Gittigidiyor Kategoriler';
		$data['heading_title2'] = '';

		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', $token_link, 'SSL'),
		);
		$data['breadcrumbs'][] = array(
			'text'      => 'GG Kategoriler',
			'href'      => $this->url->link('gittigidiyor/dashboard', $token_link, 'SSL'),
		);
		$data['links'] = $this->model_gittigidiyor_general->getlinks();
		$data['shops'] = $this->model_gittigidiyor_general->getShops();
		$data['actshop_id'] = $this->model_gittigidiyor_general->actShopid();

		$data['token'] = $token;
		$data['token_link'] = $token_link;

		if (isset($this->request->get['filter_category'])) {
			$filter_category = $this->request->get['filter_category'];
		} else {
			$filter_category = '';
		}

		if (isset($this->request->get['filter_category_id'])) {
			$filter_category_id = $this->request->get['filter_category_id'];
		} else {
			$filter_category_id = '';
		}


		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
		} else {
			$filter_status = '';
		}

		if (isset($this->request->get['filter_eslesme'])) {
			$filter_eslesme = $this->request->get['filter_eslesme'];
		} else {
			$filter_eslesme = '';
		}

		if (isset($this->request->get['filter_komisyon'])) {
			$filter_komisyon = $this->request->get['filter_komisyon'];
		} else {
			$filter_komisyon = '';
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['filter_eslesme'])) {
			$url .= '&filter_eslesme=' . $this->request->get['filter_eslesme'];
		}

		if (isset($this->request->get['filter_category_id'])) {
			$url .= '&filter_category_id=' . $this->request->get['filter_category_id'];
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['filter_komisyon'])) {
			$url .= '&filter_komisyon=' . $this->request->get['filter_komisyon'];
		}

		$data['categories'] = array();

		$filter_data = array(
			'filter_eslesme'	  => $filter_eslesme,
			'filter_category_id'	  => $filter_category_id,
			'filter_status'	  => $filter_status,
			'filter_komisyon' => $filter_komisyon,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);

		$category_total = $this->model_gittigidiyor_category->getTotalopcategories($filter_data);

		$results = $this->model_gittigidiyor_category->getOpcategories($filter_data);

		foreach ($results as $result) {
			$data['categories'][] = array(
				'category_id' => $result['category_id'],
				'ggcomission' => $result['ggcomission'],
				'gg_id' 	  => $result['gg_id'],
				'gcatstatus'  => $result['gcatstatus'],
				'gg_name'    => $this->model_gittigidiyor_category->getggnamebyid($result['gg_id']),
				'name'        => $result['name'],
				'sort_order'  => $result['sort_order'],
				'edit'        => $this->url->link('catalog/category/edit', $token_link . '&category_id=' . $result['category_id'] . $url, 'SSL'),
				'delete'      => $this->url->link('catalog/category/delete',   $token_link . '&category_id=' . $result['category_id'] . $url, 'SSL')
			);

		}
		$pagination = new Pagination();
		$pagination->total = $category_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('gittigidiyor/category', $token_link . $url . '&page={page}', 'SSL');

        $data['category_total'] = $category_total;
		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($category_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($category_total - $this->config->get('config_limit_admin'))) ? $category_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $category_total, ceil($category_total / $this->config->get('config_limit_admin')));

		$data['filter_status'] = $filter_status;
		$data['filter_category'] = $filter_category;
		$data['filter_category_id'] = $filter_category_id;
		$data['filter_eslesme'] = $filter_eslesme;
		$data['filter_komisyon'] = $filter_komisyon;

		$this->model_gittigidiyor_general->gettheme($data, 'category');
	}

	public function getsecenek(){
		$this->load->model('gittigidiyor/general');
		$this->load->model('gittigidiyor/category');
		$this->load->model('catalog/option');
		$ocoptions = $this->model_catalog_option->getOptions();

		$categorycode = $this->request->get['categorycode'];
		$getcategorysecenek = $this->db->query("SELECT * FROM ".DB_PREFIX."ggcategories WHERE category_code = '".$categorycode."'");
		if(!empty($getcategorysecenek->row['variants']) or $getcategorysecenek->row['variants'] != 0){
			$secilicat = json_decode($getcategorysecenek->row['variants']);
			$secili_option = $secilicat->ocsecenek;
			$secili_values = $secilicat->ocsenecek_deger;
		} else {
			$secilicat = array();
		}

		$client = $this->model_gittigidiyor_general->getAuth();
		$specs = $client->getCategoryVariantSpecs($categorycode);

		echo '<form id="secenekform" method="post">';
		echo '<input type="hidden" name="categorycode" value="'.$categorycode.'">';
		echo '<table class="table table-hover">';
		echo '<thead><tr><th>GG Seçeneği</th><th>Site Seçeneği</th></thead>';
		if(isset($specs->specs->spec->specValues)){

				echo '<tr class="active">';
					echo '<td><strong>'.$specs->specs->spec->name.'<input type="hidden" value="'.$specs->specs->spec->name.'" name="ggsecenek['.$specs->specs->spec->nameId.']"></strong></td>';
					echo '<td>';
					echo '<select name="ocsecenek['.$specs->specs->spec->nameId.']" class="form-control secenek_secin" data-specid="'.$specs->specs->spec->nameId.'">';
					echo '<option value="">-- Seçiniz --</option>';
					foreach ($ocoptions as $option) {
					  	echo '<option value="'.$option['option_id'].'">'.$option['name'].'</option>';
					}
					echo '</select>';
					echo '</td>';
				echo '<tr>';
				foreach ($specs->specs->spec->specValues->specValue as $deger) {
					echo '<tr>';
						echo '<td>'.$deger->value.' <input type="hidden" value="'.$deger->value.'" name="ggsecenek_deger['.$deger->valueId.']"></td>';
						echo '<td class="spec_'.$specs->specs->spec->nameId.'">
						<select id="select_spec_'.$specs->specs->spec->nameId.'" name="ocsenecek_deger['.$deger->valueId.']" class="form-control input-sm">
								<option value="">-- Önce Seçenek Karşılığı Seçin --</option>
							</select>
						</td>';
					echo '</tr>';
				}
		} else {
			// 1 den fazla seçenek ekleme şansı varsa
			foreach ($specs->specs->spec as $kspec) {
					echo '<tr class="active gsecene_ad_'.$kspec->nameId.'">';
						echo '<td><strong>'.$kspec->name.'<input type="hidden" value="'.$kspec->name.'" name="ggsecenek['.$kspec->nameId.']"></strong></td>';
						echo '<td>';
						echo '<select name="ocsecenek['.$kspec->nameId.']" class="form-control secenek_secin" data-specid="'.$kspec->nameId.'">';
						echo '<option value="">-- Seçiniz --</option>';

							$sabitoptions = array();
							$sabitoptions[] = array(
								'option_id' => 'sabit',
								'type' => 'select',
								'sort_order' => 0,
								'language_id' => (int)$this->config->get('config_language_id'),
								'name' => 'Sabit Değer Tanımla'
							);



							$ocoptionsb = array_merge($ocoptions, $sabitoptions);
							foreach ($ocoptionsb as $option) {
								if($secili_option->{$kspec->nameId} == $option['option_id']){
									echo '<option selected="selected" value="'.$option['option_id'].'">'.$option['name'].'</option>';
								} else {
							  		echo '<option value="'.$option['option_id'].'">'.$option['name'].'</option>';
								}
							}
						echo '</select>';
						echo '</td>';
					echo '<tr>';
					foreach ($kspec->specValues->specValue as $deger) {
						echo '<tr class="secenek_degeri_'.$kspec->nameId.'">';
							echo '<td>'.$deger->value.'<input type="hidden" value="'.$deger->value.'" name="ggsecenek_deger['.$deger->valueId.']"></td>';
							echo '<td class="spec_'.$kspec->nameId.'">
								<select id="selec_specval_'.$deger->valueId.'" name="ocsenecek_deger['.$deger->valueId.']" class="form-control input-sm select_spec_'.$kspec->nameId.'">
									<option value="">-- Önce Seçenek Karşılığı Seçin --</option>
								</select>
							</td>';
						echo '</tr>';
					}

			}
		}
		echo '</table>';
		echo '</form><script type="text/javascript">
			$(document).ready(function(){
				$(".secenek_secin").trigger("change");
				setTimeout(function () {
			';
				foreach ($secili_values as $vkey => $vvalue) {
					echo '
						$("#selec_specval_'.$vkey.'").val('.$vvalue.');
					';
				}
		echo ' }, 1000); });</script>';
	}

	public function getoptionvalue(){
		$this->load->model('gittigidiyor/general');
		$option_id = $this->request->get['optionid'];
		$categorycode = $this->request->get['categorycode'];
		$specid = $this->request->get['specid'];
		if($option_id == 'sabit'){
			// sabit değer tanımlama
			$client = $this->model_gittigidiyor_general->getAuth();
			$specs = $client->getCategoryVariantSpecs($categorycode);
			foreach ($specs->specs->spec as $kspec) {
				if($kspec->nameId == $specid){
					echo '<tr class="secenek_degeri_'.$kspec->nameId.'">';
						echo '<td>'.$kspec->name.'</td>';
						echo '<td>';
						echo '<select id="selec_specval_sabit_'.$kspec->nameId.'" name="ocsenecek_deger[sabit_'.$kspec->nameId.']" class="form-control input-sm select_spec_'.$kspec->nameId.'">';
						foreach ($kspec->specValues->specValue as $deger) {
							echo '<option value="'.$deger->valueId.'">'.$deger->value.'</option>';
						}
						echo '</select>';
						echo '</td>';
						echo '</select>';
					echo '</tr>';
				}
			}
		} else {
			$getcategorysecenek = $this->db->query("SELECT * FROM ".DB_PREFIX."ggcategories WHERE category_code = '".$categorycode."'");
			$this->load->model('catalog/option');
			$option_values = $this->model_catalog_option->getOptionValues($option_id);
			echo '<option value="">-- Seçiniz --</option>';
			foreach ($option_values as $value) {

				echo '<option value="'.$value['option_value_id'].'">'.$value['name'].'</option>';
			}
		}
	}


	public function seceneksave(){
		$post = $this->request->post;
		$up = $this->db->query("UPDATE ".DB_PREFIX."ggcategories SET variants = '".$this->db->escape(json_encode($post))."' WHERE category_code = '".$post['categorycode']."'");
		if($up){
			$json = array('status' => 1, 'msg' => 'Kategori seçenek eşleştirmesi başarıyla düzenlendi');
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	/*
		gittigidiyor root kategorileri seçiniz
	*/
	public function select_root(){
		$this->load->model('gittigidiyor/general');
		ini_set('error_reporting', E_ALL);
		if(VERSION < 3){
			$token = $this->session->data['token'];
			$token_link = 'token=' . $this->session->data['token'];
		} else {
			$token = $this->session->data['user_token'];
			$token_link = 'user_token=' . $this->session->data['user_token'];
		}

		$this->model_gittigidiyor_general->CreatePage('GG Ana Kategori Seçme', array('gittigidiyor/category'));
		$data['heading_title'] = 'Gittigidiyor Kategori Seçme';
		$data['heading_title2'] = '';

		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', $token_link, 'SSL'),
		);
		$data['breadcrumbs'][] = array(
			'text'      => 'Gittigidiyor Kategoriler',
			'href'      => $this->url->link('gittigidiyor/category/select_root', $token_link, 'SSL'),
		);

		$data['links'] = $this->model_gittigidiyor_general->getlinks();
		$data['shops'] = $this->model_gittigidiyor_general->getShops();
		$data['actshop_id'] = $this->model_gittigidiyor_general->actShopid();

		$data['token'] = $token;
		$data['token_link'] = $token_link;


		$data['return'] = $this->url->link('gittigidiyor/category/ggcategory', $token_link, 'SSL');

		$this->load->model('setting/setting');
		$data['success'] = '';
		if($this->request->server['REQUEST_METHOD'] == 'POST'){
			if($this->request->post['kategorilerikaydet'] == 'kydt'){
				$this->model_setting_setting->editSetting('gg_kat', $this->request->post);
				$data['success'] = 'Kategoriler başarılı bir şekilde seçildi';
			}
		}

		$data['gg_kategoriler'] = array();
		if (isset($this->request->post['gg_kategori'])) {
			$data['gg_kategoriler'] = $this->request->post['gg_kategori'];
		} else {
			$data['gg_kategoriler'] = $this->config->get('gg_kategori');
		}

		$data['ggcat'] = $this->model_gittigidiyor_category->gettoplvl($this->model_gittigidiyor_general->getAuth());

		$this->model_gittigidiyor_general->gettheme($data, 'gg_select_root');
	}


	/* gittigidiyor alt kategori indir */
	public function download_parent(){
		$this->load->model('gittigidiyor/general');
		$this->load->model('gittigidiyor/category');
		$this->load->model('setting/setting');
		if(isset($this->request->get['page'])){
			$page = $this->request->get['page'];
		} else {
			$page = 0;
		}
		$cats = $this->config->get('gg_kategori');
		if(isset($cats[$page])){
			$result = $this->model_gittigidiyor_category->download_parent($this->model_gittigidiyor_general->getAuth(), $page, $cats[$page]);
		} else {
			$result = array('status' => 1, 'msg' => 'Kategori İndirme Tamamlandı');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($result));
	}

	public function ggcategory(){
		$this->load->model('gittigidiyor/general');
		ini_set('error_reporting', E_ALL);
		if(VERSION < 3){
			$token = $this->session->data['token'];
			$token_link = 'token=' . $this->session->data['token'];
		} else {
			$token = $this->session->data['user_token'];
			$token_link = 'user_token=' . $this->session->data['user_token'];
		}

		$this->model_gittigidiyor_general->CreatePage('GG Kategori İşlemleri', array('gittigidiyor/category'));
		$data['heading_title'] = 'GG Kategori İşlemleri';
		$data['heading_title2'] = '';


		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', $token_link, 'SSL'),
		);
		$data['breadcrumbs'][] = array(
			'text'      => 'GG Kategori İşlemleri',
			'href'      => $this->url->link('gittigidiyor/category/ggcategory', $token_link, 'SSL'),
		);

		$data['links'] = $this->model_gittigidiyor_general->getlinks();
		$data['shops'] = $this->model_gittigidiyor_general->getShops();
		$data['actshop_id'] = $this->model_gittigidiyor_general->actShopid();

		$data['token'] = $token;
		$data['token_link'] = $token_link;

		if(isset($this->session->data['success'])){
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if(isset($this->session->data['error'])){
			$data['error'] = $this->session->data['error'];
			unset($this->session->data['error']);
		} else {
			$data['error'] = '';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$data['categories'] = array();

		$filter_data = array(
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);

		$category_total = $this->model_gittigidiyor_category->getTotalGgcategories();

		$results = $this->model_gittigidiyor_category->getGgcategories($filter_data);

		foreach ($results as $result) {
			//$attr_count = $this->model_gittigidiyor_category->attrCount($result['category_code']);
			$data['categories'][] = array(
				'category_code' => $result['category_code'],
				'name'        => $result['name'],
				'variant'        => $result['variant'],
				'status'      => $result['status'],
				//'attr_count'  => $attr_count
			);
		}

		$pagination = new Pagination();
		$pagination->total = $category_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('gittigidiyor/category/ggcategory', $token_link.'&page={page}', 'SSL');

		$data['pagination'] = $pagination->render();
		$data['results'] = sprintf($this->language->get('text_pagination'), ($category_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($category_total - $this->config->get('config_limit_admin'))) ? $category_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $category_total, ceil($category_total / $this->config->get('config_limit_admin')));

		$data['select_root'] = $this->url->link('gittigidiyor/category/select_root', $token_link, 'SSL');
		$data['donwload_parent'] = $this->url->link('gittigidiyor/category/download_parent', $token_link, 'SSL');


		$this->model_gittigidiyor_general->gettheme($data, 'gg_category');
	}

	/*
	 n11 kategori autocomplete
	*/
	 public function ggkategoriara(){
	 	$json = array();
  		if(isset($this->request->get['filter_name'])){
  			$sqlSorgu = "SELECT * FROM ".DB_PREFIX."ggcategories WHERE ";
			$kelime = explode(" ", $this->request->get['filter_name']);
			foreach ($kelime as $key => $value) {
			     $whereSql[] = " name like '%".$this->db->escape($value)."%'";
			}
			$sqlSorgu .= implode(" AND ",$whereSql);
  			$query = $this->db->query($sqlSorgu);
  			if($query->num_rows > 0){
  				foreach ($query->rows as $value){
  					$json[] = $value['category_code'].'|'.strip_tags(html_entity_decode($value['name'], ENT_QUOTES, 'UTF-8'));
  				}
  			}
  		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	/* kategori durum değiştirme (opencart) */
	public function changencatstatus(){
		$category_id = $this->request->get['category_id'];
		$status = $this->request->get['status'];
		$up = $this->db->query("UPDATE ".DB_PREFIX."category SET gcatstatus = '".$status."' WHERE category_id = '".$category_id."'");
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode(array('status' => 1, 'msg' => 'Kategori durumu başarıyla değiştirildi')));
	}

	/* kategori durum değiştirme */
	public function changenstatus(){
		$category_code = $this->request->get['category_code'];
		$status = $this->request->get['status'];
		$up = $this->db->query("UPDATE ".DB_PREFIX."ggcategories SET status = '".$status."' WHERE category_code = '".$category_code."'");
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode(array('status' => 1, 'msg' => 'Kategori durumu başarıyla değiştirildi')));
	}

	/* xeditable kategori eşleştirme */
	public function eslestir(){
		$gg = explode('|', $this->request->post['value']);
		$this->db->query("UPDATE ".DB_PREFIX."category SET gg_id = '".$gg[0]."', ggattr = '' WHERE category_id = '".$this->request->post['pk']."'");
		$json = array('opcategory' => $this->request->post['pk'], 'ggcategory' => $gg[0]);
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}


	/* x editabel kategori komisyon */
	public function category_komisyon(){
		$post = $this->request->post;
		$this->db->query("UPDATE ".DB_PREFIX."category SET ggcomission = '".$post['value']."' WHERE category_id = '".$post['pk']."'");
	}

	/*
		bir kategorinin özelliklerini getir
	*/
	public function getattr(){
		$this->load->model('gittigidiyor/general');
		$this->load->model('gittigidiyor/product');
		$client = $this->model_gittigidiyor_general->getAuth();
		if(isset($this->request->get['ggid'])){
			$ggid = $this->request->get['ggid'];
		} else {
			echo '<div class="alert alert-danger">Kategori Kodu Alınamadı</div>';
			exit;
		}
		$attr = $client->getCategorySpecs($ggid);
		if($attr->ackCode != 'success'){
			echo '<div class="alert alert-danger">'.$attr->error->message.'</div>';
			exit;
		}
		$op_attr = $this->db->query("SELECT * FROM ".DB_PREFIX."category WHERE category_id = '".$this->request->get['ocid']."'");
		$opattr = (array)json_decode($op_attr->row['gg_attribute']);
		$attrb = array();
		$html = '';
		$html .= '<form id="attrform" method="post">';
		$html .= '<input type="hidden" name="category_id" value="'.$this->request->get['ocid'].'">';
		foreach ($attr->specs->spec as $value){
			$atrlist[$value->name]['name'] = $value->name;
			$atrlist[$value->name]['type'] = $value->type;
			$atrlist[$value->name]['required'] = $value->required;
			if(is_array($value->values->value)){
				foreach ($value->values->value as $val) {
					if(isset($val)){
						$attr_values[$value->name][] = $val;
					}
				}
			} else {
				$attr_values[$value->name][] = $value->values->value;
			}
		}

		foreach ($attr_values as $key => $value){
			$temiz_value = array_unique($value, SORT_STRING);
			$atrlist[$key]['value'] = $temiz_value;
		}

		foreach ($atrlist as $key => $tatrr) {
			if($tatrr['required'] == 1){
				$zorunlu = 'required="required"';
				$html .= '<div class="form-group"><label>'.$key.' (Zorulu)</label>';
			} else {
				$html .= '<div class="form-group"><label>'.$key.'</label>';
				$zorunlu = '';
			}


			$html .= '<select name="prdattr['.$key.']" class="form-control input-sm" '.$zorunlu.'>';
			if($tatrr['required'] != 1){
				$html .= '<option value="">- Seçiniz -</option>';
			}
			rsort($tatrr['value']);
			foreach ($tatrr['value'] as $atrval){

				if(isset($opattr)){
					foreach ($opattr as $ke => $val) {
						if($ke == $key){
							if($val == $atrval){
								$html .= '<option value="'.$atrval.'" selected>'.$atrval.'</option>';
							} else {
								$html .= '<option value="'.$atrval.'">'.$atrval.'</option>';
							}
						}
					}
				} else {
					$html .= '<option value="'.$atrval.'">'.$atrval.'</option>';
				}
			}
			$html .= '</select></div>';
		}

		$html .= '</form>';
		echo $html;
	}


	/*
	 kategori özelliklerini kaydet
	*/
	public function attrsave(){
		$post = $this->request->post;
		$up = $this->db->query("UPDATE ".DB_PREFIX."category SET ggattr = '".$this->db->escape(json_encode($post['prdattr']))."' WHERE category_id = '".$post['category_id']."'");
		if($up){
			$json = array('status' => 1, 'msg' => 'Kategori özellikleri başarıyla düzenlendi');
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
?>
