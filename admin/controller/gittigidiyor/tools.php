<?php
class ControllerGittigidiyorTools extends Controller {
	public function index(){
		ini_set('error_reporting', E_ALL);
		if(VERSION < 3){
			$token = $this->session->data['token'];
			$token_link = 'token=' . $this->session->data['token'];
		} else {
			$token = $this->session->data['user_token'];
			$token_link = 'user_token=' . $this->session->data['user_token'];
		}

		$data = array();
		$this->load->model('gittigidiyor/general');
		$this->model_gittigidiyor_general->CreatePage('GG Araçları', array('gittigidiyor/sale'));
		$data['heading_title'] = 'GG Araçlar';

		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', $token_link, 'SSL'),
		);
		$data['breadcrumbs'][] = array(
			'text'      => 'GG Araçlar',
			'href'      => $this->url->link('gittigidiyor/tools', $token_link, 'SSL'),
		);

		$data['links'] = $this->model_gittigidiyor_general->getlinks();
		$data['shops'] = $this->model_gittigidiyor_general->getShops();
		$data['actshop_id'] = $this->model_gittigidiyor_general->actShopid();
		
		$data['token'] = $token;
		$data['token_link'] = $token_link;

		$this->model_gittigidiyor_general->gettheme($data, 'tool_list');
	}

	public function downloadCustomer(){
		/*
		$data = array();
		$this->load->model('gittigidiyor/general');
		$this->load->model('gittigidiyor/product');
		$this->load->model('gittigidiyor/category');
		if(isset($this->request->get['page'])){
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
		
		$json = $this->model_gittigidiyor_product->downloadCustomer($this->model_gittigidiyor_general->getAuth(), $page);
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
		*/
	}
}
?>
