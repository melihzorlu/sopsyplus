<?php
class ControllerGittigidiyorShops extends Controller {

	public function index(){
		// mağaza listemizi alıyoruz
		ini_set('error_reporting', E_ALL);
		if(VERSION < 3){
			$token = $this->session->data['token'];
			$token_link = 'token=' . $this->session->data['token'];
		} else {
			$token = $this->session->data['user_token'];
			$token_link = 'user_token=' . $this->session->data['user_token'];
		}

		$data = array();
		$this->load->model('gittigidiyor/general');
		$this->model_gittigidiyor_general->CreatePage('GG Dükkanlar', array('gittigidiyor/sale'));
		$data['heading_title'] = 'GG Dükkanlar';
		$data['heading_title2'] = '';


		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', $token_link, 'SSL'),
		);
		$data['breadcrumbs'][] = array(
			'text'      => 'GG Dükkanlar',
			'href'      => $this->url->link('gittigidiyor/shops', $token_link, 'SSL'),
		);
		$data['links'] = $this->model_gittigidiyor_general->getlinks();
		$data['add_shop'] = $this->url->link('gittigidiyor/shops/addshop', $token_link, 'SSL');


		$data['shops'] = array();
		$data['shops'] = $this->model_gittigidiyor_general->getShops();

		$data['actshop_id'] = $this->model_gittigidiyor_general->actShopid();

		$data['token'] = $token;
		$data['token_link'] = $token_link;

		$this->model_gittigidiyor_general->gettheme($data, 'shop_list');
	}

	public function addshop(){
		ini_set('error_reporting', E_ALL);
		if(VERSION < 3){
			$token = $this->session->data['token'];
			$token_link = 'token=' . $this->session->data['token'];
		} else {
			$token = $this->session->data['user_token'];
			$token_link = 'user_token=' . $this->session->data['user_token'];
		}


		$data = array();
		$this->load->model('gittigidiyor/general');
		$this->model_gittigidiyor_general->CreatePage('GG Dükkan Ekle', array('gittigidiyor/sale'));
		$data['heading_title'] = 'GG Dükkan Ekle';

		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', $token_link, 'SSL'),
		);
		$data['breadcrumbs'][] = array(
			'text'      => 'GG Dükkan Ekle',
			'href'      => $this->url->link('gittigidiyor/shops', $token_link, 'SSL'),
		);
		$data['breadcrumbs'][] = array(
			'text'      => 'Dükkan Ekle',
			'href'      => $this->url->link('gittigidiyor/shops/addshop', $token_link, 'SSL'),
		);

		// mağazamızı ekleyelim
		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			$this->model_gittigidiyor_general->addShop($this->request->post);
			$this->session->data['success'] = 'Dükkan başarıyla eklendi!';
			$this->response->redirect($this->url->link('gittigidiyor/shops', $token_link, 'SSL'));
		}

		$data['token'] = $token;
		$data['token_link'] = $token_link;

		$data['shops'] = array();
		$data['shops'] = $this->model_gittigidiyor_general->getShops();
		$data['actshop_id'] = $this->model_gittigidiyor_general->actShopid();

		$data['links'] = $this->model_gittigidiyor_general->getlinks();
		$data['shop'] = array(
			'id' => '',
			'name' => '',
			'apikey' => '',
			'secretkey' => '',
			'nick' => '',
			'password' => '',
			'auth_user' => '',
			'auth_pass' => '',
			'default_subtitle' => '',
			'cargo_company' => '',
			'shipping_condition' => '',
			'shipping_price' => '',
			'shipping_aprice' => '',
			'difference_type' => '',
			'difference_value' => '',
			'currency' => '',
			'currency_calctype' => '',
			'tax_option' => '',
			'description' => '{aciklama}'
		);

		$this->model_gittigidiyor_general->gettheme($data, 'shop_form');
	}

	public function edit(){
		ini_set('error_reporting', E_ALL);
		if(VERSION < 3){
			$token = $this->session->data['token'];
			$token_link = 'token=' . $this->session->data['token'];
		} else {
			$token = $this->session->data['user_token'];
			$token_link = 'user_token=' . $this->session->data['user_token'];
		}


		$data = array();
		$this->load->model('gittigidiyor/general');
		$this->model_gittigidiyor_general->CreatePage('GG Dükkan Düzenle', array('gittigidiyor/sale'));
		$data['heading_title'] = 'GG Dükkan Düzenle';
		$data['heading_title2'] = '';

		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', $token_link, 'SSL'),
		);
		$data['breadcrumbs'][] = array(
			'text'      => 'Gittigidiyor Dükkanlar',
			'href'      => $this->url->link('gittigidiyor/shops', $token_link, 'SSL'),
		);
		$data['breadcrumbs'][] = array(
			'text'      => 'Gittigidiyor Dükkan Düzenle',
			'href'      => $this->url->link('gittigidiyor/shops/edit', $token_link, 'SSL'),
		);


		// mağazamızı ekleyelim
		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			$this->model_gittigidiyor_general->editshop($this->request->post);
			$this->session->data['success'] = 'Dükkan başarıyla düzenlendi!';
			$this->response->redirect($this->url->link('gittigidiyor/shops', $token_link, 'SSL'));
		}

		$data['token'] = $token;
		$data['token_link'] = $token_link;

		$data['shops'] = array();
		$data['shops'] = $this->model_gittigidiyor_general->getShops();
		$data['actshop_id'] = $this->model_gittigidiyor_general->actShopid();

		$data['links'] = $this->model_gittigidiyor_general->getlinks();
		$data['shop'] = $this->model_gittigidiyor_general->getShop($this->request->get['shop_id']);
		//print_r($data['shop']);

		$this->model_gittigidiyor_general->gettheme($data, 'shop_form');
	}

	public function delete(){
		ini_set('error_reporting', E_ALL);
		if(VERSION < 3){
			$token = $this->session->data['token'];
			$token_link = 'token=' . $this->session->data['token'];
		} else {
			$token = $this->session->data['user_token'];
			$token_link = 'user_token=' . $this->session->data['user_token'];
		}

		$this->load->model('gittigidiyor/general');
		$delete = $this->model_gittigidiyor_general->deleteshop($this->request->get['shop_id']);
		$this->response->redirect($this->url->link('gittigidiyor/shops', $token_link, 'SSL'));
	}
}
?>
