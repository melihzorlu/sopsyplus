<?php
class ControllerGittigidiyorSales extends Controller {
	public function index(){
		ini_set('error_reporting', E_ALL);
		if(VERSION < 3){
			$token = $this->session->data['token'];
			$token_link = 'token=' . $this->session->data['token'];
		} else {
			$token = $this->session->data['user_token'];
			$token_link = 'user_token=' . $this->session->data['user_token'];
		}


		$data = array();
		$this->load->model('gittigidiyor/general');
		$this->model_gittigidiyor_general->CreatePage('GG Siparişleri', array('gittigidiyor/sale'));
		$data['heading_title'] = 'Gittigidiyor Siparişleriniz';
		$data['heading_title2'] = '';

		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', $token_link, 'SSL'),
		);
		$data['breadcrumbs'][] = array(
			'text'      => 'GG Siparişleriniz',
			'href'      => $this->url->link('gittigidiyor/sales', $token_link, 'SSL'),
		);
		$data['links'] = $this->model_gittigidiyor_general->getlinks();
		$data['shops'] = $this->model_gittigidiyor_general->getShops();
		$data['actshop_id'] = $this->model_gittigidiyor_general->actShopid();

		$data['token'] = $token;
		$data['token_link'] = $token_link;

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
			$data['page'] = $page;
		} else {
			$page = 1;
			$data['page'] = 1;
		}

		if (isset($this->request->get['status'])) {
			$status = $this->request->get['status'];
			$data['status'] = $this->request->get['status'];
		} else {
			$status = 'P';
			$data['status'] = 'P';
		}

		$url = '';
		if (isset($this->request->get['status'])) {
			$url .= '&status=' . $this->request->get['status'];
		}
		$filter_data = array(
			'status' => $status,
			'shop_id' => $this->model_gittigidiyor_general->actShopid(),
			'start' => $page,
			'limit' => 100
		);
		//$order_total = $this->model_gittigidiyor_sale->gettotalsale($filter_data);
		$results = $this->model_gittigidiyor_sale->getSales($this->model_gittigidiyor_general->getAuth(), $filter_data);

		$data['orders'] = array();
		$data['prev'] = $data['next'] = '';

		if($results){
			if(isset($results->sales->sale)){
				if(isset($results->sales->sale->saleCode)){

					$check = $this->db->query("SELECT * FROM ".DB_PREFIX."order WHERE ggsalecode = '".$results->sales->sale->saleCode."'");
					if($check->num_rows > 0){
						$orderdata = new StdClass;
						$orderdata = $results->sales->sale;
						$orderdata->checkoc = 1;
					} else {
						$orderdata = new StdClass;
						$orderdata = $results->sales->sale;
						$orderdata->checkoc = 0;
					}
					$data['orders'][] = $orderdata;
				} else {
					foreach ($results->sales->sale as $value){
						$check = $this->db->query("SELECT * FROM ".DB_PREFIX."order WHERE ggsalecode = '".$value->saleCode."'");
						if($check->num_rows > 0){
							$orderdata = new StdClass;
							$orderdata = $value;
							$orderdata->checkoc = 1;
						} else {
							$orderdata = new StdClass;
							$orderdata = $value;
							$orderdata->checkoc = 0;
						}
						$data['orders'][] = $orderdata;
					}
				}

				if($page > 1){
					$data['prev'] = $this->url->link('gittigidiyor/sales', $token_link . $url . '&page='.($page-1), 'SSL');
				}
				if($results->nextPageAvailable == 1){
					$data['next'] = $this->url->link('gittigidiyor/sales', $token_link . $url . '&page='.($page+1), 'SSL');
				}
			}
		}

		$this->model_gittigidiyor_general->gettheme($data, 'sale_list');
	}

	public function siparisaktar(){
		$this->load->model('gittigidiyor/general');
		$this->load->model('gittigidiyor/sale');
		$salecode = $this->request->post['salecode'];
		$this->model_gittigidiyor_sale->insertopencart($this->model_gittigidiyor_general->getAuth(), $salecode);
		$json = array('status' => 1, 'msg' => 'Sipariş Başarıyla Aktarıldı');
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}


	/*
	  gittigidiyor sipariş detayını getiriyoruz
	  sipariş ile ilgili alabileceğimiz tüm bilgiler burada
	*/
	public function sale_detail(){
		$this->load->model('gittigidiyor/general');
		$this->load->model('gittigidiyor/sale');
		$salecode = $this->request->get['salecode'];
		$results = $this->model_gittigidiyor_sale->getSale($this->model_gittigidiyor_general->getAuth(), $salecode);


		if($results->ackCode != "failure"){
			$sale = $results->sales->sale;

			if($sale->variantId != 0){
				$variant = $this->model_gittigidiyor_sale->getVariant($this->model_gittigidiyor_general->getAuth(), $sale->productId, (int)$sale->variantId);
				if($variant->ackCode != 'failure'){
					$vardata = $variant->variantGroups->variantGroup->alias;
				} else {
					$vardata = '';
					echo '<div class="alert alert-danger">'.$variant->error->message.'</div>';
				}
			} else {
				$vardata = '';
			}
			echo '<table class="table table-bordered table-striped" style="border-color:#e9e9e9;">';
					echo '<thead><th colspan="2" style="background:#e9e9e9; color: #333;  border-color:#e9e9e9;">MÜŞTERİ BİLGİLERİ</th></thead>';
					echo '<tbody>';
							echo '<tr><td width="200" style="border-color:#e9e9e9;"><strong>Kullanıcı Adı : </strong></td><td style="border-color:#e9e9e9;">'.$sale->buyerInfo->username.'</td>';

							echo '<tr><td style="border-color:#e9e9e9;"><strong>Adı Soyadı : </strong></td><td style="border-color:#e9e9e9;">'.$sale->buyerInfo->name.' '.$sale->buyerInfo->surname.'</td>';
							echo '<tr><td width="200" style="border-color:#e9e9e9;"><strong>Telefonu : </strong></td><td style="border-color:#e9e9e9;">'.$sale->buyerInfo->phone.'</td>';
							if(isset($sale->buyerInfo->neighborhoodType->neighborhoodName)){ $mah = $sale->buyerInfo->neighborhoodType->neighborhoodName; } else { $mah = ''; }
							echo '<tr><td width="200" style="border-color:#e9e9e9;"><strong>Adres : </strong></td><td style="border-color:#e9e9e9;">'.$mah.' '.$sale->buyerInfo->address.' '.$sale->buyerInfo->district.' / '.$sale->buyerInfo->city.'</td>';
							if($sale->buyerInfo->zipCode){
								echo '<tr><td style="border-color:#e9e9e9;"><strong>Posta Kodu : </strong></td><td style="border-color:#e9e9e9;">'.$sale->buyerInfo->zipCode.'</td>';
							}
					echo '</tbody>';

					echo '<thead><th colspan="2" style="background:#e9e9e9; color: #333; border-color:#e9e9e9;">SATIŞ BİLGİLERİ</th></thead>';
					echo '<tbody>';
							echo '<tr><td width="200" style="border-color:#e9e9e9;"><strong>Satış Kodu : </strong></td><td style="border-color:#e9e9e9;">'.$sale->saleCode.'</td>';
							if(isset($sale->moneyDate)){
								echo '<tr><td style="border-color:#e9e9e9;"><strong>Ödeme Tarihi : </strong></td><td style="border-color:#e9e9e9;">'.$sale->moneyDate.'</td>';
							}
							echo '<tr><td style="border-color:#e9e9e9;"><strong>Son İşlem Tarihi : </strong></td><td style="border-color:#e9e9e9;">'.$sale->lastActionDate.'</td>';
							echo '<tr><td style="border-color:#e9e9e9;"><strong>Durumu : </strong></td><td style="border-color:#e9e9e9;">'.$sale->status.'</td>';
							echo '<tr><td style="border-color:#e9e9e9;"><strong>Ürün GG ID : </strong></td><td style="border-color:#e9e9e9;">'.$sale->productId.'</td>';
							echo '<tr><td style="border-color:#e9e9e9;"><strong>Ürün Adı : </strong></td><td style="border-color:#e9e9e9;">'.$sale->productTitle.'</td>';
							if($vardata){
								echo '<tr><td style="border-color:#e9e9e9;"><strong>Seçenek : </strong></td><td style="border-color:#e9e9e9;">'.$vardata.'</td>';
							}
							echo '<tr><td style="border-color:#e9e9e9;"><strong>Adet : </strong></td><td style="border-color:#e9e9e9;">'.$sale->amount.'</td>';
							echo '<tr><td style="border-color:#e9e9e9;"><strong>Fiyat : </strong></td><td style="border-color:#e9e9e9;">'.$sale->price.'</td>';
							echo '<tr><td style="border-color:#e9e9e9;"><strong>Kargo Ödemesi : </strong></td><td style="border-color:#e9e9e9;">'.$sale->cargoPayment.'</td>';
							echo '<tr><td style="border-color:#e9e9e9;"><strong>Kargo Kodu : </strong></td><td style="border-color:#e9e9e9;">'.$sale->cargoCode.'</td>';
					echo '</tbody>';
					if(isset($sale->invoiceInfo->fullname)){
						echo '<thead><th colspan="2" style="background:#e9e9e9; color: #333; border-color:#e9e9e9;">FATURA ADRESİ</th></thead>';
						echo '<tbody>';
							$kargoadres = '';
							$kargoadres .= '<tr><td style="border-color:#e9e9e9;" colspan="2">';
							$kargoadres .= '<strong>Ad Soyad :</strong> '.$sale->invoiceInfo->fullname.'<br>';
							$kargoadres .= '<strong>TC No :</strong> '.$sale->invoiceInfo->tcCertificate.'<br>';
							$kargoadres .= '<strong>Telefon :</strong> '.$sale->invoiceInfo->phoneNumber.'<br>';
								if($sale->invoiceInfo->companyTitle){
									$kargoadres .= '<strong>Şirket :</strong> '.$sale->invoiceInfo->companyTitle.'<br>';
								}
							$city = $this->model_gittigidiyor_sale->getCity($this->model_gittigidiyor_general->getAuth(), $sale->invoiceInfo->cityCode);
							$kargoadres .= '<strong>Adres :</strong> '.$sale->invoiceInfo->address.' '.$city->cities->city->cityName.' / '.$sale->invoiceInfo->district.'<br>';
							echo $kargoadres.'</td></tr>';

					echo '</tbody>';
					}

			echo '</table>';
		} else {
			echo '<div class="alert alert-danger">'.$results->error->message.'</div>';
		}
	}

	public function kargo_insert(){
		$salecode = $this->request->get['salecode'];
		$iform = '<form method="post" id="kargoform"><input type="hidden" name="saleCode" value="'.$salecode.'" class="form-control input-sm">';
		$iform .= '<div class="form-group"><label>Kargo Şirketi</label><select name="cargoCompany" class="form-control input-sm"><option value="Yurtici">Yurtici</option><option value="Aras">Aras</option><option value="MNG">MNG</option><option value="PTT">PTT</option><option value="UPS">UPS</option><option value="Express">Express</option><option value="Surat">Surat</option><option value="Diger">Diger</option></select></div>';
		$iform .= '<div class="form-group"><label>Kargo Takip Kodu</label><input type="text" name="cargoPostCode" class="form-control input-sm"></div>';
		$iform .= '<div class="form-group"><label>Kargo Çıkış Şube</label><input type="text" name="cargoBranchCode" class="form-control input-sm"></div>';
		$iform .= '<div class="form-group"><label>Kargo Takip URL</label><input type="text" name="followUpUrl" class="form-control input-sm"></div>';
		$iform .= '<div class="form-group"><button type="button" class="btn btn-primary btn-sm savekargo">Kaydet</button></div>';
		$iform .= '</form>';
		echo $iform;
	}

	public function save_kargo(){
		$post = $this->request->post;
		$this->load->model('gittigidiyor/general');
		$this->load->model('gittigidiyor/sale');
		$insert = $this->model_gittigidiyor_sale->sendCargo($this->model_gittigidiyor_general->getAuth(), $post);
		if($insert->ackCode != "failure"){
			$json = array('status' => 1, 'msg' => $insert->ackCode);
		} else {
			$json = array('status' => 0, 'msg' => $insert->error->message);
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
?>
