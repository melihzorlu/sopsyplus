<?php
class ControllerGittigidiyorProduct extends Controller {
	public function index(){
		// gittigidiyor general dosyamızı alıyoruz
		ini_set('error_reporting', E_ALL);
		if(VERSION < 3){
			$token = $this->session->data['token'];
			$token_link = 'token=' . $this->session->data['token'];
		} else {
			$token = $this->session->data['user_token'];
			$token_link = 'user_token=' . $this->session->data['user_token'];
		}


		$data = array();
		$this->load->model('gittigidiyor/general');
		$this->model_gittigidiyor_general->CreatePage('Ürün İşlemleri', array('gittigidiyor/product', 'gittigidiyor/category', 'catalog/product'));

		$data['heading_title'] = 'Gittigidiyor Ürünler';
		$data['heading_title2'] = '';

		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', $token_link, 'SSL'),
		);
		$data['breadcrumbs'][] = array(
			'text'      => 'GG Ürünler',
			'href'      => $this->url->link('gittigidiyor/product', $token_link, 'SSL'),
		);
		$data['links'] = $this->model_gittigidiyor_general->getlinks();
		$data['shops'] = $this->model_gittigidiyor_general->getShops();
		$data['actshop_id'] = $this->model_gittigidiyor_general->actShopid();

		$data['token'] = $token;
		$data['token_link'] = $token_link;

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = null;
		}

		if (isset($this->request->get['filter_model'])) {
			$filter_model = $this->request->get['filter_model'];
		} else {
			$filter_model = null;
		}

		if (isset($this->request->get['filter_eslesme'])) {
			$filter_eslesme = $this->request->get['filter_eslesme'];
		} else {
			$filter_eslesme = null;
		}

		if (isset($this->request->get['filter_price'])) {
			$filter_price = $this->request->get['filter_price'];
		} else {
			$filter_price = null;
		}

		if (isset($this->request->get['filter_quantity'])) {
			$filter_quantity = $this->request->get['filter_quantity'];
		} else {
			$filter_quantity = null;
		}

		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
		} else {
			$filter_status = null;
		}


		$filter_ggstatus = 'gonderilmemis';


		if (isset($this->request->get['filter_category'])) {
			$filter_category = $this->request->get['filter_category'];
		} else {
			$filter_category = null;
		}

		if (isset($this->request->get['filter_category_id'])) {
			$filter_category_id = $this->request->get['filter_category_id'];
		} else {
			$filter_category_id = null;
		}

		if (isset($this->request->get['filter_manufacturer'])) {
			$filter_manufacturer = $this->request->get['filter_manufacturer'];
		} else {
			$filter_manufacturer = null;
		}

		if (isset($this->request->get['filter_manufacturer_id'])) {
			$filter_manufacturer_id = $this->request->get['filter_manufacturer_id'];
		} else {
			$filter_manufacturer_id = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'p.product_id';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_model'])) {
			$url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_eslesme'])) {
			$url .= '&filter_eslesme=' . $this->request->get['filter_eslesme'];
		}

		if (isset($this->request->get['filter_price'])) {
			$url .= '&filter_price=' . $this->request->get['filter_price'];
		}

		if (isset($this->request->get['filter_quantity'])) {
			$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
		}

		if (isset($this->request->get['filter_category_id'])) {
			$url .= '&filter_category_id=' . $this->request->get['filter_category_id'];
		}

		if (isset($this->request->get['filter_manufacturer_id'])) {
			$url .= '&filter_manufacturer_id=' . $this->request->get['filter_manufacturer_id'];
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}


		$data['products'] = array();

		$filter_data = array(
			'filter_name'	  => $filter_name,
			'filter_model'	  => $filter_model,
			'filter_price'	  => $filter_price,
			'filter_quantity' => $filter_quantity,
			'filter_status'   => $filter_status,
			'filter_ggstatus'   => $filter_ggstatus,
			'filter_eslesme'  => $filter_eslesme,
			'shop_id'  => $this->model_gittigidiyor_general->actShopid(),
			'filter_category_id'   => $filter_category_id,
			'filter_manufacturer_id'   => $filter_manufacturer_id,
			'sort'            => $sort,
			'order'           => $order,
			'start'           => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'           => $this->config->get('config_limit_admin')
		);

		$this->load->model('tool/image');
		$product_total = $this->model_gittigidiyor_product->getOptotalproduct($filter_data);
		$results = $this->model_gittigidiyor_product->getOpproducts($filter_data);
		foreach ($results as $result) {
			if (is_file(DIR_IMAGE . $result['image'])) {
				$image = $this->model_tool_image->resize($result['image'], 40, 40);
			} else {
				$image = $this->model_tool_image->resize('no_image.png', 40, 40);
			}

			$special = false;

			$product_specials = $this->model_catalog_product->getProductSpecials($result['product_id']);

			foreach ($product_specials  as $product_special) {
				if (($product_special['date_start'] == '0000-00-00' || strtotime($product_special['date_start']) < time()) && ($product_special['date_end'] == '0000-00-00' || strtotime($product_special['date_end']) > time())) {
					$special = $product_special['price'];

					break;
				}
			}

			$gg_query = $this->db->query("SELECT * FROM ".DB_PREFIX."ggproduct WHERE shop_id = '".$this->model_gittigidiyor_general->actShopid()."' AND product_id = '".$result['product_id']."'");
			if($gg_query->num_rows > 0){
				$ggprd = $gg_query->row;
			} else {
				$ggprd = array(
					'gg_id' => '0',
					'gg_title' => '0',
					'gg_subtitle' => '0',
					'gg_price' => '0',
					'gg_commission' => '0',
					'gg_description' => '0',
					'gg_salestatus' => '0',
					'gg_catid' => '0',
					'gg_cargocampany' => '0',
					'gg_attribute' => '0',
					'latest_update' => null,
				);
			}
			$ggprd['gg_catname'] = $this->model_gittigidiyor_category->getggnamebyid($ggprd['gg_catid']);
			$data['products'][] = array(
				'product_id' => $result['product_id'],
				'image'      => $image,
				'name'       => $result['name'],
				'model'      => $result['model'],
				'price'      => $result['price'],
				'special'    => $special,
				'quantity'   => $result['quantity'],
				'gg'		 => $ggprd,
				'status'     => ($result['status']) ? $this->language->get('text_enabled') : $this->language->get('text_disabled'),
				'edit'       => $this->url->link('catalog/product/edit', $token_link. '&product_id=' . $result['product_id'] . $url, 'SSL')
			);
		}
		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_model'])) {
			$url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_eslesme'])) {
			$url .= '&filter_eslesme=' . $this->request->get['filter_eslesme'];
		}

		if (isset($this->request->get['filter_price'])) {
			$url .= '&filter_price=' . $this->request->get['filter_price'];
		}

		if (isset($this->request->get['filter_quantity'])) {
			$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
		}

		if (isset($this->request->get['filter_category_id'])) {
			$url .= '&filter_category_id=' . $this->request->get['filter_category_id'];
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}


		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$pagination = new Pagination();
		$pagination->total = $product_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('gittigidiyor/product', $token_link . $url . '&page={page}', 'SSL');

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($product_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($product_total - $this->config->get('config_limit_admin'))) ? $product_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $product_total, ceil($product_total / $this->config->get('config_limit_admin')));

		$data['filter_name'] = $filter_name;
		$data['filter_category'] = $filter_category;
		$data['filter_category_id'] = $filter_category_id;
		$data['filter_manufacturer'] = $filter_manufacturer;
		$data['filter_manufacturer_id'] = $filter_manufacturer_id;
		$data['filter_status'] = $filter_status;
		$data['filter_ggstatus'] = $filter_ggstatus;
		$data['filter_eslesme'] = $filter_eslesme;
		$data['filter_model'] = $filter_model;
		$data['filter_price'] = $filter_price;
		$data['filter_quantity'] = $filter_quantity;

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['prodlink'] = $this->url->link('gittigidiyor/product/product_new', $token_link, 'SSL');
		$data['gonderilmemis'] = $this->url->link('gittigidiyor/product', $token_link, 'SSL');

		$this->model_gittigidiyor_general->gettheme($data, 'product');
	}


	public function product_new(){
		ini_set('error_reporting', E_ALL);
		if(VERSION < 3){
			$token = $this->session->data['token'];
			$token_link = 'token=' . $this->session->data['token'];
		} else {
			$token = $this->session->data['user_token'];
			$token_link = 'user_token=' . $this->session->data['user_token'];
		}

		$data = array();
		$this->load->model('gittigidiyor/general');
		$this->model_gittigidiyor_general->CreatePage('Ürün İşlemleri', array('gittigidiyor/product', 'gittigidiyor/category', 'tool/image', 'catalog/product'));

		$data['heading_title'] = 'Gittigidiyor Ürünler';
		$data['heading_title2'] = '';

		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', $token_link, 'SSL'),
		);
		$data['breadcrumbs'][] = array(
			'text'      => 'GG Ürünler',
			'href'      => $this->url->link('gittigidiyor/product', $token_link, 'SSL'),
		);

		$data['links'] = $this->model_gittigidiyor_general->getlinks();
		$data['shops'] = $this->model_gittigidiyor_general->getShops();
		$data['actshop_id'] = $this->model_gittigidiyor_general->actShopid();

		$data['token'] = $token;
		$data['token_link'] = $token_link;


		$data['prodlink'] = $this->url->link('gittigidiyor/product/product_new', $token_link, 'SSL');
		$data['gonderilmemis'] = $this->url->link('gittigidiyor/product', $token_link, 'SSL');

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = null;
		}

		if (isset($this->request->get['filter_model'])) {
			$filter_model = $this->request->get['filter_model'];
		} else {
			$filter_model = null;
		}

		if (isset($this->request->get['filter_sku'])) {
			$filter_sku = $this->request->get['filter_sku'];
		} else {
			$filter_sku = null;
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_model'])) {
			$url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_sku'])) {
			$url .= '&filter_sku=' . urlencode(html_entity_decode($this->request->get['filter_sku'], ENT_QUOTES, 'UTF-8'));
		}

		$limit = 50;
		if(isset($this->request->get['page'])){
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
		if(isset($this->request->get['status'])){
			$status = $this->request->get['status'];
		} else {
			$status = 'A';
		}
		$data['status'] = $status;

		$offset = ($page - 1) * $limit;

		if($filter_name or $filter_model){
			$filter_data = array(
				'filter_name'	  => $filter_name,
				'filter_model'	  => $filter_model,
				'filter_sku'	  => $filter_sku
			);

			$results = $this->model_gittigidiyor_product->getOpproducts($filter_data);
			foreach ($results as $prd) {
				$client = $this->model_gittigidiyor_general->getAuth();
				$prdcts = $client->getProduct(null, $prd['product_id']);
				$products = new StdClass;
				$products->products = new StdClass;
				$products->products->product = $prdcts->productDetail;
				$product_total = 1;
			}
		} else {
			$client = $this->model_gittigidiyor_general->getAuth();
			$products = $client->getProducts($offset, $limit, $status, true);
			if(isset($products->productCount)){
				$product_total = $products->productCount;
			} else {
				$product_total = 0;
			}
		}



		if($this->config->get('config_currency') != 'TRY'){
			$connect_web = simplexml_load_file('http://www.tcmb.gov.tr/kurlar/today.xml');
			$kur['USD'] = $connect_web->Currency[0]->BanknoteSelling;
			$kur['EUR'] = $connect_web->Currency[3]->BanknoteSelling;
		}


		$ggcatname = '- Bulunamadı -';
		$data['products'] = array();
		if($product_total > 0){
			if($product_total > 1){
				foreach ($products->products->product as $product) {
					if(isset($product->itemId)){
						$prodinfo = $this->model_catalog_product->getProduct($product->itemId);
					} else {
						$prodinfo = '';
					}

					if($prodinfo){
						if (is_file(DIR_IMAGE . $prodinfo['image'])) {
							$image = $this->model_tool_image->resize($prodinfo['image'], 40, 40);
						} else {
							$image = $this->model_tool_image->resize('no_image.png', 40, 40);
						}

						$special = false;

						$product_specials = $this->model_catalog_product->getProductSpecials($product->itemId);

						foreach ($product_specials  as $product_special) {
							if (($product_special['date_start'] == '0000-00-00' || strtotime($product_special['date_start']) < time()) && ($product_special['date_end'] == '0000-00-00' || strtotime($product_special['date_end']) > time())) {
								$special = $product_special['price'];

								break;
							}
						}

						$gg_query = $this->db->query("SELECT * FROM ".DB_PREFIX."ggproduct WHERE shop_id = '".$this->model_gittigidiyor_general->actShopid()."' AND product_id = '".$product->itemId."'");
						if($gg_query->num_rows > 0){
							$ggprd = $gg_query->row;
						} else {
							$ggprd = array(
								'gg_id' => $product->productId,
								'gg_title' => '0',
								'gg_subtitle' => '0',
								'gg_price' => '0',
								'gg_commission' => '0',
								'gg_description' => '0',
								'gg_salestatus' => '0',
								'gg_catid' => '0',
								'gg_cargocampany' => '0',
								'gg_attribute' => '0',
								'latest_update' => null,
							);
						}
						if(isset($product->product->categoryCode)){
							$ggcatname = $this->model_gittigidiyor_category->getggnamebyid($product->product->categoryCode);
						}

						if($this->config->get('config_currency') != 'TRY'){
							$price = (float)$prodinfo['price'] * (float)$kur[$this->config->get('config_currency')];
							$price = $this->model_gittigidiyor_product->vergihesapla($price, $prodinfo['tax_class_id'], true);
						} else {
							$price = (float)$prodinfo['price'];
							$price = $this->model_gittigidiyor_product->vergihesapla($price, $prodinfo['tax_class_id'], true);
						}


					 	$c = $product->product->buyNowPrice / 100;
 						$yuzde_kac = 100 - floor($price / $c);

						$data['products'][] = array(
							'image' => $image,
							'product_id' => $prodinfo['product_id'],
							'name'  => $prodinfo['name'],
							'ggcatname' => $ggcatname,
							'ggcatid' => $product->product->categoryCode,
							'model' => $prodinfo['model'],
							'price' => $price,
							'price_fark' => $yuzde_kac,
							'gg'		 => $ggprd,
							'quantity' => $prodinfo['quantity'],
							'lday' => $product->product->listingDays,
							'kargo' => $product->product->cargoDetail->shippingPayment,
							'gg_quantity' => $product->product->productCount,
							'gg_price' => $product->product->buyNowPrice,
							'special' => $special
						);
					} else {
						$ggprd = array(
								'gg_id' => $product->productId,
								'gg_title' => '0',
								'gg_subtitle' => '0',
								'gg_price' => '0',
								'gg_commission' => '0',
								'gg_description' => '0',
								'gg_salestatus' => '0',
								'gg_catid' => '0',
								'gg_cargocampany' => '0',
								'gg_attribute' => '0',
								'latest_update' => null,
							);
						if(isset($product->product->categoryCode)){
							$ggcatname = $this->model_gittigidiyor_category->getggnamebyid($product->product->categoryCode);
						}
						$data['products'][] = array(
							'image' => '',
							'name'  => $product->product->title,
							'product_id' => null,
							'ggcatname' => $ggcatname,
							'ggcatid' => $product->product->categoryCode,
							'price_fark' => 100,
							'model' => '- eski -',
							'price' => 0,
							'gg'		 => $ggprd,
							'quantity' => 0,
							'lday' => $product->product->listingDays,
							'kargo' => $product->product->cargoDetail->shippingPayment,
							'gg_quantity' => $product->product->productCount,
							'gg_price' => $product->product->buyNowPrice,
							'special' => ''
						);
					}
				}
			} else {
				$product = $products->products->product;

				$prodinfo = $this->model_catalog_product->getProduct($product->itemId);
					if($prodinfo){
						if (is_file(DIR_IMAGE . $prodinfo['image'])) {
							$image = $this->model_tool_image->resize($prodinfo['image'], 40, 40);
						} else {
							$image = $this->model_tool_image->resize('no_image.png', 40, 40);
						}



						$special = false;

						$product_specials = $this->model_catalog_product->getProductSpecials($product->itemId);

						foreach ($product_specials  as $product_special) {
							if (($product_special['date_start'] == '0000-00-00' || strtotime($product_special['date_start']) < time()) && ($product_special['date_end'] == '0000-00-00' || strtotime($product_special['date_end']) > time())) {
								$special = $product_special['price'];

								break;
							}
						}

						$gg_query = $this->db->query("SELECT * FROM ".DB_PREFIX."ggproduct WHERE shop_id = '".$this->model_gittigidiyor_general->actShopid()."' AND product_id = '".$product->itemId."'");
						if($gg_query->num_rows > 0){
							$ggprd = $gg_query->row;
						} else {
							$ggprd = array(
								'gg_id' => $product->product->productId,
								'gg_title' => '0',
								'gg_subtitle' => '0',
								'gg_price' => '0',
								'gg_commission' => '0',
								'gg_description' => '0',
								'gg_salestatus' => '0',
								'gg_catid' => '0',
								'gg_cargocampany' => '0',
								'gg_attribute' => '0',
								'latest_update' => null,
							);
						}
						if(isset($product->product->categoryCode)){
							$ggcatname = $this->model_gittigidiyor_category->getggnamebyid($product->product->categoryCode);
						}

						if($this->config->get('config_currency') != 'TRY'){
							$price = $prodinfo['price'] * $kur[$this->config->get('config_currency')];
							$price = $this->model_gittigidiyor_product->vergihesapla($price, $prodinfo['tax_class_id'], true);
						} else {
							$price = $prodinfo['price'];
							$price = $this->model_gittigidiyor_product->vergihesapla($price, $prodinfo['tax_class_id'], true);
						}

					 	$c = $product->product->buyNowPrice / 100;
 						$yuzde_kac = 100 - floor($price / $c);

						$data['products'][] = array(
							'image' => $image,
							'name'  => $prodinfo['name'],
							'product_id' => $prodinfo['product_id'],
							'ggcatname' => $ggcatname,
							'ggcatid' => $product->product->categoryCode,
							'model' => $prodinfo['model'],
							'price' => $price,
							'price_fark' => $yuzde_kac,
							'gg'		 => $ggprd,
							'quantity' => $prodinfo['quantity'],
							'lday' => $product->product->listingDays,
							'kargo' => $product->product->cargoDetail->shippingPayment,
							'gg_quantity' => $product->product->productCount,
							'gg_price' => $product->product->buyNowPrice,
							'special' => $special
						);
					} else {
						$ggprd = array(
							'gg_id' => $product->product->productId,
							'gg_title' => '0',
							'gg_subtitle' => '0',
							'gg_price' => '0',
							'gg_commission' => '0',
							'gg_description' => '0',
							'gg_salestatus' => '0',
							'gg_catid' => '0',
							'gg_cargocampany' => '0',
							'gg_attribute' => '0',
							'latest_update' => null,
						);
						if(isset($product->product->categoryCode)){
							$ggcatname = $this->model_gittigidiyor_category->getggnamebyid($product->product->categoryCode);
						}


						$data['products'][] = array(
							'image' => '',
							'ggcatname' => $ggcatname,
							'ggcatid' => $product->product->categoryCode,
							'product_id' => null,
							'name'  => $product->product->title,
							'model' => '- eski -',
							'price_fark' => 100,
							'price' => 0,
							'gg'		 => $ggprd,
							'quantity' => 0,
							'lday' => $product->product->listingDays,
							'kargo' => $product->product->cargoDetail->shippingPayment,
							'gg_quantity' => $product->product->productCount,
							'gg_price' => $product->product->buyNowPrice,
							'special' => ''
						);
					}
			}
		}

		$pagination = new Pagination();
		if(isset($products->productCount)){
		$pagination->total = $products->productCount;
		} else {
			$pagination->total = 0;
		}
		$pagination->page = $page;
		$pagination->limit = $limit;
		$pagination->url = $this->url->link('gittigidiyor/product/product_new', $token_link.'&status='.$status.'&page={page}', 'SSL');

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($product_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($product_total - $this->config->get('config_limit_admin'))) ? $product_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $product_total, ceil($product_total / $this->config->get('config_limit_admin')));

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_model'])) {
			$url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
		}

		$data['filter_name'] = $filter_name;
		$data['status'] = $status;
		$data['filter_model'] = $filter_model;
		$data['filter_sku'] = $filter_sku;

		$this->model_gittigidiyor_general->gettheme($data, 'product_new');
	}

	public function stokfiyatguncelle(){
		$data = array();
		$this->load->model('gittigidiyor/general');
		$this->load->model('gittigidiyor/product');
		$this->load->model('gittigidiyor/category');
		if(isset($this->request->get['product_id'])){
			$json = $this->model_gittigidiyor_product->fiyatstokGuncelle($this->model_gittigidiyor_general->getAuth(), $this->model_gittigidiyor_general->actShopid(), $this->request->get['product_id'], null);
		} else {
			if(isset($this->request->get['page'])){
				$page = $this->request->get['page'];
			} else {
				$page = 0;
			}
			$json = $this->model_gittigidiyor_product->fiyatstokGuncelle($this->model_gittigidiyor_general->getAuth(), $this->model_gittigidiyor_general->actShopid(), null, $page);
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function tumunuguncelle(){
		$data = array();
		$this->load->model('gittigidiyor/general');
		$this->load->model('gittigidiyor/product');
		$this->load->model('gittigidiyor/category');
		if(isset($this->request->get['product_id'])){
			$json = $this->model_gittigidiyor_product->urunleriGuncelle($this->model_gittigidiyor_general->getAuth(), $this->model_gittigidiyor_general->actShopid(), $this->request->get['product_id'], null);
		} else {
			if(isset($this->request->get['page'])){
				$page = $this->request->get['page'];
			} else {
				$page = 0;
			}
			$json = $this->model_gittigidiyor_product->urunleriGuncelle($this->model_gittigidiyor_general->getAuth(), $this->model_gittigidiyor_general->actShopid(), null, $page);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}


	public function sendProduct(){
		error_reporting(E_ALL);

		$data = array();
		$this->load->model('gittigidiyor/general');
		$this->load->model('gittigidiyor/product');
		$this->load->model('gittigidiyor/category');
		if(isset($this->request->get['product_id'])){
			$json = $this->model_gittigidiyor_product->SaveProduct($this->model_gittigidiyor_general->getAuth(), $this->model_gittigidiyor_general->actShopid(), $this->request->get['product_id'], null);
		} else {
			if(isset($this->request->get['page'])){
				$page = $this->request->get['page'];
			} else {
				$page = 0;
			}
			$json = $this->model_gittigidiyor_product->SaveProduct($this->model_gittigidiyor_general->getAuth(), $this->model_gittigidiyor_general->actShopid(), null, $page);
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function secilenlerigonder(){
		$data = array();
		$post = $this->request->post;
		$this->load->model('gittigidiyor/general');
		$this->load->model('gittigidiyor/product');
		$this->load->model('gittigidiyor/category');
		$msg = '';
		foreach ($post['prodids'] as $product_id) {
			$json = $this->model_gittigidiyor_product->SaveProduct($this->model_gittigidiyor_general->getAuth(), $this->model_gittigidiyor_general->actShopid(), $product_id, null);
			if($json['status'] == 1){
				$msg .= '<div class="alert alert-success" style="padding:5px !important; margin-bottom:5px;">'.$json['msg'].'</div>';
			} else {
				$msg .= '<div class="alert alert-danger" style="padding:5px !important; margin-bottom:5px;">'.$json['msg'].'</div>';
			}
		}
		echo $msg;
	}

	public function secilenlerisonlandir(){
		$data = array();
		$post = $this->request->post;
		$this->load->model('gittigidiyor/general');
		$this->load->model('gittigidiyor/product');
		$this->load->model('gittigidiyor/category');
		$msg = '';
		foreach ($post['prodids'] as $product_id) {
			$json = $this->model_gittigidiyor_product->satiSonlandir($this->model_gittigidiyor_general->getAuth(), $product_id, null, $this->model_gittigidiyor_general->actShopid());
			if($json['status'] == 1){
				$msg .= $json['msg'];
			} else {
				$msg .= $json['msg'];
			}
		}
		echo $msg;
	}

	public function secilenlerisil(){
		$data = array();
		$post = $this->request->post;
		$this->load->model('gittigidiyor/general');
		$this->load->model('gittigidiyor/product');
		$this->load->model('gittigidiyor/category');
		$msg = '';
		foreach ($post['prodids'] as $product_id) {
			$json = $this->model_gittigidiyor_product->DeleteProductBySellerCode($this->model_gittigidiyor_general->getAuth(), $product_id, null, $this->model_gittigidiyor_general->actShopid());
			if($json['status'] == 1){
				$msg .= $json['msg'];
			} else {
				$msg .= $json['msg'];
			}
		}
		echo $msg;
	}

	public function eszamanla(){
		$data = array();
		$this->load->model('gittigidiyor/general');
		$this->load->model('gittigidiyor/product');
		$this->load->model('gittigidiyor/category');
		if(isset($this->request->get['page'])){
			$page = $this->request->get['page'];
		} else {
			$page = 0;
		}
		$json = $this->model_gittigidiyor_product->eszamanla($this->model_gittigidiyor_general->getAuth(), $this->model_gittigidiyor_general->actShopid(), $page);
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function cateslestir(){
		$this->load->model('gittigidiyor/general');
		$this->load->model('gittigidiyor/product');
		$this->load->model('gittigidiyor/category');
		$gg = explode('|', $this->request->post['value']);
		$checkprod = $this->db->query("SELECT * FROM ".DB_PREFIX."ggproduct WHERE product_id = '".$this->request->post['pk']."'");
		if($checkprod->num_rows > 0){
			$this->db->query("UPDATE ".DB_PREFIX."ggproduct SET gg_catid = '".$gg[0]."' WHERE shop_id = '".$this->model_gittigidiyor_general->actShopid()."' AND product_id = '".$this->request->post['pk']."'");
		} else {
			$this->db->query("INSERT INTO ".DB_PREFIX."ggproduct SET gg_catid = '".$gg[0]."', shop_id = '".$this->model_gittigidiyor_general->actShopid()."', product_id = '".$this->request->post['pk']."'");
		}
		$json = array('product_id' => $this->request->post['pk'], 'ggcategory' => $gg[0]);
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	/*
		ürün açıklamasını getirelim
	*/
	public function getdesc(){
		$this->load->model('gittigidiyor/general');
		$this->load->model('gittigidiyor/product');
		$this->load->model('gittigidiyor/category');
		$check = $this->db->query("SELECT * FROM ".DB_PREFIX."ggproduct WHERE shop_id = '".$this->model_gittigidiyor_general->actShopid()."' AND product_id = '".$this->request->get['product_id']."'");
		if($check->num_rows > 0){
			$title = $check->row['gg_title'];
			$subtitle = $check->row['gg_subtitle'];
			$price = $check->row['gg_price'];
			$description = $check->row['gg_description'];
		} else {
			$title = '';
			$subtitle = '';
			$price = '';
			$description = '';
		}

		$html = '';
		$html .= '<form id="descform" method="post">';
		$html .= '<input type="hidden" name="product_id" value="'.$this->request->get['product_id'].'">';
		$html .='<div class="form-group"><label>Ürün Başlığı</label><input type="text" name="gg_title" class="form-control input-sm" value="'.$title.'"></div>';
		$html .='<div class="form-group"><label>Ürün Alt Başlığı</label><input type="text" name="gg_subtitle" class="form-control input-sm" value="'.$subtitle.'"></div>';
		$html .='<div class="form-group"><label>Ürün Fiyatı</label><input type="text" name="gg_price" class="form-control input-sm" value="'.$price.'"></div>';
		$html .='<div class="form-group"><label>Ürün Açıklaması</label><textarea name="gg_description" rows="25" placeholder="N11 Ürün Açıklaması" id="input-meta-n11description" class="form-control input-sm summernote">'.$description.'</textarea></div>';
		$html .= '</form>';
		echo $html;
	}

	/*
		ürün detaylarını kaydedelim
	*/
	public function descsave(){
		$this->load->model('n11/general');
		$this->load->model('n11/product');
		$this->load->model('n11/category');
		$post = $this->request->post;
		$checkprod = $this->db->query("SELECT * FROM ".DB_PREFIX."ggproduct WHERE shop_id = '".$this->model_gittigidiyor_general->actShopid()."' AND product_id = '".$post['product_id']."'");
		if($checkprod->num_rows > 0){
			$up = $this->db->query("UPDATE ".DB_PREFIX."ggproduct SET gg_title = '".$post['gg_title']."', gg_subtitle = '".$post['gg_subtitle']."', gg_description = '".$this->db->escape($post['gg_description'])."', gg_price = '".$post['gg_price']."' WHERE shop_id = '".$this->model_gittigidiyor_general->actShopid()."' AND product_id = '".$post['product_id']."'");
		} else {
			$up = $this->db->query("INSERT INTO ".DB_PREFIX."ggproduct SET shop_id = '".$this->model_gittigidiyor_general->actShopid()."', gg_title = '".$post['gg_title']."', gg_subtitle = '".$post['gg_subtitle']."', gg_description = '".$this->db->escape($post['gg_description'])."', gg_price = '".$post['gg_price']."'");
		}
		if($up){
			$json = array('status' => 1, 'msg' => 'Ürün bilgileri başarıyla düzenlendi');
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}


	/*
		ürün komisyon değiştirme
	*/
	public function changecomission(){
		$this->load->model('gittigidiyor/general');
		$this->load->model('gittigidiyor/product');
		$this->load->model('gittigidiyor/category');
		$post = $this->request->post;
		$checkprod = $this->db->query("SELECT * FROM ".DB_PREFIX."ggproduct WHERE shop_id = '".$this->model_gittigidiyor_general->actShopid()."' AND product_id = '".$post['pk']."'");
		if($checkprod->num_rows > 0){
			$this->db->query("UPDATE ".DB_PREFIX."ggproduct SET gg_commission = '".$post['value']."' WHERE shop_id = '".$this->model_gittigidiyor_general->actShopid()."' AND product_id = '".$post['pk']."'");
		} else {
			$this->db->query("INSERT INTO ".DB_PREFIX."ggproduct SET gg_commission = '".$post['value']."', product_id = '".$post['pk']."', shop_id = '".$this->model_gittigidiyor_general->actShopid()."'");
		}
	}

	/*
		ürün durumu değiştirme
	*/
	public function changeprodstatus(){
		$this->load->model('gittigidiyor/general');
		$this->load->model('gittigidiyor/product');
		$this->load->model('gittigidiyor/category');
		$post = $this->request->get;
		$checkprod = $this->db->query("SELECT * FROM ".DB_PREFIX."ggproduct WHERE shop_id = '".$this->model_gittigidiyor_general->actShopid()."' AND product_id = '".$post['product_id']."'");
		if($checkprod->num_rows > 0){
			$up = $this->db->query("UPDATE ".DB_PREFIX."ggproduct SET gg_salestatus = '".$post['status']."' WHERE product_id = '".$post['product_id']."' AND shop_id = '".$this->model_gittigidiyor_general->actShopid()."'");
		} else {
			$up = $this->db->query("INSERT INTO ".DB_PREFIX."ggproduct SET gg_salestatus = '".$post['status']."', product_id = '".$post['product_id']."', shop_id = '".$this->model_gittigidiyor_general->actShopid()."'");
		}
		if(isset($up) and $up){
			$json = array('status' => 1, 'msg' => 'Ürün durumu başarıyla değiştirildi');
		} else {
			$json = array('status' => 0, 'msg' => 'Ürün durumu değiştirilemedi. '. $result['msg']);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	/*
	mesajlar
	*/
	public function okundu(){
		$this->load->model('gittigidiyor/general');
		$this->load->model('gittigidiyor/product');
		$client = $this->model_gittigidiyor_general->getAuth();
		$okundu = $client->readMessage($this->request->post['queid']);
		if($okundu->ackCode == 'success'){
			$json = array('status' => 1);
		} else {
			$json = array('status' => 0);
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	/*
		soru cevapla
	*/

	public function savequestion(){
		$this->load->model('gittigidiyor/general');
		$this->load->model('gittigidiyor/product');
		$post = $this->request->post;
		$client = $this->model_gittigidiyor_general->getAuth();
		$mesaj_gonder = $client->sendNewMessage($post['to'], $post['title'], $post['yanit']);
		if($mesaj_gonder->ackCode == 'success'){
			$json = array('status' => 1, 'msg' => $mesaj_gonder->result);
		} else {
			$json = array('status' => 1, 'msg' => $mesaj_gonder->result);
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}


	public function chats(){
		ini_set('error_reporting', E_ALL);
		if(VERSION < 3){
			$token = $this->session->data['token'];
			$token_link = 'token=' . $this->session->data['token'];
		} else {
			$token = $this->session->data['user_token'];
			$token_link = 'user_token=' . $this->session->data['user_token'];
		}

		$data = array();
		$this->load->model('gittigidiyor/general');
		$this->load->model('gittigidiyor/product');
		$this->model_gittigidiyor_general->CreatePage('GG Gelen Kutusu', array('gittigidiyor/product'));
		$data['heading_title'] = 'Gittigidiyor Gelen Kutusu';
		$data['heading_title2'] = '';

		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', $token_link, 'SSL'),
		);
		$data['breadcrumbs'][] = array(
			'text'      => 'GG Mesaj Kutusu',
			'href'      => $this->url->link('gittigidiyor/product/questions', $token_link, 'SSL'),
		);

		$client = $this->model_gittigidiyor_general->getAuth();


		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			$send = $client->putMessage(array('conversationId' => $this->request->get['mesage_id'], 'message' => $this->request->post['message'], 'sendCopy' => 'true', 'lang' => 'tr'));
			if(isset($send->return->updated) and $send->return->updated == 'true'){
				$this->response->redirect($this->url->link('gittigidiyor/product/chats', $token_link. '&mesage_id='.$this->request->get['mesage_id'], true));
			} else {
				$this->session->data['error'] = $send->error->message;
			}
		}

		$data['links'] = $this->model_gittigidiyor_general->getlinks();
		$data['shops'] = $this->model_gittigidiyor_general->getShops();
		$data['actshop_id'] = $this->model_gittigidiyor_general->actShopid();

		$data['token'] = $token;
		$data['token_link'] = $token_link;

		if(isset($this->request->get['page'])){
			$page = $this->request->get['page'];
		} else {
			$page = 0;
		}
		if(isset($this->request->get['status'])){
			$status = $this->request->get['status'];
			$data['status'] = $status;
		} else {
			$status = '';
			$data['status'] = '';
		}
		$url = '';
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		if (isset($this->request->get['status'])) {
			$url .= '&status=' . $this->request->get['status'];
		}


		$start = ($page * 50) + 1;

		$message = $client->getMessages(array('conversationId' => $this->request->get['mesage_id'], 'startOffset' => $start, 'rowCount' => 100, 'lang' => 'tr'));
		$shop = $this->model_gittigidiyor_general->getShop($data['actshop_id']);

			$data['messages'] = array();
			if(isset($message->messages->message->messageId)){
				if($message->messages->message->senderNickName == $shop['nick']){
					$message->messages->message->floatz = 'right';
				} else {
					$message->messages->message->floatz = 'left';
				}
				$data['messages'][] = $message->messages->message;
			} else {
				foreach ($message->messages->message as  $value) {
					if($value->senderNickName == $shop['nick']){
						$value->floatz = 'right';
					} else {
						$value->floatz = 'left';
					}
					$data['messages'][] = $value;
				}
			}


		if(isset($this->session->data['error'])){
			$data['error'] = $this->session->data['error'];
			unset($this->session->data['error']);
		} else {
			$data['error'] = '';
		}
		$this->model_gittigidiyor_general->gettheme($data, 'chats');
	}

	public function questions(){
		ini_set('error_reporting', E_ALL);
		if(VERSION < 3){
			$token = $this->session->data['token'];
			$token_link = 'token=' . $this->session->data['token'];
		} else {
			$token = $this->session->data['user_token'];
			$token_link = 'user_token=' . $this->session->data['user_token'];
		}


		$data = array();
		$this->load->model('gittigidiyor/general');
		$this->load->model('gittigidiyor/product');

		$this->model_gittigidiyor_general->CreatePage('GG Gelen Kutusu', array('gittigidiyor/product'));


		$data['heading_title'] = 'Gittigidiyor Gelen Kutusu';
		$data['heading_title2'] = '';

		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', $token_link, 'SSL'),
		);
		$data['breadcrumbs'][] = array(
			'text'      => 'GG Mesaj Kutusu',
			'href'      => $this->url->link('gittigidiyor/product/questions', $token_link, 'SSL'),
		);

		$data['links'] = $this->model_gittigidiyor_general->getlinks();
		$data['shops'] = $this->model_gittigidiyor_general->getShops();
		$data['actshop_id'] = $this->model_gittigidiyor_general->actShopid();

		$data['token'] = $token;
		$data['token_link'] = $token_link;

		if(isset($this->request->get['page'])){
			$page = $this->request->get['page'];
		} else {
			$page = 0;
		}
		if(isset($this->request->get['status'])){
			$status = $this->request->get['status'];
			$data['status'] = $status;
		} else {
			$status = '';
			$data['status'] = '';
		}
		$url = '';
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		if (isset($this->request->get['status'])) {
			$url .= '&status=' . $this->request->get['status'];
		}

		$client = $this->model_gittigidiyor_general->getAuth();
		$start = ($page * 50) + 1;

		$message = $client->getInboxMessages(array('startOffSet' => $start, 'rowCount' => 100, 'messageType' => 'ALL', 'lang' => 'tr'));

		$shop = $this->model_gittigidiyor_general->getShop($data['actshop_id']);

			$data['questions'] = array();
			if(isset($message->conversationsCount) and $message->conversationsCount > 0){
				foreach ($message->conversations->conversation as  $value) {
					foreach ($value->participants->participant as $katilimcilar) {
						if($katilimcilar->nickName == $shop['nick']){
							$value->isRead = $katilimcilar->isRead;
						} else {
							$value->from = $katilimcilar->nickName;
						}
					}
					$data['questions'][] = $value;
				}
			}


		$pagination = new Pagination();
		$pagination->total = $message->conversationsCount;
		$pagination->page = $page;
		$pagination->limit = 50;
		$pagination->url = $this->url->link('gittigidiyor/product/questions', $token_link . $url . '&page={page}', 'SSL');

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($message->conversationsCount) ? (($page - 1) * 100) + 1 : 0, ((($page - 1) * 100) > ($message->conversationsCount - 100)) ? $message->conversationsCount : ((($page - 1) * 100) + 100), $message->conversationsCount, ceil($message->conversationsCount / 100));

		if(isset($this->session->data['error'])){
			$data['error'] = $this->session->data['error'];
			unset($this->session->data['error']);
		} else {
			$data['error'] = '';
		}
		$this->model_gittigidiyor_general->gettheme($data, 'inbox');
	}


	/*
 	  ürün özelliklerini kaydedelim
 	*/
 	public function attrsave(){
 		$this->load->model('gittigidiyor/general');
		$this->load->model('gittigidiyor/product');
		$post = $this->request->post;
		$up = $this->db->query("UPDATE ".DB_PREFIX."ggproduct SET gg_attribute = '".$this->db->escape(json_encode($post['prdattr']))."' WHERE product_id = '".$post['product_id']."' AND shop_id = '".$this->model_gittigidiyor_general->actShopid()."'");
		if($up){
			$json = array('status' => 1, 'msg' => 'Ürün özellikleri başarıyla düzenlendi');
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}


	public function getattr(){
		$this->load->model('gittigidiyor/general');
		$this->load->model('gittigidiyor/product');
		$client = $this->model_gittigidiyor_general->getAuth();
		if(isset($this->request->get['ggid'])){
			$ggid = $this->request->get['ggid'];
		} else {
			echo '<div class="alert alert-danger">Kategori Kodu Alınamadı</div>';
			exit;
		}
		$attr = $client->getCategorySpecs($ggid);


		if($attr->ackCode != 'success'){
			echo '<div class="alert alert-danger">'.$attr->error->message.'</div>';
			exit;
		}
		$op_attr = $this->db->query("SELECT * FROM ".DB_PREFIX."ggproduct WHERE product_id = '".$this->request->get['ocid']."'");
		$opattr = (array)json_decode($op_attr->row['gg_attribute']);

		$attrb = array();
		$html = '';
		$html .= '<form id="attrform" method="post">';
		$html .= '<input type="hidden" name="product_id" value="'.$this->request->get['ocid'].'">';
		foreach ($attr->specs->spec as $value){
			$atrlist[$value->name]['name'] = $value->name;
			$atrlist[$value->name]['type'] = $value->type;
			$atrlist[$value->name]['required'] = $value->required;
			if(is_array($value->values->value)){
				foreach ($value->values->value as $val) {
					if(isset($val)){
						$attr_values[$value->name][] = $val;
					}
				}
			} else {
				$attr_values[$value->name][] = $value->values->value;
			}
		}



		if(isset($attr_values)){
			foreach ($attr_values as $key => $value){
				$temiz_value = array_unique($value, SORT_STRING);
				$atrlist[$key]['value'] = $temiz_value;
			}
		}



		foreach ($atrlist as $key => $tatrr) {
			if($tatrr['required'] == 1){
				$zorunlu = 'required="required"';
				$html .= '<div class="form-group"><label>'.$key.' (Zorulu)</label>';
			} else {
				$html .= '<div class="form-group"><label>'.$key.'</label>';
				$zorunlu = '';
			}


			$html .= '<select name="prdattr['.$key.']" class="form-control input-sm" '.$zorunlu.'>';
			if($tatrr['required'] != 1){
				$html .= '<option value="">- Seçiniz -</option>';
			}


			if(isset($tatrr['value'])){
				rsort($tatrr['value']);
				foreach ($tatrr['value'] as $atrval){
					if(is_array($opattr) and count($opattr) > 0){
						foreach ($opattr as $ke => $val) {
							if($ke == $key){
								if($val == $atrval){
									$html .= '<option value="'.$atrval.'" selected>'.$atrval.'</option>';
								} else {
									$html .= '<option value="'.$atrval.'">'.$atrval.'</option>';
								}
							}
						}
					} else {

						$html .= '<option value="'.$atrval.'">'.$atrval.'</option>';
					}
				}
			}
			$html .= '</select></div>';
		}

		$html .= '</form>';
		echo $html;
	}

	public function delProduct(){
		$data = array();
		$this->load->model('gittigidiyor/general');
		$this->load->model('gittigidiyor/product');
		$this->load->model('gittigidiyor/category');
		$client = $this->model_gittigidiyor_general->getAuth();
		if(isset($this->request->get['product_id'])){
			$json = $this->model_gittigidiyor_product->DeleteProductBySellerCode($client, $this->request->get['product_id'], null, $this->model_gittigidiyor_general->actShopid());
		} else {
			if(isset($this->request->get['page'])){
				$page = $this->request->get['page'];
			} else {
				$page = 0;
			}
			$json = $this->model_gittigidiyor_product->DeleteProductBySellerCode($client, null, $page, $this->model_gittigidiyor_general->actShopid());
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function satiSonlandir(){
		$data = array();
		$this->load->model('gittigidiyor/general');
		$this->load->model('gittigidiyor/product');
		$this->load->model('gittigidiyor/category');
		$client = $this->model_gittigidiyor_general->getAuth();
		if(isset($this->request->get['product_id'])){
			$json = $this->model_gittigidiyor_product->satiSonlandir($client, $this->request->get['product_id'], null, $this->model_gittigidiyor_general->actShopid());
		} else {
			if(isset($this->request->get['page'])){
				$page = $this->request->get['page'];
			} else {
				$page = 0;
			}
			$json = $this->model_gittigidiyor_product->satiSonlandir($client, null, $page, $this->model_gittigidiyor_general->actShopid());
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
?>
