<?php
class ControllerGittigidiyorLogs extends Controller {
	public function index(){
		ini_set('error_reporting', E_ALL);
		if(VERSION < 3){
			$token = $this->session->data['token'];
			$token_link = 'token=' . $this->session->data['token'];
		} else {
			$token = $this->session->data['user_token'];
			$token_link = 'user_token=' . $this->session->data['user_token'];
		}

		$data = array();
		$this->load->model('gittigidiyor/general');
		$this->model_gittigidiyor_general->CreatePage('Gittigidiyor Loglar', array('gittigidiyor/category'));

		$data['heading_title'] = 'Gittigidiyor Loglar';
		$data['heading_title2'] = '';

		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', $token_link, 'SSL'),
		);
		$data['breadcrumbs'][] = array(
			'text'      => 'GG Loglar',
			'href'      => $this->url->link('gittigidiyor/logs', $token_link, 'SSL'),
		);
		$data['links'] = $this->model_gittigidiyor_general->getlinks();
		$data['shops'] = $this->model_gittigidiyor_general->getShops();
		$data['actshop_id'] = $this->model_gittigidiyor_general->actShopid();

		$data['token'] = $token;
		$data['token_link'] = $token_link;

		$data['clear'] = $this->url->link('gittigidiyor/logs/clear', $token_link, true);


		$data['log'] = '';

		$file = DIR_LOGS . 'gglogs.txt';

		if (file_exists($file)) {
			$size = filesize($file);

			if ($size >= 5242880) {
				$suffix = array(
					'B',
					'KB',
					'MB',
					'GB',
					'TB',
					'PB',
					'EB',
					'ZB',
					'YB'
				);

				$i = 0;

				while (($size / 1024) > 1) {
					$size = $size / 1024;
					$i++;
				}

				$data['error_warning'] = sprintf($this->language->get('error_warning'), basename($file), round(substr($size, 0, strpos($size, '.') + 4), 2) . $suffix[$i]);
			} else {
				$data['log'] = file_get_contents($file, FILE_USE_INCLUDE_PATH, null);
			}
		}

		$this->model_gittigidiyor_general->gettheme($data, 'log_list');
	}

	public function clear() {
		ini_set('error_reporting', E_ALL);
		if(VERSION < 3){
			$token = $this->session->data['token'];
			$token_link = 'token=' . $this->session->data['token'];
		} else {
			$token = $this->session->data['user_token'];
			$token_link = 'user_token=' . $this->session->data['user_token'];
		}

		$file = DIR_LOGS . 'gglogs.txt';
		$handle = fopen($file, 'w+');
		fclose($handle);
		$this->response->redirect($this->url->link('gittigidiyor/logs', $token_link, true));
	}
}
?>
