<?php
class ControllerGittigidiyorInfo extends Controller {
	public function index(){
		$data = array();
		$this->load->model('gittigidiyor/general');
		$this->document->setTitle('Gittigidiyor Entegrasyonu Hakkında');
		$this->document->addStyle('view/template/gittigidiyor/asset/global_style.css');
		$data['heading_title'] = 'Gittigidiyor Bilgi';
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'user_token=' . $this->session->data['user_token'], 'SSL'),
		);
		$data['breadcrumbs'][] = array(
			'text'      => 'GG Bilgi',
			'href'      => $this->url->link('gittigidiyor/info', 'user_token=' . $this->session->data['user_token'], 'SSL'),
		);
		$data['links'] = $this->model_gittigidiyor_general->getlinks();
		$data['shops'] = $this->model_gittigidiyor_general->getShops();
		$data['actshop_id'] = $this->model_gittigidiyor_general->actShopid();
		$data['user_token'] = $this->session->data['user_token'];
		$this->model_gittigidiyor_general->gettheme($data, 'info');
	}
}
?>
