<?php
class ControllerGittigidiyorInstall extends Controller {
	/*
		11 ilk kurulum yaparken çalışacak step by step panel
		adım 1 : mağaza ekleme bu alanda müşteri mağaza bilgilerini ekleyecektir
	*/
	public function index(){
		if(VERSION < 3){
			$token = $this->session->data['token'];
			$token_link = 'token=' . $this->session->data['token'];
		} else {
			$token = $this->session->data['user_token'];
			$token_link = 'user_token=' . $this->session->data['user_token'];
		}

		$data = array();
		unset($this->session->data['ggshop_id']);
		unset($this->session->data['action']);
		$this->load->model('gittigidiyor/general');
		$this->document->setTitle('Gittigidiyor Kurulum Ekranı - Mağaza Bilgileri');
		$this->document->addStyle('view/template/gittigidiyor/asset/global_style.css');
		$data['heading_title'] = 'Gittigidiyor Kurulum Ekranı - Mağaza Bilgileri';

		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			$this->model_gittigidiyor_general->addShop($this->request->post);
			$this->session->data['success'] = 'Mağaza başarıyla eklendi!';
			$this->response->redirect($this->url->link('gittigidiyor/install/step2', $token_link, 'SSL'));
		}

		$data['token'] = $token;
		$data['token_link'] = $token_link;

		$this->model_gittigidiyor_general->gettheme($data, 'install/step1');
	}



	public function step2(){
		if(VERSION < 3){
			$token = $this->session->data['token'];
			$token_link = 'token=' . $this->session->data['token'];
		} else {
			$token = $this->session->data['user_token'];
			$token_link = 'user_token=' . $this->session->data['user_token'];
		}
		$data = array();
		$this->load->model('gittigidiyor/general');
		$this->document->setTitle('GG Kurulum Ekranı - Varsayılan Değerler');
		$this->document->addStyle('view/template/gittigidiyor/asset/global_style.css');
		$data['heading_title'] = 'GG Kurulum Ekranı - Varsayılan Değerler';

		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			$this->model_gittigidiyor_general->updateshop($this->request->post);
			$this->session->data['success'] = 'Mağaza Ayarları Başarıyla Güncellendi!';
			if($this->request->post['iaction'] == 'edit'){
				$this->response->redirect($this->url->link('gittigidiyor/setting', $token_link, 'SSL'));
			} else {
				$this->response->redirect($this->url->link('gittigidiyor/install/step3', $token_link, 'SSL'));
			}
		}

		// şehirler
		$data['sehirler'] = array();
		$client = $this->model_gittigidiyor_general->getAuth();
		$sehirler = $client->getCities(0,100);
		if($sehirler->ackCode == 'success'){
			$data['sehirler'] = $sehirler->cities->city;
		}


		$data['token'] = $token;
		$data['token_link'] = $token_link;

		$data['shop_id'] = $this->session->data['ggshop_id'];
		$shop = $this->model_gittigidiyor_general->getShop($data['shop_id']);
		$data['shop'] = $shop;


		if(isset($this->session->data['install_action'])){
			$data['action'] = $this->session->data['install_action'];
		} else {
			$data['action'] = 'install';
		}
		
		$data['geri_don'] = $this->url->link('gittigidiyor/setting', $token_link, 'SSL');
		$this->model_gittigidiyor_general->gettheme($data, 'install/step2');
	}

	/* 
		Bu alanda müşterinin seçtiği anakategorinin alt kategorileri inecektir.
	*/
	public function step3(){
		if(VERSION < 3){
			$token = $this->session->data['token'];
			$token_link = 'token=' . $this->session->data['token'];
		} else {
			$token = $this->session->data['user_token'];
			$token_link = 'user_token=' . $this->session->data['user_token'];
		}

		$data = array();
		$this->load->model('gittigidiyor/general');
		$this->load->model('setting/setting');
		$this->model_gittigidiyor_general->CreatePage('Ana Kategori Seçimi', array('gittigidiyor/category'));
		
		$data['heading_title'] = 'GG Kurulum Ekranı - Ana Kategori Seçimi';
		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			$this->model_setting_setting->editSetting('gg_kat', $this->request->post);
			$this->session->data['success'] = 'Mağaza Ayarları Başarıyla Güncellendi!';
			if($this->request->post['iaction'] == 'edit'){
				$this->response->redirect($this->url->link('gittigidiyor/setting', $token_link, 'SSL'));
			} else {
				$this->response->redirect($this->url->link('gittigidiyor/install/step4', $token_link, 'SSL'));
			}
		}

		if(isset($this->session->data['install_action'])){
			$data['action'] = $this->session->data['install_action'];
		} else {
			$data['action'] = 'install';
		}

		$data['geri_don'] = $this->url->link('gittigidiyor/setting', $token_link, 'SSL');

		$data['token'] = $token;
		$data['token_link'] = $token_link;
		$data['shop_id'] = $this->session->data['ggshop_id'];
		$shop = $this->model_gittigidiyor_general->getShop($data['shop_id']);
		$data['shop'] = $shop;


		$data['gg_kategoriler'] = array();
		if (isset($this->request->post['gg_kategori'])) {
			$data['gg_kategoriler'] = $this->request->post['gg_kategori'];
		} else {
			$data['gg_kategoriler'] = $this->config->get('gg_kategori');
		}

		$data['ggcat'] = $this->model_gittigidiyor_category->gettoplvl($this->model_gittigidiyor_general->getAuth());


		$this->model_gittigidiyor_general->gettheme($data, 'install/step3');
	}

	public function step4(){
		if(VERSION < 3){
			$token = $this->session->data['token'];
			$token_link = 'token=' . $this->session->data['token'];
		} else {
			$token = $this->session->data['user_token'];
			$token_link = 'user_token=' . $this->session->data['user_token'];
		}

		$data = array();
		$this->load->model('gittigidiyor/general');
		$this->load->model('setting/setting');
		$this->model_gittigidiyor_general->CreatePage('Alt Kategoriler İndiriliyor', array('gittigidiyor/category'));
		$data['heading_title'] = 'GG Kurulum Ekranı - Alt Kategoriler İndiriliyor';

		if(isset($this->session->data['install_action'])){
			$data['action'] = $this->session->data['install_action'];
		} else {
			$data['action'] = 'install';
		}

		$data['geri_don'] = $this->url->link('gittigidiyor/setting', $token_link, 'SSL');

		$data['token'] = $token;
		$data['token_link'] = $token_link;
		$data['shop_id'] = $this->session->data['ggshop_id'];
		$shop = $this->model_gittigidiyor_general->getShop($data['shop_id']);
		$data['shop'] = $shop;		
		$this->model_gittigidiyor_general->gettheme($data, 'install/step4');
	}


	public function editing(){
		if(VERSION < 3){
			$token = $this->session->data['token'];
			$token_link = 'token=' . $this->session->data['token'];
		} else {
			$token = $this->session->data['user_token'];
			$token_link = 'user_token=' . $this->session->data['user_token'];
		}
		$this->session->data['install_action'] = $this->request->get['action'];
		$this->session->data['ggshop_id'] = $this->request->get['shop_id'];
		$this->response->redirect($this->url->link('gittigidiyor/install/step'.$this->request->get['step'], $token_link, 'SSL'));
	}

	public function checkkey(){
		date_default_timezone_set('Europe/Istanbul');
		$post = $this->request->post;
		try {
			$client = new Gittigidiyor();
			$client->apiKey = trim($post['apikey']);
			$client->secretKey = trim($post['secretkey']);
			$client->auth_user = trim($post['auth_user']);
			$client->auth_pass = trim($post['auth_pass']);
			$client->lang = 'tr';
			list($usec, $sec) = explode(" ", microtime());
			$time = round(((float)$usec + (float)$sec) * 100).'0';
			$client->time = $time;
			$client->sign = md5($post['apikey'].$post['secretkey'].$time);

			$sonuc = $client->getCities(0, 100);
			if($sonuc->ackCode == 'success'){
				$json = array('status' => 1, 'msg' => 'Mağaza Bilgileriniz Doğrudur, Devam Edebilirsiniz', 'btn' => '<button type="submit" class="btn btn-sm btn-primary pull-right">SONRAKİ ADIMA GEÇ</button>');
			} else {
				$json = array('status' => 0, 'msg' => 'Hata :'.$sonuc->result->errorMessage);
			}
		} catch (Exception $e) {
		    $json = array('status' => 0, 'msg' => 'Hata :'.  $e->getMessage());
		}
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
?>