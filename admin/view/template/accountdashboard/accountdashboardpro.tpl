<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      	<div class="pull-right">
        	<button type="submit" form="form-ciaccount-dashboard" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-check"></i> <?php echo $button_save; ?></button>
        </div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
        <div class="pull-right">
          <select name="store_id" onchange="window.location = 'index.php?route=accountdashboard/accountdashboardpro&token=<?php echo $token; ?>&store_id='+ this.value;">
            <option value="0"><?php echo $text_default; ?></option>
            <?php foreach($stores as $store) { ?>
            <option value="<?php echo $store['store_id']; ?>" <?php echo ($store['store_id'] == $store_id) ? 'selected="selected"' : ''; ?>><?php echo $store['name']; ?></option>
            <?php } ?>
          </select>
        </div>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-ciaccount-dashboard" class="form-horizontal">
		<ul class="nav nav-tabs">
			<li class="active"><a href="#tab-general" data-toggle="tab"><i class="fa fa-cogs"></i> <?php echo $tab_general; ?></a></li>
			<li><a href="#tab-profile" data-toggle="tab"><i class="fa fa-image"></i> <?php echo $tab_profile; ?></a></li>
			<li><a href="#tab-template" data-toggle="tab"><i class="fa fa-desktop"></i> <?php echo $tab_template; ?></a></li>
			<li><a href="#tab-accountdashboardpro-links" data-toggle="tab"><i class="fa fa-columns"></i> <?php echo $tab_customer_link; ?></a></li>
			<li><a href="#tab-design" data-toggle="tab"><i class="fa fa-eye-slash"></i> <?php echo $tab_design; ?></a></li>
			<li><a href="#tab-support" data-toggle="tab"><i class="fa fa-envelope"></i> <?php echo $tab_support; ?></a></li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active" id="tab-general">
				<div class="form-group">
					<label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
					<div class="col-sm-10">
					  <select name="accountdashboardpro_status" id="input-status" class="form-control">
						<?php if ($accountdashboardpro_status) { ?>
						<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
						<option value="0"><?php echo $text_disabled; ?></option>
						<?php } else { ?>
						<option value="1"><?php echo $text_enabled; ?></option>
						<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
						<?php } ?>
					  </select>
					</div>
				</div>
				<fieldset>
					<legend><?php echo $text_first ?></legend>
					<div class="form-group">
					  <label class="col-sm-2 control-label" for="input-latest-order"><?php echo $entry_latest_order; ?></label>
						<div class="col-sm-10">
						  <label class="radio-inline">
							<input type="radio" name="accountdashboardpro_latest_orders" value="1" <?php echo ($accountdashboardpro_latest_orders) ? 'checked="checked"' : ''; ?> />
							<?php echo $text_yes; ?>                
						  </label>
						  <label class="radio-inline">
							<input type="radio" name="accountdashboardpro_latest_orders" value="0" <?php echo (!$accountdashboardpro_latest_orders) ? 'checked="checked"' : ''; ?> />
							<?php echo $text_no; ?>
						  </label>
						</div>
					</div>
					<div class="form-group">
					  <label class="col-sm-2 control-label" for="input-orders"><?php echo $entry_total_orders; ?></label>
						<div class="col-sm-10">
						  <label class="radio-inline">
							<input type="radio" name="accountdashboardpro_total_orders" value="1" <?php echo ($accountdashboardpro_total_orders) ? 'checked="checked"' : ''; ?> />
							<?php echo $text_yes; ?>                
						  </label>
						  <label class="radio-inline">
							<input type="radio" name="accountdashboardpro_total_orders" value="0" <?php echo (!$accountdashboardpro_total_orders) ? 'checked="checked"' : ''; ?> />
							<?php echo $text_no; ?>
						  </label>
						</div>
					</div>
					<div class="form-group">
					  <label class="col-sm-2 control-label" for="input-transactions"><?php echo $entry_total_transactions; ?></label>
						<div class="col-sm-10">
						  <label class="radio-inline">
							<input type="radio" name="accountdashboardpro_total_transactions" value="1" <?php echo ($accountdashboardpro_total_transactions) ? 'checked="checked"' : ''; ?> />
							<?php echo $text_yes; ?>                
						  </label>
						  <label class="radio-inline">
							<input type="radio" name="accountdashboardpro_total_transactions" value="0" <?php echo (!$accountdashboardpro_total_transactions) ? 'checked="checked"' : ''; ?> />
							<?php echo $text_no; ?>
						  </label>
						</div>
					</div>						
					<div class="form-group">
					  <label class="col-sm-2 control-label" for="input-wishlists"><?php echo $entry_total_wishlist; ?></label>
						<div class="col-sm-10">
						  <label class="radio-inline">
							<input type="radio" name="accountdashboardpro_total_wishlists" value="1" <?php echo ($accountdashboardpro_total_wishlists) ? 'checked="checked"' : ''; ?> />
							<?php echo $text_yes; ?>                
						  </label>
						  <label class="radio-inline">
							<input type="radio" name="accountdashboardpro_total_wishlists" value="0" <?php echo (!$accountdashboardpro_total_wishlists) ? 'checked="checked"' : ''; ?> />
							<?php echo $text_no; ?>
						  </label>
						</div>
					</div>
					<div class="form-group">
					  <label class="col-sm-2 control-label" for="input-rewardpoints"><?php echo $entry_reward_points; ?></label>
						<div class="col-sm-10">
						  <label class="radio-inline">
							<input type="radio" name="accountdashboardpro_total_rewardpoints" value="1" <?php echo ($accountdashboardpro_total_rewardpoints) ? 'checked="checked"' : ''; ?> />
							<?php echo $text_yes; ?>                
						  </label>
						  <label class="radio-inline">
							<input type="radio" name="accountdashboardpro_total_rewardpoints" value="0" <?php echo (!$accountdashboardpro_total_rewardpoints) ? 'checked="checked"' : ''; ?> />
							<?php echo $text_no; ?>
						  </label>
						</div>
					</div>
				</fieldset>				
			</div>
			<div class="tab-pane" id="tab-profile">
				<div class="form-group">
					<label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
					<div class="col-sm-10">
					  <select name="accountdashboardpro_customer_picture_status" id="input-status" class="form-control">
						<?php if ($accountdashboardpro_customer_picture_status) { ?>
						<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
						<option value="0"><?php echo $text_disabled; ?></option>
						<?php } else { ?>
						<option value="1"><?php echo $text_enabled; ?></option>
						<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
						<?php } ?>
					  </select>
					</div>
				</div>				
				<div class="form-group">
				  <label class="col-sm-2 control-label" for="input-file-ext-allowed"><span data-toggle="tooltip" title="<?php echo $help_file_ext_allowed; ?>"><?php echo $entry_file_ext_allowed; ?></span></label>
				  <div class="col-sm-10">
					<textarea name="accountdashboardpro_file_ext_allowed" rows="5" placeholder="<?php echo $entry_file_ext_allowed; ?>" id="input-file-ext-allowed" class="form-control"><?php echo $accountdashboardpro_file_ext_allowed; ?></textarea>
				  </div>
				</div>
				<div class="form-group">
				  <label class="col-sm-2 control-label" for="input-file-mime-allowed"><span data-toggle="tooltip" title="<?php echo $help_file_mime_allowed; ?>"><?php echo $entry_file_mime_allowed; ?></span></label>
				  <div class="col-sm-10">
					<textarea name="accountdashboardpro_file_mime_allowed" rows="5" placeholder="<?php echo $entry_file_mime_allowed; ?>" id="input-file-mime-allowed" class="form-control"><?php echo $accountdashboardpro_file_mime_allowed; ?></textarea>
				  </div>
				</div>				
			</div>
			<div class="tab-pane" id="tab-template">
				<div class="form-group required">
	                <label class="col-sm-2 control-label" for="input-theme"><?php echo $entry_template;  ?></label>
	                <div class="col-sm-10">
	                  <select name="accountdashboardpro_template" id="input-template" class="form-control">
	                  	<?php foreach($templates as $template) { ?>
	                  	<?php if($template['template_id'] == $accountdashboardpro_template) { ?>
	                    <option value="<?php echo $template['template_id']; ?>" selected="selected"><?php echo $template['name'] ?></option>
	                    <?php } else{ ?>
	                    <option value="<?php echo $template['template_id']; ?>"><?php echo $template['name'] ?></option>
	                    <?php } ?>
	                    <?php } ?>
	                  </select> 
	                  <br>
	                  <img src="" alt="" id="template" class="img-thumbnail" />
	              	</div>
              	</div>
			</div>
			<div class="tab-pane" id="tab-accountdashboardpro-links">
				<div class="row">
                <div class="col-sm-4 col-xs-12 col-md-3">
                  <ul class="nav nav-pills nav-stacked" id="links">
                  <?php $link_row = 0; ?>
                    <?php foreach($links as $link) { ?>
                    <li class="links-li"><a href="#tab-link<?php echo $link_row; ?>" data-toggle="tab"><i class="fa fa-minus-circle" onclick="$('a[href=\'#tab-link<?php echo $link_row; ?>\']').parent().remove(); $('#tab-link<?php echo $link_row; ?>').remove(); $('#links a:first').tab('show');"></i> <?php echo (!empty($link['description'][$config_language_id]['title']) ? $link['description'][$config_language_id]['title'] : $tab_link .'-' . ($link_row + (int)1)); ?> <i class="fa fa-arrows pull-right" aria-hidden="true"></i></a></li>
                    <?php $link_row++; ?>
                    <?php } ?>
                  </ul>
                  <ul class="nav nav-pills nav-stacked addlinkbutton">
                    <li><button type="button" class="btn btn-default btn-block" onclick="addLink();"><i class="fa fa-plus-circle" aria-hidden="true"></i> <?php echo $button_add_link; ?></button></li>
                  </ul>
                </div>
                <div class="col-sm-8 col-xs-12 col-md-9">
                  <div class="tab-content" id="tab-content">
                  <?php $link_row = 0; ?>
                    <?php foreach($links as $link_key => $link) { ?>
                    <div class="tab-pane" id="tab-link<?php echo $link_row; ?>">

                        <div class="link-group">
                          <ul class="nav nav-tabs" id="link-language<?php echo $link_row; ?>">
                            <?php foreach ($languages as $language) { ?>
                            <li><a href="#link-language<?php echo $link_row; ?>-<?php echo $language['language_id']; ?>" data-toggle="tab">
                            <?php if(VERSION >= '2.2.0.0') { ?>
                            <img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /> 
                            <?php } else{ ?>
                            <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" />
                            <?php } ?> <?php echo $language['name']; ?></a></li>
                            <?php } ?>
                          </ul>
                          <div class="tab-content">
                            <?php foreach ($languages as $language) { ?>
                            <div class="tab-pane" id="link-language<?php echo $link_row; ?>-<?php echo $language['language_id']; ?>">
                              <div class="form-group required">
                                <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="<?php echo $help_title; ?>"><?php echo $entry_title; ?></span></label>
                                <div class="col-sm-10">
                                  <input type="text" name="accountdashboardpro_link[<?php echo $link_row; ?>][description][<?php echo $language['language_id']; ?>][title]" value="<?php echo isset($link['description'][$language['language_id']]['title']) ? $link['description'][$language['language_id']]['title'] : ''; ?>" placeholder="<?php echo $entry_title; ?>" class="form-control" />
                                  <?php if (isset($error_title[$link_key][$language['language_id']])) { ?>
                                  	<div class="text-danger"><?php echo $error_title[$link_key][$language['language_id']]; ?></div>
                                  <?php } ?>
                                </div>
                              </div>  
                            </div>  
                            <?php } ?>
                          </div>
                        </div>

                      <fieldset>
                        <legend><?php echo $text_type_setting ?></legend>
                        <div class="form-group required">
                          <label class="col-sm-2 control-label"><?php echo $entry_link; ?></label>
                          <div class="col-sm-10">
                            <input type="text" name="accountdashboardpro_link[<?php echo $link_row; ?>][link]" class="form-control" value="<?php echo $link['link']; ?>" />
                            <?php if (isset($errorlink[$link_key])) { ?>
	                          <div class="text-danger"><?php echo $errorlink[$link_key]; ?></div>
	                    	 <?php } ?>	
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2 control-label"><?php echo $entry_status; ?></label>
                          <div class="col-sm-10">
                            <select name="accountdashboardpro_link[<?php echo $link_row; ?>][status]" class="form-control link-status" rel="<?php echo $link_row; ?>">
                              <?php if ($link['status']) { ?>
                              <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                              <option value="0"><?php echo $text_disabled; ?></option>
                              <?php } else { ?>
                              <option value="1"><?php echo $text_enabled; ?></option>
                              <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                              <?php } ?>
                            </select>
                          </div>
                        </div>
	                     <div class="form-group">
							<label class="col-sm-2 control-label"><?php echo $entry_icontype; ?></label>
							<div class="col-sm-10">
								<label class="radio-inline icon_type">
									<?php if ($link['type'] == 'image') { ?>
										<input type="radio" rel="<?php echo $link_row; ?>" name="accountdashboardpro_link[<?php echo $link_row; ?>][type]" value="image"  checked="checked" /> <?php echo $text_type1; ?>
									<?php } else { ?>
										<input type="radio" rel="<?php echo $link_row; ?>" name="accountdashboardpro_link[<?php echo $link_row; ?>][type]" value="image" /> <?php echo $text_type1; ?>
									<?php } ?>
								</label>
								<label class="radio-inline icon_type">
									<?php if ($link['type'] == 'icon') { ?>
										<input type="radio" rel="<?php echo $link_row; ?>" name="accountdashboardpro_link[<?php echo $link_row; ?>][type]" value="icon" checked="checked" /> <?php echo $text_type2; ?>
									<?php } else { ?>
										<input type="radio" rel="<?php echo $link_row; ?>" name="accountdashboardpro_link[<?php echo $link_row; ?>][type]" value="icon" /> <?php echo $text_type2; ?>
									<?php } ?>
								</label>
							</div>
						</div>
						<div class="linkimage-<?php echo $link_row; ?>" style="<?php if($link['type'] == 'icon') { echo 'display: none;'; } ?>" id="linkimage-<?php echo $link_row; ?>">
							<div class="form-group required">
								<label class="col-sm-2 control-label"><?php echo $entry_image; ?></label>
								<div class="col-sm-10"><a href="" id="thumb-image<?php echo $link_row; ?>" data-toggle="image" class="img-thumbnail img-circle"><img class="img-responsive img-circle" src="<?php echo $link['thumb']; ?>"  alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
									<input type="hidden" name="accountdashboardpro_link[<?php echo $link_row; ?>][image]" value="<?php echo $link['image']; ?>" id="input-image<?php echo $link_row; ?>" />	
									<?php if (isset($error_image[$link_key])) { ?>
	                                  <div class="text-danger"><?php echo $error_image[$link_key]; ?></div>
                                	 <?php } ?>								
								</div>
							</div>              
						</div>
						<div class="linkicon-<?php echo $link_row; ?>" style="<?php if($link['type'] == 'image') { echo 'display: none;'; } ?>" id="linkicon-<?php echo $link_row; ?>">
							<div class="form-group required">
								<label class="col-sm-2 control-label" for="input-icon_class"><?php echo $entry_iconclass ?></label>
								<div class="col-sm-10">
									<input type="text" class="form-control" name="accountdashboardpro_link[<?php echo $link_row; ?>][icon_class]" placeholder="<?php echo $entry_iconclass ?>" value="<?php echo $link['icon_class'] ; ?>" />
									<?php if (isset($error_icon_class[$link_key])) { ?>
	                                  <div class="text-danger"><?php echo $error_icon_class[$link_key]; ?></div>
                                	 <?php } ?>
								</div>
							</div>
						</div>
                        <div class="form-group hide">
                          <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="<?php echo $help_sort_order; ?>"><?php echo $entry_sort_order; ?></span></label>
                          <div class="col-sm-10">
                            <input type="text" name="accountdashboardpro_link[<?php echo $link_row; ?>][sort_order]"  value="<?php echo $link['sort_order']; ?>" class="form-control link-sortorder" />
                          </div>
                        </div>
                      </fieldset>
                    </div>
                    <?php $link_row++; ?>
                  <?php } ?>
                  </div>
                </div>
              </div>				
			</div>
			<div class="tab-pane" id="tab-design">
				<div class="form-group">
                    <label class="col-sm-2 control-label" for="input-accountdashboardpro-custom"><span data-toggle="tooltip" title="<?php echo $help_customcss; ?>"><?php echo $entry_customcss; ?></span></label>
                    <div class="col-sm-10">
                      <textarea name="accountdashboardpro_customcss" rows="10" placeholder="<?php echo $entry_customcss; ?>" id="input-accountdashboardpro-custom" class="form-control"><?php echo $accountdashboardpro_customcss; ?></textarea>
                    </div>
                  </div>
			  	<div class="form-group">
					<label class="col-sm-2 control-label"><span data-toggle="tooltip" title="<?php echo $help_theme_color; ?>"><?php echo $entry_theme_color; ?></span></label>
            		    <div class="col-sm-8">
							<input type="text" name="accountdashboardpro_theme_color" value="<?php echo $accountdashboardpro_theme_color; ?>" placeholder="<?php echo $entry_theme_color;?>" class="form-control color-picker" />
                      	</div>
                      	<div class="col-sm-2">
                      		<div class="preview"></div>
                      	</div>
                </div>                    
                <div class="form-group">
					<label class="col-sm-2 control-label"><span data-toggle="tooltip" title="<?php echo $help_text_color; ?>"><?php echo $entry_text_color; ?></span></label>
            		    <div class="col-sm-8">
							<input type="text" name="accountdashboardpro_text_color" value="<?php echo $accountdashboardpro_text_color; ?>" placeholder="<?php echo $entry_text_color; ?>" class="form-control color-picker" />
                      	</div>
                      	<div class="col-sm-2">
                      		<div class="preview"></div>
                      	</div>
                </div>
			</div>
			<div class="tab-pane" id="tab-support">
				<p class="text-center">For Support and Enquiry Please Feel Free To Contact</p>
				<p class="text-center"><strong>codinginspect@gmail.com</strong></p>
			</div>
          </div>
        </form>
      </div>
    </div>
  </div>  
   <script type="text/javascript"><!--
$('select[name=\'accountdashboardpro_template\']').on('change', function() {
	$.ajax({
		url: 'index.php?route=accountdashboard/accountdashboardpro/template&token=<?php echo $token; ?>&template=' + this.value,
		dataType: 'html',
		beforeSend: function() {
			$('select[name=\'accountdashboardpro_template\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
		},
		complete: function() {
			$('.fa-spin').remove();
		},
		success: function(html) {
			$('#template').attr('src', html);
		}
	});
});

$('select[name=\'accountdashboardpro_template\']').trigger('change');
//--></script>
<script type="text/javascript"><!--
$('#language a:first').tab('show');
  $('#customer-email-language a:first').tab('show');
  $('#admin-email-language a:first').tab('show');
  $('#success-language a:first').tab('show');

  $('#email a:first').tab('show');

  $('#links li:first-child a').tab('show');
  <?php $j = 0; foreach($links as $key => $link) { ?>
  $('#link-language<?php echo $j; ?> li:first-child a').tab('show');
  <?php $j++; } ?>
//--></script>
<script type="text/javascript"><!--
$('#links a:first').tab('show');

var link_row = <?php echo $link_row; ?>;

function addLink() {
  html = '<div class="tab-pane" id="tab-link' + link_row + '">';

      html += '<div class="link-group">';
        html += '<ul class="nav nav-tabs" id="link-language' + link_row + '">';
          <?php foreach ($languages as $language) { ?>
          html += '<li><a href="#link-language' + link_row + '-<?php echo $language['language_id']; ?>" data-toggle="tab">';
          <?php if(VERSION >= '2.2.0.0') { ?>
          html += '<img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" />';
          <?php } else{ ?>
          html += '<img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" />';
          <?php } ?>
         html += ' <?php echo $language['name']; ?></a></li>';
          <?php } ?>
        html += '</ul>';
        html += '<div class="tab-content">';
          <?php foreach ($languages as $language) { ?>
          html += '<div class="tab-pane" id="link-language' + link_row + '-<?php echo $language['language_id']; ?>">';
            html += '<div class="form-group required">';
              html += '<label class="col-sm-2 control-label"><span data-toggle="tooltip" title="<?php echo $help_title; ?>"><?php echo $entry_title; ?></span></label>';
              html += '<div class="col-sm-10">';
                html += '<input type="text" name="accountdashboardpro_link[' + link_row + '][description][<?php echo $language['language_id']; ?>][title]" value="" placeholder="<?php echo $entry_title; ?>" class="form-control" />';
              html += '</div>';
            html += '</div>';
           html += '</div>';
          <?php } ?>
        html += '</div>';
      html += '</div>';

    html += '<fieldset>';
      html += '<legend><?php echo $text_type_setting ?></legend>';
  		html += '<div class="form-group required">';
          html += '<label class="col-sm-2 control-label"><?php echo $entry_link; ?></label>';
          html += '<div class="col-sm-10">';
            html += '<input type="text" name="accountdashboardpro_link[' + link_row + '][link]" class="form-control" value="" />';
          html += '</div>';
        html += '</div>';
          html += '<div class="form-group">';
            html += '<label class="col-sm-2 control-label"><?php echo $entry_status; ?></label>';
            html += '<div class="col-sm-10">';
              html += '<select name="accountdashboardpro_link[' + link_row + '][status]" class="form-control link-status" rel="' + link_row + '">';
                html += '<option value="1" selected="selected"><?php echo $text_enabled; ?></option>';
                html += '<option value="0"><?php echo $text_disabled; ?></option>';
              html += '</select>';
            html += '</div>';
          html += '</div>';
          html += '<div class="form-group">';
			html += '<label class="col-sm-2 control-label"><?php echo $entry_icontype; ?></label>';
			 html += '<div class="col-sm-10">';
		      html += '<label class="radio-inline icon_type">';			  		
				 html += '<input type="radio" rel="'+ link_row +'"  name="accountdashboardpro_link[' + link_row + '][type]" value="image" /> <?php echo $text_type1; ?>';
					 html += '</label>';
					html += '<label class="radio-inline icon_type">';		
	 				html += '<input type="radio"  rel="'+ link_row +'" name="accountdashboardpro_link[' + link_row + '][type]" value="icon" checked="checked"/> <?php echo $text_type2; ?>';				 
				html += '</label>';
			html += '	</div>';	
		  html += '</div>';

		html += '<div style="display: none;" class="linkimage-'+ link_row +'" id="linkimage-'+ link_row +'">	';
			html += '<div class="form-group required">';
			 html += '<label class="col-sm-2 control-label"><?php echo $entry_image; ?></label>';
			   html += '<div class="col-sm-10"><a href="" id="thumb-image'+ link_row +'" data-toggle="image" class="img-thumbnail img-circle"><img class="img-responsive img-circle" src="<?php echo $placeholder; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>';
				html += '<input type="hidden" name="accountdashboardpro_link[' + link_row + '][image]" value="" id="	input-image" />';
				html += '	</div>	';	
			  html += '</div>';              
			html += '</div>	';                                            
		html += '<div class="linkicon-'+ link_row +'" id="linkicon-'+ link_row +'">';
		 	html += '<div class="form-group required">';
				html += '<label class="col-sm-2 control-label" for="input-icon_class"><?php echo $entry_iconclass ?></label>';
				 html += '<div class="col-sm-10">';
					html += '<input type="text" class="form-control" name="accountdashboardpro_link[' + link_row + '][icon_class]" placeholder="<?php echo $entry_iconclass ?>" value="" />';
				html += '	</div>';
			html += '</div>';
		html += '</div>';																	
          html += '<div class="form-group hide">';
            html += '<label class="col-sm-2 control-label"><span data-toggle="tooltip" title="<?php echo $help_sort_order; ?>"><?php echo $entry_sort_order; ?></span></label>';
            html += '<div class="col-sm-10">';
              html += '<input type="text" name="accountdashboardpro_link[' + link_row + '][sort_order]"  value="' + (link_row + 1) + '" class="form-control link-sortorder" />';
            html += '</div>';
          html += '</div>';        
    html += '</fieldset>';  
  html += '</div>';

  $('#tab-accountdashboardpro-links #tab-content').append(html);

  $('#links').append('<li class="links-li"><a href="#tab-link' + link_row + '" data-toggle="tab"><i class="fa fa-minus-circle" onclick=" $(\'#links a:first\').tab(\'show\');$(\'a[href=\\\'#tab-link' + link_row + '\\\']\').parent().remove(); $(\'#tab-link' + link_row + '\').remove();"></i> <?php echo $tab_link; ?>-'+ (link_row + 1)  +' <i class="fa fa-arrows pull-right" aria-hidden="true"></i></a></li>');

  $('#links a[href=\'#tab-link' + link_row + '\']').tab('show');
  
  $('#link-language'+ link_row +' a:first').tab('show');

  $('[data-toggle=\'tooltip\']').tooltip({
    container: 'body',
    html: true
  });

  link_row++;
}
//--></script>
<script type="text/javascript"><!--
$(document).ready(function() {
  $("#links").sortable({
    cursor: "move",
    stop: function() {
      $('#links .links-li').each(function() {
        $($(this).find('a').attr('href')).find('.link-sortorder').val(($(this).index() + 1));
      });
    }
  });
});
//--></script>
<script type="text/javascript"><!--
$(document).delegate('.icon_type input[type=\'radio\']', 'change', function() {
	var icon_type = $(this).val();
	var rel = $(this).attr('rel');
	if(icon_type == 'icon') {
		$('#linkicon-'+ rel).show('slow');
		$('#linkimage-'+ rel).hide('slow');
	}else{
		$('#linkicon-'+ rel).hide('slow');
		$('#linkimage-'+ rel).show('slow');
	}
});
//--></script>
<script type="text/javascript">
  var element = null;
  $('.color-picker').ColorPicker({
    curr : '',
    onShow: function (colpkr) {
      $(colpkr).fadeIn(500);
      return false;
    },
    onHide: function (colpkr) {
      $(colpkr).fadeOut(500);
    return false;
    },
    onSubmit: function(hsb, hex, rgb, el) {
      $(el).val('#'+hex);
      $(el).ColorPickerHide();
    },
    onBeforeShow: function () {
      $(this).ColorPickerSetColor(this.value);
    },
    onChange: function (hsb, hex, rgb) {
      element.curr.parent().next().find('.preview').css('background', '#' + hex);
      element.curr.val('#'+hex);
    }
  }).bind('keyup', function(){
    $(this).ColorPickerSetColor(this.value);
  }).click(function(){
    element = this;
    element.curr = $(this);
  });

  $.each($('.color-picker'),function(key,value) {
    $(this).parent().next().find('.preview').css({'background': $(this).val()});
  });


$(document).ready(function() {

	var link_tabs = [];
	$('#tab-content > .tab-pane').each(function() {
		if($(this).has('.has-error').length) {
			link_tabs.push('#'+$(this).attr('id'));
		}
	});

	$('#links > .links-li').each(function() {
		// found error ids
		$(this).removeClass('tab-error');
		if( link_tabs.indexOf($(this).find('a').attr('href')) >= 0 ) {
			$(this).addClass('tab-error');
		}
	});
});
</script>
<style>
.preview { width: 45px;	height: 35px; border: 1px solid #ccc; }
.links-li i.pull-right{ margin-top: 2px; }
.addlinkbutton{ padding-top: 5px; }

.tab-error a { background-color: #f56b6b; color: #fff; }
</style>
</div>
<?php echo $footer; ?>