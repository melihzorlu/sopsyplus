
<?php if($coupon_code){ ?>
<div class="alert alert-danger col-sm-12">Daha önce bu <b><?=$coupon_code;?></b> Kupon kodu oluşturulmuştur!</div>
<?php } ?>

<div class="form-group">
    <label class="col-sm-2 control-label" for="input-coupon-cost">Kupon Adı</label>
    <div class="col-sm-10">
        <input type="text" name="coupon_name" value="<?=$coupon_name;?>" id="input-coupon-cost" class="form-control" placeholder="Kupon Adı: <?=$price_total;?>"/>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label" for="input-coupon-cost">Kupon Tutarı</label>
    <div class="col-sm-10">
        <input type="text" name="coupon_cost" value="" id="input-coupon-cost" class="form-control" placeholder="Tahmini kupon tutarı: <?=$price_total;?>"/>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label" for="input-coupon-code">Kupon Kodu</label>
    <div class="col-sm-2">
        <input type="text" name="coupon_code" value="" id="input-coupon-code" class="form-control"/>
    </div>
    <button class="btn btn-primary" id="make_coupon_code">Kod Oluştur</button>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label" for="input-coupon-code">Kampanyada Kullanılmasına İzin Ver</label>
    <div class="col-sm-2">
        <input type="checkbox" name="use_campaign" value="1" id="input-use-campaign" class="form-control"/>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label" for="input-coupon-cost">İade talebinde bulunulan ürünler</label>
    <div class="col-sm-10">
        <ul>
            <?php foreach($return_products_list as $item){ ?>
            <li><?=$item['name'];?> - <b style="color: red"></b></li>
            <?php } ?>
        </ul>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label" for="input-coupon-code"></label>
    <button class="btn btn-warning" id="make_coupon_code_done">Kupon Oluşturma İşlemini Bitir</button>
</div>

<div class="form-group" id="alert_box">
    <label class="col-sm-2 control-label" for="input-coupon-code"></label>
</div>



<script type="text/javascript"><!--

    $('#make_coupon_code_done').on('click', function(e) {



        if($('#input-use-campaign').prop('checked')) {
            var use_campaign = 1;
        } else {
            var use_campaign = 0;
        }

        e.preventDefault();
        $.ajax({
            url: 'index.php?route=sale/return/returnCouponMakeDone&token=<?=$token;?>&order_id=<?=$order_id;?>&return_id=<?=$return_id;?>',
            type: 'POST',
            dataType: 'json',
            data: '&coupon_name=' + $('input[name=\'coupon_name\']').val() + '&coupon_cost=' + $('input[name=\'coupon_cost\']').val()
                + '&coupon_code=' + $('input[name=\'coupon_code\']').val() + '&use_campaign=' + use_campaign,
            beforeSend: function() {
                $('#make_coupon_code_done').button('loading');
                $('.alert').remove();
            },
            complete: function() {
                $('#make_coupon_code_done').button('reset');
            },
            success: function(json) {
                if(json['warning']){
                    $('#alert_box').append('<div class="alert alert-danger col-sm-4">'+ json['warning'] +'</div>');
                }

                if(json['success']){
                    $('#alert_box').append('<div class="alert alert-success col-sm-10"><i class="fa fa-check-circle"></i>'+ json['success'] +'</div>');
                }
            }
        });
    });

    $('#make_coupon_code').on('click', function(e) {
        e.preventDefault();

        $.ajax({
            url: 'index.php?route=sale/return/returnCouponMake&token=<?=$token;?>',
            type: 'post',
            dataType: 'json',
            data: '',
            beforeSend: function() {
                $('#make_coupon_code').button('loading');
            },
            complete: function() {
                $('#make_coupon_code').button('reset');
            },
            success: function(json) {

                $('input[name=\'coupon_code\']').val(json['coupon_code']);
            }
        });
    });

    //--></script>