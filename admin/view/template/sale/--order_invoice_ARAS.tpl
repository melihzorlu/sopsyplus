<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<head>
<meta charset="UTF-8" />
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<link href="//scripts.piyersoft.com/stylesheet/bootstrap.css" rel="stylesheet" media="all" />
<script type="text/javascript" src="//scripts.piyersoft.com/javascript/jquery/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="//scripts.piyersoft.com/javascript/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="//scripts.piyersoft.com/ps-panel/javascripts/JsBarcode.all.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" type="text/css" rel="stylesheet" />
<link type="text/css" href="//scripts.piyersoft.com/stylesheet/stylesheet.css" rel="stylesheet" media="all" />
</head>
<body>
<div class="container">
  <?php foreach ($orders as $order) { ?>
  <?php for ($kargo_paketno=1; $kargo_paketno <= $order['aras_kargo_info']['kargo_paketadet']; $kargo_paketno++) {
	$piece_id= 1000000 + $order['order_id'] . substr(('0' . $kargo_paketno),-2);
  ?>
  <div style="page-break-after: always;">
    <h1><?php echo $text_invoice; ?> #<?php echo $order['order_id']; ?></h1>
    <table class="table table-bordered">
      <thead>
        <tr>
          <td colspan="2"><?php echo $text_order_detail; ?></td>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td style="width: 50%;"><address>
            <strong><?php echo $order['store_name']; ?></strong><br />
            <?php echo $order['store_address']; ?>
            </address>
            <b><?php echo $text_telephone; ?></b> <?php echo $order['store_telephone']; ?><br />
            <?php if ($order['store_fax']) { ?>
            <b><?php echo $text_fax; ?></b> <?php echo $order['store_fax']; ?><br />
            <?php } ?>
            <b><?php echo $text_email; ?></b> <?php echo $order['store_email']; ?><br />
            <b><?php echo $text_website; ?></b> <a href="<?php echo $order['store_url']; ?>"><?php echo $order['store_url']; ?></a></td>
          <td style="width: 50%;"><b><?php echo $text_date_added; ?></b> <?php echo $order['date_added']; ?><br />
            <?php if ($order['invoice_no']) { ?>
            <b><?php echo $text_invoice_no; ?></b> <?php echo $order['invoice_no']; ?><br />
            <?php } ?>
            <b><?php echo $text_order_id; ?></b> <?php echo $order['order_id']; ?><br />
            <b><?php echo $text_payment_method; ?></b> <?php echo $order['payment_method']; ?><br />
            <?php if ($order['shipping_method']) { ?>
            <b><?php echo $text_shipping_method; ?></b> <?php echo $order['shipping_method']; ?><br />
            <?php } ?></td>
        </tr>
      </tbody>
    </table>

    <h1>ARAS KARGO GÖNDERİ BİLGİLERİ <?=$kargo_paketno?>/<?=$order['aras_kargo_info']['kargo_paketadet']?></h1>
    <table class="table table-bordered">
      <thead>
        <tr>
          <td style="width: 50%;"><b><?php echo $text_shipping_address; ?></b></td>
          <td style="width: 50%;"><b>İletişim</b></td>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td><address>
            <?php if(empty($order['shipping_address'])){ echo $order['payment_address'];} else { echo $order['shipping_address']; } ?>
            </address></td>
          <td><address>
            <?php echo $order['email']; ?><br/>
            <?php echo $order['telephone']; ?>
            </address></td>
        </tr>
        <tr>
          <td>Kargo Barkodu<br/><svg class="barcode" data-piece_id="<?=$order['aras_kargo_info']['kargo_barcode']?>"></svg></td>
          <td>Parça Barkodu<br/><svg class="barcode" data-piece_id="<?=$piece_id?>"></svg></td>
        </tr>
      </tbody>
    </table>

	</div>
  <?php } ?>
  <?php } ?>
</div>
	<script>
	$('.barcode').each(function(){
		var piece_id = $(this).data('piece_id');
		JsBarcode(this, piece_id);
	});
	</script>
</body>
</html>