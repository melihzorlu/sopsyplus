<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" id="button-shipping" form="form-order" formaction="<?php echo $shipping; ?>" formtarget="_blank" data-toggle="tooltip" title="<?php echo $button_shipping_print; ?>" class="btn btn-info"><i class="fa fa-truck"></i></button>
        <button type="submit" id="button-invoice" form="form-order" formaction="<?php echo $invoice; ?>" formtarget="_blank" data-toggle="tooltip" title="<?php echo $button_invoice_print; ?>" class="btn btn-info"><i class="fa fa-print"></i></button>
        <a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
        <a href="<?php echo $excel_export; ?> "  data-toggle="tooltip" title="Excel ile dışarı aktar.TARİH SEÇMEDEN İNDİRMEYİNİZ!" class="btn btn-info" ><i class="fa fa-file-excel-o"></i></a>
        <button type="button" id="button-delete" form="form-order" formaction="<?php echo $delete; ?>" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger"><i class="fa fa-trash-o"></i></button>
      </div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
      </div>
      <div class="panel-body">
        <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
          Arama Filtresini Göster / Gizle
        </a>

        <div class="btn-group pull-right" role="group" aria-label="Button group with nested dropdown">
          <div class="btn-group" role="group">
            <button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Görünüm Sayısı
            </button>
            <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
              <a class="dropdown-item" href="<?= $limit; ?>&limit=100">100 Satır Listele</a><br>
              <a class="dropdown-item" href="<?= $limit; ?>&limit=250">250 Satır Listele</a><br>
              <a class="dropdown-item" href="<?= $limit; ?>&limit=500">500 Satır Listele</a><br>
              <a class="dropdown-item" href="<?= $limit; ?>&limit=1000">1000 Satır Listele</a>
            </div>
          </div>
        </div>

        <div class="well collapse" id="collapseExample">
          <div class="row">
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-order-id"><?php echo $entry_order_id; ?></label>
                <input type="text" name="filter_order_id" value="<?php echo $filter_order_id; ?>" placeholder="<?php echo $entry_order_id; ?>" id="input-order-id" class="form-control" />
              </div>

              <div class="form-group">
                <label class="control-label" for="input-email">Mail</label>
                <input type="text" name="filter_email" value="<?php echo $filter_email; ?>" placeholder="Mail" id="input-email" class="form-control" />
              </div>

              <div class="form-group">
                <label class="control-label" for="input-filter_telephone">Telefon</label>
                <input type="text" name="filter_telephone" value="<?php echo $filter_telephone; ?>" placeholder="Telefon" id="input-filter_telephone" class="form-control" />
              </div>

              <div class="form-group">
                <label class="control-label" for="input-customer"><?php echo $entry_customer; ?></label>
                <input type="text" name="filter_customer" value="<?php echo $filter_customer; ?>" placeholder="<?php echo $entry_customer; ?>" id="input-customer" class="form-control" />
              </div>
            </div>

            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-order-status"><?php echo $entry_order_status; ?></label>
                <select name="filter_order_status" id="input-order-status" class="form-control">
                  <option value="*"></option>
                  <?php if ($filter_order_status == '0') { ?>
                  <option value="0" selected="selected"><?php echo $text_missing; ?></option>
                  <?php } else { ?>
                  <option value="0"><?php echo $text_missing; ?></option>
                  <?php } ?>
                  <?php foreach ($order_statuses as $order_status) { ?>
                  <?php if ($order_status['order_status_id'] == $filter_order_status) { ?>
                  <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
                  <?php } else { ?>
                  <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                  <?php } ?>
                  <?php } ?>
                </select>
              </div>

              <div class="form-group">
                <label class="control-label" for="input-order-zone">Şehir</label>
                <select name="filter_order_zone" id="input-order-zone" class="form-control">
                  <option value="*"></option>
                  <?php foreach ($zones as $zone) { ?>
                  <?php if ($zone['zone_id'] == $filter_order_zone) { ?>
                  <option value="<?php echo $zone['zone_id']; ?>" selected="selected"><?php echo $zone['name']; ?></option>
                  <?php } else { ?>
                  <option value="<?php echo $zone['zone_id']; ?>"><?php echo $zone['name']; ?></option>
                  <?php } ?>
                  <?php } ?>
                </select>
              </div>

              <div class="form-group">
                <label class="control-label" for="input-order-payment">Ödeme Yöntemi</label>
                <select name="filter_order_payment" id="input-order-payment" class="form-control">
                  <option value="*"></option>
                  <?php foreach ($paymnet_methods as $paymnet_method) { ?>
                  <?php if ($paymnet_method['payment_code'] == $filter_order_payment) { ?>
                  <option value="<?php echo $paymnet_method['payment_code']; ?>" selected="selected"><?php echo $paymnet_method['payment_method']; ?></option>
                  <?php } else { ?>
                  <option value="<?php echo $paymnet_method['payment_code']; ?>"><?php echo $paymnet_method['payment_method']; ?></option>
                  <?php } ?>
                  <?php } ?>
                </select>
              </div>

              <div class="form-group">
                <label class="control-label" for="input-total"><?php echo $entry_total; ?></label>
                <input type="text" name="filter_total" value="<?php echo $filter_total; ?>" placeholder="<?php echo $entry_total; ?>" id="input-total" class="form-control" />
              </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-date-added"><?php echo $entry_date_added; ?></label>
                <div class="input-group date">
                  <input type="text" name="filter_date_added" value="<?php echo $filter_date_added; ?>" placeholder="<?php echo $entry_date_added; ?>" data-date-format="YYYY-MM-DD" id="input-date-added" class="form-control" />
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                  </span></div>
              </div>
              <div class="form-group">
                <label class="control-label" for="input-date-modified"><?php echo $entry_date_modified; ?></label>
                <div class="input-group date">
                  <input type="text" name="filter_date_modified" value="<?php echo $filter_date_modified; ?>" placeholder="<?php echo $entry_date_modified; ?>" data-date-format="YYYY-MM-DD" id="input-date-modified" class="form-control" />
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                  </span></div>
              </div>
              <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-filter"></i> <?php echo $button_filter; ?></button>
            </div>
          </div>
        </div>
        <form method="post" action="" enctype="multipart/form-data" id="form-order">
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
              <tr>
                <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
                <td class="text-right"><?php if ($sort == 'o.order_id') { ?>
                  <a href="<?php echo $sort_order; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_order_id; ?></a>
                  <?php } else { ?>
                  <a href="<?php echo $sort_order; ?>"><?php echo $column_order_id; ?></a>
                  <?php } ?></td>
            
                <td class="text-left"><?php if ($sort == 'customer') { ?>
                  <a href="<?php echo $sort_customer; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_customer; ?></a>
                  <?php } else { ?>
                  <a href="<?php echo $sort_customer; ?>"><?php echo $column_customer; ?></a>
                  <?php } ?></td>
                <td class="text-left">
                  <a >Kaç Kere Sipariş Verdi ?</a></td>

                <td class="text-left"><?php if ($sort == 'order_status') { ?>
                  <a href="<?php echo $sort_status; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_status; ?></a>
                  <?php } else { ?>
                  <a href="<?php echo $sort_status; ?>"><?php echo $column_status; ?></a>
                  <?php } ?></td>
                <td class="text-right"><?php if ($sort == 'o.total') { ?>
                  <a href="<?php echo $sort_total; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_total; ?></a>
                  <?php } else { ?>
                  <a href="<?php echo $sort_total; ?>"><?php echo $column_total; ?></a>
                  <?php } ?></td>
                <td class="text-center">Ödeme Yöntemi</td>
                <td class="text-center">Teslimat Adresi</td>
                <td class="text-left"><?php if ($sort == 'o.date_added') { ?>
                  <a href="<?php echo $sort_date_added; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_date_added; ?></a>
                  <?php } else { ?>
                  <a href="<?php echo $sort_date_added; ?>"><?php echo $column_date_added; ?></a>
                  <?php } ?></td>
                <td class="text-left"><?php if ($sort == 'o.date_modified') { ?>
                  <a href="<?php echo $sort_date_modified; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_date_modified; ?></a>
                  <?php } else { ?>
                  <a href="<?php echo $sort_date_modified; ?>"><?php echo $column_date_modified; ?></a>
                  <?php } ?></td>
                <td class="text-right"><?php echo $column_action; ?></td>
              </tr>
              </thead>
              <tbody>
              <?php if ($orders) { ?>
              <?php foreach ($orders as $order) { ?>
              <tr>
                <td class="text-center"><?php if (in_array($order['order_id'], $selected)) { ?>
                  <input type="checkbox" name="selected[]" value="<?php echo $order['order_id']; ?>" checked="checked" />
                  <?php } else { ?>
                  <input type="checkbox" name="selected[]" value="<?php echo $order['order_id']; ?>" />
                  <?php } ?>
                  <input type="hidden" name="shipping_code[]" value="<?php echo $order['shipping_code']; ?>" /></td>
                <td class="text-right"><?php echo $order['order_id']; ?></td>
                <td class="text-left"><?php echo $order['customer']; ?></td>
                <td class="text-left"> <?php if (isset($order['total_customer_orders'])) { ?>

                  <?php if ($order['total_customer_orders'] == '0') { ?>
                  Müşteri Üye Değil
                  <?php } else { ?>
                  <?php echo $order['total_customer_orders']; ?>
                  <?php } ?>

                  <?php } ?></td>
                <td class="text-left"><?php echo $order['order_status']; ?></td>
                <td class="text-right"><?php echo $order['total']; ?></td>
                <td class="text-center"><?php echo $order['payment_method']; ?></td><!--payment-method-melih zorlu-->
                <td class="text-center"><?php echo $order['shipping_address']; ?></td><!--payment-method-melih zorlu-->
                <td class="text-left"><?php echo $order['date_added']; ?></td>
                <td class="text-left"><?php echo $order['date_modified']; ?></td>
                <td class="text-right">
                  <a href="<?php echo $order['view']; ?>" data-toggle="tooltip" title="<?php echo $button_view; ?>" class="btn btn-info"><i class="fa fa-info"></i></a>
                  <a href="<?php echo $order['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                </td>

              </tr>
              <?php } ?>
              <?php } else { ?>
              <tr>
                <td class="text-center" colspan="8"><?php echo $text_no_results; ?></td>
              </tr>
              <?php } ?>
              </tbody>
            </table>
          </div>
        </form>
       <!-- <div class="well">
          <div class="row">
          <div class="col-sm-2">
            <label class="label-control">Order Status</label><br>
            <select id="scf_order_status_id" class="form-control" name="order_status_id" style="width: 208px; float: left; margin-right: 10px">
              <?php foreach ($order_statuses as $order_status) { ?>
              <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
              <?php } ?>
            </select>
          </div>
          <div class="col-sm-1" style="margin-left:30px;">
            <label class="label-control">Mail Gitsin</label><br>
            <input type="checkbox" name="override" value="1" id="scf_notify" data-toggle="tooltip" title="<?php echo $add_notify; ?>" style="margin-top:5px;"/>&nbsp;&nbsp; 
          </div>
          <div class="col-sm-2">  
            <label class="label-control">Açıklama(İsteğe Bağlı)</label>  
            <div class="extra-comment comment-box">
              <label class="add-comment btn btn-info" data-toggle="tooltip" title="<?php echo $add_comment; ?>"><i class="fa fa-comment add-comment" aria-hidden="true"> </i> Comment</label>
              <div class="comment-description">
                <div class="comment-icon"></div>
                  <textarea name="comment" rows="8" id="scf_comment" class="form-control"></textarea>
                </div>    &nbsp;&nbsp;&nbsp;

              </div>
            </div>
            <div class="col-sm-3"><br><button id="btn-update-status" data-toggle="tooltip" title="<?php echo $button_update_status; ?>" class="btn btn-info btn-update-status" style="margin-top:5px;"><i class="fa fa-refresh"></i> Siparişleri Güncelle</button></div>
          </div>
          <div class="row">
            <div class="notification col-sm-7"></div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
      </div>-->
    </div>
  </div>
  <script type="text/javascript"><!--
    $('#button-filter').on('click', function() {
      url = 'index.php?route=sale/order&token=<?php echo $token; ?>';

      var filter_order_id = $('input[name=\'filter_order_id\']').val();

      if (filter_order_id) {
        url += '&filter_order_id=' + encodeURIComponent(filter_order_id);
      }

      var filter_email = $('input[name=\'filter_email\']').val();

      if (filter_email) {
        url += '&filter_email=' + encodeURIComponent(filter_email);
      }

      var filter_telephone = $('input[name=\'filter_telephone\']').val();

      if (filter_telephone) {
        url += '&filter_telephone=' + encodeURIComponent(filter_telephone);
      }

      var filter_customer = $('input[name=\'filter_customer\']').val();

      if (filter_customer) {
        url += '&filter_customer=' + encodeURIComponent(filter_customer);
      }

      var filter_order_status = $('select[name=\'filter_order_status\']').val();

      if (filter_order_status != '*') {
        url += '&filter_order_status=' + encodeURIComponent(filter_order_status);
      }

      var filter_order_zone = $('select[name=\'filter_order_zone\']').val();

      if (filter_order_zone != '*') {
        url += '&filter_order_zone=' + encodeURIComponent(filter_order_zone);
      }

      var filter_order_payment = $('select[name=\'filter_order_payment\']').val();

      if (filter_order_payment != '*') {
        url += '&filter_order_payment=' + encodeURIComponent(filter_order_payment);
      }

      var filter_total = $('input[name=\'filter_total\']').val();

      if (filter_total) {
        url += '&filter_total=' + encodeURIComponent(filter_total);
      }

      var filter_date_added = $('input[name=\'filter_date_added\']').val();

      if (filter_date_added) {
        url += '&filter_date_added=' + encodeURIComponent(filter_date_added);
      }

      var filter_date_modified = $('input[name=\'filter_date_modified\']').val();

      if (filter_date_modified) {
        url += '&filter_date_modified=' + encodeURIComponent(filter_date_modified);
      }

      location = url;
    });
    //--></script>
  <script type="text/javascript"><!--
    $('input[name=\'filter_customer\']').autocomplete({
      'source': function(request, response) {
        $.ajax({
          url: 'index.php?route=customer/customer/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
          dataType: 'json',
          success: function(json) {
            response($.map(json, function(item) {
              return {
                label: item['name'],
                value: item['customer_id']
              }
            }));
          }
        });
      },
      'select': function(item) {
        $('input[name=\'filter_customer\']').val(item['label']);
      }
    });
    //--></script>
  <script type="text/javascript"><!--
    $('input[name^=\'selected\']').on('change', function() {
      $('#button-shipping, #button-invoice').prop('disabled', true);

      var selected = $('input[name^=\'selected\']:checked');

      if (selected.length) {
        $('#button-invoice').prop('disabled', false);
      }

      for (i = 0; i < selected.length; i++) {
        if ($(selected[i]).parent().find('input[name^=\'shipping_code\']').val()) {
          $('#button-shipping').prop('disabled', false);

          break;
        }
      }
    });

    $('#button-shipping, #button-invoice').prop('disabled', true);

    $('input[name^=\'selected\']:first').trigger('change');

    // IE and Edge fix!
    $('#button-shipping, #button-invoice').on('click', function(e) {
      $('#form-order').attr('action', this.getAttribute('formAction'));
    });

    $('#button-delete').on('click', function(e) {
      $('#form-order').attr('action', this.getAttribute('formAction'));

      if (confirm('<?php echo $text_confirm; ?>')) {
        $('#form-order').submit();
      } else {
        return false;
      }
    });
    //--></script>
  <script src="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
  <link href="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen" />
  <script type="text/javascript"><!--
    $('.date').datetimepicker({
      pickTime: false
    });
    //--></script></div>

<!--<script>
    var exportExcel = function () {
      var excelLink = "<?= $excel_export; ?>";
      if (excelLink.indexOf ("filter_date_added") != -1) window.location.href = excelLink.replace (/amp;/g, '');
      else alert ("Lütfen bir gün seçin!");
    };
</script>-->
<script>
  var downloadExcel = function () {
    var excel_download_link = "<?= $excel_download; ?>";
    if (excel_download_link.indexOf ("filter_date_added") != -1) window.location.href = excel_download_link.replace (/amp;/g, '');
    else alert ("Lütfen bir gün seçin!");
  };
</script>

      <style type="text/css">
        .comment-icon:after, .comment-icon:before {
        bottom: 100%; left: 50%; border: solid transparent; content: " "; height: 0; width: 0; position: absolute; pointer-events: none; }
        .comment-icon:after { border-color: rgba(213, 130, 100, 0); border-bottom-color: #ffffff; border-width: 12px; margin-left: -12px;}
        .comment-icon:before { border-color: rgba(0, 0, 0, 0); border-bottom-color: #dedede;
          border-width: 12px; margin-left: -12px; }         
        .comment-description{ background: #ffffff; display: none; left: -131px;position: absolute; top: 45px;
          width: 300px; z-index: 99999999; padding:15px; border:1px solid #dedede;}
        .comment-box{ position:relative; height: 45px;}
        .table-responsive{ overflow-x: unset;}
        .extra-comment:hover .comment-description{ display:block; }
      </style>      
      
  

<?php echo $footer; ?>