<?php echo $header; ?>
<?php echo $column_left; ?>

<div id="content">
    <form action="" method="post">


        <?php if ( !$terms) { ?>
            <div class="container-fluid panel">
                <?php if ($error_message) { ?>
                    <div class="alert alert-danger">
                        Kayıt sırasında hata oluştu.
                        <?php echo $error_message; ?>
                    </div>
                <?php } ?>
                <hr/>
                <h1	align="center">Kurulum veya Güncelleme !</h1>
                <hr/>

                <div class="col-md-12">
                    <h2> Başlamak İçin Aşağıdaki Formu Doldurunuz.</h2>
                    <small>Bu formdaki bilgiler üçüncü kişi ve/veya kurumlarla paylaşılmayacak olup ileride ücretsiz
                        güncelleme ve güvenlik kontrolü v.b. işlemlerinde kullanılacaktır.
                    </small>
                </div>
                <hr/>
                <div class="col-md-12">
                    <div class="col-md-6">
                        <div class="input-group">
                            <label>Adınız</label><br/>
                            <input type="text" name="spr_shop_firstname" class="form-control" required/>
                        </div>
                        <div class="input-group">
                            <label>Soyadınız</label><br/>
                            <input type="text" name="spr_shop_lastname" class="form-control" required />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="input-group">
                            <label class="label label-danger"> Cep Telefonu Numaranız (SMS alacak) </label><br/>
                            <input class="form-control" name="spr_shop_phone" type="tel" pattern="^[0-9\-\+\s\(\)]*$" maxlength="11" minlength="11" placeholder="05XX XXX XXXX" required />
                        </div>
                        <div class="input-group">
                            <label>Bir Şifre Belirleyin</label><br/>
                            <input class="form-control" type="text" name="spr_shop_password" required/>
                        </div>
                    </div>
                </div>
                <hr/>
                <div class="alert alert-info"><b>Bu bilgiler hiçbir kişi veya kurum ile paylaşılmayacak olup</b> istenildiği
                    taktirde kullanıcı tarafından silinebilecektir.
                    Telefon numaranız ve e-postanız dolandırıcılık koruma işlemleri, değişiklikler ve güncellemeler
                    ile ilgili bilgilendirme amaçlı kullanılılacak olup isteğiniz dışında
                    <strong>hiç bir şekilde reklam/tanıtım amaçlı kullanılmayacaktır.</strong></div>
                <div class="checkbox-inline">
                    <input type="checkbox" checked="true" required name="tos_sanalpospro"/>
                    SanalPos Pro! <a href="https://sanalpospro.com/kullanim-sartlari" target="_blank">Kullanım Koşullarını</a> Okudum Kabul Ediyorum
                </div>
                <br/>
                <div class="checkbox-inline">
                    <input type="checkbox" checked="true" class="" name="tos_sanalpospro"/>
                    FP007 Dolandırıcılık Koruma Servisi (Ücretsiz) <a href="https://sanalpospro.com/fp07-tos" target="_blank">Kullanım Koşullarını </a>Okudum Kabul Ediyorum
                </div>
                <div class="row text-center align-center">
                    <br/>
                    <input type="hidden" name="spr_terms" value="1"/>
                    <input type="hidden" name="spr_shop_key" value=""/>
                    <button type="submit" class="center btn btn-large btn-info">Kurulumu Tamamla</button>
                </div>
                <div class="col-md-12">
                    <h3>SanalPOS Pro! Nedir ?</h3>
                    <ul>
                        <li>SanalPOS Pro! Eticaret sitenizden kredi kartı ile ödeme almanızı sağlayan,
                            <strong>Türkiye'nin en gelişmiş ve en çok kullanılan </strong>SanalPOS yazılımıdır. </li>
                        <li>Doğrudan kendi hizmetinize ait hesaba tahsilat yapar. Herhangi bir aracılık veya bankacılık servisi değildir.</li>
                        <li>2005 yılında EticSoft tarafından geliştirilmeye başlanmıştır. Tamamen <strong>açık kaynaklı</strong>dır. 2018 itibariyle 3000+ mağazada kullanılmaktadır.</li>
                        <li>Tüm bankalar ve onlarca ödeme kuruluşu ile entegredir.</li>
                        <li>Teknik destek ve güncellemeler <strong>ömür boyu ücretsizdir</strong>. Teknik destek </li>
                        <li>4691 sayılı <strong>Teknoloji Geliştirme Bölgeleri Kanunu</strong>'na istinaden,
                            T.C. Bilim Sanayi ve Teknoloji Bakanlığı'nın 026763 proje kodu ile Akdeniz Ünv.
                            Teknokenti'nde geliştirilmektedir. </li>
                        <li>EticSoft'un başka bir ArGe projesi olan FP007 <strong>dolandırıclık koruma yazılımı</strong> ile entegredir.</li>
                        <li>Uluslararası <strong>PCI-DSS</strong> güvenlik standartlarına uyumludur.
                            Kart bilgilerini doğrudan pos sistemine iletir.
                            Okumaz, saklamaz ve paylaşmaz.</li>
                        <li>6102 Türk Ticaret Kanunu ve 5411 sayılı Bankacılık Kanunu'na uyumludur. Türkiye'deki
                            tüm bankaların altayapısına uygundur.</li>
                    </ul>
                </div>
            </div>
        <?php } else { ?>

            <div class="container-fluid ">
                <div class="row">

                    <?php if ($messages) { ?>

                        <?php foreach($messages as $_key => $mesg) { ?>
                            <div class="col-sm-6 col-xs-12">
                                <div class="alert alert-<?php echo $mesg['type']; ?>"><?php echo $mesg['message']; ?></div>
                            </div>
                        <?php } ?>
                    <?php } ?>

                </div>
                <?php if ($viewlog) { ?>
                    <div class="row">
                        <div class="col-sm-12 col-xs-12">
                            İşlem İçin Kayıt Defteri
                            <pre><textarea style="height: 300px"><?php echo $viewlog; ?></textarea></pre>
														</div>
													</div>
                <?php } ?>
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                    <li class="active"><a href="#sanalposprosettings" role="tab" data-toggle="tab"><i class="icon icon-cogs"></i> Genel Ayarlar</a></li>
                    <li><a href="#pos" role="tab" data-toggle="tab"><i class="icon icon-cart-arrow-down"></i>POS Ayarları</a></li>
                    <li><a href="#cards" role="tab" data-toggle="tab"><i class="icon icon-credit-card"></i> Taksitler </a></li>
                    <li><a href="#integration" role="tab" data-toggle="tab"><i class="icon icon-link"></i> Yöntem Ekle </a></li>

            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                    <div class="tab-pane active" id="sanalposprosettings"><?php echo $general_tab; ?></div>
                    <div class="tab-pane" id="pos"><?php echo $banks_tab; ?></div>
                    <div class="tab-pane" id="cards"><?php echo $cards_tab; ?></div>
                    <div class="tab-pane" id="integration"><?php echo $integration_tab; ?></div>
                    <div class="tab-pane" id="help"> <?php echo $help_tab; ?> </div>
                    <div class="tab-pane" id="stats"><?php echo $help_tab; ?></div>
                    <div class="tab-pane" id="tools"><?php echo $tools_tab; ?></div>
                    <div class="tab-pane" id="campaign"><?php echo $campaign_tab; ?></div>
                    <div class="tab-pane" id="masterpass"><?php echo $masterpass_tab; ?></div>
            </div>

            <div class="panel">
                    <div class="panel-body">
                            <div class="row eticsoft_garantipay-header">
                                    <div class="col-xs-6 col-md-4 text-center">
                                            <iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FEticSoft%2F&width=450&layout=standard&action=like&size=small&show_faces=true&share=true&height=80&appId=166162726739815" width="450" height="80" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>		</div>
                                    <div class="col-xs-6 col-md-8 text-center">
                                            <a href="https://www.youtube.com/user/EticSoft"><img src="<?php echo $spr_url; ?>image/sanalpospro/icons/youtube.png" target="_blank" /></a>
                                            <a href="https://www.linkedin.com/company/eticsoft-yaz%C4%B1l%C4%B1m"><img src="<?php echo $spr_url; ?>image/sanalpospro/icons/linkedin.png" target="_blank" /></a>
                                            <a href="https://twitter.com/eticsoft"><img src="<?php echo $spr_url; ?>image/sanalpospro/icons/twitter.png" target="_blank" /></a>
                                            <a href="https://www.instagram.com/eticsoft/"><img src="<?php echo $spr_url; ?>image/sanalpospro/icons/instagram.png" target="_blank" /></a>
                                            <a href="https://wordpress.org/support/users/eticsoft-lab/"><img src="<?php echo $spr_url; ?>image/sanalpospro/icons/wordpress.png" target="_blank" /></a>
                                            <a href="https://github.com/eticsoft/"><img src="<?php echo $spr_url; ?>image/sanalpospro/icons/github.png" target="_blank" /></a>
                                    </div>
                            </div>
                    </div>
            </div>
        </div>
        <?php } ?>
    </form>
</div>
<?php echo $footer; ?>
