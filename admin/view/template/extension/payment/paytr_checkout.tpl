<?php echo $header; ?>
<?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" form="form-free-checkout" data-toggle="tooltip"
                        title="<?php echo $button_save; ?>"
                        class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>"
                   class="btn btn-default"><i class="fa fa-reply"></i></a></div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <div class="alert alert-info">
            <i class="fa fa-exclamation-circle"></i>&nbsp;<?php echo $help_paytr_checkout; ?>
        </div>

        <?php if (isset($errors)) { foreach ($errors as $key => $val) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $errors_message[$key]; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } } ?>

        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-free-checkout"
              class="form-horizontal">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_settings; ?> </h3>
                </div>
                <div class="panel-body">
                    <ul class="nav nav-tabs">
                        <li class="nav-tab active"><a href="#tab_api_information"
                                                      data-toggle="tab"><?php echo $text_general; ?></a></li>
                        <li class="nav-tab"><a href="#tab_order_status"
                                               data-toggle="tab"><?php echo $text_order_status; ?></a>
                        </li>
                        <li class="nav-tab"><a href="#tab_module_setting"
                                               data-toggle="tab"><?php echo $text_module_settings; ?></a></li>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_api_information">
                            <div class="form-group required">
                                <label class="col-sm-2 control-label"
                                       for="input_merchant_id"><?php echo $entry_merchant_id; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" id="input_merchant_id" name="payment_paytr_checkout_merchant_id"
                                           value="<?php echo $payment_paytr_checkout_merchant_id; ?>"
                                           class="form-control"
                                           placeholder="<?php echo $entry_merchant_id; ?>"/>
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-sm-2 control-label"
                                       for="input_merchant_key"><?php echo $entry_merchant_key; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" id="input_merchant_key"
                                           name="payment_paytr_checkout_merchant_key"
                                           value="<?php echo $payment_paytr_checkout_merchant_key; ?>"
                                           class="form-control"
                                           placeholder="<?php echo $entry_merchant_key; ?>"/>
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-sm-2 control-label"
                                       for="input_merchant_salt"><?php echo $entry_merchant_salt; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" id="input_merchant_salt"
                                           name="payment_paytr_checkout_merchant_salt"
                                           value="<?php echo $payment_paytr_checkout_merchant_salt; ?>"
                                           class="form-control"
                                           placeholder="<?php echo $entry_merchant_salt; ?>"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label"
                                       for="input_language"><?php echo $entry_language; ?></label>
                                <div class="col-sm-10">
                                    <select id="input_language" name="payment_paytr_checkout_lang"
                                            class="form-control">
                                        <?php foreach ($language_arr as $key => $k) { ?>
                                        <option value="<?php echo $key; ?>"
                                        <?php if($payment_paytr_checkout_lang == $key) { echo 'selected="selected"'; } ?>
                                        ><?php echo $k; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input_total" class="col-sm-2 control-label"><span data-toggle="tooltip"
                                                                                              title="<?php echo $help_total; ?>"><?php echo $entry_total; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" id="input_total"
                                           name="payment_paytr_checkout_total"
                                           value="<?php echo $payment_paytr_checkout_total; ?>" class="form-control"
                                           placeholder="<?php echo $entry_total; ?>"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input_module_layout"
                                       class="col-sm-2 control-label"><?php echo $entry_module_layout; ?></label>
                                <div class="col-sm-10">
                                    <select id="input_module_layout" name="payment_paytr_checkout_module_layout"
                                            class="form-control">
                                        <?php foreach ($module_layout as $key => $k) { ?>
                                        <option value="<?php echo $key; ?>"
                                        <?php if($payment_paytr_checkout_module_layout == $key) { echo 'selected="selected"'; } ?>
                                        ><?php echo $k; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label"
                                       for="input_module_status"><?php echo $entry_status; ?></label>
                                <div class="col-sm-10">
                                    <select id="input_module_status" name="paytr_checkout_status"
                                            class="form-control">
                                        <?php if($paytr_checkout_status == 0) { ?>
                                        <option value="1"><?php echo $text_enabled; ?></option>
                                        <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                        <?php } else { ?>
                                        <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                        <option value="0"><?php echo $text_disabled; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input_sort_order"
                                       class="col-sm-2 control-label"><?php echo $entry_sort_order; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" id="input_sort_order"
                                           name="payment_paytr_checkout_sort_order"
                                           value="<?php echo $payment_paytr_checkout_sort_order; ?>"
                                           class="form-control" placeholder="<?php echo $entry_sort_order; ?>"/>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_order_status">
                            <div class="form-group required">
                                <label class="col-sm-2 control-label"
                                       for="input_order_status_completed"><?php echo $entry_payment_complete; ?></label>
                                <div class="col-sm-10">
                                    <select name="payment_paytr_checkout_order_completed_id"
                                            id="input_order_status_completed"
                                            class="form-control">
                                        <?php if($payment_paytr_checkout_order_completed_id == '') { ?>
                                        <option value='' selected><?php echo $text_select; ?></option>
                                        <?php } ?>
                                        <?php foreach($order_statuses as $order_status) { ?>
                                        <?php if($order_status['order_status_id'] == $payment_paytr_checkout_order_completed_id) { ?>
                                        <option value="<?php echo $order_status['order_status_id']; ?>"
                                                selected="selected"><?php echo $order_status['name']; ?>
                                        </option>
                                        <?php } else { ?>
                                        <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                                        <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-sm-2 control-label"
                                       for="input_order_status_canceled"><?php echo $entry_payment_failed; ?></label>
                                <div class="col-sm-10">
                                    <select name="payment_paytr_checkout_order_canceled_id"
                                            id="input_order_status_canceled"
                                            class="form-control">
                                        <?php if($payment_paytr_checkout_order_canceled_id == '') { ?>
                                        <option value='' selected><?php echo $text_select; ?></option>
                                        <?php } ?>
                                        <?php foreach($order_statuses as $order_status) { ?>
                                        <?php if($order_status['order_status_id'] == $payment_paytr_checkout_order_canceled_id) { ?>
                                        <option value="<?php echo $order_status['order_status_id']; ?>"
                                                selected="selected"><?php echo $order_status['name']; ?>
                                        </option>
                                        <?php } else { ?>
                                        <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                                        <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label"
                                       for="input_order_notify"><?php echo $entry_notify_status; ?></label>
                                <div class="col-sm-10">
                                    <select name="payment_paytr_checkout_notify" class="form-control"
                                            id="input_order_notify">
                                        <?php if($payment_paytr_checkout_notify == 0) { ?>
                                        <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                        <option value="1"><?php echo $text_enabled; ?></option>
                                        <?php } else { ?>
                                        <option value="0"><?php echo $text_disabled; ?></option>
                                        <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                        <?php } ?>
                                    </select>
                                    <span class="help"><?php echo $help_notify; ?></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label"
                                       for="input_order_ins_total"><?php echo $entry_ins_total; ?></label>
                                <div class="col-sm-10">
                                    <select name="payment_paytr_checkout_ins_total" class="form-control"
                                            id="input_order_ins_total">
                                        <?php if($payment_paytr_checkout_ins_total == 0) { ?>
                                        <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                        <option value="1"><?php echo $text_enabled; ?></option>
                                        <option value="2"><?php echo $text_ins_total; ?></option>
                                        <?php } elseif ($payment_paytr_checkout_ins_total == 1) { ?>
                                        <option value="0"><?php echo $text_disabled; ?></option>
                                        <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                        <option value="2"><?php echo $text_ins_total; ?></option>
                                        <?php } else { ?>
                                        <option value="0"><?php echo $text_disabled; ?></option>
                                        <option value="1"><?php echo $text_enabled; ?></option>
                                        <option value="2" selected="selected"><?php echo $text_ins_total; ?></option>
                                        <?php } ?>
                                    </select>
                                    <span class="help"><?php echo $help_ins_total; ?></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label"
                                       for="input_order_total"><?php echo $entry_order_total; ?></label>
                                <div class="col-sm-10">
                                    <select name="payment_paytr_checkout_order_total" class="form-control"
                                            id="input_order_total">
                                        <?php if($payment_paytr_checkout_order_total == 0) { ?>
                                        <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                        <option value="1"><?php echo $text_enabled; ?></option>
                                        <?php } else { ?>
                                        <option value="0"><?php echo $text_disabled; ?></option>
                                        <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                        <?php } ?>
                                    </select>
                                    <span class="help"><?php echo $help_order_total; ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_module_setting">
                            <div class="form-group">
                                <label class="col-sm-2 control-label"
                                       for="input_installment_max"><?php echo $entry_max_installments; ?></label>
                                <div class="col-sm-10">
                                    <select id="input_installment_max" name="payment_paytr_checkout_installment_number"
                                            class="form-control">
                                        <?php foreach($installment_arr as $key => $k) { ?>
                                        <?php if($key==$payment_paytr_checkout_installment_number) { ?>
                                        <option value="<?php echo $key; ?>"
                                                selected="selected"><?php echo $k; ?></option>
                                        <?php } else { ?>
                                        <option value="<?php echo $key; ?>"><?php echo $k; ?></option>
                                        <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div id="categoryBasedArea" class="row" style="border-top: 5px solid #efefef;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<style>
    #categoryBasedArea .col-md-3:nth-child(2n) {
        background: #efefef;
    }
</style>

<script type="text/javascript">

    $(document).on('change', '#input_installment_max', function () {
        ajaxCategoryBased(this.value);
    });

    $(window).on('load', function () {
        ajaxCategoryBased($('#input_installment_max').val());
    });

    function ajaxCategoryBased(val) {

        const body = $('#categoryBasedArea');

        if (val == 13) {
            $.ajax({
                url: 'index.php?route=extension/payment/paytr_checkout/ajaxCategoryBased&token=<?php echo $token; ?>',
                dataType: 'json',
                beforeSend: function () {
                    body.html('<div style="text-align: center;margin-top: 20px;"><img src="<?php echo $paytr_icon_loader; ?>" /></div>');
                },
                success: function (json) {
                    let formGroup = '';

                    if (typeof json != "undefined" && json != null) {

                        if (json.result !== false) {
                            let result = json.result;

                            $.each(json.categories, function (ci, cv) {
                                formGroup += '<div class="col-md-3"><div class="">';
                                formGroup += '<label class="control-label">' + cv + '</label>';
                                formGroup += '<div class=""><select name="payment_paytr_checkout_category_installment[' + ci + ']" class="form-control">';

                                $.each(json.installments, function (ii, iv) {
                                    formGroup += '<option value="' + ii + '"';

                                    if (result != null) {
                                        if (result[ci] == ii) {
                                            formGroup += 'selected="selected"';
                                        }
                                    }

                                    formGroup += '>' + iv + '</option>';
                                });

                                formGroup += '</select></div>';
                                formGroup += '</div></div>';
                            });
                        } else {
                            alert('An error occurred');
                        }
                    }

                    body.html(formGroup);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        } else {
            body.html('');
        }
    }

</script>
<?php echo $footer; ?>