<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid ">
    <?php /*
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-puzzle-piece"></i> <?php echo $odeme_yontemleri; ?></h3>
      </div>
      <div class="panel-body">


          <?php foreach($odeme_modulleri as $odememodulleri){ ?>
            <a href="<?php echo $odememodulleri['href']; ?>">
              <div class="col-md-3 col-xs-12 col-lg-3 col-sm-3 modulesdata">
                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 image">
                  <img src="<?php echo $odememodulleri['image']; ?>">
                </div>
                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 title">
                  <?php echo $odememodulleri['name']; ?>
                </div>
              </div>
            </a>
          <?php } ?>


      </div>
    </div>
    */ ?>

    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-puzzle-piece"></i> <?php echo $kargo_islemleri; ?></h3>
      </div>
      <div class="panel-body">

        <?php foreach($kargo_modulleri as $odememodulleri){ ?>
          <a href="<?php echo $odememodulleri['href']; ?>">
            <div class="col-md-3 col-xs-12 col-lg-3 col-sm-3 modulesdata">
              <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 image">
                <img src="<?php echo $odememodulleri['image']; ?>">
              </div>
              <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 title">
                <?php echo $odememodulleri['name']; ?>
              </div>
            </div>
          </a>
        <?php } ?>
      </div>
    </div>


    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-puzzle-piece"></i> <?php echo $kampanya_yonetimi; ?></h3>
      </div>
      <div class="panel-body">
        <?php foreach($kampanya_modulleri as $odememodulleri){ ?>
           <a href="<?php echo $odememodulleri['href']; ?>">
            <div class="col-md-3 col-xs-12 col-lg-3 col-sm-3 modulesdata">
              <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 image">
                <img src="<?php echo $odememodulleri['image']; ?>">
              </div>
              <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 title">
                <?php echo $odememodulleri['name']; ?>
              </div>
            </div>
            <a/>
        <?php } ?>
      </div>
    </div>


    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-puzzle-piece"></i> <?php echo $analitik_araclari; ?></h3>
      </div>
      <div class="panel-body">
        <?php foreach($google_modulleri as $odememodulleri){ ?>
          <a href="<?php echo $odememodulleri['href']; ?>">
            <div class="col-md-3 col-xs-12 col-lg-3 col-sm-3 modulesdata">
              <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 image">
                <img src="<?php echo $odememodulleri['image']; ?>">
              </div>
              <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 title">
                <?php echo $odememodulleri['name']; ?>
              </div>
            </div>
          </a>
        <?php } ?>
      </div>
    </div>


    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-puzzle-piece"></i> <?php echo $toplu_urun; ?></h3>
      </div>
      <div class="panel-body">
        <p class='alert alert-danger'>N11 modülü alt yapı güncellemeleri sebebi ile geçici süreliğine pasif durumdadır. İlgili işlemlerin akabinde tekrar açılacaktır.</p>
        <?php foreach($toplu_urun_modulleri as $odememodulleri){ ?>
          <a href="<?php echo $odememodulleri['href']; ?>">
            <div class="col-md-3 col-xs-12 col-lg-3 col-sm-3 modulesdata">
              <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 image">
                <img src="<?php echo $odememodulleri['image']; ?>">
              </div>
              <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 title">
                <?php echo $odememodulleri['name']; ?>
              </div>
            </div>
          </a>
        <?php } ?>

      </div>
    </div>


    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-puzzle-piece"></i> <?php echo $diger_moduller; ?></h3>
      </div>
      <div class="panel-body">
      <?php foreach($diger as $digermoduller){ ?>

           <a href="<?php echo $digermoduller['href']; ?>">
            <div class="col-md-3 col-xs-12 col-lg-3 col-sm-3 modulesdata">
              <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 image">
                <img src="<?php echo $digermoduller['image']; ?>">
              </div>
              <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 title">
                <?php echo $digermoduller['name']; ?>
              </div>
            </div>
            <a/>
        <?php } ?>
      </div>
    </div>

  <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title"><i class="fa fa-puzzle-piece"></i> <?php echo $muhasebe_moduller; ?></h3>
    </div>
    <div class="panel-body">
      <?php foreach($muhasebe as $muhasebemoduller){ ?>

      <a href="<?php echo $muhasebemoduller['href']; ?>">
        <div class="col-md-3 col-xs-12 col-lg-3 col-sm-3 modulesdata">
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 image">
            <img src="<?php echo $muhasebemoduller['image']; ?>">
          </div>
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 title">
            <?php echo $muhasebemoduller['name']; ?>
          </div>
        </div>
        <a/>
        <?php } ?>
    </div>
  </div>

  <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title"><i class="fa fa-puzzle-piece"></i> <?php echo $fatura_modulleri; ?></h3>
    </div>
    <div class="panel-body">
      <?php foreach($fatura as $faturamoduller){ ?>

      <a href="<?php echo $faturamoduller['href']; ?>">
        <div class="col-md-3 col-xs-12 col-lg-3 col-sm-3 modulesdata">
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 image">
            <img src="<?php echo $faturamoduller['image']; ?>">
          </div>
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 title">
            <?php echo $faturamoduller['name']; ?>
          </div>
        </div>
        <a/>
        <?php } ?>
    </div>
  </div>



</div>

</div>
<?php echo $footer; ?>
