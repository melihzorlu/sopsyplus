  <?php
  $popup_settings = isset($moduleData['popup_settings']) ? $moduleData['popup_settings'] : array() ;
  ?>
  <div class="form-group">
    <label class="col-sm-2 control-label" for="input-image"><span data-toggle="tooltip" title="If you enable this option, a popup will open with the contect, when a product from this promotion is added to the cart.">Sepete ekleyince popup göster</label>
    <div class="col-sm-6">
   		<select name="popup_settings[status]" id="input-popup" class="form-control">
          <option <?php echo (isset($popup_settings['status']) && $popup_settings['status'] == 'yes') ? 'selected="selected"' : ''; ?> value="yes">Aktif</option>
          <option <?php echo (isset($popup_settings['status']) && $popup_settings['status'] == 'no' || isset($is_new)) ? 'selected="selected"' : ''; ?> value="no">Pasif</option>
      	</select>
    </div>
  </div> 

  <div style="display:none;" data-triggered-from="#input-popup" class="display_when_enabled">

  <div class="form-group">
    <label class="col-sm-2 control-label">Açılır Pencere İçeriği:</label>
    <div class="col-sm-6">
      <ul class="nav nav-tabs popup_tabs">
        <?php foreach ($languages as $key => $value) { ?>
          <li class="active"><a href="#pop_up_description_<?php echo $key; ?>" data-toggle="tab"><img src="<?php echo $value['flag_url']; ?>"> <?php echo $value['name']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>

  <?php foreach ($languages as $key => $value) { ?>
    <div class="form-group">
    <div class="col-sm-offset-2 col-sm-6">
      <textarea name="popup_settings[content][<?php echo $key; ?>]" placeholder="" id="pop_up_description_<?php echo $key; ?>">
      <?php echo isset($popup_settings['content'][$key]) ? $popup_settings['content'][$key] : ''; ?>
      </textarea>
    </div>
  </div>
  <?php } ?>

  </div>

   <script type="text/javascript"><!--
<?php foreach ($languages as $key => $value) { ?>
$('#pop_up_description_<?php echo $key; ?>').summernote({height: 300});
<?php } ?>
//--></script> 