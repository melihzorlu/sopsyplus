  <?php
  $actions = isset($moduleData['actions'][1]) ? $moduleData['actions'][1] : array() ;
  ?>

        <div class="row">
           <div class="col-sm-12">
          <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="Koşullar karşılandığında uygulanacak eylemi seçin">Seçiniz</span></label>
          <div class="col-sm-4">
              <select class="form-control" onchange="get_cond_getField(1);" id="getMainSelect1" name="actions[1][mainSelect]">
                    <option disabled="disabled" <?php echo (!isset($actions['mainSelect']) || $actions['mainSelect'] == '') ? 'selected="selected"' : ''; ?> >-- Eylem Seçin --</option>
                    <option <?php echo (isset($actions['mainSelect']) && $actions['mainSelect'] == 'order_discount') ? 'selected="selected"' : ''; ?> value="order_discount">Sepet toplamından indirim</option>
                    <option <?php echo (isset($actions['mainSelect']) && $actions['mainSelect'] == 'order_discount_range') ? 'selected="selected"' : ''; ?> value="order_discount_range">Sepet toplamından aralıklarla indirim</option>
                    <option <?php echo (isset($actions['mainSelect']) && $actions['mainSelect'] == 'discount_all_products') ? 'selected="selected"' : ''; ?> value="discount_all_products">Tüm ürünlerde indirim</option>
                    <option <?php echo (isset($actions['mainSelect']) && $actions['mainSelect'] == 'discount_on_products') ? 'selected="selected"' : ''; ?> value="discount_on_products">Seçilen ürünlerde indirim</option>
                    <option <?php echo (isset($actions['mainSelect']) && $actions['mainSelect'] == 'discount_on_categories') ? 'selected="selected"' : ''; ?> value="discount_on_categories">Kategorideki tüm ürünlerde indirim</option>
                    <option <?php echo (isset($actions['mainSelect']) && $actions['mainSelect'] == 'discount_on_manufacturers') ? 'selected="selected"' : ''; ?> value="discount_on_manufacturers">Markalardaki tüm ürünlerde indirim</option>
                    <option <?php echo (isset($actions['mainSelect']) && $actions['mainSelect'] == 'discount_cheapest') ? 'selected="selected"' : ''; ?> value="discount_cheapest">En ucuz üründe indirim</option>
                    <option <?php echo (isset($actions['mainSelect']) && $actions['mainSelect'] == 'discount_expensive') ? 'selected="selected"' : ''; ?> value="discount_expensive">En pahalı üründe indirim</option>
                    <option <?php echo (isset($actions['mainSelect']) && $actions['mainSelect'] == 'discount_products_from_conditions') ? 'selected="selected"' : ''; ?> value="discount_products_from_conditions">Eğer varsa koşullar sekmesindeki  ürünleri indirime dahil etme</option>
                    <!--option value="give_coupon">Give coupon</option-->
                    <option <?php echo (isset($actions['mainSelect']) && $actions['mainSelect'] == 'free_shipping') ? 'selected="selected"' : ''; ?> value="free_shipping">Kargoyu ücretsiz yap</option>
                    <!--option <?php echo (isset($actions['mainSelect']) && $actions['mainSelect'] == 'add_product_cart') ? 'selected="selected"' : ''; ?> value="add_product_cart">Add product to cart and apply discount</option-->
              </select>
          </div>
          </div>
        </div>


        <div class="row">
            <div id="get_conditions_group" class="col-sm-12">
                  <div id="getcondition1" data-id="1">
                        <div id="get_cond_discount_amount1" class="row">
                          <div class="col-sm-12"><hr>
                              <label class="col-sm-2 control-label" for="input-disc-type"><span data-toggle="tooltip" title="İndirim türünü seçin. Sabit fiyat, mevcut fiyatı bu değerle değiştirecektir (Gerçekten daha düşük olmalıdır)">İndirim Tipi</span></label>
                              <div class="col-sm-4">
                                <select class="form-control" name="actions[1][disc_type]" id="discount_type">
                                  <option <?php echo (isset($actions['disc_type']) && $actions['disc_type'] == 'fixed') ? 'selected="selected"' : ''; ?> value="fixed">Sabit Değer</option>
                                  <option <?php echo (isset($actions['disc_type']) && $actions['disc_type'] == 'percentage') ? 'selected="selected"' : ''; ?> value="percentage">Yüzde</option>
                                  <option <?php echo (isset($actions['disc_type']) && $actions['disc_type'] == 'fixed_price') ? 'selected="selected"' : ''; ?> value="fixed_price">Sabit Fiyat (ürün fiyatından küçük olmalı)</option>
                                </select>
                              </div>
                            </div>
                             <div class="col-sm-12"><hr>
                              <label class="col-sm-2 control-label" for="input-disc-amount"><span data-toggle="tooltip" title="İndirim tutarını seçin">Tutar Girin</span></label>
                              <div class="col-sm-4">
                                <div class="input-group">
                                <div class="input-group-addon amount_type_addon"> <?php echo $currency; ?></div>
                                <input type="text" class="form-control" name="actions[1][amount]" id="input-amount" value="<?php echo isset($actions['amount']) ? $actions['amount'] : ''; ?>" />
                                </div>
                              </div>
                            </div>
                        </div>

                          <div style="display:none;" id="get_products1" class="row"><hr>
                            <div class="col-sm-12">
                              <label class="col-sm-2 control-label" for="input-get-product"><span data-toggle="tooltip" title="Bu ürünler siparişe eklendiğinde indirimli olacaktır.">Ürünler</span></label>
                              <div class="col-sm-4">
                                <input type="text" value="" placeholder="Enter product" id="input-get-product" class="form-control" />
                                <div id="get-product" class="well well-sm" style="height: 150px; overflow: auto;">
                                <?php if(isset($actions['product'])) { ?>
                                  <?php foreach ($actions['product'] as $product) { ?>
                                  <div id="get-product<?php echo $product['product_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $product['name']; ?>
                                    <input type="hidden" name="actions[1][product][]" value="<?php echo $product['product_id']; ?>" />
                                  </div>
                                  <?php } ?>
                                <?php } ?>
                                </div>
                              </div>
                            </div>
                            <div class="col-sm-12"><hr>
                                <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="İndirim yapılacak maksimum ürünleri girin. Bu alanı boş bırakırsanız herhangi bir sınırlama olmayacaktır.">Maksimum Miktar
</span></label>
                                <div class="col-sm-4">
                                  <input type="number" class="form-control" name="actions[1][max_quantity_specific_product]" min="1" value="<?php echo isset($actions['max_quantity_specific_product']) ? $actions['max_quantity_specific_product'] : '1'; ?>" />
                                </div>
                            </div>
                          </div>

                          <div style="display:none;" id="get_categories1" class="row"><hr>
                            <div class="col-sm-12">
                              <label class="col-sm-2 control-label" for="input-get-category"><span data-toggle="tooltip" title="Bu kategorilerdeki ürünler siparişe eklendiğinde indirimli olacaktır.">Kategoriler</span></label>
                              <div class="col-sm-4">
                                <input type="text" value="" placeholder="Enter category" id="input-get-category" class="form-control" />
                                <div id="get-category" class="well well-sm" style="height: 150px; overflow: auto;">
                                <?php if(isset($actions['category'])) { ?>
                                  <?php foreach ($actions['category'] as $category) { ?>
                                  <div id="buy-category<?php echo $category['category_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $category['name']; ?>
                                    <input type="hidden" name="actions[1][category][]" value="<?php echo $category['category_id']; ?>" />
                                  </div>
                                  <?php } ?>
                                <?php } ?>
                                </div>
                              </div>
                            </div>
                            <div class="col-sm-12"><hr>
                                <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="İndirim yapılacak maksimum ürünleri girin. Bu alanı boş bırakırsanız herhangi bir sınırlama olmayacaktır.">Maksimum Miktar
</span></label>
                                <div class="col-sm-4">
                                  <input type="number" class="form-control" name="actions[1][max_quantity_category]" min="1" value="<?php echo isset($actions['max_quantity_category']) ? $actions['max_quantity_category'] : '1'; ?>" />
                                </div>
                            </div>

                            <br>
                            <div class="col-sm-12"><hr>
                              <label class="col-sm-2 control-label" for="input-get-product"><span data-toggle="tooltip" title="Bu ürünler indirimden hariç tutulacaktır.">Hariç Ürünler</span></label>
                              <div class="col-sm-4">
                                <input type="text" value="" placeholder="Enter product" id="input-exclude-product-from-cat" class="form-control" />
                                <div id="exclude-product-from-cat" class="well well-sm" style="height: 150px; overflow: auto;">
                                <?php if(isset($actions['exclude_product_from_cat'])) { ?>
                                  <?php foreach ($actions['exclude_product_from_cat'] as $product) { ?>
                                  <div id="exclude-product-from-cat<?php echo $product['product_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $product['name']; ?>
                                    <input type="hidden" name="actions[1][exclude_product_from_cat][]" value="<?php echo $product['product_id']; ?>" />
                                  </div>
                                  <?php } ?>
                                <?php } ?>
                                </div>
                              </div>
                            </div>
                        </div>

                        <div style="display:none" id="get_manufacturers1" class="row"><hr>
                          <div class="col-sm-12">
                              <label class="col-sm-2 control-label" for="input-get-manufacturer"><span data-toggle="tooltip" title="Bu üreticilerin/markalar ürünleri siparişe eklendiklerinde indirimli olacaktır.">Markalar</span></label>
                              <div class="col-sm-4">
                                  <input type="text" value="" placeholder="Üreticiyi/markayı girin" id="input-get-manufacturer" class="form-control" />
                                  <div id="get-manufacturer" class="well well-sm" style="height: 150px; overflow: auto;">
                                      <?php if(isset($actions['manufacturer'])) { ?>
                                      <?php foreach ($actions['manufacturer'] as $manufacturer) { ?>
                                      <div id="buy-manufacturer<?php echo $manufacturer['manufacturer_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $manufacturer['name']; ?>
                                        <input type="hidden" name="actions[1][manufacturer][]" value="<?php echo $manufacturer['manufacturer_id']; ?>" />
                                      </div>
                                      <?php } ?>
                                    <?php } ?>
                                  </div>
                              </div>
                          </div>
                          <div class="col-sm-12"><hr>
                              <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="İndirim yapılacak maksimum ürünleri girin. Bu alanı boş bırakırsanız herhangi bir sınırlama olmayacaktır.">Maksimum Miktar</span></label>
                              <div class="col-sm-4">
                                  <input type="number" class="form-control" name="actions[1][max_quantity_manufacturer]" min="1" value="<?php echo isset($actions['max_quantity_manufacturer']) ? $actions['max_quantity_manufacturer'] : '1'; ?>"/>
                              </div>
                          </div>

                          <br>
                          <div class="col-sm-12"><hr>
                              <label class="col-sm-2 control-label" for="input-get-product"><span data-toggle="tooltip" title="Bu ürünler indirimden hariç tutulacaktır.">Hariç Ürünler</span></label>
                              <div class="col-sm-4">
                                  <input type="text" value="" placeholder="Enter product" id="input-exclude-product-from-manufacturer" class="form-control" />
                                  <div id="exclude-product-from-manufacturer" class="well well-sm" style="height: 150px; overflow: auto;">
                                      <?php if(isset($actions['exclude_product_from_manufacturer'])) { ?>
                                      <?php foreach ($actions['exclude_product_from_manufacturer'] as $product) { ?>
                                      <div id="exclude-product-from-cat<?php echo $product['product_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $product['name']; ?>
                                        <input type="hidden" name="actions[1][exclude_product_from_manufacturer][]" value="<?php echo $product['product_id']; ?>" />
                                      </div>
                                      <?php } ?>
                                    <?php } ?>
                                  </div>
                              </div>
                          </div>
                      </div>

                        <div style="display:none" id="exclude_condition_option" class="row"><hr>
                            <div class="col-sm-12">
                              <label class="col-sm-2 control-label" for="select-include-condition-product"><span data-toggle="tooltip" title="İndirim veya koşulları ürünleri atlamayı seçin. İndirim uygulamak için ürün alanına da eklemeniz gerekmektedir.">Koşullar seşmesindeki ürünler atla</span></label>
                              <div class="col-sm-4">
                                <select class="form-control" name="actions[1][skip_condition_products]" id="select-include-condition-product">
                                  <option <?php echo (isset($actions['skip_condition_products']) && $actions['skip_condition_products'] == 'yes') ? 'selected="selected"' : ''; ?> value="yes">Evet</option>
                                  <option <?php echo (isset($actions['skip_condition_products']) && $actions['skip_condition_products'] == 'no') ? 'selected="selected"' : ''; ?> value="no">Hayır</option>
                                </select>
                              </div>
                            </div>
                            <br>
                            <div class="col-sm-12"><hr>
                              <label class="col-sm-2 control-label" for="select-skip-special-products"><span data-toggle="tooltip" title="Özel ürünlerle indirim yapmayı veya ürünleri atlamayı seçin. İndirim uygulamak için ürün alanına da eklemeniz gerekmektedir.">İndirimli ürünler dahil etme</span></label>
                              <div class="col-sm-4">
                                <select class="form-control" name="actions[1][skip_special_products]" id="select-skip-special-products">
                                  <option <?php echo (isset($actions['skip_special_products']) && $actions['skip_special_products'] == 'yes') ? 'selected="selected"' : ''; ?> value="yes">Evet </option>
                                  <option <?php echo (isset($actions['skip_special_products']) && $actions['skip_special_products'] == 'no') ? 'selected="selected"' : ''; ?> value="no">Hayır</option>
                                </select>
                              </div>
                            </div>

                        </div>

                        <div style="display:none" id="exclude" class="row"><hr>
                            <div class="col-sm-12">
                                <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="İndirim yapılacak maksimum ürünleri girin. Bu alanı boş bırakırsanız herhangi bir sınırlama olmayacaktır.">Maksimum Miktar
</span></label>
                                <div class="col-sm-4">
                                  <input type="number" class="form-control" name="actions[1][max_quantity_all_products]" min="1" value="<?php echo isset($actions['max_quantity_all_products']) ? $actions['max_quantity_all_products'] : '1'; ?>" />
                                </div>
                            </div>

                            <div class="col-sm-12"><hr>
                              <label class="col-sm-2 control-label" for="input-get-product"><span data-toggle="tooltip" title="Bu ürünler indirimden hariç tutulacaktır.">Ürünler Hariç
</span></label>
                              <div class="col-sm-4">
                                <input type="text" value="" placeholder="Enter product" id="input-exclude-product" class="form-control" />
                                <div id="exclude-product" class="well well-sm" style="height: 150px; overflow: auto;">
                                <?php if(isset($actions['exclude_product'])) { ?>
                                  <?php foreach ($actions['exclude_product'] as $product) { ?>
                                  <div id="exclude-product<?php echo $product['product_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $product['name']; ?>
                                    <input type="hidden" name="actions[1][exclude_product][]" value="<?php echo $product['product_id']; ?>" />
                                  </div>
                                  <?php } ?>
                                <?php } ?>
                                </div>
                              </div>
                            </div>

                            <div class="col-sm-12">
                              <label class="col-sm-2 control-label" for="input-get-category"><span data-toggle="tooltip" title="Bu kategorilerdeki ürünler indirimden çıkarılacaktır.">Kategoriler Hariç</span></label>
                              <div class="col-sm-4">
                                <input type="text" value="" placeholder="Enter category" id="input-exclude-category" class="form-control" />
                                <div id="exclude-category" class="well well-sm" style="height: 150px; overflow: auto;">
                                <?php if(isset($actions['exclude_category'])) { ?>
                                  <?php foreach ($actions['exclude_category'] as $category) { ?>
                                  <div id="exclude-category<?php echo $category['category_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $category['name']; ?>
                                    <input type="hidden" name="actions[1][exclude_category][]" value="<?php echo $category['category_id']; ?>" />
                                  </div>
                                  <?php } ?>
                                <?php } ?>
                                </div>
                              </div>
                            </div>

                        </div>


                          <div style="display:none;"  id="get_cond_ranges_row1"  >
                          <?php if(isset($actions['ranges'])) { ?>
                                <?php foreach ($actions['ranges'] as $key => $range) { ?>
                                    <div id="range<?php echo $key; ?>" style="margin-top:10px;" class="row">
                                      <div class="col-sm-12">
                                        <label style="width:10%;" class="col-sm-2 control-label" for=""><span data-toggle="tooltip" title="Kampanyanın çalışabilmesi için zorunlu olan min. sepet tutarını girin"> 'den büyük</span></label>
                                        <div class="col-sm-2">
                                          <input type="number" name="actions[1][ranges][<?php echo $key; ?>][total]" value="<?php echo isset($range['total'])? $range['total'] : ''; ?>" placeholder="" class="form-control" />
                                        </div>


                                        <div class="col-sm-2">
                                            <select class="form-control" name="actions[1][ranges][<?php echo $key; ?>][type]" id="discount_type">
                                             <option disabled> -- İndirim Türünü Seç -- </option>
                                             <option <?php echoSelected($range['type'], 'fixed'); ?> value="fixed">Sabit Değer</option>
                                             <option <?php echoSelected($range['type'], 'percentage'); ?> value="percentage">Yüzde %</option>
                                            </select>
                                        </div>



                                        <div class="col-sm-2">
                                          <div class="input-group">
                                          <div class="input-group-addon"> Miktar</div>
                                          <div class="input-group-addon amount_type_addon"> <?php echo $currency; ?></div>
                                            <input type="number" name="actions[1][ranges][<?php echo $key; ?>][amount]" value="<?php echo isset($range['amount'])? $range['amount'] : ''; ?>" class="form-control" />
                                          </div>
                                        </div>


                                        <div class="col-sm-2">
                                          <a data-remove-id="<?php echo $key; ?>" data-toggle="tooltip" title="Remove" class="btn btn-danger" onclick="remove_range(<?php echo $key; ?>);" data-original-title="">
                                          <i class="fa fa-trash"></i></a>
                                        </div>
                                        </div>
                                        <hr>
                                    </div>
                                <?php } ?>
                          <?php } ?>
                          </div>
                              <hr><div id="get_cond_ranges1" style="display:none" class="col-sm-1">
                                <a onclick="addrange(1);" class="btn btn-primary"><i class="fa fa-plus"></i>&nbsp;Aralık</a>
                              </div>
                        </div>
                      </div>
              </div>
 <script type="text/javascript">
  $(document).ready(function () {

    $('.date').datetimepicker({
      pickTime: false
    });

    $('#get_conditions_group div[id^=getcondition]').each(function () {
            get_cond_getField($(this).data('id'));
    });

    var discount_type = $('#discount_type').val();

    if(discount_type == 'fixed' || discount_type == 'fixed_price' ) {
      $('.amount_type_addon').text('<?php echo $currency; ?>');
    } else {
      $('.amount_type_addon').text('%');
    }

    $('#discount_type').on('change', function() {
      if($(this).val() == 'fixed' || $(this).val() == 'fixed_price' ) {
        $('.amount_type_addon').text('<?php echo $currency; ?>');
      } else {
        $('.amount_type_addon').text('%');
      }
    });

  });


    //products-autocomplete
$('#input-exclude-product').autocomplete({
  'source': function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/product/autocomplete&<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
      dataType: 'json',
      success: function(json) {
        response($.map(json, function(item) {
          return {
            label: item['name'],
            value: item['product_id']
          }
        }));
      }
    });
  },
  'select': function(item) {
    $('#input-exclude-product').val('');

    $('#exclude-product' + item['value']).remove();

    $('#exclude-product').append('<div id="exclude-product' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="actions[1][exclude_product][]" value="' + item['value'] + '" /></div>');
  }
});

$('#exclude-product').delegate('.fa-minus-circle', 'click', function() {
  $(this).parent().remove();
});

// Category
$('#input-exclude-category').autocomplete({
  'source': function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/category/autocomplete&<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
      dataType: 'json',
      success: function(json) {
        response($.map(json, function(item) {
          return {
            label: item['name'],
            value: item['category_id']
          }
        }));
      }
    });
  },
  'select': function(item) {
    $('#input-exclude-category').val('');

    $('#exclude-category' + item['value']).remove();

    $('#exclude-category').append('<div id="exclude-category' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="actions[1][exclude_category][]" value="' + item['value'] + '" /></div>');
  }
});

$('#exclude-category').delegate('.fa-minus-circle', 'click', function() {
  $(this).parent().remove();
});



    //products-autocomplete
$('#input-get-product').autocomplete({
  'source': function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/product/autocomplete&<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
      dataType: 'json',
      success: function(json) {
        response($.map(json, function(item) {
          return {
            label: item['name'],
            value: item['product_id']
          }
        }));
      }
    });
  },
  'select': function(item) {
    $('#input-get-product').val('');

    $('#get-product' + item['value']).remove();

    $('#get-product').append('<div id="buy-product' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="actions[1][product][]" value="' + item['value'] + '" /></div>');
  }
});

$('#get-product').delegate('.fa-minus-circle', 'click', function() {
  $(this).parent().remove();
});

    //products-autocomplete
$('#input-exclude-product-from-cat').autocomplete({
  'source': function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/product/autocomplete&<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
      dataType: 'json',
      success: function(json) {
        response($.map(json, function(item) {
          return {
            label: item['name'],
            value: item['product_id']
          }
        }));
      }
    });
  },
  'select': function(item) {
    $('#input-exclude-product-from-cat').val('');

    $('#exclude-product-from-cat' + item['value']).remove();

    $('#exclude-product-from-cat').append('<div id="exclude-product-from-cat' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="actions[1][exclude_product_from_cat][]" value="' + item['value'] + '" /></div>');
  }
});

$('#exclude-product-from-cat').delegate('.fa-minus-circle', 'click', function() {
  $(this).parent().remove();
});

// Category
$('#input-get-category').autocomplete({
  'source': function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/category/autocomplete&<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
      dataType: 'json',
      success: function(json) {
        response($.map(json, function(item) {
          return {
            label: item['name'],
            value: item['category_id']
          }
        }));
      }
    });
  },
  'select': function(item) {
    $('#input-get-category').val('');

    $('#get-category' + item['value']).remove();

    $('#get-category').append('<div id="buy-category' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="actions[1][category][]" value="' + item['value'] + '" /></div>');
  }
});

$('#get-category').delegate('.fa-minus-circle', 'click', function() {
  $(this).parent().remove();
});


// Manufacturer
    $('#input-get-manufacturer').autocomplete({
        'source': function(request, response) {
            $.ajax({
                url: 'index.php?route=catalog/manufacturer/autocomplete&<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
                dataType: 'json',
                success: function(json) {
                    response($.map(json, function(item) {
                        return {
                            label: item['name'],
                            value: item['manufacturer_id']
                        }
                    }));
                }
            });
        },
        'select': function(item) {
            $('#input-get-manufacturer').val('');

            $('#buy-manufacturer' + item['value']).remove();

            $('#get-manufacturer').append('<div id="buy-manufacturer' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="actions[1][manufacturer][]" value="' + item['value'] + '" /></div>');
        }
    });

    $('#get-manufacturer').delegate('.fa-minus-circle', 'click', function() {
        $(this).parent().remove();
    });

    //products-autocomplete
    $('#input-exclude-product-from-manufacturer').autocomplete({
        'source': function(request, response) {
            $.ajax({
                url: 'index.php?route=catalog/product/autocomplete&<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
                dataType: 'json',
                success: function(json) {
                    response($.map(json, function(item) {
                        return {
                            label: item['name'],
                            value: item['product_id']
                        }
                    }));
                }
            });
        },
        'select': function(item) {
            $('#input-exclude-product-from-manufacturer').val('');

            $('#exclude-product-from-manufacturer' + item['value']).remove();

            $('#exclude-product-from-manufacturer').append('<div id="exclude-product-from-manufacturer' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="actions[1][exclude_product_from_manufacturer][]" value="' + item['value'] + '" /></div>');
        }
    });

    $('#exclude-product-from-manufacturer').delegate('.fa-minus-circle', 'click', function() {
        $(this).parent().remove();
    });

</script>

<script type="text/javascript">

function changeAmountTypeAddon(id, element) {
  var discount_type = $(element).val();

    if(discount_type == 'fixed' || discount_type == 'fixed_price' ) {
      $(element).parent().parent().find('.amount_type_addon_range'+id).text('<?php echo $currency; ?>');
    } else {
      $(element).parent().parent().find('.amount_type_addon_range'+id).text('%');
    }
}

var range = <?php echo (isset($actions['ranges'])) ? count($actions['ranges']) : 0 ?>;
function addrange(id) {

  var total = $('#get_cond_ranges_row'+id+' div.row:first input:first').val() * ($('#get_cond_ranges_row'+id+' div.row').length+1) || '';

  html =     '<div id="range'+ range +'" style="" class="row"><div class="col-sm-12"><hr>';
  html +=    '<label class="col-sm-2 control-label"><span data-toggle="tooltip" title="Greater than"> Total greater than</span></label>';
  html +=    '<div class="col-sm-2"><div class="input-group"><div style="dispay:none;" class="input-group-addon currency_addon"> <?php echo $currency; ?></div>';
  html +=    '<input type="number" name="actions['+ id +'][ranges]['+ range +'][total]" value="'+total+'" placeholder="" class="form-control" />';
  html +=    '</div>';
  html +=    '</div>';

  html += '<div class="col-sm-2">';
   html += '<select class="form-control" onchange="changeAmountTypeAddon('+ id +', this);" name="actions['+ id +'][ranges]['+ range +'][type]" id="discount_type_range'+ id +'">';
    html += '<option disabled selected> -- İndirim türünü seçin -- </option>';
   html += '<option value="fixed">Sabit Tutar</option>';
   html += '<option value="percentage">Yüzde</option>';
  html += '</select>';
  html +=    '</div>';



  html +=    '<div class="col-sm-2">';
  html +=    '<div class="input-group">';
  html +=     '<div class="input-group-addon"> Tutar</div>';
  html +=     '<div class="input-group-addon amount_type_addon_range'+ id +'"> <?php echo $currency; ?></div>';
  html +=    '<input type="number" name="actions['+ id +'][ranges]['+ range +'][amount]" value="" class="form-control" />';
  html +=    '</div>';
  html +=    '</div>';


  html +=    '<div class="col-sm-2">';
  html +=    '<a data-remove-id="'+ range +'" data-toggle="tooltip" title="Remove" class="btn btn-danger" onclick="remove_range('+ range +');" data-original-title="">';
  html +=    '<i class="fa fa-trash"></i></a>';
  html +=    '</div>';
  html +=    '</div>';
  html +=    '</div>';

  $('#get_cond_ranges_row'+id).append(html);
  range++;
}

function remove_range(id){
  $('#range'+id).remove();
}

    function get_cond_getField(id){
        if($('#getMainSelect'+id).val() == 'order_discount_range'){
            $('#get_cond_ranges'+id).show();
            $('#get_cond_discount_amount'+id).hide();
            $('#exclude_condition_option').hide();
            $('#get_products'+id).hide();
            $('#get_categories'+id).hide();
            $('#get_manufacturers'+id).hide();
            $('#get_cond_ranges_row'+id).show();
            $('#exclude').hide();
        } else if ($('#getMainSelect'+id).val() == 'discount_on_products'){
            $('#get_cond_ranges'+id).hide();
            $('#get_cond_discount_amount'+id).show();
            $('#exclude_condition_option').show();
            $('#get_products'+id).show();
            $('#get_categories'+id).hide();
            $('#get_manufacturers'+id).hide();
            $('#get_cond_ranges_row'+id).hide();
            $('#exclude').hide();
        }
        /*
         else if ($('#getMainSelect'+id).val() == 'add_product_cart'){
         $('#get_cond_ranges'+id).hide();
         $('#get_cond_discount_amount'+id).show();
         $('#get_products'+id).show();
         $('#get_categories'+id).hide();
         $('#get_cond_ranges_row'+id).hide();
         $('#exclude').hide();
         }
         */
        else if ($('#getMainSelect'+id).val() == 'discount_on_categories'){
            $('#get_cond_ranges'+id).hide();
            $('#get_cond_discount_amount'+id).show();
            $('#exclude_condition_option').show();
            $('#get_products'+id).hide();
            $('#get_categories'+id).show();
            $('#get_manufacturers'+id).hide();
            $('#get_cond_ranges_row'+id).hide();
            $('#exclude').hide();
        }
        else if ($('#getMainSelect'+id).val() == 'discount_on_manufacturers'){
            $('#get_cond_ranges'+id).hide();
            $('#get_cond_discount_amount'+id).show();
            $('#exclude_condition_option').show();
            $('#get_products'+id).hide();
            $('#get_categories'+id).hide();
            $('#get_manufacturers'+id).show();
            $('#get_cond_ranges_row'+id).hide();
            $('#exclude').hide();
        } else if($('#getMainSelect'+id).val() == 'discount_all_products') {
            $('#get_cond_ranges'+id).hide();
            $('#get_cond_discount_amount'+id).show();
            $('#exclude_condition_option').show();
            $('#get_products'+id).hide();
            $('#get_categories'+id).hide();
            $('#get_manufacturers'+id).hide();
            $('#get_cond_ranges_row'+id).hide();
            $('#exclude').show();
        } else if($('#getMainSelect'+id).val() == 'free_shipping') {
            $('#get_cond_ranges'+id).hide();
            $('#get_cond_discount_amount'+id).hide();
            $('#exclude_condition_option').hide();
            $('#get_products'+id).hide();
            $('#get_categories'+id).hide();
            $('#get_manufacturers'+id).hide();
            $('#get_cond_ranges_row'+id).hide();
            $('#exclude').hide();
        } else {
            $('#get_cond_ranges'+id).hide();
            $('#get_cond_discount_amount'+id).show();
            $('#get_products'+id).hide();
            $('#exclude_condition_option').hide();
            $('#get_categories'+id).hide();
            $('#get_manufacturers'+id).hide();
            $('#get_cond_ranges_row'+id).hide();
            $('#exclude').hide();
        }
    }

function remove_cond(id){
  $('#condition'+id).remove();
}
</script>
