  <?php
  $condition_rules = isset($moduleData['condition_rules']) ? $moduleData['condition_rules'] : array() ;
  $conditions = isset($moduleData['conditions']) ? $moduleData['conditions'] : array() ;
   ?>

   <div class="row">
    <div class="col-sm-12">
      <h3 style="font-size:13px;">Kampanya şu durumda aktif olsun;
        <select name="condition_rules[condition_combine]" style="display:inline-block;width:70px;margin: 0 5px;" class="form-control input-sm" id="condition_combine">
          <option <?php echo isset($condition_rules['condition_combine']) && $condition_rules['condition_combine'] == 'All' ? 'selected' : ''; ?>  value="All">Aşağıdakilerin hepsi gerçekleştiğinde</option>
          <option <?php echo isset($condition_rules['condition_combine']) && $condition_rules['condition_combine'] == 'Some' ? 'selected' : ''; ?> value="Some">Aşağıdakilerin bazıları gerçekleştiğinde</option>
        </select>
Aşağıdaki koşullar
         <select name="condition_rules[condition_bool]" style="display:inline-block;width:70px;margin: 0 5px;" class="form-control  input-sm" id="condition_bool">
            <option <?php echo isset($condition_rules['condition_bool']) && $condition_rules['condition_bool']== 'True' ? 'selected' : ''; ?> value="True">Aktif</option>
            <option <?php echo isset($condition_rules['condition_bool']) && $condition_rules['condition_bool']== 'False' ? 'selected' : ''; ?> value="False">Pasif</option>
          </select>

         &nbsp;&nbsp;<a href="javascript:void(0)" data-toggle="modal" data-target="#condHelperModal"><i class="fa fa-info-circle" aria-hidden="true"></i></a>
      </h3>
    </div>
  </div>

  <div class="row">
    <div class="col-sm-12">
      <div id="conditions_group">
        <?php foreach ($conditions as $key => $cond) { ?>
            <div class="conditionRowData row" id="condition<?php echo $key; ?>" data-id="<?php echo $key; ?>"><hr>
              <div class="col-sm-3">
                <select id="mainSelect<?php echo $key; ?>" onchange="getFields(this);" name="conditions[<?php echo $key; ?>][condSelect]" class="form-control">
                  <optgroup label="Sepet Koşulu">
                    <option <?php echoSelected($cond['condSelect'], 'cart_quantity'); ?> data-condition="general" value="cart_quantity">Sepet Adedi</option>
                    <option <?php echoSelected($cond['condSelect'], 'cart_total'); ?> data-condition="general" value="cart_total" >Sepet Tutarı</option>
                    <option <?php echoSelected($cond['condSelect'], 'cart_weight'); ?> data-condition="general" value="cart_weight">Sepet Ağırlığı (kg)</option>
                  </optgroup>
                  <optgroup label="Ürün Koşulu">
                    <option <?php echoSelected($cond['condSelect'], 'product_name'); ?> data-condition="product_name" value="product_name">Sepetteki Ürünler (Otomatik Tamamlama)</option>
                    <option <?php echoSelected($cond['condSelect'], 'product_option'); ?> data-condition="product_option" value="product_option">Sepetteki Alt Ürünler (Otomatik Tamamlama)</option>
                    <option <?php echoSelected($cond['condSelect'], 'product_list'); ?> data-condition="product_list" value="product_list">En Az Bir Ürün (Otomatik Tamamlama)</option>
                  </optgroup>
                  <optgroup label="Üye Koşulu">
                    <option <?php echoSelected($cond['condSelect'], 'customer_name'); ?> data-condition="customer_name" value="customer_name">Müşteri</option>
                    <option <?php echoSelected($cond['condSelect'], 'customer_order'); ?> data-condition="general" value="customer_order">Müşteri Tüm Siparişleri Toplamı</option>
                    <option <?php echoSelected($cond['condSelect'], 'customer_reg_date'); ?> data-condition="customer_reg_date" value="customer_reg_date">Müşteri Üyelik Tarihi</option>
                  </optgroup>
                  <optgroup label="Marka Koşulu">
                    <option <?php echoSelected($cond['condSelect'], 'manufac_name'); ?> data-condition="manufac_name" value="manufac_name">Markanın Ürünleri</option>
                  </optgroup>
                  <optgroup label="Kategori Koşulu">
                    <option <?php echoSelected($cond['condSelect'], 'category_name'); ?> data-condition="category_name" value="category_name">Kategorinin Ürünleri</option>
                  </optgroup>
                  <optgroup label="Diğer Koşul">
                    <option <?php echoSelected($cond['condSelect'], 'day'); ?> data-condition="day" value="day">Haftanın Günleri</option>
                  </optgroup>
                </select>

            </div>

            <div class="col-sm-1" style="padding-left: 10px;font-weight: bold;width: 2%;line-height: 36px;"></div>

            <div class="conditionRows">
              <div class="general condition" style="display:none;">
                <span class="col-sm-2">
                  <select name="conditions[<?php echo $key; ?>][condEquals]" class="form-control">
                    <option <?php echoSelected($cond['condEquals'], 'eq'); ?> value="eq" selected="selected">Eşit</option>
                    <option <?php echoSelected($cond['condEquals'], 'neq'); ?> value="neq">Eşit Değil</option>
                    <option <?php echoSelected($cond['condEquals'], 'gtr'); ?> value="gtr">'den Büyük</option>
                    <option <?php echoSelected($cond['condEquals'], 'lth'); ?> value="lth">'den Küçük</option>
                  </select>
                </span>

                <span class="col-xs-3">
                  <input type="text" class="form-control" name="conditions[<?php echo $key; ?>][condition_input]" placeholder="Enter Condition" value="<?php echo isset($cond['condition_input']) ? $cond['condition_input'] : '' ?>" />
                </span>
              </div>

              <div class="customer_reg_date condition" style="display:none;">
                <span class="col-sm-2 date">
                  <div class="input-group">
                    <input type="text" class="form-control" name="conditions[<?php echo $key; ?>][customer_date]" value="<?php echo isset($cond['customer_date']) ? $cond['customer_date'] : '' ?>" placeholder="YYYY-MM-DD" data-date-format="YYYY-MM-DD" />
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                    </span>
                  </div>
                </span>
              </div>

              <div class="product_name condition" style="display:none;">
                <span class="col-sm-2">
                  <input type="text" class="form-control products-autocomplete" data-cond-id="<?php echo $key; ?>" readonly placeholder="Enter Product" value="<?php echo isset($cond['product_name']) ? $cond['product_name'] : '' ?>" />
                  <input type="hidden" name="conditions[<?php echo $key; ?>][product_id]" value="<?php echo isset($cond['product_id']) ? $cond['product_id'] : '' ?>" />
                </span>

                <span class="col-sm-3">
                  <div class="input-group">
                    <div class="input-group-addon">Min. adet:</div>
                      <input type="number" name="conditions[<?php echo $key; ?>][product_quantity]" class="form-control" value="<?php echo isset($cond['product_quantity']) ? $cond['product_quantity'] : '1' ?>" />
                  </div>
                </span>
              </div>

              <div class="product_option condition" style="display:none;">

                <span style="margin-bottom: 10px;" class="col-xs-offset-3 col-xs-6">
                  <input type="text" class="form-control products-autocomplete" data-cond-id="<?php echo $key; ?>" readonly placeholder="Enter Product" value="<?php echo isset($cond['product_name']) ? $cond['product_name'] : '' ?>" />
                  <input type="hidden" name="conditions[<?php echo $key; ?>][product_id]" value="<?php echo isset($cond['product_id']) ? $cond['product_id'] : '' ?>" />
                </span>

                <span style="margin-bottom: 10px;" class="col-xs-offset-3 col-xs-6">
                 <span style="padding:0;" class="col-xs-6">
                  <input type="text" class="form-control products-option-autocomplete" placeholder="Enter Option Name" readonly value="<?php echo isset($cond['option_data']['name']) ? $cond['option_data']['name'] : '' ; ?>" />
                  <input type="hidden" name="conditions[<?php echo $key; ?>][option_id]" value="<?php echo isset($cond['option_data']['option_id']) ? $cond['option_data']['option_id'] : '0' ; ?>" />
                 </span>
                 <span style="padding:0;padding-left: 15px;" class="col-xs-6">
                  <select name="conditions[<?php echo $key; ?>][option_value_id]" class="form-control products-option-value-select">
                    <option disabled="disabled" selected="selected">-- Bir Değer Seçiniz --</option>
                    <?php if(isset($cond['option_data_values'])) : ?>
                    <?php foreach($cond['option_data_values'] as $option_value) : ?>
                    <option  <?php echoSelected($cond['option_value_id'], $option_value['option_value_id']); ?> value="<?php echo $option_value['option_value_id']; ?>"><?php echo $option_value['name']; ?></option>
                    <?php endforeach; ?>
                    <?php endif; ?>
                  </select>
                 </span>
                </span>

                <span style="margin-bottom: 10px;" class="col-xs-offset-3 col-xs-6">
                  <div class="input-group">
                    <div class="input-group-addon">Min. adet:</div>
                      <input type="number" name="conditions[<?php echo $key; ?>][product_option_quantity]" class="form-control" value="<?php echo isset($cond['product_option_quantity']) ? $cond['product_option_quantity'] : '1' ?>" />
                  </div>
                </span>
              </div>

              <div class="product_list condition" style="display:none">
                  <div class="col-sm-4">
                    <input type="text" value="" placeholder="Enter product" class="form-control product-list-autocomplete" />
                    <div id="product-list<?php echo $key; ?>" class="well well-sm" style="height: 150px; overflow: auto;">
                    <?php if(isset($cond['product_list'])) { ?>
                      <?php foreach ($cond['product_list'] as $product) { ?>
                      <div id="product-list-product<?php echo $product['product_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $product['name']; ?>
                        <input type="hidden" name="conditions[<?php echo $key; ?>][product_list][]" value="<?php echo $product['product_id']; ?>" />
                      </div>
                      <?php } ?>
                    <?php } ?>
                    </div>
                  </div>
                  <div class="col-sm-3">
                      <div class="input-group">
                        <div class="input-group-addon"> Min. adet:</div>
                          <input type="number" name="conditions[<?php echo $key; ?>][product_list_quantity]" class="form-control" value="<?php echo isset($cond['product_list_quantity']) ? $cond['product_list_quantity'] : '' ?>" />
                      </div>
                  </div>
              </div>

              <div class="category_name condition" style="display:none;">
                <span class="col-sm-2">
                  <input type="text" class="form-control categories-autocomplete" data-cond-id="<?php echo $key; ?>" readonly placeholder="Enter Category" value="<?php echo isset($cond['category_name']) ? $cond['category_name'] : '' ?>" />
                  <input type="hidden" name="conditions[<?php echo $key; ?>][category_id]" value="<?php echo isset($cond['category_id']) ? $cond['category_id'] : '' ?>" />
                </span>

                <span style="width: 12%;" class="col-sm-2">
                    Üst kategorilerde dahil
                    <input style="float:right;" type="checkbox" name="conditions[<?php echo $key; ?>][category_parent]" <?php echo isset($cond['category_parent']) && $cond['category_parent'] == 'yes' ? 'checked' : '' ?>  value="yes"> <br>
                    Alt kategorilerde dahil
                    <input style="float:right;" type="checkbox" name="conditions[<?php echo $key; ?>][category_child]" <?php echo isset($cond['category_child']) && $cond['category_child'] == 'yes' ? 'checked' : '' ?> value="yes">
                </span>

                <span style="width: 13%;" class="col-sm-1">
                  <div class="input-group">
                    <div class="input-group-addon">Min. adet:</div>
                      <input type="number" name="conditions[<?php echo $key; ?>][category_quantity]" class="form-control" value="<?php echo isset($cond['category_quantity']) ? $cond['category_quantity'] : '1' ?>" />
                  </div>
                </span>
              </div>

              <div class="customer_name condition" style="display:none;">
                <span class="col-sm-2">
                  <input type="text" class="form-control customer-autocomplete" readonly placeholder="Enter Customer" value="<?php echo isset($cond['customer_name']) ? $cond['customer_name'] : '' ?>" />
                  <input type="hidden" name="conditions[<?php echo $key; ?>][customer_id]" value="<?php echo isset($cond['customer_id']) ? $cond['customer_id'] : '' ?>" />
                </span>
              </div>

              <div class="manufac_name condition" style="display:none;">
                <span class="col-sm-2">
                  <input type="text" class="form-control manufacturer-autocomplete" readonly placeholder="Enter Manufacturer" value="<?php echo isset($cond['manufacturer_name']) ? $cond['manufacturer_name'] : '' ?>" />
                  <input type="hidden" name="conditions[<?php echo $key; ?>][manufacturer_id]" value="<?php echo isset($cond['manufacturer_id']) ? $cond['manufacturer_id'] : '' ?>" />
                </span>
                <span class="col-sm-3">
                  <div class="input-group">
                    <div class="input-group-addon"> Min. adet:</div>
                      <input type="number" name="conditions[<?php echo $key; ?>][manufacturer_quantity]" class="form-control" value="<?php echo isset($cond['manufacturer_quantity']) ? $cond['manufacturer_quantity'] : '' ?>" />
                  </div>
                </span>
              </div>

              <div class="day condition" style="display:none;">
                <span class="col-sm-3">
                <select name="conditions[<?php echo $key; ?>][dayofweek]" class="form-control">
                  <option <?php echoSelected($cond['dayofweek'], 'mon'); ?> value="mon" selected="selected">Pazartesi</option>
                  <option <?php echoSelected($cond['dayofweek'], 'tue'); ?> value="tue">Salı</option>
                  <option <?php echoSelected($cond['dayofweek'], 'wed'); ?> value="wed">Çarşamba</option>
                  <option <?php echoSelected($cond['dayofweek'], 'thu'); ?> value="thu">Perşembe</option>
                  <option <?php echoSelected($cond['dayofweek'], 'fri'); ?> value="fri">Cuma</option>
                  <option <?php echoSelected($cond['dayofweek'], 'sat'); ?> value="sat">Cumartesi</option>
                  <option <?php echoSelected($cond['dayofweek'], 'sun'); ?> value="sun">Pazar</option>
                </select>
                </span>
              </div>
            </div>

            <div class="col-sm-1">
              <a data-remove-id="<?php echo $key; ?>" data-toggle="tooltip" title="Remove" class="btn btn-danger" onclick="remove_cond(<?php echo $key; ?>);" data-original-title="">
                <i class="fa fa-trash"></i>
              </a>
            </div>

            </div>
       <?php } ?>
        </div>
    </div>
  </div>

  <div class="row"><hr>
    <div class="col-sm-12">
      <a data-toggle="tooltip" title="Add a new condition" id="addnewcond" class="btn btn-primary" data-original-title=""><i class="fa fa-plus"></i> Koşul Ekle</a>
    </div>
  </div>

  <div id="condHelperModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Yardım</h4>
        </div>
        <div class="modal-body">




          <span style="font-size:13px;" class="text"> Koşullar sekmesi promosyonun kurallarını tanımlar. Müşterinin indirimi alabileceğiniz eylemler Sekmesine ilerlemeden önce, koşulları atamalısınız.<br>
          <br><br>

          Açıklama:
          <hr>
          <ol>
            <li>Sepet koşulu çok basit. Koşulları karşılamak için müşteri, sepette tam / düşük / yüksek miktarda ürün / ağırlık / fiyat bulundurmalıdır.</li>
            <li>Ürün / kategori / üretici koşulları, müşterinin belirtilen ürünü belirtilen kategori / marka veya ürüne sahip olmasını gerektirir. Bu ürün olmadan (veya kategori / marka ürününden), koşul karşılanmayacaktır.
            </li>
            <li>Müşteri koşulu, yalnızca belirli müşterilere sunulacak, müşteriye özel promosyonlar oluşturmak için kullanılabilir.</li>
            <li>"Haftanın Günü" şartı açıktır. Promosyon haftanın bu günü geçerli olacak (her Pazartesi / Cuma günü% X indirim) gibi promosyonlar için geçerli olacaktır.</li>
          </ol>
          <hr>
          Büyük promosyonlar yapmak için çeşitli promosyonları gerçekleştirmek için farklı koşulları birleştirebilirsiniz. <br><br>

          <strong>Uyarı:</strong> Promosyonu koşulsuz olarak bırakırsanız, her zaman geçerli olacaktır (müşteri, promosyonun genel ayarını yerine getirmemişse hariç).
          </span>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Kapat</button>
        </div>
      </div>

    </div>
  </div>






<script type="text/javascript">
  function getFields(element){
    var mainSelect = $(element).val();
    var selectedOption = $(element).find(":selected").attr('data-condition');

/*
    if(mainSelect == 'cart_total') {
      html = '<div class="input-group"><div class="input-group-addon"><?php echo $currency; ?></div></div>';
      $('#condition-input'+id).append(html).find('input').appendTo('#condition-input'+id+' .input-group');
    } else {
      $('#condition-input'+id+' .input-group').find('input').prependTo('#condition-input'+id);
      $('#condition-input'+id+' .input-group').remove();
    }
*/
    $(element).parents('.conditionRowData').find('.conditionRows div.condition').hide();
    $(element).parents('.conditionRowData').find('.conditionRows .' + selectedOption).show();
  }


  $(document).ready(function () {
    $('#conditions_group div[id^=condition]').each(function () {

      var id = $(this).data('id');

      getFields($(this).find('#mainSelect'+id));
      applyAutocomplete(id);
    });
  });
</script>
