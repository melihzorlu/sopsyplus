    <div class="form-group required">
      <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="<?php echo $helper_module_name; ?>"><?php echo $text_module_name; ?></span></label>
      <div class="col-sm-6">
        <input type="text" name="name" value="<?php echo isset($moduleData['name']) ? $moduleData['name'] : ''; ?>" placeholder="<?php echo $text_module_name; ?>" id="input-name" class="form-control">
      </div>
    </div>

    <?php foreach ($languages as $key => $value) { ?>
      <div class="form-group required">
        <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="<?php echo $helper_promo_name; ?>"><?php echo $text_promo_name; ?></span></label>
        <div class="col-sm-6">
          <div class="input-group">
            <span class="input-group-btn">
              <button class="btn btn-default" type="button"><img src="<?php echo $value['flag_url']; ?>"></button>
            </span>
            <input type="text" name="general_settings[name][<?php echo $key; ?>]" value="<?php echo isset($moduleData['general_settings']['name'][$key]) ? $moduleData['general_settings']['name'][$key] : ''; ?>" placeholder="<?php echo $text_promo_name; ?>" id="input-name" class="form-control">
          </div>
        </div>
      </div>
    <?php } ?>




    <div class="form-group required">
      <label class="col-sm-2 control-label"><?php echo $text_status; ?></label>
      <div class="col-sm-6">
        <select name="status" id="input-status" class="form-control">
              <option <?php echo (isset($moduleData['status']) && $moduleData['status'] == 'yes') ? 'selected="selected"' : ''; ?>   value="yes" selected="selected">Aktif</option>
              <option <?php echo (isset($moduleData['status']) && $moduleData['status'] == 'no') ? 'selected="selected"' : ''; ?>  value="no">Pasif</option>
          </select>
      </div>
    </div>

    <div class="form-group required">
      <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="<?php echo $helper_stackable; ?>"><?php echo $text_stackable; ?></span></label>
      <div class="col-sm-6">
        <select name="general_settings[stackable]" id="select-stackable" class="form-control">
              <option <?php echo (isset($moduleData['general_settings']['stackable']) && $moduleData['general_settings']['stackable'] == 'yes') ? 'selected="selected"' : ''; ?>   value="yes" selected="selected">Evet</option>
              <option <?php echo (isset($moduleData['general_settings']['stackable']) && $moduleData['general_settings']['stackable'] == 'no') ? 'selected="selected"' : ''; ?>  value="no">Hayır</option>
          </select>
      </div>
    </div>

    <div class="form-group required">
      <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="Bunu evet olarak ayarlarsanız, promosyon aynı sırayla birden fazla kez uygulanabilir.">Birden fazla kullanılabilsin mi</span></label>
      <div class="col-sm-6">
        <select name="general_settings[multiply]" id="select-stackable" class="form-control">
              <option <?php echo (isset($moduleData['general_settings']['multiply']) && $moduleData['general_settings']['multiply'] == 'yes') ? 'selected="selected"' : ''; ?>   value="yes" selected="selected">Evet</option>
              <option <?php echo (isset($moduleData['general_settings']['multiply']) && $moduleData['general_settings']['multiply'] == 'no') ? 'selected="selected"' : ''; ?>  value="no">Hayır</option>
          </select>
      </div>
    </div>

    <div class="form-group required">
      <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="<?php echo $helper_sort_order; ?>"><?php echo $text_sort_order; ?></span></label>
      <div class="col-sm-6">
              <input type="number" name="general_settings[sort_order]" min="0" value="<?php echo isset($moduleData['general_settings']['sort_order']) ? $moduleData['general_settings']['sort_order'] : '0'; ?>" id="input-sort-order" class="form-control">
      </div>
    </div>

  <div class="form-group">
        <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="<?php echo $helper_start_date; ?>"><?php echo $text_start_date; ?></span></label>
        <div class="col-sm-6 date">
          <div class="input-group">
            <input type="text" name="general_settings[StartDate]" value="<?php echo isset($moduleData['general_settings']['StartDate']) ? $moduleData['general_settings']['StartDate'] : ''; ?>" placeholder="YYYY-MM-DD" data-date-format="YYYY-MM-DD" class="form-control">
            <span class="input-group-btn">
              <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
            </span>
          </div>
        </div>
  </div>

  <div class="form-group">
        <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="<?php echo $helper_end_date; ?>"><?php echo $text_end_date; ?></span></label>
        <div class="col-sm-6 date">
          <div class="input-group">
            <input type="text" name="general_settings[EndDate]" value="<?php echo isset($moduleData['general_settings']['EndDate']) ? $moduleData['general_settings']['EndDate'] : ''; ?>" placeholder="YYYY-MM-DD" data-date-format="YYYY-MM-DD" class="form-control">
            <span class="input-group-btn">
              <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
            </span>
          </div>
        </div>
  </div>

  <div class="form-group">
        <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="<?php echo $helper_total_uses; ?>"><?php echo $text_total_uses; ?></span></label>
        <div class="col-sm-6">
          <input type="number" name="general_settings[TotalUses]" min="0" value="<?php echo isset($moduleData['general_settings']['TotalUses']) ? $moduleData['general_settings']['TotalUses'] : '0'; ?>" class="form-control">
        </div>
  </div>

  <div class="form-group">
        <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="<?php echo $helper_user_total_uses; ?>"><?php echo $text_user_total_uses; ?></span></label>
        <div class="col-sm-6">
          <input type="number" name="general_settings[TotalUsesCustomer]" min="0" value="<?php echo isset($moduleData['general_settings']['TotalUsesCustomer']) ? $moduleData['general_settings']['TotalUsesCustomer'] : '0'; ?>" class="form-control">
        </div>
  </div>

  <div class="form-group">
      <label class="col-sm-2 control-label" for="promotion_type"><span data-toggle="tooltip" title="<?php echo $helper_promo_type; ?>"><?php echo $text_promo_type; ?></span></label>
      <div class="col-sm-6">
        <select name="general_settings[PromotionType]" id="promotion_type" class="form-control">
              <option value="1" <?php echo (isset($moduleData['general_settings']['PromotionType']) && $moduleData['general_settings']['PromotionType'] == 1) ? 'selected="selected"' : ''; ?> >Otomatik Kampanya</option>
              <option value="0" <?php echo (isset($moduleData['general_settings']['PromotionType']) && $moduleData['general_settings']['PromotionType'] == 0) ? 'selected="selected"' : ''; ?>  >Kupon Kampanyası</option>
          </select>
      </div>
  </div>

  <div style="display:none" id="coupon_code" class="form-group">
        <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="<?php echo $helper_coupon_code; ?>"><?php echo $text_coupon_code; ?></span></label>
        <div class="col-sm-6">
          <input type="text" name="general_settings[CouponCode]" value="<?php echo isset($moduleData['general_settings']['CouponCode']) ? $moduleData['general_settings']['CouponCode'] : ''; ?>" class="form-control">
        </div>
  </div>

  <div class="form-group">
      <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="<?php echo $helper_need_login; ?>"><?php echo $text_need_login; ?></span></label>
      <div class="col-sm-6">
        <select name="general_settings[need_login]" id="need_login" class="form-control">
              <option value="0" <?php echo (isset($moduleData['general_settings']['need_login']) && $moduleData['general_settings']['need_login'] == 0) ? 'selected="selected"' : ''; ?>  >Hayır</option>
              <option value="1" <?php echo (isset($moduleData['general_settings']['need_login']) && $moduleData['general_settings']['need_login'] == 1) ? 'selected="selected"' : ''; ?>  >Evet</option>
          </select>
      </div>
  </div>

    <div id="cust_groups" class="form-group">
      <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="<?php echo $helper_customer_groups; ?>"><?php echo $text_customer_groups; ?></span></label>
      <div class="col-sm-6">
        <div class="well well-sm" style="height: 150px; overflow: auto;">
          <?php foreach ($customer_groups as $cstgr) { ?>
            <?php $selected = (isset($moduleData['general_settings']['CustGroup']) && in_array($cstgr['customer_group_id'], $moduleData['general_settings']['CustGroup'])) ? 'checked="checked"' : ''; ?>
            <?php if(!isset($moduleData['general_settings']['CustGroup'])) $selected = 'checked="checked"'; ?>
            <div class="checkbox">
              <label>
              <input type="checkbox" name="general_settings[CustGroup][]" value="<?php echo $cstgr['customer_group_id']; ?>" <?php echo $selected; ?> >&nbsp;<?php echo $cstgr['name']; ?>
              </label>
            </div>
          <?php } ?>

        </div>
      </div>
    </div>

    <div class="form-group">
      <label class="col-sm-2 control-label"><?php echo $text_stores; ?></label>
      <div class="col-sm-6">
        <div class="well well-sm" style="height: 150px; overflow: auto;">
          <?php foreach ($stores as $store) { ?>
          <div class="checkbox">
              <label>
              <?php if (isset($moduleData['general_settings']['Stores']) && in_array($store['store_id'], $moduleData['general_settings']['Stores'])) { ?>
              <input type="checkbox" name="general_settings[Stores][]" value="<?php echo $store['store_id']; ?>" checked="checked" />
              <?php echo $store['name']; ?>
              <?php } else { ?>
              <input type="checkbox" name="general_settings[Stores][]" value="<?php echo $store['store_id']; ?>" />
              <?php echo $store['name']; ?>
              <?php } ?>
            </label>
          </div>
          <?php } ?>
        </div>
      </div>
    </div>

<script type="text/javascript">
$('.date').datetimepicker({
  pickTime: false
});

$(document).ready(function() {
  $('#promotion_type').change(function(){
    if($(this).val() == 0){
      $('#coupon_code').show();
    } else{
      $('#coupon_code').val('');
      $('#coupon_code').hide();
    }
  });

  if($('#promotion_type').val() == 0){
      $('#coupon_code').show();
    } else{
      $('#coupon_code').val('');
      $('#coupon_code').hide();
    }

  $('#need_login').change(function(){
    if($(this).val() == 1){
      $('#cust_groups').show();
    } else{
      $('#cust_groups').hide();
    }
  });

  if($('#need_login').val() == 1){
      $('#cust_groups').show();
    } else{
      $('#cust_groups').hide();
    }

});
</script>
