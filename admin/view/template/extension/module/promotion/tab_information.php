<?php
  $information_page = isset($moduleData['information_page']) ? $moduleData['information_page'] : array() ;
?>
<div class="form-group">
  <label class="col-sm-3 control-label" style="text-align: left;" for="input-image"><span data-toggle="tooltip" title="">Kampanyalar Sayfası:</label>
  <div class="col-sm-6">
    <select name="information_page[status]" id="input-info-page" class="form-control">
        <option <?php echo (isset($information_page['status']) && $information_page['status'] == 'yes') ? 'selected="selected"' : ''; ?> value="yes" selected="selected">Aktif</option>
        <option <?php echo (isset($information_page['status']) && $information_page['status'] == 'no' || isset($is_new))  ? 'selected="selected"' : ''; ?> value="no">Pasif</option>
      </select>
  </div>
</div> 

<div style="display:none;" data-triggered-from="#input-info-page" class="display_when_enabled">
  <div class="form-group">
     <div class="col-sm-offset-3 col-sm-6">
        <div class="alert alert-info"><i class="fa fa-thumb-tack"></i> Kampanyalar burada listelenir:
            <?php echo HTTP_CATALOG; ?>index.php?route=<?php echo $modulePath; ?>
        </div>
    </div>
  </div>

  <div class="form-group">
    <label class="col-sm-3 control-label" style="text-align: left;"><span data-toggle="tooltip" title="Link ">Kampanyayı ürün tabında göster (ürün sayfası tablarında)</label>
    <div class="col-sm-6">
      <select name="information_page[link_promotion]" id="input-link-promo" class="form-control">
          <option <?php echo (isset($information_page['link_promotion']) && $information_page['link_promotion'] == 'yes') ? 'selected="selected"' : ''; ?> value="yes">Evet</option>
          <option <?php echo (isset($information_page['link_promotion']) && $information_page['link_promotion'] == 'no') ? 'selected="selected"' : ''; ?> value="no">Hayır</option>
        </select>
    </div>
  </div> 

  <div class="form-group">
    <label class="col-sm-3 control-label" style="text-align: left;"><span data-toggle="tooltip" title="Link ">Sepete ekle butonunun altına link ekle </label>
    <div class="col-sm-6">
      <select name="information_page[button_link]" id="input-link-promo" class="form-control">
          <option <?php echo (isset($information_page['button_link']) && $information_page['button_link'] == 'yes') ? 'selected="selected"' : ''; ?> value="yes">Evet</option>
          <option <?php echo (isset($information_page['button_link']) && $information_page['button_link'] == 'no') ? 'selected="selected"' : ''; ?> value="no">Hayır</option>
        </select>
    </div>
  </div>

  <div class="form-group">
    <label class="col-sm-3 control-label" style="word-break: normal; text-align: left;"><span data-toggle="tooltip" title="Display the products / categories / brands associated in this promotion">Ürünleri / Kategorileri / Markaları görüntüle</label>
    <div class="col-sm-6">
      <select name="information_page[display_products]" id="input-display-products" class="form-control">
          <option <?php echo (isset($information_page['display_products']) && $information_page['display_products'] == 'yes') ? 'selected="selected"' : ''; ?> value="yes">Evet</option>
          <option <?php echo (isset($information_page['display_products']) && $information_page['display_products'] == 'no') ? 'selected="selected"' : ''; ?> value="no">Hayır</option>
        </select>
    </div>
  </div>

  <div class="form-group">
    <label class="col-sm-3 control-label" style="text-align: left;">Açıklama:</label>
    <div class="col-sm-6">
      <ul class="nav nav-tabs popup_tabs">
        <?php foreach ($languages as $key => $value) { ?>
          <li><a href="#description_<?php echo $key; ?>" data-toggle="tab"><img src="<?php echo $value['flag_url']; ?>"> <?php echo $value['name']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  
  <div class="form-group">
	  <div class="col-sm-6 col-sm-offset-3">
        <div class="tab-content">
          <?php foreach ($languages as $key => $value) { ?>
            <div id="description_<?php echo $key; ?>" class="tab-pane fade in active">
              <textarea name="information_page[description][<?php echo $key; ?>]" placeholder="">
              <?php echo isset($information_page['description'][$key]) ? $information_page['description'][$key] : ''; ?>
              </textarea>
            </div>
          <?php } ?>
        </div>
    </div>
  </div>

  <div class="form-group">
    <label class="col-sm-3 control-label" style="text-align: left;" for="input-seo-slug">SEO:</label>
    <div class="col-sm-6">
      <input type="text" class="form-control" name="information_page[seo_slug]" value="<?php echo isset($information_page['seo_slug']) ? $information_page['seo_slug'] : '' ; ?>" id="input-seo-slug" />
    </div>
  </div>

  <div class="form-group">
    <label class="col-sm-3 control-label" style="text-align: left;" for="input-page-title">Sayfa Başlığı:</label>
    <div class="col-sm-6">
      <input type="text" class="form-control" name="information_page[page_title]" value="<?php echo isset($information_page['page_title']) ? $information_page['page_title'] : '' ; ?>" id="input-page-title" />
    </div>
  </div>

  <div class="form-group">
    <label class="col-sm-3 control-label" style="text-align: left;" for="input-seo-slug"> Meta Description:</label>
    <div class="col-sm-6">
      <textarea class="form-control" name="information_page[meta_description]"><?php echo isset($information_page['meta_description']) ? $information_page['meta_description'] : '' ; ?></textarea>
    </div>
  </div>

  <div class="form-group">
    <label class="col-sm-3 control-label" style="text-align: left;" for="input-seo-slug">Meta Keywords:</label>
    <div class="col-sm-6">
      <textarea class="form-control"name="information_page[meta_keywords]"><?php echo isset($information_page['meta_keywords']) ? $information_page['meta_keywords'] : '' ; ?></textarea>
    </div>
  </div>

  <div class="form-group">
    <label class="col-sm-3 control-label" style="text-align: left;" for="input-image">Ana Resim:</label>
    <div class="col-sm-6">
      <a href="" id="thumb-image" data-toggle="image" class="img-thumbnail"><img src="<?php echo $information_thumb; ?>" alt="" title="" data-placeholder="<?php echo $image_placeholder; ?>" /></a>
      <input type="hidden" name="information_page[image]" value="<?php echo isset($information_page['image']) ? $information_page['image'] : '' ; ?>" id="input-image" />
    </div>
  </div>

  <div class="form-group">
    <label class="col-sm-3 control-label" style="text-align: left;" for="input-image-small-size">Ana Resim Boyutu</label>
    <div class="col-sm-6">
      <div class="row">
        <div class="col-sm-6">
          <input type="text" name="information_page[main_image_width]" value="<?php echo isset($information_page['main_image_width']) ? $information_page['main_image_width'] : '500'; ?>" placeholder="Width" class="form-control">
        </div>
        <div class="col-sm-6">
          <input type="text" name="information_page[main_image_height]" value="<?php echo isset($information_page['main_image_height']) ? $information_page['main_image_height'] : '500'; ?>" placeholder="Height" class="form-control">
        </div>
      </div>
    </div>
  </div>

  <div class="form-group">
    <label class="col-sm-3 control-label" style="text-align: left;" for="input-image-small-size">Küçük Resim Boyutu</label>
    <div class="col-sm-6">
      <div class="row">
        <div class="col-sm-6">
          <input type="text" name="information_page[small_image_width]" value="<?php echo isset($information_page['small_image_width']) ? $information_page['small_image_width'] : '200'; ?>" placeholder="Width" class="form-control">
        </div>
        <div class="col-sm-6">
          <input type="text" name="information_page[small_image_height]" value="<?php echo isset($information_page['small_image_height']) ? $information_page['small_image_height'] : '200'; ?>" placeholder="Height" class="form-control">
        </div>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript" src="view/javascript/summernote/summernote.js"></script>
<link href="view/javascript/summernote/summernote.css" rel="stylesheet" />
<script type="text/javascript" src="view/javascript/summernote/opencart.js"></script>
<script type="text/javascript"><!--
<?php foreach ($languages as $key => $value) { ?>
$('#description_<?php echo $key; ?> textarea').summernote({height: 300});
<?php } ?>
//--></script> 
<script>
$('.popup_tabs a:first').tab('show');
</script>