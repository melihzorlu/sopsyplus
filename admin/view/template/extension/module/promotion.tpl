<?php

  function echoSelected($cond, $field){
    echo (isset($cond) && $cond == $field) ? 'selected="selected"' : '';
  }

?>
<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-promotion" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i>&nbsp;Geri</a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
      <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
      </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading" style="display:none;">
        <h3 class="panel-title"><i class="fa fa-pencil"></i><?php echo isset($is_new) ? ' ' : ''; ?> <?php echo isset($moduleData['name']) ? ' - '.$moduleData['name'] : ''; ?></h3>
        <?php if(isset($is_new)) { ?>
          <a data-toggle="modal" data-target="#Tutorials" style="margin-top: -6px;padding: 5px 10px;" class="pull-right btn btn-primary">Kampanya Sihirbazı</a>
        <?php } ?>

      </div>
      <div class="panel-body">

        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-promotion" class="form-horizontal">

          <ul class="nav nav-tabs">
            <li class="active"><a href="#tab-general" data-toggle="tab">Genel Ayarlar</a></li>
            <li><a href="#tab-conditions" data-toggle="tab">Şartlar</a></li>
            <li><a href="#tab-actions" data-toggle="tab">Kampanya</a></li>
            <li><a href="#tab-info"data-toggle="tab">Kampanya Sayfası</a></li>
            <!--li><a href="#tab-popup"data-toggle="tab">Popup Settings</a></li-->
          </ul>

          <div class="tab-content">
            <?php
            if (!function_exists('modification_vqmod')) {
              function modification_vqmod($file) {
                if (class_exists('VQMod')) {
                  return VQMod::modCheck(modification($file), $file);
                } else {
                  return modification($file);
                }
              }
            }
            ?>
            <div class="tab-pane active" id="tab-general">
            <?php require_once modification_vqmod(DIR_APPLICATION.'view/template/'.$modulePath.'/tab_general.php'); ?>
            </div>

            <div class="tab-pane" id="tab-conditions">
            <?php require_once modification_vqmod(DIR_APPLICATION.'view/template/'.$modulePath.'/tab_conditions.php'); ?>
            </div>

            <div class="tab-pane" id="tab-actions">
            <?php require_once modification_vqmod(DIR_APPLICATION.'view/template/'.$modulePath.'/tab_actions.php'); ?>
            </div>

            <div class="tab-pane" id="tab-info">
            <?php require_once modification_vqmod(DIR_APPLICATION.'view/template/'.$modulePath.'/tab_information.php'); ?>
            </div>

            <!--div class="tab-pane" id="tab-popup">
            <?php require_once modification_vqmod(DIR_APPLICATION.'view/template/'.$modulePath.'/tab_popup.php'); ?>
            </div-->

            </div>
        </form>
      </div>
    </div>
  </div>
</div>


<!-- Modal -->
<div id="Tutorials" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Örnek Uygulama</h4>
      </div>
      <div class="modal-body">
        <p>
          Örnek uygulamayı izleyerek gerçek bir kampanya nasıl kurulur görebilirsiniz.
        </p>
        <ol id="tutorials_list" style="margin-top: 20px;">
          <li style="margin-top:2px; margin-bottom: 7px;" onclick="start_tour(data_tour_bogof);"><a class="btn btn-default" >1 alana 1 bedava</a></li>
          <li style="margin-top:2px; margin-bottom: 7px;" onclick="start_tour(data_tour_flat_disc_on_cat);"><a class="btn btn-default" >Seçilen kategoride indirim</a></li>
          <li style="margin-top:2px; margin-bottom: 7px;" onclick="start_tour(data_free_shipping_on_orders);"><a class="btn btn-default" >X TL üzeri kargo ücretsiz</a></li>
        </ol>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Kapat</button>
      </div>
    </div>

  </div>
</div>

<script type="text/javascript">

$('.display_when_enabled').each(function() {
  if($($(this).attr('data-triggered-from')).val() == 'yes') {
    $(this).show();
  }
});

$(document).ready(function() {
  $('.display_when_enabled').each(function() {
    var ele = $(this);
    $( ele.attr('data-triggered-from') ).on('change', function() {
      if($(this).val() == 'yes') {
        ele.show();
      } else {
        ele.hide();
      }
    });
  });
});


$('#tutorials_list li').click(function() {
  $('#Tutorials').modal('hide');
});


$(document).ready(function () {


      var conds = ($('#conditions_group div[id^=condition].row').last().data('id') > 0) ? $('#conditions_group div[id^=condition].row').last().data('id') + 1 : '1';
      $('#addnewcond').click(function (){

            html =             '<div class="conditionRowData row" id="condition'+ conds +'" data-id="'+ conds +'">';
            html +=            '<hr><div class="col-xs-3">';
            html +=            '<select id="mainSelect'+ conds +'" onchange="getFields(this);" name="conditions['+ conds +'][condSelect]" class="form-control">';
            html +=            '<option disabled="disabled" selected="selected">-- Koşul Belirleyin --</option>'
            html +=            '<optgroup label="Sepet Koşulu">';
            html +=            '<option value="cart_quantity" data-condition="general">Sepet Adedi</option>';
            html +=            '<option value="cart_total" data-condition="general" >Sepet Tutarı</option>';
            html +=            '<option value="cart_weight" data-condition="general">Sepet Ağırlığı (kg)</option>';
            html +=            '</optgroup>';
            html +=            '<optgroup label="Ürün Koşulu">';
            html +=            '<option value="product_name" data-condition="product_name">Sepetteki Ürünler</option>';
            html +=            '<option value="product_option" data-condition="product_option">Ürün Sepetindeki Seçeneği İle</option>';
            html +=            '<option value="product_list" data-condition="product_list">Listedeki Ürünlerden En Az Biri</option>';
            html +=            '</optgroup>';
            html +=            '<optgroup label="Müşteri Koşulları">';
            html +=            '<option value="customer_name" data-condition="customer_name">Müşteri Adı</option>';
            html +=            '<option value="customer_order" data-condition="general">Müşteri Sipariş Sayısı</option>';
            html +=            '<option value="customer_reg_date" data-condition="customer_reg_date">Müşteri Giriş Tarihi</option>';
            html +=            '</optgroup>';
            html +=            '<optgroup label="Marka Koşulları">';
            html +=            '<option value="manufac_name" data-condition="manufac_name">Sepetteki Markaya Ait Ürünler</option>';
            html +=            '</optgroup>';
            html +=            '<optgroup label="Kategori Gereksinimleri">';
            html +=            '<option value="category_name" data-condition="category_name">Alışveriş sepetinde ki Ürün Kategorisi</option>';
            html +=            '</optgroup>';
            html +=            '<optgroup label="Diğer Koşullar">';
            html +=            '<option value="day" data-condition="day">Haftanın Günleri</option>';
            html +=            '</optgroup>';
            html +=            '</select>';
            html +=            '</div>';

            html +=                 '<div class="col-xs-1" style="font-weight: bold;width: 2%;line-height: 36px;">ise</div>';

            html +=                 '<div class="conditionRows">';
            html +=                 '<div class="general condition" style="display:none;">';
            html +=                 ' <span class="col-sm-2">';
            html +=                 '  <select name="conditions['+ conds +'][condEquals]" class="form-control">';
            html +=                 '   <option value="eq" selected="selected">Eşit</option>';
            html +=                 '   <option value="neq">Eşit Değil</option>';
            html +=                 '   <option value="gtr">Bundan Büyük</option>';
            html +=                 '   <option value="lth">Bundan Küçük</option>';
            html +=                 '  </select>';
            html +=                 ' </span>';

            html +=                 ' <span class="col-xs-3">';
            html +=                    '<input type="text" class="form-control" name="conditions['+ conds +'][condition_input]" placeholder="Koşul Girin" value="" />';
            html +=                 ' </span>';
            html +=                 '</div>';

            html += '<div class="customer_reg_date condition" style="display:none;">';
            html += '  <span class="col-xs-3 date">';
            html += '    <div class="input-group">';
            html += '      <input type="text" name="conditions[' + conds + '][customer_date]" class="form-control" value="" placeholder="YYYY-AA-GG" data-date-format="YYYY-AA-GG" />';
            html += '      <span class="input-group-btn">';
            html += '        <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>';
            html += '      </span>';
            html += '    </div>';
            html += '  </span>';
            html += '</div>';

            html += '<div class="product_name condition" style="display:none;">';
            html += ' <span class="col-xs-2">';
            html += '   <input type="text" class="form-control products-autocomplete" placeholder="Ürün Girin" value="" />';
            html += '   <input type="hidden" name="conditions[' + conds + '][product_id]" value="" />';
            html += ' </span>';

            html += ' <span class="col-xs-3">';
            html += '   <div class="input-group">';
            html += '     <div class="input-group-addon">Min. quantity</div>';
            html += '     <input type="number" name="conditions[' + conds + '][product_quantity]" class="form-control" value="1" />';
            html += '   </div>';
            html += ' </span>';
            html += '</div>';

            html += '<div class="product_option condition" style="display:none;">';
            html += ' <span style="margin-bottom: 10px;" class="col-xs-offset-3 col-xs-6">';
            html += '   <input type="text" class="form-control products-autocomplete" placeholder="Ürün Girin" value="" />';
            html += '   <input type="hidden" name="conditions[' + conds + '][product_id]" value="" />';
            html += ' </span>';

            html += ' <span style="margin-bottom: 10px;" class="col-xs-offset-3 col-xs-6">';
            html += '  <span style="padding:0;" class="col-xs-6">';
            html += '   <input type="text" class="form-control products-option-autocomplete" placeholder="Seçenek İsmi Girin" value="" />';
            html += '   <input type="hidden" name="conditions[' + conds + '][option_id]" value="" />';
            html += '  </span>';
            html += ' <span style="padding:0;padding-left: 15px;" class="col-xs-6">';
            html += '   <select name="conditions[' + conds + '][option_value_id]" class="form-control products-option-value-select">';
            html += '     <option disabled="disabled" selected="selected">-- Seçenek Seçin --</option>';
            html += '   </select>';
            html += '  </span>';
            html += ' </span>';

            html += ' <span style="margin-bottom: 10px;" class="col-xs-offset-3 col-xs-6">';
            html += '   <div class="input-group">';
            html += '     <div class="input-group-addon">Minimum Miktar</div>';
            html += '     <input type="number" name="conditions[' + conds + '][product_quantity]" class="form-control" value="1" />';
            html += '   </div>';
            html += ' </span>';
            html += '</div>';


            html +=                 '<div class="product_list condition" style="display:none">' +
                                      '<div class="col-sm-4">' +
                                        '<input type="text" value="" placeholder="Enter product" class="form-control product-list-autocomplete" />' +
                                        '<div id="product-list'+ conds +'" class="well well-sm" style="height: 150px; overflow: auto;"></div>' +
                                      '</div>' +
                                      '<div class="col-sm-3">' +
                                          '<div class="input-group">' +
                                            '<div class="input-group-addon"> Min. adet:</div>' +
                                              '<input type="number" name="conditions['+ conds +'][product_list_quantity]" class="form-control" value="" />'+
                                          '</div>' +
                                      '</div>' +
                                  '</div>';

                                  html += '<div class="category_name condition" style="display:none;">';
                                  html += ' <span class="col-xs-3">';
                                  html += '   <input type="text" class="form-control categories-autocomplete" placeholder="Kategori Girin" value="" style="margin-bottom: 7px;"/>';
                                  html += '   <input type="hidden" name="conditions[' + conds + '][category_id]" value="" />';
                                  html += '        Ana Kategoriler';
                                  html += '        <input style="float:right;" type="checkbox" name="conditions[' + conds + '][category_parent]" value="evet"> <br>';
                                  html += '        Alt Kategoriler';
                                  html += '        <input style="float:right;" type="checkbox" name="conditions[' + conds + '][category_child]" value="yes"> ';
                                  html += ' </span>';

                                  html += ' <span class="col-sm-3">';
                                  html += '   <div class="input-group">';
                                  html += '     <div class="input-group-addon">Minimum Miktar</div>';
                                  html += '     <input type="number" name="conditions[' + conds + '][category_quantity]" class="form-control" value="1" />';
                                  html += '   </div>';
                                  html += ' </span>';
                                  html += '</div>';

                                  html += '<div class="customer_name condition" style="display:none;">';
                                  html += ' <span class="col-xs-3">';
                                  html += '   <input type="text" class="form-control customer-autocomplete" placeholder="Müşteri Ekleyin" value="" />';
                                  html += '   <input type="hidden" name="conditions[' + conds + '][customer_id]" value="" />';
                                  html += ' </span>';
                                  html += '</div>';


                                  html += '<div class="manufac_name condition" style="display:none;">';
                                  html += '<span class="col-xs-2">';
                                  html += '<input type="text" class="form-control manufacturer-autocomplete" placeholder="Marka Girin" value="" />';
                                  html += '<input type="hidden" name="conditions[' + conds + '][manufacturer_id]" value="" />';
                                  html += '</span>';
                                  html += '<span class="col-sm-4">';
                                  html += '<div class="input-group">';
                                  html += '<div class="input-group-addon">Minimum Miktar</div>';
                                  html += '<input type="number" name="conditions[' + conds + '][manufacturer_quantity]" class="form-control" value="" />';
                                  html += '</div>';
                                  html += '</span>';
                                  html += '</div>';


                                  html += '<div class="day condition" style="display:none;">';
                                  html += '  <span class="col-xs-3">';
                                  html += '  <select name="conditions[' + conds + '][dayofweek]" class="form-control">';
                                  html += '    <option value="mon" selected="selected">Pazartesi</option>';
                                  html += '    <option value="tue">Salı</option>';
                                  html += '    <option value="wed">Çarşamba</option>';
                                  html += '    <option value="thu">Perşembe</option>';
                                  html += '    <option value="fri">Cuma</option>';
                                  html += '    <option value="sat">Cumartesi</option>';
                                  html += '    <option value="sun">Pazar</option>';
                                  html += '  </select>';
                                  html += '  </span>';
                                  html += '</div>';
                                  html += '</div>';


                                  html += '<div class="col-xs-1">';
                                  html += '<a data-remove-id="' + conds + '" data-toggle="tooltip" title="Remove" class="btn btn-danger" onclick="remove_cond(' + conds + ');" data-original-title="">';
                                  html += '<i class="fa fa-trash"></i>';
                                  html += '</a>';
                                  html += '</div>';


            html +=              '</div>';

            $('#conditions_group').append(html);

            $('#reg_date' + conds + ' .date').datetimepicker({
             pickTime: false
            });

            var mainSelect = $('select[name="conditions['+ conds +'][condSelect]"]');

            getFields(mainSelect);

            applyAutocomplete(conds);

            conds++;
      });

});


function applyAutocomplete(id_cond){

  $('#condition'+id_cond).find('.products-autocomplete').autocomplete({
    'source': function(request, response) {
      $.ajax({
        url: '<?php echo $productAutocomplete; ?>&filter_name=' +  encodeURIComponent(request),
        dataType: 'json',
        success: function(json) {

          response($.map(json, function(item) {
            return {
              label: item['name'],
              value: item['product_id']
            }
          }));
        }
      });
    },
    'select': function(item) {
      $(this).val(item['label']);
      $('input[name="conditions['+id_cond+'][product_id]"]').val(item['value']);
      $(this).parent().find('.products-autocomplete').attr('readonly', true);

    }
  });

  $('#condition'+id_cond).find('.products-autocomplete').click(function() {
      $(this).attr('readonly', false);
      $(this).val('');
  });

  $('#condition'+id_cond).find('.products-option-autocomplete').autocomplete({
    'source': function(request, response) {
      $.ajax({
        url: '<?php echo $productOptionAutocomplete; ?>&filter_name=' +  encodeURIComponent(request),
        dataType: 'json',
        success: function(json) {
          response($.map(json, function(item) {
            return {
              label: item['name'],
              value: item['option_id'],
              option_values: item['option_values']
            }
          }));
        }
      });
    },
    'select': function(item) {

      $(this).val(item['label']);

      $('input[name="conditions['+id_cond+'][option_id]"]').val(item['value']);

      $(this).parent().find('.products-option-autocomplete').attr('readonly', true);

      var options = '<option disabled="disabled" selected="selected">-- Select an option value --</option>';

      $.map(JSON.parse(item['option_values']), function(arr_item) {
        options += '<option value="'+arr_item['option_value_id']+'">'+arr_item['name']+'</opton>';
      });

      $('#condition'+id_cond).find('.products-option-value-select').html(options);

    }
  });

  $('#condition'+id_cond).find('.products-option-autocomplete').click(function() {
      $(this).attr('readonly', false);
      $(this).val('');
  });

  $('#condition'+id_cond).find('.categories-autocomplete').autocomplete({
    'source': function(request, response) {
      $.ajax({
        url: '<?php echo $categoryAutocomplete; ?>&filter_name=' +  encodeURIComponent(request),
        dataType: 'json',
        success: function(json) {

          response($.map(json, function(item) {
            return {
              label: item['name'],
              value: item['category_id']
            }
          }));
        }
      });
    },
    'select': function(item) {
      $(this).val(item['label']);
      $('input[name="conditions['+id_cond+'][category_id]"]').val(item['value']);
      $(this).parent().find('.categories-autocomplete').attr('readonly', true);

    }
  });

  $('#condition'+id_cond).find('.categories-autocomplete').click(function() {
    $(this).attr('readonly', false);
    $(this).val('');
  });

  $('#condition'+id_cond).find('.customer-autocomplete').autocomplete({
    'source': function(request, response) {
      $.ajax({
        url: '<?php echo $customerAutocomplete; ?>&filter_name=' +  encodeURIComponent(request),
        dataType: 'json',
        success: function(json) {
          response($.map(json, function(item) {
            return {
              label: item['name'],
              value: item['customer_id']
            }
          }));
        }
      });
    },
    'select': function(item) {
        $(this).val(item['label']);
        $('input[name="conditions['+id_cond+'][customer_id]"]').val(item['value']);
        $(this).parent().find('.customer-autocomplete').attr('readonly', true);
    }
  });

  $('#condition'+id_cond).find('.customer-autocomplete').click(function() {
    $(this).attr('readonly', false);
    $(this).val('');
  });

  $('#condition'+id_cond).find('.manufacturer-autocomplete').autocomplete({
    'source': function(request, response) {
      $.ajax({
        url: '<?php echo $manufacturerAutocomplete; ?>&filter_name=' +  encodeURIComponent(request),
        dataType: 'json',
        success: function(json) {
          response($.map(json, function(item) {
            return {
              label: item['name'],
              value: item['manufacturer_id']
            }
          }));
        }
      });
    },
    'select': function(item) {
      $(this).val(item['label']);
      $('input[name="conditions['+id_cond+'][manufacturer_id]"]').val(item['value']);
      $(this).parent().find('.manufacturer-autocomplete').attr('readonly', true);
    }
  });

  $('#condition'+id_cond).find('.manufacturer-autocomplete').click(function() {
    $(this).attr('readonly', false);
    $(this).val('');
  });

  $('#condition'+id_cond).find('.product-list-autocomplete').autocomplete({
    'source': function(request, response) {
      $.ajax({
        url: '<?php echo $productAutocomplete; ?>&filter_name=' +  encodeURIComponent(request),
        dataType: 'json',
        success: function(json) {

          response($.map(json, function(item) {
            return {
              label: item['name'],
              value: item['product_id']
            }
          }));
        }
      });
    },
    'select': function(item) {
      $('#autocomplete-product-list'+id_cond+' input').val('');

      $('#product-list-product' + item['value']).remove();

      $('#product-list'+id_cond).append('<div id="product-list-product' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="conditions['+id_cond+'][product_list][]" value="' + item['value'] + '" /></div>');

    }
  });

  $('#product-list'+id_cond).delegate('.fa-minus-circle', 'click', function() {
    $(this).parent().remove();
  });

}

$('#form-promotion').submit(function(e) {
    if($('#input-name').val().length == 0) {
      e.preventDefault();
      $('a[href="#tab-general"]').click();
      $('#input-name').parents('div.form-group').addClass('has-error');
    }
});
</script>
<?php echo $footer; ?>
