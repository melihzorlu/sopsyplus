<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <a href="<?php echo $addnew; ?>" data-toggle="tooltip" title="Yeni Kampanya Ekle" class="btn btn-info"><i class="fa fa-plus"></i>&nbsp;&nbsp;Yeni Kampanya Ekle</a>
        <button type="submit" form="form-promotion" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary save-changes"><i class="fa fa-save"></i>&nbsp;Kaydet</button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">

    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
      <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
      </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i><?php echo $heading_title; ?></h3>

      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-promotion-stats" class="form-horizontal">
			<ul class="nav nav-tabs">
	            <li class="active"><a href="#tab-promos" data-toggle="tab">Kampanyalar</a></li>
	            <?php /* <li><a href="#tab-settings" data-toggle="tab">Ayarlar</a></li> */ ?>
          	</ul>
          <div style="padding:10px 25px;" class="tab-content">
	          	<?php
	            if (!function_exists('modification_vqmod')) {
	              function modification_vqmod($file) {
	                if (class_exists('VQMod')) {
	                  return VQMod::modCheck(modification($file), $file);
	                } else {
	                  return modification($file);
	                }
	              }
	            }
	            ?>
	          <div class="tab-pane" id="isense_support">
	            <?php require_once modification_vqmod(DIR_APPLICATION.'view/template/'.$modulePath.'/tab_support.php'); ?>
	          </div>

	          <div class="tab-pane" id="tab-settings">
	      	    	<div class="row">
	      	    		<div class="form-group">
	      	    			<label class="col-sm-2 control-label" style="text-align: left;"><span data-toggle="tooltip" title="Enable this if you want only admin (logged from the administration) to view and use the promotions.">Admin modu</span></label>
						    <div class="col-sm-3">
						              <select name="<?php echo $moduleName; ?>[development_mode]" id="input-development-mode" class="form-control">
						              	  <option <?php echo (isset($moduleData['development_mode']) && $moduleData['development_mode'] == 'no') ? 'selected="selected"' : '' ?> value="no" >Pasif</option>

							              <option <?php echo (isset($moduleData['development_mode']) && $moduleData['development_mode'] == 'yes') ? 'selected="selected"' : '' ?> value="yes">Aktif</option>
							          </select>
						    </div>
	      	    		</div>
	      	    		<div class="form-group">
	      	    			<label class="col-sm-2 control-label" style="text-align: left;"><span data-toggle="tooltip" title="">Kdv hariç rakamdan hesapla</span></label>
						    <div class="col-sm-3">
						              <select name="<?php echo $moduleName; ?>[discount_to_taxes]" id="input-development-mode" class="form-control">
						              	  <option <?php echo (isset($moduleData['discount_to_taxes']) && $moduleData['discount_to_taxes'] == 'no') ? 'selected="selected"' : '' ?> value="no" >Pasif</option>

							              <option <?php echo (isset($moduleData['discount_to_taxes']) && $moduleData['discount_to_taxes'] == 'yes') ? 'selected="selected"' : '' ?> value="yes">Aktif</option>
							          </select>
						    </div>
	      	    		</div>

				 	</div>
	          </div>

	          <div class="tab-pane active" id="tab-promos">
	          		<table class="table">
						<thead>
							<tr>
								<th>İsim</th>
								<th>Durum</th>
								<th>Misafir Kullanımı</th>
								<th>Üye Kullanımı</th>
								<th>Toplam Kullanım</th>
								<th>Sıralama</th>
								<th>Eylem</th>
							</tr>
						</thead>
						<tbody>
						<?php if(!empty($promotion)) { ?>
						<?php foreach ($promotion as $promo) { ?>
							<tr>
								<td style="width:40%;">
									<?php echo $promo['name']; ?>
								</td>
								<td>
									<label class="switch">
									  <input onChange="changeStatus(this);" <?php echo ($promo['status'] == 'yes') ? 'checked="checked"' : ''; ?> data-module-id="<?php echo $promo['module_id']; ?>" type="checkbox">
									  <div class="slider round"></div>
									</label>
								</td>
								<td>
									<?php echo isset($promo['uses']['guests']) ? $promo['uses']['guests'] : '0'; ?>
								</td>
								<td>
									<?php echo isset($promo['uses']['customers']) ? $promo['uses']['customers'] : '0'; ?>
								</td>
								<td>
									<?php echo isset($promo['uses']['total']) ? $promo['uses']['total'] : '0'; ?>
								</td>
								<td>
									<?php echo isset($promo['general_settings']['sort_order']) ? $promo['general_settings']['sort_order'] : '0'; ?>
								</td>
								<td>
									<a href="<?php echo $promo['link']; ?>" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
									<a onclick="confirm('Are you sure?') ? location.href='<?php echo $promo['delete_link']; ?>' : false;" data-toggle="tooltip" title="" class="btn btn-danger" data-original-title="Delete"><i class="fa fa-trash-o"></i></a>
								</td>
							</tr>
							<?php } ?>
							<?php } else { ?>
								<tr>
									<td  style="text-align: center;font-size:16px; padding-top: 30px; padding-bottom: 0;" colspan="6"> Henüz promosyon eklenmemiş. Yeni bir tane oluşturmak için sağ üst kısımdan Yeni Ekleyin!</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>

					<div class="row">
						<div class="col-sm-12">
							<hr>
							<a href="<?php echo $addnew; ?>" data-toggle="tooltip" title="Yeni Kampanya Ekle" class="btn btn-info"><i class="fa fa-plus"></i>&nbsp;&nbsp;Yeni Kampanya Ekle</a>
						</div>
					</div>

			  </div>

		  </div>

        </form>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
	$(document).ready(function() {

	});

	function changeStatus(element)
	{
		var status;
		if ( $(element).prop('checked')==true){
	        status = 'yes';
	    } else {
	    	status = 'no';
	    }

		var module_id  = $(element).attr('data-module-id');

		 $.ajax({
		    url: '<?php echo $change_status_url; ?>&<?php echo $token; ?>',
		    type: "post",
		    data: {module_id: module_id, status: status},
		    dataType: 'html',
		    success: function (response) {
		    	//SILENCE
		    },
		    error: function(jqXHR, textStatus, errorThrown) {
		       console.log(textStatus, errorThrown);
		    }
		});
	}
</script>



<?php echo $footer; ?>
