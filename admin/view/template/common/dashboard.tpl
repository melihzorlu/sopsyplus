<?php echo $header; ?>

<?php echo $column_left; ?>
<style>
.tile{
  background-color: #FFF;
  color: #19233C;
  border-radius: 20px 20px 0px 0px;
}
.tile-heading {
  padding: 5px 8px;
  text-transform: uppercase;
  background-color: #FFF;
  color: #19233C;
  border-radius: 20px 20px 0px 0px;
}
.tile-body {
  padding: 15px;
  color: #19233C;
  line-height: 48px;
}
.tile .tile-body i {
  font-size: 50px;
  opacity: 1;
  transition: all 1s;
}
.tile-footer {
  padding: 5px 8px;
  background-color: #19233C;
}
</style>
<div id="content">
  <div class="page-header">

  </div>
  <div class="container-fluid">

    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
      <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
        <?php if(isset($order)){ ?>
         <div class=""> <?php echo $order; ?> </div>
        <?php } ?>
      </div>
      <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
        <?php if(isset($sale)){ ?>
         <div class=""> <?php echo $sale; ?> </div>
        <?php } ?>
      </div>
      <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
        <?php if(isset($customer)){ ?>
         <div class=""> <?php echo $customer; ?> </div>
        <?php } ?>
      </div>
    </div>

    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
      <?php if(isset($recent)){ ?>
       <div class=""> <?php echo $recent; ?> </div>
      <?php } ?>
      <?php if(isset($chart)){ ?>
        <div class=""><?php echo $chart; ?></div>
      <?php } ?>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
      <?php if(isset($activity)){ ?>
       <div class=""> <?php echo $activity; ?> </div>
      <?php } ?>
    </div>

</div>

<script>
$('#new-ticket').on('click', function() {
  $('#form-new-ticket').submit();
});

$('#all-ticket').on('click', function() {
  $('#form-all-ticket').submit();
});

</script>
<?php echo $footer; ?>
