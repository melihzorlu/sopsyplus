<style>
#mobileMenu li a svg{
  float: left;
  width: 30px;
}
#mobileMenu li a span{
  float: left;
margin-left: 20px;
}
</style>

<nav id="mobileMenu">
<ul>
  <li>
    <a href="index.php?route=common/dashboard&token=<?php echo $_GET["token"]; ?>">
    <svg width="20" height="22" xmlns="http://www.w3.org/2000/svg"><g stroke="#FFF" stroke-width="2" fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round"><path d="M1 8l9-7 9 7v11a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V8z"></path><path d="M7 21V11h6v10"></path></g></svg>
    <span>Anasayfa</span>
  </a>
  </li>



  <li>
    <a href="index.php?route=sale/order&token=<?php echo $_GET["token"]; ?>">
      <svg width="31" height="26" xmlns="http://www.w3.org/2000/svg"><g stroke="#FFF" stroke-width="2" fill="none" fill-rule="evenodd" stroke-linejoin="round"><path d="M2.458 22.942h-.5V11.588h.5z"/><path d="M2.375 20.765c14.875 4.94 9.917 4.94 26.917-3.53-1.506-1.5-2.695-1.853-4.25-1.411L18.76 17.9" stroke-linecap="round"/><path d="M2.375 13h4.25c3.332 0 5.667 2.118 6.375 2.824h4.25c2.258 0 2.258 2.823 0 2.823H9.458M12.292 5.941a4.242 4.242 0 0 0 4.25 4.235 4.242 4.242 0 0 0 4.25-4.235 4.242 4.242 0 0 0-4.25-4.235 4.242 4.242 0 0 0-4.25 4.235zM16.542 4.53v2.823" stroke-linecap="round"/></g></svg>
      <span>Siparişler</span>
    </a>
  </li>


  <li>
    <a href="index.php?route=catalog/product&token=<?php echo $_GET["token"]; ?>">
    <svg width="22" height="23" xmlns="http://www.w3.org/2000/svg"><path d="M16.458 8.942a1.009 1.009 0 0 1-.02.01L12 11.12v9.548l7.445-3.638a.975.975 0 0 0 .555-.875V7.211l-3.542 1.731zM8.24 3.188l7.76 3.792 2.758-1.348-7.315-3.575a1.02 1.02 0 0 0-.887-.001L8.241 3.188zM6.005 4.28L3.241 5.63l7.76 3.793 2.763-1.351L6.005 4.28zM5.47 2.357c.054-.033.111-.06.17-.083L9.665.306a3.062 3.062 0 0 1 2.67 0l8 3.91A2.927 2.927 0 0 1 22 6.841v9.316c0 1.113-.645 2.13-1.663 2.623l-8 3.91a3.062 3.062 0 0 1-2.684 0L1.65 18.78A2.923 2.923 0 0 1 0 16.147V6.84c0-1.113.646-2.13 1.663-2.624l3.806-1.86zM10 20.674v-9.553L2 7.21v8.942a.974.974 0 0 0 .547.879L10 20.674z" fill="#FFF" fill-rule="nonzero"/></svg>
    <span>Ürünler</span>
  </a>
  </li>

  <li>
    <a href="index.php?route=catalog/information&token=<?php echo $_GET["token"]; ?>">
    <svg width="18px" height="22px" viewBox="0 0 18 22" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <defs></defs>
        <g id="FOX" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
            <g id="FOX-01-Copy-3" transform="translate(-1051.000000, -80.000000)" fill="#FFFFFF" fill-rule="nonzero">
                <g id="Group-8" transform="translate(231.000000, 79.000000)">
                    <g id="Group-6-Copy-2" transform="translate(820.000000, 1.000000)">
                        <path d="M12,3.41421356 L12,6 L14.5857864,6 L12,3.41421356 Z M16,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,2 L3,2 C2.44771525,2 2,2.44771525 2,3 L2,19 C2,19.5522847 2.44771525,20 3,20 L15,20 C15.5522847,20 16,19.5522847 16,19 L16,8 Z M11,0 C11.2652165,0 11.5195704,0.10535684 11.7071068,0.292893219 L17.7071068,6.29289322 C17.8946432,6.4804296 18,6.73478351 18,7 L18,19 C18,20.6568542 16.6568542,22 15,22 L3,22 C1.34314575,22 0,20.6568542 0,19 L0,3 C0,1.34314575 1.34314575,0 3,0 L11,0 Z" id="Combined-Shape" class="active-color"></path>
                    </g>
                </g>
            </g>
        </g>
    </svg>
    <span>Bilgi Sayfaları</span>
  </a>
  </li>

  <li>
    <a href="index.php?route=extension/modules&token=<?php echo $_GET["token"]; ?>">
    <svg width="26px" height="18px" viewBox="0 0 26 18" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <g id="FOL-YENI" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
            <g id="Add-ons-Copy-2" transform="translate(-1025.000000, -21.000000)" fill="#FFFFFF" fill-rule="nonzero" stroke="#FFFFFF" stroke-width="0.5">
                <g id="Group-3" transform="translate(1020.000000, 12.000000)">
                    <path d="M25.4076539,25.9602978 L19.9767055,25.9602978 C19.6572379,25.9602978 19.3777038,25.6823821 19.3777038,25.3647643 L19.3777038,23.3796526 C19.3777038,23.3796526 19.3777038,23.3796526 19.3777038,23.3796526 C19.3777038,22.6253102 18.7387687,22.0297767 17.9800333,22.0297767 C17.2212978,22.0297767 16.5823627,22.6650124 16.5823627,23.4193548 L16.5823627,25.4044665 C16.5823627,25.7220844 16.3028286,26 15.9833611,26 L10.5524126,26 C10.2329451,26 9.95341098,25.7220844 9.95341098,25.4044665 L9.95341098,20.560794 L8.55574043,20.560794 C7.87687188,20.560794 7.23793677,20.2828784 6.718802,19.8064516 C6.27953411,19.2903226 6,18.6550868 6,17.9801489 C6,16.5508685 7.15806988,15.3995037 8.59567388,15.3995037 L9.99334443,15.3995037 L9.99334443,10.5955335 C9.99334443,10.2779156 10.2728785,10 10.5923461,10 L15.9833611,10 C16.3028286,10 16.5823627,10.2779156 16.5823627,10.5955335 L16.5823627,12.5806452 C16.5823627,13.3349876 17.2212978,13.9702233 17.9800333,13.9702233 C18.7387687,13.9702233 19.3777038,13.3349876 19.3777038,12.5806452 L19.3777038,10.5955335 C19.3777038,10.2779156 19.6572379,10 19.9767055,10 L25.4076539,10 C25.7271215,10 26.0066556,10.2779156 26.0066556,10.5955335 L26.0066556,15.3995037 L27.4043261,15.3995037 C28.8419301,15.3995037 30,16.5508685 30,17.9801489 C30,19.4094293 28.8419301,20.560794 27.4043261,20.560794 C27.4043261,20.560794 27.4043261,20.560794 27.4043261,20.560794 L26.046589,20.560794 L26.046589,25.3647643 C26.0066556,25.6823821 25.7271215,25.9602978 25.4076539,25.9602978 Z M20.5757072,24.7692308 L24.8086522,24.7692308 L24.8086522,19.9652605 C24.8086522,19.6476427 25.0881864,19.369727 25.4076539,19.369727 L27.4043261,19.369727 C27.4043261,19.369727 27.4043261,19.369727 27.4043261,19.369727 C28.1630616,19.369727 28.7620632,18.7344913 28.7620632,17.9801489 C28.7620632,17.2258065 28.1231281,16.5905707 27.3643927,16.5905707 L25.3677205,16.5905707 C25.0482529,16.5905707 24.7687188,16.3126551 24.7687188,15.9950372 L24.7687188,11.191067 L20.5757072,11.191067 L20.5757072,12.5806452 C20.5757072,14.0099256 19.4176373,15.1612903 17.9800333,15.1612903 C16.5424293,15.1612903 15.3843594,14.0099256 15.3843594,12.5806452 L15.3843594,11.191067 L11.1514143,11.191067 L11.1514143,15.9950372 C11.1514143,16.3126551 10.8718802,16.5905707 10.5524126,16.5905707 L8.55574043,16.5905707 C7.79700499,16.5905707 7.15806988,17.2258065 7.15806988,17.9801489 C7.15806988,18.7344913 7.79700499,19.369727 8.55574043,19.369727 L10.5524126,19.369727 C10.7121464,19.369727 10.8718802,19.4491315 10.9916805,19.528536 C11.1114809,19.6079404 11.1514143,19.8064516 11.1514143,19.9652605 L11.1514143,24.7692308 L15.3843594,24.7692308 L15.3843594,23.3796526 C15.3843594,21.9503722 16.5424293,20.7990074 17.9800333,20.7990074 C19.4176373,20.7990074 20.5757072,21.9503722 20.5757072,23.3796526 C20.5757072,23.3796526 20.5757072,23.3796526 20.5757072,23.4193548 L20.5757072,24.7692308 Z" id="Shape"></path>
                </g>
            </g>
        </g>
    </svg>
    <span>Eklentiler</span>
  </a>
  </li>

  <li>
    <a href="<?php echo HTTPS_CATALOG; ?>" target="_blank">
    <svg width="28" height="20" xmlns="http://www.w3.org/2000/svg"><g fill="#FFF" fill-rule="nonzero"><path d="M22.48 4.48C17.795-.2 10.205-.2 5.52 4.48L0 10l5.52 5.52c4.685 4.68 12.275 4.68 16.96 0L28 10l-5.52-5.52zm-1.4 9.62a10 10 0 0 1-14.16 0L2.82 10l4.1-4a10 10 0 0 1 14.16 0l4.1 4-4.1 4.1z"/><path d="M14 6a4 4 0 1 0 0 8 4 4 0 0 0 0-8zm0 6a2 2 0 1 1 0-4 2 2 0 0 1 0 4z"/></g></svg>
    <span>Siteyi Görüntüle</span>
  </a>
  </li>
  <li>
    <a href="index.php?route=setting/setting&token=<?php echo $_GET['token']; ?>">
    <svg width="24" height="24" xmlns="http://www.w3.org/2000/svg"><path d="M20.299 8.715A.647.647 0 0 0 20.83 9H21a3 3 0 0 1 0 6h-.086a.65.65 0 0 0-.595.394.65.65 0 0 0 .118.719l.06.06a3 3 0 1 1-4.244 4.244l-.052-.052a.654.654 0 0 0-.727-.126.649.649 0 0 0-.394.591V21a3 3 0 0 1-6 0 .698.698 0 0 0-.484-.685.647.647 0 0 0-.709.122l-.06.06a3 3 0 1 1-4.244-4.244l.052-.052a.654.654 0 0 0 .126-.727.649.649 0 0 0-.591-.394H3a3 3 0 0 1 0-6 .698.698 0 0 0 .685-.484.647.647 0 0 0-.122-.709l-.06-.06a3 3 0 1 1 4.244-4.244l.052.052a.65.65 0 0 0 .717.13 1 1 0 0 1 .2-.064A.647.647 0 0 0 9 3.17V3a3 3 0 0 1 6 0v.086c.001.26.156.493.404.6a.647.647 0 0 0 .709-.123l.06-.06a3 3 0 1 1 4.244 4.244l-.052.052a.65.65 0 0 0-.13.717 1 1 0 0 1 .064.2zM9.129 5.672a2.655 2.655 0 0 1-2.736-.615l-.06-.06a1 1 0 1 0-1.416 1.416l.068.068c.757.774.967 1.932.554 2.864A2.657 2.657 0 0 1 3.09 11.08H3a1 1 0 0 0 0 2h.174a2.646 2.646 0 0 1 2.42 1.596 2.654 2.654 0 0 1-.537 2.931l-.06.06a1 1 0 1 0 1.416 1.416l.068-.068c.774-.757 1.932-.967 2.864-.554a2.657 2.657 0 0 1 1.735 2.449V21a1 1 0 0 0 2 0v-.174a2.646 2.646 0 0 1 1.596-2.42 2.654 2.654 0 0 1 2.931.537l.06.06a1 1 0 1 0 1.416-1.416l-.068-.068a2.65 2.65 0 0 1-.53-2.923A2.648 2.648 0 0 1 20.91 13H21a1 1 0 0 0 0-2h-.174a2.65 2.65 0 0 1-2.425-1.606 1 1 0 0 1-.073-.264 2.655 2.655 0 0 1 .615-2.737l.06-.06a1 1 0 1 0-1.416-1.416l-.068.068a2.646 2.646 0 0 1-2.913.534A2.651 2.651 0 0 1 13 3.09V3a1 1 0 0 0-2 0v.174a2.65 2.65 0 0 1-1.606 2.425 1 1 0 0 1-.264.073zM12 16a4 4 0 1 1 0-8 4 4 0 0 1 0 8zm0-2a2 2 0 1 0 0-4 2 2 0 0 0 0 4z" fill="#FFF" fill-rule="nonzero"></path></svg>
    <span>Ayarlar</span>
  </a>
  </li>

  <li>
    <a href="index.php?route=common/logout&token=<?php echo $_GET["token"]; ?>">
    <svg width="26" height="27" xmlns="http://www.w3.org/2000/svg"><path d="M23.025 1.33c1.083 0 1.975.894 1.975 1.979V23.69a1.987 1.987 0 0 1-1.975 1.98h-3.95a.658.658 0 0 1-.577-.993c.12-.205.34-.33.578-.327h3.95a.644.644 0 0 0 .658-.66V3.31a.644.644 0 0 0-.659-.66h-3.95a.658.658 0 0 1-.577-.992c.12-.206.34-.33.578-.327h3.95zm-11.2 4.904a.658.658 0 0 0-.453 1.103l4.937 5.503H2.018a.657.657 0 0 0-.617.357.661.661 0 0 0 .617.963h14.29l-4.936 5.493a.662.662 0 0 0 .058.93.658.658 0 0 0 .93-.054l5.923-6.596a.66.66 0 0 0 0-.876L12.36 6.46a.658.658 0 0 0-.534-.227z" fill="#FFF" fill-rule="nonzero" stroke="#FFF"></path></svg>
    <span>Çıkış</span>
    </a>
  </li>
    <?php /* foreach ($menus as $menu) { ?>

    <li id="<?php echo $menu['id']; ?>">
      <?php if ($menu['href']) { ?>
      <a href="<?php echo $menu['href']; ?>"><i class="fa <?php echo $menu['icon']; ?> fw"></i> <span><?php echo $menu['name']; ?></span></a>
      <?php } else { ?>
      <a class="parent"><i class="fa <?php echo $menu['icon']; ?> fw"></i> <span><?php echo $menu['name']; ?></span></a>
      <?php } ?>
      <?php if ($menu['children']) { ?>
      <ul>
        <?php foreach ($menu['children'] as $children_1) { ?>
        <li>
          <?php if ($children_1['href']) { ?>

          <a href="<?php echo $children_1['href']; ?>"> <?php echo $children_1['name']; ?></a>
          <?php } else { ?>
          <a class="parent"><?php echo $children_1['name']; ?></a>
          <?php } ?>
          <?php if ($children_1['children']) { ?>
          <ul aria-expanded="false" class="collapse" style="height: 0px;">
            <?php foreach ($children_1['children'] as $children_2) { ?>
            <li>
              <?php if ($children_2['href']) { ?>
              <a href="<?php echo $children_2['href']; ?>"><?php echo $children_2['name']; ?></a>
              <?php } else { ?>
              <a class="parent"><?php echo $children_2['name']; ?></a>
              <?php } ?>
              <?php if ($children_2['children']) { ?>
              <ul>
                <?php foreach ($children_2['children'] as $children_3) { ?>
                <li><a href="<?php echo $children_3['href']; ?>"><?php echo $children_3['name']; ?></a></li>
                <?php } ?>
              </ul>
              <?php } ?>
            </li>
            <?php } ?>
          </ul>
          <?php } ?>
        </li>
        <?php } ?>
      </ul>
      <?php } ?>
    </li>
  <?php }*/ ?>
  </ul>
</nav>
<nav id="column-left">

  <!--BundleExpert-->
  <script type="text/javascript">
    $(document).ready(function () {

      var element = $(".be-cache-update-menu-button").first();

      $(element).removeClass("hidden");
      $(element).hide();

      if($(element).length>0){
        var menu_element = $(element).closest('a')
        $(menu_element).removeAttr("href");
        $(menu_element).attr("onclick", "be_update_cache();");

        var spin_html = "<i class='fa fa-spinner fa-spin' style='margin-left: 4px;vertical-align: middle;'></i>";
        spin_html += "<i class='fa fa-check-circle' style='margin-left: 4px;vertical-align: middle;'></i>";
        spin_html += "<span class='be-cache-update-progress' style='font-size: 10px;margin-left: 2px;vertical-align: middle;'></span>";//+++
        $(element).html(spin_html);
      }

    });

    function be_update_cache() {
      var element = $(".be-cache-update-menu-button").first();

      $.ajax({
        url: 'index.php?route=catalog/bundle_expert/updateCacheWithProgress&<?php echo $be_token_name; ?><?php echo $be_token_value; ?>',
        dataType: 'json',
        beforeSend: function() {
          $('.alert-container').html('');
          $(element).show();
          $(element).find('.fa-spinner').show();
          $(element).find('.fa-check-circle').hide();
          $(element).find('.be-cache-update-progress').show();//+++
          $(element).find('.be-cache-update-progress').html('0%');//+++
        },
        complete: function() {

        },
        success: function(json) {
          if (json['success']) {
            //+++
            $(element).find('.fa-spinner').hide();
            $(element).find('.fa-check-circle').show();
            $(element).find('.be-cache-update-progress').hide();//+++
            //+++

            $('.alert-container').append('<div class="alert alert-success"><i class="fa fa-exclamation-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');

          }

          //+++
          if (json['total']) {
            $('.be-cache-update-progress').html(json['total'] + '%');
          }
          if (json['next']) {
            be_update_cache_next(json['next']);
          }
          //+++
        },
        error: function(xhr, ajaxOptions, thrownError) {
          alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
      });
    }

    function be_update_cache_next(url) {
      var element = $(".be-cache-update-menu-button").first();

      $.ajax({
        url: url,
        dataType: 'json',
        success: function(json) {
          $('.alert-dismissible').remove();

          if (json['error']) {
            $('#content > .container-fluid').prepend('<div class="alert alert-danger alert-dismissible"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
          }

          if (json['success']) {
            $(element).find('.fa-spinner').hide();
            $(element).find('.fa-check-circle').show();
            $(element).find('.be-cache-update-progress').hide();//+++
          }

          if (json['total']) {
            $('.be-cache-update-progress').html(json['total'] + '%');
          }

          if (json['next']) {
            be_update_cache_next(json['next']);
          }
        },
        error: function(xhr, ajaxOptions, thrownError) {
          alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
      });
    }
  </script>
  <!--BundleExpert-->

  <ul id="menu">
    <?php foreach ($menus as $menu) { ?>
    <li id="<?php echo $menu['id']; ?>">
      <?php if ($menu['href']) { ?>
      <a href="<?php echo $menu['href']; ?>"><i class="fa <?php echo $menu['icon']; ?> fw"></i> <span><?php echo $menu['name']; ?></span></a>
      <?php } else { ?>
      <a class="parent"><i class="fa <?php echo $menu['icon']; ?> fw"></i> <span><?php echo $menu['name']; ?></span></a>
      <?php } ?>
      <?php if ($menu['children']) { ?>
      <ul>
        <?php foreach ($menu['children'] as $children_1) { ?>
        <li>
          <?php if ($children_1['href']) { ?>

          <a href="<?php echo $children_1['href']; ?>"> <?php echo $children_1['name']; ?></a>
          <?php } else { ?>
          <a class="parent"><?php echo $children_1['name']; ?></a>
          <?php } ?>
          <?php if ($children_1['children']) { ?>
          <ul aria-expanded="false" class="collapse" style="height: 0px;">
            <?php foreach ($children_1['children'] as $children_2) { ?>
            <li>
              <?php if ($children_2['href']) { ?>
              <a href="<?php echo $children_2['href']; ?>"><?php echo $children_2['name']; ?></a>
              <?php } else { ?>
              <a class="parent"><?php echo $children_2['name']; ?></a>
              <?php } ?>
              <?php if ($children_2['children']) { ?>
              <ul>
                <?php foreach ($children_2['children'] as $children_3) { ?>
                <li><a href="<?php echo $children_3['href']; ?>"><?php echo $children_3['name']; ?></a></li>
                <?php } ?>
              </ul>
              <?php } ?>
            </li>
            <?php } ?>
          </ul>
          <?php } ?>
        </li>
        <?php } ?>
      </ul>
      <?php } ?>
    </li>
    <?php } ?>
  </ul>
</nav>
<style>
  #menu > #menu-report > ul {
    left: -70px !important;
  }
  @media all ( max-width: 767px ) {
    #column-left {
      display: none;
    }
  }
  .mobileMenuButton {
    list-style: none;
    color: white;
    font-size: 24px;
    float: left;
    padding: 9px 9px;
  }
  .mobileMenuButton a {
    color: white;
  }
  @media all ( min-width: 768px ) {
    #mobileMenu {
      display: none;
    }
    .mobileMenuButton {
      display: none;
    }
  }
</style>
