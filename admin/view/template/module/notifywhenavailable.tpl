<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
        <div class="container-fluid">
          <h1><?php echo $heading_title; ?></h1>
          <ul class="breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } ?>
          </ul>
        </div>
  </div> 
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
		<div class="alert alert-danger autoSlideUp"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
	<?php } ?>
    <?php if (isset($preorder) && isset($preorder['preorder']) && $preorder['preorder']['Enabled'] == 'yes') { ?>
		<div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> <?php echo $preorder_enabled; ?></div>
	<?php } ?>
	<?php if ($success) { ?>
		<div class="alert alert-success autoSlideUp"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<script>$('.autoSlideUp').delay(3000).fadeOut(600, function(){ $(this).show().css({'visibility':'hidden'}); }).slideUp(600);</script>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
            <div class="storeSwitcherWidget">
                <div class="form-group">
                           <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown"><?php echo $store['name']; if($store['store_id'] == 0) echo ' <strong>'.$text_default.'</strong>'; ?>&nbsp;<span class="caret"></span><span class="sr-only">
Açılır Pencereyi Değiştir</span></button>
                            <ul class="dropdown-menu" role="menu">
                                <?php foreach ($stores as $st) { ?> <li> <a href="index.php?route=" . $modulePath . "&store_id=<?php echo $st['store_id'];?>&<?php echo $tokenString ?>=<?php echo $token; ?>"><?php echo $st['name']; ?></a></li><?php } ?> 
                            </ul>
                </div>
            </div>
            <h3 class="panel-title"><i class="fa fa-list"></i>&nbsp;<span style="vertical-align:middle;font-weight:bold;">Modül ayarları</span></h3>
        </div>
        <div class="panel-body">
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form"> 
                <input type="hidden" name="notifywhenavailable_status" value="1" />
                <input type="hidden" name="store_id" value="<?php echo $store['store_id']; ?>" />
                <div class="tabbable">
                    <div class="tab-navigation form-inline">
                        <ul class="nav nav-tabs mainMenuTabs" id="mainTabs">
                            <li class="active"><a href="#controlpanel" data-toggle="tab"><i class="fa fa-power-off"></i>&nbsp;Kontrol Paneli</a></li>
                            <li><a href="#orders" data-toggle="tab"><i class="fa fa-bell"></i>&nbsp;Bekleme Listesi</a></li>
                            <li><a href="#archive" data-toggle="tab"><i class="fa fa-database"></i>&nbsp;Arşiv</a></li>
                            <li><a href="#statistics" data-toggle="tab"><i class="fa fa-bar-chart"></i>&nbsp;İstatistikler</a></li>
                            <li><a href="#settings" data-toggle="tab"><i class="fa fa-cog"></i>&nbsp;Ayarlar</a></li>
                        </ul>
                        <div class="tab-buttons">
                            <button type="submit" class="btn btn-success save-changes"><i class="fa fa-check"></i>&nbsp;Değişiklikleri Kaydet</button>
                            <a onclick="location = '<?php echo $cancel; ?>'" class="btn btn-warning"><i class="fa fa-times"></i>&nbsp;<?php echo $button_cancel?></a>
                        </div> 
                    </div><!-- /.tab-navigation --> 
                    <div class="tab-content">
                        <?php
                        if (!function_exists('modification_vqmod')) {
                            function modification_vqmod($file) {
                                if (class_exists('VQMod')) {
                                    return VQMod::modCheck(modification($file), $file);
                                } else {
                                    return modification($file);
                                }
                            }
                        }
                        ?>
                        <div id="orders" class="tab-pane">
                            <?php require_once modification_vqmod(DIR_APPLICATION.'view/template/' . $modulePath . '/tab_viewcustomers.php'); ?>                        </div>
                        <div id="controlpanel" class="tab-pane active">
                            <?php require_once modification_vqmod(DIR_APPLICATION.'view/template/' . $modulePath . '/tab_controlpanel.php'); ?>                        </div>
                        <div id="settings" class="tab-pane">
                            <?php require_once modification_vqmod(DIR_APPLICATION.'view/template/' . $modulePath . '/tab_settings.php'); ?>                        </div>
                        <div id="statistics" class="tab-pane">
                             <?php require_once modification_vqmod(DIR_APPLICATION.'view/template/' . $modulePath . '/tab_chart.php'); ?>                    
                        </div>
                        <div id="archive" class="tab-pane">
                            <?php require_once modification_vqmod(DIR_APPLICATION.'view/template/' . $modulePath . '/tab_archive.php'); ?>                        </div>           
                        <div id="isense-support" class="tab-pane">
                            <?php require_once modification_vqmod(DIR_APPLICATION.'view/template/' . $modulePath . '/tab_support.php'); ?>                        </div>
                    </div><!-- /.tab-content -->
                </div><!-- /.tabbable -->
            </form>
        </div>
    </div>
  </div>
</div>
<script>
if (window.localStorage && window.localStorage['currentTab']) {
  $('.mainMenuTabs a[href='+window.localStorage['currentTab']+']').trigger('click');  
}
$('.fadeInOnLoad').css('visibility','visible');
$('.mainMenuTabs a[data-toggle="tab"]').click(function() {
  if (window.localStorage) {
    window.localStorage['currentTab'] = $(this).attr('href');
  }
});
if (typeof drawChart == 'function') { 
    google.setOnLoadCallback(drawChart);
}
$('a[href=#statistics]').on('click', function() {
	if (typeof drawChart == 'function') { 
		setTimeout(function() { drawChart(); }, 250);
	}
});
</script>
<?php echo $footer; ?>