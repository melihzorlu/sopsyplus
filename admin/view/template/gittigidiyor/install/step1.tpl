<?php echo $header; ?><?php echo $column_left; ?>

<div id="content">
  <form method="post" id="step1form">

  <div class="container-fluid" id="content">
  <div class="col-md-5 col-centered">

  <div class="panel panel-default" style="margin-top:40px;">
        <div class="panel-heading" style="font-weight: bold; font-size: 14px;"><?php echo $heading_title; ?></div>
        <div class="panel-body install-panel">
            <div class="row">
                <div class="form-group">
                  <label>MAĞAZANIZ İÇİN BİR AD BELİRLEYİN</label>
                  <p style="font-size: 12px !important;" class="text-danger">Bu adım sistem işleyişi konusunda bir önem teşkil etmez sadece sizin mağazanızı tanıyabilmeniz için gereken bir alandır ve boş bırakılamaz</p>
                  <input type="text" class="form-control input-sm" name="name">
                </div>

                <div class="form-group">
                  <label>GG APİ KEY</label>
                  <p style="font-size: 12px  !important;" class="text-danger" >Gg Api Keyiniz api servisinize bağlanmanız için gereki bir bilgidir ve zorunludur. Api keyinizi dev.gittigidiyor.com adresinden temin edebilirsiniz</p>
                  <input type="text" class="form-control input-sm" name="apikey">
                </div>

                <div class="form-group">
                  <label>GG SECRET KEY</label>
                  <p style="font-size: 12px  !important;" class="text-danger">
                    Gg Secret key tıpkı api key gibi api servisinize bağlanmanız için gerekli ve zorunlu bir alandır. Api secret bilginizi de key ile birlikte alabilirsiniz.
                  </p>
                  <input type="text" class="form-control input-sm" name="secretkey">
                </div>
                <div class="form-group">
                  <label>ROLE NAME</label>
                  <p style="font-size: 12px  !important;" class="text-danger">
                    Role name web servise bağlanabilmeniz için gerekli olan kullanıcı adıdır.
                  </p>
                  <input type="text" class="form-control input-sm" name="auth_user">
                </div>
                <div class="form-group">
                  <label>ROLE PASSWORD</label>
                  <p style="font-size: 12px  !important;" class="text-danger">
                    Role password web servise bağlanabilmeniz için gerekli olan kullanıcı şifredir.
                  </p>
                  <input type="text" class="form-control input-sm" name="auth_pass">
                </div>
                <div class="form-group devamet">
                    <button type="button" class="btn btn-sm btn-info pull-right checkkey" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Mağaza Bilgileri Kontrol Ediliyor">MAĞAZAYI KONTROL ET</button>

                </div>
          </div>
        </div>
  </div>
</div>
</div>
</div>
<?php echo $footer; ?>
<script type="text/javascript">
  $(document).on('click', '.checkkey', function(){
      $('.panel-body .alert').remove();
       $('.checkkey').button('loading');
       $.ajax({
          url: 'index.php?route=gittigidiyor/install/checkkey&<?php echo $token_link; ?>',
          type: 'post',
          dataType: 'json',
          data : $('#step1form').serialize(),
          success: function(json) {
            setTimeout(function() {
                if(json.status == 1){
                    $('.devamet').before('<div class="alert alert-success">'+json.msg+'</div>');
                    $('.devamet').html(json.btn);
                } else {
                   $('.devamet').before('<div class="alert alert-danger">'+json.msg+'</div>');
                   $('.checkkey').button('reset');
                }
            }, 1000);
          }
        });
  });
</script>
