<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <form method="post" id="step1form">
  <input type="hidden" name="shop_id" value="<?php echo $shop_id; ?>">
  <div class="container-fluid" id="content">
  <div class="col-md-12 col-centered">

  <div class="panel panel-default"  style="margin-top:40px;">
        <div class="panel-heading" style="font-weight: bold; font-size: 14px;"><?php echo $heading_title; ?></div>
        <div class="panel-body install-panel">
            <input type="hidden" name="iaction" value="<?php echo $action; ?>">
            <div class="row">
            <?php foreach($ggcat as $ggct){ ?>
              <div class="col-sm-4">
                <div class="togselect" style="margin:3px;  border:1px solid #ededed; padding: 0px 5px; border-radius: 3px;">
                  <input type="checkbox" data-value="<?php echo $ggct->categoryCode; ?>" type="checkbox" data-toggle="toggle" data-size="mini"<?php if(isset($gg_kategoriler)){if(in_array($ggct->categoryCode, $gg_kategoriler)){echo 'checked';}}?> id="<?php echo $ggct->categoryCode; ?>" value="<?php echo $ggct->categoryCode; ?>" name="gg_kategori[]" />
                  <label for="<?php echo $ggct->categoryCode; ?>" style="border-left: 1px solid #ededed; margin-bottom: 0px; margin-left: 5px; padding: 5px 5px 6px 5px; font-family: Arial;"><?php echo $ggct->categoryName; ?></label>
                </div>
              </div>
            <?php } ?>
             <div class="form-group devamet">
                    <?php if($action == 'edit'){ ?>
                    <button type="submit" class="btn btn-sm btn-primary pull-right" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Ayarlar Kaydediliyor">AYARLARI KAYDET</button> <a href="<?php echo $geri_don; ?>" class="btn btn-sm btn-danger pull-right"><i class="fa fa-reply" aria-hidden="true"></i> GERİ DÖN</a>
                    <?php } else { ?>
                    <button type="submit" class="btn btn-sm btn-info pull-right" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Sonraki Adıma Geçiliyor">SONRAKİ ADIMA GEÇ</button>
                    <?php } ?>
                </div>
            </div>
        </div>
  </div>
</div>
</div>
</div>
<script type="text/javascript" src="view/javascript/summernote/summernote.js"></script>
<link href="view/javascript/summernote/summernote.css" rel="stylesheet" />
<script type="text/javascript">
    $('#aciklama').summernote({height: 300});
</script>
<?php echo $footer; ?>
