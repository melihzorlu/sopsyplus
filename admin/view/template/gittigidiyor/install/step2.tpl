<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <form method="post" id="step1form">
  <input type="hidden" name="shop_id" value="<?php echo $shop_id; ?>">
  <div class="container-fluid" id="content">
  <div class="col-md-5 col-centered">

  <div class="panel panel-default"  style="margin-top:40px;">
        <div class="panel-heading" style="font-weight: bold; font-size: 14px;"><?php echo $heading_title; ?></div>
        <div class="panel-body install-panel">
            <input type="hidden" name="iaction" value="<?php echo $action; ?>">
            <div class="row">
                <div class="form-group">
                  <label>KARGO ÇIKIŞI YAPILACAK ŞEHİR</label>
                  <p style="font-size: 12px  !important;" class="text-danger">
                      Kargolarınızın çıkışının yapılacağı şehri seçiniz, eğer birden fazla şehirden gönderim yapıyorsanız daha sonra ürün veya kategori bazlı seçim yapabiliriniz.
                  </p>
                  <select name="city" class="form-control input-sm">
                      <?php foreach ($sehirler as $key => $sehir) { ?>
                          <option value="<?php echo $sehir->trCode; ?>" <?php if($shop['city'] == $sehir->trCode){ echo 'selected'; } ?>><?php echo $sehir->cityName; ?></option>
                      <?php } ?>
                  </select>
                </div>
                <div class="form-group">
                  <label>KARGO FİRMANIZI SEÇİN</label>
                  <p style="font-size: 12px  !important;" class="text-danger">
                      Ürünleri hangi kargo şirketi ile gönderileceğini seçin.
                  </p>
                  <select name="cargo_company" class="form-control input-sm">
                    <option value="aras" <?php if($shop['cargo_company'] == 'aras'){ echo 'selected'; } ?>>Aras Kargo</option>
                    <option value="borusan" <?php if($shop['cargo_company'] == 'borusan'){ echo 'selected'; } ?>>Borusan Lojistik</option>
                    <option value="ceva" <?php if($shop['cargo_company'] == 'ceva'){ echo 'selected'; } ?>>Ceva Lojistik</option>
                    <option value="dhl" <?php if($shop['cargo_company'] == 'dhl'){ echo 'selected'; } ?>>DHL</option>
                    <option value="other" <?php if($shop['cargo_company'] == 'other'){ echo 'selected'; } ?>>Diğer</option>
                    <option value="fedex" <?php if($shop['cargo_company'] == 'fedex'){ echo 'selected'; } ?>>FedEx</option>
                    <option value="fillo" <?php if($shop['cargo_company'] == 'fillo'){ echo 'selected'; } ?>>Fillo Lojistik</option>
                    <option value="horoz" <?php if($shop['cargo_company'] == 'horoz'){ echo 'selected'; } ?>>Horoz Lojistik</option>
                    <option value="mng" <?php if($shop['cargo_company'] == 'mng'){ echo 'selected'; } ?>>Mng Kargo</option>
                    <option value="narpost" <?php if($shop['cargo_company'] == 'narpost'){ echo 'selected'; } ?>>Narpost Kargo</option>
                    <option value="ptt" <?php if($shop['cargo_company'] == 'ptt'){ echo 'selected'; } ?>>PTT Kargo</option>
                    <option value="surat" <?php if($shop['cargo_company'] == 'surat'){ echo 'selected'; } ?>>Sürat Kargo</option>
                    <option value="tnt" <?php if($shop['cargo_company'] == 'tnt'){ echo 'selected'; } ?>>TNT</option>
                    <option value="tezel" <?php if($shop['cargo_company'] == 'tezel'){ echo 'selected'; } ?>>Tezel Lojistik</option>
                    <option value="ups" <?php if($shop['cargo_company'] == 'ups'){ echo 'selected'; } ?>>UPS</option>
                    <option value="varan" <?php if($shop['cargo_company'] == 'varan'){ echo 'selected'; } ?>>Varan Kargo</option>
                    <option value="yurtici" <?php if($shop['cargo_company'] == 'yurtici'){ echo 'selected'; } ?>>Yurtiçi Kargo</option>
                  </select>
                </div>
                <div class="form-group">
                  <label>ŞEHİR İÇİ KARGO FİYATI</label>
                  <p style="font-size: 12px  !important;" class="text-danger">
                      Şehir içi kargo için fiyat girin, alıcı ödemeli ve alıcı sepette öder kargo seçeneklerinde bu fiyat zorunludur.
                  </p>
                  <input type="text" name="ship_cityprice" value="<?php echo $shop['ship_cityprice']; ?>" class="form-control input-sm">
                </div>
                <div class="form-group">
                  <label>TÜM TÜRKİYE KARGO FİYATI</label>
                  <p style="font-size: 12px  !important;" class="text-danger">
                      Şehir içi kargo için fiyat girin, alıcı ödemeli ve alıcı sepette öder kargo seçeneklerinde bu fiyat zorunludur. Ayrıca bu fiyat ücretsiz kargo - fiyata dahil seçeneğinde ürün fiyatına eklenir.
                  </p>
                  <input type="text" name="ship_countryprice" value="<?php echo $shop['ship_countryprice']; ?>" class="form-control input-sm">
                </div>
                <div class="form-group">
                  <label>SATIŞ SÜRESİ (360 Önerilir)</label>
                  <p style="font-size: 12px  !important;" class="text-danger">
                      Ürünün kaç gün satışta gösterileceğini belirler süre sonunda tekrar satışa açmanız gerekir. Maksimimum 360 girilebilmektedir. Girebileceğiniz Günler : 10 - 30 - 60 - 180 - 360
                  </p>
                  <input type="text" name="ship_listingday" value="<?php echo $shop['ship_listingday']; ?>" class="form-control input-sm">
                </div>
                <div class="form-group">
                  <label>KARGOYA VERME SÜRESİ</label>
                  <p style="font-size: 12px  !important;" class="text-danger">Kargoya verme süresini belirtilen formatta yazınız. (Örn : 1day - 2-3days - 2days vs)</p>
                  <input type="text" class="form-control input-sm" value="<?php echo $shop['shipping_days']; ?>" name="shipping_days">
                </div>
                <div class="form-group">
                  <label>ÜCRETSİZ KARGO KOŞULU</label>
                  <p style="font-size: 12px  !important;" class="text-danger">Ürünlerinizi belli bir koşula göre ücretsiz olarak gönderecekseniz aşağıdan bu koşulu seçin.</p>
                  <select name="shipping_condition" class="form-control input-sm">
                        <option value="0" <?php if($shop['shipping_condition'] == '0'){ echo 'selected'; } ?>>-- İşlem Yapma --</option>
                        <option value="1" <?php if($shop['shipping_condition'] == '1'){ echo 'selected'; } ?>>X TL Üstü Ücretsiz</option>
                        <option value="2" <?php if($shop['shipping_condition'] == '2'){ echo 'selected'; } ?>>Kargo Gereksiz Seçili Ürünler</option>
                        <option value="3" <?php if($shop['shipping_condition'] == '3'){ echo 'selected'; } ?>>Fiyata Dahil (Kargoyu Fiyata Ekle Tüm Ürünleri Ücretsiz Yap)</option>
                  </select>
                </div>
                <div class="form-group">
                  <label>KAÇ TL ÜSTÜ ÜCRETİSİZ KARGO</label>
                  <p style="font-size: 12px  !important;" class="text-danger">Eğer ücretsiz kargo koşulunuz x tl üzeri ücretsiz ise kaç tl üstünün ücretsiz olacağını bu alana yazınız</p>
                  <input type="text" class="form-control input-sm" value="<?php echo  $shop['shipping_price']; ?>" name="shipping_price">
                </div>
                <div class="form-group">
                  <label>FİYAT DEĞİŞİM TİPİ</label>
                  <p style="font-size: 12px  !important;" class="text-danger">Sitenizdeki fiyat üzerinden gg'ye ürün gönderirken fiyatta oynama yapmak isterseniz bu alanı doldurun. Aynı fiyattan gidecekse işlem yapma olarak kalsın.</p>
                  <select name="difference_type" id="difference_type" class="form-control input-sm">
                      <option value="0" <?php if($shop['difference_type'] == '0'){ echo 'selected'; } ?>>--- İşlem Yapma ---</option>
                        <option value="yuzde" <?php if($shop['difference_type'] == 'yuzde'){ echo 'selected'; } ?>>% Yüzde Arttır</option>
                        <option value="sabit" <?php if($shop['difference_type'] == 'sabit'){ echo 'selected'; } ?>>Sabit Tutar Olarak Arttır</option>
                        <option value="yuzdeazalt" <?php if($shop['difference_type'] == 'yuzdeazalt'){ echo 'selected'; } ?>>% Yüzde Azalt</option>
                        <option value="sabitazalt" <?php if($shop['difference_type'] == 'sabitazalt'){ echo 'selected'; } ?>>Sabit Tutar Olarak Azalt</option>
                  </select>
                </div>
                <div class="form-group">
                  <label>FİYAT DEĞİŞİM MİKTARI (YÜZDE / SABİT)</label>
                  <p style="font-size: 12px  !important;" class="text-danger">Fiyat değişim tipinde seçtiği alana göre bu alanı şekillendirin, buraya 10 yazarsanız ve fiyat değişim tipi yüzde olarak arttır ise %10 arttırır, sabit olarak arttır ise 10 TL ekler.</p>
                  <input type="text" class="form-control input-sm" value="<?php echo $shop['difference_value']; ?>" name="difference_value">
                </div>
                <div class="form-group">
                    <label>MAĞAZA PARA BİRİMİ</label>
                    <p style="font-size: 12px  !important;" class="text-danger">gittigidiyor yabancı kurda fiyat kabul etmez, siteniz eur ve usd kurunda satış yapıyorsa seçin, güncel kurdan ürününüz dönüştürülerek gönderilir.</p>
                    <select name="currency" id="currency" class="form-control input-sm">
                        <option value="1" <?php if($shop['currency'] == '1'){ echo 'selected'; } ?>>TRY</option>
                        <option value="2" <?php if($shop['currency'] == '2'){ echo 'selected'; } ?>>USD</option>
                        <option value="3" <?php if($shop['currency'] == '3'){ echo 'selected'; } ?>>EUR</option>
                    </select>
                </div>
                <div class="form-group">
                  <label>KUR DÖNÜŞTÜRME METODU</label>
                  <p style="font-size: 12px  !important;" class="text-danger">Sitenizde eur veya usd satış yapıyorsanız kur dönüştürülken hangi yöntemle kur dönüştürme yapılacağını seçin, merkez bankası önerilir. Eğer tl ile satış yapıyorsanız, bu alan önemsizdir.</p>
                  <select name="currency_calctype" id="currency_calctype" class="form-control input-sm">
                        <option value="1" <?php if($shop['currency_calctype'] == '1'){ echo 'selected'; } ?>>Otomatik, Merkez Bankası</option>
                        <option value="2" <?php if($shop['currency_calctype'] == '2'){ echo 'selected'; } ?>>Sitemdeki Kur Değeri</option>
                  </select>
                </div>
                <div class="form-group">
                    <label>FİYAT KDV SEÇENEĞİ</label>
                    <p style="font-size: 12px  !important;" class="text-danger">Ürünleriniz gg'ye gönderilirken kdv dahil olarak mı, kdv hariç olarak mı gönderilsin istersiniz ? Eğer kdv seçili değilse bu alan çalışmayacaktır.</p>
                    <select name="tax_option" id="tax_option" class="form-control input-sm">
                        <option value="1" <?php if($shop['tax_option'] == '1'){ echo 'selected'; } ?>>KDV Dahil</option>
                        <option value="2" <?php if($shop['tax_option'] == '2'){ echo 'selected'; } ?>>KDV Hariç</option>
                    </select>
                </div>
                <div class="form-group">
                  <label>VARSAYILAN AÇIKLAMA BİÇİMİ</label>
                  <p style="font-size: 12px  !important;" class="text-danger">
                      Bu alan ürünlerinizi gittigidiyor.com a gönderirken ürün açıklamasına eklemeler yapmak isterseniz kullanılır. {aciklama} parametresi ürün açıklamanızdır bunu silerseniz ürün eklerken girdiğiniz ürün açıklaması gittigidiyor.com'a gitmez. Bu parametreyi kullanarak açıklamanıza yeni alanlar ekleyebilirsiniz.
                  </p>
                  <textarea class="form-control" id="aciklama" name="description" required="required"><?php echo $shop['description']; ?></textarea>
                </div>

                <div class="form-group devamet">
                    <?php if($action == 'edit'){ ?>
                    <button type="submit" class="btn btn-sm btn-primary pull-right" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Ayarlar Kaydediliyor">AYARLARI KAYDET</button> <a href="<?php echo $geri_don; ?>" class="btn btn-sm btn-danger pull-right"><i class="fa fa-reply" aria-hidden="true"></i> GERİ DÖN</a>
                    <?php } else { ?>
                    <button type="submit" class="btn btn-sm btn-info pull-right" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Sonraki Adıma Geçiliyor">SONRAKİ ADIMA GEÇ</button>
                    <?php } ?>
                </div>
          </div>
        </div>
  </div>
</div>
</div>
</div>
<script type="text/javascript" src="view/javascript/summernote/summernote.js"></script>
<link href="view/javascript/summernote/summernote.css" rel="stylesheet" />
<script type="text/javascript">
    $('#aciklama').summernote({height: 300});
</script>
<?php echo $footer; ?>
