<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <form method="post" id="step1form">
  <input type="hidden" name="shop_id" value="<?php echo $shop_id; ?>">
  <div class="container-fluid" id="content">
  <div class="col-md-12 col-centered">

  <div class="panel panel-default"  style="margin-top:40px;">
        <div class="panel-heading" style="font-weight: bold; font-size: 14px;"><?php echo $heading_title; ?></div>
        <div class="panel-body install-panel">
            <input type="hidden" name="iaction" value="<?php echo $action; ?>">
            <div class="row">
                <center class="loadingdownload"><i class='fa fa-circle-o-notch fa-spin'></i> Lütfen Bekleyin GG Alt Kategorileriniz İndiriliyor...</center>
                <div class="download_status" style="margin-top: 15px;"></div>

            </div>
        </div>
  </div>
</div>
</div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        donwload_ajax("index.php?route=gittigidiyor/category/download_parent&<?php echo $token_link; ?>");
    });

    function donwload_ajax(url) {
        $.ajax({
          url: url,
          type: 'post',
          dataType: 'json',
          success: function(json) {
              if (json.status == 0) {
                  $('.download_status').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json.msg + '</div>');
              }
              if (json.status == 1){
                  $('.download_status').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i>  ' + json.msg + '</div>');
              }
              if(json['next']){
                  donwload_ajax(json['next']);
              } else {
                  <?php if($action == 'edit'){ ?>
                     window.location = 'index.php?route=gittigidiyor/dashboard&action=edit&<?php echo $token_link; ?>';
                  <?php } else { ?>
                     window.location = 'index.php?route=gittigidiyor/dashboard&<?php echo $token_link; ?>';
                  <?php } ?>

              }

          }
        });
  }
</script>
<?php echo $footer; ?>
