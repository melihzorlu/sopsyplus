<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <h1><?php echo $heading_title2; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid" id="content">
    <ul class="nav nav-tabs" role="tablist">
        <?php foreach ($links as $link) { ?>
        <li class="<?php echo $link['css']; ?>"><a href="<?php echo $link['link']; ?>"><b><?php echo $link['text']; ?></b></a></li>
        <?php } ?>
    </ul>
    <div class="panel panel-warning">
        <div class="panel-heading"><?php echo $heading_title; ?></div>
        <div class="panel-body">
          <form class="form-horizontal" method="post">
            <input type="hidden" name="id" value="<?php echo $shop['id']; ?>">
            
           
            <div class="form-group">
              <label class="control-label col-sm-4">VARSAYILAN ALT BAŞLIK</label>
              <div class="col-sm-6">
                <input type="text" class="form-control input-sm" value="<?php echo  $shop['default_subtitle']; ?>" name="default_subtitle" required="required">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-4">KARGO ŞİRKETLERİ <br>(tamamen küçük harf ile yazın ve virgül ile ayırın)</label>
              <div class="col-sm-6">
                <input type="text" class="form-control input-sm" value="<?php echo $shop['cargo_company']; ?>" name="cargo_company" required="required">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-4">ÜCRETSİZ KARGO KOŞULU</label>
              <div class="col-sm-6">
                <select name="shipping_condition" class="form-control input-sm">
                    <option value="0" <?php echo ($shop['shipping_condition'] == 0 ? 'selected' : '');?>>-- İşlem Yapma --</option>
                    <option value="1" <?php echo ($shop['shipping_condition'] == 1 ? 'selected' : '');?>>X TL Üstü Ücretsiz</option>
                    <option value="2" <?php echo ($shop['shipping_condition'] == 2 ? 'selected' : '');?>>Kargo Gereksiz Seçili Ürünler</option>
                    <option value="3" <?php echo ($shop['shipping_condition'] == 3 ? 'selected' : '');?>>Fiyata Dahil (Kargoyu Fiyata Ekle Tüm Ürünleri Ücretsiz Yap)</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-4">KAÇ TL ÜSTÜ ÜCRETİSİZ KARGO</label>
              <div class="col-sm-6">
                <input type="text" class="form-control input-sm" value="<?php echo  $shop['shipping_price']; ?>" name="shipping_price">
              </div>
            </div>
             <div class="form-group">
              <label class="control-label col-sm-4">KARGO ÜCRETİ (FİYATA DAHİL İSE)</label>
              <div class="col-sm-6">
                <input type="text" class="form-control input-sm" value="<?php echo $shop['shipping_aprice']; ?>" name="shipping_aprice">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-4">FİYAT DEĞİŞİM TİPİ</label>
              <div class="col-sm-6">
                <select name="difference_type" id="difference_type" class="form-control input-sm">
                    <option value="0" <?php echo ($shop['difference_type'] == 0 ? 'selected' : '');?>>--- İşlem Yapma ---</option>
                    <option value="yuzde" <?php echo ($shop['difference_type'] == "yuzde" ? 'selected' : '');?>>% Yüzde Arttır</option>
                    <option value="sabit" <?php echo ($shop['difference_type'] == "sabit" ? 'selected' : '');?>>Sabit Tutar Olarak Arttır</option>
                    <option value="yuzdeazalt" <?php echo ($shop['difference_type'] == "yuzdeazalt" ? 'selected' : '');?>>% Yüzde Azalt</option>
                    <option value="sabitazalt" <?php echo ($shop['difference_type'] == "sabitazalt" ? 'selected' : '');?>>Sabit Tutar Olarak Azalt</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-4">FİYAT DEĞİŞİM MİKTARI (YÜZDE / SABİT)</label>
              <div class="col-sm-6">
                <input type="text" class="form-control input-sm" value="<?php echo $shop['difference_value']; ?>" name="difference_value">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-4">MAĞAZA PARA BİRİMİ</label>
              <div class="col-sm-6">
                <select name="currency" id="currency" class="form-control input-sm">
                    <option value="1" <?php echo ($shop['currency'] == 1 ? 'selected' : '');?>>TRY</option>
                    <option value="2" <?php echo ($shop['currency'] == 2 ? 'selected' : '');?>>USD</option>
                    <option value="3" <?php echo ($shop['currency'] == 3 ? 'selected' : '');?>>EUR</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-4">KUR DÖNÜŞTÜRME METODU</label>
              <div class="col-sm-6">
                <select name="currency_calctype" id="currency_calctype" class="form-control input-sm">
                    <option value="1" <?php echo ($shop['currency_calctype'] == 1 ? 'selected' : '');?>>Otomatik, Merkez Bankası</option>
                    <option value="2" <?php echo ($shop['currency_calctype'] == 2 ? 'selected' : '');?>>Sitemdeki Kur Değeri</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-4">FİYAT KDV SEÇENEĞİ</label>
              <div class="col-sm-6">
                <select name="tax_option" id="tax_option" class="form-control input-sm">
                    <option value="1" <?php echo ($shop['tax_option'] == 1 ? 'selected' : '');?>>KDV Dahil</option>
                    <option value="2" <?php echo ($shop['tax_option'] == 2 ? 'selected' : '');?>>KDV Hariç</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-4">VARSAYILAN AÇIKLAMA BİÇİMİ</label>
              <div class="col-sm-8">
                <textarea class="form-control input-sm summernote" id="aciklama" name="description"><?php echo $shop['description'] ?></textarea>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-4"></div>
              <div class="col-sm-6"><button class="btn btn-success" type="submit">MAĞAZAYI KAYDET</button></div>
            </div>
          </form>
      </div>
  </div>
</div>
<script type="text/javascript" src="view/javascript/summernote/summernote.js"></script>
<link href="view/javascript/summernote/summernote.css" rel="stylesheet" />
<script type="text/javascript">
    $('#aciklama').summernote({height: 300});
</script>
<?php echo $footer; ?>