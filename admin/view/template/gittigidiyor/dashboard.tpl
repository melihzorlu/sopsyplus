<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <h1><?php echo $heading_title2; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid" id="content">
    <?php if($error){ ?>
    <div class="alert alert-danger"><?php echo $error; ?></div>
    <?php } ?>
 
    <ul class="nav nav-tabs" role="tablist">
         <?php foreach ($links as $link) { ?>
        <li class="<?php echo $link['css']; ?>"><a href="<?php echo $link['link']; ?>"><b><?php echo $link['text']; ?></b></a></li>
        <?php } ?>
    </ul>
    <div class="panel panel-warning">
        <div class="panel-heading"><?php echo $heading_title; ?></div>
        <div class="panel-body">
            <div class="rows">
                <h4 class="col-lg-12 col-md-12 col-sm-12">GİTTİGİDİYOR ENTEGRASYONUNA HOŞGELDİNİZ!</h4>
                <p class="col-lg-12 col-md-12 col-sm-12">Bu modül tüm gittigiyor işlemlerinizi site panelinden kolayca halletmeniz için tasarlanmıştır. Siparişlerinizi, Ürünlerinizi, Ürün Sorularınızı yönetebilir, ayrıca satış arttırmaya yardımcı araçlarımızla gittigidiyor müşterilerinizi sadık birer site müşteriniz haline getirebilirsiniz.<br><br>
                Modül hakkında daha detaylı bilgiye, kurulum ve kullanım dökümanına ayrıca tanıtım videosuna bilgi tabından ulabilirsiniz.</p>
            </div>
            <hr>
            <div class="rows">
            <dic class="col-md-3">
              <a class="btn btn-success btn-block allsend" onclick="productsend('index.php?route=gittigidiyor/product/eszamanla&<?php echo $token_link; ?>', null)"><i class="fa fa-refresh" aria-hidden="true"></i> EŞZAMANLA</a>
              <a class="btn btn-primary btn-block allsend" onclick="productsend('index.php?route=gittigidiyor/product/sendProduct&<?php echo $token_link; ?>', null)">TÜM ÜRÜNLERİ GÖNDER & GÜNCELLE</a>
              <a class="btn btn-danger btn-block" onclick="productsend('index.php?route=gittigidiyor/product/delProduct&<?php echo $token_link; ?>', null)">TÜM ÜRÜNLERİ SİL</a>
            </dic>
            <div class="col-md-6 tiles">
                <div class="row">
                    <h4 class="col-lg-12 col-md-12 col-sm-12">
                      GİTTİGİDİYOR SİPARİŞLERİNİZE GÖZ ATIN
                    <hr>
                    </h4>
                    <div class="col-lg-4 col-md-4 col-sm-6">
                        <div class="tilered">
                          <div class="tilered-heading">Ödeme Beklenen</div>
                          <div class="tilered-body"><i class="fa fa-shopping-cart"></i>
                            <h2 class="pull-right"><?php echo $salecount['odemebeklenen_count']; ?></h2>
                          </div>
                          <div class="tilered-footer"><a href="<?php echo $salecount['odemebeklenen']; ?>">Siparişler Gör</a></div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6">
                        <div class="tilered">
                          <div class="tilered-heading">KARGO YAPL.</div>
                          <div class="tilered-body"><i class="fa fa-shopping-cart"></i>
                            <h2 class="pull-right"><?php echo $salecount['kargoyapilacaklar_count']; ?></h2>
                          </div>
                          <div class="tilered-footer"><a href="<?php echo $salecount['kargoyaplacaklar']; ?>">Siparişler Gör</a></div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6">
                        <div class="tilered">
                          <div class="tilered-heading">ONAY BEKLEYENLER</div>
                          <div class="tilered-body"><i class="fa fa-shopping-cart"></i>
                            <h2 class="pull-right"><?php echo $salecount['onaybekleyenler_count']; ?></h2>
                          </div>
                          <div class="tilered-footer"><a href="<?php echo $salecount['onaybekleyenler']; ?>">Siparişler Gör</a></div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6">
                        <div class="tilered">
                          <div class="tilered-heading">PARA TRANSFERİ</div>
                          <div class="tilered-body"><i class="fa fa-shopping-cart"></i>
                            <h2 class="pull-right"><?php echo $salecount['paratransferi_count']; ?></h2>
                          </div>
                          <div class="tilered-footer"><a href="<?php echo $salecount['paratransferi']; ?>">Siparişler Gör</a></div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6">
                        <div class="tilered">
                          <div class="tilered-heading">İADELER</div>
                          <div class="tilered-body"><i class="fa fa-shopping-cart"></i>
                            <h2 class="pull-right"><?php echo $salecount['iadekonumu_count']; ?></h2>
                          </div>
                          <div class="tilered-footer"><a href="<?php echo $salecount['iadekonumu']; ?>">Siparişler Gör</a></div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6">
                        <div class="tilered">
                          <div class="tilered-heading">TAMAMLANANLAR</div>
                          <div class="tilered-body"><i class="fa fa-shopping-cart"></i>
                            <h2 class="pull-right"><?php echo $salecount['tamamlananlar_count']; ?></h2>
                          </div>
                          <div class="tilered-footer"><a href="<?php echo $salecount['tamamlananlar']; ?>">Siparişler Gör</a></div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-lg-3 col-md-3">
                  <h4>
                      BEKLEYEN İŞLERİNİZ
                  <hr>
                  </h4>
                  <div class="tile tile-primary">
                          <div class="tile-heading">OKUNMAMIŞ MESAJLAR</div>
                          <div class="tile-body"><i class="fa fa-envelope-o"></i>
                            <h2 class="pull-right"><?php echo $mesajsayi; ?></h2>
                          </div>
                          <div class="tile-footer"><a href="index.php?route=gittigidiyor/product/questions&<?php echo $token_link; ?>">Mesajları Gör</a></div>
                        </div>
                   <div class="tile tile-primary">
                          <div class="tile-heading">EŞLEŞMEMİŞ<br>KATEGORİLERİNİZ</div>
                          <div class="tile-body"><i class="fa fa-bars"></i>
                            <h2 class="pull-right"><?php echo $escat; ?></h2>
                          </div>
                          <div class="tile-footer"><a href="index.php?route=gittigidiyor/category&<?php echo $token_link; ?>&filter_eslesme=0">Kategorilere Git</a></div>
                        </div>
            </div>
            </div>
        </div>
  </div>
</div>
<div class="pageload">
    <div class="container" style="padding: 20% 0px;">
        <center>Yeni Siparişler Alınıyor, Lütfen Bekleyin...<br><br><i class="fa fa-spinner fa-spin"></i></center>
    </div>
</div>
<script type="text/javascript">
    function productsend(url, product_id) {
      $.ajax({
        url: url,
        type: 'post',
        dataType: 'json',
        beforeSend: function() {
          if(product_id != null){
               $('.'+product_id+'btnsend').button('loading');
          } else {
              $('.allsend').button('loading');
          }
        },
        success: function(json) {
            $('.alert, .text-danger, .progress').remove();
            if (json.status == 0) {
                $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json.msg + '</div>');
            }
            if (json.status == 1){
                $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i>  ' + json.msg + '</div>');
            }
            if(product_id != null){
               $('.'+product_id+'btnsend').button('reset');
            } else {
               $('.allsend').button('reset');
            }
            productsend(json['next']);
        }
      });
    }
</script>
<?php echo $footer ?>