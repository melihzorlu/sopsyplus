<?php echo $header; ?>
<style type="text/css">
  .mypanel .form-group {
     border-bottom: 1px solid #ededed;
  }
</style>
<?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <h1><?php echo $heading_title2; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid" id="content">
    <ul class="nav nav-tabs" role="tablist">
        <?php foreach($links as $link){ ?>
        <li class="<?php echo $link['css']; ?>"><a href="<?php echo $link['link']; ?>"><b><?php echo $link['text']; ?></b></a></li>
        <?php } ?>
    </ul>
    <div class="panel panel-danger">
        <div class="panel-heading"><?php echo $heading_title; ?></div>
        <div class="panel-body">
            <?php if($success){ ?>
              <div class="alert alert-success"><?php echo $success; ?></div>
            <?php } ?>
                    <div class="panel panel-default">
                      <div class="panel-heading">MAĞAZA BİLGİLERİ</div>
                      <div class="panel-body form-horizontal">
                          <div class="form-group">
                              <label class="control-label col-md-5">Mağaza Adı</label>
                              <div class="col-md-7">
                                <?php echo $shop['name']; ?>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="control-label col-md-5">Api Key</label>
                              <div class="col-md-7">
                                <?php echo $shop['apikey']; ?>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="control-label col-md-5">Secret Key</label>
                              <div class="col-md-7">
                                <?php echo $shop['secretkey']; ?>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="control-label col-md-5">Role Name</label>
                              <div class="col-md-7">
                                <?php echo $shop['auth_user']; ?>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="control-label col-md-5">Role Pass</label>
                              <div class="col-md-7">
                                <?php echo $shop['auth_pass']; ?>
                              </div>
                          </div>
                      </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">VARSAYILAN DEĞERLER</div>
                        <div class="panel-body form-horizontal mypanel">
                          <div class="row">
                              <div class="col-md-4">
                                  <div class="form-group">
                                      <label class="control-label col-md-6">Kargolama Süresi</label>
                                      <div class="col-md-6"><?php echo str_replace('days', 'gün', $shop['shipping_days']); ?></div>
                                  </div>
                              </div>
                              <div class="col-md-4">
                                  <div class="form-group">
                                      <label class="control-label col-md-6">Kargo Çıkış Şehir</label>
                                      <div class="col-md-6"><?php echo $sehir; ?></div>
                                  </div>
                              </div>
                              <div class="col-md-4">
                                  <div class="form-group">
                                    <label class="control-label col-md-6">Kargo Şirketi</label>
                                    <div class="col-md-6">
                                      <?php echo $shop['cargo_company']; ?>
                                    </div>
                                  </div>
                              </div>
                              <div class="col-md-4">
                                  <div class="form-group">
                                    <label class="control-label col-md-6">Şehiriçi Kargo Bedeli</label>
                                    <div class="col-md-6">
                                      <?php echo $shop['ship_cityprice']; ?>
                                    </div>
                                  </div>
                              </div>
                              <div class="col-md-4">
                                  <div class="form-group">
                                    <label class="control-label col-md-6">Tüm Türkiye Kargo Bedeli</label>
                                    <div class="col-md-6">
                                      <?php echo $shop['ship_countryprice']; ?>
                                    </div>
                                </div>
                              </div>
                              <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label col-md-6">Listeleme Süresi</label>
                                    <div class="col-md-6">
                                      <?php echo $shop['ship_listingday']; ?>
                                    </div>
                                </div>
                              </div>
                              <div class="col-md-4">
                                  <div class="form-group">
                                      <label class="control-label col-md-6">Ücretsiz Kargo Koşulu</label>
                                      <div class="col-md-6">
                                          <?php if($shop['shipping_condition'] == '0'){ echo 'İşlem Yok'; } ?>
                                          <?php if($shop['shipping_condition'] == '1'){ echo 'X TL ÜSTÜ ÜCRETSİZ'; } ?>
                                          <?php if($shop['shipping_condition'] == '2'){ echo 'Kargo Gereksiz Seçili Ürünler Ücretsiz'; } ?>
                                          <?php if($shop['shipping_condition'] == '3'){ echo 'Fiyata Dahil (Kargo ücretini ürün fiyatına ekle ve tümünü ücretsiz yap)'; } ?>
                                      </div>
                                  </div>
                              </div>
                              <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label col-md-6">Kaç TL üstü Ücretsiz</label>
                                    <div class="col-md-6">
                                        <?php echo $shop['shipping_price']; ?>
                                    </div>
                                </div>
                              </div>
                              <div class="col-md-4">
                                  <div class="form-group">
                                    <label class="control-label col-md-6">Fiyat Değişim Şekli</label>
                                    <div class="col-md-6">
                                        <?php if($shop['difference_type'] == '0'){ echo 'İşlem Yok'; } ?>
                                        <?php if($shop['difference_type'] == 'yuzde'){ echo 'Yüzde Olarak Arttır'; } ?>
                                        <?php if($shop['difference_type'] == 'sabit'){ echo 'Sabit Miktar Olarak Arttır'; } ?>
                                        <?php if($shop['difference_type'] == 'yuzdeazalt'){ echo 'Yüzde Olarak Azalt'; } ?>
                                        <?php if($shop['difference_type'] == 'sabitazalt'){ echo 'Sabit Miktar Olarak Azalt'; } ?>
                                    </div>
                                  </div>
                              </div>
                              <div class="col-md-4">
                                  <div class="form-group">
                                    <label class="control-label col-md-6">Fiyat Değişim Değeri</label>
                                    <div class="col-md-6">
                                      <?php echo $shop['difference_value']; ?>
                                    </div>
                                  </div>
                              </div>
                              <div class="col-md-4">
                                  <div class="form-group">
                                      <label class="control-label col-md-6">Mağaza Para Birimi</label>
                                      <div class="col-md-6">
                                         <?php if($shop['currency'] == '1'){ echo 'TRY'; } ?>
                                          <?php if($shop['currency'] == '2'){ echo 'USD'; } ?>
                                          <?php if($shop['currency'] == '3'){ echo 'EUR'; } ?>
                                      </div>
                                  </div>
                              </div>
                              <div class="col-md-4">
                                  <div class="form-group">
                                      <label class="control-label col-md-6">Kur Dönüştürme Metodu</label>
                                      <div class="col-md-6">
                                        <?php if($shop['currency_calctype'] == '1'){ echo 'Oto Merkez Bankası'; } ?>
                                        <?php if($shop['currency_calctype'] == '2'){ echo 'Sistem Kur Değeri'; } ?>
                                      </div>
                                  </div>
                              </div>
                              <div class="col-md-4">
                                  <div class="form-group">
                                    <label class="control-label col-md-6">Kdv Seçeneği</label>
                                    <div class="col-md-6">
                                        <?php if($shop['tax_option'] == '1'){ echo 'KDV Dahil'; } ?>
                                        <?php if($shop['tax_option'] == '2'){ echo 'KDV Hariç'; } ?>
                                    </div>
                                  </div>
                              </div>
                          </div>
                        </div>
                        <div class="panel-footer">
                          <a class="btn btn-primary" href="<?php echo $degerleri_duzenle; ?>">Değerleri Düzenle</a>
                        </div>
                    </div>
               
      </div>
  </div>
</div>
<?php echo $footer; ?>