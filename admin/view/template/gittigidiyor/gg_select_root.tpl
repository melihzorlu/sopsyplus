<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <h1><?php echo $heading_title2; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid" id="content">
    <ul class="nav nav-tabs" role="tablist">
        <?php foreach ($links as $link) { ?>
        <li class="<?php echo $link['css']; ?>"><a href="<?php echo $link['link']; ?>"><b><?php echo $link['text']; ?></b></a></li>
        <?php } ?>
    </ul>
    <div class="panel panel-danger">
        <div class="panel-heading"><?php echo $heading_title; ?></div>
        <div class="panel-body selectlist">
          <div class="row">
              <h4 class="col-lg-12 col-md-12 col-sm-12">N11 ANA KATEGORİLERİ SEÇME</h4>
              <p class="col-lg-12 col-md-12 col-sm-12">Bu alandan n11 ana kategorileri seçebilirsiniz, bu alan sayesinde gereksiz olan kategorilerin alt kategorileri ile boğuşmaz ve o kategorilerin özelliklerin inmesini beklemezsiniz.</p>
          </div>
          <?php if($success){ ?>
          <div class="alert alert-success"><?php echo $success; ?></div>
         <?php } ?>
          <hr style="margin-top: 5px; margin-bottom: 5px;">
            <a class="btn btn-primary" href="<?php echo $return; ?>"><i class="fa fa-reply" aria-hidden="true"></i> GERİ DÖN</a>
          <hr style="margin-top: 5px;">
          <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
            <input type="hidden" name="kategorilerikaydet" value="kydt" />
            <div class="row">
            <?php foreach($ggcat as $ggct){ ?>
              <div class="col-sm-4">
                <div class="togselect" style="margin:3px;  border:1px solid #ededed; padding: 0px 5px; border-radius: 3px;">
                  <input type="checkbox" data-value="<?php echo $ggct->categoryCode; ?>" type="checkbox" data-toggle="toggle" data-size="mini"<?php if(isset($gg_kategoriler)){if(in_array($ggct->categoryCode, $gg_kategoriler)){echo 'checked';}}?> id="<?php echo $ggct->categoryCode; ?>" value="<?php echo $ggct->categoryCode; ?>" name="gg_kategori[]" />
                  <label for="<?php echo $ggct->categoryCode; ?>" style="border-left: 1px solid #ededed; margin-bottom: 0px; margin-left: 5px; padding: 5px 5px 6px 5px; font-family: Arial;"><?php echo $ggct->categoryName; ?></label>
                </div>
              </div>
            <?php } ?>
            </div>
            <hr>
            <input type="submit" value="AYARLARI KAYDET" class="btn btn-primary btn-xl">
          </form>
      </div>
  </div>
</div>
<script type="text/javascript">
  $('.togselect input[type=checkbox]').bootstrapToggle({
    on: '<i class="fa fa-check" aria-hidden="true"></i>',
    off: '<i class="fa fa-times" aria-hidden="true"></i>'
  });
</script>
<?php echo $footer; ?>