<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <h1><?php echo $heading_title2; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid" id="content">
    <ul class="nav nav-tabs" role="tablist">
        <?php foreach ($links as $link) { ?>
        <li class="<?php echo $link['css']; ?>"><a href="<?php echo $link['link']; ?>"><b><?php echo $link['text']; ?></b></a></li>
        <?php } ?>
    </ul>
    <div class="panel panel-warning">
        <div class="panel-heading"><?php echo $heading_title; ?></div>
        <div class="panel-body">
          <!--<div class="row">
                <h4 class="col-lg-12 col-md-12 col-sm-12">KATEGORİ İŞLEMLERİ</h4>
                <p class="col-lg-12 col-md-12 col-sm-12">Bu sayfadan kategori eşleştirmelerinizi yapabilir, kategorilere varsayılan özellikler tanımlayabilir ve kategori için özel komisyon oranlarını ayarlayabilirsiniz. gg kategorilerini görmek, kategorilerin özelliklerini indirmek için gg kategori yönetimi sayfasına gidiniz.</p>
          </div>
          <hr>-->
          <div class="well">
                  <div class="row">
                      <div class="col-md-3">
                        <div class="form-group">
                          <label>Kategori (Otm. Tamamlama)</label>
                          <input type="text" class="form-control input-sm" name="filter_category" value="<?php echo $filter_category; ?>">
                          <input type="hidden" name="filter_category_id" value="<?php echo $filter_category_id; ?>">
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label>Kategori Durumu</label>
                          <select name="filter_status" class="form-control input-sm">
                            <option value="">Tümü</option>
                            <?php if($filter_status  ==  '1'){ ?>
                            <option value="1" selected="selected">Açık</option>
                            <?php } else { ?>
                            <option value="1">Açık</option>
                            <?php } ?>

                            <?php if($filter_status  ==  '0'){ ?>
                            <option value="0" selected="selected">Kapalı</option>
                            <?php } else { ?>
                            <option value="0">Kapalı</option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label>Eşleşme Durumu</label>
                          <select name="filter_eslesme" class="form-control input-sm">
                            <option value="">Tümü</option>
                            <?php if($filter_eslesme  ==  '1'){ ?>
                            <option value="1" selected="selected">Eşleşenler</option>
                            <?php } else { ?>
                            <option value="1">Eşleşenler</option>
                            <?php } ?>

                            <?php if($filter_eslesme  ==  '0'){ ?>
                            <option value="0" selected="selected">Eşleşmeyenler</option>
                            <?php } else { ?>
                            <option value="0">Eşleşmeyenler</option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>

                      <div class="col-md-3">
                        <div class="form-group">
                          <label>Komisyon Durumu Durumu</label>
                          <select name="filter_komisyon" class="form-control input-sm">
                            <option value="">Tümü</option>
                            <?php if($filter_komisyon  ==  '1'){ ?>
                            <option value="1" selected="selected">Var</option>
                            <?php } else { ?>
                            <option value="1">Var</option>
                            <?php } ?>

                            <?php if($filter_komisyon  ==  '0'){ ?>
                            <option value="0" selected="selected">Yok</option>
                            <?php } else { ?>
                            <option value="0">Yok</option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>
                  </div>
                  <button class="btn btn-primary btn-sm" id="button-filter">Kategorileri Getir</button>
              </div>

          <table class="table table-bordered">
              <thead>
                <th>KATEGORİ</th>
                <th>GG KATEGORİ</th>
                <th><span data-toggle="tooltip" title="" data-original-title="Eğer ürün birebir eşleştirilmemişse, varsaylan kategori özellikleri kullanılır.">VARSAYILAN ÖZELLİKLER <i class="fa fa-question-circle"></i></span></th>
                <th><span data-toggle="tooltip" title="" data-original-title="Eğer ürün gittigidiyor tarafında varyantlı ise, bu alanı doldurmak zorundasınız, kendi sitenizdeki seçenekerle eşleştirebilirsiniz.">SEÇENEKLER <i class="fa fa-question-circle"></i></span></th>
                <th>KOMİSYON (%)</th>
              </thead>
              <tbody>
                <?php if($categories){ ?>
                  <?php foreach($categories as $category){ ?>
                  <tr>
                    <td><?php echo $category['name']; ?></td>
                    <td>
                      <?php if($category['gg_id']){ ?>
                        <a href="#" class="eslestirme" data-type="typeaheadjs" data-pk="<?php echo $category['category_id']; ?>" data-value="<?php echo $category['gg_id']; ?>|<?php echo $category['gg_name']; ?>" data-url="index.php?route=gittigidiyor/category/eslestir&<?php echo $token_link; ?>" data-title="Lütfen Kategori Arayın"></a>
                      <?php  } else { ?>
                      <a href="#" class="eslestirme" data-type="typeaheadjs" data-pk="<?php echo $category['category_id']; ?>" data-value="" data-url="index.php?route=gittigidiyor/category/eslestir&<?php echo $token_link; ?>" data-title="Lütfen Kategori Arayın"></a>
                      <?php } ?>
                    </td>
                    <td class="attr_<?php echo $category['category_id']; ?>">
                      <?php if(!$category['gg_id']){
                       echo 'Önce Kategoriyi Eşleştirin';
                       } else {
                       ?>
                      <a data-ocid="<?php echo $category['category_id']; ?>"  data-ggid="<?php echo $category['gg_id']; ?>" class="btn btn-xs btn-warning setattr">Özellikleri Ayarla</a>
                      <?php } ?>
                    </td>
                    <td class="variants_<?php echo $category['category_id']; ?>">
                      <?php if(!$category['gg_id']){ echo 'Önce Kategoriyi Eşleştirin'; } else { ?>
                      <a class="btn btn-xs btn-warning secenekesitle" data-categorycode="<?php echo $category['gg_id']; ?>">Seçenekleri Eşitle</a>
                      <?php } ?>
                    </td>
                    <td>
                      <?php if($category['ggcomission'] != 0){ ?>
                      <a href="#" class="komisyon" data-type="text" data-pk="<?php echo $category['category_id']; ?>" data-value="<?php echo $category['ggcomission']; ?>" data-url="index.php?route=gittigidiyor/category/category_komisyon&<?php echo $token_link; ?>" data-title="Kategori Komisyon Oranını Girin">%<?php echo $category['ggcomission']; ?></a>
                      <?php } else { ?>
                      <a href="#" class="komisyon" data-type="text" data-pk="<?php echo $category['category_id']; ?>" data-value="" data-url="index.php?route=gittigidiyor/category/category_komisyon&<?php echo $token_link; ?>" data-title="Kategori Komisyon Oranını Girin"></a>
                      <?php } ?>
                    </td>
                  </tr>
                <?php  } ?>
                <?php } else { ?>

                <tr>
                  <td colspan="5"><center>Henüz Bir Kategoriniz Yok</center></td>
                </tr>
                <?php } ?>
              </tbody>
          </table>
          <div class="row">


            <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
            <div class="col-sm-6 text-right"><?php echo $results; ?></div>
          </div>
      </div>
  </div>
</div>





<!-- attr modal -->
<div class="modal fade" id="attrmodal" tabindex="-1" role="dialog" aria-labelledby="attrmodallabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Kategori Özelliklerini Ata</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Vazgeç</button>
        <button type="button" class="btn btn-primary saveattr">Değişiklikleri Kaydet</button>
      </div>
    </div>
  </div>
</div>
<!-- seçenek eşitleme modalı -->
<div class="modal fade" id="secenekmodal" tabindex="-1" role="dialog" aria-labelledby="secenekmodalmodallabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Seçenekleri Eşitleyin</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Vazgeç</button>
        <button type="button" class="btn btn-primary seceneksave">Değişiklikleri Kaydet</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">

$('#button-filter').on('click', function() {
  var url = 'index.php?route=gittigidiyor/category&<?php echo $token_link; ?>';
  var filter_status = $('select[name=\'filter_status\']').val();
  if (filter_status) {
    url += '&filter_status=' + encodeURIComponent(filter_status);
  }

  var filter_eslesme = $('select[name=\'filter_eslesme\']').val();
  if (filter_eslesme) {
    url += '&filter_eslesme=' + encodeURIComponent(filter_eslesme);
  }

  var filter_category = $('input[name=\'filter_category\']').val();
  if (filter_category) {
    url += '&filter_category=' + encodeURIComponent(filter_category);
  } else {
    $('input[name=\'filter_category_id\']').val('');
  }
  var filter_category_id = $('input[name=\'filter_category_id\']').val();
  if (filter_category_id) {
    url += '&filter_category_id=' + encodeURIComponent(filter_category_id);
  }

  var filter_komisyon = $('select[name=\'filter_komisyon\']').val();
  if (filter_komisyon) {
    url += '&filter_komisyon=' + encodeURIComponent(filter_komisyon);
  }

  location = url;
});

  $('.togselect input[type=checkbox]').bootstrapToggle({
      on: '<i class="fa fa-check" aria-hidden="true"></i>',
      off: '<i class="fa fa-times" aria-hidden="true"></i>'
  });


  // özellikleri kaydedin
  $(document).on('click', '.saveattr', function(){
     $.ajax({
        url: 'index.php?route=gittigidiyor/category/attrsave&<?php echo $token_link; ?>',
        type: 'post',
        dataType: 'json',
        data : $('#attrform').serialize(),
        success: function(json) {
           if(json.status == 1){
              $('#attrmodal').modal('hide');
              $('#attrmodal .modal-body').html('');
              $.toast({heading: 'Başarılı',text: json.msg, position: 'top-right',loader: false,allowToastClose: false,showHideTransition: 'slide',icon: 'success'});
           }
        }
      });
  });

  // özellik formnunu getir
  $(document).ready(function(){

    $(document).on('click','.setattr', function(e){
        var ocid = $(this).data('ocid');
        var ggid = $(this).data('ggid');
        $( "#attrmodal .modal-body" ).load( "index.php?route=gittigidiyor/category/getattr&<?php echo $token_link; ?>&ocid="+ocid+"&ggid="+ggid, function() {
           $('#attrmodal').modal('show');
           e.preventDefault();
        });
    });


    // kategoriyi eşeştir
    $('.eslestirme').editable({
        mode : 'inline',
        typeahead: {
            limit: 200,
            remote: 'index.php?route=gittigidiyor/category/ggkategoriara&<?php echo $token_link; ?>&filter_name=%QUERY',
            displayKey: 'name',
            valueKey : 'name',
            display: function(item){ return item.name }
        }
    }).on('save', function(e, params) {
      console.log(params);
        if(params.newValue == ''){
          $.toast({heading: 'Başarılı',text: 'Eşleştirme Silindi!', position: 'top-right',loader: false,allowToastClose: false,showHideTransition: 'slide',icon: 'success'});
          $('.attr_'+params.response.opcategory).html('Önce Kategoriyi Eşleştirin');
        } else {
          $.toast({heading: 'Başarılı',text: params.newValue +' Başarıyla Eşleşti', position: 'top-right',loader: false,allowToastClose: false,showHideTransition: 'slide',icon: 'success'});
          $('.attr_'+params.response.opcategory).html('<a data-ocid="'+params.response.opcategory+'"  data-ggid="'+params.response.ggcategory+'" class="btn btn-xs btn-warning setattr">Özellikleri Ayarla</a>');
        }
    });


    $('.komisyon').editable({mode : 'inline', inputclass: 'intclass input-sm'});

    $(document).on('click', '.seceneksave', function(){
         $.ajax({
            url: 'index.php?route=gittigidiyor/category/seceneksave&<?php echo $token_link; ?>',
            type: 'post',
            dataType: 'json',
            data : $('#secenekform').serialize(),
            success: function(json) {
               if(json.status == 1){
                  $('#secenekmodal').modal('hide');
                  $('#secenekmodal .modal-body').html('');
                  $.toast({heading: 'Başarılı',text: json.msg, position: 'top-right',loader: false,allowToastClose: false,showHideTransition: 'slide',icon: 'success'});
               }
            }
          });
    });

    $(document).on('change', '.secenek_secin', function(){
          var categorycode = $('#secenekform input[name=categorycode]').val();
          var specid = $(this).data('specid');
          var optionid = $(this).val();
          if(optionid == 'sabit'){
              $(document).find('.secenek_degeri_'+specid).remove();
              $.ajax({
                  url: 'index.php?route=gittigidiyor/category/getoptionvalue&<?php echo $token_link; ?>&optionid='+optionid+'&categorycode='+categorycode+'&specid='+specid,
                  dataType: 'html',
                  success: function(results) {
                      $(document).find('.gsecene_ad_'+specid).after(results);
                  }
              });
          } else {
              $.ajax({
                  url: 'index.php?route=gittigidiyor/category/getoptionvalue&<?php echo $token_link; ?>&optionid='+optionid+'&categorycode='+categorycode+'&specid='+specid,
                  dataType: 'html',
                  success: function(results) {
                      $(document).find('.select_spec_'+specid).html(results);
                  }
              });
          }

    });

    $(document).on('click','.secenekesitle', function(e){
        var categorycode = $(this).data('categorycode');
        $( "#secenekmodal .modal-body" ).load( "index.php?route=gittigidiyor/category/getsecenek&<?php echo $token_link; ?>&categorycode="+categorycode, function() {
           $('#secenekmodal').modal('show');
           e.preventDefault();
        });
    });
  });

  // Category
$('input[name=\'filter_category\']').autocomplete({
  minLength: 2,
  'source': function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/category/autocomplete&<?php echo $token_link; ?>&filter_name=' +  encodeURIComponent(request),
      dataType: 'json',
      success: function(json) {
        response($.map(json, function(item) {
          return {
            label: item['name'],
            value: item['category_id']
          }
        }));
      }
    });
  },
  'select': function(item) {
    $('input[name=filter_category]').val(item['label']);
    $('input[name=filter_category_id]').val(item['value']);
  }
});

</script>
<?php echo $footer; ?>
