<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <h1><?php echo $heading_title2; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid" id="content">
    <?php if($error){ ?>
    <div class="alert alert-danger"><?php echo $error; ?></div>
    <?php } ?>

    <ul class="nav nav-tabs" role="tablist">
        <?php foreach ($links as $link) { ?>
        <li class="<?php echo $link['css']; ?>"><a href="<?php echo $link['link']; ?>"><b><?php echo $link['text']; ?></b></a></li>
        <?php } ?>
    </ul>
    <div class="panel panel-warning">
        <div class="panel-heading"><?php echo $heading_title; ?></div>
        <div class="panel-body">
            <div class="rows">
                <h4 class="col-lg-12 col-md-12 col-sm-12">GİTTİGİDİYOR MESAJ KUTUSU</h4>
                <p class="col-lg-12 col-md-12 col-sm-12">Bu sayfadan gittigidiyor mesajlarınızı görebilir, yanıtlayabilirsiniz. Sağ kısımda bulunan filtrelemeyi kullanarak okunmamış mesajlarınızı veya tüm mesajlarınızı görebilirsiniz.</p>
            </div>
            <hr style="margin-top: 5px; margin-bottom: 5px;">
            <div class="clearfix"></div>
            <hr style="margin-top: 5px;">
            <?php foreach ($messages as $message) { ?>
              <fieldset style="<?php if($message->floatz == 'left'){ echo 'background: #f5f5f5;'; } else {  'background: #e1f0f6;'; } ?> padding:15px; margin-bottom: 20px;">
                  <legend style="    border: 1px solid #ededed;
    padding-bottom: 5px;
    background: #fff;
    padding: 5px 5px;
    font-size: 12px;
    text-align: <?php echo $message->floatz; ?>"><?php echo $message->senderNickName; ?></legend>
                  <p><?php echo $message->messageText; ?><br><br><small><?php echo $message->messageDate; ?></small></p>
              </fieldset>
            <?php } ?>
            <form action="" method="post">
              <div class="form-group">
                <textarea class="form-control" required="required" name="message" placeholder="Mesajınızı Yazın"></textarea>
              </div>
              <div class="form-group">
                <button type="submit" class="btn btn-primary">Mesajı Gönder</button>
              </div>
            </form>
      </div>
  </div>
</div>
<?php echo $footer; ?>