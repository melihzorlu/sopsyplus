<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <h1><?php echo $heading_title2; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid" id="content">
    <ul class="nav nav-tabs" role="tablist">
        <?php foreach ($links as $link) { ?>
        <li class="<?php echo $link['css']; ?>"><a href="<?php echo $link['link']; ?>"><b><?php echo $link['text']; ?></b></a></li>
        <?php } ?>
    </ul>
    <div class="panel panel-warning">
        <div class="panel-heading"><?php echo $heading_title; ?></div>
        <div class="panel-body">
            <div class="row">
            <h4 class="col-lg-12 col-md-12 col-sm-12">LİSANS AYARLARI</h4>
            <p class="col-lg-12 col-md-12 col-sm-12">Kullandığınız eklenti lisanslı bir eklentidir, site bilgileriniz üretiyice iletilmiştir! Lütfen lisans kodunuzu girip kaydet butonuna basınız. Eğer bir lisans kodunuz yoksa satis@xdestek.com email adresi ile iletişime geçebilirsiniz.</p>
            </div>
            <hr>
            <form method="post">
              <div class="form-group">
                <label>Lisans Kodu</label>
                <input type="text" name="gglicence" value="<?php echo $gglicence; ?>" class="form-control">
              </div>
              <button type="submit" class="btn btn-primary">Lisansı Kaydet</button>
            </form>
        </div>
  </div>
</div>
<?php echo $footer; ?>