<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <h1><?php echo $heading_title2; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid" id="content">
    <ul class="nav nav-tabs" role="tablist">
        <?php foreach ($links as $link) { ?>
        <li class="<?php echo $link['css']; ?>"><a href="<?php echo $link['link']; ?>"><b><?php echo $link['text']; ?></b></a></li>
        <?php } ?>
    </ul>
    <div class="panel panel-warning">
        <div class="panel-heading"><?php echo $heading_title; ?></div>
        <div class="panel-body">
            <div class="row">
                <!--
                <div class="col-md-4">
                  <div class="arac">
                    <div class="pd10">
                    <h4>MÜŞTERİ AKTARIM ARACI</h4>
                    <p>Bu Araç gittigidiyor.com müşterileriniz için opencart sitenizde hesap oluşturup şifrelerini ve mail adreslerine otomatik gönderen bir araçtır. Bu araç ile pazaryeri müşterilerizi kendi sitenize çekebilirsiniz.</p>
                    <p class="text-danger"><strong>Önemli Uyarı :</strong> Bu araç pazaryeri mağazanızın kapanmasına neden olabilir, doğacak sorumluluklardan ocdestek.com sorumlu değildir.</p>
                    </div>
                    <hr style="margin-top: 0px; margin-bottom: 5px;">
                    <div class="pd10">
                      <button class="btn btn-primary allsend" onclick="starttool('index.php?route=gittigidiyor/tools/downloadCustomer&user_user_token={{user_user_token}}')">ARACI ÇALIŞTIR</button>
                    </div>
                  </div>
                </div>
                -->
                <center>Gittigidiyor için henüz bir araç yok</center>
            </div>
        </div>
  </div>
</div>
<script type="text/javascript">
  function starttool(url) {
      $.ajax({
        url: url,
        type: 'post',
        dataType: 'json',
        beforeSend: function() {
            $('.allsend').button('loading');
        },
        success: function(json) {
            $('.alert, .text-danger, .progress').remove();
            if (json.status == 0) {
                $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json.msg + '</div>');
            }
            if (json.status == 1){
                $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i>  ' + json.msg + '</div>');
            }
            if(json['next']){
                starttool(json['next']);
            } else {
               $('.allsend').button('reset');
            }
        }
      });
    }
</script>
<?php echo $footer; ?>