<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <h1><?php echo $heading_title2; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid" id="content">
    <ul class="nav nav-tabs" role="tablist">
        <?php foreach ($links as $link) { ?>
        <li class="<?php echo $link['css']; ?>"><a href="<?php echo $link['link']; ?>"><b><?php echo $link['text']; ?></b></a></li>
        <?php } ?>
    </ul>
    <div class="panel panel-warning">
        <div class="panel-heading"><?php echo $heading_title; ?></div>
        <div class="panel-body">
            <div class="col-md-12">
              <button type="button" class="btn btn-primary downparent" onclick="donwload_ajax('index.php?route=gittigidiyor/secenekler/checkvariantmycategories&<?php echo $token_link; ?>');">Gittigidiyor Seçeneklerini İndir</button>
            </div>

            <br><br>
        </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  function donwload_ajax(url) {
      $.ajax({
          url: url,
          type: 'post',
          dataType: 'json',
          beforeSend: function() {
            $('.downparent').button('loading');
          },
          complete: function() {
            $('.downparent').button('reset');
          },
          success: function(json) {
              $('.alert, .text-danger').remove();
              if (json.status == 0) {
                  $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json.msg + '</div>');
              }
              if (json.status == 1){
                  $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i>  ' + json.msg + '</div>');
              }
              donwload_ajax(json['next']);
          }
      });
  }

</script>
<?php echo $footer; ?>