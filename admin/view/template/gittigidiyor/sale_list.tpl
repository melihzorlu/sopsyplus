<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <h1><?php echo $heading_title2; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid" id="content">
        <ul class="nav nav-tabs" role="tablist">
            <?php foreach ($links as $link) { ?>
            <li class="<?php echo $link['css']; ?>"><a href="<?php echo $link['link']; ?>"><b><?php echo $link['text']; ?></b></a></li>
            <?php } ?>
        </ul>
        <div class="panel panel-warning">
            <div class="panel-heading"><?php echo $heading_title; ?></div>
            <div class="panel-body">
                <div class="row">
                    <h4 class="col-lg-12 col-md-12 col-sm-12">GİTTİGİDİYOR SİPARİŞ YÖNETİMİ</h4>
                    <p class="col-lg-12 col-md-12 col-sm-12">Bu sayfadan gittigidiyor siparişlerinizi görebilirsiniz ve yönetebilirsiniz. Sipariş kalemini kabul edebilir veya reddedebilirsiniz. Yeni sipariş kontrolü ile yeni siparişlerinizi kontrol edebilirsiniz. Siparişleri aktar ile geçmiş siparişlerinizi aktarabilir. Tam kontrol ile siparişlerinizin kontrol ve durum güncellemelerini sağlayabilirsiniz.</p>
                </div>
                <hr style="margin-top: 5px; margin-bottom: 5px;">
                <div class="pull-left">
                    <div class="btn-group">
                        <button class="btn btn-primary btn-sm <?php if($status == 'P'){ echo 'active'; } ?>" onclick="goOrder('P');" >Ödeme Beklenen</button>
                        <button class="btn btn-primary btn-sm <?php if($status == 'S'){ echo 'active'; } ?>" onclick="goOrder('S');">Kargo Yapılacaklar</button>
                        <button class="btn btn-primary btn-sm <?php if($status == 'C'){ echo 'active'; } ?>" onclick="goOrder('C');">Onay Bekleyenler</button>
                        <button class="btn btn-primary btn-sm <?php if($status == 'T'){ echo 'active'; } ?>" onclick="goOrder('T');">Para Transferleri</button>
                        <button class="btn btn-primary btn-sm <?php if($status == 'R'){ echo 'active'; } ?>" onclick="goOrder('R');">İadeler</button>
                        <button class="btn btn-primary btn-sm <?php if($status == 'O'){ echo 'active'; } ?>" onclick="goOrder('O');">Tamamlananlar</button>
                    </div>
                </div>
                <div class="clearfix"></div>
                <hr style="margin-top: 5px;">
                <table class="table table-bordered">
                    <thead>
                    <th>Resim</th>
                    <th>Alıcı</th>
                    <th>Satış Kodu</th>
                    <th>Ürün Adı</th>
                    <th>Adet</th>
                    <th>Fiyat</th>
                    <th>Tarih</th>
                    <th>Durum</th>
                    <th>İşlem</th>
                    </thead>
                    <?php if($orders){ ?>
                    <?php foreach($orders as $order){
                      ?>
                    <tr>
                        <td><img src="<?php echo $order->thumbImageLink; ?>" width="50" height="50"></td>
                        <td><?php echo $order->buyerInfo->name; ?> <?php echo $order->buyerInfo->surname; ?></td>
                        <td><?php echo $order->saleCode; ?></td>
                        <td><?php echo $order->productTitle; ?></td>
                        <td><?php echo $order->amount; ?> </td>
                        <td><?php echo $order->price; ?></td>
                        <td><?php echo $order->lastActionDate; ?></td>
                        <td><?php echo $order->status; ?></td>
                        <td>
                            <button class="btn btn-primary btn-block btn-xs detail" data-salecode="<?php echo $order->saleCode; ?>">Sipariş Detayları</button>

                            <?php if($status == 'S'){ ?>
                            <button class="btn btn-warning btn-block btn-xs kargoinsert" data-salecode="<?php echo $order->saleCode; ?>">Kargo Bilgisi Gir</button>
                            <?php } ?>
                            <?php if($order->checkoc == 0){ ?>
                            <button class="btn btn-info btn-block btn-xs ocaktar btn_<?php echo $order->saleCode; ?>" data-salecode="<?php echo $order->saleCode; ?>">Siparişi Aktar</button>
                            <?php } ?>
                        </td>
                    </tr>
                    <?php } ?>
                    <?php } else { ?>
                    <tr>
                        <td colspan="9"><center>Sipariş Bulunamadı</center></td>
                    </tr>
                    <?php } ?>
                </table>
                <hr>
                <div class="pagination pull-right">
                    <?php if($prev){ ?>
                    <li><a href="<?php echo $prev; ?>"><i class="fa fa-chevron-left"></i> Önceki Sayfa</a></li>
                    <?php } ?>
                    <li><a>Sayfa <?php echo $page; ?></a></li>
                    <?php if($next){ ?>
                    <li><a href="<?php echo $next; ?>">Sonraki Sayfa <i class="fa fa-chevron-right"></i></a></li>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="pageload">
        <div class="container" style="padding: 20% 0px;">
            <center>Lütfen Bekleyin...<br><br><i class="fa fa-spinner fa-spin"></i></center>
        </div>
    </div>
    <div class="modal fade" id="detaymodal" tabindex="-1" role="dialog" aria-labelledby="detaymodallabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"></h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        function goOrder(status){
            location.href = 'index.php?route=gittigidiyor/sales&<?php echo $token_link; ?>&status='+status;
        }
        $(document).on('click', '.detail', function(){
            $("#detaymodal .modal-body").html('');
            var salecode = $(this).data('salecode');
            $('.pageload').fadeIn();
            $('body').addClass('modal-open');
            $('#detaymodal .modal-title').html(salecode+' Numaralı Sipariş');
            $("#detaymodal .modal-body").load( "index.php?route=gittigidiyor/sales/sale_detail&<?php echo $token_link; ?>&salecode="+salecode, function() {
                $('#detaymodal').modal('show');
                $('.pageload').fadeOut();
            });
        });

        $(document).on('click', '.kargoinsert', function(){
            $("#detaymodal .modal-body").html('');
            var salecode = $(this).data('salecode');
            $('.pageload').fadeIn();
            $('body').addClass('modal-open');
            $('#detaymodal .modal-title').html(salecode+' Numaralı Sipariş Kargo Girişi');
            $("#detaymodal .modal-body").load( "index.php?route=gittigidiyor/sales/kargo_insert&<?php echo $token_link; ?>&salecode="+salecode, function() {
                $('#detaymodal').modal('show');
                $('.pageload').fadeOut();
            });
        });

        $(document).on('click', '.savekargo', function(){
            $('.siparisaktar').button('loading');
            $.ajax({
                url: 'index.php?route=gittigidiyor/sales/save_kargo&<?php echo $token_link; ?>',
                type: 'post',
                dataType: 'json',
                data : $('#kargoform').serialize(),
                success: function(json) {
                    if(json.status == 1){
                        $('#detaymodal').modal('hide');
                        $('#detaymodal .modal-body').html('');
                        $.toast({heading: 'Başarılı',text: json.msg, position: 'top-right',loader: false,allowToastClose: false,showHideTransition: 'slide',icon: 'success'});
                    } else {
                        $.toast({heading: 'Hata',text: json.msg, position: 'top-right',loader: false,allowToastClose: false,showHideTransition: 'slide',icon: 'danger'});
                        $('.savekargo').button('reset');
                    }
                }
            });
        });

        $(document).on('click', '.ocaktar', function(){
            $('.ocaktar').button('loading');
            var salecode = $(this).data('salecode');
            $.ajax({
                url: 'index.php?route=gittigidiyor/sales/siparisaktar&<?php echo $token_link; ?>',
                type: 'post',
                dataType: 'json',
                data : { salecode : salecode },
                success: function(json) {
                    if(json.status == 1){
                        $('.btn_'+salecode).remove();
                        $.toast({heading: 'Başarılı',text: json.msg, position: 'top-right',loader: false,allowToastClose: false,showHideTransition: 'slide',icon: 'success'});
                    } else {
                        $.toast({heading: 'Hata',text: json.msg, position: 'top-right',loader: false,allowToastClose: false,showHideTransition: 'slide',icon: 'danger'});
                        $('.ocaktar').button('reset');
                    }
                     $('.ocaktar').button('reset');
                }
            });
        });
    </script>
    <?php echo $footer; ?>