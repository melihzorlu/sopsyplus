<?php echo $header; ?>
<style type="text/css">
  .label { font-size: 12px !important; padding: 5px !important; min-width: 100px; }
  .btn-group .btn { border:0px !important; border-radius: 0px; }
  .btn-group .active {
    background: rgba(0,0,0,0.7) !important;
  }
</style>
<?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <h1><?php echo $heading_title2; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid" id="content">
    <ul class="nav nav-tabs" role="tablist">
        <?php foreach ($links as $link) { ?>
        <li class="<?php echo $link['css']; ?>"><a href="<?php echo $link['link']; ?>"><b><?php echo $link['text']; ?></b></a></li>
        <?php } ?>
    </ul>
    <div class="panel panel-danger">
        <div class="panel-heading"><?php echo $heading_title; ?></div>
        <div class="panel-body">
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="alert alert-danger" style="padding: 10px; font-size: 12px !important;">
              <strong>ÖNEMLİ UYARILAR</strong><br>
              <p style="font-size: 12px !important;">
                1 - Eğer bir ürün satış almışsa ürünün resmini değiştiremezsiniz, ürünü önce satıştan kaldırıp, sonra silip yeniden eklemeniz gerekmektedir.<br>
                2 - Bir ürünü satıştan kaldırdığınızda bu ürünün silindiği anlamına gelmez <strong>15 dakika kadar</strong> sonra SATILMAYANLAR tabına düşecektir, bu tabdan silebilir veya yeniden yayına alabilirsiniz<br>
                3 - Yapılan her işlem için log tutulmaktadır ve açıklayıcı hata mesajları vardır, bir ürün ile ilgili sorun oluştuğunda destek istemeden önce LOGLAR tabından kayıtlarına bakmayı unutmayın<br>
                4 - Bir ürün <strong>nadirende olsa</strong> gittigidiyora gönderilmiş ama yayına alınmamış olabilir, bu ürünleri Listelenmeye Hazır Ürünler tabından görebilirsiniz ve yayına alabilirsiniz.<br>
                5 - Oran değiştirdikten sonra ürünü silip / eklemenize gerek yoktur stok ve fiyat güncellemesi yapmanız yeterlidir.<br>
                6 - Gittigidiyor'da ürünler belirlenen gün sonra satıştan kalkmaktadır (60 gün), yayından kalktıktan sonra SATILAMAYANLAR tabına düşmektedir. Buradan tekrar satışa açabilir veya silebilirsiniz.<br>
                7 - GG Status durumunu aktif ettiğimizde önceden ürün gönderimini de aynı anda yapmaktaydı bu işlev iptal edildi, artık sadece gittigidiyora ürünün gönderilip gönderilmeyeceğini belirlemek için kullanılmakta.
              </p>
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="well">
                  <div class="row">
                      <div class="col-md-3">
                        <div class="form-group">
                          <label>Ürün Adı (Otm. Tamamlama)</label>
                          <input type="text" class="form-control input-sm" name="filter_name" value="<?php echo $filter_name; ?>">
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label>Ürün Kodu (Otm. Tamamlama)</label>
                          <input type="text" class="form-control input-sm" name="filter_model" value="<?php echo $filter_model; ?>">
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label>Kategori (Otm. Tamamlama)</label>
                          <input type="text" class="form-control input-sm" name="filter_category" value="<?php echo $filter_category; ?>">
                          <input type="hidden" name="filter_category_id" value="<?php echo $filter_category_id; ?>">
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label>Ürün Markası (Otm. Tamamlama)</label>
                          <input type="text" class="form-control input-sm" name="filter_manufacturer" value="<?php echo $filter_manufacturer; ?>">
                          <input type="hidden" class="form-control input-sm" name="filter_manufacturer_id" value="<?php echo $filter_manufacturer_id; ?>">
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label>Ürün Durumu</label>
                          <select name="filter_status" class="form-control input-sm">
                            <option value="">Tümü</option>
                            <?php if($filter_status == "1"){ ?>
                              <option value="1" selected="selected">Açık</option>
                            <?php } else { ?>
                              <option value="1">Açık</option>
                            <?php } ?>
                            <?php if($filter_status == "0"){ ?>
                              <option value="0" selected="selected">Kapalı</option>
                            <?php } else { ?>
                              <option value="0">Kapalı</option>
                            <?php } ?>

                          </select>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label>Eşleşme Durumu</label>
                          <select name="filter_eslesme" class="form-control input-sm">
                            <option value="">Tümü</option>
                            <option value="1" <?php if($filter_eslesme == "1"){ echo 'selected'; } ?>>Eşleştirilenler</option>
                            <option value="3" <?php if($filter_eslesme == "3"){ echo 'selected'; } ?>>Eşleştirilmemişler</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label>Stok Durumu</label>
                          <input type="text" name="filter_quantity" value="<?php echo $filter_quantity; ?>" class="form-control input-sm">
                        </div>
                      </div>
                  </div>
                  <button class="btn btn-primary btn-sm" id="button-filter">Ürünleri Getir</button>
              </div>
              <div class="btn-group">
                  <button type="button" class="btn btn-info allsend secilenlerigonder">Seçilenleri Gönder</button>
                  <button type="button" class="btn btn-warning allsend"  onclick="productsend('index.php?route=gittigidiyor/product/sendProduct&<?php echo $token_link; ?>')">Tümünü Gönder</button>
              </div>
              <hr style="margin-top: 5px; margin-bottom: 5px;">
              <div class="btn-group">
                  <a class="btn btn-primary <?php if($status == 'A'){ echo 'active'; } ?>" href="<?php echo $prodlink; ?>&status=A">Aktif Satışlar</a>
                  <a class="btn btn-primary <?php if($status == 'L'){ echo 'active'; } ?>" href="<?php echo $prodlink; ?>&status=L">Listelenmeye Hazır Ürünler</a>
                  <a class="btn btn-primary <?php if($status == 'U'){ echo 'active'; } ?>" href="<?php echo $prodlink; ?>&status=U">Satılmayan</a>
                  <a class="btn btn-primary <?php if($status == 'R'){ echo 'active'; } ?>" href="<?php echo $prodlink; ?>&status=R">Yeniden Listelenenler</a>
                  <a class="btn btn-info" href="<?php echo $gonderilmemis; ?>">Gönderilmemiş Ürünler</a>
              </div>
           <form id="products-form" method="post">
           <table class="table table-bordered">
              <thead>
                <td><input type="checkbox" onclick="$('input[name*=\'prodids\']').prop('checked', this.checked);" /></td>
                <th>RESİM</th>
                <th>ÜRÜN ADI</th>
                <th>MODEL</th>
                <th>FİYAT</th>
                <th>STOK</th>
                <th>GG KATEGORİ</th>
                <th>GG DURUM</th>
                <th>ORAN</th>
                <th>ÖZELLİKLER</th>
                <th>İŞLEMLER</th>
              </thead>
              <tbody>
                <?php if($products){ ?>
                    <?php foreach($products as $product){ ?>
                    <tr>
                      <td><input type="checkbox" name="prodids[]" value="<?php echo $product['product_id']; ?>"></td>
                      <td><img src="<?php echo $product['image']; ?>"></td>
                      <td><?php echo $product['name']; ?></td>
                      <td><?php echo $product['model']; ?></td>
                      <td>
                        <?php if($product['special']){ ?>
                          <span style="text-decoration: line-through;"><?php echo $product['price']; ?></span><br/>
                          <div class="text-danger"><?php echo $product['special']; ?></div>
                        <?php } else { ?>
                          <?php echo $product['price']; ?>
                        <?php } ?>
                      </td>
                      <td>
                        <?php if($product['quantity'] <= 0){ ?>
                        <span class="label label-warning"><?php echo $product['quantity']; ?></span>
                        <?php } elseif($product['quantity'] <= 5){ ?>
                        <span class="label label-danger"><?php echo $product['quantity']; ?></span>
                        <?php } else { ?>
                        <span class="label label-success"><?php echo $product['quantity']; ?></span>
                        <?php } ?>
                      </td>
                      <td>
                        <?php if($product['gg']['gg_catid'] != '0' and !empty($product['gg']['gg_catid'])){ ?>
                        <a href="#" class="eslestirme" data-type="typeaheadjs" data-pk="<?php echo $product['product_id']; ?>" data-value="<?php echo $product['gg']['gg_catid']; ?>|<?php echo $product['gg']['gg_catname']; ?>" data-url="index.php?route=gittigidiyor/product/cateslestir&<?php echo $token_link; ?>" data-title="Lütfen Kategori Arayın"></a>
                        <?php } else { ?>
                        <a href="#" class="eslestirme" data-type="typeaheadjs" data-pk="<?php echo $product['product_id']; ?>" data-value="" data-url="index.php?route=gittigidiyor/product/cateslestir&<?php echo $token_link; ?>" data-title="Lütfen Kategori Arayın"></a>
                        <?php } ?>
                      </td>
                      <td>
                        <div class="togselect">
                          <input type="checkbox" class="gproductstatus" value="1" name="gcategorystatus[]" data-value="<?php echo $product['product_id']; ?>" type="checkbox" data-toggle="toggle" data-size="mini" <?php if(isset($product['gg']) and $product['gg']['gg_salestatus'] == 1){ echo 'checked'; } ?>>
                        </div>
                      </td>
                      <td>
                      <?php if($product['gg']['gg_commission'] != 0){ ?>

                      <a href="#" class="komisyon" data-type="text" data-pk="<?php echo $product['product_id']; ?>" data-url="index.php?route=gittigidiyor/product/changecomission&<?php echo $token_link; ?>" data-title="Kategori Komisyon Oranını Girin"><?php echo $product['gg']['gg_commission']; ?></a>
                      <?php } else { ?>
                      <a href="#" class="komisyon" data-type="text" data-pk="<?php echo $product['product_id']; ?>" data-url="index.php?route=gittigidiyor/product/changecomission&<?php echo $token_link; ?>" data-title="Kategori Komisyon Oranını Girin"></a>
                      <?php } ?>
                      </td>
                      <td class="attr_<?php echo $product['product_id']; ?>">
                        <?php if($product['gg']['gg_catid'] != '0' and !empty($product['gg']['gg_catid'])){ ?><a data-ocid="<?php echo $product['product_id']; ?>"  data-ggid="<?php echo $product['gg']['gg_catid']; ?>" class="btn btn-xs btn-warning setattr">Özellikleri Ayarla</a><?php } else { ?>
                        Önce Kategoriyi Eşleştirin
                        <?php } ?>
                      </td>
                      <td width="140">
                        <?php if($product['gg']['gg_id'] != 0){ ?>
                        <a class="btn btn-success btn-xs btn-block" href="https://urun.gittigidiyor.com/<?php echo $product['gg']['gg_id']; ?>" target="_blank"><i class="fa fa-external-link" aria-hidden="true"></i> Ürün GittiGidiyor Linki</a>
                        <?php } ?>
                        <a class="btn btn-warning btn-xs btn-block editprod" data-prodid="<?php echo $product['product_id']; ?>" data-toggle="tooltip" data-original-title="Ürün Title, Ürün Alt Başlık ve Açıklamasını Özelleştirin!"><i class="fa fa-edit"></i> Bilgi Düzenle</a>
                        <a onclick="productsend('index.php?route=gittigidiyor/product/sendProduct&<?php echo $token_link; ?>&product_id=<?php echo $product['product_id']; ?>', '<?php echo $product['product_id']; ?>')" class="btn btn-info btn-block btn-xs <?php echo $product['product_id']; ?>btnsend">Ürünü Gönder</a>
                        <?php if($product['gg']['gg_id'] != 0){ ?>
                        <a class="btn btn-danger btn-xs btn-block  <?php echo $product['product_id']; ?>btnsend" onclick="productsend('index.php?route=gittigidiyor/product/delProduct&<?php echo $token_link; ?>&product_id=<?php echo $product['product_id']; ?>', '<?php echo $product['product_id']; ?>')"><i class="fa fa-trash" aria-hidden="true"></i> Ürün Sil</a>
                        <?php } ?>
                      </td>
                    </tr>
                    <?php } ?>
                 <?php } else { ?>
                <tr>
                  <td colspan="9"><p style="text-align: center;">Henüz Bir Ürününüz Yok</p></td>
                </tr>
                <?php } ?>
              </tbody>
           </table>
         </form>
           <div class="row">
            <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
            <div class="col-sm-6 text-right"><?php echo $results; ?></div>
          </div>
      </div>
  </div>
</div>
<!-- attr modal -->
<div class="modal fade" id="attrmodal" tabindex="-1" role="dialog" aria-labelledby="attrmodallabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Ürün Özelliklerini Ata</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Vazgeç</button>
        <button type="button" class="btn btn-primary saveattr">Değişiklikleri Kaydet</button>
      </div>
    </div>
  </div>
</div>
<!-- description modal -->
<div class="modal fade" id="descmodal" tabindex="-1" role="dialog" aria-labelledby="descmodallabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Ürün Bilgileri Özelleştir</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Vazgeç</button>
        <button type="button" class="btn btn-primary savedesc">Değişiklikleri Kaydet</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript" src="view/javascript/summernote/summernote.js"></script>
<link href="view/javascript/summernote/summernote.css" rel="stylesheet" />
<script type="text/javascript" src="view/javascript/summernote/opencart.js"></script>
<script type="text/javascript">
    function productsend(url, product_id) {
      $.ajax({
        url: url,
        type: 'post',
        dataType: 'json',
        beforeSend: function() {
          if(product_id != null){
               $('.'+product_id+'btnsend').button('loading');
          } else {
              $('.allsend').button('loading');
          }
        },
        success: function(json) {
            $('.alert, .text-danger, .progress').remove();
            if (json.status == 0) {
                $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json.msg + '</div>');
            }
            if (json.status == 1){
                $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i>  ' + json.msg + '</div>');
            }
            if(product_id != null){
               $('.'+product_id+'btnsend').button('reset');
            } else {
               $('.allsend').button('reset');
            }
            if(json['next']){
              productsend(json['next']);
            } else {
               $('.allsend').button('reset');
            }

        }
      });
    }

    $(document).on('click', '.secilenlerigonder', function(){
      $('.allsend').button('loading');
       $.ajax({
          url: 'index.php?route=gittigidiyor/product/secilenlerigonder&<?php echo $token_link; ?>',
          type: 'post',
          dataType: 'html',
          data : $('#products-form').serialize(),
          success: function(json) {
              $('#content > .container-fluid').prepend(json);
              $('.allsend').button('reset');
          }
        });
    });

    $(document).on('click', '.savedesc', function(){
      $('textarea[name="gg_description"]').html($('.summernote').code());

       $.ajax({
          url: 'index.php?route=gittigidiyor/product/descsave&<?php echo $token_link; ?>',
          type: 'post',
          dataType: 'json',
          data : $('#descform').serialize(),
          success: function(json) {
             if(json.status == 1){
                $('#descmodal').modal('hide');
                $('#descmodal .modal-body').html('');
                $.toast({heading: 'Başarılı',text: json.msg, position: 'top-right',loader: false,allowToastClose: false,showHideTransition: 'slide',icon: 'success'});
             }
          }
        });
    });

    $(document).on('click', '.saveattr', function(){
       $.ajax({
          url: 'index.php?route=gittigidiyor/product/attrsave&<?php echo $token_link; ?>',
          type: 'post',
          dataType: 'json',
          data : $('#attrform').serialize(),
          success: function(json) {
             if(json.status == 1){
                $('#attrmodal').modal('hide');
                $('#attrmodal .modal-body').html('');
                $.toast({heading: 'Başarılı',text: json.msg, position: 'top-right',loader: false,allowToastClose: false,showHideTransition: 'slide',icon: 'success'});
             }
          }
        });
    });

    $(document).on('click','.setattr', function(e){
        var ocid = $(this).data('ocid');
        var ggid = $(this).data('ggid');
        $( "#attrmodal .modal-body" ).load( "index.php?route=gittigidiyor/product/getattr&<?php echo $token_link; ?>&ocid="+ocid+"&ggid="+ggid, function() {
           $('#attrmodal').modal('show');
           e.preventDefault();
        });
    });

    $(document).on('click', '.editprod', function(){
        var prodid = $(this).data('prodid');
        $( "#descmodal .modal-body" ).load( "index.php?route=gittigidiyor/product/getdesc&<?php echo $token_link; ?>&product_id="+prodid, function() {
           $('.summernote').summernote({height: 200});
           $('#descmodal').modal('show');
           e.preventDefault();
        });
    });

    $('.eslestirme').editable({
        mode : 'inline',
        typeahead: {
            remote: 'index.php?route=gittigidiyor/category/ggkategoriara&<?php echo $token_link; ?>&filter_name=%QUERY',
            displayKey: 'name',
            valueKey : 'name',
            display: function(item){ return item.name }
        }
    }).on('save', function(e, params) {
      console.log(params);
        if(params.newValue == ''){
          $.toast({heading: 'Başarılı',text: 'Eşleştirme Silindi!', position: 'top-right',loader: false,allowToastClose: false,showHideTransition: 'slide',icon: 'success'});
          $('.attr_'+params.response.product_id).html('Önce Kategoriyi Eşleştirin');
        } else {
          $.toast({heading: 'Başarılı',text: params.newValue +' Başarıyla Eşleşti', position: 'top-right',loader: false,allowToastClose: false,showHideTransition: 'slide',icon: 'success'});
          $('.attr_'+params.response.product_id).html('<a data-ocid="'+params.response.product_id+'"  data-ggid="'+params.response.ggcategory+'" class="btn btn-xs btn-warning setattr">Özellikleri Ayarla</a>');
        }
    });

    $('.togselect input[type=checkbox]').bootstrapToggle({
        on: '<i class="fa fa-check" aria-hidden="true"></i>',
        off: '<i class="fa fa-times" aria-hidden="true"></i>'
    });

    $('.gproductstatus').on('change', function(){
      var product_id = $(this).data('value');
      if ($(this).is(':checked')) { var status = 1; } else { var status = 2; }
      $.ajax({
        url: 'index.php?route=gittigidiyor/product/changeprodstatus&<?php echo $token_link; ?>&product_id='+product_id+'&status='+status,
        dataType: 'json',
        success: function(json) {
            if(json.status == 1){
                $.toast({heading: 'Başarılı', text: json.msg, position: 'top-right',loader: false,allowToastClose: false,showHideTransition: 'slide',icon: 'success'});
            }
            if(json.status == 0){
                $.toast({heading: 'Hata', text: json.msg, position: 'top-right',loader: false,allowToastClose: false,showHideTransition: 'slide',icon: 'danger'});
            }
        }
      });
    });


    $(document).ready(function() {


        $('.komisyon').editable({mode : 'inline', inputclass: 'intclass input-sm'});
    });

    $('#button-filter').on('click', function() {
  var url = 'index.php?route=gittigidiyor/product&<?php echo $token_link; ?>';
  var filter_name = $('input[name=\'filter_name\']').val();
  if (filter_name) {
    url += '&filter_name=' + encodeURIComponent(filter_name);
  }

  var filter_model = $('input[name=\'filter_model\']').val();
  if (filter_model) {
    url += '&filter_model=' + encodeURIComponent(filter_model);
  }

  var filter_eslesme = $('select[name=\'filter_eslesme\']').val();
  if (filter_eslesme) {
    url += '&filter_eslesme=' + encodeURIComponent(filter_eslesme);
  }

  var filter_category = $('input[name=\'filter_category\']').val();
  if (filter_category) {
    url += '&filter_category=' + encodeURIComponent(filter_category);
  } else {
    $('input[name=\'filter_category_id\']').val('');
  }
  var filter_category_id = $('input[name=\'filter_category_id\']').val();
  if (filter_category_id) {
    url += '&filter_category_id=' + encodeURIComponent(filter_category_id);
  }



  var filter_manufacturer = $('input[name=\'filter_manufacturer\']').val();
  if (filter_manufacturer) {
    url += '&filter_manufacturer=' + encodeURIComponent(filter_manufacturer);
  } else {
    $('input[name=\'filter_manufacturer_id\']').val('');
  }
  var filter_manufacturer_id = $('input[name=\'filter_manufacturer_id\']').val();
  if (filter_manufacturer_id) {
    url += '&filter_manufacturer_id=' + encodeURIComponent(filter_manufacturer_id);
  }

  var filter_price = $('input[name=\'filter_price\']').val();
  if (filter_price) {
    url += '&filter_price=' + encodeURIComponent(filter_price);
  }

  var filter_quantity = $('input[name=\'filter_quantity\']').val();
  if (filter_quantity) {
    url += '&filter_quantity=' + encodeURIComponent(filter_quantity);
  }

  var filter_status = $('select[name=\'filter_status\']').val();
  if (filter_status) {
    url += '&filter_status=' + encodeURIComponent(filter_status);
  }

  var filter_ggstatus = $('select[name=\'filter_ggstatus\']').val();
  if (filter_ggstatus) {
    url += '&filter_ggstatus=' + encodeURIComponent(filter_ggstatus);
  }

  location = url;
});

$('input[name=\'filter_name\']').autocomplete({
  'source': function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/product/autocomplete&<?php echo $token_link; ?>&filter_name=' +  encodeURIComponent(request),
      dataType: 'json',
      success: function(json) {
        response($.map(json, function(item) {
          return {
            label: item['name'],
            value: item['product_id']
          }
        }));
      }
    });
  },
  'select': function(item) {
    $('input[name=\'filter_name\']').val(item['label']);
  }
});

// Category
$('input[name=\'filter_category\']').autocomplete({
  minLength: 4,
  'source': function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/category/autocomplete&<?php echo $token_link; ?>&filter_name=' +  encodeURIComponent(request),
      dataType: 'json',
      success: function(json) {
        response($.map(json, function(item) {
          return {
            label: item['name'],
            value: item['category_id']
          }
        }));
      }
    });
  },
  'select': function(item) {
    $('input[name=filter_category]').val(item['label']);
    $('input[name=filter_category_id]').val(item['value']);
  }
});

$('input[name=\'filter_model\']').autocomplete({
  'source': function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/product/autocomplete&<?php echo $token_link; ?>&filter_model=' +  encodeURIComponent(request),
      dataType: 'json',
      success: function(json) {
        response($.map(json, function(item) {
          return {
            label: item['model'],
            value: item['product_id']
          }
        }));
      }
    });
  },
  'select': function(item) {
    $('input[name=\'filter_model\']').val(item['label']);
  }
});

$('input[name=\'filter_manufacturer\']').autocomplete({
  'source': function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/manufacturer/autocomplete&<?php echo $token_link; ?>&filter_name=' +  encodeURIComponent(request),
      dataType: 'json',
      success: function(json) {
        response($.map(json, function(item) {
          return {
            label: item['name'],
            value: item['manufacturer_id']
          }
        }));
      }
    });
  },
  'select': function(item) {
    $('input[name=\'filter_manufacturer\']').val(item['label']);
    $('input[name=\'filter_manufacturer_id\']').val(item['value']);
  }
});
</script>
<?php echo $footer; ?>
