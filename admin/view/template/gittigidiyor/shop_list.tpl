<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <h1><?php echo $heading_title2; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid" id="content">
    <ul class="nav nav-tabs" role="tablist">
        <?php foreach ($links as $link) { ?>
        <li class="<?php echo $link['css']; ?>"><a href="<?php echo $link['link']; ?>"><b><?php echo $link['text']; ?></b></a></li>
        <?php } ?>
    </ul>
    <div class="panel panel-warning">
        <div class="panel-heading"><?php echo $heading_title; ?></div>
        <div class="panel-body">
             <div class="row">
                <h4 class="col-lg-12 col-md-12 col-sm-12">GİTTİGİDİYOR.COM DÜKKAN YÖNETİMİ</h4>
                <p class="col-lg-12 col-md-12 col-sm-12">Sistemimiz birden fazla mağaza ekleme ve birden çok mağzaya ürün göndermeyi desteklemektedir. Bu bölümden mağazalarınızı ekleyebilir ve mağazalar için varsayılan değerleri tanımlayabilirsiniz.</p>
            </div>
            <hr style="margin-top: 5px; margin-bottom: 5px;">
            <table class="table table-bordered">
                <thead>
                  <th>Mağaza Adı</th>
                  <th>Api Key</th>
                  <th>Api Secret</th>
                  <th>Fiyat Değişim</th>
                  <th>Fiyat Değişim Değeri</th>
                  <th>İşlemler</th>
                </thead>
                <tbody>
                  <?php if($shops){ ?>
                      <?php foreach($shops as $sho){ ?>
                      <tr>
                        <td><?php echo $sho['name']; ?></td>
                        <td><?php echo $sho['apikey']; ?></td>
                        <td><?php echo $sho['secretkey']; ?></td>
                        <td>
                          <?php if($sho['difference_type'] == "0"){ echo '-- İşlem Yapma --'; } ?>
                          <?php if($sho['difference_type'] == "yuzde"){ echo '% Olarak Arttır'; } ?>
                          <?php if($sho['difference_type'] == "sabit"){ echo 'Sabit Olarak Arttır'; } ?>
                          <?php if($sho['difference_type'] == "yuzdeazalt"){ echo '% olarak azalt'; } ?>
                          <?php if($sho['difference_type'] == "sabitazalt"){ echo 'Sabit olarak azalt'; } ?>
                        </td>
                        <td><?php echo $sho['difference_value']; ?></td>
                        <td>
                          <a class="btn btn-xs btn-info" href="index.php?route=gittigidiyor/shops/edit&user_token=<?php echo $user_token; ?>&shop_id=<?php echo $sho['id']; ?>"><i class="fa fa-edit"></i> Düzenle</a>
                          <a class="btn btn-xs btn-danger" href="index.php?route=gittigidiyor/shops/delete&user_token=<?php echo $user_token; ?>&shop_id=<?php echo $sho['id']; ?>" onclick="return confirm('Mağazayı silerseniz, mağazaya ait tüm ürünler, siparişler ve diğer verilerde silinir, emin misiniz ?')"><i class="fa fa-trash"></i> Sil</a>
                        </td>
                      </tr>
                      <?php } ?>
                  <?php } else { ?>
                  <tr>
                    <td colspan="6">
                      <center>
                        <p>Henüz bir gittigidiyor dükkanı eklemediniz, hemen işlere koyulmak için bir mazağa ekle!</p><br>
                        <a class="btn btn-primary" href="<?php echo $add_shop; ?>"><i class="fa fa-plus"></i> Mağaza Ekleyin</a>
                      </center>
                    </td>
                  </tr>
                  <?php } ?>
                </tbody>
            </table>
            <hr style="margin-top: 5px;">
            <a class="btn btn-primary" href="<?php echo $add_shop; ?>"><i class="fa fa-plus" aria-hidden="true"></i> MAĞAZA EKLE</a>
      </div>
  </div>
</div>
<?php echo $footer; ?>