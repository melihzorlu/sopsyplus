<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <h1><?php echo $heading_title2; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid" id="content">
    <ul class="nav nav-tabs" role="tablist">
        <?php foreach ($links as $link) { ?>
        <li class="<?php echo $link['css']; ?>"><a href="<?php echo $link['link']; ?>"><b><?php echo $link['text']; ?></b></a></li>
        <?php } ?>
    </ul>
    <div class="panel panel-warning">
        <div class="panel-heading"><?php echo $heading_title; ?></div>
        <div class="panel-body">
            <div class="row">
                <h4 class="col-lg-12 col-md-12 col-sm-12">GİTTİGİDİYOR KATEGORİ İŞLEMLERİ</h4>
                <p class="col-lg-12 col-md-12 col-sm-12">Bu kısımdan gittigidiyor kategorilerini, alt kategorilerini ve kategori özelliklerini indirebilir, güncelleyebilirsiniz. İşlemlerin daha hızlı olması açısından tüm gittigidiyor kategorileri veritabanına kayıt edilmektedir.</p>
            </div>
            <hr style="margin-top: 5px; margin-bottom: 5px;">
                <a class="btn btn-primary" href="<?php echo $select_root; ?>"><i class="fa fa-check"></i> GG ANA KATEGORİLERİ SEÇ</a>
                <a class="btn btn-danger downparent" onclick="donwload_ajax('index.php?route=gittigidiyor/category/download_parent&<?php echo $token_link; ?>');"><i class="fa fa-download"></i> TÜM ALT KATEGORİLERİ İNDİR</a>
      
            <hr style="margin-top: 5px;">
            <?php if($success){ ?>
            <div class="alert alert-success"><?php echo $success; ?></div>
            <?php } ?>
            <table class="table table-bordered">
              <thead>
                <th>GG KATEGORİ CODE</th>
                <th>KATEGORİ ADI</th>
                <th>SEÇENEK VAR MI</th>
                <th><span data-toggle="tooltip" title="" data-original-title="Kategori durumunu kapatırsanız o kategori ile eşleşmiş kategorilere ait ürünler ve ürünler gönderilmez!">DURUM <i class="fa fa-question-circle"></i></span></th>
              </thead>
              <tbody>
                <?php if($categories){ ?>
                  <?php foreach($categories as $category){ ?>
                  <tr>
                    <td><?php echo $category['category_code']; ?></td>
                    <td><?php echo $category['name']; ?></td>
                    <td>
                      <?php if($category['variant'] == '1'){ ?>
                      <a class="btn btn-xs btn-warning secenekesitle" data-categorycode="<?php echo $category['category_code']; ?>">Seçenekleri Eşitle</a>
                      <?php } else { ?>
                      -- Seçenek Yok --
                      <?php } ?>
                    </td>
                    <td class="togselect">
                      <input type="checkbox" class="gcategorystatus" value="1" name="gcategorystatus[]" data-value="<?php echo $category['category_code']; ?>" type="checkbox" data-toggle="toggle" data-size="mini" <?php if($category['status'] == 1){ echo 'checked'; }  ?>>
                    </td>
                  </tr>
                  <?php } ?>
                <?php } else { ?>
                <tr>
                  <td colspan="5"><center>Henüz Bir Gittigidiyor Kategoriniz Yok</center></td>
                </tr>
                <?php } ?>
                
              </tbody>
          </table>
          <div class="row">
            <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
            <div class="col-sm-6 text-right"><?php echo $results; ?></div>
          </div>
        </div>
    </div>
</div>
<div class="modal fade" id="secenekmodal" tabindex="-1" role="dialog" aria-labelledby="secenekmodalmodallabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Seçenekleri Eşitleyin</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Vazgeç</button>
        <button type="button" class="btn btn-primary seceneksave">Değişiklikleri Kaydet</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
 $('.togselect input[type=checkbox]').bootstrapToggle({
        on: '<i class="fa fa-check" aria-hidden="true"></i>',
        off: '<i class="fa fa-times" aria-hidden="true"></i>'
    });

    $(document).on('click', '.seceneksave', function(){
         $.ajax({
            url: 'index.php?route=gittigidiyor/category/seceneksave&<?php echo $token_link; ?>',
            type: 'post',
            dataType: 'json',
            data : $('#secenekform').serialize(),
            success: function(json) {
               if(json.status == 1){
                  $('#secenekmodal').modal('hide');
                  $('#secenekmodal .modal-body').html('');
                  $.toast({heading: 'Başarılı',text: json.msg, position: 'top-right',loader: false,allowToastClose: false,showHideTransition: 'slide',icon: 'success'});
               }
            }
          });
    });

    $(document).on('change', '.secenek_secin', function(){
         var specid = $(this).data('specid');
         var optionid = $(this).val();
         $.ajax({
          url: 'index.php?route=gittigidiyor/category/getoptionvalue&<?php echo $token_link; ?>&optionid='+optionid,
          dataType: 'html',
          success: function(results) {   
             $(document).find('.select_spec_'+specid).html(results);
          }
        });
    });
  
    $(document).on('click','.secenekesitle', function(e){
        var categorycode = $(this).data('categorycode');
        $( "#secenekmodal .modal-body" ).load( "index.php?route=gittigidiyor/category/getsecenek&<?php echo $token_link; ?>&categorycode="+categorycode, function() {
           $('#secenekmodal').modal('show');
           e.preventDefault();
        });
    });

$('.ncategorystatus').on('change', function(){
  var category_code = $(this).data('value');
  if ($(this).is(':checked')) {
    var status = 1;
  } else {
    var status = 0;
  }
  
  $.ajax({
    url: 'index.php?route=gittigidiyor/category/changenstatus&<?php echo $token_link; ?>&category_code='+category_code+'&status='+status,
    dataType: 'json',
    success: function(json) {   
        if(json.status == 1){
            $.toast({heading: 'Başarılı',text: json.msg, position: 'top-right',loader: false,allowToastClose: false,showHideTransition: 'slide',icon: 'success'});
        }
    }
  });
});


function donwload_ajax(url) {
  $.ajax({
    url: url,
    type: 'post',
    dataType: 'json',
    beforeSend: function() {
      $('.downparent').button('loading');
    },
    complete: function() {
      $('.downparent').button('reset');
    },
    success: function(json) {
        $('.alert, .text-danger').remove();
        if (json.status == 0) {
            $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json.msg + '</div>');
        }
        if (json.status == 1){
            $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i>  ' + json.msg + '</div>');
        }
        donwload_ajax(json['next']);
    }
  });
}

$('.togselect input[type=checkbox]').bootstrapToggle({
  on: '<i class="fa fa-check" aria-hidden="true"></i>',
  off: '<i class="fa fa-times" aria-hidden="true"></i>'
});
</script>
<?php echo $footer; ?>