<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <h1><?php echo $heading_title2; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid" id="content">
    <ul class="nav nav-tabs" role="tablist">
        <?php foreach ($links as $link) { ?>
        <li class="<?php echo $link['css']; ?>"><a href="<?php echo $link['link']; ?>"><b><?php echo $link['text']; ?></b></a></li>
        <?php } ?>
    </ul>
    <div class="panel panel-warning">
        <div class="panel-heading"><?php echo $heading_title; ?></div>
        <div class="panel-body">
            <p>Aşağıdaki Cron Linklerini İstediğiniz Zaman Aralıkları İle Tanımlayabilirsiniz. Bu linkler otomatik olarak ürün göndermenize ve siparilerinizi kontrol etmenize yardımcı olur.</p>
            <table class="table table-bordered table-striped">
              <thead>
                <th colspan="2">CRON LİNKLERİ</th>
              </thead>
              <tbody>
                 <?php foreach($shops as $shop){ ?>
                 <tr>
                   <td colspan="2" style="font-weight: bold;"><?php echo $shop['name']; ?> CRON LİNKLERİ</td>
                 </tr>
                 <tr>
                    <td><strong>ÜRÜN GÖNDER & GÜNCELLE</strong></td>
                    <td>index.php?route=api/ggcron/sendproduct&shop_id=<?php echo $shop['id']; ?></td>
                 </tr>
                  <tr>
                    <td><strong>SİPARİŞLERİ AL</strong></td>
                    <td>index.php?route=api/ggcron/checkorders&shop_id=<?php echo $shop['id']; ?></td>
                 </tr>
                 <?php } ?>
              </tbody>
            </table>
          
        </div>
  </div>
</div>
<?php echo $footer; ?>