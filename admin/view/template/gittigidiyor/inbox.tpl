<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <h1><?php echo $heading_title2; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid" id="content">
    <?php if($error){ ?>
    <div class="alert alert-danger"><?php echo $error; ?></div>
    <?php } ?>

    <ul class="nav nav-tabs" role="tablist">
        <?php foreach ($links as $link) { ?>
        <li class="<?php echo $link['css']; ?>"><a href="<?php echo $link['link']; ?>"><b><?php echo $link['text']; ?></b></a></li>
        <?php } ?>
    </ul>
    <div class="panel panel-warning">
        <div class="panel-heading"><?php echo $heading_title; ?></div>
        <div class="panel-body">
            <div class="rows">
                <h4 class="col-lg-12 col-md-12 col-sm-12">GİTTİGİDİYOR MESAJ KUTUSU</h4>
                <p class="col-lg-12 col-md-12 col-sm-12">Bu sayfadan gittigidiyor mesajlarınızı görebilir, yanıtlayabilirsiniz. Sağ kısımda bulunan filtrelemeyi kullanarak okunmamış mesajlarınızı veya tüm mesajlarınızı görebilirsiniz.</p>
            </div>
            <hr style="margin-top: 5px; margin-bottom: 5px;">
            <div class="rows">
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="pull-left">
                    
                  </div>
                  <div class="pull-right">
                      <select name="questatus" class="form-control questatus">
                          <option value="">Tümü</option>
                          <option value="1" <?php if($status == 1){ echo 'selected'; } ?>>Okunmamışlar</option>
                      </select>
                  </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <hr style="margin-top: 5px;">
          <table class="table table-bordered">
            <thead>
                <th style="width:400px !important;">KONU</th>
                <th>SORAN / EMAİL</th>
                <th>SORU DURUMU</th>
                <th>TARİH</th>
                <th>İŞLEMLER</th>
            </thead>
            <tbody>
                <?php


                if($questions){ ?>
                <?php foreach($questions as $que){
                 ?>

                <tr>
                  <td  <?php if($que->isRead != 1){ echo 'style="font-weight:bold; color: red;"'; } ?>><p style="width:400px; position:relative; white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;"><?php echo $que->title; ?></p></td>
                  <td <?php if($que->isRead != 1){ echo 'style="font-weight:bold; color: red;"'; } ?>><?php echo $que->from; ?> </td>
                  <td <?php if($que->isRead != 1){ echo 'style="font-weight:bold; color: red;"'; } ?>><?php if($que->isRead != 1){ ?> OKUNMADI <?php } else { ?> OKUNDU <?php } ?></td>
                  <td <?php if($que->isRead != 1){ echo 'style="font-weight:bold; color: red;"'; } ?>><?php echo $que->createDate; ?></td>
                  <td><a href="index.php?route=gittigidiyor/product/chats&<?php echo $token_link; ?>&mesage_id=<?php echo $que->conversationId; ?>" class="btn btn-xs btn-block btn-primary yanitla">MESAJLARI GÖR</a></td>
                </tr>
                <?php  } ?>
                <?php } else { ?>
                <tr>
                  <td colspan="5"><center>Mesajınız Yok</center></td>
                </tr>
                <?php } ?>
            </tbody>
             
          </table>
          <hr>
          <div class="row">
            <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
            <div class="col-sm-6 text-right"><?php echo $results; ?></div>
          </div>
      </div>
  </div>
</div>
<!-- Yanıt Modal -->
<div class="modal fade" id="yanitmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <form id="queform">
      <div class="modal-body">
        ...
      </div>
      </form>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Kapat</button>
        <button type="button" class="btn btn-sm btn-primary savequest">Yanıtı Kaydet</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
    $(document).on('click', '.btnyanitla', function(){
        $.ajax({
            url: 'index.php?route=gittigidiyor/product/savequestion&<?php echo $token_link; ?>',
            type: 'post',
            dataType: 'json',
            data : $('#queform').serialize(),
            success: function(json) {
               if(json.status == 1){
                  $('#yanitmodal').modal('hide');
                  $('#yanitmodal .modal-body').html('');
                  $.toast({heading: 'Başarılı',text: json.msg, position: 'top-right',loader: false,allowToastClose: false,showHideTransition: 'slide',icon: 'success'});
                  location.reload();
               } else {
                  $.toast({heading: 'Başarılı',text: json.msg, position: 'top-right',loader: false,allowToastClose: false,showHideTransition: 'slide',icon: 'danger'});
               }
            }
          });
      });

  $('.questatus').on('change', function(){
      var status = $(this).val();
      location.href = 'index.php?route=gittigidiyor/product/questions&<?php echo $token_link; ?>&status='+status;
  });

  $('.yanitla').on('click', function(){
    var title = $(this).data('title');
    var to = $(this).data('to');
    var queid = $(this).data('queid');
    var que = $(this).data('que');
     $.ajax({
            url: 'index.php?route=gittigidiyor/product/okundu&<?php echo $token_link; ?>',
            type: 'post',
            dataType: 'json',
            data : { queid : queid },
            success: function(json) {
                if(json.status == 1){
                    $('#yanitmodal .modal-title').html(title);
                    $('#yanitmodal .modal-body').html('<p>'+que+'</p><div class="form-group"><label>Yanıtınızı Girin</label><textarea class="form-control" name="yanit"></textarea></div><input type="hidden" name="queid" value="'+queid+'"><input type="hidden" name="title" value="'+title+'"><input type="hidden" name="to" value="'+to+'">');
                    $('#yanitmodal .modal-footer').html('<button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Kapat</button><button type="button" class="btn btn-sm btn-primary btnyanitla">Yanıtı Kaydet</button>');
                    $('#yanitmodal').modal('show');
                } else {
                    $('#yanitmodal .modal-title').html(title);
                    $('#yanitmodal .modal-body').html('<p>'+que+'<div class="alert alert-danger">Bu Mesaj Okundu Yapılamadı</div></p><div class="form-group"><label>Yanıtınızı Girin</label><textarea class="form-control" name="yanit"></textarea></div><input type="hidden" name="queid" value="'+queid+'">');
                    $('#yanitmodal .modal-footer').html('<button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Kapat</button><button type="button" class="btn btn-sm btn-primary btnyanitla">Yanıtı Kaydet</button>');
                    $('#yanitmodal').modal('show');
                }
               
            }
          });

   
  });
</script>
<?php echo $footer; ?>