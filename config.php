<?php


// HTTP
define('HTTP_SERVER', 'https://'.$_SERVER["HTTP_HOST"].'/');

// HTTPS
define('HTTPS_SERVER', 'https://'.$_SERVER["HTTP_HOST"].'/');

// DIR
define('DIR_APPLICATION', '/home/pazarmo/public_html/catalog/');
define('DIR_SYSTEM', '/home/pazarmo/public_html/system/');
define('DIR_IMAGE', '/home/pazarmo/public_html/image/');
define('DIR_LANGUAGE', '/home/pazarmo/public_html/catalog/language/');
define('DIR_TEMPLATE', '/home/pazarmo/public_html/catalog/view/theme/');
define('DIR_CONFIG', '/home/pazarmo/public_html/system/config/');
define('DIR_CACHE', '/home/pazarmo/public_html/system/storage/cache/');
define('DIR_DOWNLOAD', '/home/pazarmo/public_html/system/storage/download/');
define('DIR_LOGS', '/home/pazarmo/public_html/system/storage/logs/');
define('DIR_MODIFICATION', '/home/pazarmo/public_html/system/storage/modification/');
define('DIR_UPLOAD', '/home/pazarmo/public_html/system/storage/upload/');

define('DB_DRIVER', 'mpdo');
define('DB_HOSTNAME', 'localhost');

if($_SERVER["HTTP_HOST"]=='www.imrennikahsekeri.net'){

  define('DB_USERNAME', 'pazarmo_imren');
  define('DB_DATABASE', 'pazarmo_imren');
}elseif($_SERVER["HTTP_HOST"]=='www.butiklotus.com'){
  define('DB_USERNAME', 'pazarmo_lotus');
  define('DB_DATABASE', 'pazarmo_lotus');
}elseif($_SERVER["HTTP_HOST"]=='www.vanessamybabydress.sopsyplus.com'){
  define('DB_USERNAME', 'pazarmo_vanessa');
  define('DB_DATABASE', 'pazarmo_vanessa');
}elseif($_SERVER["HTTP_HOST"]=='www.salkimshop.sopsyplus.com'){
  define('DB_USERNAME', 'pazarmo_salkimshop');
  define('DB_DATABASE', 'pazarmo_salkimshop');
}elseif($_SERVER["HTTP_HOST"]=='www.yeniyasamglobal.sopsyplus.com'){
  define('DB_USERNAME', 'pazarmo_yeniyasamglobal');
  define('DB_DATABASE', 'pazarmo_yeniyasamglobal');
}elseif($_SERVER["HTTP_HOST"]=='www.tema2.sopsyplus.com'){
  define('DB_USERNAME', 'pazarmo_db');
  define('DB_DATABASE', 'pazarmo_tema2');
}elseif($_SERVER["HTTP_HOST"]=='www.tema3.sopsyplus.com'){
  define('DB_USERNAME', 'pazarmo_db');
  define('DB_DATABASE', 'pazarmo_tema3');
}elseif($_SERVER["HTTP_HOST"]=='www.tema4.sopsyplus.com'){
  define('DB_USERNAME', 'pazarmo_db');
  define('DB_DATABASE', 'pazarmo_tema4');
}elseif($_SERVER["HTTP_HOST"]=='www.tema5.sopsyplus.com'){
  define('DB_USERNAME', 'pazarmo_db');
  define('DB_DATABASE', 'pazarmo_tema5');
}elseif($_SERVER["HTTP_HOST"]=='www.tema6.sopsyplus.com'){
  define('DB_USERNAME', 'pazarmo_db');
  define('DB_DATABASE', 'pazarmo_tema6');
}elseif($_SERVER["HTTP_HOST"]=='www.tema7.sopsyplus.com'){
  define('DB_USERNAME', 'pazarmo_db');
  define('DB_DATABASE', 'pazarmo_tema7');
}elseif($_SERVER["HTTP_HOST"]=='www.tema8.sopsyplus.com'){
  define('DB_USERNAME', 'pazarmo_db');
  define('DB_DATABASE', 'pazarmo_tema8');
}elseif($_SERVER["HTTP_HOST"]=='www.tema9.sopsyplus.com'){
  define('DB_USERNAME', 'pazarmo_db');
  define('DB_DATABASE', 'pazarmo_tema9');
}elseif($_SERVER["HTTP_HOST"]=='www.demo.magaza.sopsyplus.com'){
  define('DB_USERNAME', 'pazarmo_db');
  define('DB_DATABASE', 'pazarmo_demo.magaza');
}elseif($_SERVER["HTTP_HOST"]=='www.joujoubyzra.sopsyplus.com'){
  define('DB_USERNAME', 'pazarmo_db');
  define('DB_DATABASE', 'pazarmo_joujoubyzra');
}elseif($_SERVER["HTTP_HOST"]=='www.hobbidukkan.sopsyplus.com'){
  define('DB_USERNAME', 'pazarmo_db');
  define('DB_DATABASE', 'pazarmo_hobbidukkan');
}elseif($_SERVER["HTTP_HOST"]=='www.tema10.sopsyplus.com'){
  define('DB_USERNAME', 'pazarmo_db');
  define('DB_DATABASE', 'pazarmo_tema10');
}elseif($_SERVER["HTTP_HOST"]=='www.tema11.sopsyplus.com'){
  define('DB_USERNAME', 'pazarmo_db');
  define('DB_DATABASE', 'pazarmo_tema11');
}elseif($_SERVER["HTTP_HOST"]=='www.tema12.sopsyplus.com'){
  define('DB_USERNAME', 'pazarmo_db');
  define('DB_DATABASE', 'pazarmo_tema12');
}elseif($_SERVER["HTTP_HOST"]=='www.nurgamboc.com'){
  define('DB_USERNAME', 'pazarmo_db');
  define('DB_DATABASE', 'pazarmo_nurgamboc');
}elseif($_SERVER["HTTP_HOST"]=='www.miniepazar.sopsyplus.com'){
  define('DB_USERNAME', 'pazarmo_db');
  define('DB_DATABASE', 'pazarmo_miniepazar');
}elseif($_SERVER["HTTP_HOST"]=='www.bohomathilda.sopsyplus.com'){
  define('DB_USERNAME', 'pazarmo_db');
  define('DB_DATABASE', 'pazarmo_bohomathilda');
}elseif($_SERVER["HTTP_HOST"]=='www.aylinhomeland.sopsyplus.com'){
  define('DB_USERNAME', 'pazarmo_db');
  define('DB_DATABASE', 'pazarmo_aylinhomeland');
}elseif($_SERVER["HTTP_HOST"]=='www.pelinoll.sopsyplus.com'){
  define('DB_USERNAME', 'pazarmo_db');
  define('DB_DATABASE', 'pazarmo_pelinoll');
}elseif($_SERVER["HTTP_HOST"]=='www.lenaca.sopsyplus.com'){
  define('DB_USERNAME', 'pazarmo_db');
  define('DB_DATABASE', 'pazarmo_lenaca');
}elseif($_SERVER["HTTP_HOST"]=='www.madesignistanbul.sopsyplus.com'){
  define('DB_USERNAME', 'pazarmo_db');
  define('DB_DATABASE', 'pazarmo_madesignistanbul');
}elseif($_SERVER["HTTP_HOST"]=='www.minerervadecomn.sopsyplus.com'){
  define('DB_USERNAME', 'pazarmo_db');
  define('DB_DATABASE', 'pazarmo_minerervadecomn');
}elseif($_SERVER["HTTP_HOST"]=='www.ayushi.sopsyplus.com'){
  define('DB_USERNAME', 'pazarmo_db');
  define('DB_DATABASE', 'pazarmo_ayushi');
}elseif($_SERVER["HTTP_HOST"]=='www.papermore.sopsyplus.com'){
  define('DB_USERNAME', 'pazarmo_db');
  define('DB_DATABASE', 'pazarmo_papermore');
}elseif($_SERVER["HTTP_HOST"]=='www.leylabilen.sopsyplus.com'){
  define('DB_USERNAME', 'pazarmo_db');
  define('DB_DATABASE', 'pazarmo_leylabilen');
}elseif($_SERVER["HTTP_HOST"]=='www.silifkeorganik.com'){
  define('DB_USERNAME', 'pazarmo_db');
  define('DB_DATABASE', 'pazarmo_silifkeorganik');
}elseif($_SERVER["HTTP_HOST"]=='www.trtheordinary.sopsyplus.com'){
  define('DB_USERNAME', 'pazarmo_db');
  define('DB_DATABASE', 'pazarmo_trtheordinary');
}elseif($_SERVER["HTTP_HOST"]=='www.tontonemmi.com'){
  define('DB_USERNAME', 'pazarmo_db');
  define('DB_DATABASE', 'pazarmo_tontonemmi');
}elseif($_SERVER["HTTP_HOST"]=='www.kozmodepo.com'){
  define('DB_USERNAME', 'pazarmo_db');
  define('DB_DATABASE', 'pazarmo_kozmodepo');
}elseif($_SERVER["HTTP_HOST"]=='www.tatlisifa.com'){
  define('DB_USERNAME', 'pazarmo_db');
  define('DB_DATABASE', 'pazarmo_tatlisifa');
}else{
  define('DB_USERNAME', 'pazarmo_db');
  define('DB_DATABASE', 'pazarmo_db');
}

// DB


define('DB_PASSWORD', 'CtwE3V@!GI^B');

define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');


/*define('CACHE_HOSTNAME', '127.0.0.1');
define('CACHE_PORT', '11211');
define('CACHE_PREFIX', 'pazar_');*/
