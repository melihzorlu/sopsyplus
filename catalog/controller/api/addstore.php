<?php
class ControllerApiAddstore extends Controller {
  public function index(){
    $store_id = $this->request->get['store_id'];

    $data['config_name'] = 'Lotus Butik';
    $data['config_url'] = 'https://www.butiklotus.com';
    $data['config_ssl'] = 'https://www.butiklotus.com';

    $this->db->query("INSERT INTO " . DB_PREFIX . "store SET store_id='".$store_id."', name = '" . $this->db->escape($data['config_name']) . "', `url` = '" . $this->db->escape($data['config_url']) . "', `ssl` = '" . $this->db->escape($data['config_ssl']) . "'");

		//$store_id = $this->db->getLastId();

		// Layout Route
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "layout_route WHERE store_id = '0'");

		foreach ($query->rows as $layout_route) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "layout_route SET layout_id = '" . (int)$layout_route['layout_id'] . "', route = '" . $this->db->escape($layout_route['route']) . "', store_id = '" . (int)$store_id . "'");
		}

		$this->cache->delete('store');

  }
}
