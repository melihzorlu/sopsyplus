<?php
class ControllerApiXML extends Controller {

  public function index() {
    ini_set('memory_limit', '-1');
    ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);
    set_time_limit(0);

    $_POST['main_element'] = 'Products';

    ini_set('mysql.connect_timeout', 600);
    ini_set('default_socket_timeout', 600);
    $page = $this->request->get['Page'];
    $url = 'https://www.gimeravm.com/FaprikaXml/TZ3TZX/'.$page.'/';
    $xml = simplexml_load_file($url, "SimpleXMLElement", LIBXML_NOCDATA);
    $array = $xml;
    $xml2json=json_encode($array);
    $xmlparser = json_decode($xml2json,true);
    $products = $this->XML2Array($array);


    header('Content-type: text/xml');
header('Pragma: public');
header('Cache-control: private');
header('Expires: -1');
echo "<?xml version=\"1.0\" encoding=\"utf-8\"?>";

echo '<xml>';

    //print_r($products);
    $productdata=array();
    foreach($products as $key => $product){
      if(isset($product['Categories']['Category']['@attributes/Path']) && $product['Categories']['Category']['@attributes/Path']){
        if($product['@attributes/StockQuantity']>5){
      echo "<product>";
      echo '<name_tr><![CDATA['.$product["@attributes/Name"].']]></name_tr>';
      echo "<meta_title_tr><![CDATA[".$product['@attributes/Name']."]]></meta_title_tr>";
      if(isset($product['Specifications']['Specification/0']['@attributes/Value']) && $product['Specifications']['Specification/0']['@attributes/Value']){
        echo "<model><![CDATA[".$product['@attributes/ModelCode']."_".$product['Specifications']['Specification/0']['@attributes/Value']."]]></model>";
      }else{
        echo "<model><![CDATA[".$product['@attributes/ModelCode']."]]></model>";
      }

      echo "<sku><![CDATA[".$product['@attributes/Sku']."]]></sku>";
      echo "<jan><![CDATA[".$product['@attributes/ModelCode']."]]></jan>";
      if(isset($product['Specifications']['Specification/0']['@attributes/Value']) && $product['Specifications']['Specification/0']['@attributes/Value']){
        echo "<upc><![CDATA[name:".$product['Specifications']['Specification/0']['@attributes/Name']."|value:".$product['Specifications']['Specification/0']['@attributes/Value']."]]></upc>";
      }else{
        echo "<upc></upc>";
      }

      echo "<quantity><![CDATA[".$product['@attributes/StockQuantity']."]]></quantity>";
      echo "<price><![CDATA[".$product['@attributes/OldPrice']."]]></price>";
      if(isset($product['@attributes/Price']) && $product['@attributes/Price']>0){
        echo '<product_special><![CDATA[1:0:'.$product['@attributes/Price'].':0000-00-00:0000-00-00]]></product_special>';
      }

      echo "<tax><![CDATA[".$product['@attributes/Tax']."]]></tax>";
      echo "<category><![CDATA[".@$product['Categories']['Category']['@attributes/Path']."]]></category>";



      /*$productdata[$key]['name_tr']=$product['@attributes/Name'];
      $productdata[$key]['meta_title_tr']=$product['@attributes/Name'];
      $productdata[$key]['model']=$product['@attributes/ModelCode'];
      $productdata[$key]['sku']=$product['@attributes/Sku'];
      $productdata[$key]['quantity']=$product['@attributes/StockQuantity'];
      $productdata[$key]['price']=$product['@attributes/OldPrice'];
      $productdata[$key]['product_special']='1:0:'.$product['@attributes/Price'].':0000-00-00:0000-00-00';
      $productdata[$key]['tax']=$product['@attributes/Tax'];
      $productdata[$key]['category']=@$product['Categories']['Category']['@attributes/Path'];
      */
      if(isset($product['Pictures'])){
        $i=0;
        $additional_image='';
        foreach($product['Pictures'] as $picture){
          if($i==0){
            echo "<image><![CDATA[".@$picture['@attributes/Path']."]]></image>";
          }else{
            $additional_image.=$picture['@attributes/Path'].'|';
          }
          //$productdata[$key]['picture'][]=$picture['@attributes/Path'];
          $i++;
        }
        echo "<additional_images><![CDATA[".@rtrim($additional_image,'|')."]]></additional_images>";
      }
      $optiondata='';
      if(isset($product['Combinations'])){
        foreach($product['Combinations'] as $options){
          if(isset($options['Attributes']['Attribute']['@attributes/Value']) && $options['Attributes']['Attribute']['@attributes/Value']){
            if($options['@attributes/StockQuantity']>0){
              /*$option[]=array(
                'option_name' =>$options['Attributes']['Attribute']['@attributes/Name'],
                'option_value' =>$options['Attributes']['Attribute']['@attributes/Value'],
                'option_quantity' =>$options['@attributes/StockQuantity']
              );*/
              $optiondata.='select:'.$options['Attributes']['Attribute']['@attributes/Name'].':'.$options['Attributes']['Attribute']['@attributes/Value'].':+0.0000:'.$options['@attributes/StockQuantity'].':1:+0.00000000:1|';
            }
          }
          /*$productdata[$key]['options'][]=array(
            'option_sku'=>$options['@attributes/Sku'],
            'option_gtin'=>$options['@attributes/Gtin'],
            'option_quantity'=>$options['@attributes/StockQuantity'],
            'option_price'=>$options['@attributes/Price'],
            'option_name'=>$options['Attributes']['Attribute']['@attributes/Name'],
            'option_value'=>$options['Attributes']['Attribute']['@attributes/Value']);*/
        }
      }
      echo "<options><![CDATA[".@rtrim($optiondata,'|')."]]></options>";
      //$productdata[$key]['product_option']=rtrim($optiondata,'|');
      /*$productdata[$key]['specifications'] = array(
        'name'=> @$product['Specifications']['Specification/0']['@attributes/Name'],
        'value' => @$product['Specifications']['Specification/0']['@attributes/Value']
      );*/
      //echo $product['@attributes/Name']."<br>";
      echo "</product>";

        }

      }
    }

    echo '</xml>';

    //echo $productdata;


    //print_r($productdata);


  }



  public function XML2Array($xml) {
    $_POST['main_element'] = 'Products';
    $array = (array)$xml;
    $data = array();
    $prod = array();
    if (count($array) == 0) {
      $array = (string)$xml;
    }
    if (is_array($array)) {
      $i=0;
      foreach ($array as $key => $value) {
        if (is_object($value)) {
          if (strpos(get_class($value), 'SimpleXML') !== false) {
            $array[$key] = $this->XML2Array($value);
            if(is_array($array[$key])){
              foreach ($array[$key] as $key3 => $values2){
                  $data[$key][$key3] = $values2;
              }
            }
          }
        } else if (is_array($value)) {
          $array[$key] = $this->XML2Array($value);
          if(is_array($array[$key])){
            foreach ($array[$key] as $key2 => $values){
  						if($key==$_POST['main_element']){
  							$data[$key] = $values;
  						}else{
  							$data[$key.'/'.$key2] = $values;
  						}

            }
          }
        } else {
          $data[$this->clearkey($key)] = $value;
        }
      }
    }
    return $data;
  }

  public function clearkey($key){
    //$key = preg_replace('/[0-9]+/', '',$key);
    $key = preg_replace('/[^A-Za-z0-9\-]/', '', $key);
    $key = str_replace('@','',$key);
    return $key;
  }

}
