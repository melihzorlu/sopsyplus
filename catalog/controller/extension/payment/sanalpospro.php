<?php
//ini_set("display_errors", "on");
//error_reporting(E_ALL);
/*
 * Do not change following lines
 */

class ControllerExtensionPaymentSanalpospro extends Controller
{

	public function index()
	{
		$this->load->language('payment/sanalpospro');
		$this->document->addStyle('catalog/view/theme/default/stylesheet/sanalpospro_form.css');

		$data['text_instruction'] = $this->language->get('text_instruction');
		$data['text_description'] = $this->language->get('text_description');
		$data['text_payment'] = $this->language->get('text_payment');
		$data['text_loading'] = $this->language->get('text_loading');
		$data['button_confirm'] = $this->language->get('button_confirm');
		$data['bank'] = nl2br($this->config->get('sanalpospro_bank' . $this->config->get('config_language_id')));
		$data['button_url'] = $this->url->link('extension/payment/sanalpospro/paymentform', '', true);

		return $this->load->view('extension/payment/sanalpospro', $data);
	}

	public function paymentform()
	{

		$this->load->model('checkout/order');
		$this->load->model('setting/setting');
		include(DIR_APPLICATION . 'controller/extension/payment/sanalpospro/lib/class/inc.php');
		// Optional. Set the title of your web page
		$this->load->language('extension/payment/sanalpospro');
		$this->document->setTitle($this->language->get('text_title'));



		if (!isset($this->session->data['order_id']) OR ! $this->session->data['order_id']) {
			$data = array();
			$data['Request'] = $_REQUEST;
			$data['Server'] = $_SERVER;

			$cli = New SanalPosApiClient(1, 1, 'reportissue');
			$cli->validateRequest()
				->run($data)
				->getResponse();
			die("invalid cart params");
		}


		$error_message = false;
		$tr = EticTransaction::createTransaction($this);
		$order = $this->model_checkout_order->getOrder($this->session->data['order_id']);

		$mp = false;
		$this->document->addStyle(HTTPS_SERVER . 'catalog/view/theme/default/stylesheet/sanalpospro/payment.css');
		$this->document->addStyle(HTTPS_SERVER . 'catalog/view/theme/default/stylesheet/sanalpospro/pro-form.css');
		$this->document->addStyle(HTTPS_SERVER . 'catalog/view/theme/default/stylesheet/sanalpospro/jquery.card.css');
		$this->document->addScript(HTTPS_SERVER . 'catalog/view/javascript/sanalpospro/jquery.card.js');
		$this->document->addScript(HTTPS_SERVER . 'catalog/view/javascript/sanalpospro/jquery.payment.min.js');
		$this->document->addScript(HTTPS_SERVER . 'catalog/view/javascript/sanalpospro/pro.js');

		if (EticConfig::get("MASTERPASS_ACTIVE") == 'on') {
			include_once(DIR_APPLICATION . 'controller/extension/payment/sanalpospro/lib/EticsoftMasterPassLoader.php');
			$mp = new EticsoftMasterpass($tr);
			$mp->prepareUi();
			$this->document->addStyle(HTTPS_SERVER . 'catalog/view/theme/default/stylesheet/sanalpospro/masterpass.css');
		}

		$card_rates = EticInstallment::getRates((float) $tr->total_cart_fix);
		$restrictions = EticInstallment::getRestrictedProducts($tr->id_cart);
		if (is_array($restrictions) && !empty($restrictions))
			$card_rates = array();

		$sanalpospro_uri = HTTPS_SERVER . 'controller/extension/payment/sanalpospro/';
		$currency = Etictools::getCurrency($order['currency_code']);
		$curname = $order['currency_code'];
		$currency_default = 'TRY';
		$cards = $card_rates;
		$defaultins = EticInstallment::calcDefaultRate((float) $tr->total_cart_fix);
		$c_auto_currency = EticConfig::get('POSPRO_AUTO_CURRENCY');
		$c_min_inst_amount = (float) EticConfig::get('POSPRO_MIN_INST_AMOUNT');
		$auf = EticConfig::get('POSPRO_ORDER_AUTOFORM');

		// Breadcrumbs for the page
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('information/static')
		);

		// Get "heading_title" from language file
		$data['heading_title'] = $this->language->get('heading_title');

		// All the necessary page elements
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$sanalpospro_path = DIR_APPLICATION . 'controller/extension/payment/sanalpospro/';
		$form_path = $sanalpospro_path . 'lib/tool/payform.php';

		//print_r($this->language->all()); exit;

		if (EticConfig::get("MASTERPASS_ACTIVE") == 'on') {

			if (Etictools::getValue('mp_api_token') AND Etictools::getValue('mp_api_refno')) {
				$mpgw = new EticsoftMasterpassGateway($tr, Etictools::getValue('mp_api_refno'));
				$mpgw->apiPay();
				$tr = $mpgw->tr;
				if ($tr->result) {
					$this->completePayment($tr);
					return;
				}
				$error_message = $tr->result_code . ' ' . $tr->result_message;
				return include($form_path);
			}

			if (Etictools::getValue('mptd') AND Etictools::getValue('oid')) {
				$mpgw = new EticsoftMasterpassGateway($tr);
				$mpgw->tdValidate();
				$tr = $mpgw->tr;

				if ($tr->result) {
					$this->completePayment($tr);
					return;
				}
				$error_message = $tr->result_code . ' ' . $tr->result_message;
				return include($form_path);
			}
		}

		if (!Etictools::getValue('cc_number') AND ! Etictools::getValue('sprtdvalidate')) {
			return include($form_path);
		}

		$gateway = New EticGateway($tr->gateway);
		$lib_class_name = 'Eticsoft_' . $gateway->lib;
		$lib_class_path = $sanalpospro_path . '/lib/gateways/' . $gateway->lib . '/' . $lib_class_name . '.php';
		$tr->debug("Try to include  " . $lib_class_name, true);
		include_once($lib_class_path);
		if (Etictools::getValue('sprtdvalidate')) {
			// var_dump(EticSql::getRow('spr_transaction', 'id_cart', 62));
			// die("sadadsad");
			if ($exists = EticTransaction::getTransactionByCartId($this->session->data['order_id'])) {
				$tr->id_transaction = $exists['id_transaction'];
				$tr->__construct();
				$tr->exists = true;
			} else
				die("order not found");

			$lib = New $lib_class_name();
			$tr = $lib->tdValidate($tr);
			$tr->save();
		}
		else {

			$tr->createTransaction($this);

			$tr->debug("\n\n*********\n\n " . 'Form posted via ' . EticConfig::get('POSPRO_PAYMENT_PAGE'));

			if (!$tr->validateTransaction()) {

				$error_message = $tr->result_code . ' ' . $tr->result_message;
				return include($form_path);
			}
			$lib = New $lib_class_name();
			$tr = $lib->pay($tr);
			$tr->save();
			if ($tr->tds AND $tr->tds_echo) {
				echo $tr->tds_echo;
				return;
			}
		}
		if ($tr->result) {
			$this->completePayment($tr);
			return;
		}
		$error_message = $tr->result_code . ' ' . $tr->result_message;
		return include($form_path);
	}

	public function translate($txt)
	{
		$this->load->language('extension/payment/sanalpospro');
		$key = str_replace("'", "", $txt);
		$key = substr(strtolower(preg_replace('#[^a-z0-9\\-/]#i', '_', $txt)), 0, 70);
		return $this->language->get($key);

		return;

		$file_en = DIR_LANGUAGE . "en-gb/extension/payment/sanalpospro.php";
		$key = str_replace("'", "", $txt);
		$key = substr(strtolower(preg_replace('#[^a-z0-9\\-/]#i', '_', $txt)), 0, 70);
		$this->language->load('language/file');
		if ($this->language->get($key)) {
			$handle = fopen($file_en, "a+");
			$string = '$_[\'' . $key . '\'] = \'' . $txt . '\'; ' . "\n";
			fwrite($handle, $string);
			fclose($handle);
			return $txt;
		}
	}

	public function completePayment($tr)
	{
		$tr->id_order = $this->session->data['order_id'];
		$tr->save();
		$this->session->data['payment_method']['code'] = 'sanalpospro';
		//$this->model_checkout_order->addOrderHistory($this->session->data['order_id'], EticConfig::get('sanalpospro_order_status_id'), 'Sanalpos PRO! '.$tr->gateway.' '.$tr->installment);
		$this->response->redirect($this->url->link('checkout/success', '', 'SSL'));
		return;
	}
}
