<?php

class Eticsoft_paratika
{

	var $version = 210709;

    const API_URL = "https://vpos.paratika.com.tr/paratika/api/v2";
    const TEST_API_URL = "https://entegrasyon.paratika.com.tr/paratika/api/v2";
    const SECURE_URL = "https://vpos.paratika.com.tr/merchant/post/sale/";
    const TEST_SECURE_URL = "https://entegrasyon.paratika.com.tr/merchant/post/sale/";
    const SECURE_URL_3D = "https://vpos.paratika.com.tr/paratika/api/v2/post/sale3d/";
    const TEST_SECURE_URL_3D = "https://entegrasyon.paratika.com.tr/paratika/api/v2/post/sale3d/";

    public function pay($tr)
    {
        $secure_url = $tr->gateway_params->test_mode == "off" ? self::SECURE_URL : self::TEST_SECURE_URL;
        $secure_url_3d = $tr->gateway_params->test_mode == "off" ? self::SECURE_URL_3D : self::TEST_SECURE_URL_3D;

        $pay_url = $tr->gateway_params->tdmode == "off" ? $secure_url : $secure_url_3d;
        $api_url = $tr->gateway_params->test_mode == "off" ? self::API_URL : self::TEST_API_URL;

        $tr->boid = "PLUS-".$tr->id_transaction.rand();

        $parametersMap = array();
        $parametersMap['ACTION'] = 'SESSIONTOKEN';
        $parametersMap['MERCHANTPAYMENTID'] = $tr->boid;
        $parametersMap['CURRENCY'] = $tr->currency_code;
        $parametersMap['SESSIONTYPE'] = 'PAYMENTSESSION';
        $parametersMap['RETURNURL'] = $tr->ok_url;
        $parametersMap['MERCHANT'] = $tr->gateway_params->MERCHANT;
        $parametersMap['MERCHANTUSER'] = $tr->gateway_params->MERCHANTUSER;
        $parametersMap['MERCHANTPASSWORD'] = $tr->gateway_params->MERCHANTPASSWORD;
        $parametersMap['CUSTOMER'] = $tr->id_customer;
        $parametersMap['CUSTOMERNAME'] = $tr->customer_firstname." ".$tr->customer_lastname;
        $parametersMap['CUSTOMEREMAIL'] = $tr->customer_email;
        $parametersMap['CUSTOMERIP'] = $tr->cip;
        $parametersMap['CUSTOMERPHONE'] = $tr->customer_mobile;
        $parametersMap['NAMEONCARD'] = $tr->cc_name;
        $parametersMap['CARDPAN'] = $tr->cc_number;
        $parametersMap['CARDEXPIRY'] = str_pad($tr->cc_expire_month, 2, "0", STR_PAD_LEFT).'.'."20".str_pad(substr($tr->cc_expire_year, -2) ,2 ,"0", STR_PAD_LEFT);
        $parametersMap['CARDCVV'] = $tr->cc_cvv;

        $urunler = array();
        $total_cart = 0;

        foreach ($tr->product_list as $key => $product) {
            $item = array();
            $item["name"] = $product['name'];
            $item["description"] = $product['name'];
            $item["quantity"] = $product['quantity'];
            $item["amount"] = number_format($product['price'],2,".","");
            $total_cart += $item["amount"] * $item["quantity"];
            array_push($urunler, $item);
        }

        $cart_diff = $tr->total_cart - $total_cart;
        $pay_diff = ($tr->total_pay - $tr->total_cart) + $cart_diff;

        $item = array();
        $item["name"] = "Diğer";
        $item["description"] = "Diğer";
        $item["quantity"] = 1;
        $item["amount"] = number_format($pay_diff,2,".","");
        array_push($urunler, $item);

        $parametersMap['AMOUNT'] = number_format($tr->total_pay,2,".","");
        $parametersMap['ORDERITEMS'] = urlencode(json_encode($urunler));

        $requestData = $this->convertToRequestData($parametersMap);
        $request = curl_init();

        $this->getConnection($request, $api_url);
        $post_response = $this->getResponse($request, $requestData);


        $session_result = json_decode($post_response);

        if ($session_result->responseCode == "00") {

            $form = '<form action="'.$pay_url.$session_result->sessionToken.'" method="post" id="three_d_form"/>
            <input name="cardOwner" type="hidden" id="cardOwner" value="' . $tr->cc_name . '">
            <input name="pan" type="hidden" id="pan" value="' . $tr->cc_number . '">
            <input name="expiryMonth" type="hidden" id="expiryMonth" value="' . str_pad($tr->cc_expire_month, 2, "0", STR_PAD_LEFT) . '">
            <input name="expiryYear" type="hidden" id="expiryYear" value="' . "20".str_pad(substr($tr->cc_expire_year, -2) ,2 ,"0", STR_PAD_LEFT) . '">
            <input name="cvv" type="hidden" id="cvv" value="' . $tr->cc_cvv . '">
            <input name="cardName" type="hidden" id="cardName" value="'.$tr->cc_name.'">
            <input name="installmentCount" type="hidden" id="installmentCount" value="' . $tr->installment . '">
            </form>';
            $form .= '<script>document.getElementById("three_d_form").submit();</script>';
            $form .= '<script type="text/javascript"
            src="https://h.online-metrix.net/fp/tags.js?org_id=6bmm5c3v&amp&session_id='.$session_result->sessionToken.'&pageid=1">
            </script>
            <noscript>
            <iframe
            style="width: 100px; height: 100px; border: 0; position: absolute; top: -5000px;"
            src="https://h.online-metrix.net/fp/tags.js?org_id=6bmm5c3v&amp&session_id='.$session_result->sessionToken.'&pageid=1 "></iframe>
            </noscript>
            ';
            $tr->tds = true;
            $tr->tds_echo = $form;
        }
        else {
            $tr->result_code = $session_result->errorCode;
            $tr->result_message = $session_result->errorMsg;
            $tr->result = false;
            $tr->debug(json_encode($session_result));
            return $tr;
        }

        return $tr;

    }

    public function tdValidate($tr)
    {

        $response = $_POST;

        if ($response["responseCode"] == "00") {
            $tr->result = true;
            $tr->result_code = $response['responseCode'];
            $tr->result_message = "Ödeme Başarılı";

        }
        else {
            $tr->result = false;
            $tr->result_code = $response['errorCode'];
            $error = '';
            if(isset($response['errorMsg'])) {
                $error = urldecode($response['errorMsg']);
            }
            else if (isset($response['pgTranErrorText'])) {
                $error = $response['pgTranErrorText'];
            } else {
                $error = $response['responseMsg'];
            }
            $tr->result_message = $error;
            $tr->debug(json_encode($response));

        }

        return $tr;
    }

    public function getConnection($request, $url) {
        curl_setopt($request, CURLOPT_URL, $url);
        curl_setopt($request, CURLOPT_HEADER, false);
        curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($request, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded', "Expect:"));
        curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE);
    }


    public function getResponse($request, $requestData) {
        curl_setopt($request, CURLOPT_POSTFIELDS, $requestData);
        $post_response = curl_exec($request);
        curl_close($request);
        return $post_response;
    }

    public function convertToRequestData($map) {
        $post_string = "";
        foreach ($map as $key => $value) {
            $post_string .= "$key=" . urlencode($value) . "&";
        }
        $post_string = rtrim($post_string, "& ");
        return $post_string;
    }

}
