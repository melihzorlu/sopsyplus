<?php 
if(!isset($sanalpospro_uri)) exit;
			/**
			Chrome Cookie SameSite Policy fix 
			*/
			
			$path = ini_get('session.cookie_path');
			$domain =  $this->request->server['HTTP_HOST'];
			foreach($_COOKIE as $k => $v){
				SameSiteCookieSetter::setcookie($k,$v, array('secure' => true, 'samesite' => 'None', 'path' => $path, 'domain' => $domain));
			}	
			/**
			Chrome Cookie SameSite Policy fix 
			*/
?>
<?php echo $data['header']; ?>
<script>
    var protrccname = '<?php echo $this->translate('Your Name') ?>';
    var currency_sign = "<?php echo $currency->sign ?>";
    var card = new Array();
    var cards = new Array();
    var sanalposprouri = "<?php echo $sanalpospro_uri ?>";
    var defaultins = "<?php echo $defaultins['total'] ?>";
<?php foreach ($cards as $family => $frates): ?>
	    cards ['<?php echo $family ?>'] = new Array();
	<?php foreach ($frates as $div => $ins): ?>
		<?php if ($c_min_inst_amount < $ins['month']): ?>
			    cards["<?php echo $family ?>"]["<?php echo $div ?>"] = "<?php echo $ins['total'] ?>";
		<?php endif; ?>
	<?php endforeach; ?>
<?php endforeach; ?>
</script>
<div id="information-information" class="container">
	<ul class="breadcrumb">
		<?php foreach ($data['breadcrumbs'] as $breadcrumb): ?>
			<li><a href="<?php echo $breadcrumb['href'] ?>"><?php echo $breadcrumb['text'] ?></a></li>
		<?php endforeach; ?>
	</ul>
	<div class="row"><?php echo $data['column_right']; ?>
		<?php
		if ($data['column_right'] && $data['column_left'])
			$css_class = 'col-sm-6';
		elseif ($data['column_right'] && $data['column_left'])
			$css_class = 'col-sm-9';
		else
			$css_class = 'col-sm-12';

		?>
		<div id="content" class="<?php echo $css_class ?>"><?php echo $data['content_top'] ?>
			<div class="row" id="spp_top">
				<div class="col-xs-12 col-lg-6" align="center">
					<h2><?php echo $this->translate('Secure Payment With Credit Card') ?></h2>
					<small>
						<?php echo $this->translate('This page allows you make a secure credit card payment via an SSL encrypted form') ?><br/>
						<?php echo $this->translate('You may redirect to the 3D Secure page and use your SMS password.') ?>
						<?php echo $this->translate('Total to Pay is ') ?>  
					</small><span class="price" id="spr_total_to_pay"></span> <?php echo $currency->name ?>
					<hr/>
					<span class="price" id="total_to_pay"></span>
				</div>
				<div class="col-xs-12 col-sm-6 hidden-md-down" align="center">
					<img class="img-responsive" src="<?php echo HTTPS_SERVER ?>catalog/view/theme/default/image/sanalpospro/safepayment.png"/>
				</div>
			</div>

			<?php if ($error_message) : ?>
				<div class="row">
					<div class="alert alert-danger" id="errDiv">
						<div class="spperror" id="errDiv">
							<?php echo $this->translate('Payment Failed. Your bank answer:') ?> <br/><?php echo $error_message ?><br/>
							<?php echo $this->translate('Please check the form and try again.') ?>
						</div>
					</div>
				</div>
			<?php endif; ?>
			<hr/>
			<?php if ($mp): ?>
				<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
				<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

				<script type="text/javascript" src="<?php echo $sanalpospro_uri ?>/sanalpospro/views/js/mfs-client.min.js" ></script>
				<?php echo $mp->ui['forms']['js_init'] ?>
				<script type="text/javascript" src="<?php echo $sanalpospro_uri ?>/sanalpospro/views/js/masterpass.js" ></script>

				<form id="masterpass_payform" method="post" action="#">
					<div class="row">
						<div class="col-lg-6 col-md-12">
							<div id="eticsoftMP_cardList_container" style="display:none">
								<div class="list-header">
									<img id="eticsoftMP_cardList_container_img" src="<?php echo $sanalpospro_uri ?>/sanalpospro/img/masterpass.svg" class="img-responsive">
									<span class="btn btn-info pull-right" id="usesavedcard" style="display:none">
										<?php echo $this->translate('Use a saved card') ?>
									</span>
								</div>
								<ul class="" id="eticsoftMP_cardList">
								</ul>
								<div class="eticsoftMP_cartitem2 row">
									<div class="col-md-6">
										<a id="usenewcard" class="btn btn-info" style="color:#fff" ><span class="glyphicon glyphicon-plus"></span> <?php echo $this->translate('Use another credit card') ?></a>
									</div>
									<div class="col-md-6">
										<a class="btn btn-warning" style="text-align:right; color:#fff" id="emp_delete_cc" > <?php echo $this->translate('Delete Selected Card') ?></a>
									</div>
								</div>
							</div>
						</div>
				</form>
				<div class="col-lg-6 col-md-12">
					<div id="mp_tx_selected_holder" style="display:none">
						<div id="eticsoftMP_scard_display">	
							<select class="form-control input-lg" name="cc_installment" id="mp_installment_select">
								<option value="1"> Tek Çekim </option>
							</select>
						</div>

						<hr/>
						<small><?php echo $this->translate('The amount will be charged your credit card is :') ?></small>
						<div id="eticsoftMP_totalToPay"></div>
						<hr/>
						<input name="cc_family" id="mp_tx_selected_holder_family" type="hidden"/>
						<input id="mp_tx_selected_holder_cc_id" name="cc_id" type="hidden"/>
						<input id="mp_tx_selected_holder_cc_name" name="cc_name" type="hidden"/>
						<input id="mp_tx_selected_holder_cc_number" name="cc_number" type="hidden"/>
						<input id="mp_tx_selected_holder_cc_expiry" name="cc_expiry" type="hidden"/>
						<div id="mp_tx_selected_holder_total_pay"> 
							<div id="emp_form_cardcvv">
								<input type="text" size="3" style="font-size:1.5em" name="cc_cvv" placeholder="CVV" id="emp_cc_cvv"/>
								<img width="80px" src="<?php echo $sanalpospro_uri ?>/sanalpospro/img/cvv.png" style="vertical-align: bottom;">
							</div>
							<br/>
						</div>
						<div class="text-center">
							<button type="submit" class="btn btn-info">Ödemeyi Tamamla </button>
						</div>
					</div>
				</div>
			</div>
		<?php endif; ?>


		<form novalidate action="" autocomplete="on" method="POST" id="cc_form">
			<div class="row">
				<div class="col-xs-12 col-md-12 col-lg-6" align="center" id="cc_form_table">
					<div class="row" >
						<div class="col-xs-12 col-sm-6 col-md-6">
							<?php echo $this->translate('Card Number') ?><br/>
							<input type="text" id="cc_number" name="cc_number" class="cc_input form-control input-lg" placeholder="•••• •••• •••• ••••" 
								   value="<?php if (Etictools::getValue('cc_number')): ?><?php echo Etictools::getValue('cc_number') ?><?php endif; ?>"/>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-6">
							<?php echo $this->translate('Card Expity Date') ?><br/>
							<input type="text" id="cc_expiry" name="cc_expiry" class="cc_input form-control input-lg" placeholder="<?php echo $this->translate('MM/YY') ?>" 
								   value="<?php echo Etictools::getValue('cc_expiry') ?>"/>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-md-6">
							<?php echo $this->translate('Card CVC') ?> <br/>
							<input type="text" id="cc_cvc" name="cc_cvv" class="cc_input form-control input-lg" placeholder="•••" 
								   value="<?php echo Etictools::getValue('cc_cvc') ?>"/>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-6">
							<?php echo $this->translate('Name On Card') ?><br/>
							<input type="text" id="cc_name" name="cc_name" class="cc_input form-control input-lg" placeholder="<?php echo $this->translate('Your Name') ?>" 
								   value="<?php echo Etictools::getValue('cc_name') ?>"/>
						</div>
					</div>
					<hr/>
					<div class="row">
						<div class="col-xs-12 col-sm-4">
							<select name="cc_family" id="tx_bank_selector" class="input-lg form-control" data-no-uniform="true">
								<option value="all"><?php echo $this->translate('All Card Types') ?></option>
								<?php foreach ($cards as $family => $rate): ?>
									<option value="<?php echo $family ?>"><?php echo ucfirst($family) ?></option>
								<?php endforeach; ?>
							</select>
						</div>
						<div class="col-xs-12 col-sm-8">
							<div id="tx_selected_holder" class="pull-left">

							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-md-6 hidden-md-down">
					<div id="card-wrapper"></div>
					<div id="prefix_bank_logo" align="center"></div>
					<div id="prefix_bank" align="center"></div>
				</div>
			</div>

			<div class="row text-center" style="text-align:center">
				<hr/>
				<button name="sanalpospro_submit" type="submit" id="cc_form_submit" align="center" class="btn btn-lg btn-primary"><?php echo $this->translate('Pay Now') ?></button>
			</div>
			<?php if ($mp): ?>
				<div class="row text-center" id="mp_register_container" style="text-align:center">
					<?php echo $mp->ui['forms']['registerMPcheck'] ?>
					<?php echo $mp->ui['forms']['registerMPcontainer'] ?>
				</div>
			<?php endif; ?>
		</form>

		<?php foreach ($cards as $family => $rate): ?>
			<div class="tx_banka" style="display:none" id="tx_banka_<?php echo $family ?>">
				<select style="min-width:200px" class="cc_installment_select input-lg cc_input form-control" name="cc_installment" id="tx_inst_<?php echo $family ?>">
					<?php foreach ($rate as $div => $installment): ?>
						<option value="<?php echo $div ?>" dataamount="<?php echo $installment['total'] ?>">
							<?php if ($div == 1) : ?>
								<?php echo $this->translate('One\'s Way') ?> 
								<?php if ($installment['rate'] == 0): ?> <?php echo $this->translate('No Fees') ?>
								<?php else: ?> <?php echo $installment['total'] . ' ' . $currency->sign ?>
								<?php endif; ?>
								<?php else: ?>
								<?php echo $div ?> <?php echo $this->translate('Ins.') ?> X <?php echo $installment['month'] ?>
								<?php if ($installment['rate'] == 0): ?> <?php echo $this->translate('No Fees') ?>
								<?php else: ?> <?php echo $this->translate('Total') ?> <?php echo $installment['total'] . ' ' . $currency->sign ?> <?php endif; ?>
							</option>
						<?php endif; ?>
					<?php endforeach; ?>
				</select>
			</div>
		<?php endforeach; ?>
		<div class="tx_banka" style="display:none" id="tx_banka_all">
			<select style="min-width:200px"  class="cc_installment_select input-lg cc_input form-control" name="cc_installment" id="tx_inst_all">
				<option value="1" dataamount="<?php echo $defaultins['total'] ?>"> <?php echo $this->translate('Pay one\'s way') ?></option>
			</select>
		</div>
		<br/>
		<br/>
		<hr/>
		<?php if ($mp): ?>
			<div class="modal fade emp_modal" id="eticsoftMP_loader" role="dialog">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-body">
							<h2 align="center">Lütfen Bekleyin<h2>
									<div id="eticsoftMP_loaderImg"></div>
									</div>
									</div>
									</div>
									</div>

									<div class="modal fade emp_modal" id="eticsoftMP_message_panel" role="dialog">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">X</span>
													</button>
													<img src="<?php echo $sanalpospro_uri ?>/sanalpospro/img/masterpass.svg" class="img-responsive">
												</div>
												<div class="modal-body">
													<h2 align="center" id="eticsoftMP_message_title">Bir hata oluştu</h2>
													<div id="eticsoftMP_message_text" class="alert alert-warning"></div>
												</div>
											</div>
										</div>
									</div>
									<div id="eticsoftMP_container" style="font-size:.8em">
										<?php echo $mp->ui['forms']['checkMP'] ?>
										<?php echo $mp->ui['forms']['otp'] ?>
										<?php echo $mp->ui['forms']['mpin'] ?>
										<?php echo $mp->ui['forms']['linkCardtoClient'] ?>
										<?php echo $mp->ui['forms']['tos'] ?>
									</div>
								<?php endif; ?>
								</div>



								<?php echo $data['content_bottom'] ?></div>
								<?php echo $data['column_right'] ?></div>
								</div>
								<?php
								echo $data['footer'];
								exit;
								
								?>