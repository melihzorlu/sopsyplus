<?php

Class EticConfig
{

	public static $order_themes = array(
		'pro' => 'PRO!tema (Önerilir)',
		// '3d' => 'Üç boyutlu JS tema (Seksi)',
		// 'cr' => 'Kredity Form (Resmi)',
		// 'st' => 'Basit standart form '
	);
	public static $installment_themes = array(
		'color' => 'Renkli (Önerilir)',
		'simple' => 'Basit (Renksiz)',
		'white' => 'Beyaz (Resmi)',
		'colorize' => 'Colorize (Seksi) '
	);
	public static $families = array(
		'axess', 'bonus', 'maximum', 'cardfinans', 'world', 'paraf', 'advantage', 'combo', 'miles-smiles'
	);
	public static $messages = array();
	public static $gateways;

	public static function get($key, $code = 'payment_sanalpospro')
	{
		$result = EticSql::getRow('setting', array('key' => $key, 'code' => $code));
		return isset($result['value']) ? $result['value'] : false;
	}

	public static function getAllSettings($code = 'payment_sanalpospro')
	{
		return EticSql::getRows('setting', array('code' => $code));
	}

	public static function set($key, $value, $code = 'payment_sanalpospro')
	{
		$serialized = false;
		if (is_array($value)) {
			$value = serialize($value);
			$serialized = true;
		}
		if (EticSql::getRow('setting', array('key' => $key, 'code' => $code))) {
			return EticSql::updateRow('setting', array('value' => $value, 'serialized' => $serialized), array('key' => $key, 'code' => $code));
		}
		return EticSql::insertRow('setting', array(
				'value' => $value,
				'serialized' => $serialized,
				'key' => $key,
				'code' => $code
		));
	}

	public static function getAdminGeneralSettingsForm($obj)
	{

		$t = '<form action="" method="post" id="general_settings_form" >
			<div class="panel">
				<div class="row ">
					<div class="col-md-8"> <!-- required for floating -->
					<p>Modülün görünümü ve temel fonksyionlarını bu panelden değiştirebilirsiniz. Pos ayarları için <a href="#rates" role="tab" data-toggle="tab">buraya</a> tıklayınız.</p>
				</div>
				<div class="col-md-4"> 
					<a class="btn btn-default pull-right" href="#help" role="tab" data-toggle="tab"><i class="process-icon-help"></i> Yardım</a> 
					<button type="submit" name="submitspr" class="btn btn-default pull-right"><i class="process-icon-save"></i> Ayarları Kaydet</button>
				</div>
			</div>';

		$t .= '
			<h2>Genel Ayarlar</h2>
			<div class="row">';
		$t .= '
            <div class="col-md-3 text-center sppbox ' . (EticConfig::get('payment_sanalpospro_status') == '1' ? 'bggreen ' : 'bgred') . '">
                <h2>Eklenti Aktif ?</h2>
                <select class="form-control" name="spr_config[payment_sanalpospro_status]">
                    <option value="1"> Aktif </option>
                    <option value="0" ' . (EticConfig::get('payment_sanalpospro_status') != '1' ? 'SELECTED ' : '') . '> Pasif </option>
                </select>
                <br/>
                Eklentiyi aktifleştirir<br/>
                <a href="https://sanalpospro.com/sikca-sorulan-sorular/" class="btn btn-info btn-large" target="_blank"><i class="icon-question-sign"></i> Açıklama </a>
            </div>';

// Hata Kayıt Modu
		$t .= '
            <div class="col-md-3 text-center sppbox ' . (EticConfig::get("POSPRO_DEBUG_MOD") == 'on' ? 'bgred ' : 'bggray') . '">
                <h2>Detaylı İşlem Kayıt</h2>
                <select class="form-control" name="spr_config[POSPRO_DEBUG_MOD]">
                    <option value="on" ' . (EticConfig::get("POSPRO_DEBUG_MOD") == 'on' ? 'SELECTED ' : '') . '> Açık </option>
                    <option value="off" ' . (EticConfig::get("POSPRO_DEBUG_MOD") == 'off' ? 'SELECTED ' : '') . '> Kapalı </option>
                </select>
                <br/>
                Tüm işlemleri incelemek üzere kaydeder.<br/>
                <a href="https://sanalpospro.com/sikca-sorulan-sorular/" class="btn btn-info btn-large" target="_blank"><i class="icon-question-sign"></i> Açıklama </a>
            </div>';

//3D Auto Form
		$t .= '
		<div class="col-md-3 text-center sppbox bggray">
		<h2>3DS Otomatik Yönlendirme</h2>
		<select class="form-control" name="spr_config[POSPRO_ORDER_AUTOFORM]">
		<option value="on" > Açık (önerilir)</option>
		<option value="off" ' . (EticConfig::get("POSPRO_ORDER_AUTOFORM") == 'off' ? 'SELECTED ' : '') . ' > Kapalı </option>
		</select>
		<br/>
		3DS formlarını otomatik yönlendir.<br/>
		<a href="https://sanalpospro.com/sikca-sorulan-sorular/" class="btn btn-info btn-large" target="_blank"><i class="icon-question-sign"></i> Açıklama </a>
		</div>';
// Order status 
		$statuses = $obj->model_localisation_order_status->getOrderStatuses();
		$t .= '
		 <div class="col-md-3 text-center sppbox bggray">
		 <h2>Sipariş Durumu</h2>
		 <select name="spr_config[sanalpospro_order_status_id]" class="form-control">';
		foreach ($statuses as $status)
			$t .= '<option value="' . $status['order_status_id'] . '" '
				. (EticConfig::get("sanalpospro_order_status_id") == $status['order_status_id'] ? 'selected="SELECTED" ' : '') . ' >'
				. $status['name'] . ' </option>';

		$t .= '<option value="Gizle" ' . (EticConfig::get("sanalpospro_order_status_id") == 'off' ? 'SELECTED ' : '') . ' > Gizle </option>
		 </select>
		 <br/>
		 Ödeme yapıldıktan sonra sipariş durumu.<br/>
		 <a href="https://sanalpospro.com/sikca-sorulan-sorular/" class="btn btn-info btn-large" target="_blank"><i class="icon-question-sign"></i> Açıklama </a>
		 </div>';
// currency
		/*
		  $t .= '<div class="col-md-3 text-center sppbox bggray">
		  <h2>Otomatik TL çevirimi</h2>
		  <select class="form-control" name="spr_config[POSPRO_AUTO_CURRENCY]">
		  <option value="on"> Açık (önerilir)</option>
		  <option value="off" ' . (EticConfig::get("POSPRO_AUTO_CURRENCY") == 'off' ? 'SELECTED ' : '') . '> Kapalı </option>
		  </select>
		  <br/>
		  Yabancı kurlar ödemeleri TL ye çevirir<br/>
		  <a href="https://sanalpospro.com/sikca-sorulan-sorular/" class="btn btn-info btn-large" target="_blank"><i class="icon-question-sign"></i> Açıklama </a>
		  </div>';
		 */

		$t .= '</div>'; //row
// Açıklama
		/*  $t .= '<div class="col-md-6 text-center sppbox bgred">
		  <h2>Önemli</h2>
		  Posların çoğu USD ve EUR codelarındaki ödemeleri kabul eder ve EticSoft SanalPos PRO! tüm kurları destekler.
		  Fakat sanal POS hizmetinizin banka tanımlarında eksiklik ve hatalar olması gibi durumlarda hatalı veya başarısız ödemelerle karşılaşabilirsiniz.<br/>
		  <b>Para birimlerinin ISO ve Numerik codelarını doğru girmeniz oldukça önemlidir. Örneğin Türk lisasının ISO codeu "TRY" dir.</b>
		  <br/>
		  <br/>
		  </div>
		  </div>';
		 */

		$t .= '<h2>Taksit Ayarları</h2>
		<div class="row">';
		$default_rate = EticInstallment::getDefaultRate();
		$gwas = EticGateway::getGateways(true);

		if ($gwas) {

			$t .= '<div class="col-md-3 text-center sppbox bggray">
		<h2>Min Taksit Tutarı</h2>
		<input name="spr_config[POSPRO_MIN_INST_AMOUNT]" size="4" class="form-control" value="' . (float) Eticconfig::get('POSPRO_MIN_INST_AMOUNT') . '" type="text"/><br />
		Taksit seçeneğinin aylık tutarı en az bu kadar olmalıdır. (TL)<br/>
		<a href="https://sanalpospro.com/sikca-sorulan-sorular/" class="btn btn-info btn-large" target="_blank"><i class="icon-question-sign"></i> Açıklama </a>
		</div>';


			$t .= '<div class="col-md-3 text-center sppbox bggray">
		<h2>Varsayılan POS</h2>';
			$t .= '<select name="spr_config_default_gateway" class="form-control">';
			foreach ($gwas as $gw)
				$t .= '<option value="' . $gw->name . '" ' . ($default_rate['gateway'] == $gw->name ? ' selected ' : '') . '>' . $gw->full_name . '</option>';
			$t .= '</select>'
				. '<br />
		Taksit yapılamayan kartlar için bu POS\'u kullan.<br/>
		<a href="https://sanalpospro.com/sikca-sorulan-sorular/" class="btn btn-info btn-large" target="_blank"><i class="icon-question-sign"></i> Açıklama </a>
        </div>';

			$t .= '<div class="col-md-3 text-center sppbox bggray">
		<h2>Tek Çekim Komisyonu </h2>
		<input name="spr_config_default_rate" class="form-control" size="4" value="' . (float) $default_rate['rate'] . '" type="number" step="0.01"/><br />
		Varsayılan POS kullanıldığı zaman müşteriye yansıtılacak yüzde.<br/>
		<a href="https://sanalpospro.com/sikca-sorulan-sorular/" class="btn btn-info btn-large" target="_blank"><i class="icon-question-sign"></i> Açıklama </a>
		</div>';

			$t .= '<div class="col-md-3 text-center sppbox bggray">
		<h2>Tek Çekim Maliyeti </h2>
		<input name="spr_config_default_fee" size="4" class="form-control" value="' . (float) $default_rate['fee'] . '" type="number" step="0.1"/><br />
		Varsayılan POS kullanıldığı zaman sizden kesilecek yüzde <br/>
		<a href="https://sanalpospro.com/sikca-sorulan-sorular/" class="btn btn-info btn-large" target="_blank"><i class="icon-question-sign"></i> Açıklama </a>
		</div>';
		} else {
			$t .= '<div class="alert alert-danger">Kurulu POS hizetiniz bulunamadı. Lütfen önce bir POS kurulumunu yapınız</div>';
		}

		$t .= '</div>';



		$t .= '<h2>Görünüm Ayarları</h2>
		<div class="row">';

// taksitleri göster
		$t .= '
		<div class="col-md-3 text-center sppbox bggray">
		<h2>Taksitler Sekmesi</h2>
		<select name="spr_config[POSPRO_TAKSIT_GOSTER]" class="form-control">
		<option value="Goster"> Göster </option>
		<option value="Gizle" ' . (EticConfig::get("POSPRO_TAKSIT_GOSTER") == 'off' ? 'SELECTED ' : '') . ' > Gizle </option>
		</select>
		<br/>
		Ürün sayfasının altında bulunan taksit seçenekleri.<br/>
		<a href="https://sanalpospro.com/sikca-sorulan-sorular/" class="btn btn-info btn-large" target="_blank"><i class="icon-question-sign"></i> Açıklama </a>
		</div>';




// ödeme tema
		$t .= '
		<div class="col-md-3 text-center sppbox bggray">
		<h2>Ödeme Ekranı Teması</h2>
		<select name="spr_config[POSPRO_PAYMENT_PAGE]" class="form-control">';
		foreach (EticConfig::$order_themes as $k => $v):
			$t .= '<option value="' . $k . '" ' . (EticConfig::get("POSPRO_PAYMENT_PAGE") == $k ? 'SELECTED ' : '') . '>' . $v . '</option>';
		endforeach;
		$t .= '</select>
		<br/>
		Ödeme sayfanızın yapısını seçiniz<br/>
		<a href="https://sanalpospro.com/sikca-sorulan-sorular/" class="btn btn-info btn-large" target="_blank"><i class="icon-question-sign"></i> Açıklama </a>
		</div>';
// taksit tema
		$t .= '<div class="col-md-3 text-center sppbox bggray">
		<h2>Taksitler Tema</h2>
		<select name="spr_config[POSPRO_PRODUCT_TMP]" class="form-control">';
		foreach (EticConfig::$installment_themes as $k => $v):
			$t .= '<option value="' . $k . '" ' . (EticConfig::get("POSPRO_PRODUCT_TMP") == $k ? 'SELECTED ' : '') . '>' . $v . '</option>';
		endforeach;
		$t .= '</select>
		<br/>
		Taksit seçenekleri sekmesinin görünümü.<br/>
		<a href="https://sanalpospro.com/sikca-sorulan-sorular/" class="btn btn-info btn-large" target="_blank"><i class="icon-question-sign"></i> Açıklama </a>
		</div>
		<div class="col-md-3 text-center sppbox bggreen">
		</div>
		</div><hr/>';

		$t .= '<div class="row">
		<div class="alert alert-warning"><h2>Uyarı</h2>SanalPOS PRO! tüm para birimlerini destekler. 
		Fakat yabancı kurlarda ödeme alabilmeniz için hizmet aldığınız POS altyapısının da desteklemesi gerekir. 
		</div>
		</div>';


		$t .= '<input name="conf-form" type="hidden" value="1" />
		</div>
		</form>';
		return $t;
	}

	public static function getMasterPassForm()
	{
		$t = '
			<script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js" integrity="sha384-SlE991lGASHoBfWbelyBPLsUlwY1GwNDJo3jSJO04KZ33K2bwfV9YBauFfnzvynJ" crossorigin="anonymous"></script>

			<form action="" method="post" id="masterpass_settings_form">
			<div class="panel">
				<div class="row ">
					<div class="col-md-8"> <!-- required for floating -->
						<p class="alert alert-info"><b>Masterpass</b> MasterCard tarafından sağlanan bir e-cüzdan sistemidir.
						<b>Masterpass</b> cüzdan hesabı olan kullanıcılara tek tıkla ödeme seçeneği gösterir.
						Masterpass hesabı olmayan müşterileriniz için ise hiç bir şey değişmez. Eskisi gibi kart bilgilerini girerek ödeme yaparlar.
						<b>Masterpass</b> mağazanızın ödeme sayfasını değiştirmeden ek bir seçenek olarak 
						çalışır. Ödemeleriniz eskisi gibi anlaşmalı olduğunuz bankanın posundan çekilmeye devam eder.
						<b>Herhangi bir komisyon ücreti almaz ve ödemenize dokunmaz.</b> 
						
						</p>
					</div>
					<div class="col-md-4"> 
						<a class="btn btn-default pull-right" href="https://sanalpospro.com/masterpass-entegrasyon/" target="_blank" ><i class="process-icon-help"></i> Yardım</a> 
						<button type="submit" name="submitspr" class="btn btn-default pull-right"><i class="process-icon-save"></i> Ayarları Kaydet</button>
					</div>
				</div>	
				<div class="row">
					<h2>Masterpass Üye İşyeri Olmanız İçin 8 Neden</h2>

				</div>
			<div class="row">
					<div class="col-sm-3 sppbox spph250">
						<h2><i class="fab fa-cc-mastercard mpicon" ></i><br/><br/>  BY MASTERCARD</h2>
						<p>Masterpass altyapısı tamamen MasterCard tarafından 
						sağlanan, dünyanın ve ülkemizin büyük markaları ve 
						alışveriş platformlarında kullanılan bir servistir.</p>
					</div>
					<div class="col-sm-3 sppbox spph250">
						<h2><i class="fas fa-sync mpicon" ></i><br/><br/> POS DEĞİŞİKLİĞİ YOK !</h2>
						<p>Mevcut kullandığınız pos sistemini veya bankanızı 
						değiştirmenize gerek yok. Masterpass altyapısı sadece 
						bir cüzdan servisidir. Ödemeleriniz eskisi gibi kendi 
						sanalposunuzdan tahsil edilir.</p>
					</div>
					<div class="col-sm-3 sppbox spph250">
						<h2><i class="far fa-money-bill-alt mpicon" ></i><br/><br/> 1 YIL ÜCRETSİZ DENEYİN</h2>
						<p>Masterpass e-cüzdan sistemini SanalPOS PRO! 
						sponsorluğu ile birlikte 1 yıl hiç bir ücret ödemeden 
						kullanabilirsiniz. Masterpass servisinde 
						hiç bir komisyon, ek ücret yoktur.</p>
					</div>
					<div class="col-sm-3 sppbox spph250">
						<h2><i class="far fa-thumbs-up mpicon" ></i><br/><br/> ETICSOFT DESTEĞİ </h2>
						<p>Bu eklentiyi Masterpass işbirliği kapsamında biz geliştirdik.
						İhtiyaç duyduğunuz tüm teknik desteği <b>ÜCRETSİZ</b> olarak biz sağlıyoruz.
						</p>
					</div>
					<div class="col-sm-3 sppbox spph250 ">
						<h2><i class="fas fa-user-secret mpicon" ></i><br/><br/>  DAHA GÜVENLİ !</h2>
						<p>Masterpass tarafından sağlanan ek kullanıcı doğrulama, cep telefonu doğrulama ve 
						kart sahipliği doğrulama fonksiyonları sayesinde alışverişler daha güvenli olur.</p>
					</div>
					<div class="col-sm-3 sppbox spph250">
						<h2><i class="fas fa-fighter-jet mpicon"></i><br/><br/> DAHA HIZLI ÖDEME !</h2>
						<p>Masterpass kullanan müşterileriniz tek tıkla ödeme yapar. 
						Kart bilgilerinin bankaya iletilmesi siteniz üzerinden değil Masterpass üzerinden sağlanır.
						</p>
					</div>
					<div class="col-sm-3 sppbox spph250">
						<h2><i class="fas fa-magic mpicon" ></i><br/><br/> DAHA ŞIK !</h2>
						<p>Masterpass entegrasyonu yaptığınızda SanalPOS PRO! ödeme sayfanıza 
						oldukça sade ve şık bir UI/UX tasarımı belirir. Bu standartlar 
						Masterpass tarafından belirlenmektedir.</p>
					</div>
					<div class="col-sm-3 sppbox spph250">
						<h2><i class="fas fa-power-off mpicon" ></i><br/><br/> TEK TIKLA AÇ/KAPAT !</h2>
						<p>Masterpass entegrasyonunu bu sayfadan istediğiniz an kapatabilir, entegrasyonu by-pass ederek pasifleştirebilirsiniz. 
						Kapattığınız an ödeme sayfanız tamamen eskisi gibi çalışır.
						</p>
					</div>
				</div>
				<div class="row">
					<p class="alert alert-info"> EticSoft R&D lab olarak, MasterCard ile Masterpass projesinde 
					işbiriliği ve sponsorluk konularında bir anlaşma imzaladık. 
					Bu kapsamda Opensource (açık kaynak) e-ticaret platformlarındaki 
					Masterpass standartlarına uygun eklentilerin geliştirmelerini,
					eklentilerin dağıtımı, tanıtımı ve teknik desteğini ücretsiz olarak biz sağlıyoruz. 
					Kullandığınız bu eklentinin tüm teknik sürecin arkasında biz olduğumuz gibi 
					Masterpass sürecinizde de sizin yanınızda olacağız.				
					</p>
				</div>

				<hr/>
				<div class="row ">
					<div class="col-md-4 text-center sppbox ' . (EticConfig::get("MASTERPASS_ACTIVE") != 'on' ? 'alert-danger ' : 'alert-success') . '">
						<h2>Durum</h2>
						<select name="spr_config[MASTERPASS_ACTIVE]">
							<option value="on"> Aktif </option>
							<option value="off" ' . (EticConfig::get("MASTERPASS_ACTIVE") != 'on' ? 'SELECTED ' : '') . '> Pasif </option>
						</select>
						<br/>
						MasterPass sistemini aktif eder.<br/>
						<a href="https://sanalpospro.com/sikca-sorulan-sorular/" class="btn btn-info btn-large" target="_blank"><i class="icon-question-sign"></i> Açıklama </a>
					</div>';
		$t .= '
					<div class="col-md-4 text-center sppbox bggray">
						<h2>MasterPass Client ID</h2>
						<input name="spr_config[MASTERPASS_CLIENT_ID]" class="form-control" size="4" value="' . (float) Eticconfig::get('MASTERPASS_CLIENT_ID') . '" type="number"/><br />
						Masterpass tarafından sağlanan mağaza ID no<br/>
						<a href="https://sanalpospro.com/sikca-sorulan-sorular/" class="btn btn-info btn-large" target="_blank"><i class="icon-question-sign"></i> Açıklama </a>
					</div>';
		$t .= '
					<div class="col-md-4 text-center sppbox bggray">
						<h2>MasterPass Ortamı</h2>
						<select name="spr_config[MASTERPASS_STAGE]">
							<option value="TEST"> Test Ortamı </option>
							<option value="UAT" ' . (EticConfig::get("MASTERPASS_STAGE") == 'UAT' ? 'SELECTED ' : '') . '> UAT Ortam </option>
							<option value="PROD" ' . (EticConfig::get("MASTERPASS_STAGE") == 'PROD' ? 'SELECTED ' : '') . '> Canlı Ortam </option>
						</select><br/>
						MasterPass ortam seçimi. Test ortamında ödemeler gerçekten tahsil edilmez.<br/>
						<a href="https://sanalpospro.com/sikca-sorulan-sorular/" class="btn btn-info btn-large" target="_blank"><i class="icon-question-sign"></i> Açıklama </a>
					</div>';
		$t .= '</div>';
		/*
		  $t .= '
		  <div class="row">
		  <div class="col-md-3 panel">
		  <h2>Participant number</h2>
		  <input name="spr_config[MASTERPASS_PART_NO]" class="form-control"  value="' . Eticconfig::get('MASTERPASS_PART_NO') . '" /><br />
		  Masterpass tarafından sağlanan Program participant name <br/>
		  <a href="https://sanalpospro.com/sikca-sorulan-sorular/" class="btn btn-info btn-large" target="_blank"><i class="icon-question-sign"></i> Açıklama </a>
		  </div>
		  <div class="col-md-3 panel">
		  <h2>Participant name</h2>
		  <input name="spr_config[MASTERPASS_PART_NAME]" class="form-control" value="' . Eticconfig::get('MASTERPASS_PART_NAME') . '" /><br />
		  Masterpass tarafından sağlanan Program participant number <br/>
		  <a href="https://sanalpospro.com/sikca-sorulan-sorular/" class="btn btn-info btn-large" target="_blank"><i class="icon-question-sign"></i> Açıklama </a>
		  </div>
		  <div class="col-md-3 panel">
		  <h2>MasterPass sponsor No</h2>
		  <i>PS702973</i><br /><hr/>
		  Masterpass sponsorluk no <br/>
		  <a href="https://sanalpospro.com/sikca-sorulan-sorular/" class="btn btn-info btn-large" target="_blank"><i class="icon-question-sign"></i> Açıklama </a>
		  </div>
		  <div class="col-md-3 panel">
		  <h2>MasterPass sponsor Name</h2>
		  <i>Eticsoft Sponsor</i><br /><hr/>
		  Masterpass sponsor adı <br/>
		  <a href="https://sanalpospro.com/sikca-sorulan-sorular/" class="btn btn-info btn-large" target="_blank"><i class="icon-question-sign"></i> Açıklama </a>
		  </div>
		  </div>';
		 * 
		 */
		$t .= '		
				<div class="row">
					<div class="col-md-8"> 
						<i>
							Enc ' . EticConfig::get("MASTERPASS_ENC_KEY") . ' Mac ' . EticConfig::get("MASTERPASS_MAC_KEY") . ' Last' . date("Y-m-d H:i:s", (int) EticConfig::get('MASTERPASS_LAST_KEYGEN')) . '
						</i>
					</div>
					<div class="col-md-4"> 
						<a class="btn btn-default pull-right" href="https://sanalpospro.com/sikca-sorulan-sorular/" target="_blank" ><i class="process-icon-help"></i> Yardım</a> 
						<button type="submit" name="submitspr" class="btn btn-default pull-right"><i class="process-icon-save"></i> Ayarları Kaydet</button>
					</div>

				<div>';
		/*
		  $t .= '	<div class="row">
		  <div class="col-md-12">
		  <h2>Entegrasyon Adımları Nelerdir ?</h2>
		  <ol>
		  <li><a href="https://eticsoft.com/masterpass-sozlesmesi.pdf" target="_blank">Bu adresten</a>
		  Masterpass İşlem Yönlendirme Hizmeti Taahhütnamesini indirip inceleyin.
		  Başvuru için tüm sayfalarını imzalayın ve kaşe basın.</li>
		  <li> Onaylandığınız zaman size Masterpass üye iş yeri bilgilerinizi göndereceğiz.  </li>
		  <li> Bu sayfadaki forma bu bilgileri giriniz. Bizim ve Masterpass ekibinin bir test uzmanı entegrasyonu test edecek. </li>
		  <li> Hepsi bu kadar. Dilerseniz Masterpass üye işyeri ikonunu sitenizin altına ekleyebilirsiniz. Desteğe ihtiyacınız olduğunda biz yanınızda olacağız. </li>
		  </ol>
		  </div>
		  </div>
		  ';
		 */
		$t .= '</div><input name="conf-form" type="hidden" value="1" />
		</form>';
		return $t;
	}

	public static function getAdminGatewaySettingsForm($module_dir)
	{
		$gateways = EticGateway::$gateways;
		if (!EticGateway::getGateways()) {
			return '<div class="panel text-center"> <i style="font-size:60px" class="process-icon-cancel"></i><br/> '
				. '<h1> Henüz hiç bir Sanal POS hizmeti kurulmamış !</h1>'
				. '<p>SanalPOS hizmeti aldığınız banka veya ödeme kuruluşlarının hizmetlerini '
				. '<a role="tab" data-toggle="tab" href="#integration"> Ödeme Yöntemleri </a> sekmesinden kurabilirsiniz.</p>'
				. ''
				. '</div>';
		}
		$t = '<form action="" method="post" id="bank_settings_form" class="sppform ">
			<div class="panel">
				<div class="row ">
                    <div class="col-md-6"> <!-- required for floating -->
                        <h2>Pos ayarları</h2>
                        <p>Pos ayarlarını bu sekmeden yapabilirsiniz. Taksit ayarları için <a href="#cards" role="tab" data-toggle="tab">buraya</a> tıklayınız.</p>
                    </div>
                    <div class="col-md-6"> 

                        <a class="btn btn-success pull-right" href="#integration" data-toggle="tab"><i class="process-icon-plus"></i> Yeni Pos</a> 
                        <a class="btn btn-default pull-right" href="#help" role="tab" data-toggle="tab"><i class="process-icon-help"></i> Yardım</a> 
                        <button type="submit" name="submit" class="btn btn-default pull-right"><i class="process-icon-save"></i> Tümünü Kaydet</button>
                        <input type="hidden" name="submitgwsetting" value="1"/>
                    </div>
                </div>
        <div class="row">
            <div class="col-md-2"> <!-- required for floating -->
                <!-- Nav tabs -->
                <ul class="nav nav-pills nav-stacked">
                    ';
		$satir = 0;
		foreach (EticGateway::getGateways(false) as $gwbutton):
			$t .= '<li class="' . ($satir == 0 ? 'active' : '' ) . '">
                            <a href="#bf_' . $gwbutton->name . '_form" data-toggle="tab">
                                <img src="' . HTTPS_CATALOG . 'catalog/view/theme/default/image/sanalpospro/gateways/' . $gwbutton->name . '.png" width="125px"/>
                            </a>
                        </li>';
			$satir++;
		endforeach;

		$t .= '</ul>
            </div>

            <div class="col-md-10 ">
                <!-- Tab panes -->
                <div class="tab-content">';
		$satir = 0;
		foreach (EticGateway::getGateways(false) as $gwd):
			if ($gw = New EticGateway($gwd->name)):

				$gwe = EticGateway::$gateways->{$gw->name};
				if (!isset($gwe->families)) {
					continue;
				}


				if (!isset($gwe->paid) OR ! $gwe->paid) {
					Etictools::rwm($gwe->full_name . ' POS lisansınız aktif değil. Bu sanalPOS kullanılamaz.'
						. '<br/>Lütfen eticsoft ile iletişime geçiniz.');
				}
				//print_r(EticGateway::$gateways); exit;
				$t .= '<!-- BANKA -->

                        <div class="' . ($satir == 0 ? 'active' : '') . ' tab-pane" id="bf_' . $gw->name . '_form">
                            <div class="col-md-7 sppbox bggray">
                                <input name="pdata[' . $gw->name . '][id_bank]" type="hidden" value="' . $gw->name . '" />
                                <h2>' . $gw->full_name . ' Pos Ayarları </h2>
                                <hr/>';
				if (isset($gw->params->test_mode) && $gw->params->test_mode == 'on'):
					$t .= '<div class="alert alert-danger">' . $gw->full_name . ' test modunda çalışıyor. '
						. 'Test modunda yapılan siparişlerin başarılı görünür fakat ödemesi alınmaz. </div>';
				endif;
				$t .= '<h2>Parametreler</h2>' . $gw->createFrom();

				$t .= ' <br/>
						<div class="row">
                        <button type="submit" value="' . $gw->name . '" name="submit_for" class="btn btn-success"><i class="icon icon-save"></i> Ayarları Kaydet</button>
						<a class="btn btn-default pull-right" href="#" role="tab" data-toggle="tab"><i class="icon icon-question-sign"></i> Yardım</a> 
						</div>

			<hr/>
			<br/>
			</div>
			<div class="col-md-5 sppbox bggray text-center">
			<h2><img src="' . HTTPS_CATALOG . 'catalog/view/theme/default/image/sanalpospro/gateways/' . $gw->name . '.png"/></h2>';

				if (json_decode($gwe->families)):
					$t .= '<div style="line-height:25px"> <span>Taksit yapabildiği kartlar:</span><br/>';
					foreach (json_decode($gwe->families) as $family):
						$t .= '<img class="img-thumbnail" width="90px" src="' . HTTPS_CATALOG . 'catalog/view/theme/default/image/sanalpospro/cards/' . $family . '.png"/>';
					endforeach;
					$t .= '</div>';
				endif;


				$t .= '<div id="' . $satir . '-kmp" >
                    
    			<table class="table">';

				$t .= '</table>
			</div>
			<a href="https://sanalpospro.com/sikca-sorulan-sorular/" class="btn btn-info btn-large" target="_blank"><i class="icon-question-sign"></i> Açıklama </a>
            
            <a data-toggle="collapse" class="btn btn-danger" data-target="#' . $gw->name . '_remove">
                <i class="icon-remove"></i> Bu POS\'u Kaldır</a>
            
            
            <div id="' . $gw->name . '_remove" class="collapse">
                <div class="panel">
                    <div class="alert alert-danger"> Dikkat! POS silme işlemi geri alınamaz. 
                    Sildiğiniz POS\'u daha sonra yeniden kurabilirsiniz. Fakat
                    ' . $gw->full_name . ' için girdiğiniz kullanıcı bilgileri ve oranlar da silinecektir.</div>
                    <div class="toggle"><button type="submit" class="btn btn-danger" name="remove_pos" value="' . $gw->name . '">Kaldır</button></div>
                </div>
            </div>
			</div>';

				if (EticTools::getValue('adv')):
					$t .= '<div class="col-sm-6">
					<input name="' . $gw->name . '[lib]" value="' . $gw->lib . '"/>
				</div>';
				endif;


				$t .= '</div>';
				$satir++;
			endif;
		endforeach;
		$t .= '
            
            </div>
            </div>

            </div>
            </div>
            <input name="bank-form" type="hidden" value="1" />
		</form>';
		return $t;
	}

	public static function getAdminIntegrationForm()
	{

		$t = '<div class="panel">';
		$exists_gws = array();


		foreach (EticGateway::getGateways() as $gateway)
			$exists_gws [] = $gateway->name;



			$t .= '
			<div class="row">
				<div class ="col-md-12 panel">
					<h2>Kullanmak İstediğiniz POS servisini seçiniz</h2>
					EticSoft sadece BDDK lisanslı ve <b>güvenilir</b> ödeme kuruluşları ve bankalar ile entegrasyon sağlıyor.
					Kullanmak istediğiniz ödeme sistemi aşağıda yoksa, ilgili ödeme şirketi/banka standartlara 
					uygun bulunmamış veya bizimle hiç iletişime geçmemiş olabilir. 
					<hr/>
					<p align="center" class="alert alert-info">
					Tüm bankaları ve ödeme sistemlerini birlikte çalışacak şekilde (hibrit) kullanabilirsiniz.
					Örnek: Tüm kartların tek çekimlerini Xbankası üzerinden, 5 taksitli ödemeleri Ypay ödeme 
					kuruluşu üzerinden, ABC kartının 2 taksitli ödemelerini Zpara ödeme kuruluşu üzerinden tahsil edebilirsiniz.						
					Kart türlerine ödeme yönetiminin nasıl çalışacağını seçmek için 
					<a href="#cards" role="tab" data-toggle="tab">Taksitler</a> tıklayınız.
					</p>
				</div>
			</div>';
			
		$count = 1;

		foreach (EticGateway::$gateways as $k => $gw) {
			if ($count%4 == 1)
			{ 
				$t .= '<div class="row">';
			}

			$gw->is_bank = isset($gw->is_bank) && $gw->is_bank ? true : false;
			$gw->lib = isset($gw->lib) && $gw->lib ? $gw->lib : $k;
			$gw->eticsoft = isset($gw->eticsoft) && $gw->eticsoft ? true : false;

			$t .= '<div class="col-md-3 text-center panel">
                  <div class="panel-heading">' . $gw->full_name . '</div>';

			$t .= '<div class="panel-body spph250">';
			$t .= '<img src="' . HTTPS_CATALOG . 'catalog/view/theme/default/image/sanalpospro/gateways/' . $k . '.png" class="thumbnail center-block "/>';

			if (!$gw->active):
				$t .= '<p align="center" class="alert alert-danger">Bu entegrasyon geçici olarak aktif değil</p>';
			else :
				if ($gw->eticsoft)
					$t .= '<p align="center" class="alert alert-success"> EticSoft ' . ($k == 'onlineodemesistemi' ? ' tarafından geliştirilmiş arge ürünü yazılımdır' : 'resmi iş ortağıdır.') . '</p>';
				else
				if (!$gw->is_bank)
					$t .= '<p align="center" class="alert alert-danger"> EticSoft iş ortağı DEĞİLDİR. Teknik destekte kısıtlamalar olabilir. </p>';
			endif;
			$t .= ' <a href="https://sanalpospro.com/opencart/' . $k . '-sanal-pos-kurulumu/" target="_blank" class="label label-default"><i class="icon-question"></i> Yardım</a>';
			if (json_decode($gw->families)):
				$t .= '<div style="line-height:25px"> <span>Taksit yapabildiği kartlar:</span>';
				foreach (json_decode($gw->families) as $family):
					$t .= ' <span class="label label-info">' . ucfirst($family) . '</span>';
				endforeach;
				$t .= '</div>';
			endif;
			$t .= '</div>';

			$t .= '<div class="panel-footer">';
			if ($gw->active):

				if (in_array($k, $exists_gws)):
					$t .= '<p align="center" class="alert alert-success"> Kurulu !</p>';
				else:
					$t .= '<span class="spr_price">' . ($gw->price == 0 ? 'Ücretsiz' : number_format($gw->price, 2) . ' TL/yıllık') . '</span><br/>';
					if (isset($gw->paid) AND $gw->paid):
						$t .= ' <form action="" method="post">'
							. '<input type="hidden" name="add_new_pos" value="' . $k . '"/>'
							. '<button type="submit" class="btn btn-success"><i class="icon-plus"></i> Kurulum </button>'
							. '</form>';
					else:
						$t .= EticConfig::getApiForm(array('redirect' => '?controller=gateway&action=buy&gateway=' . $k));
					endif;
				endif;

			endif;
			$t .= '</div></div>';
			
			if ($count%4 == 0)
			{
				$t .= "</div>";
			}
			$count++;
		}
		if ($count%4 != 1) 
			$t .= "</div>";
			
		$t .= '</div>';

		return $t;
	}

	public static function getApiForm($custom_array = false, $button_content = '<i class="icon-shopping-cart"></i> Satın Al')
	{
		$api = New SanalPosApiClient(1);
		$apilogininfo = $api->getLoginFormValues();
		$t = '<form action="' . $apilogininfo['url'] . '" target="_blank" method="post">';
		foreach ($apilogininfo as $k => $v)
			$t .= '<input type="hidden" name="' . $k . '" value="' . $v . '" />';
		if ($custom_array AND is_array($custom_array))
			foreach ($custom_array as $k => $v)
				$t .= '<input type="hidden" name="' . $k . '" value="' . $v . '" />';
		$t .= '<input type="hidden" name="api_login" value="1">'
			. '<button type="submit" class="btn btn-info">' . $button_content . '</button>'
			. ' </form>';
		return $t;
	}

	public static function getAdminToolsForm()
	{

		$t = '<form action="" method="post" id="toolsform">

            <div class="panel">
            <div class="row">';
		$t .= '
			<div class="col-md-4 sppbox bgred spph300"> <!--required for floating -->
            <h2>Eski Kayıtları Temizle</h2>
            <p>SanalPOS PRO! üzerinden yapılan alışveriş işlemlerinin tüm detaylarını, banka sorgu ve cevaplarını veri tabanına kayıt eder. (Kredi kartı bilgileri kayıt edilmez.)
            Bu bilgileri banka kayıtlarındaki uyumsuzluklarla karşılaştırmak, hata ayıklamak ve olası hukuki ihtilaflarda resmi mercilere sunmak için sizin sisteminize kayıt ediyoruz.
            Veritabanınızda çok fazla veri biriktiğinde bu bilgileri zaman zaman temizleyebilirsiniz. Bu temizleme işlemi son bir ay işlemleri hariç tüm işlemlerin detaylarını <b>geri getirilemeyecek şekilde</b> siler.</p>
            <hr/>
            <button class="btn btn-large btn-warning" name="clear-logs" value="1">Eski logları temizle</button>
            </div>';
		$t .= '
            <div class="col-md-4 text-center sppbox bgpurple spph300">
            <h2>Sunucu Uyumluluk Testi</h2>
            <p>
            Pos ve ödeme kuruluşlarının sistemleri bazı özel gereksinimlere ihtiyaç duyar. Bu gereksinimleri ve sisteminizin uyumluluğunu kontrol etmek için aşağıdaki aracı kullanabilirsiniz.
            Bu araç ayrıca SanalPOS PRO! modülünün çalışmasına engel olacak/etkileyecek modülleri/eklentileri de listeler.</p>
            <hr/>
            <button class="btn btn-large btn-success" name="check-server" value="1">Sunucuyu ve sistemi kontrol et</button>
            <br/>
            <br/>
            </div>';
		$t .= '
            <div class="col-md-4 text-center sppbox bgyellow spph300">
            <h2>Eski Versiyon Ayarları</h2>
            <p>
			Daha önce bir SanalPOS PRO! versiyonu kullandıysanız daha önce girilen banka parametrelerini göstermek için aşağıdaki butona tıklayabilirsiniz.
            Bu araç daha önceki versiyonlarda kurulu bankaları ve bilgilerini listeler. </p>
            <hr/>
            <button class="btn btn-large btn-info" name="check-oldtables" value="1">Eski bankaları göster</button>
            <br/>
            <br/>
            </div>';
		$t .= '</div>'; // Row
		/*
		  $cats = New HelperTreeCategoriesCore(1);
		  $cats->setUseCheckBox(true);
		  $cats->setTitle('Taksit uygulanmayacak kategoriler');
		  $cats->setInputName('spr_config_res_cats');

		  if (is_array(EticConfig::getResCats()))
		  $cats->setSelectedCategories(EticConfig::getResCats());
		  $t .= '<div class="row">';

		  $t .= '
		  <div class="col-md-6 panel">
		  <h2>Taksit Kısıtlaması</h2>
		  <p> Taksit yapılmayacak ürünlerin kategorilerini seçiniz.
		  Alışveriş sepetinde bu kategorilerden ürünler varsa taksitli alışveriş yapılmayacak,
		  ödemeler tek çekim olarak yapılabilecektir. Taksit kısıtlaması olan ürünler
		  sepete atıldığında müşteriye bir uyarı mesajı gösterilmektedir.
		  <b>Taksit kısıtlaması olan ürünleriniz yoksa hiç bir kategoriyi seçmeyiniz !</b>
		  </p>
		  ' . $cats->render() . '
		  <button type="submit" name="savetoolsform" value="1" class="btn btn-default pull-right"><i class="process-icon-save"></i> Kısıtlamaları Kaydet</button>
		  </div>';

		  $t .= '<div class="col-md-6 bgblue sppbox">'
		  . '<h2>FP007 Dolandırıcılık Koruma Sistemi</h2>'
		  . '<div class="alert alert-info">SanalPOS PRO! mağazalarının alışveriş süreçlerini güvenli hale getiren'
		  . 'FP007 proje kodlu yazılım servisimiz henüz yapım aşamasında !</div>'
		  . '<hr>'
		  . '</div>';
		  $t .= '</div>'; // Row

		 */
		$t .= '</div>'; // Panel



		$t .= '</form>  ';
		return $t;
	}

	public static function getApiSettingsForm()
	{

		$t = '<form action="" method="post" id="toolsform">

            <div class="panel">
            <div class="row">
            <div class="col-md-12 sppbox bgred"> <!--required for floating -->
            <h2>Eski Kayıtları Temizle</h2>
            <p>SanalPOS PRO! üzerinden yapılan alışveriş işlemlerinin tüm detaylarını, banka sorgu ve cevaplarını veri tabanına kayıt eder. (Kredi kartı bilgileri kayıt edilmez.)
            Bu bilgileri banka kayıtlarındaki uyumsuzluklarla karşılaştırmak, hata ayıklamak ve olası hukuki ihtilaflarda resmi mercilere sunmak için sizin sisteminize kayıt ediyoruz.
            Veritabanınızda çok fazla veri biriktiğinde bu bilgileri zaman zaman temizleyebilirsiniz. Bu temizleme işlemi son bir ay işlemleri hariç tüm işlemlerin detaylarını <b>geri getirilemeyecek şekilde</b> siler.</p>
            <hr/>
            <button class="btn btn-large btn-warning" name="clear-logs" value="1">Eski logları temizle</button>
            </div>
            <div class="col-md-6 text-center sppbox bgpurple">
            <h2>Sunucu Uyumluluk Testi</h2>
            <p>
            Pos ve ödeme kuruluşlarının sistemleri bazı özel gereksinimlere ihtiyaç duyar. Bu gereksinimleri ve sisteminizin uyumluluğunu kontrol etmek için aşağıdaki aracı kullanabilirsiniz.
            Bu araç ayrıca SanalPOS PRO!modülünün çalışmasına engel olacak/etkileyecek modülleri/eklentileri de listeler.</p>
            <hr/>
            <button class="btn btn-large btn-warning" name="check-server" value="1">Sunucuyu ve sistemi kontrol et</button>
            <br/>
            <br/>
            </div>
            </div>
            </div>
            </form> 
        ';
		return $t;
	}

	public static function getCardSettingsForm($module_dir)
	{
		$all_gws = EticGateway::getGateways(true);
		$def_rate = EticInstallment::getDefaultRate();

		$t = '<form action="" id="cards" method="post"> 
                <div class="panel">
				<div class="row ">
                    <div class="col-md-4"> <!-- required for floating -->
                        <h2>Kartlar ve taksit seçenekleri</h2>
                        <p>Taksit ayarlarını bu sekmeden yapabilirsiniz. Pos ayarları için <a href="#pos" role="tab" data-toggle="tab">buraya</a> tıklayınız.</p>
                    </div>
                    <div class="col-md-8"> 
                        <a class="btn btn-default pull-right" href="#help" role="tab" data-toggle="tab"><i class="process-icon-help"></i> Yardım</a> 
                        <button type="submit" name="submitcards" class="btn btn-default pull-right"><i class="process-icon-save"></i> Oranları Kaydet</button>
                        <input type="hidden" name="submitcardrates" value="1"/>
                    </div>
                </div>';



		$t .= '<div class="row">';
		foreach (EticConfig::$families as $family):
			$gwas = EticGateway::getByFamily($family, true);

			$t .= '<div class="col-md-4 sppbox spph750"><br/>'
				. '<img src="' . HTTPS_CATALOG . 'catalog/view/theme/default/image/sanalpospro/cards/' . $family . '.png" class="thumbnail center-block"/><br/>';
			if (!$gwas OR empty($gwas)) {
				$t .= '<h2>Uygun POS Yok veya Kurulmamış</h2>'
					. '<p><i style="font-size: 45px;" class="icon-remove"></i></p>' . ucfirst($family) . ' kart ailesine taksit yapabileceğiniz'
					. ' hiç bir POS sistemi kurulu değil. ' . ucfirst($family) . ' ödemelerini taksitsiz olarak alabilirsiniz.<br/>'
					. '<div class="alert alert-info">Sadece Tek Çekim Ödeme alabilirsiniz. Tek Çekimler için '
					. 'tanımlanmış POS sistemi: ' . ucfirst($def_rate['gateway']) . ' </div>';
				$t .= '<div style="line-height:25px"> <h2>Taksit yapabilen pos sistemleri</h2>';
				foreach (EticGateway::getByFamily($family, false) as $gwall)
					$t .= ' <span class="label label-info">' . ucfirst(EticGateway::$gateways->{$gwall}->full_name) . '</span>';
				$t .= '</div></div>';
				continue;
			}

			$t .= '<div class="row"><div class="col-sm-6 col-xs-6">'
				. 'Tümü için toplu seçim </div>'
				. '<div class="col-sm-6 col-xs-6">'
				. '<select class="inst_select_all" id="' . $family . '" name="' . $family . '_all">'
				. '<option value="">Seçiniz</option>'
				. '<option value="0">Taksit Yok</option>';

			foreach ($gwas as $gwa)
				$t .= '<option value="' . $gwa . '">' . ucfirst(EticGateway::$gateways->{$gwa}->full_name) . '</option>';

			$t .= '</select></div></div>'
				. '<table class="table">'
				. '<tr>'
				. '<td>Taksit</td>'
				. '<td>Pos</td>'
				. '<td>Oran (%)<br/><small>Müşteriye<br/> yansıyacak</small></td>'
				. '<td>Maliyet (%)<br/><small>Sizden <br/> kesilecek</small></td>'
				. '</tr>';
			for ($i = 1; $i <= 12; $i++) :
				$ins = EticInstallment::getByFamily($family, $i);

				$t .= '<tr>'
					. '<td>' . $i . '</td>'
					. '<td><select class="inst_select ' . $family . ' form-control" id="row_' . $family . '_' . $i . '" name="' . $family . '[' . $i . '][gateway]">'
					. '<option value="0">Kapalı</option>';

				if ($i == 1) {
					$t .= '<option value="' . $def_rate['gateway'] . '">' . ucfirst($def_rate['gateway']) . ''
						. '(Varsayılan)</option>';
					foreach ($all_gws as $gwa)
						$t .= '<option ' . ($ins && $ins['gateway'] == $gwa->name ? 'selected ' : '')
							. 'value="' . $gwa->name . '">' . ucfirst($gwa->name) . '</option>';
				} else
					foreach ($gwas as $gwa)
						$t .= '<option ' . ($ins && $ins['gateway'] == $gwa ? 'selected ' : '')
							. 'value="' . $gwa . '">' . ucfirst(EticGateway::$gateways->{$gwa}->name) . '</option>';

				$t .= '</select></td>'
					. '<td><div class="input-group">' . ($ins ? '<span class="row_' . $family . '_' . $i . ' input-group-addon">%</span>' : '' )
					. '<input class="form-control row_' . $family . '_' . $i . '  input_' . $family . '" size="5" step="0.01" type="number" style="width:60px" '
					. 'name="' . $family . '[' . $i . '][rate]" value="' . ($ins ? (float) $ins['rate'] : '') . '">'
					. '</div></td>'
					. '<td><div class="input-group' . ($ins['fee'] == 0 ? ' has-error' : '') . '">'
					. ($ins ? '<span class="row_' . $family . '_' . $i . ' input-group-addon">%</span>' : '' )
					. '<input type="number" step="0.01" style="width:60px" class="form-control row_' . $family . '_' . $i . ' input_' . $family . '"'
					. ' name="' . $family . '[' . $i . '][fee]" value="' . ($ins ? (float) $ins['fee'] : '') . '">'
					. '</div></td>'
					. '</tr>';
			endfor;

			$t .= '</table>';
			$t .= '<div style="line-height:25px"><span>' . ucfirst($family) . ' kartlarına taksit yapabilen pos sistemleri</span>';
			foreach (EticGateway::getByFamily($family, false) as $gwall)
				$t .= ' <span class="label label-' . (in_array($gwall, $gwas) ? 'success' : 'default') . '">'
					. '' . ucfirst(EticGateway::$gateways->{$gwall}->name) . '</span>';
			$t .= '</div>';
			$t .= '</div>';
		endforeach;
		$t .= '<div class="clear clearfix"></div>
            </div></div>
            </form>';
		return $t;
	}

	public static function getHelpForm()
	{

		$t = '
		<div class="panel">
			<div class="row">
				<div class="col-sm-6 text-center">            
					<h1>Yardıma mı ihtiyacınız var? <br/> Hemen eticsoft\'u çağırın !</h1>
					<div class="row">
						<div class="col-sm-2"></div>
						<img src="' . HTTPS_CATALOG . 'catalog/view/theme/default/image/sanalpospro/help2.png" style="height: 300px;" class="img-responsive text-center" id="payment-logoh" />
						<div class="col-sm-2"></div>
					</div>
				</div>
				<div class="col-sm-6 panel text-center">
					<h1>Destek Kanalları</h1><hr/>
					' . EticConfig::getApiForm(array('redirect' => '?controller=home&action=addticket'), '<h2><i class="icon icon-ambulance"> </i> Destek Sistemine Bağlan</h2>')
			. '<hr/>
					<a class="btn btn-info btn-large"><i class="icon icon-book"> </i> Kullanım Klavuzu</a> <hr/>
					<a href="mailto:destek@eticsoft.com?Subject=Opencart SanalPOS PRO " 
					   class="btn btn-info btn-large"><i class="icon icon-mail-reply"> </i> E-posta destek@eticsoft.com</a>
					<hr/>
					<a class="btn btn-info btn-large"><i class="icon icon-phone"> </i> Telefon 0242 241 59 85</a> 
				</div>

				<hr/>
			</div>
		</div>

		<div class="panel">
			<div class="row">
				<div class="col-sm-6 spph450 panel text-center">            
					<h2>Projenizi bizimle geliştirmek ister misiniz ?</h2>
					<a href="https://eticsoft.com/">
						<img src="' . HTTPS_CATALOG . 'catalog/view/theme/default/image/sanalpospro/eticsoft-infogram.png" class="img-responsive" id="payment-logoy" />
					</a>
				</div>
				<div class="col-sm-3 spph450 panel text-center">            
					<h2>Kusursuz Opencart Hosting !</h2>
					<a href="https://iyonhost.com/opencart-uyumlu-hosting-php//">
						<img src="' . HTTPS_CATALOG . 'catalog/view/theme/default/image/sanalpospro/hosting.jpg" class="img-responsive" id="payment-logo" />
					</a>
				</div>
				<div class="col-sm-3 panel spph450 text-center">            
					<h2>Opencart modüllerimiz</h2>
					<a href="https://eticsoft.com/shop/tr/13-opencart">
						<img src="' . HTTPS_CATALOG . 'catalog/view/theme/default/image/sanalpospro/shop.jpg" class="img-responsive" id="payment-logox" />
					</a>
				</div>

				<hr/>
			</div>
		</div>
		';
		return $t;
	}

	public static function getCampaigns(){
		$t = '<div class="panel">
		<div class="row">
			<div class="col-md-4">
				<a target="_blank" href="https://bit.ly/2VCy0Gi">
					<img style="width:100%;" src="https://sanalpospro.com/img/kampanyalar/ipara/kampanya.png"
						class="thumbnail center-block" />
				</a>
			</div>
			<div class="col-md-4">
				<a target="_blank" href="https://bit.ly/38j47QA">
					<img style="width:100%;" src="https://sanalpospro.com/img/kampanyalar/paybyme/kampanya.png"
						class="thumbnail center-block" />
				</a>
			</div>
			<div class="col-md-4">
				<a target="_blank" href="https://bit.ly/2CXqsY9">
					<img style="width:100%;" src="https://sanalpospro.com/img/kampanyalar/paynet/kampanya.png"
						class="thumbnail center-block" />
				</a>
			</div>
			<div class="col-md-4">
				<a target="_blank" href="https://bit.ly/2YShNij">
					<img style="width:100%;" src="https://sanalpospro.com/img/kampanyalar/paytr/kampanya.png"
						class="thumbnail center-block" />
				</a>
			</div>
			<div class="col-md-4">
				<a target="_blank" href="https://bit.ly/38maylP">
					<img style="width:100%;" src="https://sanalpospro.com/img/kampanyalar/paytrek/kampanya.png"
						class="thumbnail center-block" />
				</a>
			</div>
			<div class="col-md-4">
				<a target="_blank" href="https://bit.ly/3ijNQ2x">
					<img style="width:100%;" src="https://sanalpospro.com/img/kampanyalar/parampos/kampanya.png"
						class="thumbnail center-block" />
				</a>
			</div>
		</div>
	</div>';
		return $t;
	}

	public static function saveCardSettingsForm()
	{
		foreach (EticConfig::$families as $family) {

			if (!Etictools::getValue($family) OR ! is_array(Etictools::getValue($family)))
				continue;
			$installments = Etictools::getValue($family);
			foreach ($installments as $i => $ins) {
				if ($ins['gateway'] == '0') {
					EticInstallment::deletebyFamily($family, $i);
					continue;
				}
				$ins['divisor'] = $i;
				$ins['family'] = $family;
				EticInstallment::save($ins);
			}
		}
		Etictools::rwm('Taksitler Güncellendi !', true, 'success');
	}

	public static function saveToolsForm()
	{
		if (Etictools::getValue('check-oldtables')) {
			if (EticSql::tableExists('spr_bank')) {
				$old_banks = EticSql::getRows('spr_bank');
				if ($old_banks) {
					$old_txt = '';
					foreach ($old_banks as $old_bank) {
						$params = unserialize($old_bank['params']);
						$old_txt .= '<hr/><b>' . $old_bank['ad'] . '</b> Parametreler <br/>';
						foreach ($params['params'] as $k => $v)
							$old_txt .= $k . ' : ' . $v['value'] . '</br>';
					}
					Etictools::rwm('Eski versiyona ait bankalar ' . $old_txt, true, 'success');
				}
			}
			Etictools::rwm('Eski versiyona ait bilgi bulunamadı', true, 'warning');
		} else {
			if (!Etictools::getValue('spr_config_res_cats') OR ! is_array(Etictools::getValue('spr_config_res_cats')))
				Eticconfig::set('SPR_RES_CATS', 'off');
			else
				Eticconfig::set('SPR_RES_CATS', json_encode(Etictools::getValue('spr_config_res_cats')));
			Etictools::rwm('Taksit Kısıtlamaları Güncellendi !', true, 'success');
		}
	}

	public static function saveGatewaySettings()
	{
		if (Etictools::getValue('remove_pos')) {
			$gw = New EticGateway(Etictools::getValue('remove_pos'));
			$gw->delete();
		}

		foreach (EticGateway::getGateways() as $gw) {
			if (Etictools::getValue('submit_for') AND Etictools::getValue('submit_for') != $gw->name)
				continue;
			$data = Etictools::getValue($gw->name);
			if (isset($data['lib']))
				$gw->lib = $data['lib'];
			$lib = EticGateway::$api_libs->{$gw->lib};
			if (!$lib OR ! $data) {
				Etictools::rwm($gw->name . ' Güncelleme hatası tespit edildi. Lütfen formu gözden geçiriniz ' . $gw->lib, true, 'fail');
				continue;
			}
			foreach ($lib->params as $pk => $pv)
				if (isset($data['params'][$pk]))
					$gw->params->{$pk} = $data['params'][$pk];
			$gw->test_mode = isset($data['test_mode']) ? $data['test_mode'] : false;
			if ($gw->name == "paybyme") {
				if (Etictools::getValue('submit_for')) 
					Eticconfig::paybymeInstallment($data["params"]["username"],$data["params"]["token"],$data["params"]["keywordID"]);

			} 
			if ($gw->save()) {
				
				Etictools::rwm($gw->full_name . ' güncellendi', true, 'success');
			}
		}
	}

	public static function paybymeInstallment($username,$password,$keywordId)
		{
			$installment_url = "https://pos.payby.me/webServicesExt/FunctionInstallmentList"; 
			$assetPrice = "10000";
			$currencyCode = "TRY";

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $installment_url);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, "username=$username&password=$password&keywordId=$keywordId&assetPrice=$assetPrice&currencyCode=$currencyCode");
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/x-www-form-urlencoded'
			));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			$server_output = curl_exec($ch);
			$result = json_decode($server_output);
			$ins = array();  

			foreach ($result->InstallmentList as $key => $il) { 
				$ins['gateway'] = "paybyme";
				$ins['rate'] = ($il->lastPriceRatio*100)-100;
				$ins["fee"] = $il->commissionShare;
				$ins['divisor'] = $il->installmentCount;
				$ins['family'] = strtolower($il->program) == "bankkart" ? "combo" : strtolower($il->program) == "miles&smiles" ? "miles-smiles" : strtolower($il->program);
				EticInstallment::save($ins);
				
			}
		}

	public static function saveGeneralSettings()
	{
		//Change stage needs refresh token
		if (isset(EticTools::getValue('spr_config')['MASTERPASS_STAGE']))
			if (EticConfig::get('MASTERPASS_STAGE') != EticTools::getValue('spr_config')['MASTERPASS_STAGE'])
				EticConfig::set('MASTERPASS_LAST_KEYGEN', 0);
		if ($spr_config = EticTools::getValue('spr_config'))
			foreach ($spr_config as $k => $v)
				Eticconfig::set($k, $v);

		EticSql::updateRow('spr_installment', array(
			'rate' => Etictools::getValue('spr_config_default_rate'),
			'fee' => Etictools::getValue('spr_config_default_fee'),
			'gateway' => Etictools::getValue('spr_config_default_gateway')
			), array('family' => 'all'));
	}

	public static function getConfigNotifications()
	{
		//Check
		foreach (EticGateway::getGateways(true) as $gw) {
			if (isset($gw->params->test_mode) && $gw->params->test_mode == 'on')
				Etictools::rwm($gw->full_name . ' <strong>Test Modunda Çalışıyor</strong>');
		}
		if (EticSql::getRow('spr_installment', 'fee', 0)) {
			Etictools::rwm('Taksit tablosundaki maliyetler (Sizden kesilecek oranlar) eksik girilmiş.'
				. '<br/>Bu durum hesaplamada hatalara neden olabilir.');
		}
	}

	public static function cleardebuglogs()
	{
		return EticSql::deleteRows('spr_debug');
	}

	public static function testSys()
	{
		return true;
	}

	public static function getResCats()
	{
		return json_decode(EticConfig::get('SPR_RES_CATS'));
	}

	public static function displayError($body, $title = 'Hata !')
	{
		return '<div class="alert alert-danger"><h2>' . $title . '</h2>' . $body . '</div>';
	}
}
