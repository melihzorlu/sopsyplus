<?php

// Google Chrome Update semsite FIX //
include_once (dirname(__FILE__).'/../tool/SameSiteCookieSetter.php');

foreach($_COOKIE as $k => $v){
	//header('Set-Cookie: '.$k.'='.$v.'; SameSite=None; Secure');	
	SameSiteCookieSetter::setcookie($k, $v, array('secure' => true, 'samesite' => 'None'));
}

// 

include_once (dirname(__FILE__).'/Eticconfig.php');
include_once (dirname(__FILE__).'/Etictools.php');
include_once (dirname(__FILE__).'/Eticgateway.php');
include_once (dirname(__FILE__).'/Eticinstallment.php');
include_once (dirname(__FILE__).'/Eticsql.php');
include_once (dirname(__FILE__).'/Etictransaction.php');
include_once (dirname(__FILE__).'/SanalPosApiClient.php');
include_once (dirname(__FILE__).'/Eticstats.php');
include_once (dirname(__FILE__).'/EticUi.php');
include_once (dirname(__FILE__).'/EticUiOpencart.php');