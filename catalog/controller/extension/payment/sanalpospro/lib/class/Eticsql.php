<?php

// Sql Helper //
class EticSql
{

	function __construct()
	{
		$this->time = time();
	}

	public static function fix($v)
	{
		if (function_exists('mb_ereg_replace'))
			return mb_ereg_replace('[\x00\x0A\x0D\x1A\x22\x25\x27\x5C]', '\\\0', $v);
		return preg_replace('~[\x00\x0A\x0D\x1A\x22\x25\x27\x5C]~u', '\\\$0', $v);
	}

	public static function execute($q)
	{
		$eticdb = new DB(DB_DRIVER, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		return $eticdb->query($q);
	}

	public static function executeS($q)
	{
		$result = EticSql::execute($q);
		if($result && $result->num_rows > 0)
			return $result->rows;
		return false;
	}

	public static function getRow($table, $where, $what = false, $deb = false)
	{
		$q = "SELECT * FROM `" . DB_PREFIX . "$table` WHERE ";
		if (is_array($where)) {
			$i = count($where);
			foreach ($where as $k => $v) {
				$i--;
				$q .= '`' . $k . '` = \'' . EticSql::fix($v) . '\' ';
				if ($i != 0)
					$q .= ' AND ';
			}
		}
		else {
			$q .= "`$where`='" . EticSql::fix($what) . "'";
		}
		
		$query = EticSql::execute($q);
		if ($query AND $query->num_rows) {
			return $query->row;
		} else {
			return false;
		}
	}

	public static function deleteRow($table, $where, $what = null, $deb = false)
	{
		$q = "DELETE FROM `" . DB_PREFIX . "$table` WHERE ";
		if (is_array($where)) {
			$i = count($where);
			foreach ($where as $k => $v) {
				$i--;
				$q .= '`' . $k . '` = \'' . EticSql::fix($v) . '\' ';
				if ($i != 0)
					$q .= ' AND ';
			}
		}
		else {
			$q .= "`$where`='" . EticSql::fix($what) . "'";
		}
		$q .= " LIMIT 1";
		return EticSql::execute($q);
	}

	public static function deleteRows($table, $where, $what)
	{
		$q = "DELETE FROM `" . DB_PREFIX . "$table` WHERE `$where` = '" . EticSql::fix($what) . "'";
		return EticSql::execute($q);
	}

	public static function getRows($table, $where = false, $what = false, $limit = false, $order = false, $deb = false)
	{
		$result = array();
		$q = "SELECT * FROM " . DB_PREFIX . "$table ";
		if (!$where)
			return EticSql::ExecuteS($q);
		else
			$q .= ' WHERE ';
		if (is_array($where)) {
			$i = count($where);
			foreach ($where as $k => $v) {
				$i--;
				$q .= '`' . $k . '` = \'' . EticSql::fix($v) . '\' ';
				if ($i != 0)
					$q .= ' AND ';
			}
		} else
			$q .= " `$where` = '" . EticSql::fix($what) . "' ";
		if ($order) {
			if (is_array($order))
				$q .= "ORDER BY `" . $order['by'] . "` " . $order['type'] . ' ';
			else
				$q .= "ORDER BY `$order` ";
		}
		if ($limit)
			$q .= " LIMIT $limit ";
		return EticSql::ExecuteS($q);
	}

	public static function getRowsAll($table, $limit = "0, 300", $order = false, $order_type = 'ASC')
	{
		$result = array();
		$q = "SELECT * FROM `" . DB_PREFIX . "$table` ";
		if ($order)
			$q .= " ORDER BY `$order` $order_type";
		if ($limit)
			$q .= " LIMIT $limit ";
		return EticSql::ExecuteS($q);
	}

	public static function updateRow($table, $array, $where, $what = null, $deb = false)
	{
		$q = "UPDATE `" . DB_PREFIX . "$table` SET ";
		$i = count($array);
		foreach ($array as $k => $v) {
			$q .= '`' . $k . '` = ' . "'" . EticSql::fix($v) . "'";
			$i--;
			if ($i > 0)
				$q .= " ,\n";
		}
		$q .= ' WHERE ';
		if (is_array($where)) {
			$i = count($where);
			foreach ($where as $k => $v) {
				$i--;
				$q .= '`' . $k . '` = \'' . EticSql::fix($v) . '\' ';
				if ($i != 0)
					$q .= ' AND ';
			}
		} else
			$q .= "`$where` = '" . EticSql::fix($what) . "' LIMIT 1";
//		if($table == 'spr_installment')
//		die($q);
		return EticSql::execute($q);
	}

	public static function insertRow($table, $array, $deb = false)
	{
		$f = '';
		$d = '';
		$q = "INSERT INTO `" . DB_PREFIX . "$table` ( ";
		$i = count($array);
		foreach ($array as $k => $v) {
			if (is_array($v))
				print_r($v);
			$f .= "`" . $k . "`";
			$d .= "'" . EticSql::fix($v) . "'";
			$i--;
			if ($i > 0) {
				$f .= ", ";
				$d .= ", ";
			}
		}
		$q .= $f . ') VALUES (' . $d . ' )';
		if ($deb)
			echo $q;

		$eticdb = new DB(DB_DRIVER, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		if ($eticdb->query($q))
			return $eticdb->getLastId();
		return false;
	}

	public static function Count($table, $where = false, $what = false, $count = false, $deb = false)
	{
		if (!$count)
			$q = 'SELECT COUNT(*) as `total` FROM `' . DB_PREFIX . $table . '`';
		else
			$q = 'SELECT COUNT(`' . $count . '`) as `total` FROM `' . DB_PREFIX . $table . '`';
		if (!$where)
			$q .= "";
		else
			$q .= ' WHERE ';
		if (is_array($where)) {
			$i = count($where);
			foreach ($where as $k => $v) {
				$i--;
				$q .= '`' . $k . '` = \'' . EticSql::fix($v) . '\' ';
				if ($i != 0)
					$q .= ' AND ';
			}
		}
		else if ($where != false AND $what)
			$q .= "`$where` = '" . EticSql::fix($what) . "' ";
		else
			$q .= '';
		return EticSql::execute($q);
	}

	public static function XCount($table, $where = false, $what = false, $q = "", $deb = false)
	{
		$q = "SELECT * FROM `" . DB_PREFIX . "$table` ";
		if (!$where)
			$q .= "";
		else if (is_array($where)) {
			$i = count($where);
			foreach ($where as $k => $v) {
				$i--;
				$q .= '`' . $k . '` = \'' . EticSql::fix($v) . '\' ';
				if ($i != 0)
					$q .= ' AND ';
			}
		} else
			$q .= "`$where` = '$what' LIMIT 1";
		return EticSql::execute($q);
	}

	public static function tableExists($table)
	{

		// Try a select statement against the table
		// Run it in try/catch in case PDO is in ERRMODE_EXCEPTION.
		try {
			$result = EticSql::execute("SELECT 1 FROM " . DB_PREFIX . "$table LIMIT 1");
		} catch (Exception $e) {
			// We got an exception == table not found
			return FALSE;
		}

		// Result is either boolean FALSE (no table found) or PDOStatement Object (table found)
		return $result !== FALSE;
	}
}

?>