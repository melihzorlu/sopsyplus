<?php

class EticUiOpencart extends EticUi
{

	function __construct()
	{
		$this->store_uri = HTTPS_CATALOG;
		$this->store_url = HTTPS_CATALOG;
		$this->uri = HTTPS_CATALOG.'catalog/view/theme/default/image/sanalpospro/';
		$this->url = HTTPS_CATALOG.'catalog/view/theme/default/image/sanalpospro/';
	}

	public function l($txt)
	{ // translate
		return $txt;
	}

	public function displayPrice($price, $currency = null)
	{
		return $price . ' ' . $currency;
	}

	public function addCSS($file, $external = false)
	{
		return '<link rel="stylesheet" href="' . $this->uri . $file . '" type="text/css" media="all">';
	}

	public function displayProductInstallments($price)
	{
//		$product = New Product(Etictools::getValue('id_product'));
//		if (EticInstallment::getProductRestriction($product->id_category_default))
//			return '<section><br/><div class="alert alert-info">Bu ürün kanun gereği taksitli olarak satılamamaktadır.'
//				. ' Kredi kartınızdan taksitsiz olarak ödeyebilirsiniz.</div></section>';
		$prices = EticInstallment::getRates($price);
		if (count($prices) < 1)
			return;
		$return = '<div class="row">';
		$block_count = 0;
		foreach ($prices as $f => $v) {
			$block_count++;
			if ($block_count == 4) {
				$return .= '</div><div class="row">';
			}
			$return .= '<div class="col-lg-4 col-sm-4 col-xs-6 eticsoft_spr_bank">
				<div class="eticsoft_inst_container ' . $f . '">
					<div class="block_title" align="center"><img src="' . $this->uri . 'img/cards/' . $f . '.png"></div>';
			$return .= '<table class="table">
						<tr>
							<th>' . $this->l('Ins.') . '</th>
							<th>' . $this->l('Monthly') . '</th>
							<th>' . $this->l('Total') . '</th>
						</tr>';
			foreach ($v as $k => $ins) {
				$return .= '<tr class="' . ($k % 2 ? $f . '-odd' : '' ) . '">
				<td>' . $k . '</td>
				<td>' . EticUiWoo::displayPrice($ins['month']) . '</td>
				<td>' . EticUiWoo::displayPrice($ins['total']) . '</td>
			</tr>';
			}
			$return .= '</table></div></div>';
		}
		$return .= '<div class="col-lg-4 col-sm-4 col-xs-6 eticsoft_spr_bank">
				<div class="eticsoft_inst_container">
					<div class="block_title"><h3>' . $this->l('Other Cards') . '</h3></div>
					' . $this->l('You can pay ones way (without installment) via all visa/mastercard/amex credit cards, also debit cards') . '
					<hr/>
					<img class="col-sm-12 img-responsive" src="' . $this->uri . 'img/master_visa_aexpress.png"/>
					</div>
					</div>';
		$return .= '</div></section>';
		return $return;
	}

	public function displayAdminOrder($tr)
	{
		$cur = Etictools::getCurrency($tr->id_currency, 'iso_number');
		$currency = $cur->iso_code;

		$t = '
		<div class="eticsoft">
			<div align="center">
				<h2 align="center" class="spp_head">
					Kart işlem detayları
				</h2>

				<table class="table ">
					<tr>
						<td>İşlem Tarihi<br/>
						<span class="badge">' . $tr->date_update . '</span>'
					. '</td>'
					. '<td>#' . $tr->boid . '</td>
					</tr>

					<tr>
						<td colspan="2">
							' . $this->l('IP Address') . ' <span class="badge">' . $tr->cip . '</span> <br/>
							İşlem ID <span class="badge">' . $tr->boid . '</span>
						
						</td>
					</tr>
					<tr>
						<td colspan="2" align="center" >
							<img src="' . $this->url . 'gateways/' . $tr->gateway . '.png" width="150px" />
						</td>
					</tr>
				</table>

			</div>
				<hr/>
			<div align="center">
				<h2 align="center" class="spp_head">
					Kart Bilgileri
				</h2>
				<table class="table">

					<tr>
						<td>Taksit</td>
						<td>' . $tr->installment . '</td>
					</tr>
					<tr>
						<td>Kart Sahibi</td>
						<td>' . $tr->cc_name . '</td>
					</tr>
					<tr>
						<td>Kart Numarası</td>
						<td>' . $tr->cc_number . '</td>
					</tr>
				</table>
			</div>
			<hr/>

				</div>
			</div>';
		return $t;
	}
}
