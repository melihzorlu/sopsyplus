<?php
class ControllerExtensionModuleYoMenu extends Controller {
	public function index($setting) {
		static $module = 1;

		if (!isset($setting['yomenu']['store_id']) || !in_array($this->config->get('config_store_id'), $setting['yomenu']['store_id'])) {
			return;
		}

		if (!isset($setting['yomenu']['all_customers'])) {
			if (!$this->customer->isLogged() || !isset($setting['yomenu']['customer_group_id']) || !in_array($this->config->get('config_customer_group_id'), $setting['yomenu']['customer_group_id'])) {
				return;
			}
		}

		if (isset($this->request->get['path'])) {
			$parts = explode('_', (string)$this->request->get['path']);
		} else {
			$parts = array();
		}

		if (!isset($setting['yomenu']['location']) && isset($this->request->get['path'])) {
			if (!isset($setting['yomenu']['fcid']) || !in_array((int)end($parts), $setting['yomenu']['fcid'])) {
				return;
			}
		}

		$this->load->language('extension/module/yo_menu');

		$data['menu_design'] = $setting['yomenu']['menu_design'];

		if (file_exists('catalog/view/theme/' . $this->config->get('config_template') . '/stylesheet/yo-menu/yo-' . $data['menu_design'] . '.css')) {
			$this->document->addStyle('catalog/view/theme/' . $this->config->get('config_template') . '/stylesheet/yo-menu/yo-' . $data['menu_design'] . '.css');
		} else {
			$this->document->addStyle('catalog/view/theme/default/stylesheet/yo-menu/yo-' . $data['menu_design'] . '.css');
		}

		if ($data['menu_design'] == 'am') {
			$this->document->addScript('catalog/view/javascript/jquery/yo-menu/jquery.easing.1.3.js');
		}

		if ($data['menu_design'] == 'fm') {
			$this->document->addScript('catalog/view/javascript/jquery/yo-menu/jquery.menu-aim.js');
		}

		$data['button_back'] = $this->language->get('button_back');
		$data['menu_type'] = $setting['yomenu']['menu_type'];

		$data['minimized'] = $setting['yomenu']['minimized'];

		$data['count'] = !empty($setting['yomenu']['count']) ? $setting['yomenu']['count'] : '';
		$data['current_count'] = !empty($setting['yomenu']['current_count']) ? $setting['yomenu']['current_count'] : '';

		$data['level_2'] = !empty($setting['yomenu']['level_2']) ? $setting['yomenu']['level_2'] : '';
		$data['level_3'] = !empty($setting['yomenu']['level_3']) ? $setting['yomenu']['level_3'] : '';
		$data['level_4'] = !empty($setting['yomenu']['level_4']) ? $setting['yomenu']['level_4'] : '';
		$data['level_5'] = !empty($setting['yomenu']['level_5']) ? $setting['yomenu']['level_5'] : '';

		$data['current_level_3'] = !empty($setting['yomenu']['current_level_3']) ? $setting['yomenu']['current_level_3'] : '';
		$data['current_level_4'] = !empty($setting['yomenu']['current_level_4']) ? $setting['yomenu']['current_level_4'] : '';
		$data['current_level_5'] = !empty($setting['yomenu']['current_level_5']) ? $setting['yomenu']['current_level_5'] : '';

		$data['am_icon'] = !empty($setting['yomenu']['am_icon']) ? $setting['yomenu']['am_icon'] : '';
		$data['pm_icon'] = !empty($setting['yomenu']['pm_icon']) ? $setting['yomenu']['pm_icon'] : '';
		$data['fm_icon'] = !empty($setting['yomenu']['fm_icon']) ? $setting['yomenu']['fm_icon'] : '';
		$data['toggle'] = !empty($setting['yomenu']['toggle']) ? $setting['yomenu']['toggle'] : '';
		$data['save_view'] = !empty($setting['yomenu']['save_view']) ? $setting['yomenu']['save_view'] : '0';

		$data['products_by_category'] = $setting['yomenu']['products_by_category'];
		$data['products_by_brand'] = $setting['yomenu']['products_by_brand'];

		$data['item_description'] = $setting['yomenu']['item_description'];
		$data['description_limit'] = !empty($setting['yomenu']['description_limit']) ? $setting['yomenu']['description_limit'] : '-1';
		$data['flyout_design'] = $setting['yomenu']['flyout_design'];
		$data['column'] = !empty($setting['yomenu']['column']) ? $setting['yomenu']['column'] : '3';
		$data['image_position'] = !empty($setting['yomenu']['image_position']) ? $setting['yomenu']['image_position'] : 'center';

		$data['item_image'] = $setting['yomenu']['item_image'];
		$data['image_width'] = !empty($setting['yomenu']['image_width']) ? $setting['yomenu']['image_width'] : '200';
		$data['image_height'] = !empty($setting['yomenu']['image_height']) ? $setting['yomenu']['image_height'] : '200';
		$data['item_banner'] = $setting['yomenu']['item_banner'];
		$data['banner_width'] = !empty($setting['yomenu']['banner_width']) ? $setting['yomenu']['banner_width'] : '200';
		$data['banner_height'] = !empty($setting['yomenu']['banner_height']) ? $setting['yomenu']['banner_height'] : '200';

		$data['easing'] = $setting['yomenu']['easing'];
		$data['easing_d'] = !empty($setting['yomenu']['easing_d']) ? (int)$setting['yomenu']['easing_d'] : '200';

		$data['class'] = !empty($setting['yomenu']['class']) ? $setting['yomenu']['class'] : 'yo-menu';

		$this->load->model('catalog/yo_menu');
		$this->load->model('catalog/product');
		$this->load->model('tool/image');

		$data['manufacturers'] = array();
		$data['categories'] = array();

		if ($setting['yomenu']['menu_type'] == 'tree') {

			if ($setting['yomenu'][$this->config->get('config_language_id')]['title']) {
				$data['heading_title'] = $setting['yomenu'][$this->config->get('config_language_id')]['title'];
			} elseif ($setting['yomenu']['minimized']) {
				$data['heading_title'] = $this->language->get('heading_title');
			} else {
				$data['heading_title'] = '';
			}

			if (!$setting['yomenu']['all_categories']) {
				if (!empty($setting['yomenu']['featured_category'])) {
					$categories = $setting['yomenu']['featured_category'];
				} else {
					$categories = array();
				}
			}

			if ($setting['yomenu']['all_categories']) {
				$categories = $this->model_catalog_yo_menu->getCategories(0);
			}

			foreach ($categories as $category) {
				if (!$setting['yomenu']['all_categories']) {
					$category = $this->model_catalog_yo_menu->getCategory($category);
				}

				if ($category) {

					if ($data['level_2']) {

						$children1_data = array();

						if (!empty($setting['yomenu']['subcat_limit'])) {
							$children1 = array_slice($this->model_catalog_yo_menu->getCategories($category['category_id']), 0, $setting['yomenu']['subcat_limit']);
						} else {
							$children1 = $this->model_catalog_yo_menu->getCategories($category['category_id']);
						}

						foreach ($children1 as $child1) {

							if ($data['level_3']) {

								$children2_data = array();

								if (!empty($setting['yomenu']['subcat_limit'])) {
									$children2 = array_slice($this->model_catalog_yo_menu->getCategories($child1['category_id']), 0, $setting['yomenu']['subcat_limit']);
								} else {
									$children2 = $this->model_catalog_yo_menu->getCategories($child1['category_id']);
								}

								foreach ($children2 as $child2) {

									if ($data['level_4']) {

										$children3_data = array();

										if (!empty($setting['yomenu']['subcat_limit'])) {
											$children3 = array_slice($this->model_catalog_yo_menu->getCategories($child2['category_id']), 0, $setting['yomenu']['subcat_limit']);
										} else {
											$children3 = $this->model_catalog_yo_menu->getCategories($child2['category_id']);
										}

										foreach ($children3 as $child3) {

											if ($data['level_5']) {

												$children4_data = array();

												if (!empty($setting['yomenu']['subcat_limit'])) {
													$children4 = array_slice($this->model_catalog_yo_menu->getCategories($child3['category_id']), 0, $setting['yomenu']['subcat_limit']);
												} else {
													$children4 = $this->model_catalog_yo_menu->getCategories($child3['category_id']);
												}

												foreach ($children4 as $child4) {

													$filter_data = array(
														'filter_category_id'  => $child4['category_id'],
														'filter_sub_category' => true
														);

													$children4_data[] = array(
														'category_id' => $child4['category_id'],
														'name'        => $child4['name'],
														'href'        => $this->url->link('product/category', !empty($category['parent_id']) ? 'path=' . $category['parent_id'] . '_' . $category['category_id'] . '_' . $child1['category_id'] . '_' . $child2['category_id'] . '_' . $child3['category_id'] . '_' . $child4['category_id'] : 'path=' . $category['category_id'] . '_' . $child1['category_id'] . '_' . $child2['category_id'] . '_' . $child3['category_id'] . '_' . $child4['category_id']),
														'count'       => $data['count'] ? $this->model_catalog_product->getTotalProducts($filter_data) : '',
														'active'      => in_array($child4['category_id'], $parts),
														'icon'        => $child4['yomenu_icon']
														);

												}

											}

											$filter_data = array(
												'filter_category_id'  => $child3['category_id'],
												'filter_sub_category' => true
												);

											$children3_data[] = array(
												'category_id' => $child3['category_id'],
												'name'        => $child3['name'],
												'href'        => $this->url->link('product/category', !empty($category['parent_id']) ? 'path=' . $category['parent_id'] . '_' . $category['category_id'] . '_' . $child1['category_id'] . '_' . $child2['category_id'] . '_' . $child3['category_id'] : 'path=' . $category['category_id'] . '_' . $child1['category_id'] . '_' . $child2['category_id'] . '_' . $child3['category_id']),
												'count'       => $data['count'] ? $this->model_catalog_product->getTotalProducts($filter_data) : '',
												'active'      => in_array($child3['category_id'], $parts),
												'icon'        => $child3['yomenu_icon'],
												'thumb'       => $this->model_tool_image->resize($child3['image'], $data['image_width'], $data['image_height']),
												'children4'   => $data['level_5'] ? $children4_data : ''
												);

										}

									}

									$filter_data = array(
										'filter_category_id'  => $child2['category_id'],
										'filter_sub_category' => true
										);

									$children2_data[] = array(
										'category_id' => $child2['category_id'],
										'name'        => $child2['name'],
										'href'        => $this->url->link('product/category', !empty($category['parent_id']) ? 'path=' . $category['parent_id'] . '_' . $category['category_id'] . '_' . $child1['category_id'] . '_' . $child2['category_id'] : 'path=' . $category['category_id'] . '_' . $child1['category_id'] . '_' . $child2['category_id']),
										'count'       => $data['count'] ? $this->model_catalog_product->getTotalProducts($filter_data) : '',
										'active'      => in_array($child2['category_id'], $parts),
										'icon'        => $child2['yomenu_icon'],
										'thumb'       => $this->model_tool_image->resize($child2['image'], $data['image_width'], $data['image_height']),
										'children3'   => $data['level_4'] ? $children3_data : ''
										);

								}

							}

							$filter_data = array(
								'filter_category_id'  => $child1['category_id'],
								'filter_sub_category' => true
								);

							$children1_data[] = array(
								'category_id' => $child1['category_id'],
								'name'        => $child1['name'],
								'href'        => $this->url->link('product/category', !empty($category['parent_id']) ? 'path=' . $category['parent_id'] . '_' . $category['category_id'] . '_' . $child1['category_id'] : 'path=' . $category['category_id'] . '_' . $child1['category_id']),
								'count'       => $data['count'] ? $this->model_catalog_product->getTotalProducts($filter_data) : '',
								'active'      => in_array($child1['category_id'], $parts),
								'icon'        => $child1['yomenu_icon'],
								'thumb'       => $this->model_tool_image->resize($child1['image'], $data['image_width'], $data['image_height']),
								'children2'   => $data['level_3'] ? $children2_data : ''
								);

						}

					}

					$filter_data = array(
						'filter_category_id'  => $category['category_id'],
						'filter_sub_category' => true
						);

					$data['categories'][] = array(
						'category_id' => $category['category_id'],
						'name'        => $category['name'],
						'href'        => $this->url->link('product/category', !empty($category['parent_id']) ? 'path=' . $category['parent_id'] . '_' . $category['category_id'] : 'path=' . $category['category_id']),
						'count'       => $data['count'] ? $this->model_catalog_product->getTotalProducts($filter_data) : '',
						'active'      => in_array($category['category_id'], $parts),
						'icon'        => $category['yomenu_icon'],
						'description' => utf8_substr(strip_tags(html_entity_decode($category['description'], ENT_QUOTES, 'UTF-8')), 0, $data['description_limit']) . ($category['description'] && $data['description_limit'] > 0 ? '...' : ''),
						'thumb'       => $this->model_tool_image->resize($category['image'], $data['image_width'], $data['image_height']),
						'banner'      => $this->model_tool_image->resize($category['yomenu_image'], $data['banner_width'], $data['banner_height']),
						'children1'   => $data['level_2'] ? $children1_data : ''
						);
				}
			}
		}

		if ($setting['yomenu']['menu_type'] == 'current' || $setting['yomenu']['menu_type'] == 'parent') {

			if ($setting['yomenu']['menu_type'] == 'current') {
				$current_id = (int)end($parts);
			} else {
				$current_id = (int)array_shift($parts);
			}

			$current = $this->model_catalog_yo_menu->getCategory($current_id);

			if ($setting['yomenu'][$this->config->get('config_language_id')]['title']) {
				$data['heading_title'] = $setting['yomenu'][$this->config->get('config_language_id')]['title'];
			} elseif (!empty($current_id)) {
				$data['heading_title'] = $current['name'];
			} else {
				$data['heading_title'] = '';
			}

			$results = $this->model_catalog_yo_menu->getCategories($current_id);

			foreach ($results as $result) {

				if ($setting['yomenu']['products_by_category']) {
					$data['button_cart'] = $this->language->get('button_cart');

					$products_sort_order = explode('-', $setting['yomenu']['products_sort']);
					$sort = $products_sort_order['0'];
					$order = $products_sort_order['1'];

					$products_data = array(
						'filter_category_id' => $result['category_id'],
						'sort'               => $sort,
						'order'              => $order,
						'start'              => 0,
						'limit'              => $setting['yomenu']['products_limit']
						);

					$children2_data = array();
					$children2 = $this->model_catalog_product->getProducts($products_data);

					foreach ($children2 as $child2) {

						if ($child2['image']) {
							$image = $this->model_tool_image->resize($child2['image'], $data['image_width'], $data['image_height']);
						} else {
							$image = false;
						}

						if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
							$price = $this->currency->format($this->tax->calculate($child2['price'], $child2['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
						} else {
							$price = false;
						}

						if ((float)$child2['special']) {
							$special = $this->currency->format($this->tax->calculate($child2['special'], $child2['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
						} else {
							$special = false;
						}

						if ($this->config->get('config_review_status')) {
							$rating = $child2['rating'];
						} else {
							$rating = false;
						}

						if ($setting['yomenu']['menu_type'] == 'current') {
							$href = $this->url->link('product/product', isset($this->request->get['path']) ? 'path=' . $this->request->get['path'] . '_' . $result['category_id'] . '&product_id=' . $child2['product_id'] : 'path=' . $result['category_id'] . '&product_id=' . $child2['product_id']);
						} else {
							$href = $this->url->link('product/product', isset($this->request->get['path']) ? 'path=' . $current_id . '_' . $result['category_id'] . '&product_id=' . $child2['product_id'] : 'path=' . $result['category_id'] . '&product_id=' . $child2['product_id']);
						}

						$children2_data[] = array(
							'product_id' => $child2['product_id'],
							'name'       => $child2['name'],
							'href'       => $href,
							'thumb'      => $image,
							'price'      => $price,
							'special'    => $special,
							'rating'     => $rating,
							'reviews'    => sprintf($this->language->get('text_reviews'), (int)$child2['reviews']),
							'active'     => false,
							'children3'  => false
							);
					}
				} else {
					if ($data['current_level_3']) {

						$children2_data = array();

						if (!empty($setting['yomenu']['current_subcat_limit'])) {
							$children2 = array_slice($this->model_catalog_yo_menu->getCategories($result['category_id']), 0, $setting['yomenu']['current_subcat_limit']);
						} else {
							$children2 = $this->model_catalog_yo_menu->getCategories($result['category_id']);
						}

						foreach ($children2 as $child2) {

							if ($data['current_level_4']) {

								$children3_data = array();

								if (!empty($setting['yomenu']['current_subcat_limit'])) {
									$children3 = array_slice($this->model_catalog_yo_menu->getCategories($child2['category_id']), 0, $setting['yomenu']['current_subcat_limit']);
								} else {
									$children3 = $this->model_catalog_yo_menu->getCategories($child2['category_id']);
								}

								foreach ($children3 as $child3) {

									if ($data['current_level_5']) {

										$children4_data = array();

										if (!empty($setting['yomenu']['current_subcat_limit'])) {
											$children4 = array_slice($this->model_catalog_yo_menu->getCategories($child3['category_id']), 0, $setting['yomenu']['current_subcat_limit']);
										} else {
											$children4 = $this->model_catalog_yo_menu->getCategories($child3['category_id']);
										}

										foreach ($children4 as $child4) {
											$filter_data = array(
												'filter_category_id'  => $child4['category_id'],
												'filter_sub_category' => true
												);

											if ($setting['yomenu']['menu_type'] == 'current') {
												$href4 = $this->url->link('product/category', isset($this->request->get['path']) ? 'path=' . $this->request->get['path'] . '_' . $result['category_id'] . '_' . $child2['category_id'] . '_' . $child3['category_id'] . '_' . $child4['category_id'] : 'path=' . $result['category_id'] . '_' . $child2['category_id'] . '_' . $child3['category_id'] . '_' . $child4['category_id']);
											} else {
												$href4 = $this->url->link('product/category', isset($this->request->get['path']) ? 'path=' . $current_id . '_' . $result['category_id'] . '_' . $child2['category_id'] . '_' . $child3['category_id'] . '_' . $child4['category_id'] : 'path=' . $result['category_id'] . '_' . $child2['category_id'] . '_' . $child3['category_id'] . '_' . $child4['category_id']);
											}

											$children4_data[] = array(
												'name'        => $child4['name'],
												'href'        => $href4,
												'active'      => in_array($child4['category_id'], $parts),
												'count'       => $data['current_count'] ? $this->model_catalog_product->getTotalProducts($filter_data) : '',
												'icon'        => $child4['yomenu_icon']
												);
										}
									}

									$filter_data = array(
										'filter_category_id'  => $child3['category_id'],
										'filter_sub_category' => true
										);

									if ($setting['yomenu']['menu_type'] == 'current') {
										$href3 = $this->url->link('product/category', isset($this->request->get['path']) ? 'path=' . $this->request->get['path'] . '_' . $result['category_id'] . '_' . $child2['category_id'] . '_' . $child3['category_id'] : 'path=' . $result['category_id'] . '_' . $child2['category_id'] . '_' . $child3['category_id']);
									} else {
										$href3 = $this->url->link('product/category', isset($this->request->get['path']) ? 'path=' . $current_id . '_' . $result['category_id'] . '_' . $child2['category_id'] . '_' . $child3['category_id'] : 'path=' . $result['category_id'] . '_' . $child2['category_id'] . '_' . $child3['category_id']);
									}

									$children3_data[] = array(
										'category_id' => $child3['category_id'],
										'name'        => $child3['name'],
										'href'        => $href3,
										'active'      => in_array($child3['category_id'], $parts),
										'count'       => $data['current_count'] ? $this->model_catalog_product->getTotalProducts($filter_data) : '',
										'icon'        => $child3['yomenu_icon'],
										'thumb'       => $this->model_tool_image->resize($child3['image'], $data['image_width'], $data['image_height']),
										'children4'   => $data['current_level_5'] ? $children4_data : ''
										);
								}
							}

							$filter_data = array(
								'filter_category_id'  => $child2['category_id'],
								'filter_sub_category' => true
								);

							if ($setting['yomenu']['menu_type'] == 'current') {
								$href2 = $this->url->link('product/category', isset($this->request->get['path']) ? 'path=' . $this->request->get['path'] . '_' . $result['category_id'] . '_' . $child2['category_id'] : 'path=' . $result['category_id'] . '_' . $child2['category_id']);
							} else {
								$href2 = $this->url->link('product/category', isset($this->request->get['path']) ? 'path=' . $current_id . '_' . $result['category_id'] . '_' . $child2['category_id'] : 'path=' . $result['category_id'] . '_' . $child2['category_id']);
							}

							$children2_data[] = array(
								'category_id' => $child2['category_id'],
								'name'        => $child2['name'],
								'href'        => $href2,
								'active'      => in_array($child2['category_id'], $parts),
								'icon'        => $child2['yomenu_icon'],
								'thumb'       => $this->model_tool_image->resize($child2['image'], $data['image_width'], $data['image_height']),
								'count'       => $data['current_count'] ? $this->model_catalog_product->getTotalProducts($filter_data) : '',
								'children3'   => $data['current_level_4'] ? $children3_data : ''
								);
						}
					}
				}

				$filter_data = array(
					'filter_category_id'  => $result['category_id'],
					'filter_sub_category' => true
					);

				if ($setting['yomenu']['menu_type'] == 'current') {
					$href = $this->url->link('product/category', isset($this->request->get['path']) ? 'path=' . $this->request->get['path'] . '_' . $result['category_id'] :  'path=' . $result['category_id']);
				} else {
					$href = $this->url->link('product/category', isset($this->request->get['path']) ? 'path=' . $current_id . '_' . $result['category_id'] :  'path=' . $result['category_id']);
				}

				$data['categories'][] = array(
					'category_id' => $result['category_id'],
					'name'        => $result['name'],
					'href'        => $href,
					'active'      => in_array($result['category_id'], $parts),
					'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $data['description_limit']) . ($result['description'] && $data['description_limit'] > 0 ? '...' : ''),
					'count'       => $data['current_count'] ? $this->model_catalog_product->getTotalProducts($filter_data) : '',
					'icon'        => $result['yomenu_icon'],
					'thumb'       => $this->model_tool_image->resize($result['image'], $data['image_width'], $data['image_height']),
					'banner'      => $this->model_tool_image->resize(($result['yomenu_image'] ? $result['yomenu_image'] : $result['image']), $data['banner_width'], $data['banner_height']),
					'children2'   => $data['current_level_3'] || $setting['yomenu']['products_by_category'] ? $children2_data : ''
					);
			}
		}

		if ($setting['yomenu']['menu_type'] == 'brands') {

			$this->load->model('catalog/manufacturer');

			if ($setting['yomenu'][$this->config->get('config_language_id')]['title']) {
				$data['heading_title'] = $setting['yomenu'][$this->config->get('config_language_id')]['title'];
			} elseif ($setting['yomenu']['minimized']) {
				$data['heading_title'] = $this->language->get('text_brand');
			} else {
				$data['heading_title'] = '';
			}

			if (isset($this->request->get['manufacturer_id'])) {
				$data['manufacturer_id'] = (int)$this->request->get['manufacturer_id'];
			} else {
				$data['manufacturer_id'] = 0;
			}

			if (isset($this->request->get['product_id'])) {
				$data['product_id'] = (int)$this->request->get['product_id'];
			} else {
				$data['product_id'] = 0;
			}

			$product_info = $this->model_catalog_product->getProduct($data['product_id']);
			$data['bids'] = $product_info['manufacturer_id'];

			if (!$setting['yomenu']['all_brands']) {
				if (!empty($setting['yomenu']['featured_brands'])) {
					$manufacturers = $setting['yomenu']['featured_brands'];
				} else {
					$manufacturers = array();
				}
			}

			if ($setting['yomenu']['all_brands']) {
				$manufacturers = $this->model_catalog_manufacturer->getManufacturers();
			}

			foreach ($manufacturers as $manufacturer) {
				if (!$setting['yomenu']['all_brands']) {
					$manufacturer = $this->model_catalog_manufacturer->getManufacturer($manufacturer);
				}

				if ($manufacturer) {
					if ($data['products_by_brand']) {
						$data['button_cart'] = $this->language->get('button_cart');

						$products_sort_order = explode('-', $setting['yomenu']['products_by_brand_sort']);
						$sort = $products_sort_order['0'];
						$order = $products_sort_order['1'];

						$products_data = array(
							'filter_manufacturer_id' => $manufacturer['manufacturer_id'],
							'sort'                   => $sort,
							'order'                  => $order,
							'start'                  => 0,
							'limit'                  => $setting['yomenu']['products_by_brand_limit']
							);

						$children_data = array();
						$children = $this->model_catalog_product->getProducts($products_data);

						foreach ($children as $child) {

							if ($child['image']) {
								$image = $this->model_tool_image->resize($child['image'], $data['image_width'], $data['image_height']);
							} else {
								$image = false;
							}

							if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
								$price = $this->currency->format($this->tax->calculate($child['price'], $child['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
							} else {
								$price = false;
							}

							if ((float)$child['special']) {
								$special = $this->currency->format($this->tax->calculate($child['special'], $child['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
							} else {
								$special = false;
							}

							if ($this->config->get('config_review_status')) {
								$rating = $child['rating'];
							} else {
								$rating = false;
							}

							$children_data[] = array(
								'product_id' => $child['product_id'],
								'name'       => $child['name'],
								'href'       => $this->url->link('product/product', 'manufacturer_id=' . $manufacturer['manufacturer_id'] . '&product_id=' . $child['product_id']),
								'thumb'      => $image,
								'price'      => $price,
								'special'    => $special,
								'rating'     => $rating,
								'reviews'    => sprintf($this->language->get('text_reviews'), (int)$child['reviews']),
								'active'     => $child['product_id'] == $data['product_id'] ? 1 : 0
								);

						}

					}

					$data['manufacturers'][] = array(
						'manufacturer_id' => $manufacturer['manufacturer_id'],
						'name'            => $manufacturer['name'],
						'href'            => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $manufacturer['manufacturer_id']),
						'description'     => isset($manufacturer['description']) ? utf8_substr(strip_tags(html_entity_decode($manufacturer['description'], ENT_QUOTES, 'UTF-8')), 0, $data['description_limit']) . ($manufacturer['description'] && $data['description_limit'] > 0 ? '...' : '') : '',
						'active'          => $manufacturer['manufacturer_id'] == $data['manufacturer_id'] ? 1 : 0,
						'thumb'           => $this->model_tool_image->resize($manufacturer['image'], $data['image_width'], $data['image_height']),
						'banner'          => $this->model_tool_image->resize($manufacturer['ym_banner'], $data['banner_width'], $data['banner_height']),
						'children'        => $data['products_by_brand'] ? $children_data : ''
						);
				}
			}
		}

		$data['module'] = $module++;

		if ($data['categories'] || $data['manufacturers']) {
			return $this->load->view('extension/module/yo_menu/yo_' . $data['menu_design'], $data);
		}
	}
}