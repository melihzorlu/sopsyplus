<?php
class product
{

    public function __construct($registry)
    {
        $this->registry = $registry;
        $this->db = $registry->get('db');
        $this->config = $registry->get('config');
        $this->request = $registry->get('request');
        $this->session = $registry->get('session');

    }

    public function dump(){
        /*if(isset($this->session->data['user_id'])){
            if($this->session->data['user_id'] == 3){
                if($data){
                    var_dump($data); die();
                }
            }
        }*/
        return 'adsdaadssa';
    }

    public function dump2($data = ""){
        if(isset($this->session->data['user_id'])){
            if($this->session->data['user_id'] == 3){
                if(isset($data)){
                    var_dump($data);
                }
            }
        }
    }

    public function delFileIn($dir)
    {
        if (is_dir($dir)) {
            if ($handle = opendir($dir)) {
                while (($file = readdir($handle)) !== false) {
                    if ($file != "." && $file != ".." && $file != "Thumbs.db") {
                        //echo '<a target="_blank" href="' .  . '">' . $file . '</a><br>' . "\n";
                        unlink($dir . $file);
                    }
                }
                closedir($handle);
            }
        }
    }

    public function downloadXML($xml_url = false, $file_name = '', $folder = '')
    {
        if (!$file_name)
            $file_name = @date("Ymd-His") . '.xml';

        $feed_data = array();

        if ($xml_url) {
            $feed_data['xml_url'] = $xml_url;
        }

        $xml_dir = DIR_DOWNLOAD . 'xml/';
        if (!is_dir($xml_dir)) {
            mkdir($xml_dir);
        }

        if($folder){
            if (!is_dir($xml_dir . $folder)) {
                mkdir($xml_dir . $folder);
            }
            $xml_dir = $xml_dir . $folder;
        }

        $feed_data['xml_url'] = htmlspecialchars_decode($feed_data['xml_url']);


        $save_xml_name = $xml_dir . $file_name;


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $feed_data['xml_url']);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
        $content = curl_exec($ch);

        if ($content == false) {
            $content = file_get_contents($feed_data['xml_url']);
        }

        $last_chars = substr($content, -1000);
        $last_tag = explode('</', $last_chars);
        $last_tag = $last_tag[count($last_tag) - 1];
        $last_tag = '</' . $last_tag;
        $content = str_replace($last_tag, "\n" . $last_tag, $content);
        file_put_contents($save_xml_name, $content);

        return $save_xml_name;
    }

    public function priceEditFormat($price)
    {
        // virgüllü para biçimini noktalı biçime çevirir
        return str_replace(',', '.', $price);
    }

    public function taxPriceCalc($price, $tax_rate = 1.18)
    {
        return $price / $tax_rate;
    }

    public function addManufacturer($name)
    {
        $manufacture_id = 0;
        $manufacturer_data = array();
        $manufacturer = $this->getManufacturerName($name);

        if (!$manufacturer) {
            $manufacturer_description[0] = array(
                'custom_title' => ''
            );
            $manufacturer_data = array(
                'name' => $name,
                'sort_order' => 0,
                'manufacturer_description' => $manufacturer_description
            );
            $manufacture_id = $this->addManufacturerDb($manufacturer_data);
        } else {
            $manufacture_id = $manufacturer['manufacturer_id'];
        }

        return $manufacture_id;
    }

    public function getManufacturerName($name)
    {
        return $this->db->query("SELECT * FROM oc_manufacturer WHERE name='" . $this->db->escape($name) . "' ")->row;
    }

    public function addManufacturerDb($data)
    {
        $this->db->query("INSERT INTO oc_manufacturer SET 
        name = '" . $this->db->escape($data['name']) . "', 
        sort_order = '" . (int)$data['sort_order'] . "' ");

        $manufacturer_id = $this->db->getLastId();

        if (isset($data['manufacturer_description'])) {
            foreach ($data['manufacturer_description'] as $language_id => $value) {
                $this->db->query("INSERT INTO oc_manufacturer_description SET 
                manufacturer_id = '" . (int)$manufacturer_id . "', 
                language_id = '" . (int)$language_id . "', 
                custom_title = '" . $this->db->escape($value['custom_title']) . "' ");
            }
        }

        $this->db->query("INSERT INTO oc_manufacturer_to_store SET 
        manufacturer_id = '" . (int)$manufacturer_id . "', 
        store_id = '0'");


    }

    public function askCategory($category_name)
    {
        $sql = "SELECT * FROM oc_category c 
        LEFT JOIN oc_category_description cd ON(c.category_id = cd.category_id)
        WHERE cd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND cd.name = '" . $this->db->escape($category_name) . "'  ";
        return $this->db->query($sql)->row;
    }

    public function addCategory($category_path, $marker)
    {
        $k_store[0] = 0;
        $product_category = array();
        $xml_kategories = explode($marker, $category_path);

        $c_id = 0;
        $c_id1 = 0;
        $c_id2 = 0;
        $c_id3 = 0;

        if (isset($xml_kategories[0])) {
            $cat = $this->askCategory($xml_kategories[0]);
            if ($cat) {
                $product_category[$cat['category_id']] = $cat['category_id'];
                $c_id = $cat['category_id'];
            } else {
                if (!empty($xml_kategories[0])) {
                    $k_description = array();
                    foreach ($this->product->getLanguages() as $lnag) {
                        $k_description[$lnag['language_id']] = array('name' => $xml_kategories[0], 'description' => '', 'meta_title' => $xml_kategories[0], 'meta_description' => '', 'meta_keyword' => '', );
                    }
                    $category_data = array(
                        'parent_id' => 0,
                        'top' => 0,
                        'column' => 1,
                        'sort_order' => 0,
                        'status' => 1,
                        'category_store' => $k_store,
                        'category_description' => $k_description,
                    );
                    $c_id = $this->addCategoryDb($category_data);
                    $product_category[$c_id] = $c_id;
                }
            }
        }

        if (isset($xml_kategories[1])) {
            $cat1 = $this->askCategory($xml_kategories[1]);
            if ($cat1) {
                $product_category[$cat1['category_id']] = $cat1['category_id'];
                $c1_id = $cat1['category_id'];
            } else {
                if (!empty($xml_kategories[1])) {
                    $k_description = array();
                    foreach ($this->product->getLanguages() as $lnag) {
                        $k_description[$lnag['language_id']] = array('name' => $xml_kategories[1], 'description' => '', 'meta_title' => $xml_kategories[1], 'meta_description' => '', 'meta_keyword' => '', );
                    }
                    $category_data = array(
                        'parent_id' => $c_id,
                        'top' => 0,
                        'column' => 1,
                        'sort_order' => 0,
                        'status' => 1,
                        'category_store' => $k_store,
                        'category_description' => $k_description,
                    );
                    $c1_id = $this->addCategoryDb($category_data);
                    $product_category[$c1_id] = $c1_id;
                }
            }
        }


        if (isset($xml_kategories[2])) {
            $cat2 = $this->askCategory($xml_kategories[2]);
            if ($cat2) {
                $product_category[$cat2['category_id']] = $cat2['category_id'];
                $c2_id = $cat2['category_id'];
            } else {

                if (!empty($xml_kategories[2])) {
                    $k_description = array();
                    foreach ($this->product->getLanguages() as $lnag) {
                        $k_description[$lnag['language_id']] = array('name' => $xml_kategories[2], 'description' => '', 'meta_title' => $xml_kategories[2], 'meta_description' => '', 'meta_keyword' => '', );
                    }
                    $category_data = array(
                        'parent_id' => $c1_id,
                        'top' => 0,
                        'column' => 1,
                        'sort_order' => 0,
                        'status' => 1,
                        'category_store' => $k_store,
                        'category_description' => $k_description,
                    );
                    $c2_id = $this->addCategoryDb($category_data);
                    $product_category[$c2_id] = $c2_id;
                }

            }
        }


        if (isset($xml_kategories[3])) {
            $cat3 = $this->askCategory($xml_kategories[3]);
            if ($cat3) {
                $product_category[$cat3['category_id']] = $cat3['category_id'];
                $c3_id = $cat3['category_id'];
            } else {
                if (!empty($xml_kategories[3])) {
                    $k_description = array();
                    foreach ($this->product->getLanguages() as $lnag) {
                        $k_description[$lnag['language_id']] = array('name' => $xml_kategories[3], 'description' => '', 'meta_title' => $xml_kategories[3], 'meta_description' => '', 'meta_keyword' => '', );
                    }
                    $category_data = array(
                        'parent_id' => $c2_id,
                        'top' => 0,
                        'column' => 1,
                        'sort_order' => 0,
                        'status' => 1,
                        'category_store' => $k_store,
                        'category_description' => $k_description,
                    );
                    $c3_id = $this->addCategoryDb($category_data);
                    $product_category[$c3_id] = $c3_id;
                }
            }
        }

        return $product_category;

    }

    public function addCategoryDb($data)
    {
        $this->db->query("INSERT INTO oc_category SET 
        parent_id = '" . (int)$data['parent_id'] . "', 
        `top` = '" . (isset($data['top']) ? (int)$data['top'] : 0) . "', 
        `column` = '" . (int)$data['column'] . "', 
        sort_order = '" . (int)$data['sort_order'] . "', 
        status = '" . (int)$data['status'] . "', 
        date_added = NOW() ");

        $category_id = $this->db->getLastId();

        if (isset($data['category_description'])) {
            foreach ($data['category_description'] as $language_id => $value) {
                $this->db->query("INSERT INTO oc_category_description SET 
                category_id = '" . (int)$category_id . "', 
                language_id = '" . (int)$language_id . "', 
                name = '" . $this->db->escape($value['name']) . "', 
                description = '" . $this->db->escape($value['description']) . "', 
                meta_title = '" . $this->db->escape($value['meta_title']) . "', 
                meta_description = '" . $this->db->escape($value['meta_description']) . "', 
                meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
            }
        }

        if (isset($data['category_store'])) {
            foreach ($data['category_store'] as $store_id) {
                $this->db->query("INSERT INTO oc_category_to_store SET 
                category_id = '" . (int)$category_id . "', 
                store_id = '" . (int)$store_id . "'");
            }
        }

        return $category_id;

    }

    public function getLanguages()
    {
        $language_data = array();
        $query = $this->db->query("SELECT * FROM oc_language WHERE status = '1' ORDER BY sort_order, name");

        foreach ($query->rows as $result) {
            $language_data[$result['code']] = array(
                'language_id' => $result['language_id'],
                'name' => $result['name'],
                'code' => $result['code'],
                'locale' => $result['locale'],
                'image' => $result['image'],
                'directory' => $result['directory'],
                'sort_order' => $result['sort_order'],
                'status' => $result['status']
            );
        }

        return $language_data;

    }

    public function getDefaultLanguageId()
    {
        $config_lang = $this->db->query("SELECT * FROM oc_setting s WHERE s.key = 'config_language' ")->row;
        return $this->db->query("SELECT * FROM oc_language WHERE code = '". $this->db->escape($config_lang['value']) ."' ")->row['language_id'];
    }

    public function getTaxClassId($tax_rate)
    {
        return $this->db->query("SELECT * FROM oc_tax_rate WHERE rate = '" . $tax_rate . "' ")->row['tax_rate_id'];
    }

    public function saveImage($image, $file_path = '', $model)
    {
        $orginal_image = $image;
        $image = explode('/', $image);
        $image = end($image);
        $ext = pathinfo($image, PATHINFO_EXTENSION);
        if (!file_exists(DIR_IMAGE . $file_path . $model . '.' . $ext)) {
            if (@copy($orginal_image, DIR_IMAGE . $file_path . $model . '.' . $ext)) {
                return $file_path . $model . '.' . $ext;
            }
        } else {
            return $file_path . $model . '.' . $ext;
        }
    }

    public function saveImages($images, $file_path = '', $model)
    {   //error_reporting(0);
        $return_images = array();
        $ix = 0;
        foreach ($images as $key => $image) {
            $image_up = false;
            $orginal_image = $image;
            $image = $this->delSpace($image . PHP_EOL);
            $image = explode('/', $image);
            $image = end($image);
            $ext = pathinfo($image, PATHINFO_EXTENSION);
            if (!file_exists(DIR_IMAGE . $file_path . $model . '-' . $ix . '.' . $ext)) {
                if (@copy($orginal_image, DIR_IMAGE . $file_path . $model . '-' . $ix . '.' . $ext)) {
                    $image_up = true;
                }
            } else {
                $image_up = true;
            }

            if ($image_up) {
                $return_images[] = array(
                    'image' => $file_path . $model . '-' . $ix . '.' . $ext,
                    'sort_order' => $ix,
                );
            }
            $ix++;

        }
        return $return_images;

    }

    public function delSpace($text)
    {
        $text = rtrim($text);
        $text = ltrim($text);
        return $text;
    }

    public function getStockStatusId($quantity)
    {
        if ($quantity == 0)
            return 1;
    }

    public function getCurrencyId($code)
    {
        return $this->db->query("SELECT DISTINCT * FROM oc_currency WHERE code = '" . $this->db->escape($code) . "'")->row['currency_id'];

    }

    public function addProduct($data)
    {

        $this->db->query("INSERT INTO oc_product SET 
        model = '" . $this->db->escape($data['model']) . "', 
        barcode = '" . $this->db->escape($data['barcode']) . "', 
        sku = '" . $this->db->escape($data['sku']) . "', 
        quantity = '" . (int)$data['quantity'] . "', 
        image = '" . $this->db->escape($data['image']) . "', 
        minimum = '" . (int)$data['minimum'] . "', 
        subtract = '" . (int)$data['subtract'] . "', 
        stock_status_id = '" . (int)$data['stock_status_id'] . "', 
        date_available = '" . $this->db->escape($data['date_available']) . "', 
        manufacturer_id = '" . (int)$data['manufacturer_id'] . "', 
        shipping = '" . (int)$data['shipping'] . "', 
        price = '" . (float)$data['price'] . "', 
        currency_id = '" . (int)$data['currency_id'] . "', 
        points = '" . (int)$data['points'] . "', 
        weight = '" . (float)$data['weight'] . "', 
        weight_class_id = '" . (int)$data['weight_class_id'] . "', 
        length = '" . (float)$data['length'] . "', 
        width = '" . (float)$data['width'] . "', 
        height = '" . (float)$data['height'] . "', 
        length_class_id = '" . (int)$data['length_class_id'] . "', 
        status = '" . (int)$data['status'] . "', 
        tax_class_id = '" . (int)$data['tax_class_id'] . "', 
        sort_order = '" . (int)$data['sort_order'] . "', 
        date_added = NOW()");

        $product_id = $this->db->getLastId();

        if(isset($data['new'])){
            $this->db->query("UPDATE oc_product SET 
            new = '" . $data['new'] . "' 
            WHERE product_id = '" . (int)$product_id . "'");
        }

        if(isset($data['nebim_product_code'])){
            $this->db->query("UPDATE oc_product SET 
            nebim_product_code = '" . $this->db->escape($data['nebim_product_code']) . "' 
            WHERE product_id = '" . (int)$product_id . "'");
        }

        if(isset($data['ItemDim1Code'])){
            $this->db->query("UPDATE oc_product SET 
            ItemDim1Code = '" . $this->db->escape($data['ItemDim1Code']) . "' 
            WHERE product_id = '" . (int)$product_id . "'");
        }

        if(isset($data['ColorCode'])){
            $this->db->query("UPDATE oc_product SET 
            ColorCode = '" . $this->db->escape($data['ColorCode']) . "' 
            WHERE product_id = '" . (int)$product_id . "'");
        }


        if (isset($data['product_description']) and $data['product_description']) {
            //$this->db->query("DELETE FROM oc_product_description WHERE product_id = '" . (int)$product_id . "' ");
            foreach ($data['product_description'] as $language_id => $value) {
                $this->db->query("INSERT INTO oc_product_description SET 
                product_id = '" . (int)$product_id . "', 
                language_id = '" . (int)$language_id . "', 
                name = '" . $this->db->escape($value['name']) . "', 
                description = '" . $this->db->escape($value['description']) . "', 
                tag = '" . $this->db->escape($value['tag']) . "', 
                meta_title = '" . $this->db->escape($value['meta_title']) . "', 
                meta_description = '" . $this->db->escape($value['meta_description']) . "', 
                meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "' ");
            }
        }


        //$this->db->query("DELETE FROM oc_product_to_store WHERE product_id = '" . (int)$product_id . "' ");
        $this->db->query("INSERT INTO oc_product_to_store SET product_id = '" . (int)$product_id . "', store_id = '0' ");

        if (isset($data['product_special']) AND $data['product_special']) {

            foreach ($data['product_special'] as $product_special) {
                $this->db->query("INSERT INTO oc_product_special SET 
                product_id = '" . (int)$product_id . "', 
                customer_group_id = '" . (int)$product_special['customer_group_id'] . "', 
                price = '" . (float)$product_special['price'] . "' ");
            }
        }

        if (isset($data['product_image']) AND $data['product_image']) {
            foreach ($data['product_image'] as $product_image) {
                $this->db->query("INSERT INTO oc_product_image SET 
                product_id = '" . (int)$product_id . "', 
                image = '" . $this->db->escape($product_image['image']) . "', 
                sort_order = '" . (int)$product_image['sort_order'] . "'");
            }
        }

        if (isset($data['product_category'])) {
            foreach ($data['product_category'] as $category_id) {
                $check_cat_to_product = $this->db->query("SELECT * FROM oc_product_to_category where product_id = '" . (int)$product_id . "'AND category_id = '" . (int)$category_id . "'");
                if ($check_cat_to_product->num_rows == 0) {
                    $parent_row = $this->db->query("SELECT parent_id FROM oc_category WHERE category_id = '" . (int)$category_id . "'");
                    if ($parent_row->num_rows > 0) {
                        if ((int)$parent_row->row['parent_id'] > 0) {
                            $check_cat_to_product = $this->db->query("SELECT * FROM oc_product_to_category WHERE product_id = '" . (int)$product_id . "'AND category_id = '" . (int)$parent_row->row['parent_id'] . "'");
                            if ($check_cat_to_product->num_rows == 0) {

                                $this->db->query("INSERT INTO oc_product_to_category SET product_id ='" . (int)$product_id . "',category_id ='" . (int)$parent_row->row['parent_id'] . "'");
                            }
                            $parent_sec_row = $this->db->query("SELECT parent_id FROM pd_category WHERE category_id = '" . (int)$parent_row->row['parent_id'] . "'");
                            if ($parent_sec_row->num_rows > 0) {
                                if ((int)$parent_sec_row->row['parent_id'] > 0) {
                                    $check_cat_to_product = $this->db->query("SELECT * FROM oc_product_to_category WHERE product_id = '" . (int)$product_id . "'AND category_id = '" . (int)$parent_sec_row->row['parent_id'] . "'");
                                    if ($check_cat_to_product->num_rows == 0) {
                                        $this->db->query("INSERT INTO oc_product_to_category SET 
                                        product_id ='" . (int)$product_id . "',
                                        category_id ='" . (int)$parent_sec_row->row['parent_id'] . "'");
                                    }
                                }
                            }
                        }
                    }
                    $this->db->query("INSERT INTO oc_product_to_category SET 
                    product_id = '" . (int)$product_id . "', 
                    category_id = '" . (int)$category_id . "'");
                }
            }
        }

        if (isset($data['product_option'])) {
            foreach ($data['product_option'] as $product_option) {
                if ($product_option['type'] == 'select' || $product_option['type'] == 'radio' || $product_option['type'] == 'checkbox' || $product_option['type'] == 'image') {
                    if (isset($product_option['product_option_value'])) {
                        $this->db->query("INSERT INTO oc_product_option SET 
                        product_id = '" . (int)$product_id . "', 
                        option_id = '" . (int)$product_option['option_id'] . "', 
                        required = '" . (int)$product_option['required'] . "'");

                        $product_option_id = $this->db->getLastId();

                        foreach ($product_option['product_option_value'] as $product_option_value) {
                            $this->db->query("INSERT INTO oc_product_option_value SET 
                            product_option_id = '" . (int)$product_option_id . "', 
                            product_id = '" . (int)$product_id . "', 
                            option_id = '" . (int)$product_option['option_id'] . "', 
                            option_value_id = '" . (int)$product_option_value['option_value_id'] . "', 
                            quantity = '" . (int)$product_option_value['quantity'] . "', 
                            subtract = '" . (int)$product_option_value['subtract'] . "', 
                            option_value_barcode = '" . (int)$product_option_value['option_value_barcode'] . "', 
                            price = '" . (float)$product_option_value['price'] . "', 
                            price_prefix = '" . $this->db->escape($product_option_value['price_prefix']) . "', 
                            points = '" . (int)$product_option_value['points'] . "', 
                            points_prefix = '" . $this->db->escape($product_option_value['points_prefix']) . "', 
                            weight = '" . (float)$product_option_value['weight'] . "', 
                            weight_prefix = '" . $this->db->escape($product_option_value['weight_prefix']) . "',
                            date_added = NOW() ");

                            $product_option_value_id = $this->db->getLastId();
                            if(isset($data['ColorCode']) AND isset($data['ItemDim1Code'])){
                                $this->db->query("UPDATE oc_product_option_value SET 
                                    ItemDim1Code = '" . $this->db->escape($product_option_value['ItemDim1Code']) . "',
                                    ColorCode = '" . $this->db->escape($product_option_value['ColorCode']) . "', 
                                    ItemCode = '" . $this->db->escape($product_option_value['ItemCode']) . "' WHERE product_option_value_id = '". $product_option_value_id ."' ");
                            }
                        }
                    }
                } else {
                    $this->db->query("INSERT INTO oc_product_option SET 
                    product_id = '" . (int)$product_id . "', 
                    option_id = '" . (int)$product_option['option_id'] . "', 
                    value = '" . $this->db->escape($product_option['value']) . "', 
                    required = '" . (int)$product_option['required'] . "' ");
                }
            }
        }

        if (isset($data['product_attribute'])) {

            foreach ($data['product_attribute'] as $product_attribute) {
                if ($product_attribute['attribute_id']) {
                    $this->db->query("DELETE FROM oc_product_attribute WHERE product_id = '" . (int)$product_id . "' AND attribute_id = '" . (int)$product_attribute['attribute_id'] . "'");
                    foreach ($product_attribute['product_attribute_description'] as $language_id => $product_attribute_description) {
                        $this->db->query("DELETE FROM oc_product_attribute WHERE product_id = '" . (int)$product_id . "' AND attribute_id = '" . (int)$product_attribute['attribute_id'] . "' AND language_id = '" . (int)$language_id . "'");
                        $this->db->query("INSERT INTO oc_product_attribute SET product_id = '" . (int)$product_id . "', attribute_id = '" . (int)$product_attribute['attribute_id'] . "', language_id = '" . (int)$language_id . "', text = '" . $this->db->escape($product_attribute_description['text']) . "'");
                    }
                }
            }

        }


        return $product_id;
    }

    public function editProduct($data, $product_id)
    {

        $this->db->query("UPDATE oc_product SET
        quantity = '" . (int)$data['quantity'] . "',   
        sku = '" . $data['sku'] . "', 
        price = '" . (float)$data['price'] . "', 
        currency_id = '" . (int)$data['currency_id'] . "',
        manufacturer_id = '" . (int)$data['manufacturer_id'] . "',   
        tax_class_id = '" . (int)$data['tax_class_id'] . "',    
        date_modified = NOW() 
        WHERE product_id = '" . (int)$product_id . "'");

        /*if(isset($data['status'])){
            $this->db->query("UPDATE oc_product SET status = '" . (int)$data['status'] . "' WHERE product_id = '" . (int)$product_id . "'");
        }*/

        if(isset($data['nebim_product_code'])){
            $this->db->query("UPDATE oc_product SET 
            nebim_product_code = '" . $this->db->escape($data['nebim_product_code']) . "' 
            WHERE product_id = '" . (int)$product_id . "'");
        }

        if(isset($data['ItemDim1Code'])){
            $this->db->query("UPDATE oc_product SET 
            ItemDim1Code = '" . $this->db->escape($data['ItemDim1Code']) . "' 
            WHERE product_id = '" . (int)$product_id . "'");
        }

        if(isset($data['ColorCode'])){
            $this->db->query("UPDATE oc_product SET 
            ColorCode = '" . $this->db->escape($data['ColorCode']) . "' 
            WHERE product_id = '" . (int)$product_id . "'");
        }


        if (isset($data['image']) AND $data['image']) {
            $this->db->query("UPDATE oc_product SET 
            image = '" . $this->db->escape($data['image']) . "' 
            WHERE product_id = '" . (int)$product_id . "'");
        }


        if (!empty($data['product_attribute']) AND $data['product_attribute']) {
            $this->db->query("DELETE FROM oc_product_attribute WHERE product_id = '" . (int)$product_id . "'");
            foreach ($data['product_attribute'] as $product_attribute) {
                if ($product_attribute['attribute_id']) {
                    $this->db->query("DELETE FROM oc_product_attribute WHERE product_id = '" . (int)$product_id . "' AND attribute_id = '" . (int)$product_attribute['attribute_id'] . "'");
                    foreach ($product_attribute['product_attribute_description'] as $language_id => $product_attribute_description) {
                        $this->db->query("INSERT INTO oc_product_attribute SET
                        product_id = '" . (int)$product_id . "',
                        attribute_id = '" . (int)$product_attribute['attribute_id'] . "',
                        language_id = '" . (int)$language_id . "',
                        text = '" . $this->db->escape($product_attribute_description['text']) . "' ");
                    }
                }
            }
        }



        if (isset($data['product_option']) AND $data['product_option']) {
            $this->db->query("DELETE FROM oc_product_option WHERE product_id = '" . (int)$product_id . "'");
            $this->db->query("DELETE FROM oc_product_option_value WHERE product_id = '" . (int)$product_id . "'");

            foreach ($data['product_option'] as $product_option) {
                if ($product_option['type'] == 'select' || $product_option['type'] == 'radio' || $product_option['type'] == 'checkbox' || $product_option['type'] == 'image') {
                    if (isset($product_option['product_option_value'])) {
                        $this->db->query("INSERT INTO oc_product_option SET 
                        product_option_id = '" . (int)$product_option['product_option_id'] . "', 
                        product_id = '" . (int)$product_id . "', 
                        option_id = '" . (int)$product_option['option_id'] . "', 
                        required = '" . (int)$product_option['required'] . "' ");

                        $product_option_id = $this->db->getLastId();

                        foreach ($product_option['product_option_value'] as $product_option_value) {
                            $this->db->query("INSERT INTO oc_product_option_value SET 
                            product_option_value_id = '" . (int)$product_option_value['product_option_value_id'] . "', 
                            product_option_id = '" . (int)$product_option_id . "', 
                            product_id = '" . (int)$product_id . "', 
                            option_id = '" . (int)$product_option['option_id'] . "', 
                            option_value_id = '" . (int)$product_option_value['option_value_id'] . "', 
                            quantity = '" . (int)$product_option_value['quantity'] . "', 
                            subtract = '" . (int)$product_option_value['subtract'] . "', 
                            option_value_barcode = '" . (int)$product_option_value['option_value_barcode'] . "',
                            customer_group_id = '1',
                            price = '" . (float)$product_option_value['price'] . "', 
                            price_prefix = '" . $this->db->escape($product_option_value['price_prefix']) . "', 
                            points = '" . (int)$product_option_value['points'] . "', 
                            points_prefix = '" . $this->db->escape($product_option_value['points_prefix']) . "', 
                            weight = '" . (float)$product_option_value['weight'] . "', 
                            weight_prefix = '" . $this->db->escape($product_option_value['weight_prefix']) . "',
                            date_modified = NOW() ");
                            $product_option_value_id = $this->db->getLastId();
                            if(isset($data['ColorCode']) AND isset($data['ItemDim1Code'])){
                                $this->db->query("UPDATE oc_product_option_value SET 
                                    ItemDim1Code = '" . $this->db->escape($product_option_value['ItemDim1Code']) . "',
                                    ColorCode = '" . $this->db->escape($product_option_value['ColorCode']) . "', 
                                    ItemCode = '" . $this->db->escape($product_option_value['ItemCode']) . "' WHERE product_option_value_id = '". $product_option_value_id ."' ");
                            }
                        }
                    }
                } else {
                    $this->db->query("INSERT INTO oc_product_option SET 
                    product_option_id = '" . (int)$product_option['product_option_id'] . "', 
                    product_id = '" . (int)$product_id . "', 
                    option_id = '" . (int)$product_option['option_id'] . "', 
                    value = '" . $this->db->escape($product_option['value']) . "', 
                    required = '" . (int)$product_option['required'] . "' ");
                }
            }
        }



        if (isset($data['product_discount']) AND $data['product_discount']) {
            $this->db->query("DELETE FROM oc_product_discount WHERE product_id = '" . (int)$product_id . "'");
            foreach ($data['product_discount'] as $product_discount) {
                $this->db->query("INSERT INTO oc_product_discount SET 
                product_id = '" . (int)$product_id . "', 
                customer_group_id = '" . (int)$product_discount['customer_group_id'] . "', 
                quantity = '" . (int)$product_discount['quantity'] . "', 
                priority = '" . (int)$product_discount['priority'] . "', 
                price = '" . (float)$product_discount['price'] . "', 
                date_start = '" . $this->db->escape($product_discount['date_start']) . "', 
                date_end = '" . $this->db->escape($product_discount['date_end']) . "' ");
            }
        }



        if (isset($data['product_special']) AND $data['product_special']) {
            $this->db->query("DELETE FROM oc_product_special WHERE product_id = '" . (int)$product_id . "'");
            foreach ($data['product_special'] as $product_special) {
                $this->db->query("INSERT INTO oc_product_special SET 
                product_id = '" . (int)$product_id . "', 
                customer_group_id = '" . (int)$product_special['customer_group_id'] . "',  
                price = '" . (float)$product_special['price'] . "'
                 ");
            }
        }else{
            //$this->db->query("DELETE FROM oc_product_special WHERE product_id = '" . (int)$product_id . "'");
        }

        return $product_id;

    }

    public function asdads(){
        $a= 'daadasd';
        return $a;
    }

    public function getOptionIdByName($option_name)
    {
        $option_info = $this->db->query("SELECT * FROM oc_option_description WHERE name = '". $this->db->escape($option_name) ."' AND language_id = '". $this->getDefaultLanguageId() ."' ");
        if(isset($option_info['option_id'])){
            return $option_info['option_id'];
        }else{
            return '';
        }

    }

    public function getOptionValueIdByName($option_value_name, $option_id)
    {
        $option_val = $this->db->query("SELECT * FROM oc_option_value_description WHERE name = '". $this->db->escape($option_value_name) ."' AND option_id = '". $option_id ."' AND language_id = '". $this->getDefaultLanguageId() ."' ");
        if(isset($option_val->row['option_value_id'])){
            return $option_val->row['option_value_id'];
        }else{
            if($option_value_name != ''){
                return $this->addOptionValue($option_value_name, $option_id);
            }else{
                return '';
            }

        }
    }

    public function addOptionValue($option_value_name, $option_id)
    {
        $this->db->query("INSERT INTO oc_option_value SET 
		option_id = '" . (int)$option_id . "', 
		image = '', 
		sort_order = '0'");

        $option_value_id = $this->db->getLastId();

        foreach ($this->getLanguages() as $lang){
            $this->db->query("INSERT INTO oc_option_value_description SET 
            option_value_id = '" . (int)$option_value_id . "', 
            language_id = '" . (int)$lang['language_id'] . "', 
            option_id = '" . (int)$option_id . "', 
            name = '" . $this->db->escape($option_value_name) . "' ");
        }

        return $option_value_id;

    }

    public function getProductOptionId($product_id, $option_id)
    {

        $product_option_query = $this->db->query("SELECT po.product_option_id FROM oc_product_option po 
        LEFT JOIN oc_option o ON (po.option_id = o.option_id) 
        WHERE po.product_id = '" . (int)$product_id . "' AND po.option_id = '" . (int)$option_id . "'  ");
        if(isset($product_option_query->row['product_option_id'])){
            return $product_option_query->row['product_option_id'];
        }else{
            return '';
        }

    }

    public function getProductOptionValueId($product_id, $option_id, $product_option_id, $option_value_id)
    {
        $product_option_value_query = $this->db->query("SELECT * FROM oc_product_option_value pov 
            LEFT JOIN oc_option_value ov ON(pov.option_value_id = ov.option_value_id) 
            WHERE pov.product_option_id = '" . (int)$product_option_id . "' AND
            pov.product_id = '" . (int)$product_id . "' AND
            pov.option_id = '" . (int)$option_id . "' AND
            pov.option_value_id = '" . (int)$option_value_id . "'
            ");
        if(isset($product_option_value_query->row['product_option_value_id']))
            return $product_option_value_query->row['product_option_value_id'];
        else
            return '';

    }

    public function getAttributeId($attribute_group_id, $attribute_name)
    {
        $attribute_info = $this->db->query("SELECT a.attribute_id FROM oc_attribute a 
        LEFT JOIN oc_attribute_description ad ON (a.attribute_id = ad.attribute_id)
        WHERE a.attribute_group_id = '". (int)$attribute_group_id ."' AND ad.name = '". $this->db->escape($attribute_name) ."' AND ad.language_id = '". $this->getDefaultLanguageId() ."' ");
        if(isset($attribute_info->row['attribute_id'])){
            return $attribute_info->row['attribute_id'];
        }else{
            if($attribute_name != ''){
                return $this->addAttribute($attribute_group_id, $attribute_name);
            }else{
                return '';
            }
        }
    }

    public function addAttribute($attribute_group_id, $attribute_name)
    {
        $this->db->query("INSERT INTO oc_attribute SET attribute_group_id = '" . (int)$attribute_group_id . "', sort_order = '0' ");
        $attribute_id = $this->db->getLastId();
        foreach ($this->getLanguages() as $lang){
            $this->db->query("INSERT INTO oc_attribute_description SET 
            attribute_id = '" . (int)$attribute_id . "', 
            language_id = '" . (int)$lang['language_id'] . "', 
            name = '" . $this->db->escape($attribute_name) . "'");
        }

        return $attribute_id;
    }

    public function isProductExist($model)
    {
        $query = $this->db->query("SELECT * FROM oc_product WHERE model = '" . $model . "' ")->row;
        if (isset($query['product_id'])) {
            return $query['product_id'];
        } else {
            return 0;
        }

    }

    public function getSpecialPrice($price = 0)
    {
        $customer_group_info = $this->db->query("SELECT * FROM oc_setting s WHERE s.key = 'config_customer_group_id' ")->row;
        if ($price) {
            $special = array();
            $special[] = array(
                'customer_group_id' => isset($customer_group_info['value']) ? $customer_group_info['value'] : 1,
                'price' => $price
            );
            return $special;
        } else {
            return false;
        }

    }

    public function addProcessLine($text)
    {
        echo $text . '<br>';

    }

    public function addOption($data)
    {
        $this->db->query("INSERT INTO oc_option SET 
        type = '" . $this->db->escape($data['type']) . "', 
        sort_order = '" . (int)$data['sort_order'] . "' ");

        $option_id = $this->db->getLastId();

        foreach ($data['option_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO oc_option_description SET 
            option_id = '" . (int)$option_id . "', 
            language_id = '" . (int)$language_id . "', 
            name = '" . $this->db->escape($value['name']) . "' ");
        }

        if (isset($data['option_value'])) {
            foreach ($data['option_value'] as $option_value) {
                $this->db->query("INSERT INTO oc_option_value SET 
                option_id = '" . (int)$option_id . "', 
                image = '" . $this->db->escape(html_entity_decode($option_value['image'], ENT_QUOTES, 'UTF-8')) . "', 
                sort_order = '" . (int)$option_value['sort_order'] . "' ");

                $option_value_id = $this->db->getLastId();

                foreach ($option_value['option_value_description'] as $language_id => $option_value_description) {
                    $this->db->query("INSERT INTO oc_option_value_description SET 
                    option_value_id = '" . (int)$option_value_id . "', 
                    language_id = '" . (int)$language_id . "', 
                    option_id = '" . (int)$option_id . "', 
                    name = '" . $this->db->escape($option_value_description['name']) . "' ");
                }
            }
        }

        return $option_id;
    }

    public function editOption($option_id, $data)
    {
        /*
		$this->db->query("UPDATE oc_option SET
        type = '" . $this->db->escape($data['type']) . "',
        sort_order = '" . (int)$data['sort_order'] . "'
        WHERE option_id = '" . (int)$option_id . "' ");

		$this->db->query("DELETE FROM oc_option_description WHERE option_id = '" . (int)$option_id . "'");

		foreach ($data['option_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO oc_option_description SET
            option_id = '" . (int)$option_id . "',
            language_id = '" . (int)$language_id . "',
            name = '" . $this->db->escape($value['name']) . "' ");
		}

		$this->db->query("DELETE FROM oc_option_value WHERE option_id = '" . (int)$option_id . "'");
		$this->db->query("DELETE FROM oc_option_value_description WHERE option_id = '" . (int)$option_id . "'");

		if (isset($data['option_value'])) {
			foreach ($data['option_value'] as $option_value) {
				if ($option_value['option_value_id']) {
					$this->db->query("INSERT INTO oc_option_value SET
                    option_value_id = '" . (int)$option_value['option_value_id'] . "',
                    option_id = '" . (int)$option_id . "',
                    image = '" . $this->db->escape(html_entity_decode($option_value['image'], ENT_QUOTES, 'UTF-8')) . "',
                    sort_order = '" . (int)$option_value['sort_order'] . "' ");
				} else {
					$this->db->query("INSERT INTO oc_option_value SET
                    option_id = '" . (int)$option_id . "',
                    image = '" . $this->db->escape(html_entity_decode($option_value['image'], ENT_QUOTES, 'UTF-8')) . "',
                    sort_order = '" . (int)$option_value['sort_order'] . "' ");
				}

				$option_value_id = $this->db->getLastId();

				foreach ($option_value['option_value_description'] as $language_id => $option_value_description) {
					$this->db->query("INSERT INTO oc_option_value_description SET
                    option_value_id = '" . (int)$option_value_id . "',
                    language_id = '" . (int)$language_id . "',
                    option_id = '" . (int)$option_id . "',
                    name = '" . $this->db->escape($option_value_description['name']) . "'");
				}
			}

		}*/
    }

    /*public function permalinkimage($deger)
    {
        $turkce = array("ş", "Ş", "ı", "(", ") ", "‘", "ü", "Ü", "ö", "Ö", "ç", "Ç", " ",  " /",  " *",  " ?", "ş", "Ş", "ı", "ğ", "Ğ", "İ", "ö", "Ö", "Ç", "ç", "ü", "Ü");
        $duzgun =array("s", "S", "i", "", "", "", "u", "U", "o", "O", "c", "C",  " -",  " -",  " -", "", "s", "S", "i", "g", "G", "I", "o", "O", "C", "c", "u", "U");
        $deger =str_replace($turkce ,$duzgun ,$deger);
        $deger = preg_replace( "@[ ^ A -Z a -z 0 - 9 -_ ] +@i" ,"" ,$deger);
        return $deger;
    }*/

    public function getCategoryParentId($category_id)
    {
        return $this->db->query("SELECT parent_id FROM oc_category WHERE category_id=".(int)$category_id);

    }
    public function deneme(){

        return 'sdadas';
    }



}
