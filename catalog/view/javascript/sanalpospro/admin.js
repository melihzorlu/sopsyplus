$(document).ready(function () {
    $("select.inst_select").change(function () {
        var cname = $(this).attr("id");
        if ($(this).val() == 0)
            $("." + cname).hide();
        else
            $("." + cname).show();
    });

    $("select.inst_select_all").change(function () {
        var cnameall = $(this).attr("id");
        if ($(this).val() == 0) {
            $("select." + cnameall).val(0);
            $("input.input_" + cnameall).hide();
        } else {
            $("select." + cnameall).val($(this).val());
            $("input.input_" + cnameall).show()
        }
    });
    $("select.inst_select").change();
});

