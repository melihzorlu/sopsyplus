<?php if (!isset($j3)): ?>
  <style>
    body {
      display: none !important;
    }
  </style>
  <script>
    window.location = 'index.php?route=journal3/startup/error';
  </script>
<?php endif; ?>
<?php echo $j3->loadController('journal3/layout', 'bottom'); ?>

<?php if (!$j3->document->isPopup()): ?>
<footer>
  <?php echo $footer_menu; ?>
</footer>

<?php endif; ?>
<style>
.sopsy{
  padding:10px;
  margin-bottom: 10px;
}
.sopsy a{
  text-decoration: none;
}
.sopsy span {
  font-size: 12px;
  color: #000000;
  letter-spacing: 0;
  display: inline-block;
  vertical-align: middle;
  margin-right: 10px;
}
.sopsy img {
  display: inline-block;
  vertical-align: middle;
  width: 55px;
  margin: 0px 0 -9px 0;
}
</style>
<div class="col-md-2 col-md-offset-10 col-xs-12 col-lg-2 col-lg-offset-10 sopsy">
  <a href="http://www.sopsy.com/" target="_blank">
  <span>Powered by</span>
  <img src="https://dukkan.im/Fox/t2/images/sopsy-logo.svg" alt="">
</a>
</div>

</div><!-- .site-wrapper -->

<?php echo $j3->loadController('journal3/layout', 'bottom_menu'); ?>

<?php echo $j3->loadController('journal3/layout', 'side_menu'); ?>

<?php echo $j3->loadController('journal3/layout', 'notification'); ?>

<?php foreach ($j3->document->getScripts('footer') as $script): ?>
<script src="<?php echo $j3->document->staticUrl($script, false); ?>" <?php if ($j3->settings->get('performanceJSDefer')): ?>defer<?php endif; ?>></script>
<?php endforeach; ?>

<?php if ($j3->canLiveReload()): ?>
<script src="http://<?php echo $j3->getHost(); ?>:35729/livereload.js?snipver=1" async></script>
<?php endif; ?>

<?php if ($j3->settings->get('customJS')): ?>
<script type="text/javascript"><?php echo $j3->settings->get('customJS'); ?></script>
<?php endif; ?>

<?php if (!$j3->document->isPopup() && $j3->settings->get('customCodeFooter')): ?>
<?php echo $j3->settings->get('customCodeFooter'); ?>
<?php endif; ?>

<?php if (!$j3->document->isPopup() && $j3->settings->get('scrollTop')): ?>
<div class="scroll-top">
  <i class="fa fa-angle-up"></i>
</div>
<?php endif; ?>

</body>
</html>
