<?php echo $header; ?>

<div id="container" class="container j-container">

  <ul class="breadcrumb">

    <?php foreach ($breadcrumbs as $breadcrumb) { ?>

    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>

    <?php } ?>

  </ul>

  <div class="row"><?php echo $column_left; ?><?php echo $column_right; ?>

    <?php if ($column_left && $column_right) { ?>

    <?php $class = 'col-sm-6'; ?>

    <?php } elseif ($column_left || $column_right) { ?>

    <?php $class = 'col-sm-9'; ?>

    <?php } else { ?>

    <?php $class = 'col-sm-12'; ?>

    <?php } ?>

    <div id="content" class="<?php echo $class; ?>">

      <h1 class="heading-title"><?php echo $promotion_title; ?></h1>

      <?php echo $content_top; ?>

          

            <?php if(!empty($promotion_image)) { ?>

            <div id="image"><img style="border: 1px solid #efefef;" src="<?php echo $promotion_image; ?>"></div>

            <?php } ?>



            <br>

            <h3>Description</h3>

            <div id="description"><?php echo html_entity_decode($promotion_description); ?></div>







            <?php if($promotion_data['information_page']['display_products'] == 'yes'): ?>

            <br>

            <h3><?php echo $linked_products_title; ?></h3>

            <ul id="tabs" data-tabs="tabs" class="nav nav-tabs htabs">

              <?php if(!empty($promotion_data['included_products']['product_ids'])): ?>

                <li><a data-toggle="tab" href="#products">Products</a></li>

              <?php endif; ?>

              <?php if(!empty($promotion_data['included_products']['category_ids'])): ?>

                <li><a data-toggle="tab" href="#categories">Categories</a></li>

              <?php endif; ?>

              <?php if(!empty($promotion_data['included_products']['manufacturer_ids'])): ?>

                <li><a data-toggle="tab" href="#manufac">Manufacturers</a></li>

              <?php endif; ?>

            </ul>



            <div class="tabs-content">
	      		<div id="products" class="tab-pane tab-content fade">
				  <?php foreach ($promotion_data['included_products']['product_ids'] as $product) { ?>
				  <div class="product-grid-item <?php echo $this->journal2->settings->get('product_grid_classes'); ?> display-<?php echo $this->journal2->settings->get('product_grid_wishlist_icon_display'); ?> <?php echo $this->journal2->settings->get('product_grid_button_block_button'); ?>">
				      <div class="product-thumb product-wrapper <?php echo isset($product['labels']) && is_array($product['labels']) && isset($product['labels']['outofstock']) ? 'outofstock' : ''; ?>">
				        <div class="image">
				          <a href="<?php echo $product['href']; ?>" <?php if(isset($product['thumb2']) && $product['thumb2']): ?> class="has-second-image" style="background: url('<?php echo $product['thumb2']; ?>') no-repeat;" <?php endif; ?>>
				              <img class="lazy first-image" src="<?php echo $this->journal2->settings->get('product_dummy_image'); ?>" data-src="<?php echo $product['thumb']; ?>" title="<?php echo $product['name']; ?>" alt="<?php echo $product['name']; ?>" />
				          </a>
				          <?php if (isset($product['labels']) && is_array($product['labels'])): ?>
				          <?php foreach ($product['labels'] as $label => $name): ?>
				          <span class="label-<?php echo $label; ?>"><b><?php echo $name; ?></b></span>
				          <?php endforeach; ?>
				          <?php endif; ?>
				          <?php if($this->journal2->settings->get('product_grid_wishlist_icon_position') === 'image' && $this->journal2->settings->get('product_grid_wishlist_icon_display', '') === 'icon'): ?>
				              <div class="wishlist"><a onclick="addToWishList('<?php echo $product['product_id']; ?>');" class="hint--top" data-hint="<?php echo $button_wishlist; ?>"><i class="wishlist-icon"></i><span class="button-wishlist-text"><?php echo $button_wishlist;?></span></a></div>
				              <div class="compare"><a onclick="addToCompare('<?php echo $product['product_id']; ?>');" class="hint--top" data-hint="<?php echo $button_compare; ?>"><i class="compare-icon"></i><span class="button-compare-text"><?php echo $button_compare;?></span></a></div>
				          <?php endif; ?>
				        </div>
				        <div class="product-details">
				          <div class="caption">
				            <h4 class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
				            <p class="description"><?php echo $product['description']; ?></p>
				            <?php if ($product['rating']) { ?>
				            <div class="rating">
				              <?php for ($i = 1; $i <= 5; $i++) { ?>
				              <?php if ($product['rating'] < $i) { ?>
				              <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
				              <?php } else { ?>
				              <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
				              <?php } ?>
				              <?php } ?>
				            </div>
				            <?php } ?>
				          </div>
				          <div class="button-group">
				            <?php if (Journal2Utils::isEnquiryProduct($this, $product)): ?>
				            <div class="cart enquiry-button">
				              <a href="javascript:Journal.openPopup('<?php echo $this->journal2->settings->get('enquiry_popup_code'); ?>', '<?php echo $product['product_id']; ?>');" data-clk="addToCart('<?php echo $product['product_id']; ?>');" class="button hint--top" data-hint="<?php echo $this->journal2->settings->get('enquiry_button_text'); ?>"><?php echo $this->journal2->settings->get('enquiry_button_icon') . '<span class="button-cart-text">' . $this->journal2->settings->get('enquiry_button_text') . '</span>'; ?></a>
				            </div>
				            <?php else: ?>
				            <div class="cart <?php echo isset($product['labels']) && is_array($product['labels']) && isset($product['labels']['outofstock']) ? 'outofstock' : ''; ?>">
				              <a onclick="addToCart('<?php echo $product['product_id']; ?>');" class="button hint--top" data-hint="<?php echo $button_cart; ?>"><i class="button-left-icon"></i><span class="button-cart-text"><?php echo $button_cart; ?></span><i class="button-right-icon"></i></a>
				            </div>
				            <?php endif; ?>
				          </div>
				        </div>
				      </div>
				  </div>
				  <?php } ?>
	      		</div>
	      		
              <div id="categories" class="tab-pane tab-content fade">

                <?php foreach ($promotion_data['included_products']['category_ids'] as $product) { ?>

                     <div class="product-grid-item <?php echo $this->journal2->settings->get('product_grid_classes'); ?> display-<?php echo $this->journal2->settings->get('product_grid_wishlist_icon_display'); ?> <?php echo $this->journal2->settings->get('product_grid_button_block_button'); ?>">

                        <div class="product-thumb product-wrapper <?php echo isset($product['labels']) && is_array($product['labels']) && isset($product['labels']['outofstock']) ? 'outofstock' : ''; ?>">

                          <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>

                          <div class="product-details">

                            <h4 class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>

                            <p class="description"><?php echo $product['description']; ?></p>      

                          </div>

                        </div>

                      </div>

                    <?php } ?>

              </div>

              <div id="manufac" class="tab-pane tab-content fade">

                <?php foreach ($promotion_data['included_products']['manufacturer_ids'] as $product) { ?>

                      <div class="product-grid-item <?php echo $this->journal2->settings->get('product_grid_classes'); ?> display-<?php echo $this->journal2->settings->get('product_grid_wishlist_icon_display'); ?> <?php echo $this->journal2->settings->get('product_grid_button_block_button'); ?>">

                        <div class="product-thumb product-wrapper <?php echo isset($product['labels']) && is_array($product['labels']) && isset($product['labels']['outofstock']) ? 'outofstock' : ''; ?>">

                          <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>

                          <div class="product-details">

                            <h4 class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>

                          </div>

                        </div>

                      </div>

                    <?php } ?>

              </div>

            </div>

            <?php endif; ?>





    

    <?php echo $content_bottom; ?></div>

    <?php echo $column_right; ?></div>

</div>

<?php echo $footer; ?>



<script type="text/javascript">

	$(document).ready(function ($) {

       	$('#tabs li:first-child a').tab('show');



        $( "#image" ).parent().unbind( "click" );

    });

</script>