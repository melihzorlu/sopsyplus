<?php echo $header; ?>
<style>
    #promo_container .product-thumb .caption {
        min-height: initial;
        margin-bottom:20px;
    }
</style>
<div id="container" class="container j-container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row"><?php echo $column_left; ?><?php echo $column_right; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>">
      <h1 class="heading-title"><?php echo $heading_title; ?></h1>
      <?php echo $content_top; ?>
      <div id="promo_container">
        <div class="product-filter">
          <div class="display">
            <a onclick="Journal.gridView()" class="grid-view"><?php echo $this->journal2->settings->get("category_grid_view_icon", $button_grid); ?></a>
            <a onclick="Journal.listView()" class="list-view"><?php echo $this->journal2->settings->get("category_list_view_icon", $button_list); ?></a>
          </div>
          <div class="limit"><b><?php echo $text_limit; ?></b>
            <select onchange="location = this.value;">
              <?php foreach ($limits as $limits) { ?>
              <?php if ($limits['value'] == $limit) { ?>
              <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
              <?php } else { ?>
              <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
              <?php } ?>
              <?php } ?>
            </select>
          </div>
        </div>
        <div class="row main-products product-grid">
          <?php if(!empty($promotions)) { ?>
            <?php foreach ($promotions as $index => $promo) { ?>
                <div class="product-grid-item xs-100 sm-50 md-33 lg-20 xl-20">
                  <div class="product-thumb">
                      <div class="image"><a href="<?php echo $promo['page_link']; ?>"><img src="<?php echo $promo['image']; ?>" alt="<?php echo $promo['name']; ?>" title="<?php echo $promo['name']; ?>" class="img-responsive" /></a></div>
                      <div class="product-details">
                          <div class="caption">
                            <h4 class="name"><a href="<?php echo $promo['page_link']; ?>"><?php echo $promo['name']; ?></a></h4>
                            <?php $content = strip_tags(html_entity_decode($promo['information_page']['description'][$current_language])); ?>
                            <p class="description"><?php echo (strlen($content) > 100) ? substr($content, 0, 100) . '..' : $content; ?></p>
                            <a href="<?php echo $promo['page_link']; ?>" class="btn btn-primary button"><?php echo $view_more_button_text; ?></a>
                          </div>
                      </div>
                  </div>
                </div>
            <?php } ?>
            <div class="row pagination">
              <div class="col-sm-6 text-left links"><?php echo $pagination; ?></div>
              <div class="col-sm-6 text-right results"><?php echo $results; ?></div>
            </div>
          <?php } else { ?>
              <p><?php echo $text_no_promotions; ?></p>
              <div class="buttons">
                <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
              </div>
          <?php } ?>
        </div>
      </div>
    <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>