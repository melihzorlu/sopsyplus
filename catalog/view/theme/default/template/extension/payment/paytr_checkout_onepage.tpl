<?php echo $header; ?>
<div class="container">
    <div class="row"><?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
            <div class="paytr_body" style="padding-top: 20px;">
                <?php if(isset($errors)){ foreach($errors as $err_message => $key) { ?>
                <div class="alert alert-danger alert-dismissible"><i class="fa fa-exclamation-circle"></i>
                    <?php echo $key; ?>
                </div>
                <?php } } else { ?>
                <script src="https://www.paytr.com/js/iframeResizer.min.js"></script>
                <iframe src="https://www.paytr.com/odeme/guvenli/<?php echo $iframe_token; ?>" id="paytriframe"
                        frameborder="0"
                        scrolling="no" style="width: 100%;"></iframe>
                <script type="text/javascript">
                    setInterval(function () {
                        iFrameResize({}, '#paytriframe');
                    }, 1000);
                </script>
                <?php } ?>
            </div>
            <?php echo $content_bottom; ?></div>
        <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>