<?php if(isset($errors)){ foreach($errors as $err_message => $key) { ?>
<div class="alert alert-danger alert-dismissible"><i class="fa fa-exclamation-circle"></i>
    <?php echo $key; ?>
</div>
<?php } } else { ?>
<?php if($page_layout != 'onepage') { ?>
<script src="https://www.paytr.com/js/iframeResizer.min.js"></script>
<iframe src="https://www.paytr.com/odeme/guvenli/<?php echo $iframe_token; ?>" id="paytriframe" frameborder="0"
        scrolling="no" style="width: 100%;"></iframe>
<script type="text/javascript">
    setInterval(function () {
        iFrameResize({}, '#paytriframe');
    }, 1000);
</script>

<style>
    /* journal popup resize // onepage with standard option */

    .popup-checkout-payment .popup-inner-body{
        max-height: inherit!important;
        max-width: 100%!important;
        width: 1024px!important;
    }

</style>

<?php } else { ?>
<style>
    .quick-checkout-wrapper .payment-paytr_checkout {
        display: none;
    }
</style>

<div class="buttons">
    <div class="pull-right">
        <input type="button" value="<?php echo $button_confirm; ?>" id="button-confirm"
               data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary"/>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('#button-confirm').click(function () {
            $.ajax({
                'url': 'index.php?route=extension/payment/paytr_checkout/onepage',
                'dataType': 'json',
                'type': 'post',
                success: function (json) {
                    if (json.status == 'success') {
                        window.location.href = 'index.php?route=extension/payment/paytr_checkout/form';
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        })
    });
</script>
<?php } } ?>