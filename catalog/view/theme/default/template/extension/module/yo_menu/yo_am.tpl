<?php if ($menu_type == 'tree') { ?>
<div id="yo-<?php echo $module; ?>" class="<?php echo $class; ?>">
  <?php if ($heading_title) { ?>
  <div class="yo-heading yo-toggle <?php echo $minimized ? 'yo-close' : 'yo-open'; ?>"><i class="fa fa-bars"></i><span><?php echo $heading_title; ?></span></div>
  <?php } ?>
  <div class="yo-wrapper" <?php echo $minimized ? 'style="display:none"' : ''; ?>>
    <ul class="yo-am">
      <?php foreach ($categories as $category) { ?>
      <li <?php echo $category['active'] ? 'class="active"' : ''; ?>>
        <a href="<?php echo $category['href']; ?>" <?php echo $category['active'] ? 'class="active"' : ''; ?>>
          <?php if ($am_icon && $category['icon']) { ?>
          <img class="am-icon" src="image/<?php echo $category['icon']; ?>" alt="<?php echo $category['name']; ?>">
          <?php } ?>
          <span class="am-arrow"><?php echo $category['name']; ?></span>
          <?php if ($count) { ?>
          <span class="am-badge"><?php echo $category['count']; ?></span>
          <?php } ?>
        </a>
        <?php if ($category['children1']) { ?>
        <?php if ($toggle) { ?>
        <a class="am-tb"><span></span></a>
        <?php } ?>
        <ul>
          <?php if ($item_image && $category['thumb']) { ?>
          <li class="am-image"><a href="<?php echo $category['href']; ?>" title="<?php echo $category['name']; ?>"><img src="<?php echo $category['thumb']; ?>" alt="<?php echo $category['name']; ?>"></a></li>
          <?php } ?>
          <?php foreach ($category['children1'] as $child1) { ?>
          <li <?php echo $child1['active'] ? 'class="active"' : ''; ?>>
            <a href="<?php echo $child1['href']; ?>" <?php echo $child1['active'] ? 'class="active"' : ''; ?>>
              <span class="am-arrow"><?php echo $child1['name']; ?></span>
              <?php if ($count) { ?>
              <span class="am-badge"><?php echo $child1['count']; ?></span>
              <?php } ?>
            </a>
            <?php if ($child1['children2']) { ?>
            <?php if ($toggle) { ?>
            <a class="am-tb"><span></span></a>
            <?php } ?>
            <ul>
              <?php foreach ($child1['children2'] as $child2) { ?>
              <li <?php echo $child2['active'] ? 'class="active"' : ''; ?>>
                <a href="<?php echo $child2['href']; ?>" <?php echo $child2['active'] ? 'class="active"' : ''; ?>>
                  <span class="am-arrow"><?php echo $child2['name']; ?></span>
                  <?php if ($count) { ?>
                  <span class="am-badge"><?php echo $child2['count']; ?></span>
                  <?php } ?>
                </a>
                <?php if ($child2['children3']) { ?>
                <?php if ($toggle) { ?>
                <a class="am-tb"><span></span></a>
                <?php } ?>
                <ul>
                  <?php foreach ($child2['children3'] as $child3) { ?>
                  <li <?php echo $child3['active'] ? 'class="active"' : ''; ?>>
                    <a href="<?php echo $child3['href']; ?>" <?php echo $child3['active'] ? 'class="active"' : ''; ?>>
                      <span class="am-arrow"><?php echo $child3['name']; ?></span>
                      <?php if ($count) { ?>
                      <span class="am-badge"><?php echo $child3['count']; ?></span>
                      <?php } ?>
                    </a>
                    <?php if ($child3['children4']) { ?>
                    <?php if ($toggle) { ?>
                    <a class="am-tb"><span></span></a>
                    <?php } ?>
                    <ul>
                      <?php foreach ($child3['children4'] as $child4) { ?>
                      <li <?php echo $child4['active'] ? 'class="active"' : ''; ?>>
                        <a href="<?php echo $child4['href']; ?>" <?php echo $child4['active'] ? 'class="active"' : ''; ?>>
                          <span class="am-arrow"><?php echo $child4['name']; ?></span>
                          <?php if ($count) { ?>
                          <span class="am-badge"><?php echo $child4['count']; ?></span>
                          <?php } ?>
                        </a>
                      </li>
                      <?php } ?>
                    </ul>
                    <?php } ?>
                  </li>
                  <?php } ?>
                </ul>
                <?php } ?>
              </li>
              <?php } ?>
            </ul>
            <?php } ?>
          </li>
          <?php } ?>
        </ul>
        <?php } ?>
      </li>
      <?php } ?>
    </ul>
  </div>
</div>
<?php } ?>

<?php if ($menu_type == 'current' || $menu_type == 'parent') { ?>
<?php if ($categories) { ?>
<div id="yo-<?php echo $module; ?>" class="<?php echo $class; ?>">
  <?php if ($heading_title) { ?>
  <div class="yo-heading yo-toggle <?php echo $minimized ? 'yo-close' : 'yo-open'; ?>"><i class="fa fa-bars"></i><span><?php echo $heading_title; ?></span></div>
  <?php } ?>
  <div class="yo-wrapper" <?php echo $minimized ? 'style="display:none"' : ''; ?>>
    <ul class="yo-am">
      <?php foreach ($categories as $category) { ?>
      <li <?php echo $category['active'] ? 'class="active"' : ''; ?>>
        <a href="<?php echo $category['href']; ?>" <?php echo $category['active'] ? 'class="active"' : ''; ?>>
          <?php if ($am_icon && $category['icon']) { ?>
          <img class="am-icon" src="image/<?php echo $category['icon']; ?>" alt="<?php echo $category['name']; ?>">
          <?php } ?>
          <span class="am-arrow"><?php echo $category['name']; ?></span>
          <?php if ($current_count) { ?>
          <span class="am-badge"><?php echo $category['count']; ?></span>
          <?php } ?>
        </a>
        <?php if ($category['children2']) { ?>
        <?php if ($toggle) { ?>
        <a class="am-tb"><span></span></a>
        <?php } ?>
        <ul>
          <?php if ($item_image && $category['thumb']) { ?>
          <li class="am-image"><a href="<?php echo $category['href']; ?>" title="<?php echo $category['name']; ?>"><img src="<?php echo $category['thumb']; ?>" alt="<?php echo $category['name']; ?>"></a></li>
          <?php } ?>
          <?php foreach ($category['children2'] as $child2) { ?>
          <li <?php echo $child2['active'] ? 'class="active"' : ''; ?>>
            <a href="<?php echo $child2['href']; ?>" <?php echo $child2['active'] ? 'class="active"' : ''; ?>>
              <?php if ($products_by_category && $child2['price']) { ?>
              <span class="am-price">
                <?php if (!$child2['special']) { ?>
                <?php echo $child2['price']; ?>
                <?php } else { ?>
                <?php echo $child2['special']; ?>
                <?php } ?>
              </span>
              <?php } ?>
              <span class="am-arrow"><?php echo $child2['name']; ?></span>
              <?php if (!$products_by_category && $current_count) { ?>
              <span class="am-badge"><?php echo $child2['count']; ?></span>
              <?php } ?>
            </a>
            <?php if ($child2['children3']) { ?>
            <?php if ($toggle) { ?>
            <a class="am-tb"><span></span></a>
            <?php } ?>
            <ul>
              <?php foreach ($child2['children3'] as $child3) { ?>
              <li <?php echo $child3['active'] ? 'class="active"' : ''; ?>>
                <a href="<?php echo $child3['href']; ?>" <?php echo $child3['active'] ? 'class="active"' : ''; ?>>
                  <span class="am-arrow"><?php echo $child3['name']; ?></span>
                  <?php if ($current_count) { ?>
                  <span class="am-badge"><?php echo $child3['count']; ?></span>
                  <?php } ?>
                </a>
                <?php if ($child3['children4']) { ?>
                <?php if ($toggle) { ?>
                <a class="am-tb"><span></span></a>
                <?php } ?>
                <ul>
                  <?php foreach ($child3['children4'] as $child4) { ?>
                  <li <?php echo $child4['active'] ? 'class="active"' : ''; ?>>
                    <a href="<?php echo $child4['href']; ?>" <?php echo $child4['active'] ? 'class="active"' : ''; ?>>
                      <span class="am-arrow"><?php echo $child4['name']; ?></span>
                      <?php if ($current_count) { ?>
                      <span class="am-badge"><?php echo $child4['count']; ?></span>
                      <?php } ?>
                    </a>
                  </li>
                  <?php } ?>
                </ul>
                <?php } ?>
              </li>
              <?php } ?>
            </ul>
            <?php } ?>
          </li>
          <?php } ?>
        </ul>
        <?php } ?>
      </li>
      <?php } ?>
    </ul>
  </div>
</div>
<?php } ?>
<?php } ?>

<?php if ($menu_type == 'brands') { ?>
<div id="yo-<?php echo $module; ?>" class="<?php echo $class; ?>">
  <?php if ($heading_title) { ?>
  <div class="yo-heading yo-toggle <?php echo $minimized ? 'yo-close' : 'yo-open'; ?>"><i class="fa fa-bars"></i><span><?php echo $heading_title; ?></span></div>
  <?php } ?>
  <div class="yo-wrapper" <?php echo $minimized ? 'style="display:none"' : ''; ?>>
    <ul class="yo-am">
      <?php foreach ($manufacturers as $manufacturer) { ?>
      <li <?php echo $manufacturer['active'] || $manufacturer['manufacturer_id'] == $bids ? 'class="active"' : ''; ?>> <a <?php echo $manufacturer['active'] || $manufacturer['manufacturer_id'] == $bids ? 'class="active"' : ''; ?> href="<?php echo $manufacturer['href']; ?>"> <span class="am-arrow"><?php echo $manufacturer['name']; ?></span> </a>
        <?php if ($manufacturer['children']) { ?>
        <?php if ($toggle) { ?>
        <a class="am-tb"><span></span></a>
        <?php } ?>
        <ul>
          <?php if ($item_image && $manufacturer['thumb']) { ?>
          <li class="am-image"><a href="<?php echo $manufacturer['href']; ?>" title="<?php echo $manufacturer['name']; ?>"><img src="<?php echo $manufacturer['thumb']; ?>" alt="<?php echo $manufacturer['name']; ?>"></a></li>
          <?php } ?>
          <?php foreach ($manufacturer['children'] as $child) { ?>
          <li <?php echo $child['active'] ? 'class="active"' : ''; ?>> <a href="<?php echo $child['href']; ?>" <?php echo $child['active'] ? 'class="active"' : ''; ?>>
            <?php if ($child['price']) { ?>
            <span class="am-price">
              <?php if (!$child['special']) { ?>
              <?php echo $child['price']; ?>
              <?php } else { ?>
              <?php echo $child['special']; ?>
              <?php } ?>
            </span>
            <?php } ?>
            <span class="am-arrow"><?php echo $child['name']; ?> </span> </a> </li>
          <?php } ?>
        </ul>
        <?php } ?>
      </li>
      <?php } ?>
    </ul>
  </div>
</div>
<?php } ?>

<script type="text/javascript"><!--
  <?php if ($toggle) { ?>
    $('#yo-<?php echo $module; ?> ul.yo-am ul').each(function() {
      if ($(this).children('li').not('.am-image').length > 0) {
        $(this).prev().prev().addClass('am-item');
        $(this).parent('.active').addClass('am-item-open');
      } else {
        $(this).prev().remove();
      }
    });
    $('#yo-<?php echo $module; ?> ul.yo-am').find('.am-badge').parent().addClass('am-item');
  <?php } else { ?>
    $('#yo-<?php echo $module; ?> ul.yo-am ul').each(function() {
      if ($(this).children('li').not('.am-image').length > 0) {
        $(this).prev().addClass('am-parent');
        $(this).parent('.active').addClass('am-item-open');
      }
    });
  <?php } ?>

  $('#yo-<?php echo $module; ?> a.am-parent, #yo-<?php echo $module; ?> a.am-tb').click(function() {
    var parent = $(this).parent();
    parent.toggleClass('am-item-open').find('>ul').slideToggle(<?php echo $easing_d; ?>,'<?php echo $easing; ?>');
    parent.siblings('.am-item-open').removeClass('am-item-open').find('>ul').slideUp(<?php echo $easing_d; ?>,'<?php echo $easing; ?>');
    return false;
  });

  if (<?php echo $save_view; ?>) {
    if (!localStorage.getItem('yo-am-<?php echo $module; ?>')) {
      if (<?php echo $minimized; ?>) {
        localStorage.setItem('yo-am-<?php echo $module; ?>', 'close');
      } else {
        localStorage.setItem('yo-am-<?php echo $module; ?>', 'open');
      }
    }

    $('#yo-<?php echo $module; ?> .yo-toggle').click(function() {
      $(this).toggleClass('yo-open yo-close').next().slideToggle(<?php echo $easing_d; ?>,'<?php echo $easing; ?>');
      if ($(this).hasClass('yo-open')) {
        localStorage.setItem('yo-am-<?php echo $module; ?>', 'open');
      } else {
        localStorage.setItem('yo-am-<?php echo $module; ?>', 'close');
      }
    });

    if (localStorage.getItem('yo-am-<?php echo $module; ?>') == 'open') { 
      $('#yo-<?php echo $module; ?> .yo-toggle').addClass('yo-open').removeClass('yo-close').next().show();
    } else {
      $('#yo-<?php echo $module; ?> .yo-toggle').addClass('yo-close').removeClass('yo-open').next().hide();
    }
  } else {
    localStorage.removeItem('yo-am-<?php echo $module; ?>');
    $('#yo-<?php echo $module; ?> .yo-toggle').click(function() {
      $(this).toggleClass('yo-open yo-close').next().slideToggle(<?php echo $easing_d; ?>,'<?php echo $easing; ?>');
    });
  }
//--></script>