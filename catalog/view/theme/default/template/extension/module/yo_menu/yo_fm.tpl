<?php if ($menu_type == 'tree') { ?>
<div id="yo-<?php echo $module; ?>" class="<?php echo $class; ?>">
  <?php if ($heading_title) { ?>
  <div class="yo-heading yo-toggle <?php echo $minimized ? 'yo-close' : 'yo-open'; ?>"><i class="fa fa-bars"></i><span><?php echo $heading_title; ?></span></div>
  <?php } ?>
  <div class="yo-wrapper" <?php echo $minimized ? 'style="display:none"' : ''; ?>>
    <ul class="yo-fm <?php echo $flyout_design; ?>">
      <?php foreach ($categories as $category) { ?>
      <li <?php echo $category['active'] ? 'class="active"' : ''; ?>>
        <a href="<?php echo $category['href']; ?>" class="<?php echo $category['children1'] ? 'fm-parent' : ''; ?><?php echo $category['active'] ? ' active' : ''; ?>">
          <?php if ($fm_icon && $category['icon']) { ?>
          <img class="fm-icon" src="image/<?php echo $category['icon']; ?>" alt="<?php echo $category['name']; ?>">
          <?php } ?>
          <span><?php echo $category['name']; ?></span>
          <?php if ($count) { ?>
          <span class="fm-badge"><?php echo $category['count']; ?></span>
          <?php } ?>
        </a>
        <?php if ($category['children1']) { ?>
        <?php if ($flyout_design == 'fm-one') { ?>
        <ul>
          <?php if ($item_image && $category['thumb']) { ?>
          <li class="fm-image"><a href="<?php echo $category['href']; ?>" title="<?php echo $category['name']; ?>"><img src="<?php echo $category['thumb']; ?>" alt="<?php echo $category['name']; ?>"></a></li>
          <?php } ?>
          <?php foreach ($category['children1'] as $child1) { ?>
          <li <?php echo $child1['active'] ? 'class="active"' : ''; ?>>
            <a href="<?php echo $child1['href']; ?>" class="<?php echo $child1['children2'] ? 'fm-parent' : ''; ?><?php echo $child1['active'] ? ' active' : ''; ?>">
              <?php if ($fm_icon && $child1['icon']) { ?>
              <img class="fm-icon" src="image/<?php echo $child1['icon']; ?>" alt="<?php echo $child1['name']; ?>">
              <?php } ?>
              <span><?php echo $child1['name']; ?></span>
              <?php if ($count) { ?>
              <span class="fm-badge"><?php echo $child1['count']; ?></span>
              <?php } ?>
            </a>
            <?php if ($child1['children2']) { ?>
            <ul>
              <?php foreach ($child1['children2'] as $child2) { ?>
              <li <?php echo $child2['active'] ? 'class="active"' : ''; ?>>
                <a href="<?php echo $child2['href']; ?>" class="<?php echo $child2['children3'] ? 'fm-parent' : ''; ?><?php echo $child2['active'] ? ' active' : ''; ?>">
                  <?php if ($fm_icon && $child2['icon']) { ?>
                  <img class="fm-icon" src="image/<?php echo $child2['icon']; ?>" alt="<?php echo $child2['name']; ?>">
                  <?php } ?>
                  <span><?php echo $child2['name']; ?></span>
                  <?php if ($count) { ?>
                  <span class="fm-badge"><?php echo $child2['count']; ?></span>
                  <?php } ?>
                </a>
                <?php if ($child2['children3']) { ?>
                <ul>
                  <?php foreach ($child2['children3'] as $child3) { ?>
                  <li <?php echo $child3['active'] ? 'class="active"' : ''; ?>>
                    <a href="<?php echo $child3['href']; ?>" class="<?php echo $child3['children4'] ? 'fm-parent' : ''; ?><?php echo $child3['active'] ? ' active' : ''; ?>">
                      <?php if ($fm_icon && $child3['icon']) { ?>
                      <img class="fm-icon" src="image/<?php echo $child3['icon']; ?>" alt="<?php echo $child3['name']; ?>">
                      <?php } ?>
                      <span><?php echo $child3['name']; ?></span>
                      <?php if ($count) { ?>
                      <span class="fm-badge"><?php echo $child3['count']; ?></span>
                      <?php } ?>
                    </a>
                    <?php if ($child3['children4']) { ?>
                    <ul>
                      <?php foreach ($child3['children4'] as $child4) { ?>
                      <li <?php echo $child4['active'] ? 'class="active"' : ''; ?>>
                        <a href="<?php echo $child4['href']; ?>" <?php echo $child4['active'] ? 'class="active"' : ''; ?>>
                          <?php if ($fm_icon && $child4['icon']) { ?>
                          <img class="fm-icon" src="image/<?php echo $child4['icon']; ?>" alt="<?php echo $child4['name']; ?>">
                          <?php } ?>
                          <span><?php echo $child4['name']; ?></span>
                          <?php if ($count) { ?>
                          <span class="fm-badge"><?php echo $child4['count']; ?></span>
                          <?php } ?>
                        </a>
                      </li>
                      <?php } ?>
                    </ul>
                    <?php } ?>
                  </li>
                  <?php } ?>
                </ul>
                <?php } ?>
              </li>
              <?php } ?>
            </ul>
            <?php } ?>
          </li>
          <?php } ?>
        </ul>
        <?php } else { ?>
        <div class="fm-container">
          <div class="fm-brand-info">
            <div class="fm-title"><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a> </div>
            <?php if ($item_banner && $category['banner']) { ?>
            <div class="fm-image"><img src="<?php echo $category['banner']; ?>" alt="<?php echo $category['name']; ?>"></div>
            <?php } ?>
            <?php if ($item_description && $category['description']) { ?>
            <div class="fm-descr"><?php echo $category['description']; ?></div>
            <?php } ?>
          </div>
          <?php foreach (array_chunk($category['children1'], ceil(count($category['children1']) / $column)) as $children1) { ?>
          <ul>
            <?php foreach ($children1 as $child1) { ?>
            <li class="fm-image-<?php echo $image_position; ?><?php echo $child1['active'] ? ' active' : ''; ?>">
              <?php if ($item_image && $child1['thumb']) { ?>
              <div class="fm-image"><a href="<?php echo $child1['href']; ?>"><img src="<?php echo $child1['thumb']; ?>" alt="<?php echo $child1['name']; ?>"></a></div>
              <?php } ?>
              <a href="<?php echo $child1['href']; ?>" <?php echo $child1['active'] ? 'class="active"' : ''; ?>> <span><?php echo $child1['name']; ?></span> </a>
              <?php if ($image_position == 'left') { ?>
              <?php if ($child1['children2']) { ?>
              <ul class="fm-list">
                <?php foreach ($child1['children2'] as $child2) { ?>
                <li <?php echo $child2['active'] ? 'class="active"' : ''; ?>> <a href="<?php echo $child2['href']; ?>" <?php echo $child2['active'] ? 'class="active"' : ''; ?>> <span><?php echo $child2['name']; ?></span> </a> </li>
                <?php } ?>
              </ul>
              <?php } ?>
              <?php } ?>
            </li>
            <?php } ?>
          </ul>
          <?php } ?>
        </div>
        <?php } ?>
        <?php } ?>
      </li>
      <?php } ?>
    </ul>
  </div>
</div>
<?php } ?>

<?php if ($menu_type == 'current' || $menu_type == 'parent') { ?>
<?php if ($categories) { ?>
<div id="yo-<?php echo $module; ?>" class="<?php echo $class; ?>">
  <?php if ($heading_title) { ?>
  <div class="yo-heading yo-toggle <?php echo $minimized ? 'yo-close' : 'yo-open'; ?>"><i class="fa fa-bars"></i><span><?php echo $heading_title; ?></span></div>
  <?php } ?>
  <div class="yo-wrapper" <?php echo $minimized ? 'style="display:none"' : ''; ?>>
    <ul class="yo-fm <?php echo $flyout_design; ?>">
      <?php foreach ($categories as $category) { ?>
      <li <?php echo $category['active'] ? 'class="active"' : ''; ?>>
        <a href="<?php echo $category['href']; ?>" class="<?php echo $category['children2'] ? 'fm-parent' : ''; ?><?php echo $category['active'] ? ' active' : ''; ?>">
          <?php if ($fm_icon && $category['icon']) { ?>
          <img class="fm-icon" src="image/<?php echo $category['icon']; ?>" alt="<?php echo $category['name']; ?>">
          <?php } ?>
          <span><?php echo $category['name']; ?></span>
          <?php if ($current_count) { ?>
          <span class="fm-badge"><?php echo $category['count']; ?></span>
          <?php } ?>
        </a>
        <?php if ($category['children2']) { ?>
        <?php if ($flyout_design == 'fm-one') { ?>
        <ul>
          <?php if ($item_image && $category['thumb']) { ?>
          <li class="fm-image"><a href="<?php echo $category['href']; ?>" title="<?php echo $category['name']; ?>"><img src="<?php echo $category['thumb']; ?>" alt="<?php echo $category['name']; ?>"></a></li>
          <?php } ?>
          <?php foreach ($category['children2'] as $child2) { ?>
          <li <?php echo $child2['active'] ? 'class="active"' : ''; ?>>
            <a href="<?php echo $child2['href']; ?>" class="<?php echo $child2['children3'] ? 'fm-parent' : ''; ?><?php echo $child2['active'] ? ' active' : ''; ?>">
              <?php if (!$products_by_category && $fm_icon && $child2['icon']) { ?>
              <img class="fm-icon" src="image/<?php echo $child2['icon']; ?>" alt="<?php echo $child2['name']; ?>">
              <?php } ?>
              <?php if ($products_by_category && $child2['price']) { ?>
              <span class="fm-price">
                <?php if (!$child2['special']) { ?>
                <?php echo $child2['price']; ?>
                <?php } else { ?>
                <?php echo $child2['special']; ?>
                <?php } ?>
              </span>
              <?php } ?>
              <span><?php echo $child2['name']; ?></span>
              <?php if (!$products_by_category && $current_count) { ?>
              <span class="fm-badge"><?php echo $child2['count']; ?></span>
              <?php } ?>
            </a>
            <?php if ($child2['children3']) { ?>
            <ul>
              <?php foreach ($child2['children3'] as $child3) { ?>
              <li <?php echo $child3['active'] ? 'class="active"' : ''; ?>>
                <a href="<?php echo $child3['href']; ?>" class="<?php echo $child3['children4'] ? 'fm-parent' : ''; ?><?php echo $child3['active'] ? ' active' : ''; ?>">
                  <?php if ($fm_icon && $child3['icon']) { ?>
                  <img class="fm-icon" src="image/<?php echo $child3['icon']; ?>" alt="<?php echo $child3['name']; ?>">
                  <?php } ?>
                  <span><?php echo $child3['name']; ?></span>
                  <?php if ($current_count) { ?>
                  <span class="fm-badge"><?php echo $child3['count']; ?></span>
                  <?php } ?>
                </a>
                <?php if ($child3['children4']) { ?>
                <ul>
                  <?php foreach ($child3['children4'] as $child4) { ?>
                  <li <?php echo $child4['active'] ? 'class="active"' : ''; ?>>
                    <a href="<?php echo $child4['href']; ?>" <?php echo $child4['active'] ? 'class="active"' : ''; ?>>
                      <?php if ($fm_icon && $child4['icon']) { ?>
                      <img class="fm-icon" src="image/<?php echo $child4['icon']; ?>" alt="<?php echo $child4['name']; ?>">
                      <?php } ?>
                      <span><?php echo $child4['name']; ?></span>
                      <?php if ($current_count) { ?>
                      <span class="fm-badge"><?php echo $child4['count']; ?></span>
                      <?php } ?>
                    </a>
                  </li>
                  <?php } ?>
                </ul>
                <?php } ?>
              </li>
              <?php } ?>
            </ul>
            <?php } ?>
          </li>
          <?php } ?>
        </ul>
        <?php } else { ?>
        <div class="fm-container">
          <div class="fm-brand-info">
            <div class="fm-title"><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a> </div>
            <?php if ($item_banner && $category['banner']) { ?>
            <div class="fm-image"><img src="<?php echo $category['banner']; ?>" alt="<?php echo $category['name']; ?>"></div>
            <?php } ?>
            <?php if ($item_description && $category['description']) { ?>
            <div class="fm-descr"><?php echo $category['description']; ?></div>
            <?php } ?>
          </div>
          <?php foreach (array_chunk($category['children2'], ceil(count($category['children2']) / $column)) as $children2) { ?>
          <ul>
            <?php foreach ($children2 as $child2) { ?>
            <li class="fm-image-<?php echo $image_position; ?>">
              <?php if ($image_position == 'center') { ?>
              <?php if ($item_image && $child2['thumb']) { ?>
              <div class="fm-image"> <a href="<?php echo $child2['href']; ?>"><img src="<?php echo $child2['thumb']; ?>" alt="<?php echo $child2['name']; ?>" /></a>
                <?php if ($products_by_category) { ?>
                <div class="fm-cart-<?php echo $image_position; ?>">
                  <input type="button" value="<?php echo $button_cart; ?>" onclick="addToCart('<?php echo $child2['product_id']; ?>');" class="button" />
                </div>
                <?php } ?>
              </div>
              <?php } ?>
              <a href="<?php echo $child2['href']; ?>" <?php echo $products_by_category ? 'class="fm-pr-name"' : ''; ?>>
              <?php if ($products_by_category && $child2['price']) { ?>
              <span class="fm-price">
                <?php if (!$child2['special']) { ?>
                <?php echo $child2['price']; ?>
                <?php } else { ?>
                <?php echo $child2['special']; ?>
                <?php } ?>
              </span>
              <?php } ?>
              <span><?php echo $child2['name']; ?></span></a>
              <?php } else { ?>
              <?php if ($item_image && $child2['thumb']) { ?>
              <div class="fm-image"> <a href="<?php echo $child2['href']; ?>"><img src="<?php echo $child2['thumb']; ?>" alt="<?php echo $child2['name']; ?>" /></a></div>
              <?php } ?>
              <a href="<?php echo $child2['href']; ?>"> <span><?php echo $child2['name']; ?></span></a>
              <?php if ($child2['children3']) { ?>
              <ul class="fm-list">
                <?php foreach ($child2['children3'] as $child3) { ?>
                <li> <a href="<?php echo $child3['href']; ?>"> <span><?php echo $child3['name']; ?></span> </a> </li>
                <?php } ?>
              </ul>
              <?php } ?>
              <?php if ($products_by_category) { ?>
              <?php if ($child2['price']) { ?>
              <span class="fm-price fm-price-<?php echo $image_position; ?>">
                <?php if (!$child2['special']) { ?>
                <?php echo $child2['price']; ?>
                <?php } else { ?>
                <?php echo $child2['special']; ?>
                <?php } ?>
              </span>
              <?php } ?>
              <div class="fm-cart">
                <input type="button" value="<?php echo $button_cart; ?>" onclick="addToCart('<?php echo $child2['product_id']; ?>');" class="button" />
              </div>
              <?php } ?>
              <?php } ?>
            </li>
            <?php } ?>
          </ul>
          <?php } ?>
        </div>
        <?php } ?>
        <?php } ?>
      </li>
      <?php } ?>
    </ul>
  </div>
</div>
<?php } ?>
<?php } ?>

<?php if ($menu_type == 'brands') { ?>
<div id="yo-<?php echo $module; ?>" class="<?php echo $class; ?>">
  <?php if ($heading_title) { ?>
  <div class="yo-heading yo-toggle <?php echo $minimized ? 'yo-close' : 'yo-open'; ?>"><i class="fa fa-bars"></i><span><?php echo $heading_title; ?></span></div>
  <?php } ?>
  <div class="yo-wrapper" <?php echo $minimized ? 'style="display:none"' : ''; ?>>
    <ul class="yo-fm <?php echo $flyout_design; ?>">
      <?php foreach ($manufacturers as $manufacturer) { ?>
      <li <?php echo $manufacturer['active'] || $manufacturer['manufacturer_id'] == $bids ? 'class="active"' : ''; ?>> <a href="<?php echo $manufacturer['href']; ?>" class="<?php echo $manufacturer['children'] ? 'fm-parent' : ''; ?> <?php echo ($manufacturer['active'] || $manufacturer['manufacturer_id'] == $bids ? 'active' : ''); ?>"> <span><?php echo $manufacturer['name']; ?></span> </a>
        <?php if ($manufacturer['children']) { ?>
        <?php if ($flyout_design == 'fm-one') { ?>
        <ul>
          <?php if ($item_image && $manufacturer['thumb']) { ?>
          <li class="fm-image"><a href="<?php echo $manufacturer['href']; ?>" title="<?php echo $manufacturer['name']; ?>"><img src="<?php echo $manufacturer['thumb']; ?>" alt="<?php echo $manufacturer['name']; ?>"></a></li>
          <?php } ?>
          <?php foreach ($manufacturer['children'] as $child) { ?>
          <li <?php echo $child['active'] ? 'class="active"' : ''; ?>> <a class="<?php echo $child['active'] ? 'active' : ''; ?>" href="<?php echo $child['href']; ?>">
            <?php if ($child['price']) { ?>
            <span class="fm-price">
              <?php if (!$child['special']) { ?>
              <?php echo $child['price']; ?>
              <?php } else { ?>
              <?php echo $child['special']; ?>
              <?php } ?>
            </span>
            <?php } ?>
            <span><?php echo $child['name']; ?></span> </a> </li>
          <?php } ?>
        </ul>
        <?php } else { ?>
        <div class="fm-container">
          <div class="fm-brand-info">
            <div class="fm-title"><a href="<?php echo $manufacturer['href']; ?>"><?php echo $manufacturer['name']; ?></a> </div>
            <?php if ($item_banner && $manufacturer['banner']) { ?>
            <div class="fm-image"><img src="<?php echo $manufacturer['banner']; ?>" alt="<?php echo $manufacturer['name']; ?>"></div>
            <?php } ?>
            <?php if ($item_description && $manufacturer['description']) { ?>
            <div class="fm-descr"><?php echo $manufacturer['description']; ?></div>
            <?php } ?>
          </div>
          <?php foreach (array_chunk($manufacturer['children'], ceil(count($manufacturer['children']) / $column)) as $children) { ?>
          <ul>
            <?php foreach ($children as $child) { ?>
            <li class="fm-image-<?php echo $image_position; ?><?php echo $child['active'] ? ' active' : ''; ?>">
              <?php if ($image_position == 'center') { ?>
              <?php if ($item_image && $child['thumb']) { ?>
              <div class="fm-image"> <a href="<?php echo $child['href']; ?>"><img src="<?php echo $child['thumb']; ?>" alt="<?php echo $child['name']; ?>" /></a>
                <div class="fm-cart-<?php echo $image_position; ?>">
                  <input type="button" value="<?php echo $button_cart; ?>" onclick="addToCart('<?php echo $child['product_id']; ?>');" class="button" />
                </div>
              </div>
              <?php } ?>
              <a href="<?php echo $child['href']; ?>" class="fm-pr-name<?php echo $child['active'] ? ' active' : ''; ?>">
              <?php if ($child['price']) { ?>
              <span class="fm-price">
                <?php if (!$child['special']) { ?>
                <?php echo $child['price']; ?>
                <?php } else { ?>
                <?php echo $child['special']; ?>
                <?php } ?>
              </span>
              <?php } ?>
              <span><?php echo $child['name']; ?></span></a>
              <?php } else { ?>
              <?php if ($item_image && $child['thumb']) { ?>
              <div class="fm-image"> <a href="<?php echo $child['href']; ?>"><img src="<?php echo $child['thumb']; ?>" alt="<?php echo $child['name']; ?>" /></a></div>
              <?php } ?>
              <a href="<?php echo $child['href']; ?>" <?php echo $child['active'] ? 'class="active"' : ''; ?>> <span><?php echo $child['name']; ?></span></a>
              <?php if ($child['price']) { ?>
              <span class="fm-price fm-price-<?php echo $image_position; ?>">
                <?php if (!$child['special']) { ?>
                <?php echo $child['price']; ?>
                <?php } else { ?>
                <?php echo $child['special']; ?>
                <?php } ?>
              </span>
              <?php } ?>
              <div class="fm-cart">
                <input type="button" value="<?php echo $button_cart; ?>" onclick="addToCart('<?php echo $child['product_id']; ?>');" class="button" />
              </div>
              <?php } ?>
            </li>
            <?php } ?>
          </ul>
          <?php } ?>
        </div>
        <?php } ?>
        <?php } ?>
      </li>
      <?php } ?>
    </ul>
  </div>
</div>
<?php } ?>

<script type="text/javascript"><!--
  var offset1 = $('#yo-<?php echo $module; ?>').offset();
  var offset2 = $('#yo-<?php echo $module; ?> .yo-fm').offset();
  $('#yo-<?php echo $module; ?> .fm-container').css('top', offset1.top-offset2.top);
  $(window).on('resize', function() {
    var container = $('#yo-<?php echo $module; ?>').closest('.container').width();
    var menu = $('#yo-<?php echo $module; ?>').outerWidth();
    $('#yo-<?php echo $module; ?> .fm-container').outerWidth(container-menu);
    if ($(this).width() > '768') {
      $('#yo-<?php echo $module; ?> .fm-container > ul').width(100/<?php echo $column; ?> + '%');
    } else {
      $('#yo-<?php echo $module; ?> .fm-container > ul').width('50%');
    }
  }).resize();

  $('#yo-<?php echo $module; ?> ul.yo-fm,#yo-<?php echo $module; ?> ul.fm-one ul').menuAim({
    submenuDirection: $('#yo-<?php echo $module; ?>').parent().is('#column-right') ? 'left' : 'right',
    activate: function(item){
      $(item).find('>a.fm-parent').next().show();
      $(item).siblings().find('>a.fm-parent').next().hide();
    },
    deactivate: function(item){
      $(item).find('>a.fm-parent').next().fadeOut(100);
    },
    exitMenu: function(item){
      $(item).find('.fm-container:visible').delay(500).fadeOut(100);
      return true;
    }
  });

  $('#yo-<?php echo $module; ?> .fm-parent').has('span.fm-badge').closest('ul').children('li').not('.fm-image').find('>a').addClass('fm-item');

  if (<?php echo $save_view; ?>) {
    if (!localStorage.getItem('yo-fm-<?php echo $module; ?>')) {
      if (<?php echo $minimized; ?>) {
        localStorage.setItem('yo-fm-<?php echo $module; ?>', 'close');
      } else {
        localStorage.setItem('yo-fm-<?php echo $module; ?>', 'open');
      }
    }

    $('#yo-<?php echo $module; ?> .yo-toggle').click(function() {
      $(this).toggleClass('yo-open yo-close').next().slideToggle(<?php echo $easing_d; ?>,'<?php echo $easing; ?>');
      if ($(this).hasClass('yo-open')) {
        localStorage.setItem('yo-fm-<?php echo $module; ?>', 'open');
      } else {
        localStorage.setItem('yo-fm-<?php echo $module; ?>', 'close');
      }
    });

    if (localStorage.getItem('yo-fm-<?php echo $module; ?>') == 'open') { 
      $('#yo-<?php echo $module; ?> .yo-toggle').addClass('yo-open').removeClass('yo-close').next().show();
    } else {
      $('#yo-<?php echo $module; ?> .yo-toggle').addClass('yo-close').removeClass('yo-open').next().hide();
    }
  } else {
    localStorage.removeItem('yo-fm-<?php echo $module; ?>');
    $('#yo-<?php echo $module; ?> .yo-toggle').click(function() {
      $(this).toggleClass('yo-open yo-close').next().slideToggle(<?php echo $easing_d; ?>,'<?php echo $easing; ?>');
      var offset1 = $('#yo-<?php echo $module; ?>').offset();
      var offset2 = $('#yo-<?php echo $module; ?> .yo-fm').offset();
      $('#yo-<?php echo $module; ?> .fm-container').css('top', offset1.top-offset2.top);
    });
  }
//--></script>