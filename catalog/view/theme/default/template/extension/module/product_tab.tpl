  <div style="margin-bottom:15px;" class="text-center" >
	 
	<ul class="nav nav-tabs" role="tablist">
	 <?php if($latest_products){ ?>
  <li class="active">
  <a href="#tab-latest" data-toggle="tab" class="selected"> <?php echo $tab_latest; ?></a></li>
  <?php } ?>
	
	<?php if($featured_products){ ?>
<li>
	<a href="#tab-featured" data-toggle="tab" > <?php echo $tab_featured; ?></a></li>
	<?php } ?>
	
	
	
	<?php if($bestseller_products){ ?>
	<li>
	<a href="#tab-bestseller" data-toggle="tab"> <?php echo $tab_bestseller; ?></a></li>
	<?php } ?>
	<?php if($special_products){ ?>
	<li>
	<a href="#tab-special" data-toggle="tab">
<?php echo $tab_special; ?></a></li>
	<?php } ?>
	 
	 
	</ul>
  

 </div>
 
 <div  class="animation-element bounce-up tab-content margins">
 
<?php if($latest_products){ ?>
 <div id="tab-latest" class="tab-pane active">
 <div>
    <div id="tab-latestq"  style="background:none;margin-bottom:10px;" class="slideshow owl-carousel">
      <?php foreach ($latest_products as $product) { ?>
      <div class="leftrightx col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="product-layout">
    <div style="margin-bottom:0px;margin-top:0px;" class="product-thumb transition">
	<?php  if ($product['quantity']<=0) { ?>  
	<div class="miktar"></div>
	 <?php } ?>
	 
      <div class="image"><?php if ($product['thumb_swap']) { ?>	
											<a href="<?php echo $product['href']; ?>">
											<img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" />						
											<img class="img-responsive hover-image" src="<?php echo $product['thumb_swap']; ?>" alt="<?php echo $product['name']; ?>"/>
											</a>
										<?php } else {?>
										<a href="<?php echo $product['href']; ?>">
											<img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" />						
										</a>
										  
<?php } ?>
	   
		<?php  if ($product['special']) { ?>  
                  <div  class="discount">İndirim
                  <?php 
                  $string = $product['price'];
                  $string1 = $product['special'];
$int_price = substr(intval(preg_replace('/[^0-9]+/', '', $string), 10),0,-2);
$int_sprice = substr(intval(preg_replace('/[^0-9]+/', '', $string1), 10),0,-2);
					echo "%". round(($int_price-$int_sprice)/$int_price*100)  ;
                  ?> 
                  </div>
                  <?php } ?>
     <div class="button-group">
		<button type="button" data-toggle="tooltip" title="<?php echo $button_cart; ?>" onclick="cart.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $button_cart; ?></span></button>
      
	    
			 <button data-toggle="tooltip" title="İncele" type="button" onclick="window.open('<?php echo $product['href']; ?>', '_self') "><i class="fa fa-search-plus"></i> İNCELE</button>
	  
	  </div>
       
        </div>
        
		<div class="caption text-center">
		<div class="rating  text-center center-block">
		
		
		 
        </div>
		<h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
		<div class="rating">
				  <?php for ($i = 1; $i <= 5; $i++) { ?>
				  <?php if ($product['rating'] < $i) { ?>
				   <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
				  <?php } else { ?>
				  <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
				  <?php } ?>
				  <?php } ?>
				</div>
        <?php if ($product['price']) { ?>
        <p  class="price">
          <?php if (!$product['special']) { ?>
          <span class="price-new"><?php echo $product['price']; ?></span>
          <?php } else { ?>
         <span class="price-old"><?php echo $product['price']; ?></span> <span class="price-new"><?php echo $product['special']; ?></span> 
          <?php } ?>
          <?php if ($product['tax']) { ?>
          <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
          <?php } ?>
        </p>
        <?php } ?>
		 
      </div>
	   
	  
    </div>
  </div>
        </div>
        <?php } ?>
    </div></div> 
 </div>
 <script type="text/javascript"><!--
$('#tab-latestq').owlCarousel({
	items: 4,
	autoPlay: false,
	navigation: true,
	navigationText: ['<i class="fa fa-chevron-left fa-5x sol"></i>', '<i class="fa fa-chevron-right fa-5x sag"></i>'],
	pagination: false
});
--></script>
<?php } ?>

<?php if($featured_products){ ?>
  <div id="tab-featured" class="tab-pane">
  <div class=" ">
    <div id="tab-featuredq" style="background:none;" class="slideshow owl-carousel">
      <?php foreach ($featured_products as $product) { ?>
      <div class="leftrightx col-lg-12 col-md-12 col-sm-12 col-xs-12">
  <div class="product-layout">
    <div class="product-thumb transition">
	<?php  if ($product['quantity']<=0) { ?>  
	<div class="miktar"></div>
	 <?php } ?>
	 
      <div class="image"><?php if ($product['thumb_swap']) { ?>	
											<a href="<?php echo $product['href']; ?>">
											<img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" />						
											<img class="img-responsive hover-image" src="<?php echo $product['thumb_swap']; ?>" alt="<?php echo $product['name']; ?>"/>
											</a>
										<?php } else {?>
										<a href="<?php echo $product['href']; ?>">
											<img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" />						
										</a>
										  
<?php } ?>
	  <?php  if ($product['special']) { ?>  
                  <div  class="discount">İndirim
                  <?php 
                  $string = $product['price'];
                  $string1 = $product['special'];
$int_price = substr(intval(preg_replace('/[^0-9]+/', '', $string), 10),0,-2);
$int_sprice = substr(intval(preg_replace('/[^0-9]+/', '', $string1), 10),0,-2);
					echo round(($int_price-$int_sprice)/$int_price*100) . "%";
                  ?> 
                  </div>
                  <?php } ?>
     <div class="button-group">
		<button type="button" data-toggle="tooltip" title="<?php echo $button_cart; ?>" onclick="cart.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $button_cart; ?></span></button>
      
	    
			 <button data-toggle="tooltip" title="İncele" type="button" onclick="window.open('<?php echo $product['href']; ?>', '_self') "><i class="fa fa-search-plus"></i> İNCELE</button>
	  
	  </div>
       
        <div class="rating text-center center-block">
		
		
		
		 
        </div></div>
        
		<div class="caption text-center">
		<h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
		<div class="rating">
				  <?php for ($i = 1; $i <= 5; $i++) { ?>
				  <?php if ($product['rating'] < $i) { ?>
				   <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
				  <?php } else { ?>
				  <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
				  <?php } ?>
				  <?php } ?>
				</div>
        <?php if ($product['price']) { ?>
        <p class="price">
          <?php if (!$product['special']) { ?>
          <span class="price-new"><?php echo $product['price']; ?></span>
          <?php } else { ?>
         <span class="price-old"><?php echo $product['price']; ?></span> <span class="price-new"><?php echo $product['special']; ?></span> 
          <?php } ?>
          <?php if ($product['tax']) { ?>
          <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
          <?php } ?>
        </p>
        <?php } ?>
		  
      </div>
	   
	  
    </div>
  </div>
      </div>
      <?php } ?>
    </div></div>
	<div style="clear:both"></div>
	</div>
 
 <script type="text/javascript"><!--
$('#tab-featuredq').owlCarousel({
	items: 4,
	autoPlay: 3000,
	navigation: true,
	navigationText: ['<i class="fa fa-chevron-left fa-5x"></i>', '<i class="fa fa-chevron-right fa-5x"></i>'],
	pagination: false
});
--></script>
<?php } ?>

<?php if($bestseller_products){ ?>
 <div id="tab-bestseller" class="tab-pane">
 <div class="row">
    <div id="tab-bestsellerq" style="background:none;" class="slideshow owl-carousel">
      <?php foreach ($bestseller_products as $product) { ?>
      <div class="leftrightx col-lg-12 col-md-12 col-sm-12col-xs-12">
          <div class="product-layout">
    <div class="product-thumb transition">
	<?php  if ($product['quantity']<=0) { ?>  
	<div class="miktar"></div>
	 <?php } ?>
	 
      <div class="image"><?php if ($product['thumb_swap']) { ?>	
											<a href="<?php echo $product['href']; ?>">
											<img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" />						
											<img class="img-responsive hover-image" src="<?php echo $product['thumb_swap']; ?>" alt="<?php echo $product['name']; ?>"/>
											</a>
										<?php } else {?>
										<a href="<?php echo $product['href']; ?>">
											<img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" />						
										</a>
										  
<?php } ?>
	<?php  if ($product['special']) { ?>  
                  <div  class="discount">İndirim
                  <?php 
                  $string = $product['price'];
                  $string1 = $product['special'];
$int_price = substr(intval(preg_replace('/[^0-9]+/', '', $string), 10),0,-2);
$int_sprice = substr(intval(preg_replace('/[^0-9]+/', '', $string1), 10),0,-2);
					echo "%" . round(($int_price-$int_sprice)/$int_price*100) ;
                  ?> 
                  </div>
                  <?php } ?>
     <div class="button-group">
		<button type="button" data-toggle="tooltip" title="<?php echo $button_cart; ?>" onclick="cart.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $button_cart; ?></span></button>
      
	    
			 <button data-toggle="tooltip" title="İncele" type="button" onclick="window.open('<?php echo $product['href']; ?>', '_self') "><i class="fa fa-search-plus"></i> İNCELE</button>
	  
	  </div>
       
        <div class="rating text-center center-block">
		
	
		
		 
        </div></div>
        
		<div class="caption text-center">
		<h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
		<div class="rating">
				  <?php for ($i = 1; $i <= 5; $i++) { ?>
				  <?php if ($product['rating'] < $i) { ?>
				   <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
				  <?php } else { ?>
				  <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
				  <?php } ?>
				  <?php } ?>
				</div>
        <?php if ($product['price']) { ?>
        <p class="price">
          <?php if (!$product['special']) { ?>
          <span class="price-new"><?php echo $product['price']; ?></span>
          <?php } else { ?>
         <span class="price-old"><?php echo $product['price']; ?></span> <span class="price-new"><?php echo $product['special']; ?></span> 
          <?php } ?>
          <?php if ($product['tax']) { ?>
          <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
          <?php } ?>
        </p>
        <?php } ?>
		  
      </div>
	   
	  
    </div>
  </div>
        </div>
        <?php } ?>
    </div>
 </div> 
 <div style="clear:both"></div>
 </div>
 <script type="text/javascript"><!--
$('#tab-bestsellerq').owlCarousel({
	items:4,
	autoPlay: 3000,
	navigation: true,
	navigationText: ['<i class="fa fa-chevron-left fa-5x"></i>', '<i class="fa fa-chevron-right fa-5x"></i>'],
	pagination: false
});
--></script>
<?php } ?>

<?php if($special_products){ ?>
 <div  id="tab-special" class="tab-pane">
 <div class="row">
    <div id="tab-specialq" style="background:none" class="slideshow owl-carousel">
      <?php foreach ($special_products as $product) { ?>
      <div class="leftrightx col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="product-layout">
    <div class="product-thumb transition">
	<?php  if ($product['quantity']<=0) { ?>  
	<div class="miktar"></div>
	 <?php } ?>
	 
      <div class="image"><?php if ($product['thumb_swap']) { ?>	
											<a href="<?php echo $product['href']; ?>">
											<img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" />						
											<img class="img-responsive hover-image" src="<?php echo $product['thumb_swap']; ?>" alt="<?php echo $product['name']; ?>"/>
											</a>
										<?php } else {?>
										<a href="<?php echo $product['href']; ?>">
											<img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" />						
										</a>
										  
<?php } ?>
     <div class="button-group">
		<button type="button" data-toggle="tooltip" title="<?php echo $button_cart; ?>" onclick="cart.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $button_cart; ?></span></button>
      
	    
			 <button data-toggle="tooltip" title="İncele" type="button" onclick="window.open('<?php echo $product['href']; ?>', '_self') "><i class="fa fa-search-plus"></i> İNCELE</button>
	  
	  </div>
       
        <div class="rating text-center center-block">
		
		<?php  if ($product['special']) { ?>  İndirim
                  <div  class="discount">
                  <?php 
                  $string = $product['price'];
                  $string1 = $product['special'];
$int_price = substr(intval(preg_replace('/[^0-9]+/', '', $string), 10),0,-2);
$int_sprice = substr(intval(preg_replace('/[^0-9]+/', '', $string1), 10),0,-2);
					echo round(($int_price-$int_sprice)/$int_price*100) . "%";
                  ?> 
                  </div>
                  <?php } ?>
		 
        </div></div>
        
		<div class="caption text-center">
		<h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
		<div class="rating">
				  <?php for ($i = 1; $i <= 5; $i++) { ?>
				  <?php if ($product['rating'] < $i) { ?>
				   <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
				  <?php } else { ?>
				  <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
				  <?php } ?>
				  <?php } ?>
				</div>
        <?php if ($product['price']) { ?>
        <p class="price">
          <?php if (!$product['special']) { ?>
          <span class="price-new"><?php echo $product['price']; ?></span>
          <?php } else { ?>
         <span class="price-old"><?php echo $product['price']; ?></span> <span class="price-new"><?php echo $product['special']; ?></span> 
          <?php } ?>
          <?php if ($product['tax']) { ?>
          <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
          <?php } ?>
        </p>
        <?php } ?>
		  
      </div>
	   
	  
    </div>
  </div>
        </div>
        <?php } ?>
		
    </div>
	</div>
	 
 </div>
 <script type="text/javascript"><!--
$('#tab-specialq').owlCarousel({
	items: 4,
	autoPlay: false,
	navigation: true,
	navigationText: ['<i class="fa fa-chevron-left fa-5x"></i>', '<i class="fa fa-chevron-right fa-5x"></i>'],
	pagination: false
});
--></script>
<?php } ?>

  
 </div>