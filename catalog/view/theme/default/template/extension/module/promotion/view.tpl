<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
    <div class="row">
        <div class="col-sm-12">
            
            <h1><?php echo $promotion_title; ?></h1>
          
            <?php if(!empty($promotion_image)) { ?>
            <div id="image"><img style="border: 1px solid #efefef;" src="<?php echo $promotion_image; ?>"></div>
            <?php } ?>

            <br>
            <h3><?php echo $description_title; ?></h3>
            <div id="description"><?php echo html_entity_decode($promotion_description); ?></div>


            <?php if($promotion_data['information_page']['display_products'] == 'yes'): ?>

              <?php if(!empty($promotion_data['included_products']['product_ids'])): ?>
                <div class="row">
                <h3><?php echo $linked_products_title; ?></h3>
                  <div id="products">
                    <?php foreach ($promotion_data['included_products']['product_ids'] as $product) { ?>
                      <div class="product-layout col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="product-thumb transition">
                          <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
                          <div style="padding: 0 20px;">
                            <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
                            <p><?php echo $product['description']; ?></p>
                            <?php if ($product['rating']) { ?>
                            <div class="rating">
                              <?php for ($i = 1; $i <= 5; $i++) { ?>
                              <?php if ($product['rating'] < $i) { ?>
                              <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                              <?php } else { ?>
                              <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                              <?php } ?>
                              <?php } ?>
                            </div>
                            <?php } ?>       
                          </div>
                          <div class="button-group">
                          <button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');"><span class="hidden-xs hidden-sm hidden-md"><?php echo $button_cart; ?></span> <i class="fa fa-shopping-cart"></i></button>
                          <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-heart"></i></button>
                          <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-exchange"></i></button>
                        </div>
                        </div>
                      </div>
                    <?php } ?>
                </div>
                </div>
              <?php endif; ?>
              <?php if(!empty($promotion_data['included_products']['category_ids'])): ?> 
                <div class="row">
                <h3><?php echo $linked_categories_title; ?></h3> 
                <div id="categories">
                  <?php foreach ($promotion_data['included_products']['category_ids'] as $product) { ?>
                        <div class="product-layout col-lg-3 col-md-3 col-sm-6 col-xs-12">
                          <div class="product-thumb transition">
                            <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
                            <div style="padding: 0 20px;">
                              <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
                              <p><?php echo $product['description']; ?></p>      
                            </div>
                          </div>
                        </div>
                      <?php } ?>
                </div>
                </div>
              <?php endif; ?>
              <?php if(!empty($promotion_data['included_products']['manufacturer_ids'])): ?>
                <div class="row">
                <h3><?php echo $linked_manufacturers_title; ?></h3>
                <div id="manufac">
                  <?php foreach ($promotion_data['included_products']['manufacturer_ids'] as $product) { ?>
                        <div class="product-layout col-lg-3 col-md-3 col-sm-6 col-xs-12">
                          <div class="product-thumb transition">
                            <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
                            <div style="padding: 0 20px;">
                              <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
                            </div>
                          </div>
                        </div>
                      <?php } ?>
                </div>
                </div>
              <?php endif; ?>
            <?php endif; ?>
        </div>
    </div>
    
    <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>

<script type="text/javascript">
    $('#included_products li:first-child a').tab('show');
</script>