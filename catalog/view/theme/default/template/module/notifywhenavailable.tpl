<?php if($nwa_data['notifywhenavailable']['Enabled'] == 'yes'): ?>
	<div id="NotifyWhenAvailable_popup" style="display:none;width:<?php echo $nwa_data['notifywhenavailable']['PopupWidth']; ?>px;" class="NWA_popover bottom">
		<div class="arrow"></div>
		<h3 class="NWA_popover-title"><?php echo $nwa_data['notifywhenavailable']['CustomTitle']; ?></h3>
		<div class="NWA_popover-content">
		</div>
	</div> 
	<?php
		$product_id = 0;

		if(isset($_GET["product_id"]))
			$product_id = $_GET["product_id"];

	?>
	
	<script>		
		var origAddToCart;
		var nwaAddToCartSingle;
		var checkQuantity = function () {
			var query = $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea').serialize();
			debugger;
			$.ajax({
				url: 'index.php?route=<?php echo $modulePath ?>/checkQuantityNWA',
				type: 'post',
				data: query,
				dataType: 'json',
				success: function(json) {
					$('#button-cart').html('<?php echo $button_cart; ?>');
					$('#button-cart').unbind('click');
					$('#button-cart').on('click', origAddToCart);

					setTimeout(function() {
						for (i in json) {
                        	if(!json[i].PO && json[i].product_id) {
                        		$('#button-cart').html('<?php echo empty($nwa_data['notifywhenavailable']['ButtonLabel']) ? $NotifyWhenAvailable_Button : $nwa_data['notifywhenavailable']['ButtonLabel']; ?>');
                        		$('#button-cart').unbind('click');
                        		$('#button-cart').on('click', nwaAddToCartSingle);
                                
                                if($('#button-cart').parents('.form-group').hasClass('outofstock')) {
                                    $('#button-cart').parents('.form-group').removeClass('outofstock')
                                }
                        	}
                        }
					}, 100);
				}
			});
		}

	    $(document).ready(function() { 
	    	if($('#button-cart').length > 0) {
	    		$.each($._data($('#button-cart')[0],'events'), function(i, event) {
			 		if (i == 'click') {
			 			$.each(event, function(j, h) {
			 				if(h.handler.toString().indexOf('index.php?route=checkout/cart/add') > -1) {
			 					origAddToCart = h.handler;
			 				}
					    });
			 		}
				});
	    	}
	    	
			var product_id  = '<?php echo $product_id ?>';

			if(product_id == 0) {
				product_id = $('input[name=product_id]').val();
			}
				
			if(product_id) {
				checkQuantity();
			}			

			$('input[name*=option], select[name*=option]').on('change', function() {
				checkQuantity();
			});
		    
		    nwaAddToCartSingle =  function() {
		    	$('.alert, .text-danger').remove();
				$('.form-group').removeClass('has-error');

	    		$('body').append($('#NotifyWhenAvailable_popup'));
							
				offset = $('#button-cart').offset();

				$('div#NotifyWhenAvailable_popup').fadeIn('slow');
       			$(".NWA_popover-content").load("index.php?route=<?php echo $modulePath ?>/shownotifywhenavailableform&product_id="+product_id, function() {
       				var window_width = $(window).width();
					var window_height = $(window).height();
					
					if (window_width < 600) {
						if($('.modal-backdrop').length < 1) {
							$('<div class="modal-backdrop fade in"></div>').appendTo(document.body);
						} else {
							$('.modal-backdrop').css('display','block');
						}

						$('body').css('overflow-y', 'hidden');

						$('.NWA_popover.bottom .arrow').css('display','none');
						
						$('div#NotifyWhenAvailable_popup').css({
						    top: '50%',
						    left: '50%',
							position: 'fixed',
							'margin-top': -$('div#NotifyWhenAvailable_popup').height()/2,
							'margin-left': -$('div#NotifyWhenAvailable_popup').width()/2,
						});
					} else {
						$('div#NotifyWhenAvailable_popup').css({
							top: offset.top,
							left: ((offset.left-$('div#NotifyWhenAvailable_popup').width()/2) + $('#button-cart').width()/2)
						});
					}
       			});
		    	
			};
			
			$(document).click(function(event) {
		        if (!$(event.target).is("#NotifyWhenAvailable_popup, button[lkmwa=true], input[lkmwa=true], .NWA_popover-title, .arrow, .NWA_popover-content, #NWAYourName, #NWAYourEmail, #NotifyWhenAvailableSubmit, #NotifyWhenAvailable_popup p, #NotifyWhenAvailable_popup span, .NWA_popover, #NotifyWhenAvailableForm, .NWAError, input.NWA_popover_field_error, #button-cart")) {
		            $('div#NotifyWhenAvailable_popup').fadeOut(300);
		            $('.modal-backdrop').fadeOut(300);
		            $('body').css('overflow-y', 'inherit');
		        }
		    });	

		});
    </script>
<?php endif; ?>