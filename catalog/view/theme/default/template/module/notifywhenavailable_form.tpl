<?php if($nwa_data['notifywhenavailable']['Enabled'] != 'no'): ?>   
			<form id="NotifyWhenAvailableForm">
               	<input type="hidden" name="NWAProductID" value="<?php echo $NotifyWhenAvailableProductID; ?>" />     
          <?php $string = html_entity_decode($nwa_data['notifywhenavailable']['CustomText']);
                $patterns = array();
                $patterns[0] = '/{name_field}/';
                $patterns[1] = '/{email_field}/';
                $patterns[2] = '/{submit_button}/';
                $replacements = array();
                $replacements[0] = '<input type="text" class="form-control" name="NWAYourName" id="NWAYourName" placeholder="'.$NWA_YourName.'" value="'.$customer_name.'" />';
                $replacements[1] = '<input type="text" class="form-control" name="NWAYourEmail" id="NWAYourEmail" placeholder="'.$NWA_YourEmail.'" value="'.$customer_email.'" />';
                $replacements[2] = '<a id="NotifyWhenAvailableSubmit" class="btn btn-default">'.$NotifyWhenAvailable_SubmitButton.'</a>';
                echo preg_replace($patterns, $replacements, $string);
                ?>
               	<div class="NWA_loader"><div class="NWA_loading"></div></div>
                <div id="NotifyWhenAvailableSuccess"></div>  
            </form>
<script>

$('#NotifyWhenAvailableSubmit').on('click', function(){ 
	$('input#NWAYourName').removeClass("NWA_popover_field_error");
	$('input#NWAYourEmail').removeClass("NWA_popover_field_error");
	$('div.NWAError').remove();

	var email_validate = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
	if ((document.getElementById("NWAYourName").value == 0) )
	{
		  $('input#NWAYourName').addClass("NWA_popover_field_error");
		  $('input#NWAYourName').after('<div class="NWAError"><?php echo $NotifyWhenAvailable_Error1; ?></div>');
	} else if ((document.getElementById("NWAYourEmail").value.length == 0)) {
		  $('input#NWAYourEmail').addClass("NWA_popover_field_error");
		  $('input#NWAYourEmail').after('<div class="NWAError"><?php echo $NotifyWhenAvailable_Error1; ?></div>');
	} else if (!document.getElementById("NWAYourEmail").value.match(email_validate)) {
		  $('input#NWAYourEmail').addClass("NWA_popover_field_error");
		  $('input#NWAYourEmail').after('<div class="NWAError"><?php echo $NotifyWhenAvailable_Error2; ?></div>');
	} else {
		$.ajax({
			url: 'index.php?route=<?php echo $modulePath ?>/submitform',
			type: 'post',
			data: $('#NotifyWhenAvailableForm').serialize(),
			beforeSend: function(){
				  $('#NotifyWhenAvailableSubmit').hide();
				  $('.NWA_loader').show();
    		},
			success: function(response) {
				$('.NWA_loader').hide();
				$('#NotifyWhenAvailableSubmit').hide();
				$('#NotifyWhenAvailableSuccess').html("<div class='alert alert-success nwa-success' style='display: none;'><i class='fa fa-check-circle'></i>&nbsp;<?php echo $NotifyWhenAvailable_Success; ?></div>");
				$('.nwa-success').fadeIn('slow');
				$('#NWAYourName').val('');
				$('#NWAYourEmail').val('');
			}
		});
	}
});
</script>
    <?php if(!empty($nwa_data['notifywhenavailable']['CustomCSS'])): ?>
    <style>
    <?php echo htmlspecialchars_decode($nwa_data['notifywhenavailable']['CustomCSS']); ?>
    </style>
    <?php endif; ?>
<?php endif; exit; ?>
