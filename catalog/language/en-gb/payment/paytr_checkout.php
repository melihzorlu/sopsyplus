<?php

// Text
$_['text_title']                = 'Credit / Bank Card';
$_['text_credit_card']          = 'Card Information';
$_['text_start_date']           = '(possible)';
$_['text_wait']                 = 'Please Wait.';
$_['text_title_in_checkout']    = 'Credit Card';
$_['text_total']                = 'Installment Amount';
$_['orderId_text']              = 'Order id';

// Error
$_['Error_message_curl']        = 'PHP CURL Extension must be activated in your server.';
