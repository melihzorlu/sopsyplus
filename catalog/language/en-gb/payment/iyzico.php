<?php
// Text
$_['iyzico_title'] = 'Debit/Credit Card';
$_['iyzico_img_title'] 	= '<img style="width: 25%" src="admin/view/image/payment/iyzico_cards.png" alt="iyzico"/>';
$_['payment_failed'] = 'Payment Failed';
$_['payment_field_desc'] = 'Payment ID';
$_['installement_field_desc'] = 'Installment Commission';
$_['installement_count'] = ' Installment';
$_['payment_received_message'] = 'Payment received by iyzico.';


// Text
$_['text_wait'] = 'Please wait!';

// Entry
$_['button_confirm'] = "Confirm";

$_['homepage_button_text'] = "Homepage";
$_['error_message_sender_head'] = 'An Error Occured';
$_['invalid_token'] = 'Invalid Token';

