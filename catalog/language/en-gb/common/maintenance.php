<?php
// Heading
$_['heading_title']    = 'Bakım Modu';

// Text
$_['text_maintenance'] = 'Bakım Modu';
$_['text_message']     = '<h1 style="text-align:center;">We are currently performing some scheduled maintenance. <br/>We will be back as soon as possible. Please check back soon.</h1>';