<?php
// Heading 
$_['nwa_heading_title']  = 'Gelince Haber Ver';
$_['NotifyWhenAvailable_Title'] = $_['nwa_heading_title'];
$_['NotifyWhenAvailable_SubmitButton'] = 'Gelince Haber Ver!';
$_['NotifyWhenAvailable_Error1'] = 'Zorunlu Alan!';
$_['NotifyWhenAvailable_Error2'] = 'Geçersiz E-posta!';
$_['NotifyWhenAvailable_Success'] = 'Başarılı. Ürün stoklara girdiğinde e-posta adresinize bilgi maili alacaksınız.';
$_['NotifyWhenAvailable_Button'] = 'Gelince Haber Ver!';

$_['NWA_YourName'] = 'İsim';
$_['NWA_YourEmail'] = 'E-posta';
