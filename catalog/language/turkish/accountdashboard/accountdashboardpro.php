<?php
// Heading
$_['heading_title'] = 'Hesabım';

// Metin
$_['text_account'] = 'Hesap';
$_['text_my_account'] = 'Hesabım';
$_['text_my_orders'] = 'Siparişlerim';
$_['text_my_newsletter'] = 'Bülten';
$_['text_edit'] = 'Hesabı Düzenle';
$_['text_password'] = 'Parolayı Değiştir';
$_['text_address'] = 'Adres Defteri';
$_['text_credit_card'] = 'Kayıtlı Kredi Kartlarını Yönet';
$_['text_wishlist'] = 'İstek Listesi';
$_['text_order'] = 'Siparişleriniz';
$_['text_download'] = 'İndirilenler';
$_['text_logout'] = 'Çıkış';

$_['text_reward'] = 'Ödül Puanlarınız';
$_['text_return'] = 'İade Taleplerini Görüntüle';
$_['text_transaction'] = 'Bakiyeniz';
$_['text_newsletter'] = 'Bülten Abone Ol';
$_['text_recurring'] = 'Düzenli Ödemeler';
$_['text_transactions'] = 'Toplam Bakiye';
$_['text_view_order'] = 'Tüm Siparişleri Görüntüle';
$_['text_view_transactions'] = 'Tüm Bakiyeyi Görüntüle';
$_['text_view_wishlists'] = 'Tüm İstek Listelerini Görüntüle';
$_['text_view_downloads'] = 'Tüm İndirilenleri Görüntüle';
$_['text_view_reward'] = 'Tüm Ödül Puanlarını Görüntüle';

// Paneller
$_['panel_orders'] = 'Toplam Sipariş';
$_['panel_wishlist'] = 'Toplam İstek Listesi';
$_['panel_downloads'] = 'İndirilenler';
$_['panel_reward_points'] = 'Ödül Puanları';
$_['text_address_book'] = 'Adres Defteri';
$_['text_latest_order'] = 'Son Siparişler';
$_['text_no_results'] = 'Sonuç Yok';

// Sütunlar
$_['column_order_id'] = 'Sipariş Kimliği';
$_['column_product'] = 'Ürünler';
$_['column_status'] = 'Durum';
$_['column_total'] = 'Toplam';
$_['column_date_added'] = 'Tarih';
$_['column_action'] = 'İşlem';

// Düğmeler
$_['button_view_all'] = 'Tümünü Görüntüle';