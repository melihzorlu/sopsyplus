<?php
// common
$_['text_hello'] = 'Sayın müşterimiz,';
$_['text_hello_customer'] = 'Merhaba {firstname},';
$_['text_account_btn'] = 'Hesabım';
$_['text_affiliate_btn'] = 'Ortaklık Hesabım';
$_['text_goodbye'] = 'Saygılarımızla,<br/>{store_name}';

// customer.register
$_['subject_customer.register'] = '{store_name} - Kayıt olduğunuz için teşekkürler';
$_['text_customer_welcome'] = '{store_name} kayıt olduğunuz için teşekkür ederiz!';
$_['text_account_created'] = 'Hesabınız oluşturuldu. E-Posta adresinizi ve şifrenizi kullanarak giriş yapabilirsiniz. Bunun için web sitemizi ziyaret ediniz veya aşağıdaki URL ile giriş yapabilirsiniz:';
$_['text_account_approval'] = 'Giriş yapmadan önce hesabınızın onaylanması gerekmektedir. Onaylandıktan sonra aşağıdaki Linki kullanarak giriş yapabilirsiniz:';
$_['text_account_services'] = 'Giriş yaptıktan sonra, Hesap Bilgilerinizi düzenleyebilir, Geçmiş Siparişlerinizi inceleyebilir veya yazdırabilirsiniz.';
$_['text_login'] = 'Eposta adresiniz: <b>{email}</b>';
$_['text_password'] = 'Şifreniz: <b>{password}</b>';
// customer.forgotten
$_['subject_customer.forgotten'] = '{store_name} - Yeni Şifre';
// 2.2
$_['text_password_reset'] = '{store_name} Müşteri için yeni hesap şifresi istendi.';
$_['text_reset_link'] = 'Şifrenizi sıfırlamak için aşağıdaki Linke tıklayınız:';
$_['text_reset_btn'] = 'Şifre Sıfırla';
$_['text_ip'] = 'Bu isteği yapmak için kullanılan {ip}';
// customer.forgotten
$_['subject_customer.forgotten'] = '{store_name} - Yeni Şifreniz';
$_['text_password_lost'] = 'Yeni şifre talebinde bulundunuz.';
$_['text_new_password'] = 'Şifreniz: <b>{password}</b>';

// customer.approve
$_['subject_customer.approve'] = '{store_name} - Hesabınız aktifleştirildi!';
$_['text_approve_welcome'] = '{store_name} kayıt olduğunuz için teşekkür ederiz!';
$_['text_account_approved'] = 'Hesabınız oluşturuldu. E-Posta adresinizi ve şifrenizi kullanarak giriş yapabilirsiniz. Bunun için web sitemizi ziyaret ediniz veya aşağıdaki URL ile giriş yapabilirsiniz:';

// customer.transaction
$_['subject_customer.credit'] = '{store_name} - Hesap Krediniz';
$_['text_credit_received'] = '{amount} kredi aldınız!';
$_['text_credit_total'] = 'Mevcut toplam krediniz: {total}.';
$_['text_credit_info'] = 'Hesap kredisi sonraki alışverişlerinizde hesabınızdan otomatik olarak düşülür.';

// customer.reward
$_['subject_customer.reward'] = '{store_name} - Ödül Puanı';
$_['text_reward_received'] = '{amount} ödül puanı kazandınız!';
$_['text_reward_total'] = 'Mevcut toplam ödül puanınız {total}.';

// customer.voucher
$_['subject_customer.voucher'] = '{from} tarafından hediye çeki gönderildi';
$_['text_voucher_greeting'] = 'Tebrikler, <b>{amount}</b> değerinde bir hediye çeki aldınız.';
$_['text_voucher_from'] = 'Bu hediye çeki <b>{from}</b> tarafından gönderildi.';
$_['text_voucher_message'] = 'Birde mesajınız var :<br/>{message}';
$_['text_voucher_code'] = '<b>Hediye çeki kodu:</b> <b>{code}</b>';
$_['text_voucher_redeem'] = 'Hediye çeki ile istediğiniz ürünü satın alabilirsiniz. Hediye çekini kullanamak için istediğiniz ürünü sepete ekleyiniz ve kasaya gitmenden önce alışveriş sepeti sayfasında hediye çeki kodunuzu kullanın.';

// affiliate.forgotten
$_['subject_affiliate.forgotten'] = '{store_name} - Yeni Şifre';

// affiliate.register
$_['subject_affiliate.register'] = '{store_name} - Ortaklık Sistemi';
$_['text_affiliate_welcome'] = '{store_name} Ortaklık Sistemine kayıt olduğunuz için teşekkür ederiz.';
$_['text_affiliate_services'] = 'Giriş yaptıktan sonra, takip linki oluşturabilir, komisyon ödeme bilgilerinizi ve profilinizi düzenleyebilirsiniz';

// affiliate.approve
$_['subject_affiliate.approve'] = '{store_name} - Ortaklık Hesabınız aktifleştirildi!';
$_['text_affiliate_approve_welcome'] = '{store_name} ortaklık sistemine kayıt olduğunuz için teşekkür ederiz!';
$_['text_affiliate_approved'] = 'Ortaklık sistemi hesabınız oluşturuldu. Sitemizi ziyaret edip e-posta adresinizi ve şifrenizi kullanarak ortaklık hesabınıza giriş yapabilirsiniz:';

// affiliate.transaction
$_['subject_affiliate.transaction'] = '{store_name} - Ortaklık Komisyonunuz';
$_['text_transaction_received'] = 'Sizin için belirlenen komisyon oranı {amount} dir.';
$_['text_transaction_total'] = 'Şu anki komisyon ile aldığınız toplam miktar {total}.';

// order.update
$_['subject_order.update'] = '{store_name} - Siparişiniz Güncellendi {order_id}';
$_['text_update_status'] = '<b>#{order_id}</b> nolu siparişiniz şu duruma güncellendi: <b>{order_status}</b>';
$_['text_update_comment'] = 'Siparişiniz ile ilgili açıklamalar:';

// order.return
$_['subject_order.return'] = '{store_name} - İade Güncellendi {return_id}';
$_['text_return_status'] = '<b>#{return_id}</b> nolu iade talebiniz şu duruma güncellendi: <b>{order_status}</b>';

// order.confirm
$_['subject_order.confirm'] = '{store_name} - Sipariş {order_id}';
$_['text_order_confirm'] = ' {store_name} ürünlerine göstermiş olduğunuz ilgiden dolayı teşekkür ederiz. Ödemeniz onaylandıktan sonra siparişiniz işleme alınacaktır.';
$_['text_order_link'] = 'Siparişinizi görüntülemek için aşağıdaki linke tıklayın:';
$_['text_order_btn'] = 'Siparişim';
$_['text_download_btn'] = 'İndirilebilir Ürünlerim';
$_['text_no_order_status'] = 'Durum tanımlı değil';

// sale.contact
$_['subject_sale.contact'] = '';

// admin.information.contact
$_['subject_admin.information.contact'] = 'İletişim - {enquiry_name}';
$_['text_new_enquiry'] = 'Yeni bir iletişim maili aldınız.';
$_['text_name'] = '<b>İsim:</b> {enquiry_name}';
$_['text_mail'] = '<b>Email:</b> {enquiry_mail}';
$_['text_message'] = '<b>Mesaj:</b><br/>{enquiry_message}';

// admin.order.confirm
$_['subject_admin.order.confirm'] = '{store_name} - Sipariş {order_id}';
$_['text_order_received'] = 'Yeni bir sipariş aldınız.';
$_['text_order_status'] = 'Sipariş Durumu: <b>{order_status}</b>';

// admin.customer.register
$_['subject_admin.customer.register'] = 'Yeni Müşteri';
$_['text_customer_signup'] = 'Yeni bir müşteri kaydoldu:';
$_['text_customer_info'] = '<table><tr><td style="width:43%">Web Site:</td><td><b>{store_url}</b></td></tr><tr><td>Müşteri Grubu:&nbsp;</td><td><b>{customer_group}</b></td></tr><tr><td>Adı:</td><td><b>{firstname}</b></td></tr><tr><td>Soyadı:</td><td><b>{lastname}</b></td></tr><tr><td>E-Mail:</td><td><b>{email}</b></td></tr><tr><td>Telefon:</td><td><b>{telephone}</b></td></tr></table>';

// admin.affiliate.register
$_['subject_admin.affiliate.register'] = 'Yeni Ortaklık';
$_['text_affiliate_signup'] = 'Yeni bir ortaklık başvuru yapıldı:';
$_['text_affiliate_info'] = '<table><tr><td style="width:43%">Mağaza:</td><td><b>{store_url}</b></td></tr><tr><td>Adı:</td><td><b>{firstname}</b></td></tr><tr><td>Soyadı:</td><td><b>{lastname}</b></td></tr><tr><td>Şirket</td><td><b>{company}</b></td></tr><tr><td>Website:</td><td><b>{website}</b></td></tr><tr><td>E-Mail:</td><td><b>{email}</b></td></tr><tr><td>Telefon:</td><td><b>{telephone}</b></td></tr></table>';

// Invoice
$_['date_format'] = 'd/m/Y';
$_['direction'] = 'ltr';
$_['decimal_point'] = '.';
$_['thousand_point'] = ',';
$_['text_invoice'] = 'Fatura';
$_['text_proformat'] = 'Proforma fatura';
$_['text_packingslip'] = 'Sevk irsaliyesi';
$_['text_store_vat'] = 'KDV ID:';
$_['text_store_company'] = 'Şirket ID:';
$_['text_url'] = 'URL:';
$_['text_company_id'] = 'Şirket ID:';
$_['text_tax_id'] = 'KDV ID:';
$_['text_order_detail'] = 'Sipariş Detayları';
$_['text_invoice_no'] = 'Fatura No.:';
$_['text_order_id'] = 'Sipariş ID:';
$_['text_status'] = 'Durum:';
$_['text_date_added'] = 'Eklenme Tarihi:';
$_['text_date_due'] = 'Son Tarih:';
$_['text_customer'] = 'Müşteri:';
$_['text_customer_id'] = 'Müşteri ID:';
$_['text_shipping_address'] = 'Kargo Adresi';
$_['text_shipping_method'] = 'Kargo Metodu:';
$_['text_payment_address'] = 'Fatura Adresi';
$_['text_payment_method'] = 'Ödeme Metodu:';
$_['text_products'] = 'Ürünler:';
$_['text_total'] = 'Toplam:';
$_['text_instruction'] = 'Talimatlar';
$_['text_email'] = 'Email:';
$_['text_telephone'] = 'Telefon:';
$_['text_fax'] = 'Faks:';

// Column
$_['column_image'] = '';
$_['column_product'] = 'Ürün Adı';
$_['column_product_id'] = 'Ürün ID';
$_['column_model'] = 'Ürün Kodu';
$_['column_manufacturer'] = 'Üretici';
$_['column_description'] = 'Açıklama';
$_['column_mpn'] = 'MPN';
$_['column_location'] = 'Konum';
$_['column_sku'] = 'SKU';
$_['column_upc'] = 'UPC';
$_['column_expected'] = 'Geri Sipariş (Beklenen)';
$_['column_weight'] = 'Ağırlık';
$_['column_quantity'] = 'Miktar';
$_['column_slip_qty'] = 'Sipariş Edilen Miktar';
$_['column_qty_check'] = 'Sevk Edilen Miktar';
$_['column_price'] = 'Fiyat KDV Hariç';
$_['column_price_tax'] = 'Fiyat';
$_['column_tax'] = 'Vergi';
$_['column_tax_total'] = 'Vergi (toplam)';
$_['column_tax_rate'] = 'Vergi oranı';
$_['column_total'] = 'Toplam KDV hariç';
$_['column_total_tax'] = 'Toplam';
?>
