<?php
// Text
$_['iyzico_title'] = 'Banka/Kredi Kartı';
$_['iyzico_img_title'] 	= '<img style="width: 25%" src="admin/view/image/payment/iyzico_cards.png" alt="iyzico"/>';
$_['payment_failed'] = 'Ödeme Başarısız';
$_['payment_field_desc'] = 'Ödeme Numarası';
$_['installement_field_desc'] = 'Taksit Komisyon Tutarı';
$_['installement_count'] = ' Taksit';
$_['payment_received_message'] = 'Ödeme iyzico tarafından alındı.';


// Text
$_['text_wait'] = 'Lütfen bekleyiniz...';

// Entry
$_['button_confirm'] = "Onayla";

$_['homepage_button_text'] = "Anasayfa";
$_['error_message_sender_head'] = 'Bir hata meydana geldi';
$_['invalid_token'] = 'Geçersiz Token';

