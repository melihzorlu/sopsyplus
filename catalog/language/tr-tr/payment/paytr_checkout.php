<?php

// Text
$_['text_title']                = 'Kredi / Banka Kartı';
$_['text_credit_card']          = 'Kart Bilgileri';
$_['text_start_date']           = '(mümkünse)';
$_['text_wait']                 = 'Lütfen Bekleyin.';
$_['text_title_in_checkout']    = 'Kredi Kartı';
$_['text_total']                = 'Vade Farkı';
$_['orderId_text']              = 'Sipariş id\'si';

// Error
$_['Error_message_curl']        = 'Sunucunuzda PHP CURL Eklentisinin aktif olması gerekmektedir.';