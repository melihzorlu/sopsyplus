<?php
/* Turkceye Ceviren eka7a - http://www.e-piksel.com */

// Text
$_['text_information']  = 'Bilgiler';
$_['text_service']      = 'Müşteri Servisi';
$_['text_extra']        = 'Ekstralar';
$_['text_contact']      = 'İletişim';
$_['text_return']       = 'Ürün İadesi';
$_['text_sitemap']      = 'Site Haritası';
$_['text_manufacturer'] = 'Markalar';
$_['text_voucher']      = 'Hediye Çeki';
$_['text_affiliate']    = 'Ortaklık Programı';
$_['text_special']      = 'Kampanyalar';
$_['text_account']      = 'Hesabım';
$_['text_order']        = 'Siparişlerim';
$_['text_wishlist']     = 'Alışveriş Listem';
$_['text_newsletter']   = 'Bülten Aboneliği';
$_['text_powered']      = '<div class="col-md-6 col-xs-12">%s &copy; %s - Tüm Hakları Saklıdır.</div><div class="col-md-6 col-xs-12" style="text-align:right">Altyapı: <a href="https://www.pazarmo.com" title="Pazarmo">Pazarmo</a></div>';
