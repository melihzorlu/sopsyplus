<?php
// Heading
$_['heading_title']        = 'Sepet Hatırlatma Modülü';

$_['text_image'] 				= 'Resim';
$_['text_product'] 			= 'Ürün';
$_['text_model'] 				= 'Model';
$_['text_quantity'] 		= 'Miktar';
$_['text_date_added'] 	= 'Tarih Ekle';
$_['text_no_cart'] 			= 'Sonuç bulunamadı!';